unit ugocontrolerecebivel;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 10/08/2021*}                                          
{*Hora 01:28:39*}                                            
{*Unit gocontrolerecebivel *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus;

type                                                                                        
	Tfgocontrolerecebivel = class(TForm)

	{>>>datasets}
	sqlgocontrolerecebivel: TClientDataSet;
	dspgocontrolerecebivel: TDataSetProvider;
	cdsgocontrolerecebivel: TClientDataSet;
	dsgocontrolerecebivel: TDataSource;

	{objetos datasets<<<}

	{>>>objetos gerais}
	pnlTitulo: TPanel;
	lblTitulo: TLabel;
	spdOpcoes: TSpeedButton;
	spdImpressao: TSpeedButton;
	spdExportacao: TSpeedButton;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	pnlLogo: TPanel;
	pnlLinhaTitulo: TPanel;
	imglogo: TImage;
	lblSite: TLabel;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgocontrolerecebivel: TfrxReport;
	frxDBgocontrolerecebivel: TfrxDBDataset;
	Popupgocontrolerecebivel: TPopupMenu;
	{objetos gerais<<<}

	{>>>objetos de manipula��o de dados}
	pgcPrincipal: TPageControl;
	tabPrincipal: TTabSheet;
	Gridgocontrolerecebivel: TcxGrid;
	lvlregistrosgocontrolerecebivel: TcxGridLevel;
	grdregistrostbwgocontrolerecebivel: TcxGridDBTableView;
	GridResultadoPesquisa: TcxGrid;
	lvlResultadoPesquisa : TcxGridLevel;
	grdtbwResultadoPesquisa: TcxGridDBTableView;
	tabgocontrolerecebivel: tcxTabSheet;
	SCRgocontrolerecebivel: TScrollBox;
	pnlDadosgocontrolerecebivel: TPanel;
	{>>>bot�es de manipula��o de dados gocontrolerecebivel}
	pnlLinhaBotoesgocontrolerecebivel: TPanel;
	spdAdicionargocontrolerecebivel: TButton;
	spdAlterargocontrolerecebivel: TButton;
	spdGravargocontrolerecebivel: TButton;
	spdCancelargocontrolerecebivel: TButton;
	spdExcluirgocontrolerecebivel: TButton;

	{bot�es de manipula��o de dadosgocontrolerecebivel<<<}
	{campo ID}
	sqlgocontrolerecebivelID: TIntegerField;
	cdsgocontrolerecebivelID: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel1: TPanel;
	pnlLayoutTitulosgocontrolerecebivelid: TPanel;
	pnlLayoutCamposgocontrolerecebivelid: TPanel;
	dbegocontrolerecebivelid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgocontrolerecebivelDT_CADASTRO: TDateField;
	cdsgocontrolerecebivelDT_CADASTRO: TDateField;
	pnlLayoutLinhasgocontrolerecebivel2: TPanel;
	pnlLayoutTitulosgocontrolerecebiveldt_cadastro: TPanel;
	pnlLayoutCamposgocontrolerecebiveldt_cadastro: TPanel;
	dbegocontrolerecebiveldt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgocontrolerecebivelHS_CADASTRO: TTimeField;
	cdsgocontrolerecebivelHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgocontrolerecebivel3: TPanel;
	pnlLayoutTitulosgocontrolerecebivelhs_cadastro: TPanel;
	pnlLayoutCamposgocontrolerecebivelhs_cadastro: TPanel;
	dbegocontrolerecebivelhs_cadastro: TDBEdit;

	{campo CODPORTADOR}
	sqlgocontrolerecebivelCODPORTADOR: TIntegerField;
	cdsgocontrolerecebivelCODPORTADOR: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel4: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcodportador: TPanel;
	pnlLayoutCamposgocontrolerecebivelcodportador: TPanel;
	dbegocontrolerecebivelcodportador: TDBEdit;

	{campo PORTADOR}
	sqlgocontrolerecebivelPORTADOR: TStringField;
	cdsgocontrolerecebivelPORTADOR: TStringField;
	pnlLayoutLinhasgocontrolerecebivel5: TPanel;
	pnlLayoutTitulosgocontrolerecebivelportador: TPanel;
	pnlLayoutCamposgocontrolerecebivelportador: TPanel;
	dbegocontrolerecebivelportador: TDBEdit;

	{campo CODFILIAL}
	sqlgocontrolerecebivelCODFILIAL: TIntegerField;
	cdsgocontrolerecebivelCODFILIAL: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel6: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcodfilial: TPanel;
	pnlLayoutCamposgocontrolerecebivelcodfilial: TPanel;
	dbegocontrolerecebivelcodfilial: TDBEdit;

	{campo FILIAL}
	sqlgocontrolerecebivelFILIAL: TStringField;
	cdsgocontrolerecebivelFILIAL: TStringField;
	pnlLayoutLinhasgocontrolerecebivel7: TPanel;
	pnlLayoutTitulosgocontrolerecebivelfilial: TPanel;
	pnlLayoutCamposgocontrolerecebivelfilial: TPanel;
	dbegocontrolerecebivelfilial: TDBEdit;

	{campo DTCOMPETENCIA}
	sqlgocontrolerecebivelDTCOMPETENCIA: TDateField;
	cdsgocontrolerecebivelDTCOMPETENCIA: TDateField;
	pnlLayoutLinhasgocontrolerecebivel8: TPanel;
	pnlLayoutTitulosgocontrolerecebiveldtcompetencia: TPanel;
	pnlLayoutCamposgocontrolerecebiveldtcompetencia: TPanel;
	dbegocontrolerecebiveldtcompetencia: TDBEdit;

	{campo CODCARTEIRA}
	sqlgocontrolerecebivelCODCARTEIRA: TIntegerField;
	cdsgocontrolerecebivelCODCARTEIRA: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel9: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcodcarteira: TPanel;
	pnlLayoutCamposgocontrolerecebivelcodcarteira: TPanel;
	dbegocontrolerecebivelcodcarteira: TDBEdit;

	{campo CARTEIRA}
	sqlgocontrolerecebivelCARTEIRA: TStringField;
	cdsgocontrolerecebivelCARTEIRA: TStringField;
	pnlLayoutLinhasgocontrolerecebivel10: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcarteira: TPanel;
	pnlLayoutCamposgocontrolerecebivelcarteira: TPanel;
	dbegocontrolerecebivelcarteira: TDBEdit;

	{campo CODTIPO}
	sqlgocontrolerecebivelCODTIPO: TIntegerField;
	cdsgocontrolerecebivelCODTIPO: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel11: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcodtipo: TPanel;
	pnlLayoutCamposgocontrolerecebivelcodtipo: TPanel;
	dbegocontrolerecebivelcodtipo: TDBEdit;

	{campo TIPO}
	sqlgocontrolerecebivelTIPO: TStringField;
	cdsgocontrolerecebivelTIPO: TStringField;
	pnlLayoutLinhasgocontrolerecebivel12: TPanel;
	pnlLayoutTitulosgocontrolerecebiveltipo: TPanel;
	pnlLayoutCamposgocontrolerecebiveltipo: TPanel;
	dbegocontrolerecebiveltipo: TDBEdit;

	{campo NUMBORDERO}
	sqlgocontrolerecebivelNUMBORDERO: TFloatField;
	cdsgocontrolerecebivelNUMBORDERO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel13: TPanel;
	pnlLayoutTitulosgocontrolerecebivelnumbordero: TPanel;
	pnlLayoutCamposgocontrolerecebivelnumbordero: TPanel;
	dbegocontrolerecebivelnumbordero: TDBEdit;

	{campo QTTITULOS}
	sqlgocontrolerecebivelQTTITULOS: TIntegerField;
	cdsgocontrolerecebivelQTTITULOS: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel14: TPanel;
	pnlLayoutTitulosgocontrolerecebivelqttitulos: TPanel;
	pnlLayoutCamposgocontrolerecebivelqttitulos: TPanel;
	dbegocontrolerecebivelqttitulos: TDBEdit;

	{campo VLBRUTOBORDERO}
	sqlgocontrolerecebivelVLBRUTOBORDERO: TFloatField;
	cdsgocontrolerecebivelVLBRUTOBORDERO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel15: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvlbrutobordero: TPanel;
	pnlLayoutCamposgocontrolerecebivelvlbrutobordero: TPanel;
	dbegocontrolerecebivelvlbrutobordero: TDBEdit;

	{campo QTDRECUSADA}
	sqlgocontrolerecebivelQTDRECUSADA: TIntegerField;
	cdsgocontrolerecebivelQTDRECUSADA: TIntegerField;
	pnlLayoutLinhasgocontrolerecebivel16: TPanel;
	pnlLayoutTitulosgocontrolerecebivelqtdrecusada: TPanel;
	pnlLayoutCamposgocontrolerecebivelqtdrecusada: TPanel;
	dbegocontrolerecebivelqtdrecusada: TDBEdit;

	{campo VLBRUTORECUSADO}
	sqlgocontrolerecebivelVLBRUTORECUSADO: TFloatField;
	cdsgocontrolerecebivelVLBRUTORECUSADO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel17: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvlbrutorecusado: TPanel;
	pnlLayoutCamposgocontrolerecebivelvlbrutorecusado: TPanel;
	dbegocontrolerecebivelvlbrutorecusado: TDBEdit;

	{campo PRAZOMEDIO}
	sqlgocontrolerecebivelPRAZOMEDIO: TFloatField;
	cdsgocontrolerecebivelPRAZOMEDIO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel18: TPanel;
	pnlLayoutTitulosgocontrolerecebivelprazomedio: TPanel;
	pnlLayoutCamposgocontrolerecebivelprazomedio: TPanel;
	dbegocontrolerecebivelprazomedio: TDBEdit;

	{campo PRAZOMEDIOACORDADO}
	sqlgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField;
	cdsgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel19: TPanel;
	pnlLayoutTitulosgocontrolerecebivelprazomedioacordado: TPanel;
	pnlLayoutCamposgocontrolerecebivelprazomedioacordado: TPanel;
	dbegocontrolerecebivelprazomedioacordado: TDBEdit;

	{campo TAC}
	sqlgocontrolerecebivelTAC: TFloatField;
	cdsgocontrolerecebivelTAC: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel20: TPanel;
	pnlLayoutTitulosgocontrolerecebiveltac: TPanel;
	pnlLayoutCamposgocontrolerecebiveltac: TPanel;
	dbegocontrolerecebiveltac: TDBEdit;

	{campo TARIFAENTRADATITULOS}
	sqlgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField;
	cdsgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel21: TPanel;
	pnlLayoutTitulosgocontrolerecebiveltarifaentradatitulos: TPanel;
	pnlLayoutCamposgocontrolerecebiveltarifaentradatitulos: TPanel;
	dbegocontrolerecebiveltarifaentradatitulos: TDBEdit;

	{campo DESAGIOACORDADO}
	sqlgocontrolerecebivelDESAGIOACORDADO: TFloatField;
	cdsgocontrolerecebivelDESAGIOACORDADO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel22: TPanel;
	pnlLayoutTitulosgocontrolerecebiveldesagioacordado: TPanel;
	pnlLayoutCamposgocontrolerecebiveldesagioacordado: TPanel;
	dbegocontrolerecebiveldesagioacordado: TDBEdit;

	{campo CALCULOIOF}
	sqlgocontrolerecebivelCALCULOIOF: TFloatField;
	cdsgocontrolerecebivelCALCULOIOF: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel23: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcalculoiof: TPanel;
	pnlLayoutCamposgocontrolerecebivelcalculoiof: TPanel;
	dbegocontrolerecebivelcalculoiof: TDBEdit;

	{campo OUTRASDESPESAS}
	sqlgocontrolerecebivelOUTRASDESPESAS: TFloatField;
	cdsgocontrolerecebivelOUTRASDESPESAS: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel24: TPanel;
	pnlLayoutTitulosgocontrolerecebiveloutrasdespesas: TPanel;
	pnlLayoutCamposgocontrolerecebiveloutrasdespesas: TPanel;
	dbegocontrolerecebiveloutrasdespesas: TDBEdit;

	{campo PERCTAXANOMINALACORDADA}
	sqlgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField;
	cdsgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel25: TPanel;
	pnlLayoutTitulosgocontrolerecebivelperctaxanominalacordada: TPanel;
	pnlLayoutCamposgocontrolerecebivelperctaxanominalacordada: TPanel;
	dbegocontrolerecebivelperctaxanominalacordada: TDBEdit;

	{campo VLCOBERTURAFORMENTO}
	sqlgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField;
	cdsgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel26: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvlcoberturaformento: TPanel;
	pnlLayoutCamposgocontrolerecebivelvlcoberturaformento: TPanel;
	dbegocontrolerecebivelvlcoberturaformento: TDBEdit;

	{campo VLDESTINADOENCARGOFOMENTO}
	sqlgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel27: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadoencargofomento: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadoencargofomento: TPanel;
	dbegocontrolerecebivelvldestinadoencargofomento: TDBEdit;

	{campo VLDESTINADORECOMPORATITULO}
	sqlgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField;
	cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel28: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadorecomporatitulo: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadorecomporatitulo: TPanel;
	dbegocontrolerecebivelvldestinadorecomporatitulo: TDBEdit;

	{campo VLDESTINADODEFASAGEM}
	sqlgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField;
	cdsgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel29: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadodefasagem: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadodefasagem: TPanel;
	dbegocontrolerecebivelvldestinadodefasagem: TDBEdit;

	{campo VLDESTINADOCOMISSARIA}
	sqlgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel30: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadocomissaria: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadocomissaria: TPanel;
	dbegocontrolerecebivelvldestinadocomissaria: TDBEdit;

	{campo VLDESTINADOGARANTIA}
	sqlgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel31: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadogarantia: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadogarantia: TPanel;
	dbegocontrolerecebivelvldestinadogarantia: TDBEdit;

	{campo VLDESTINADORETENCAODIVIDA}
	sqlgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel32: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadoretencaodivida: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadoretencaodivida: TPanel;
	dbegocontrolerecebivelvldestinadoretencaodivida: TDBEdit;

	{campo VLDESTINADOCONTAGRAFICA}
	sqlgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel33: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvldestinadocontagrafica: TPanel;
	pnlLayoutCamposgocontrolerecebivelvldestinadocontagrafica: TPanel;
	dbegocontrolerecebivelvldestinadocontagrafica: TDBEdit;

	{campo CUSTOSFORAOPERACAO}
	sqlgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField;
	cdsgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel34: TPanel;
	pnlLayoutTitulosgocontrolerecebivelcustosforaoperacao: TPanel;
	pnlLayoutCamposgocontrolerecebivelcustosforaoperacao: TPanel;
	dbegocontrolerecebivelcustosforaoperacao: TDBEdit;

	{campo VLOUTROSCREDITOS}
	sqlgocontrolerecebivelVLOUTROSCREDITOS: TFloatField;
	cdsgocontrolerecebivelVLOUTROSCREDITOS: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel35: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvloutroscreditos: TPanel;
	pnlLayoutCamposgocontrolerecebivelvloutroscreditos: TPanel;
	dbegocontrolerecebivelvloutroscreditos: TDBEdit;

	{campo VLRECEBIDOCAIXA}
	sqlgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField;
	cdsgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField;
	pnlLayoutLinhasgocontrolerecebivel36: TPanel;
	pnlLayoutTitulosgocontrolerecebivelvlrecebidocaixa: TPanel;
	pnlLayoutCamposgocontrolerecebivelvlrecebidocaixa: TPanel;
	dbegocontrolerecebivelvlrecebidocaixa: TDBEdit;

	{campo DTCREDITO}
	sqlgocontrolerecebivelDTCREDITO: TDateField;
	cdsgocontrolerecebivelDTCREDITO: TDateField;
	pnlLayoutLinhasgocontrolerecebivel37: TPanel;
	pnlLayoutTitulosgocontrolerecebiveldtcredito: TPanel;
	pnlLayoutCamposgocontrolerecebiveldtcredito: TPanel;
	dbegocontrolerecebiveldtcredito: TDBEdit;

	{campo DTINDICE}
	sqlgocontrolerecebivelDTINDICE: TDateField;
	cdsgocontrolerecebivelDTINDICE: TDateField;
	pnlLayoutLinhasgocontrolerecebivel38: TPanel;
	pnlLayoutTitulosgocontrolerecebiveldtindice: TPanel;
	pnlLayoutCamposgocontrolerecebiveldtindice: TPanel;
	dbegocontrolerecebiveldtindice: TDBEdit;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure cdsgocontrolerecebivelAfterClose(DataSet: TDataSet);
	procedure cdsgocontrolerecebivelAfterOpen(DataSet: TDataSet);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargocontrolerecebivelClick(Sender: TObject);
	procedure spdAlterargocontrolerecebivelClick(Sender: TObject);
	procedure spdGravargocontrolerecebivelClick(Sender: TObject);
	procedure spdCancelargocontrolerecebivelClick(Sender: TObject);
	procedure spdExcluirgocontrolerecebivelClick(Sender: TObject);
	procedure cdsgocontrolerecebivelBeforePost(DataSet: TDataSet);
	procedure grdregistrostbwgocontrolerecebivelDblClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgocontrolerecebivel: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgocontrolerecebivel: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgocontrolerecebivel: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgocontrolerecebivel: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagocontrolerecebivel: Array[0..2]  of TDuplicidade;
	duplicidadeCampogocontrolerecebivel: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
	public

end;

var
fgocontrolerecebivel: Tfgocontrolerecebivel;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgocontrolerecebivel.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgocontrolerecebivel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgocontrolerecebivel.verificarEmTransacao(cdsgocontrolerecebivel);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgocontrolerecebivel.configurarGridesFormulario(fgocontrolerecebivel,true, false);

	fncgocontrolerecebivel.Free;
	{eliminando container de fun��es da memoria<<<}

	fgocontrolerecebivel:=nil;
	Action:=cafree;
end;

procedure Tfgocontrolerecebivel.FormCreate(Sender: TObject);
begin
	{>>>container de fun��es}
	fncgocontrolerecebivel:= TFuncoes.Create(Self);
	fncgocontrolerecebivel.definirConexao(dm.conexao);
	fncgocontrolerecebivel.definirConexaohistorico(dm.conexao);
	fncgocontrolerecebivel.definirFormulario(Self);
	fncgocontrolerecebivel.validarTransacaoRelacionamento:=true;
	fncgocontrolerecebivel.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
	duplicidadeCampogocontrolerecebivel.agrupador:='';
	duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebivelportador;
	duplicidadeListagocontrolerecebivel[0]:=duplicidadeCampogocontrolerecebivel;

	duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
	duplicidadeCampogocontrolerecebivel.agrupador:='';
	duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebivelfilial;
	duplicidadeListagocontrolerecebivel[1]:=duplicidadeCampogocontrolerecebivel;

	duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
	duplicidadeCampogocontrolerecebivel.agrupador:='';
	duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebiveldtcompetencia;
	duplicidadeListagocontrolerecebivel[2]:=duplicidadeCampogocontrolerecebivel;

	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='PESCODPORTADOR';
	pesquisasItem.titulo:='C�d.Portador';
	pesquisasItem.campo:='CODPORTADOR';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPORTADOR';
	pesquisasItem.titulo:='Portador';
	pesquisasItem.campo:='PORTADOR';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCARTEIRA';
	pesquisasItem.titulo:='Carteira';
	pesquisasItem.campo:='CARTEIRA';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesTIPO';
	pesquisasItem.titulo:='Tipo';
	pesquisasItem.campo:='TIPO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

	fncgocontrolerecebivel.criarPesquisas(sqlgocontrolerecebivel,cdsgocontrolerecebivel,'select * from gocontrolerecebivel', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gocontrolerecebivel}
	camposObrigatoriosgocontrolerecebivel[0] :=dbegocontrolerecebiveldtcompetencia;
	{campos obrigatorios gocontrolerecebivel<<<}

	{>>>objetos ativos no modo de manipula��o de dados gocontrolerecebivel}
	objetosModoEdicaoAtivosgocontrolerecebivel[0]:=pnlDadosgocontrolerecebivel;
	objetosModoEdicaoAtivosgocontrolerecebivel[1]:=spdCancelargocontrolerecebivel;
	objetosModoEdicaoAtivosgocontrolerecebivel[2]:=spdGravargocontrolerecebivel;
	{objetos ativos no modo de manipula��o de dados gocontrolerecebivel<<<}

	{>>>objetos inativos no modo de manipula��o de dados gocontrolerecebivel}
	objetosModoEdicaoInativosgocontrolerecebivel[0]:=spdAdicionargocontrolerecebivel;
	objetosModoEdicaoInativosgocontrolerecebivel[1]:=spdAlterargocontrolerecebivel;
	objetosModoEdicaoInativosgocontrolerecebivel[2]:=spdExcluirgocontrolerecebivel;
	objetosModoEdicaoInativosgocontrolerecebivel[3]:=spdOpcoes;
	objetosModoEdicaoInativosgocontrolerecebivel[4]:=spdImpressao;
	objetosModoEdicaoInativosgocontrolerecebivel[5]:=spdExportacao;
	objetosModoEdicaoInativosgocontrolerecebivel[6]:=tabPrincipal;
	objetosModoEdicaoInativosgocontrolerecebivel[7]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gocontrolerecebivel<<<}

	{>>>comando de adi��o de teclas de atalhos gocontrolerecebivel}
	fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Adicionar novo registro',VK_F4,spdAdicionargocontrolerecebivel.OnClick);
	fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Alterar registro selecionado',VK_F5,spdAlterargocontrolerecebivel.OnClick);
	fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Gravar �ltimas altera��es',VK_F6,spdGravargocontrolerecebivel.OnClick);
	fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Cancelar �ltimas altera��e',VK_F7,spdCancelargocontrolerecebivel.OnClick);
	fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgocontrolerecebivel.OnClick);

	{comando de adi��o de teclas de atalhos gocontrolerecebivel<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgocontrolerecebivel.open();
	{abertura dos datasets<<<}

	fncgocontrolerecebivel.criaAtalhoPopupMenuNavegacao(Popupgocontrolerecebivel,cdsgocontrolerecebivel);

	fncgocontrolerecebivel.definirMascaraCampos(cdsgocontrolerecebivel);

	fncgocontrolerecebivel.configurarGridesFormulario(fgocontrolerecebivel,false, true);

end;

procedure Tfgocontrolerecebivel.spdAdicionargocontrolerecebivelClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocontrolerecebivel.TabIndex;
	if (fncgocontrolerecebivel.adicionar(cdsgocontrolerecebivel)=true) then
		begin
			fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
			dbegocontrolerecebivelcodportador.SetFocus;
	end
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false);
end;

procedure Tfgocontrolerecebivel.spdAlterargocontrolerecebivelClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocontrolerecebivel.TabIndex;
	if (fncgocontrolerecebivel.alterar(cdsgocontrolerecebivel)=true) then
		begin
			fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
			dbegocontrolerecebivelcodportador.SetFocus;
	end
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false);
end;

procedure Tfgocontrolerecebivel.spdGravargocontrolerecebivelClick(Sender: TObject);
begin
	fncgocontrolerecebivel.verificarCamposObrigatorios(cdsgocontrolerecebivel, camposObrigatoriosgocontrolerecebivel);

	fncgocontrolerecebivel.verificarDuplicidade(dm.conexao, cdsgocontrolerecebivel, 'gocontrolerecebivel', 'ID', duplicidadeListagocontrolerecebivel);

	if (fncgocontrolerecebivel.gravar(cdsgocontrolerecebivel)=true) then
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false)
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
end;

procedure Tfgocontrolerecebivel.spdCancelargocontrolerecebivelClick(Sender: TObject);
begin
	if (fncgocontrolerecebivel.cancelar(cdsgocontrolerecebivel)=true) then
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false)
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
end;

procedure Tfgocontrolerecebivel.spdExcluirgocontrolerecebivelClick(Sender: TObject);
begin
	fncgocontrolerecebivel.excluir(cdsgocontrolerecebivel);
end;

procedure Tfgocontrolerecebivel.cdsgocontrolerecebivelBeforePost(DataSet: TDataSet);
begin
	if cdsgocontrolerecebivel.State in [dsinsert] then
	cdsgocontrolerecebivelID.Value:=fncgocontrolerecebivel.autoNumeracaoGenerator(dm.conexao,'G_gocontrolerecebivel');
end;

procedure Tfgocontrolerecebivel.GridgocontrolerecebivelDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocontrolerecebivel.TabIndex;
end;

procedure Tfgocontrolerecebivel.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgocontrolerecebivel.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgocontrolerecebivel.cdsgocontrolerecebivelAfterOpen(DataSet: TDataSet);
begin
end;

procedure Tfgocontrolerecebivel.cdsgocontrolerecebivelAfterClose(DataSet: TDataSet);
begin
end;

procedure Tfgocontrolerecebivel.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgocontrolerecebivel.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgocontrolerecebivel.IsEmpty then 
	fncgocontrolerecebivel.visualizarRelatorios(frxgocontrolerecebivel);
end;

end.

