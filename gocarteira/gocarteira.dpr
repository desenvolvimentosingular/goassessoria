program gocarteira;

uses
  Vcl.Forms,
  ugocarteira in 'ugocarteira.pas' {fgocarteira},
  Vcl.Themes,
  Vcl.Styles,
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Turquoise Gray');
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfgocarteira, fgocarteira);
  Application.Run;
end.
