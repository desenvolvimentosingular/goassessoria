program usuario;

uses
  Vcl.Forms,
  uusuarios in 'uusuarios.pas' {fusuarios},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Turquoise Gray');
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfusuarios, fusuarios);
  Application.Run;
end.
