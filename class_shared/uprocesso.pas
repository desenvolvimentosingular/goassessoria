unit uprocesso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ExtCtrls, dxGDIPlusClasses,
  cxImage, Vcl.StdCtrls, cxGroupBox, cxProgressBar;

type
   Tfprocesso = class(TForm)
    gpxdados: TcxGroupBox;
    Gifwait: TImage;
    lblprocessando: TLabel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    lblSite: TLabel;
    imglogo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  class function CriarFormulario(componente: TComponent): Tfprocesso;
  procedure  ExibirMensagem(const psMensagem: string; piTempo: SmallInt; pAndamento :String);
  end;

var
  fprocesso: Tfprocesso;

implementation

{$R *.dfm}

procedure Tfprocesso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{eliminando container de fun��es da memoria<<<}
  fprocesso:=nil;
	Action:=cafree;
  {eliminando container de fun��es da memoria<<<}
end;
class function Tfprocesso.CriarFormulario(componente: TComponent): Tfprocesso;
begin
  { Criando o formul�rio }
  Result := Tfprocesso.Create(componente);
  { Exibindo o formul�rio }
  Result.Show;
  { Primeira mensagem }
  Result.ExibirMensagem('Inicializando!',0,'');

end;

procedure Tfprocesso.ExibirMensagem(const psMensagem: string;
  piTempo: SmallInt; pAndamento :String);
begin
  { Exibindo a mensagem do processo que esta em andamento }
//  lblVerificando.Caption    := psMensagem;
  Application.ProcessMessages;
  { Tempo de espera }
  sleep(piTempo);
end;

procedure Tfprocesso.FormDeactivate(Sender: TObject);
begin
//  If fprocesso.CanFocus then
//    fprocesso.SetFocus;

end;

end.
