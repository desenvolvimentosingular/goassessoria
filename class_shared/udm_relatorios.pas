unit udm_relatorios;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxClass, frxDBSet, Data.FMTBcd,
  Datasnap.DBClient, Datasnap.Provider, Data.SqlExpr;

type
  Tdm_relatorios = class(TDataModule)
    frxpcpop: TfrxReport;
    frxdbpcpop: TfrxDBDataset;
    sqlpcop_linha: TSQLDataSet;
    sqlpcop_linhaID: TIntegerField;
    sqlpcop_linhaDT_CADASTRO: TDateField;
    sqlpcop_linhaHS_CADASTRO: TTimeField;
    sqlpcop_linhaNUMOP: TIntegerField;
    sqlpcop_linhaID_LINHA: TIntegerField;
    sqlpcop_linhaLINHA: TStringField;
    dsppcop_linha: TDataSetProvider;
    cdspcop_linha: TClientDataSet;
    cdspcop_linhaID: TIntegerField;
    cdspcop_linhaDT_CADASTRO: TDateField;
    cdspcop_linhaHS_CADASTRO: TTimeField;
    cdspcop_linhaNUMOP: TIntegerField;
    cdspcop_linhaID_LINHA: TIntegerField;
    cdspcop_linhaLINHA: TStringField;
    sqlpcpopetapaproducao: TSQLDataSet;
    sqlpcpopetapaproducaoID: TIntegerField;
    sqlpcpopetapaproducaoDT_CADASTRO: TDateField;
    sqlpcpopetapaproducaoHS_CADASTRO: TTimeField;
    sqlpcpopetapaproducaoID_ROTEIRO_ETAPA_PRODUCAO: TIntegerField;
    sqlpcpopetapaproducaoNUMOP: TIntegerField;
    sqlpcpopetapaproducaoDTINICIO: TSQLTimeStampField;
    sqlpcpopetapaproducaoDTFINAL: TSQLTimeStampField;
    sqlpcpopetapaproducaoSTATUS: TStringField;
    sqlpcpopetapaproducaoID_ETAPA_PRODUCAO: TIntegerField;
    sqlpcpopetapaproducaoETAPA_PRODUCAO: TStringField;
    sqlpcpopetapaproducaoOBRIGATORIA: TStringField;
    sqlpcpopetapaproducaoID_PRODUTO: TIntegerField;
    sqlpcpopetapaproducaoPRODUTO: TStringField;
    dsppcpopetapaproducao: TDataSetProvider;
    cdspcpopetapaproducao: TClientDataSet;
    cdspcpopetapaproducaoID: TIntegerField;
    cdspcpopetapaproducaoDT_CADASTRO: TDateField;
    cdspcpopetapaproducaoHS_CADASTRO: TTimeField;
    cdspcpopetapaproducaoNUMOP: TIntegerField;
    cdspcpopetapaproducaoDTINICIO: TSQLTimeStampField;
    cdspcpopetapaproducaoDTFINAL: TSQLTimeStampField;
    cdspcpopetapaproducaoSTATUS: TStringField;
    cdspcpopetapaproducaoID_ETAPA_PRODUCAO: TIntegerField;
    cdspcpopetapaproducaoETAPA_PRODUCAO: TStringField;
    cdspcpopetapaproducaoOBRIGATORIA: TStringField;
    cdspcpopetapaproducaoID_PRODUTO: TIntegerField;
    cdspcpopetapaproducaoPRODUTO: TStringField;
    cdspcpopetapaproducaoID_ROTEIRO_ETAPA_PRODUCAO: TIntegerField;
    sqlpcpoprecursoproducao: TSQLDataSet;
    sqlpcpoprecursoproducaoID: TIntegerField;
    sqlpcpoprecursoproducaoDT_CADASTRO: TDateField;
    sqlpcpoprecursoproducaoHS_CADASTRO: TTimeField;
    sqlpcpoprecursoproducaoNUMOP: TIntegerField;
    sqlpcpoprecursoproducaoID_RECURSO: TIntegerField;
    sqlpcpoprecursoproducaoQTHORA: TFloatField;
    sqlpcpoprecursoproducaoCUSTOHORA: TFloatField;
    sqlpcpoprecursoproducaoID_ROTEIRO_RECURSO_PRODUCAO: TIntegerField;
    sqlpcpoprecursoproducaoRECURSO_PRODUCAO: TStringField;
    dsppcpoprecursoproducao: TDataSetProvider;
    cdspcpoprecursoproducao: TClientDataSet;
    cdspcpoprecursoproducaoID: TIntegerField;
    cdspcpoprecursoproducaoDT_CADASTRO: TDateField;
    cdspcpoprecursoproducaoHS_CADASTRO: TTimeField;
    cdspcpoprecursoproducaoNUMOP: TIntegerField;
    cdspcpoprecursoproducaoID_RECURSO: TIntegerField;
    cdspcpoprecursoproducaoQTHORA: TFloatField;
    cdspcpoprecursoproducaoCUSTOHORA: TFloatField;
    cdspcpoprecursoproducaoID_ROTEIRO_RECURSO_PRODUCAO: TIntegerField;
    cdspcpoprecursoproducaoRECURSO_PRODUCAO: TStringField;
    frxdbpcpopetapaproducao: TfrxDBDataset;
    frxdbpcpoprecursoproducao: TfrxDBDataset;
    frxDBlinhaproducao: TfrxDBDataset;
    sqlpcpop: TSQLDataSet;
    dsppcpop: TDataSetProvider;
    cdspcpop: TClientDataSet;
    dspcpop: TDataSource;
    cdspcpopAGRUPADOR: TStringField;
    cdspcpopNUMOP: TIntegerField;
    cdspcpopDT_CADASTRO: TDateField;
    cdspcpopID_FILIAL: TIntegerField;
    cdspcpopFILIAL: TStringField;
    cdspcpopID_LOCAL: TIntegerField;
    cdspcpopLOCAL: TStringField;
    cdspcpopNUMPED: TIntegerField;
    cdspcpopCODPRODMASTER: TIntegerField;
    cdspcpopDESCRICAO_ACABADO: TStringField;
    cdspcpopTIPOMERC_PA: TStringField;
    cdspcpopNUMLOTE: TStringField;
    cdspcpopMETODO: TStringField;
    cdspcpopQTPRODUZIR: TFMTBCDField;
    cdspcpopPOSICAO: TStringField;
    cdspcpopDTINICIO: TDateField;
    cdspcpopID_AREA: TIntegerField;
    cdspcpopAREA: TStringField;
    cdspcpopID_FUNCIONARIO: TIntegerField;
    cdspcpopFUNCIONARIO: TStringField;
    cdspcpopREPROCESSO: TStringField;
    cdspcpopDTVALIDADE: TDateField;
    cdspcpopUNIDADE_PA: TStringField;
    cdspcpopEMBALAGEM: TStringField;
    cdspcpopID_SIMULACAO: TIntegerField;
    cdspcpopSIMULACAO: TStringField;
    cdspcpopID_PRODUTO: TIntegerField;
    cdspcpopPRODUTO: TStringField;
    cdspcpopTIPOMERC: TStringField;
    cdspcpopUNIDADE: TStringField;
    cdspcpopQTEORICA: TFloatField;
    cdspcpopVERSAO: TStringField;
    cdspcpopQTPERDATEORICA: TFloatField;
    cdspcpopQTDEVOLUCAOTEORICA: TFloatField;
    cdspcpopCUSTOREAL: TFloatField;
    cdspcpopcustototal: TFloatField;
    frxprodutoCATEGORIA: TfrxReport;
    frxprodutoSECAO: TfrxReport;
    frxprodutoMARCA: TfrxReport;
    frxprodutoFORNECEDOR: TfrxReport;
    procedure cdspcpopAfterClose(DataSet: TDataSet);
    procedure cdspcpopAfterOpen(DataSet: TDataSet);
    procedure cdspcpopCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm_relatorios: Tdm_relatorios;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udm;

{$R *.dfm}

procedure Tdm_relatorios.cdspcpopAfterClose(DataSet: TDataSet);
begin
  cdspcpoprecursoproducao.Close;
  cdspcpopetapaproducao.Close;
  cdspcop_linha.Close;
end;

procedure Tdm_relatorios.cdspcpopAfterOpen(DataSet: TDataSet);
begin
  cdspcpoprecursoproducao.open;
  cdspcpopetapaproducao.open;
  cdspcop_linha.open;
end;

procedure Tdm_relatorios.cdspcpopCalcFields(DataSet: TDataSet);
begin
   cdspcpopcustototal.AsFloat  := (cdspcpopcustoreal.AsFloat* cdspcpopQTEORICA.AsFloat);
end;

end.
