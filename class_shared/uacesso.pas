unit uacesso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.StdCtrls, Vcl.ExtCtrls,
  cxGroupBox;

type
  Tfacesso = class(TForm)
    gpxdados: TcxGroupBox;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    lblSite: TLabel;
    pnlLayoutLinhaspermissao1: TPanel;
    pnlLayoutCampospermissaoid: TPanel;
    pnlLayoutLinhaspermissaolinha1: TPanel;
    dbepermissao_usuarioUSUARIO: TEdit;
    pnlLayoutLinhaspermissao2: TPanel;
    pnlLayoutCampospermissaodestino: TPanel;
    pnlLayoutLinhaspermissaolinha2: TPanel;
    dbepermissao_usuarioSENHA: TEdit;
    spdcancelar: TButton;
    spdconfirmar: TButton;
    procedure spdconfirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbepermissao_usuarioUSUARIOExit(Sender: TObject);
    procedure dbepermissao_usuarioSENHAExit(Sender: TObject);
    procedure spdcancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  facesso: Tfacesso;

implementation

{$R *.dfm}

procedure Tfacesso.dbepermissao_usuarioSENHAExit(Sender: TObject);
begin
  spdconfirmar.SetFocus;
end;

procedure Tfacesso.dbepermissao_usuarioUSUARIOExit(Sender: TObject);
begin
  dbepermissao_usuarioSENHA.SetFocus;
end;

procedure Tfacesso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//   facesso:=nil;
//   action:=cafree;
end;

procedure Tfacesso.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfacesso.FormShow(Sender: TObject);
begin
  dbepermissao_usuarioUSUARIO.SetFocus;
end;

procedure Tfacesso.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tfacesso.spdcancelarClick(Sender: TObject);
begin
  ModalResult := mrcancel;
end;

procedure Tfacesso.spdconfirmarClick(Sender: TObject);
begin
  if not ((dbepermissao_usuarioUSUARIO.Text='TI') and (dbepermissao_usuarioSENHA.Text='@SYSTEM')) then
  begin
   Application.MessageBox('Credencias de Administrador n�o conferem.!','Acesso supervisionado',MB_OK+64);
   dbepermissao_usuarioUSUARIO.SetFocus;
   abort;
  end
  else
   ModalResult := mrok;

end;

end.
