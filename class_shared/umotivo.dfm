object fmotivo: Tfmotivo
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  BorderStyle = bsNone
  Caption = 'Motivo'
  ClientHeight = 107
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlbase: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 107
    Align = alClient
    BevelKind = bkSoft
    BevelOuter = bvNone
    TabOrder = 0
    object pnlcontroles: TPanel
      Left = 0
      Top = 78
      Width = 540
      Height = 25
      Align = alBottom
      BevelOuter = bvNone
      Color = 13752796
      ParentBackground = False
      TabOrder = 0
      object spdok: TButton
        Left = 480
        Top = 0
        Width = 60
        Height = 25
        Hint = 'Ok'
        Align = alRight
        Caption = 'OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = spdokClick
      end
      object spdcancelar: TButton
        Left = 420
        Top = 0
        Width = 60
        Height = 25
        Hint = 'Cancelar'
        Align = alRight
        Caption = 'Cancelar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = spdcancelarClick
      end
    end
    object pnlLinhaTitulo: TPanel
      Left = 0
      Top = 33
      Width = 540
      Height = 5
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Color = 7330047
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
    end
    object pnlTitulo: TPanel
      Left = 0
      Top = 0
      Width = 540
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      Color = 11833701
      ParentBackground = False
      TabOrder = 2
      OnMouseDown = pnlTituloMouseDown
      object lblTitulo: TLabel
        Left = 6
        Top = 8
        Width = 207
        Height = 16
        Caption = 'Singular - Sistemas de Ind'#250'stria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object pnlLogo: TPanel
        Left = 492
        Top = 0
        Width = 48
        Height = 33
        Align = alRight
        BevelOuter = bvNone
        Color = 11833701
        ParentBackground = False
        TabOrder = 0
        object spdfechar: TSpeedButton
          Left = 5
          Top = 3
          Width = 39
          Height = 24
          Hint = 'Assistente de impress'#227'o'
          Caption = 'X'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = spdfecharClick
        end
      end
    end
    object pnlLayoutLinhasmeta_producao_item5: TPanel
      Left = 0
      Top = 48
      Width = 540
      Height = 30
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object btnPesquisarMOTIVO: TSpeedButton
        Left = 147
        Top = 3
        Width = 22
        Height = 21
        Caption = '...'
      end
      object pnlLayoutTitulosmeta_producao_itemid_produto: TPanel
        Left = 0
        Top = 0
        Width = 89
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '              Motivo:'
        TabOrder = 1
      end
      object pnlLayoutCamposmeta_producao_itemid_produto: TPanel
        Left = 89
        Top = 0
        Width = 60
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 0
        object dbemotivoID: TEdit
          Left = 2
          Top = 3
          Width = 56
          Height = 21
          Hint = 'C'#243'digo Motivo'
          Color = 15129292
          TabOrder = 0
        end
      end
      object cxm: TcxImage
        Left = -2
        Top = -6
        Enabled = False
        ParentColor = True
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
          11744558745469746C65005461736B3B436865636B3B5DF4CC7F0000016F4944
          41545847CD94C14A03311445AB5FE08FA9F54F44C10F90F42B145CBB76A3EE04
          AD6D053742B54B37A22E0545AD4E7C37E485C9CCCB4C68D3D80B8776EE8439B7
          2DB4A3B5FE57C43227629913B1CC8958E6442C99F5FDD394AC96AFD9E10919A5
          944E01673879D524BD22D6A207A40AE45BBD730C00373C826E193C31936A4045
          CEE09B58A1DBC6559383140302F22F6293702E4FCCCC3BA04D0EE898C11333A1
          01857D6D8A24B7D74E0EE8A8C11333D28093D1A3EE1DDFEAE9CFAF6DEA09C907
          0F2F78EFE4808E1B3C31531D00F9867DA00A8C6892239096A1CAE08999F2808B
          BB272767AA23DAE436B30D78FF9CEA9DC36BEFE18047C4C8F13CEA671B808446
          EC1D0D623EF9FC0390D08832921C493200691A01F970529723C9062018B17DD0
          F7E45D75A6FBF7CFF6443D4907206F1FDF6E04E497E3B01C493E00C1885DFA39
          DAE4C84206204511F3E7BCC001B1598A0114D125964B312035550723963911CB
          9C88654EC4322762990FDDF903858E444ABDC908F80000000049454E44AE4260
          82}
        Style.BorderStyle = ebsNone
        TabOrder = 2
        Height = 36
        Width = 36
      end
      object dbemotivoMOTIVO: TEdit
        Left = 171
        Top = 3
        Width = 365
        Height = 21
        Hint = 'Motivo'
        Color = 15129292
        Enabled = False
        TabOrder = 3
      end
    end
  end
  object psqMotivo: TPesquisa
    Colunas = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 350
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TIPOMERC'
        Title.Caption = 'Tipo mercadoria'
        Width = 120
        Visible = True
      end>
    CampoResultado = 'ID'
    CampoDescricao = 'DESCRICAO'
    SQL.Strings = (
      'SELECT ID, DESCRICAO FROM MOTIVO_BLOQUEIO WHERE 1=1')
    MultiSelecao = True
    Conexao = dm.conexao
    AbrirAuto = False
    PesquisaPadrao = 1
    Filtros = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptUnknown
        Value = 'C'#243'digo'
      end
      item
        DataType = ftWideString
        Name = 'DESCRICAO'
        ParamType = ptUnknown
        Value = 'Descri'#231#227'o'
      end>
    EditCodigo = dbemotivoID
    EditDescricao = dbemotivoMOTIVO
    BotaoPesquisa = btnPesquisarMOTIVO
    OnPreencherCampos = psqMotivoPreencherCampos
    Left = 273
    Top = 3
  end
  object cdsMotivo: TClientDataSet
    PersistDataPacket.Data = {
      400000009619E0BD010000001800000002000000000003000000400002494404
      00010000000000064D4F5449564F010049000000010005574944544802000200
      FA000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'MOTIVO'
        DataType = ftString
        Size = 250
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 240
    Top = 2
    object cdsMotivoID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
    end
    object cdsMotivoMOTIVO: TStringField
      DisplayLabel = 'Motivo'
      FieldName = 'MOTIVO'
      Size = 250
    end
  end
end
