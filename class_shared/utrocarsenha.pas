unit utrocarsenha;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  dxGDIPlusClasses, Vcl.ExtCtrls, Data.FMTBcd, Data.DB, Datasnap.DBClient,
  Datasnap.Provider, Data.SqlExpr;

type
  TfTrocarSenha = class(TForm)
    pnlLinhafootertop: TPanel;
    pnlTitulo: TPanel;
    Imagelogo: TImage;
    pnlLogo: TPanel;
    spdfechar: TSpeedButton;
    pnlFundo: TPanel;
    lblSenha: TLabel;
    lblUsuario: TLabel;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    lblVersao: TLabel;
    edtUsuario: TEdit;
    edtSenhaAtual: TEdit;
    pnlTrocar: TPanel;
    spnTrocar: TSpeedButton;
    edtNovaSenha: TEdit;
    lblnovasenha: TLabel;
    edtRepetirSenha: TEdit;
    lblrepetirsenha: TLabel;
    sqlTrocarSenha: TSQLDataSet;
    dsop_dev: TDataSetProvider;
    cdsop_dev: TClientDataSet;
    cdsop_devID: TIntegerField;
    cdsop_devID_FILIAL: TIntegerField;
    cdsop_devFILIAL: TStringField;
    cdsop_devNUMTRANSACAO: TIntegerField;
    cdsop_devID_PRODUTO: TIntegerField;
    cdsop_devPRODUTO: TStringField;
    cdsop_devQT: TFMTBCDField;
    cdsop_devCUSTO: TFloatField;
    cdsop_devNUMOP: TIntegerField;
    cdsop_devCODPRODMASTER: TIntegerField;
    cdsop_devPRODUTO_ACABADO: TStringField;
    cdsop_devSTATUS: TStringField;
    cdsop_devID_LOCAL: TIntegerField;
    cdsop_devLOCAL: TStringField;
    cdsop_devTIPO_MOVIMENTACAO: TStringField;
    cdsop_devID_LINHA: TIntegerField;
    cdsop_devLINHA: TStringField;
    cdsop_devID_AREA: TIntegerField;
    cdsop_devAREA: TStringField;
    cdsop_devOBSERVACAO: TStringField;
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImagelogoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure spdfecharClick(Sender: TObject);
    procedure spnTrocarClick(Sender: TObject);
    procedure edtNovaSenhaKeyPress(Sender: TObject; var Key: Char);
    procedure edtRepetirSenhaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    sqlConnection: TSQLConnection  ;
    sqlValidarLogin: TSQLDataSet;
    sql:string;
  end;

var
  fTrocarSenha: TfTrocarSenha;

implementation

{$R *.dfm}

uses udm;

procedure TfTrocarSenha.edtNovaSenhaKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;
end;

procedure TfTrocarSenha.edtRepetirSenhaKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;
end;

procedure TfTrocarSenha.ImagelogoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
    const
   sc_DragMove = $f012;
begin
    ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TfTrocarSenha.pnlTituloMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TfTrocarSenha.spdfecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfTrocarSenha.spnTrocarClick(Sender: TObject);
begin
 sqlConnection:=udm.dm.conexao;
 if (edtUsuario.Text = '') or (edtSenhaAtual.Text = '') or (edtNovaSenha.Text = '') or (edtRepetirSenha.Text ='') then
 begin
  MessageDlg('Todos os campos devem ser preenchidos',mtInformation,[mbOK],0);
 end
 else if (edtNovaSenha.Text <> edtRepetirSenha.Text) then
 begin
  MessageDlg('As senhas n�o conferem',mtInformation,[mbOK],0)
 end
 else
 begin
   sqlValidarLogin:=TSQLDataSet.Create(Application);
   sqlValidarLogin.SQLConnection  := sqlConnection;
   sqlValidarLogin.CommandText:= 'select id_usuario, conectado,  usuario, count(*) validador from gousuarios where usuario=''' +
   edtUsuario.Text + ''' and senha=''' + edtSenhaAtual.Text + ''' group by id_usuario, conectado, usuario' ;
   sqlValidarLogin.Open;

    if sqlValidarLogin.FieldByName('validador').AsInteger = 0 then
    begin
      Application.MessageBox('Senha atual n�o confere','Auto Numera��o',MB_OK+MB_ICONERROR);
      edtUsuario.SetFocus;
      abort;
    end;

     sql:='UPDATE gousuarios set senha = ' + edtNovaSenha.text + ' where id_usuario=' + sqlValidarLogin.FieldByName('id_usuario').AsString;
     sqlConnection.ExecuteDirect(sql);
     sqlValidarLogin.Free;
     Application.MessageBox('Senha alterada com sucesso!','Senha Alterada',MB_OK);
     close;
 end;


end;

end.
