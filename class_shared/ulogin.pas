unit ulogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, jpeg, WideStrings, DB, SqlExpr, ufuncoes, Vcl.Buttons,FireDAC.Stan.Intf, FireDAC.Stan.Option,
 FireDAC.Stan.Error, FireDAC.UI.Intf,  FireDAC.Stan.Def,
 FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
 FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DBXInterBase,
 FireDAC.Phys.Intf, FireDAC.Phys.PG,
 FireDAC.Phys.PGDef, FireDAC.Phys.FBDef, dxGDIPlusClasses,
  Vcl.OleCtrls, SHDocVw, IPPeerServer, Datasnap.DSCommonServer, Datasnap.DSHTTP,
  Datasnap.DSHTTPWebBroker, utrocarsenha;

type
  Tflogin = class(TForm)
    pnlFundo: TPanel;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    edtUsuario: TEdit;
    edtSenha: TEdit;
    lblSenha: TLabel;
    pnlEntrar: TPanel;
    pnlcancelar: TPanel;
    spnEntrar: TSpeedButton;
    spncancelar: TSpeedButton;
    lblVersao: TLabel;
    pnlTitulo: TPanel;
    pnlLogo: TPanel;
    spdfechar: TSpeedButton;
    lblUsuario: TLabel;
    pnlTrocarSenha: TPanel;
    spbTrocar: TSpeedButton;
    imglogo: TImage;
    lblTitulo: TLabel;
    cmbprojetos: TComboBox;
    lblprojeto: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEntrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure spdfecharClick(Sender: TObject);
    procedure spnEntrarClick(Sender: TObject);
    procedure spncancelarClick(Sender: TObject);
    procedure spbTrocarClick(Sender: TObject);
    procedure Image2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edtSenhaKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }

  public
    sqlConnection: TSQLConnection;
    titulo: string;
    fncLogin: TFuncoes;
    autenticado: boolean;
    sqlValidarLogin: TSQLDataSet;
    sqlValidarProjeto :TSQLDataSet;
    sql:string;

   ID_USUARIO: String;
   USUARIO   : String;
  end;

var
  flogin: Tflogin;

implementation

{$R *.dfm}

uses Vcl.DialogMessage;


procedure Tflogin.btnCancelarClick(Sender: TObject);
 begin
  Application.Terminate;
end;

procedure Tflogin.btnEntrarClick(Sender: TObject);
begin

 if (trim(edtUsuario.Text)= '') then
  begin
   Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Auto Numera��o',MB_OK+MB_ICONEXCLAMATION);
   edtUsuario.SetFocus;
   abort;
 end;

 if (trim(edtSenha.Text)='') then
  begin
   Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Auto Numera��o',MB_OK+MB_ICONEXCLAMATION);
   edtSenha.SetFocus;
   abort;
 end;

 sqlValidarLogin:=TSQLDataSet.Create(Application);
 sqlValidarLogin.SQLConnection  :=sqlConnection;
 sqlValidarLogin.CommandText:= 'select id_usuario, conectado,  usuario, count(*) validador from gousuarios where usuario=''' +
 edtUsuario.Text + ''' and senha=''' + edtSenha.Text + ''' group by id_usuario, conectado, usuario' ;
 sqlValidarLogin.Open;

 if sqlValidarLogin.FieldByName('validador').AsInteger = 0 then
  begin
   Application.MessageBox('Falha na autentica��o, usu�rio e ou senha inv�lido(a)!!','Auto Numera��o',MB_OK+MB_ICONEXCLAMATION);
   edtUsuario.SetFocus;
   abort;
 end;

 sql:='update gousuarios set computador=''' + fncLogin.recuperarNomeComputador + ''', computador_ip=''' +
 fncLogin.recuperarIP + ''', dt_ult_acesso=current_date, hs_ult_acesso=current_time, conectado='+QuotedStr('S')+' where id_usuario=' + sqlValidarLogin.FieldByName('id_usuario').AsString;
 sqlConnection.ExecuteDirect(sql);


 sqlValidarLogin.Free;
 autenticado:=true;
 ModalResult := mrok;
end;



procedure Tflogin.edtSenhaKeyPress(Sender: TObject; var Key: Char);
begin

 if Key =#13 then
 spnEntrarClick(sender);


end;

procedure Tflogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if autenticado = true then
  begin
  // flogin:=nil;
  // action:=cafree;
 end
 else abort;
end;

procedure Tflogin.FormCreate(Sender: TObject);
var
 region : hrgn;
begin
  region:= CreateRoundRectRgn(0, 0, width, height, 20, 20);
  setwindowrgn(handle, region, true);

end;

procedure Tflogin.FormKeyPress(Sender: TObject; var Key: Char);
 begin
  if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tflogin.FormShow(Sender: TObject);
begin

 lblVersao.Caption:= 'vers�o ' + fncLogin.recuperarVersaoExecutavel;
 autenticado:= false;

 sqlValidarProjeto:=TSQLDataSet.Create(Application);
 sqlValidarProjeto.SQLConnection  :=sqlConnection;
 sqlValidarProjeto.CommandText:= 'select descricao from goprojetos';
 sqlValidarProjeto.Open;
 cmbprojetos.Items.Clear;
 while not sqlValidarProjeto.Eof do
 begin
   cmbprojetos.Items.Add(sqlValidarProjeto.FieldByName('descricao').AsString);
   sqlValidarProjeto.Next;
 end;


end;

procedure Tflogin.Image2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tflogin.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
    const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tflogin.spbTrocarClick(Sender: TObject);
begin
  fTrocarSenha := TfTrocarSenha.Create(Application);
  fTrocarSenha.Show;
end;

procedure Tflogin.spdfecharClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure Tflogin.spncancelarClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure Tflogin.spnEntrarClick(Sender: TObject);
begin

 if (trim(edtUsuario.Text)= '') then
  begin
   Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Campo obrigat�rio',MB_OK+MB_ICONEXCLAMATION);
   edtUsuario.SetFocus;
   abort;
 end;

 if (trim(edtSenha.Text)='') then
  begin
   Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Campo obrigat�rio',MB_OK+MB_ICONEXCLAMATION);
   edtSenha.SetFocus;
   abort;
 end;

  if (trim(cmbprojetos.Text)='') then
  begin
   Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Campo obrigat�rio',MB_OK+MB_ICONEXCLAMATION);
   cmbprojetos.SetFocus;
   abort;
 end;

 TDialogMessage.ShowWaitMessage('Checando crendenciais...',
 procedure
 begin

   sqlValidarLogin:=TSQLDataSet.Create(Application);
   sqlValidarLogin.SQLConnection  :=sqlConnection;
   sqlValidarLogin.CommandText:= 'select id_usuario, conectado,  usuario, count(*) validador from gousuarios where  status ='+QuotedStr('Ativo')+' and usuario=''' +
   edtUsuario.Text + ''' and senha=''' + edtSenha.Text + ''' group by id_usuario, conectado, usuario' ;
   sqlValidarLogin.Open;

   if sqlValidarLogin.FieldByName('validador').AsInteger = 0 then
    begin
     Application.MessageBox('Falha na autentica��o, usu�rio e ou senha inv�lido(a)!!','Auto Numera��o',MB_OK+MB_ICONEXCLAMATION);
     edtUsuario.SetFocus;
     abort;
   end;

   sql:='update gousuarios set computador=''' + fncLogin.recuperarNomeComputador + ''', computador_ip=''' +
   fncLogin.recuperarIP + ''', dt_ult_acesso=current_date, hs_ult_acesso=current_time, conectado='+QuotedStr('S')+' where id_usuario=' + sqlValidarLogin.FieldByName('id_usuario').AsString;
   sqlConnection.ExecuteDirect(sql);

end);

 sqlValidarLogin.Free;
 autenticado:=true;
 ModalResult := mrok;
end;

end.
