unit uObtemApi;

interface

uses
  System.Classes, System.SysUtils;

type
  TObtemApi = class
  private
  public
    function obter(chave : String) : String;
  end;

implementation



{ TObtemApi }

function TObtemApi.obter(chave: String): String;
var
  arquivo : TStringList;
begin
  arquivo := TStringList.Create;
  arquivo.LoadFromFile('api.txt');
  result := arquivo.Values[chave];
  FreeAndNil(arquivo);
end;

end.
