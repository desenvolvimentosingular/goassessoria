unit uobservacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ExtCtrls, dxGDIPlusClasses,
  cxImage, Vcl.StdCtrls, Vcl.Buttons;

type
   Tfobservacao = class(TForm)
    pnlbase: TPanel;
    memobservacao: TMemo;
    spdOK: TButton;
    spnFechar: TButton;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    Label2: TLabel;
    lblusuario: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure spdOKClick(Sender: TObject);
    procedure spnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  class function CriarFormulario(componente: TComponent): Tfobservacao;
  procedure  ExibirMensagem(const psMensagem: string; piTempo: SmallInt; pAndamento :String);
  function observacao :String;
  end;

var
  fobservacao: Tfobservacao;
  vobservacao: string;

implementation

{$R *.dfm}

procedure Tfobservacao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{eliminando container de fun��es da memoria<<<}
 // fobservacao:=nil;
 //	Action:=cafree;
  {eliminando container de fun��es da memoria<<<}
end;
class function Tfobservacao.CriarFormulario(componente: TComponent): Tfobservacao;
begin
  { Criando o formul�rio }
  Result := fobservacao.Create(componente);
  { Exibindo o formul�rio }
  Result.Show;
  { Primeira mensagem }
  Result.ExibirMensagem('Inicializando!',0,'');

end;

procedure Tfobservacao.ExibirMensagem(const psMensagem: string;
  piTempo: SmallInt; pAndamento :String);
begin
  { Exibindo a mensagem do processo que esta em andamento }
//  lblVerificando.Caption    := psMensagem;
  Application.ProcessMessages;
  { Tempo de espera }
  sleep(piTempo);
end;

procedure Tfobservacao.FormDeactivate(Sender: TObject);
begin
//  If fprocesso.CanFocus then
//    fprocesso.SetFocus;

end;

procedure Tfobservacao.FormShow(Sender: TObject);
begin
 memobservacao.Lines.Clear;
 memobservacao.Lines.Add('Digite aqui...');
end;

procedure Tfobservacao.spdOKClick(Sender: TObject);
begin
  ModalResult := mrOk;
  vobservacao := memobservacao.Text;
end;

procedure Tfobservacao.spnFecharClick(Sender: TObject);
begin
  ModalResult  :=mrCancel;
end;

function Tfobservacao.observacao :String;
begin
  result := vobservacao;
  ModalResult := mrOk;
end;



end.
