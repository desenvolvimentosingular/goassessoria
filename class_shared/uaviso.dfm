object Faviso: TFaviso
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Aviso'
  ClientHeight = 107
  ClientWidth = 523
  Color = 13752796
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnMouseDown = FormMouseDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlbase: TPanel
    Left = 0
    Top = 0
    Width = 523
    Height = 107
    Align = alClient
    BevelKind = bkSoft
    BevelOuter = bvNone
    TabOrder = 0
    object cxm: TcxImage
      Left = 0
      Top = 40
      Enabled = False
      ParentColor = True
      Picture.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
        000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
        11744558745469746C65005461736B3B436865636B3B5DF4CC7F0000016F4944
        41545847CD94C14A03311445AB5FE08FA9F54F44C10F90F42B145CBB76A3EE04
        AD6D053742B54B37A22E0545AD4E7C37E485C9CCCB4C68D3D80B8776EE8439B7
        2DB4A3B5FE57C43227629913B1CC8958E6442C99F5FDD394AC96AFD9E10919A5
        944E01673879D524BD22D6A207A40AE45BBD730C00373C826E193C31936A4045
        CEE09B58A1DBC6559383140302F22F6293702E4FCCCC3BA04D0EE898C11333A1
        01857D6D8A24B7D74E0EE8A8C11333D28093D1A3EE1DDFEAE9CFAF6DEA09C907
        0F2F78EFE4808E1B3C31531D00F9867DA00A8C6892239096A1CAE08999F2808B
        BB272767AA23DAE436B30D78FF9CEA9DC36BEFE18047C4C8F13CEA671B808446
        EC1D0D623EF9FC0390D08832921C493200691A01F970529723C9062018B17DD0
        F7E45D75A6FBF7CFF6443D4907206F1FDF6E04E497E3B01C493E00C1885DFA39
        DAE4C84206204511F3E7BCC001B1598A0114D125964B312035550723963911CB
        9C88654EC4322762990FDDF903858E444ABDC908F80000000049454E44AE4260
        82}
      Style.BorderStyle = ebsNone
      TabOrder = 0
      Height = 36
      Width = 36
    end
    object flwpbase: TFlowPanel
      Left = 33
      Top = 38
      Width = 486
      Height = 40
      Align = alRight
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '  Todos os componentes est'#227'o ativos ok'
      Color = 13752796
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
    end
    object pnlcontroles: TPanel
      Left = 0
      Top = 78
      Width = 519
      Height = 25
      Align = alBottom
      BevelOuter = bvNone
      Color = 13752796
      ParentBackground = False
      TabOrder = 2
      object spdok: TButton
        Left = 459
        Top = 0
        Width = 60
        Height = 25
        Hint = 'Ok'
        Align = alRight
        Caption = 'OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = spdokClick
      end
    end
    object pnlLinhaTitulo: TPanel
      Left = 0
      Top = 33
      Width = 519
      Height = 5
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Color = 7330047
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
    end
    object pnlTitulo: TPanel
      Left = 0
      Top = 0
      Width = 519
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      Color = 11833701
      ParentBackground = False
      TabOrder = 4
      OnMouseDown = pnlTituloMouseDown
      DesignSize = (
        519
        33)
      object lblTitulo: TLabel
        Left = 6
        Top = 8
        Width = 207
        Height = 16
        Caption = 'Singular - Sistemas de Ind'#250'stria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblSistema: TLabel
        Left = 289
        Top = 10
        Width = 181
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Singular - Sistemas de Ind'#250'stria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 284
      end
      object pnlLogo: TPanel
        Left = 471
        Top = 0
        Width = 48
        Height = 33
        Align = alRight
        BevelOuter = bvNone
        Color = 11833701
        ParentBackground = False
        TabOrder = 0
        object spdfechar: TSpeedButton
          Left = 5
          Top = 3
          Width = 39
          Height = 24
          Hint = 'Assistente de impress'#227'o'
          Caption = 'X'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = spdfecharClick
        end
      end
    end
  end
end
