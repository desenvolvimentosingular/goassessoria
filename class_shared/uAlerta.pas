unit uAlerta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxImage, dxGDIPlusClasses,Winapi.ShellAPI;

type
  TFalerta = class(TForm)
    pnlbase: TPanel;
    pnlcontroles: TPanel;
    spdOk: TButton;
    pnlLinhaTitulo: TPanel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    pnlLogo: TPanel;
    spdfechar: TSpeedButton;
    lblSistema: TLabel;
    spdsalvar: TButton;
    spdlimite: TSpeedButton;
    mmtexto: TMemo;
    procedure spdfecharClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spdOkClick(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure spdsalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Falerta: TFalerta;

implementation

{$R *.dfm}

procedure TFalerta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	FAlerta:=nil;
	Action:=cafree;
end;

procedure TFalerta.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TFalerta.FormShow(Sender: TObject);
begin
 spdOk.SetFocus;
end;

procedure TFalerta.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TFalerta.spdOkClick(Sender: TObject);
begin
 ModalResult := mrOK;
end;

procedure TFalerta.spdsalvarClick(Sender: TObject);
  var
    FileExt: String;
    SaveDialog: TSaveDialog;
begin
 try
      try
        SaveDialog :=TSaveDialog.Create(nil);
      SaveDialog.Filter :=
          'Arquivo Texto (*.txt) |*.txt|P�gina Web (*.html)|*.html';
        SaveDialog.Title := 'Exportar Dados';
        SaveDialog.DefaultExt := 'txt';

        if SaveDialog.Execute then
        begin
          FileExt := LowerCase(ExtractFileExt(SaveDialog.FileName));
          ShellExecute(Falerta.Handle, 'open', pchar(SaveDialog.FileName),
            nil, nil, SW_SHOW);
        end;

        if FileExists(SaveDialog.FileName) then
          application.MessageBox('Processo de exporta��o conclu�do com sucesso...','Processo manual', MB_OK + 64);
      finally
        FreeAndNil(SaveDialog);
      end;

    except
      on e: exception do
      begin
        application.MessageBox(pchar('Processo de exporta��o n�o conclu�do.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
      end;
    end;

end;

procedure TFalerta.spdfecharClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

end.
