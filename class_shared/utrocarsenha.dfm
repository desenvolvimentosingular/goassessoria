object fTrocarSenha: TfTrocarSenha
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Alterar Senha'
  ClientHeight = 457
  ClientWidth = 448
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlLinhafootertop: TPanel
    Left = 0
    Top = 0
    Width = 448
    Height = 9
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Color = 14470586
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object pnlTitulo: TPanel
    Left = 0
    Top = 9
    Width = 448
    Height = 137
    Align = alTop
    BevelOuter = bvNone
    Color = 4464147
    ParentBackground = False
    TabOrder = 1
    OnMouseDown = pnlTituloMouseDown
    object Imagelogo: TImage
      Left = 111
      Top = 6
      Width = 225
      Height = 125
      Center = True
      Picture.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000097
        000000740806000000884FA0550000000467414D410000B18E7CFB5193000000
        206348524D0000870F00008C0F0000FD520000814000007D790000E98B00003C
        E5000019CC733C857700000A2F694343504943432050726F66696C65000048C7
        9D96775454D71687CFBD777AA1CD30D2197A932E3080F42E201D045118660618
        CA00C30C4D6C88A840441111014590A08001A3A148AC88622128A8600F481050
        62308AA8A86446D64A7C7979EFE5E5F7C7BDDFDA67EF73F7D97B9FB52E00244F
        1F2E2F059602209927E0077A38D3578547D0B1FD0006788001A6003059E9A9BE
        41EEC140242F37177ABAC809FC8BDE0C0148FCBE65E8E94FA783FF4FD2AC54BE
        0000C85FC4E66C4E3A4BC4F9224ECA14A48AED3322A6C6248A194689992F4A50
        C472628E5BE4A59F7D16D951CCEC641E5BC4E29C53D9C96C31F788787B869023
        62C447C405195C4EA6886F8B58334998CC15F15B716C3287990E008A24B60B38
        AC78119B8898C40F0E7411F1720070A4B82F38E60B1670B204E243B9A4A466F3
        B971F102BA2E4B8F6E6A6DCDA07B723293380281A13F9395C8E4B3E92E29C9A9
        4C5E36008B67FE2C19716DE9A2225B9A5A5B5A1A9A19997E51A8FFBAF83725EE
        ED22BD0AF8DC3388D6F787EDAFFC52EA0060CC8A6AB3EB0F5BCC7E003AB60220
        77FF0F9BE6210024457D6BBFF1C57968E279891708526D8C8D3333338DB81C96
        91B8A0BFEB7F3AFC0D7DF13D23F176BF9787EECA89650A93047471DD58294929
        423E3D3D95C9E2D00DFF3CC4FF38F0AFF3581AC889E5F0393C5144A868CAB8BC
        3851BB796CAE809BC2A37379FFA989FF30EC4F5A9C6B9128F59F0035CA0848DD
        A002E4E73E80A21001127950DCF5DFFBE6830F05E29B17A63AB138F79F05FDFB
        AE7089F891CE8DFB1CE712184C6709F9198B6BE26B09D08000240115C80315A0
        01748121300356C016380237B002F88160100ED602168807C9800F32412ED80C
        0A4011D805F6824A5003EA41236801274007380D2E80CBE03AB809EE80076004
        8C83E76006BC01F310046121324481E42155480B3280CC2006640FB9413E5020
        140E454371100F1242B9D016A8082A852AA15AA811FA163A055D80AE4203D03D
        68149A827E85DEC3084C82A9B032AC0D1BC30CD809F68683E135701C9C06E7C0
        F9F04EB802AE838FC1EDF005F83A7C071E819FC3B3084088080D51430C1106E2
        82F82111482CC24736208548395287B4205D482F720B1941A69177280C8A82A2
        A30C51B6284F54088A854A436D4015A32A514751EDA81ED42DD4286A06F5094D
        462BA10DD036682FF42A741C3A135D802E4737A0DBD097D077D0E3E837180C86
        86D1C158613C31E19804CC3A4C31E600A615731E338019C3CC62B15879AC01D6
        0EEB87656205D802EC7EEC31EC39EC20761CFB1647C4A9E2CC70EEB8081C0F97
        872BC735E1CEE2067113B879BC145E0B6F83F7C3B3F1D9F8127C3DBE0B7F033F
        8E9F274813740876846042026133A182D042B844784878452412D589D6C40022
        97B88958413C4EBC421C25BE23C990F4492EA4489290B4937484749E748FF48A
        4C266B931DC91164017927B9917C91FC98FC5682226124E125C196D8285125D1
        2E3128F142122FA925E924B9563247B25CF2A4E40DC96929BC94B6948B14536A
        835495D429A961A959698AB4A9B49F74B274B17493F455E94919AC8CB68C9B0C
        5B265FE6B0CC4599310A42D1A0B85058942D947ACA25CA381543D5A17A5113A8
        45D46FA8FDD4195919D965B2A1B259B255B267644768084D9BE6454BA295D04E
        D08668EF97282F715AC259B26349CB92C12573728A728E721CB942B956B93B72
        EFE5E9F26EF289F2BBE53BE41F29A014F415021432150E2A5C529856A42ADA2A
        B2140B154F28DE578295F4950295D6291D56EA539A555651F6504E55DEAF7C51
        795A85A6E2A892A052A67256654A95A26AAFCA552D533DA7FA8C2E4B77A227D1
        2BE83DF4193525354F35A15AAD5ABFDABCBA8E7A887A9E7AABFA230D82064323
        56A34CA35B63465355D3573357B359F3BE165E8BA115AFB54FAB576B4E5B473B
        4C7B9B7687F6A48E9C8E974E8E4EB3CE435DB2AE836E9A6E9DEE6D3D8C1E432F
        51EF80DE4D7D58DF423F5EBF4AFF86016C6069C035386030B014BDD47A296F69
        DDD2614392A193618661B3E1A811CDC8C728CFA8C3E885B1A67184F16EE35EE3
        4F2616264926F5260F4C654C5798E6997699FE6AA66FC632AB32BB6D4E367737
        DF68DE69FE7299C132CEB283CBEE5A502C7C2DB659745B7CB4B4B2E45BB6584E
        59695A455B555B0D33A80C7F4631E38A35DADAD97AA3F569EB77369636029B13
        36BFD81ADA26DA36D94E2ED759CE595EBF7CCC4EDD8E69576B37624FB78FB63F
        643FE2A0E6C074A87378E2A8E1C8766C709C70D2734A703AE6F4C2D9C499EFDC
        E63CE762E3B2DEE5BC2BE2EAE15AE8DAEF26E316E256E9F6D85DDD3DCEBDD97D
        C6C3C2639DC7794FB4A7B7E76ECF612F652F9657A3D7CC0AAB15EB57F47893BC
        83BC2BBD9FF8E8FBF07DBA7C61DF15BE7B7C1FAED45AC95BD9E107FCBCFCF6F8
        3DF2D7F14FF3FF3E0013E01F5015F034D0343037B03788121415D414F426D839
        B824F841886E8830A43B54323432B431742ECC35AC346C6495F1AAF5ABAE872B
        8473C33B23B011A1110D11B3ABDD56EF5D3D1E6911591039B446674DD69AAB6B
        15D626AD3D132519C58C3A198D8E0E8B6E8AFEC0F463D6316763BC62AA636658
        2EAC7DACE76C4776197B8A63C729E54CC4DAC596C64EC6D9C5ED899B8A77882F
        8F9FE6BA702BB92F133C136A12E612FD128F242E248525B526E392A3934FF164
        7889BC9E149594AC94815483D482D491349BB4BD69337C6F7E433A94BE26BD53
        4015FD4CF50975855B85A319F61955196F3343334F664967F1B2FAB2F5B37764
        4FE4B8E77CBD0EB58EB5AE3B572D7773EEE87AA7F5B51BA00D311BBA376A6CCC
        DF38BEC963D3D1CD84CD899B7FC833C92BCD7BBD256C4B57BE72FEA6FCB1AD1E
        5B9B0B240AF805C3DB6CB7D56C476DE76EEFDF61BE63FF8E4F85ECC26B452645
        E5451F8A59C5D7BE32FDAAE2AB859DB13BFB4B2C4B0EEEC2ECE2ED1ADAEDB0FB
        68A974694EE9D81EDF3DED65F4B2C2B2D77BA3F65E2D5F565EB38FB04FB86FA4
        C2A7A273BFE6FE5DFB3F54C657DEA972AE6AAD56AADE513D77807D60F0A0E3C1
        961AE59AA29AF787B887EED67AD4B6D769D7951FC61CCE38FCB43EB4BEF76BC6
        D78D0D0A0D450D1F8FF08E8C1C0D3CDAD368D5D8D8A4D454D20C370B9BA78E45
        1EBBF98DEB379D2D862DB5ADB4D6A2E3E0B8F0F8B36FA3BF1D3AE17DA2FB24E3
        64CB775ADF55B751DA0ADBA1F6ECF6998EF88E91CEF0CE81532B4E7577D976B5
        7D6FF4FD91D36AA7ABCEC89E29394B389B7F76E15CCEB9D9F3A9E7A72FC45D18
        EB8EEA7E7071D5C5DB3D013DFD97BC2F5DB9EC7EF962AF53EFB92B76574E5FB5
        B97AEA1AE35AC775CBEBED7D167D6D3F58FCD0D66FD9DF7EC3EA46E74DEB9B5D
        03CB07CE0E3A0C5EB8E57AEBF26DAFDBD7EFACBC333014327477387278E42EFB
        EEE4BDA47B2FEF67DC9F7FB0E921FA61E123A947E58F951ED7FDA8F763EB88E5
        C89951D7D1BE27414F1E8CB1C69EFF94FED387F1FCA7E4A7E513AA138D936693
        A7A7DCA76E3E5BFD6CFC79EAF3F9E9829FA57FAE7EA1FBE2BB5F1C7FE99B5935
        33FE92FF72E1D7E257F2AF8EBC5EF6BA7BD67FF6F19BE437F373856FE5DF1E7D
        C778D7FB3EECFDC47CE607EC878A8F7A1FBB3E797F7AB890BCB0F01BF784F3FB
        3704291E000000097048597300002E2200002E2201AAE2DD9200001E88494441
        54785EED5D0BBC1F45754EDFF6DDDAB7B51615B5950AD652ADDA56AB057C15B4
        B6A2110C05241481502B148A4A94424A8184507929698A3C0C52BC3C6D0121A1
        96872909C134160C243744421273C93B378F7B937EDFDE33DBD9D933BBB3BBB3
        FBFFDF24FFDF6F7E37D99D3973CE996F5EE79C999DB067CF9E09BD4A53A74EFD
        C16DDBB6BD74D7AE5D6F1E191979DFE8E8E8B14827EDDCB9F30DBDE2A9DFEBBD
        E5965B7E687878F865A2B323A1AF8F516748A7209D88F451EA12EFDFB875EBD6
        5FFB01FC7A2553A7C0DABE7DFBCB21FC09BB77EFFE12D242A46D103CF7439EB3
        8B1482F767219DBC63C78ED7F64A715DD50B9DBD02B27E1CBA9A85F4B84F679A
        1EF90CF937233D8A3413748EDEB265CB2F77C57BEBE0220020D805484FFA14E0
        3E2F031768CD3565F0EFE54833D053FFA02BA5B55D0F46EE8321D345484F87EA
        2C341F68F2F72DE8F84CCC1A2F69539656C0C5A11BCC7F08423C142AB49DAF0A
        B8EC72A86F11CA4E1E1C1C7C519B4A6B83F682050B7E04BC4F64C3D7D1599D32
        A86B17D200A6D1B7B721535470717E17053D55475853A62EB8ACD1EC7BA071EA
        92254B7EB40DA5C5A4291D71521BA354953640FD0F03647F1253B668E0C2B4F4
        263038BF8A40BEBC4DC16581EC2928EC88980A8B490B3AFB43AE3D63E82C160D
        8E645C1BC790B331B856AE5CF9E360E832A4D15802C6029705B2391B376EFCF9
        180A8B4163F5EAD53F097D5D195367B1744F3AE06B0BDA604AD39D66237061E1
        791018591C5330D28A0D2E51D8CAB6D6165500079DBDBECAE626B66EABD0039F
        F76077F92B55E4B3F3D606171AEA83A87C73156643F3B6012E01D82EAEC5EA2A
        AB6939D47D0C74A69A5F4275D3753EF0BB12D3F7EFD791BD16B8A0A44FB539A4
        B7052E6B9ABC920BE93A0AAB5B06327D963680AEC111A33EB0BD1583C9515565
        AF0C2E54F48F31182EA2D136B86414BB89DBFFAA0AAB9A9FEB16E8ECF2B675D6
        367DC830827639AE8AFC95C0850A2E695B88B6D65C1ADF90E7B6360126C0BAAA
        0B9D755107672B7A584201160C2E109EDA85005D82CB8C60F471862AAC4A3EE8
        ECE2AE74D6553D02B089217A0802177D5B5D31DF35B80460578428AB4A1E6EE5
        BBD459977501603BB1063BAC4C1FA5E0C24EE18F49AC4BE6BB5873B9F2D0115E
        A6ACD0F750FCBBB846E952675DD705F936C06FFCEA229D14828B8E4D1059DD35
        E3BD0017E4DC818EF4D65000F9F2211CE637416BA86B9DF5A23EC8B98406619F
        2EBCE0E23A0485EFEB05D3BD00974C8FCBD6AD5BF7D3750146F30674F6CD5EE8
        AC577542DE2F5606974C133DE1B957E01280CDAA0B2EF0FDB73D51588F2BC532
        E0DD9ACED4918B118C9C53BBE619756E445A81463AA9A88191676E9BBC719D59
        1560321D6E6D932F8D36DB893A43FA2ED24AA42D3DE061C5AA55AB7EC2D5990A
        2E3038BB0B0651CF62A4F381FC776CDEBCF917431BB46D7081FEC2AA167C94B9
        B9239D311A752A3AC0DBE08C7FB1AB33DAD6B856864EDF837C97222DEF88AFCF
        97824B9CD1D1221C5CC1B88B42BA910EDC5030B9F930B29D051AC36D2AAD8A35
        9ABE37F0D39A6B07A419D4771D74F63B557546B0D1618FF2F7B6A92F8E98AE93
        3B3772B5D90341FB3134C4A15515A4E59769E8EEB614065E9F0C1DBD90F7CE16
        F97814A03A2486CEB83602AFCB5AE4951E9C145319702148EC00F692D895B357
        73886EC3D58211E613A0BD2336CFA407DA1F2A6B54D87A7EAB8D514B74764128
        C0CBF834EFD7AF5FFFB3A0FD6F6DE80B743792BEA92B032EBC9C16BB524E8362
        E12F35D8862AC8CD871EF94ED4133DFC07341F28E3097966B6A03386061D5B56
        77DDF7E2F36CC53505BE4FCB814B6C34CFC55414144F47E731759550A51CCFF1
        71DE8FCCFF6E8CE6AFF4F1B174E9D21F439D510DA6EC8CF8FD4515D9EBE6455D
        E7C7D4176981E6E3397049707ED4BA00AC33EA0A5EA71C64381CC24575554186
        CFF878417DEF8DAAB0B1A9B8D00C53472F4565A02FFA55A3FEB054780DEB4CA7
        AAD8C33BE87D25B62242E8A171CE8CA929C8F1A8AF5EBCBB36725DA4D7DAF241
        A3CD7530E488EA553046701B5CC18756CB140A6657D90BBB2E1526EB09AE95A2
        FC38B5C306F74B9A0C78F76C944AC6A693C1223F5D9B3A948D5C3403B059AB26
        E08245FE5763298974DA5C8C8628596C75D176BD98FEDEEFD62B6BB1686AE399
        8410D9DACA83363B37963000D730CF8C26E092F8E828B441F8A9D8DBE73A0A05
        1F374411686C54B9D0E5018DC180B9283FD05FD4F418571D1DD965E8B0071FEB
        A2080422BC082501972C5AA3D005AD9E9DAEB19525F6A78BA1B0C649B3D6836E
        B4B304A07F7C5370C4281F59A613137081E84D3190053ABBBABC45258642EBD2
        80ACB747D2D970AFD6A7AEECE25E8A211647FBE9065C0FC7A00882BC44A3D3DD
        4EAFEA83AC4F44D2D9DC5EC9A0D50BB9564692EB5603AE1591084EEF2745B5C9
        4BACF509E8FC439B7C56A50D7EE644C2C2B70CB8A258B6B176985C5598F1985F
        A274A3C4C8F77A67EDEA1FE08A72CA0B74964E909D5D0CB0EE610CD178044B55
        9E25143A8ACE189755B5FE36F3CBE6A2B16C00D79A09BCA5A63125210045FD51
        9B82F70BED4D9B36FD42449DFD5EBFC8453E2412A4B17800D7FA09129EDA9818
        09C4383D632B9A864A30F97C1F243AF4D38D8A44CD46D1197668BFDB4FE012A7
        7963D9D0661B26CC9B37EF871B531202741CC75494384063B1579B0E5D40B65C
        623AA84DCF2EC80BE062EAAC292D8C5C936208069DAD350BFAED31080A63D14C
        11FD0A2E7648282F4A583374F6974D0111B3BC389D1BC301EA596EC0B5AA3135
        100041C607EDF5E0A28C9075630C9DA131CF89A9B3A6B420178FD635FE81CE02
        03AE058DA98D818B8700F6157045892281CE6E8BA9B3A6B4C0CFFF44C2C25D06
        5C0391086E91DD671480F5EBB428235794D33468CCA136CE16D401999C578D32
        DD43AE2B0CB8623A61A3AD21FA1C5CD12E749388D6281DB20EA84C194CD17F13
        6390210DD03ADD444530CE3DCA0F88BDBF898076D97E06978423C7D2D91DB174
        56978E781DFE378A4020C2B0F9045C72154E2CBAB477BDB9AE90E3055C629F8A
        A233EE3C9B1C128EA1EB58C6532A04E28CD08B91804B4283D744D1D418F1F931
        6EEBEBE7914BCC111B22EAECC15E050CCABDF8831165E106317340234A4C9761
        103DE1534D7B543F838BB2C94E2F569B709D724A539DD529CFC5773421C60617
        AEE1FF1F5C10ECC3912BD8D9D4D7D8EFE08AE5E4357A47A30CD7BDF3BD0EA858
        46CE95C66C7A2E8BDE920197C450473B012273EF509DCB338CA2FA1D5C746003
        1051AF12A0DBC49CFBAB0B98D07272BD666CFE9F354BA2CCF617825D1F15C263
        43E49ABACED97E07974C8D5F6B4167CF35E99421E002B0FE0C6D13FD6B1EA099
        063F66C025D3586C5D11601BB5E359654AE07179EEA28A1268B77ECFBBEBB8B6
        F996AFA2B5A1B321DF8D7D657A2B7A2F9F2D9C0299A21DBDB3A6F55D3C0369EA
        CF19EE50E97F47D7D4D808C6DF551B366CF8B926CA3165791714E8DDD206AF2E
        CD2270C94E3B8ACB44A9973ABBACC93DADB6AE71EDD46F801E6D6AADFC409B21
        D229A672E0426F39B2959A85281858CD3B24EA9E2EE6A7ED50FED3A013CD0C50
        266F11B8A84CF0F391321A4DDEA3FEE4E3A4755D6B3C31CEE90A29FA4D40D6A8
        35CACF2717824B7A22EF4768F5477020CDA2EBA3EC681595C32036E4BF0129EA
        A62344C8327089757B5108AD2679C0C70B48D770215E369AF1881FC37990FF66
        A4E86B2B6594BDD19D91547F163F462E0A6DA28BE0B2A88B5759F243E80F20DD
        8A74A3FC9D87BFD1EE630866C8C958062E2AB58D5B828AF8159D3D83BFDF105D
        DD84BF03480F223D8714C5011DA23376784EB941E0622614B83A84F0BE902704
        5CA2B3E8BBEDF1A05F8C90BC223D3750793DF172BD6194F38CE3414125A34426
        CC5953249F31B61E407C7EBCCB5B857FC8FB90EF6E90C2300FF9EE4F94F37955
        18EEB7BCA123974C8F4774B9A4E8A5AE20E706981E5EE1EB6CA5314418F2A25E
        A6D64B65D4ADBB0AB8647A8C72B0B42EBF5D94A34EC4B2E0C55029B864F7F8E5
        2E18EED73AAA824B768FADDC98DC2F3A92FBBC0AF1530A2EF6445EE405053308
        709FFC55051775469B14D7237BA3C220D735BEA9D07E1E042E1658BB76ED4F81
        6894DB70C69BC2EB808B3AA3370265A31C7EE9179DD14C141AAB170C2E2A6B68
        68E86740FC3FFB45D0AEF8A80B2EEA8C1E0594E7D552E3FE0739AE9743D441B8
        09CA640F7512B5D89A7FAA1F5BA009B8AC4E794F3FCA16CA1374303374C43278
        A90C2E16940F22CC08656CBCE76B0A2EEA4CC2A25B8FE088AD6BC8CEAF799C1E
        B2C672F3D4029721C2284654DE9A3334B6A2EAD28B012E4B6727D25D5297972E
        CBD1202C6EAD5A38A955C8462802FA5E05261EE952E8AEEB8A092EEA0E3A7B6D
        BF2FF4C1DF80EFFEFDD051AC31B85811E7628C62A781A1F55D377C517DE0274A
        086F6C709969928758403BCA9D13B1F40E7E18DE53FAB5B61080450197A9487C
        6B33C160945B73EA2A0CF56F429A8A21FDCFEBD2B0CBB5012EA3337E6002F4AF
        8CD511EACA4B90536775E3EC34B0450597A9009FC17D2918E5FDEF9D05F451A9
        A88F81889F369FE705B80EABABECAEC06574261F2765C7EC740D4B9D1154725B
        62543C4425E6A297BD008D7D1C98BF1729EAD7C44CE373718C742B63F4E949B0
        79184FE0327C3308103AE3A2FFBE1675B68D6B2A0660BA3A0B99EE42F3B40A2E
        9B095AAB259AF42A08C68F80D73A20C02917E911A44B79C16F51E8EF780497AD
        338EC05CFF40D66B909E88A0B3E9D459CCA9AF08689D81CB65424EF6BC8E238E
        6C06A642799C16A8C8594804E10C0ED9787F323DF0FCE44A95EB86C63BB83C3A
        3B986B49D1D9E7A09FCB3D3A3B85C7C7A8B32A56F5D05129245FCFC015C25CD3
        3C7B1BB89AEAA3EBF2FBC115B0E2C7C8101489DA75E3F57B7D7B35B8B06BFD75
        2E8E23A413FABD21FB91BFBD1A5CFDA8F07D89A7FDE08A7841F0BE049C10597D
        E7160FE52E0DE90EA485484F222DA26D841E72C6288510E7CE8E3B1B94BB02E9
        1EA4C7644BFD30FEDE085A938B0EC4D2CC403B59D5843AFF94C7FDAB96B3F38B
        4B6BA24263927D499BB1E5B9F9E433C5A97E79EF83C68FA64BA97B928FFFB293
        D738F9FCBA40D92782AF77965DB140734820BDB4ADB8BBCF808B2739D0E873CB
        D6B8C8B3AE2C389F1F4C42BE670268BD005AEAF79D79BB705979ED3DEABD5B3E
        D854A77852464264D43B20E4F32C89EE788058AB847E43BB03B2836AF9B48695
        2B2CBDBC435FEF28EADC12DF1E2C3B78DB81749DEF43AC7857F93B052833760F
        3D93002BF8EA4A1AF47C97BB41781EAF0A761A232F8A8C1CE12A8C0BF2600D59
        1941EFCEA627A0E5608A7AC8028D7F88E1150D79AC07E0BCAD2FD1ADD0CA1DA9
        079FFC70666EF600CD4F16C9CD59A5045C9FA9A9B7651CF15DDAA86F75557A28
        333F150CFF9957834072F7A59D24147A6D0D5ACB5D57841C11AF4A8A3EC63BA4
        77572ECB02043B65C25FDE3595FB718A333223CFE73CE0FABAC92323422E1BCA
        FEBB06123CBFAB045C734BC0755E2DC1C764E787E7336D8A6775DAF31133B4BF
        D1A320FAED2E464F3A0F7F5527342CC0BF6D33C3759487D6665ADA912681963A
        42D23D64D3A233B78E9240FF36AEBBEA94157071D4F55EE948D386052EF52E59
        F0F01D9347AEA2D4C0F54F6E43CA74BCA9045CDBE8E1F001CC07F8107D70C671
        D7C15C068594B5F3A0CC7F25E02278B4C278FE774600FC7B8A270FA7057B041C
        F0802BB98455EA3BDB93E73A9B16A6EA97FB8402F3D391A6698953151B14EFBE
        E9247578479EEF39F9BE413E40E3508FCC9FB5C0A5DE67067ADBCCC25FD69439
        52E0F3632E40E49AF5D2B62CFADA19EAE6379834307F8D213E740971D0F05522
        4B0ABB4D5FF0D0E38531BE36989C104086AF6A8551C97B8DF0DC5578149DB984
        02B4D4FB25E843B4C0C5AF6C68C23FE580EB953E05945D21E4369AC839DB53EF
        255A7EB92736775B0C644CCFEDF94674D6231B12765E750D85B5DB1BDC7AB5C5
        38EAC81D4F43BEBFD77816392FF081C12E03BA4B3C6DCACB976D70A9B316AF7B
        F0F1C0E7065CEAC91408900287D7117207E426D92D25740607075F0486E92AC9
        FD641448F2F9A62C941DB11DD30CA1F6814B3E425AC94E07FA1C193550E7A627
        A33494C97DAD1ECFEEE47BDF5ACA54606E3546FECBDC4A29AB6652C0F3DCE163
        E8FC2CA5FC7F14806B9A474EDEC26383866BBEDC0FF5653EB1039ED469DAC8E7
        E3C3806BC0C3CC302A3A23341241EEC354F18077071A26C0D49B7CA0C13AEB65
        265FD185BB047251AFD1DE4149EA1547789E4ED96E39BCBB4F69D885CC27CAF5
        89C27BE53FCA7CA091BB9417CF32A334F349E71CB609B2B36A7A6583FBA21DF0
        EE224F7BF25A061B5CDFD6F2B95FFC05BD2D9E7C6C476F0737E0527745862088
        2F83A24E2A0B2C0353FC5EB3FA33530499A191CF97CFFE4C896C16D4AC1CFD90
        DEAE259F911172F0F6BBDC0FCF2FF42909EFFE59011737249CEE263940C8D8C5
        648A23B872D31A9EDDEAD6A9ED70918FB642D2C86D827C77D623AFBA9E92913B
        6973DEB0ADCD327836CCD3F50E08D59B0921DF44DF65C8B41A988A5E0FA2A537
        D121CB0A10FC6BDF4EA5C8B6641B0B8BA63BDB7606C60FF20E0B052F5CEBB851
        14F8FF8A075CDE8F9042DE4F28E01A95FB33329D12F4A73B60BB5680F17D8506
        4D18EE963FD7C941734068CC7569D8CB16070C977AE45C8832BC4F96B172EAC1
        103C9FA1F055F94C04B0F02E7B88FCD7D0860403CB1988A6F4BCF7F968D8BD41
        A63E35AB6C1C0CE8BD235C11AFF675D58ED26FF6283DD7D0A69CCF5E2631EF19
        7A54A803AEFB25EA3357ADBBAE1100E5EEE280AE3F2FEFAE5400AA7EED4C0012
        DA9C493E0E2EF85DAD2D81F0BC72883A7471580A2EB995E5EE508EC8097B81DD
        7820F8015F797B7757E4D671C07548283F763EED7E4E6920F56A7131C5A86B07
        78095EA2F140930154C0B597699C116E32EC86C0BF97F9A676D73E283BD35CE8
        B70121FE9EAA80EB05ED883DEA9D59456FC8BF9D6627DF717DBCAF7C0120DB31
        A35071989E0262CF853267BB6D8AC0651BE68A76595C4319C096F9D87C3C0A20
        7260815C5CE768A308DD252AB8C475939B4268F005BD741745200980537F2A9E
        ED42BEDC68CEC67417E362F6C9F1469B14E9FA4650DB15656400FDDC3A31A43D
        51EE1E6DA3C48124A4BC9D87EDA82A94EB09209947F51F2B238A3CA99B43A64A
        B5887D74C9371AB0A06DDA103B508EDED8A0397A862FF90E20A0DC80075CE7FA
        C02580C9DD52035ADC04A43F368CE4CDEC2EDD7C2C80678BDCFAF02CB74EC2B3
        6DE6BE51DF680F1D9CA6D052BF42067AF39118ED720952CEC422BCA59F57215D
        3104AB6D0A1A8C7621BD5CE2B2A1D44E4404A2306D2ABE0AE81A30F62B7AEBD5
        9FED102D5AD0CB223EA1E7B390839FC43D5335A1DCED1E70D163E0A5877239FB
        189E65D64754B480EB8B0EE8B47554C6DE24E51E7779034DDED5CFC537D3D5EC
        544A1E4EF5EEC640BDF04446B424AF6FBA469E35764891805B6D535E4B50A4B7
        A4220ED16E726FE8A5703EE098ADBF7C0C49CDC683B28611DF88C482B6C902E0
        F2F93C690BAA032E1A3F733F31527AE9E1FD394AA36616B91C45C9939B177ACB
        2D86B9BEB1F99793EAAAF1D9A773F3DC058300951E84DC0F79B916B33771EBB5
        7C128D9262C3C783BB6E74DBC48483683D8206B69491A2CFEE9A6948D6552A2F
        B679803B098FF03B6D50FB62A5A024DE1253075CEA86855E87227A456B492387
        ACAD08AEA3CB0061BBD5582FD76F65658ADE9B75999101FAF99247BF331C700D
        69F96C83B7D836D5EADD7A73E012A4E77603EC1176E602CF3E9D9A766F50ED27
        F65A0A0DF0718FF03442A6B47C1670F0C623EF75C0E57377D0F7E7A55764CC35
        72986F24FA465B5B5E89F6B07596333354011BF4C948149BDE2C8F7EB9AEB3F3
        F9C07580C9270B7C1FB85E53A4B7A4223496EA6CB61D93C8E37329A40B7AA1A5
        5ED1686FF7416BC0237CE62257F1FCE7B2A23C015C075C3E1F6A32A5F992184C
        BD27C4C1CF88F15E4894AA171BE4DDFD96359EA951AAA10043F9CC7777F0FFD9
        1EFDD2725F0A2EDB94233E5C9515D4F31D249E9ED7D258C80D5EFA6292D6E2DD
        7424C6D2ABDB5180E6789B616D7D42CE507C17D29791BC8170E2D0B6472E7ADD
        3570ADAF09AE9C9F90C4C173E9CD794500C0BBC4456312FEEF8DC7C23B2EF0D3
        BCBE685BE4DBE6BA56A09FA33CFAC844B4A26C90831EF9D4501A7BDD2BC6EF50
        9CA7F9407BBDD995BDB572E931C02C75ED22F259973A31D70FB83D5A6C5E1AB8
        389CD719B9721107022EBA780AE941D6DB7C3AC2BB4C8402FEFF44415EEE26D3
        BA006C5F98F47C9727BADDD84935DAF6972C9027C8418F7CEAB468EFECE923AC
        898D21BBB765EC366504C998EFF3C23421E07D70682C470506B1B9CAF4190E85
        761D70CDD5E4A2BF34005C8C9C507FE0E70BCEC835E0CBEB8E92283BDB331A25
        7E493721FF773D321C67F2224F90831EF906355A76BB8A4FB80C0EDA00B036C3
        3C046708B25AA1298DF73C2972831D1AA32981C33DF2F14211EFB7FEF06E18E9
        0ADFD126992635C619515A075C0F7A1A26B320D66843376C3CF58777531C7065
        1CD87621E93076A756D7BBBEA91AFA52CD2978FE2F16B87C0EFA8C8114657CFA
        48812A779DD501D7EA5C03D105C49D1D843B19959F8F7429D285F8FF27A19877
        578D00657E943B9C9664D0A125F77C2A8EDBF1B28F787231C96DB19218FE5C19
        5C04BC464F86FE427A05BC1CE8EA8467113D7C1F68872D7119E0CBE786BD1879
        E93AD3CAD8B629CE025A1E73299EA1C53B4FCBF2D134E4E3B1E4F901951BA84E
        A3EE2F53BD23EC0D3AF3824B9CD82760A49929A124B58088B2478206C36E6B95
        0F29270BDDAFA29E4791E82A991DFB4E2A9A18389ADB3BA910DE7C7938824337
        BCB335BA5E68F6F105126AF58197CB51E6AFEAF0C2F83BEA5C2BEB158C2E0A14
        E2E781198EBB47B6C19515C1291589EB8ACA65AB94A1951BE93DE0FB5CA40FBB
        3BCF2AB4B4BCC6CF5974EAA64A1DD0093FBBEC0DAFAE42CBCE6B1CCDAE61D547
        4FBEB0C6D34FB3EBD4C9CD10CAD2C3936B5F6F83A3004782C572DFC3515AAC0F
        E7636EA591EF126E06B43068BCBB08290947B113F29F6862B56901C7FFD5587D
        465070BD071A3C3F79B4F655522A14C0FAA0F0F11176048E989AC078CED3E0E4
        774AE89D17A463E2FEE5E8572A0B2DF3E49D96798205FF3E41D3154757BC3B89
        7270518FBFCF22F1944E4A8BEB2CD232EE179A62C45491D11DEB94BA4E7743BA
        8D2B09EFE738A1E5079116D2F1C65565EA2610B5933CD40FDEF13AF3691C09C9
        1BEA7EB5D38EA7E23DFDA7E1E01205D0F079B7B6889738A7DBF19E3BBE8790B6
        203DE84632B2219196BA95E3D9F346B904264747D7662671DEEB918F1F04E719
        44DE879A1B824907893FF2314861911801E1029A877BF963E8C91AA4159A09C4
        3372BD853C8A7BC7B6531DC3E7A045837312A2C4DEACC87B17DE73A7FD00D2F7
        91A8DB24CAD42473C21CBA7F3F9FE1FD1790323E5E1969A88FC548432EF8F02C
        7125E1EFD37684093BB3D4FF0464789B5D2F9E2F43CA8CA2F28D275AE0199971
        3F52E2D6039DCCC9207652BC5303090AA72AAE09509037FF2E74CD05C64C60AC
        EAE67086BB8E4059DE6B4AF746A62E698C44B9EC4D645C01E6D7916F81E99DEC
        D12E0025FA93A04BD6751251CBAFD6F3C44D5A2777466C50D49584D730BE4C1A
        D97B38C32E2FB1FD04170FA1D8E04A3A06FDA0D2E15680AEEBC64A0EAE70E465
        5982087918FD991EAEE57373CA073A4C42C89127B99CD8AE4FC0C58E7187CB0B
        F319770D68670EDCCA34B9C78E4E3174418B3C67465176783CDB2D912E74AE27
        E756DDF6955985A783C2472E93996B0C01985BF964566E002142EFE0D0E9F40A
        5E08CBC3972EB83872197091566E68951E354363DC3CB31A243DC08B72772265
        E29C0CF8657A4B78411E6E0072B1555A7DC65B609F4EB23B86013D3B2212C393
        52798DDBC66E58E459095D658214AD91CB80EB5AE4E3074133BAE31485E7DF26
        4069D2B1DFCB2C931E6B33EF5017238C47B5B5289E73CD951945919F0739D2A0
        04634C75D7DEC87726F2A9BE5E75E492FB0AB8E6BA80AE00FC7D1A899EF634BF
        89C9227265ADC0B99957DF647A361E315A3133B44BC32EC0F37BE5839737E1DF
        DAD43907CF07B9C6608F445D27CB11FF940FE195D3CC00154BF0E0DF9B91E6D8
        FCCAC748F99588D9B46BD1664740731D54045EF3CEF45C3916678F5C49848759
        6781269DB8E90D373222F16AAA51A46912E53B9165CC286AEA90519753FAC532
        B2F27A70FA7DDDE9FD68F98E23AF4CE769F9F4BD097B02EDCCA969FC9F764675
        6D84E7CFE3FD794E8748C2B339028AE39EE0E3C8C5E7B6FC67DB20B4DF152DE8
        79C43DF9B1B1EC1E6FF58673F12E09D721AEC04826084E40C408CAC75D0571E1
        29C226B7CAA06C7280D44E62E55F6CF1C1A924BD04C4E4E51A0534124F00FE72
        EDC7F54F26528079255F7AC01379AED736082E1F52F670D277A32F0978D0A153
        DF8C861C512E776920DF390498F0C8351381CD8FD3BB237A7A141F7936B89D55
        3EE7C2CEC49D3C488C12A8290D635157D6466720BFBA36A2BE3852D974DC6F9B
        23CF72F2CE4E69E7E3E88B773C3A576D5AE4E804621FD0EE6C32C4E806E2BACB
        B730166BF1015AE5040FCB6AEB00935F76A4EC75CFD85668971E2DD7A4C5BF8C
        97F2D9A33822309F845A17AE39ED3AB8C065709CBB23E6086207CD7164F5E98B
        CE658E801C45B9EB722DE6A63EEE9E99CFE716E3E8E49381FA223F8AD7E0C5EE
        4ECFAAEF55BECFB30003077303C0998374DDF309B4FFF97419AC5C0D1C5D3CE3
        8E07C0E24EEBD932775117FCECAF23DC5ED9F7E06263B20787DE57B1BFF1C31B
        BF6D5DFD1F092B83BCE818576F0000000049454E44AE426082}
      OnMouseDown = ImagelogoMouseDown
    end
    object pnlLogo: TPanel
      Left = 400
      Top = 0
      Width = 48
      Height = 137
      Align = alRight
      BevelOuter = bvNone
      Color = 4464147
      ParentBackground = False
      TabOrder = 0
      object spdfechar: TSpeedButton
        Left = 8
        Top = 6
        Width = 39
        Height = 33
        Hint = 'Fechar'
        Caption = 'X'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = spdfecharClick
      end
    end
  end
  object pnlFundo: TPanel
    Left = 0
    Top = 146
    Width = 448
    Height = 311
    Align = alClient
    Alignment = taLeftJustify
    BevelKind = bkSoft
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    object lblSenha: TLabel
      Left = 26
      Top = 60
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Senha Atual'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblUsuario: TLabel
      Left = 51
      Top = 33
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usu'#225'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblnovasenha: TLabel
      Left = 28
      Top = 87
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nova Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblrepetirsenha: TLabel
      Left = 14
      Top = 114
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Repetir Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnlLinhaTitulo: TPanel
      Left = 0
      Top = 286
      Width = 444
      Height = 21
      Align = alBottom
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Color = 10526880
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      object lblSite: TLabel
        Left = 302
        Top = 0
        Width = 142
        Height = 21
        Align = alRight
        Caption = ' www.gosoftware.com.br '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitHeight = 13
      end
      object lblVersao: TLabel
        Left = 3
        Top = 4
        Width = 66
        Height = 13
        Caption = 'GoSoftware'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object edtUsuario: TEdit
      Left = 109
      Top = 30
      Width = 257
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object edtSenhaAtual: TEdit
      Left = 109
      Top = 57
      Width = 257
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 2
    end
    object pnlTrocar: TPanel
      Left = 108
      Top = 150
      Width = 257
      Height = 45
      Color = 14470586
      ParentBackground = False
      TabOrder = 3
      object spnTrocar: TSpeedButton
        Left = 1
        Top = 1
        Width = 255
        Height = 43
        Align = alClient
        Caption = 'Trocar'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = spnTrocarClick
        ExplicitLeft = -63
      end
    end
    object edtNovaSenha: TEdit
      Left = 109
      Top = 84
      Width = 257
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 4
      OnKeyPress = edtNovaSenhaKeyPress
    end
    object edtRepetirSenha: TEdit
      Left = 109
      Top = 111
      Width = 257
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 5
      OnKeyPress = edtRepetirSenhaKeyPress
    end
  end
  object sqlTrocarSenha: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dm.conexao
    Left = 314
    Top = 370
  end
  object dsop_dev: TDataSetProvider
    DataSet = sqlTrocarSenha
    UpdateMode = upWhereKeyOnly
    Left = 352
    Top = 370
  end
  object cdsop_dev: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'dsop_dev'
    Left = 392
    Top = 370
    object cdsop_devID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsop_devID_FILIAL: TIntegerField
      FieldName = 'ID_FILIAL'
    end
    object cdsop_devFILIAL: TStringField
      FieldName = 'FILIAL'
      Size = 30
    end
    object cdsop_devNUMTRANSACAO: TIntegerField
      FieldName = 'NUMTRANSACAO'
    end
    object cdsop_devID_PRODUTO: TIntegerField
      FieldName = 'ID_PRODUTO'
    end
    object cdsop_devPRODUTO: TStringField
      FieldName = 'PRODUTO'
      Size = 1200
    end
    object cdsop_devQT: TFMTBCDField
      FieldName = 'QT'
      Precision = 18
      Size = 6
    end
    object cdsop_devCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object cdsop_devNUMOP: TIntegerField
      FieldName = 'NUMOP'
    end
    object cdsop_devCODPRODMASTER: TIntegerField
      FieldName = 'CODPRODMASTER'
    end
    object cdsop_devPRODUTO_ACABADO: TStringField
      FieldName = 'PRODUTO_ACABADO'
      Size = 250
    end
    object cdsop_devSTATUS: TStringField
      FieldName = 'STATUS'
    end
    object cdsop_devID_LOCAL: TIntegerField
      FieldName = 'ID_LOCAL'
    end
    object cdsop_devLOCAL: TStringField
      FieldName = 'LOCAL'
      Size = 250
    end
    object cdsop_devTIPO_MOVIMENTACAO: TStringField
      FieldName = 'TIPO_MOVIMENTACAO'
      Size = 125
    end
    object cdsop_devID_LINHA: TIntegerField
      FieldName = 'ID_LINHA'
    end
    object cdsop_devLINHA: TStringField
      FieldName = 'LINHA'
      Size = 250
    end
    object cdsop_devID_AREA: TIntegerField
      FieldName = 'ID_AREA'
    end
    object cdsop_devAREA: TStringField
      FieldName = 'AREA'
      Size = 250
    end
    object cdsop_devOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1200
    end
  end
end
