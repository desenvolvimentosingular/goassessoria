unit uBaseApi;

interface

uses
  uRetornoApi;

type
  TBaseApi = class
  private
    path: String;
    parametro : String;
  public
    constructor Create(path: String); overload;
    constructor Create(path: String; parametro : String); overload;
    function resposta(): TRetornoApi;
    function getPath() : String;
  end;

implementation

uses uDmApi, uObtemApi, System.SysUtils;

{ TBaseApi }

constructor TBaseApi.Create(path: String);
begin
  self.path := path;
  self.parametro := '';
end;

constructor TBaseApi.Create(path, parametro: String);
begin
  self.path := path;
  self.parametro := parametro;
end;

function TBaseApi.getPath: String;
begin
  result := path;
end;

function TBaseApi.resposta: TRetornoApi;
var
  api: TObtemApi;
  uri : String;
begin
  try
    api := TObtemApi.Create;

    uri := api.obter(path);
    if(Trim(parametro) <> '') then
    begin
      uri := uri +'/'+ Trim(parametro);
    end;

    dmApi.RESTClient1.BaseURL := uri;
    dmApi.RESTRequest1.Execute;
    result := TRetornoApi.Create(dmApi.FDMemTable1status.AsString,
      dmApi.FDMemTable1mensagem.AsString);
  finally
    if(api <> nil) then
    begin
      api := nil;
      api.Free;
      FreeAndNil(api);
    end;
  end;

end;

end.
