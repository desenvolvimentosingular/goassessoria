unit umotivo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.StdCtrls, dxGDIPlusClasses,
  cxImage, Vcl.Buttons, Vcl.ExtCtrls, Data.DB, Datasnap.DBClient, uPesquisa, udm,
  dxSkinsCore, dxSkinSevenClassic;

type
  Tfmotivo = class(TForm)
    pnlbase: TPanel;
    pnlcontroles: TPanel;
    spdok: TButton;
    spdcancelar: TButton;
    pnlLinhaTitulo: TPanel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    pnlLogo: TPanel;
    spdfechar: TSpeedButton;
    pnlLayoutLinhasmeta_producao_item5: TPanel;
    btnPesquisarMOTIVO: TSpeedButton;
    pnlLayoutTitulosmeta_producao_itemid_produto: TPanel;
    pnlLayoutCamposmeta_producao_itemid_produto: TPanel;
    cxm: TcxImage;
    dbemotivoID: TEdit;
    dbemotivoMOTIVO: TEdit;
    psqMotivo: TPesquisa;
    cdsMotivo: TClientDataSet;
    cdsMotivoID: TIntegerField;
    cdsMotivoMOTIVO: TStringField;
    procedure spdokClick(Sender: TObject);
    procedure spdcancelarClick(Sender: TObject);
    procedure spdfecharClick(Sender: TObject);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure psqMotivoPreencherCampos(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmotivo: Tfmotivo;

implementation

{$R *.dfm}

procedure Tfmotivo.FormCreate(Sender: TObject);
begin
  {<<>> fun��o de tradu�a� de componentes>>>}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  {<<>> fun��o de tradu�a� de componentes>>>}
end;

procedure Tfmotivo.FormShow(Sender: TObject);
begin
  cdsMotivo.EmptyDataSet;
  dbemotivoID.setfocus;
end;

procedure Tfmotivo.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
const sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tfmotivo.psqMotivoPreencherCampos(Sender: TObject);
begin
  cdsMotivo.EmptyDataSet;
  cdsMotivo.append;
  cdsMotivoID.asInteger    := strtoInt(dbemotivoID.text);
  cdsMotivoMOTIVO.asstring := dbemotivoMOTIVO.text;
  cdsMotivo.post;
end;

procedure Tfmotivo.spdcancelarClick(Sender: TObject);
begin
 dbemotivoID.text     :=EmptyStr;
 dbemotivoMOTIVO.text :=EmptyStr;
 ModalResult := mrCancel;
end;

procedure Tfmotivo.spdfecharClick(Sender: TObject);
begin
 dbemotivoID.text     :=EmptyStr;
 dbemotivoMOTIVO.text :=EmptyStr;
 ModalResult := mrCancel;
end;

procedure Tfmotivo.spdokClick(Sender: TObject);
begin
 dbemotivoID.text     :=EmptyStr;
 dbemotivoMOTIVO.text :=EmptyStr;
 ModalResult := mrOK;
end;

end.
