unit upergunta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxImage, dxGDIPlusClasses, dxSkinsCore, dxSkinSevenClassic;

type
  TFpergunta = class(TForm)
    pnlbase: TPanel;
    cxm: TcxImage;
    flwpbase: TFlowPanel;
    pnlcontroles: TPanel;
    spdnao: TButton;
    pnlLinhaTitulo: TPanel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    pnlLogo: TPanel;
    spdfechar: TSpeedButton;
    lblSistema: TLabel;
    spdsim: TButton;
    spdlimite: TSpeedButton;
    procedure spdfecharClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spdnaoClick(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure spdsimClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fpergunta: TFpergunta;

implementation

{$R *.dfm}

procedure TFpergunta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Fpergunta:=nil;
	Action:=cafree;
end;

procedure TFpergunta.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TFpergunta.FormShow(Sender: TObject);
begin
 spdnao.SetFocus;
end;

procedure TFpergunta.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TFpergunta.spdnaoClick(Sender: TObject);
begin
 ModalResult := mrOK;
end;

procedure TFpergunta.spdsimClick(Sender: TObject);
begin
  ModalResult := mrYes;
end;

procedure TFpergunta.spdfecharClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

end.
