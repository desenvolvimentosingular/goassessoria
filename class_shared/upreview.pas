unit upreview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, ActnList, frxPreview,
  Menus, frxExportPDF, frxClass, frxExportRTF, jpeg, StdCtrls,
  System.Actions, dxGDIPlusClasses, ShellAPI;

type
  Tfpreview = class(TForm)
    pnlPrincipal: TPanel;
    ACComandos: TActionList;
    acProxima: TAction;
    acVoltar: TAction;
    acUltima: TAction;
    acPrimeira: TAction;
    acImprimir: TAction;
    frPreview: TfrxPreview;
    PopupOpcoes: TPopupMenu;
    frxExportPDF: TfrxPDFExport;
    POPExportarPDF: TMenuItem;
    PopupZoom: TPopupMenu;
    POPZ25: TMenuItem;
    POPZ50: TMenuItem;
    POPZ75: TMenuItem;
    POPZ100: TMenuItem;
    POPZ150: TMenuItem;
    POPZ200: TMenuItem;
    frxExportRTF: TfrxRTFExport;
    ExportarXLSExcel1: TMenuItem;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    spdImpressao: TSpeedButton;
    spdZoom: TSpeedButton;
    spdPrimeiraPag: TSpeedButton;
    spdPaginaAnt: TSpeedButton;
    spdPagProxima: TSpeedButton;
    spdUltmaPag: TSpeedButton;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    pnlLogo: TPanel;
    imglogo: TImage;
    EnviarEmail: TMenuItem;
    procedure acPrimeiraExecute(Sender: TObject);
    procedure acVoltarExecute(Sender: TObject);
    procedure acProximaExecute(Sender: TObject);
    procedure acUltimaExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acImprimirExecute(Sender: TObject);
    procedure SPDFecharClick(Sender: TObject);
    procedure POPExportarPDFClick(Sender: TObject);
    procedure frPreviewPageChanged(Sender: TfrxPreview; PageNo: Integer);
    procedure POPZ25Click(Sender: TObject);
    procedure POPZ50Click(Sender: TObject);
    procedure POPZ75Click(Sender: TObject);
    procedure POPZ100Click(Sender: TObject);
    procedure POPZ150Click(Sender: TObject);
    procedure POPZ200Click(Sender: TObject);
    procedure ExportarXLSExcel1Click(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure spdOpcoesClick(Sender: TObject);
    procedure spdImpressaoClick(Sender: TObject);
    procedure spdZoomClick(Sender: TObject);
    procedure spdPrimeiraPagClick(Sender: TObject);
    procedure spdPaginaAntClick(Sender: TObject);
    procedure spdPagProximaClick(Sender: TObject);
    procedure spdUltmaPagClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblSiteClick(Sender: TObject);
    procedure llblsiteClick(Sender: TObject);
    procedure EnviarEmailClick(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  fpreview: Tfpreview;

implementation

{$R *.dfm}

uses uEnvioMail;

procedure Tfpreview.acPrimeiraExecute(Sender: TObject);
begin
frPreview.First;
end;

procedure Tfpreview.acVoltarExecute(Sender: TObject);
begin
frPreview.Prior;
end;

procedure Tfpreview.acProximaExecute(Sender: TObject);
begin
frPreview.Next;
end;

procedure Tfpreview.acUltimaExecute(Sender: TObject);
begin
frPreview.Last;
end;

procedure Tfpreview.spdPaginaAntClick(Sender: TObject);
begin
acVoltar.Execute;
end;

procedure Tfpreview.spdPagProximaClick(Sender: TObject);
begin
acProxima.Execute;
end;

procedure Tfpreview.spdPrimeiraPagClick(Sender: TObject);
begin
acProxima.Execute;
end;

procedure Tfpreview.FormClose(Sender: TObject; var Action: TCloseAction);
begin
fpreview:=nil;
action:=cafree;
frPreview.Cancel;

end;

procedure Tfpreview.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  frPreview.MouseWheelScroll(WheelDelta);
end;

procedure Tfpreview.FormShow(Sender: TObject);
begin
  frPreview.Zoom:=1;
end;

procedure Tfpreview.acImprimirExecute(Sender: TObject);
begin
 frPreview.Print;
end;

procedure Tfpreview.SPDFecharClick(Sender: TObject);
begin
close;
end;

procedure Tfpreview.spdImpressaoClick(Sender: TObject);
begin
 ACImprimirExecute(Self);
end;

procedure Tfpreview.spdOpcoesClick(Sender: TObject);
begin
 PopupOpcoes.Popup(mouse.CursorPos.X,mouse.CursorPos.Y);
end;

procedure Tfpreview.POPExportarPDFClick(Sender: TObject);
begin
 frPreview.Export(frxExportPDF);
end;

procedure Tfpreview.frPreviewPageChanged(Sender: TfrxPreview;
  PageNo: Integer);
begin
 pnlLinhaTitulo.Caption:=' P�gina ' + IntToStr(frPreview.PageNo)+' de ' + IntToStr(frPreview.PageCount);
end;

procedure ShellOpen(const Url: string; const Params: string = '');
begin
   ShellExecute(0, nil, PChar('http://www.gosoftware.com.br'), nil, nil, 1);
  //ShellExecute(0, 'Open', PChar(Url), PChar(Params), nil, SW_SHOWNORMAL);
end;

procedure Tfpreview.lblSiteClick(Sender: TObject);
begin
  ShellExecute(Application.Handle, 'open', 'www.gosoftware.com.br', nil, nil, 0);
end;

procedure Tfpreview.llblsiteClick(Sender: TObject);
begin

  ShellExecute(0, nil, PChar('www.gosoftware.com.br'), nil, nil, 1);

end;

procedure Tfpreview.POPZ25Click(Sender: TObject);
begin
frPreview.Zoom:=0.25;
end;

procedure Tfpreview.POPZ50Click(Sender: TObject);
begin
frPreview.Zoom:=0.50;
end;

procedure Tfpreview.POPZ75Click(Sender: TObject);
begin
frPreview.Zoom:=0.75;
end;

procedure Tfpreview.POPZ100Click(Sender: TObject);
begin
frPreview.Zoom:=1;
end;

procedure Tfpreview.POPZ150Click(Sender: TObject);
begin
frPreview.Zoom:=1.5;
end;

procedure Tfpreview.spdUltmaPagClick(Sender: TObject);
begin
acUltima.Execute;
end;

procedure Tfpreview.spdZoomClick(Sender: TObject);
begin
PopupZOOM.Popup(mouse.CursorPos.X,mouse.CursorPos.Y);
end;

procedure Tfpreview.POPZ200Click(Sender: TObject);
begin
frPreview.Zoom:=2;
end;

procedure Tfpreview.EnviarEmailClick(Sender: TObject);
begin
  if (FenvioMail = nil) then Application.CreateForm(TFenvioMail, FenvioMail);
    FenvioMail.ShowModal;
end;

procedure Tfpreview.ExportarXLSExcel1Click(Sender: TObject);
begin
  frPreview.Export(frxExportRTF);
end;

end.
