unit uFuncoes;

interface

uses
  SysUtils, Variants, Classes, Graphics, TlHelp32,  StdCtrls,
 DBCtrls,Forms,DB, DBClient, SqlExpr,Menus,ExtCtrls,
 ComCtrls, WinSock,ScktComp, cxContainer, cxTextEdit, cxCurrencyEdit,
  cxMaskEdit, cxButtonEdit, cxDBEdit,FireDAC.Stan.Intf, FireDAC.Stan.Option,
 FireDAC.Stan.Error, FireDAC.UI.Intf,  FireDAC.Stan.Def,
 FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
 FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DBXInterBase,
 FireDAC.Phys.Intf, FireDAC.Phys.PG, Winapi.ShellAPI,
 FireDAC.Phys.PGDef, FireDAC.Phys.FBDef, FileCtrl, StrUtils,
 cxGridLevel, cxClasses,    frxExportPDF, frxClass, frxExportRTF,
 cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
 Grids, cxGrid, Vcl.Controls, cxCalendar,
 Windows, Messages,  Dialogs, IniFiles, cxPC,cxGridExportLink,
  Mask,  FMTBcd,  Provider, midaslib, cxDBLookupComboBox,
 Buttons, udm, dateutils, Vcl.DialogMessage,System.Win.ComObj,
 IdSMTP, IdSSLOpenSSL, IdMessage, IdText, IdAttachmentFile,
  IdExplicitTLSClientServerBase, uprocessoatualizacao;

type
 {Classes de Helpers}

  TCustomeditHelper = class helper for TCustomEdit
  public
   function IsEmpty: boolean;
  private

  end;

 {classe para verifica��o de duplicidade}

 TDuplicidade = class
   private

   public
    agrupador: string;
    objeto:TWinControl;
    campo: string;
    tipo: string;
  end;

  TDuplicidadeObjeto = class
   private

   public
    agrupador: string;
    objeto:TFieldType;
    campo: string;
    tipo: string;
  end;

  {classe para constru��o de pesquisas}
  TPesquisas = class
   public
    agrupador: string;
    titulo:string;
    campo: string;
    tipoFDB: string;
    criterio:string;
    intervalo: string;
    tamanho: integer;
    comboItens: TStrings;
    lookupSelect: string;
    lookupCampoID: string;
    lookupCampoTipoID: string;
    lookupCampoDescricao: string;
    lookupGridItens:TStrings;
    constructor Create;
    constructor Destroy;
  end;

   {Classe fun�a� de mail}
   Tmail = class
   public
    porta: integer;
    host:string;
    username: string;
    senha: string;
    remetente:string;
    destinatario: string;
    nome_remetente: string;
    copia: string;
    copia_oculta: string;
    assunto : string;
    anexo: string;
    corpo : string;
   // constructor Create;
   // constructor Destroy;
  end;

  {classe global de fun��es}
  TFuncoes = class(TComponent)
  private

    sqlConnectionGlobal: TSQLConnection;
    sqlConnectionGlobalHistorico: TSQLConnection;
    dmGlobal : TDataModule;

    sqlDataSetGlobal: TSQLDataSet;
    clientDataSetGlobal: TClientDataSet;
    clientDataSetPesquisaGlobal: TClientDataSet;

    pesquisasListaGlobal: Array of TPesquisas;
    pesquisasListaGlobalFiltros: Array of TPesquisas;
    painelPesquisasGlobal: TFlowPanel;

    sqlGlobal, sqlWhereGlobal, sqlOrderbyGlobal: string;

    formularioGlobal: TForm;

    funcaoPosPesquisaGlobal: TNotifyEvent;

    socketServidor: TServerSocket;

    const
      cDIRSPOOL: String = 'C:\GOASSESSORIA';
      cEXTENSAOGRIDS = '.grd';


    procedure LimparConsulta(Sender: TObject);
    procedure LimparDados(Sender: TObject);
    procedure MoveAnterior(Sender: TObject);
    procedure MoveProximo(Sender: TObject);

    procedure criaPesquisas(Sender: TObject);
    procedure defineTratamentoImputacao(tipo: string; objeto: TWinControl);
    function recuperarDadosAnteriores(clientDataSet: TClientDataSet): string;
    function recuperarDadosPosteriores(clientDataSet: TClientDataSet): String;
    procedure socketServidorClientRead(Sender: TObject;
      Socket: TCustomWinSocket);


  protected

  public

    validarTransacaoRelacionamento: boolean;
    autogeneratorsequencia : integer;
    autoAplicarAtualizacoesBancoDados: boolean;
    procedure executarPesquisa(Sender: TObject);

     procedure exportarGrides(formulario: Tform;gride:TcxGrid);

    procedure criaAtalhoPopupMenu(menuPopup: TPopupMenu; titulo: string;
      teclaAtalho: TShortCut; evento: TNotifyEvent);

    procedure criaAtalhoPopupMenuNavegacao(menuPopup: TPopupMenu;
      clientDataSet: TClientDataSet);

    procedure criarPesquisas(sqlDataSet: TSQLDataSet;
      clientDataSet: TClientDataSet; sql, sqlWhere, sqlOrderby: string;
      painelOpcoes, painelPesquisas: TFlowPanel;
      pesquisasLista: array of TPesquisas;funcaoPosPesquisa: TNotifyEvent;
      clientDataSetPesquisa: TClientDataSet);

    procedure carregarfiltros(pesquisasLista: array of TPesquisas; clientdataset: TclientDataset;painelOpcoes:TFlowPanel);

    //Menu Relatorios
    procedure Menu(Sender: TObject; PopupOpcao: TPopupMenu);
    procedure Relatorio(Sender: TObject; frxrelatorio: TfrxReport; cdsrelatorio: TClientDataSet );
    //Menu Relatorios


    function autoNumeracaoGenerator(conexao: TSQLConnection;generator: string): integer;

    function Duplicarregistro(pDatasetpai : TClientDataSet;ocultarmensagem :Boolean = false; ID : String='ID';generator : string='') : Boolean;

    function autoNumeracaoSelect(conexao: TSQLConnection; tabela,
      campo: string): integer;

    function adicionar(clientDataSet: TClientDataSet): boolean;

    function alterar(clientDataSet: TClientDataSet): boolean;

    procedure ativarModoEdicao(objetosModoEdicaoAtivos,
      objetosModoEdicaoInativos: array of TControl; ativar: Boolean);overload;

    procedure ativarModoEdicao(objetosModoEdicaoInativos: array of TControl; ativar: Boolean); overload;
    procedure desabilitar_clientdataset(clientDataSet: TClientDataSet);
    procedure habilitar_clientdataset(clientDataSet: TClientDataSet);
    function update_indice_cds(clientdataset: TClientDataSet;  nome, fields: string): boolean;

    function cancelar(clientDataSet: TClientDataSet): boolean;

    function excluir(clientDataSet: TClientDataSet; ocultarPergunta :Boolean = false): boolean;

    function inativar(clientDataSet: TClientDataSet; ocultarPergunta :Boolean = false): boolean;

    function gravar(clientDataSet: TClientDataSet): boolean;

    function atualizar(clientDataSet: TClientDataSet): boolean;

    function filtrar(clientDataSet: TClientDataSet; filter : string): boolean;


    {funcionalidade de permissa�}
    function permissao(ROTINA, PERMISSAO: String;USUARIO : string ='Admin') : boolean;
    procedure permissao_rotina(fquery: TFdQuery; USUARIO, ROTINA: String);
    {Funcionalidades de conex�o}
    function fecharconexao(usuario, ipcomputador: string; conexao : Tsqlconnection): boolean; overload;
    function fecharconexao(dm : TDataModule): boolean;overload;
    {Funcionalidades de conex�o}

    {Fun��es de Data}
    function DiaAnterior(hoje: TDateTime): TDateTime;
    function DiaAtual(hoje: TDateTime): TDateTime;
    function SemanaAtual(hoje: TDateTime):TStringList;
    function SemanaAnterior(hoje: TDateTime): TStringList;
    function UltimosQuinzeDias(hoje: TDateTime): TStringList;

    function MesAtual(hoje: TDateTime): TStringList;
    function MesAnterior(hoje: TDateTime): TStringList;

    function semestreatual(hoje: TDateTime): TStringList;
    function semestreanterior(hoje: TDateTime): TStringList;
    function trimestreatual(hoje: TDateTime): TStringList;


    function anoantual(hoje: TDateTime): TStringList;
    function anoanterior(hoje: TDateTime): TStringList;
    {Fun��es de Data}

    procedure verificarCamposObrigatorios(clientDataSet: TClientDataSet;
      camposObrigatorios: array of TWinControl; pagecontrol : TcxPageControl=nil);

    procedure verificarDuplicidade(conexao: TFDConnection;
      clientDataSet: TClientDataSet; tabelaNome, chavePrimaria: string;
      duplicidadeLista: array of TDuplicidade);


    function verificarEmTransacao(clientDataSet: TClientDataSet): boolean;
    function KillTask(ExeFileName: string): Integer;



    procedure SomenteDouble(Sender: TObject; var Key: Char);

    procedure SomenteNumeros(Sender: TObject; var Key: Char);

    procedure definirMascaraCampos(clientDataSet: TClientDataSet);

    {inicio bloco de configura��es personalizadas}
    function  GetNomeArquivoConfg(pform : String;tcxgriddbTableView: TcxGridDBTableView): string; overload;
    procedure configurarGridesFormulario (pForm : TForm; psalvar, pcarregar : boolean);
    procedure carregarGridespersonalizadas(pform : String;
                                           pGrid: TcxGridDBTableView;
                                           bCarregarCaption: Boolean = False); overload;

    procedure SalvarGridespersonalizadas(pform : String;tcxgriddbTableView :TcxGridDBTableView;
                                                bSalvarCaption: Boolean = False);
    procedure salvarTextoAnsi(dados:TStrings; caminho:String);

     procedure SalvarArquivoBD(psArquivo: String);
     procedure  clonar_valores(clientdataset: TclientDataSet;campos:  String; sOrigem: String = '';
      IdOrigem:  Integer = 0;
      bValidaNumero: Boolean = True);

     {final bloco de configura��es personalizadas}

    procedure visualizarRelatorios(relatorio: TfrxReport;exportar_pdf : boolean =false);

    procedure definirConexao(sqlConnection: TSQLConnection);overload;
    procedure definirConexaohistorico(sqlConnection: TSQLConnection);overload;

    procedure definirDataModule(dtmodule: tDatamodule);overload;

    procedure definirFormulario(formulario: TForm);

    procedure listar_componentes(clientdataset: TclientDataSet ;formulario: TForm);

    function selecionarDiretorio(Title : string) : string;

    function recuperarVersaoExecutavel: string;overload;
    function recuperarVersaoExecutavel(Arquivo: string): string; overload;

    function recuperarNomeComputador: string;
    function recuperarIP: string;

    function exibirLogin(sqlConnection: TSQLConnection; titulo: string; usuario: string) : TStringList;

    procedure gravarprocesso(sqlConnection: TSQLConnection; tabela : String);
    function processexists(exefilename : String): boolean;

    {Fun��e de banco de dados}
    procedure socketServidorInicia(porta: Integer);
    procedure socketServidorFinaliza;
    function MouseShowCursor(const Show: boolean): boolean;

    {Fun��es de importa��es}
    procedure importartoStringgrid(stringGrid: Tstringgrid);
    function importartoStringgrid_agrupado(stringGrid: Tstringgrid): string;
    function XlsToStringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;


    {fun��es de correspond�bcia}
    function enviar_email(parametros : Tmail):boolean;

    {Fun��es de atualiza��o}
    function atualizacao_versao(parametros : TStringList):boolean;


    constructor Create(AOwner: TComponent);
    destructor Destroy; override;





  published

  end;

  procedure Register;


implementation
uses
ulogin, upreview;

procedure Register;
begin
  RegisterComponents('Singular', [TFuncoes]);
end;

{ TPesquisas }
constructor TPesquisas.Create();
begin
 comboItens     :=TStringList.Create;
 lookupGridItens:=TStringList.Create;
end;

constructor TPesquisas.Destroy();
begin
 comboItens.Free;
 lookupGridItens.Free;
end;

{ TFuncoes}
constructor TFuncoes.Create(AOwner: TComponent);
var arquivoini : Tinifile;
 begin
  inherited Create(AOwner);

end;

destructor TFuncoes.Destroy;
 begin
  Destroying;
  inherited Destroy;
end;

function TFuncoes.DiaAnterior(hoje: TDateTime): TDateTime;
begin
  result := Yesterday;
end;

function TFuncoes.DiaAtual(hoje: TDateTime): TDateTime;
begin
  Result := Date;
end;

function TFuncoes.Duplicarregistro(pDatasetpai : TClientDataSet;ocultarmensagem :Boolean = false; ID : String='ID';generator : string='') : Boolean;
var
  cdsClone: TClientDataSet; i : integer; posicaoTabela :TBookMark;
begin
 try
   if pDatasetpai.IsEmpty then
     abort;

   try
    Result             :=  True;
    cdsClone           :=  TClientDataSet.Create(nil);
    cdsClone.CloneCursor(pDatasetpai,false, false);

    posicaoTabela      :=  pDatasetpai.GetBookmark;
    pDatasetpai.Insert;
    pDatasetpai.CopyFields(cdsClone);
    if generator<>EmptyStr then
     pDatasetpai.FieldByName(ID).AsInteger:=autoNumeracaoGenerator(dm.conexao,generator)
    else
    pDatasetpai.FieldByName(ID).AsInteger:=0;

    pDatasetpai.Post;
    pDatasetpai.ApplyUpdates(0);

    if ocultarmensagem=false then
     application.MessageBox('Registro duplicado com sucesso.','Aten��o.',MB_OK);

   finally
    pDatasetpai.GotoBookmark(posicaoTabela);
   end;
 except
   on e : exception do
   begin
      application.MessageBox(pchar('N�o foi poss�vel duplicar o registro selecionado.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
      Result   :=  False;
   end;
 end;

end;

procedure TFuncoes.definirdatamodule(dtmodule: tDatamodule);
begin
  dmGlobal  := dtmodule;
end;

procedure TFuncoes.definirFormulario(formulario:TForm);
begin
  formularioGlobal:=formulario;
end;

function TFuncoes.recuperarVersaoExecutavel: String;
type
   PFFI = ^vs_FixedFileInfo;
var
   F       : PFFI;
   Handle  : Dword;
   Len     : Longint;
   Data    : Pchar;
   Buffer  : Pointer;
   Tamanho : Dword;
   Parquivo: Pchar;
   Arquivo : String;
begin
   Arquivo  := Application.ExeName;
   Parquivo := StrAlloc(Length(Arquivo) + 1);
   StrPcopy(Parquivo, Arquivo);
   Len := GetFileVersionInfoSize(Parquivo, Handle);
   Result := '';
   if Len > 0 then
   begin
      Data:=StrAlloc(Len+1);
      if GetFileVersionInfo(Parquivo,Handle,Len,Data) then
      begin
         VerQueryValue(Data, '',Buffer,Tamanho);
         F := PFFI(Buffer);
         Result := Format('%d.%d.%d.%d',
                          [HiWord(F^.dwFileVersionMs),
                           LoWord(F^.dwFileVersionMs),
                           HiWord(F^.dwFileVersionLs),
                           Loword(F^.dwFileVersionLs)]
                         );
      end;
      StrDispose(Data);
   end;
   StrDispose(Parquivo);
end;

function TFuncoes.recuperarVersaoExecutavel(Arquivo: string): string;
type
   PFFI = ^vs_FixedFileInfo;
var
   F       : PFFI;
   Handle  : Dword;
   Len     : Longint;
   Data    : Pchar;
   Buffer  : Pointer;
   Tamanho : Dword;
   Parquivo: Pchar;
begin
   Parquivo := StrAlloc(Length(Arquivo) + 1);
   StrPcopy(Parquivo, Arquivo);
   Len := GetFileVersionInfoSize(Parquivo, Handle);
   Result := '';
   if Len > 0 then
   begin
      Data:=StrAlloc(Len+1);
      if GetFileVersionInfo(Parquivo,Handle,Len,Data) then
      begin
         VerQueryValue(Data, '',Buffer,Tamanho);
         F := PFFI(Buffer);
         Result := Format('%d.%d.%d.%d',
                          [HiWord(F^.dwFileVersionMs),
                           LoWord(F^.dwFileVersionMs),
                           HiWord(F^.dwFileVersionLs),
                           Loword(F^.dwFileVersionLs)]
                         );
      end;
      StrDispose(Data);
   end;
   StrDispose(Parquivo);
end;


procedure TFuncoes.SalvarArquivoBD(psArquivo: String);

procedure CriarDataset;
begin

end;

 var
 vsNomeArquivo: String;
 i : integer;
 valor : String;
  f:TextFile;
 linha:String;

begin

   vsNomeArquivo := ExtractFileName(psArquivo);


   AssignFile(f,psArquivo);
   Reset(f);
   While not eof(f) do begin
     Readln(f,linha);

   End;

       Closefile(f);
       with dmGlobal do
          begin

           for I := 0 to dmGlobal.ComponentCount-1 do
           begin
              if (dmGlobal.Components[i] is TClientDataSet)  then
               begin
                 (dmGlobal.Components[i] as TClientDataSet).EmptyDataSet;
                 (dmGlobal.Components[i] as TClientDataSet).LoadFromFile(psArquivo);
                 (dmGlobal.Components[i] as TClientDataSet).Open;
                 valor :=(dmGlobal.Components[i] as TClientDataSet).FieldByName('valor').AsString;
               end;
           end;

           for I := 0 to dmGlobal.ComponentCount-1 do
           begin
             try

              if (dmGlobal.Components[i] is TFDQuery)  then
              begin
                  (dmGlobal.Components[i] as TFDQuery).Close;
                  (dmGlobal.Components[i] as TFDQuery).ParamByName('NOME').asString := vsNomeArquivo;
                  (dmGlobal.Components[i] as TFDQuery).Open();
                  if (dmGlobal.Components[i] as TFDQuery).IsEmpty then
                  begin
                      (dmGlobal.Components[i] as TFDQuery).Insert;
                      (dmGlobal.Components[i] as TFDQuery).ParamByName('nome').AsString :=vsNomeArquivo;
                      (dmGlobal.Components[i] as TFDQuery).ParamByName('valor').AsString  := valor;
                  end
                  else
                  (dmGlobal.Components[i] as TFDQuery).Edit;

                (dmGlobal.Components[i] as TFDQuery).ApplyUpdates(0);
              end;

             except on E: Exception do
             begin
               application.MessageBox(pchar(e.Message), 'Aviso', MB_OK+64);
             end;
            end;
          end;
       end;

end;


procedure TFuncoes.SalvarGridespersonalizadas( pform : String;
  tcxgriddbTableView: TcxGridDBTableView; bSalvarCaption: Boolean = False);
  var vsNomeArquivo: String;
begin
  vsNomeArquivo := GetNomeArquivoConfg(pform,tcxgriddbTableView);
  try
    { Salvando os grids }
    tcxgriddbTableView.StoreToIniFile(vsNomeArquivo,
                         True,
                         [gsoUseFilter..gsoUseSummary],
                         tcxgriddbTableView.Name);

    // SalvarArquivoBD(vsNomeArquivo);
    { Salvando os captions dos campos }
    if bSalvarCaption then
   //   SalvarCaptions(tcxgriddbTableView.DataController.DataSet);
  except

  end;
end;

procedure TFuncoes.salvarTextoAnsi(dados: TStrings; caminho: String);
var
 f: textfile;
  I: Integer;
begin
  assignfile(f,caminho);
  rewrite(f);
  for I := 0 to dados.Count - 1 do
   begin
    writeln(f,dados.Strings[i]);
  end;
  closefile(f)
end;

function TFuncoes.selecionarDiretorio(Title: string): string;
var
  Pasta : String;
begin
  SelectDirectory(Title, '', Pasta);
  if (Trim(Pasta) <> '') then
    if (Pasta[Length(Pasta)] <> '\') then
      Pasta := Pasta + '\';
  Result := Pasta;
end;

function TFuncoes.SemanaAnterior(hoje: TDateTime): TStringList;
var
 datas :TStringList;
begin
  try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheWeek (date-7)));
     datas.Add(DatetoStr(EndofTheWeek(date-7)));

   finally
     result := datas;

   end;
end;

function TFuncoes.SemanaAtual(hoje: TDateTime): TStringList;
var
 datas :TStringList;
begin
   try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheWeek(date)));
     datas.Add(DatetoStr(EndofTheWeek(date)));

   finally
     result := datas;

   end;
end;


function TFuncoes.semestreanterior(hoje: TDateTime): TStringList;
var
 datas :TStringList;
begin
try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheMonth(date-360)));
     datas.Add(DatetoStr(StartOfTheMonth(date-180)));
   finally
     result := datas;

   end;
end;

function TFuncoes.semestreatual(hoje: TDateTime): TStringList;
var
 datas :TStringList;
begin
   try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheMonth(date-180)));
     datas.Add(DatetoStr(EndofTheMonth(date)));

   finally
     result := datas;

   end;
end;

function TFuncoes.recuperarNomeComputador:string;
var
pcName : pchar; // Recebe o nome do Host
dwSize : dword; // Buffer que aloca o nome do Host
begin
dwSize:= MAX_COMPUTERNAME_LENGTH + 1;
pcName:= strAlloc( dwSize );
if not getComputerName(pcName, dwSize) then result:='error' else result:=pcName;
end;

function TFuncoes.recuperarIP: string;
var
 ss: array[0..128] of char;
 p: PHostEnt;
 WSAData: TWSAData;
begin
  WSAStartup(2, WSAData);
  GetHostName(@ss, 128);
  p := GetHostByName(@ss);
  result := iNet_ntoa(PInAddr(p^.h_addr_list^)^);
  WSACleanup;
end;

procedure TFuncoes.somenteNumeros(Sender: TObject; var Key: Char);
begin
 if not ((key in ['0'..'9'] = true) or ((Key=#22) or (Key=#3) or (key = #8))) then
 key := #0;
end;



function TFuncoes.trimestreatual(hoje: TDateTime): TStringList;
var
 datas :TStringList;
begin
   try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheMonth(date-90)));
     datas.Add(DatetoStr(EndofTheMonth(date)));

   finally
     result := datas;

   end;
end;

function TFuncoes.UltimosQuinzeDias(hoje: TDateTime): TStringList;
var
 datas :TStringList;
 DiaSemanaHoje : Byte;
begin
  try
       datas :=TStringList.Create;
       datas.Add(DateToStr(Date() - 14));
       datas.Add(DateToStr(Date() - 0));
   finally
       result := datas;
  end;

end;

function TFuncoes.update_indice_cds(clientdataset: TClientDataSet; nome,fields: string): boolean;
var i : integer;
begin
try
   result:=false;

   for I := 1 to clientdataset.IndexDefs.Count do
   begin
    clientdataset.IndexDefs.Delete(0);
   end;

   with clientdataset do
   begin
     with IndexDefs.AddIndexDef do
     begin
        Name  :=nome;
        Fields:=fields;
     end;

   end;
  finally
    clientdataset.IndexName:=nome;
    clientdataset.IndexDefs.Update;
    result := clientdataset.IndexDefs.Updated;
  end;
end;

procedure TFuncoes.somenteDouble(Sender: TObject; var Key: Char);
begin
 //case Key of #47..#57, #8, #13,#27, #46: else Key := #0; end;
 if not CharInSet(Key, ['0'..'9',#8, ^V, ^C, ^X]) then
Key := #0;
end;

procedure TFuncoes.limparDados(Sender: TObject);
 begin
  if clientDataSetGlobal.State in [dsedit, dsinsert] then
   begin
    if (Application.MainForm.ActiveMDIChild.ActiveControl) is TDBEdit then
     (Application.MainForm.ActiveMDIChild.ActiveControl as TDBEdit).Field.Clear;

    if (Application.MainForm.ActiveMDIChild.ActiveControl) is TDBComboBox then
     (Application.MainForm.ActiveMDIChild.ActiveControl as TDBComboBox).Field.Clear;
  end;
end;

procedure TFuncoes.listar_componentes(clientdataset: TclientDataSet ;formulario: TForm);
  var i : integer;
begin
  try
     clientDataSet.EmptyDataSet;

     for I := 0 to formulario.ComponentCount-1 do
     begin

       if (formulario.Components[i] is TCustomEdit)   then
       begin
        clientDataSet.Append;
        clientDataSet.FieldByName('NOME').AsString     := (formulario.Components[i] AS TCustomEdit).name;
        clientDataSet.FieldByName('TIPO').AsString     := (formulario.Components[i] AS TCustomEdit).Parent.ToString;
        clientDataSet.FieldByName('ENABLE').AsBoolean :=  (formulario.Components[i] AS TCustomEdit).Enabled;
        clientDataSet.FieldByName('VISIBLE').AsBoolean := (formulario.Components[i] AS TCustomEdit).Visible;
        clientDataSet.FieldByName('HINT').AsString     :=(formulario.Components[i] AS TCustomEdit).hint;
        clientDataSet.Post;
       end;
       if (formulario.Components[i] is TCustomComboBox) then
       begin
        clientDataSet.Append;
        clientDataSet.FieldByName('NOME').AsString     := (formulario.Components[i] as TCustomComboBox).Name;
        clientDataSet.FieldByName('TIPO').AsString     := (formulario.Components[i] AS TCustomComboBox).Parent.ToString;
        clientDataSet.FieldByName('ENABLE').AsBoolean :=  (formulario.Components[i] AS TCustomComboBox).Enabled;
        clientDataSet.FieldByName('VISIBLE').AsBoolean := (formulario.Components[i] AS TCustomComboBox).Visible;
        clientDataSet.FieldByName('HINT').AsString     :=(formulario.Components[i] AS TCustomComboBox).hint;
        clientDataSet.Post;
       end;
       if (formulario.Components[i] is TCustomCheckBox) then
       begin
        clientDataSet.Append;
        clientDataSet.FieldByName('NOME').AsString     := (formulario.Components[i] as TCustomCheckBox).Name;
        clientDataSet.FieldByName('TIPO').AsString     := (formulario.Components[i] AS TCustomCheckBox).Parent.ToString;
        clientDataSet.FieldByName('ENABLE').AsBoolean :=  (formulario.Components[i] AS TCustomCheckBox).Enabled;
        clientDataSet.FieldByName('VISIBLE').AsBoolean := (formulario.Components[i] AS TCustomCheckBox).Visible;
        clientDataSet.FieldByName('HINT').AsString     :=(formulario.Components[i] AS TCustomCheckBox).hint;
        clientDataSet.Post;
       end;


     end;
  except

  end;

end;

function TFuncoes.MesAnterior(hoje: TDateTime): TStringList;
var
 datas:TStringList;

begin
    try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartofTheMonth(date-30)));
     datas.Add(DatetoStr(EndofTheMonth(date-30)));

   finally
     result := datas;

   end;

end;

function TFuncoes.MesAtual(hoje: TDateTime): TStringList;
var
 datas :TStringList;

begin
 try
     datas :=TStringList.Create;
     datas.Add(DatetoStr(StartofTheMonth(date)));
     datas.Add(DatetoStr(EndofTheMonth(date)));

   finally
     result := datas;
   end;

end;

function TFuncoes.MouseShowCursor(const Show: boolean): boolean;
var
I: integer;
begin
    I := ShowCursor(LongBool(true));
    if Show then
    begin
     Result := I >= 0;
     while I < 0 do begin
      Result := ShowCursor(LongBool(true)) >= 0;
       Inc(I);
     end;
    end else
     begin
     Result := I < 0;
      while I >= 0 do begin
        Result := ShowCursor(LongBool(false)) < 0;
        Dec(I);
     end;
    end;
end;

procedure TFuncoes.moveAnterior(Sender: TObject);
 begin
  if not(clientDataSetGlobal.IsEmpty) or (clientDataSetGlobal.Active=true) then
  if not(clientDataSetGlobal.State in [dsedit,dsinsert]) then clientDataSetGlobal.prior;
end;

procedure TFuncoes.moveProximo(Sender: TObject);
 begin
  if not(clientDataSetGlobal.IsEmpty) or (clientDataSetGlobal.Active=true) then
  if not(clientDataSetGlobal.State in [dsedit,dsinsert]) then clientDataSetGlobal.Next;
end;

function TFuncoes.permissao(ROTINA, PERMISSAO: String;USUARIO : string ='Admin' ): boolean;
var sql : string;
begin
   sql:=
    'SELECT goUSUARIOS.USUARIO                                    '+#13#10+
    ',COALESCE(goPERMISSAO_USUARIO.acesso, ''N'') ACESSO          '+#13#10+
    ',goPERMISSAO.ROTINA                                          '+#13#10+
    ',goPERMISSAO.DESCRICAO                                       '+#13#10+
    'FROM goPERMISSAO_USUARIO                                     '+#13#10+
    '	,goPERMISSAO                                                '+#13#10+
    '	,goUSUARIOS                                                 '+#13#10+
    'WHERE goPERMISSAO.ID = gOPERMISSAO_USUARIO.ID_PERMISSAO      '+#13#10+
    '	AND goUSUARIOS.ID_USUARIO = goPERMISSAO_USUARIO.ID_USUARIO  '+#13#10+
    '	AND gousuarios.usuario = :USUARIO                           '+#13#10+
    '	AND goPERMISSAO.rotina = :ROTINA                            '+#13#10+
    '	AND goPERMISSAO.DESCRICAO=:PERMISSAO                        ';

  try
    result :=false;
    dm.FDQuery.Close;
    dm.FDQuery.SQL.Clear;
    dm.FDQuery.SQL.Add(sql);
    if USUARIO=EmptyStr then
    dm.FDQuery.ParamByName('USUARIO').asstring   := 'Admin'
    else
    dm.FDQuery.ParamByName('USUARIO').asstring   := usuario;
    dm.FDQuery.ParamByName('ROTINA').asstring    := rotina;
    dm.FDQuery.ParamByName('PERMISSAO').asstring := PERMISSAO;
    dm.FDQuery.Open;

  finally
    if dm.FDQuery.FieldByName('ACESSO').AsString='S' then
     result :=true
    else
     result :=false;
  end;



end;

procedure TFuncoes.permissao_rotina(fquery: TFdQuery; USUARIO, ROTINA: String);
var sql : string;
begin
 sql:=
    'SELECT USUARIOS.USUARIO                                    '+#13#10+
    ',COALESCE(PERMISSAO_USUARIO.acesso, ''N'') ACESSO          '+#13#10+
    ',PERMISSAO.ROTINA                                          '+#13#10+
    ',PERMISSAO.DESCRICAO                                       '+#13#10+
    'FROM PERMISSAO_USUARIO                                     '+#13#10+
    '	,PERMISSAO                                                '+#13#10+
    '	,USUARIOS                                                 '+#13#10+
    'WHERE PERMISSAO.ID = PERMISSAO_USUARIO.ID_PERMISSAO        '+#13#10+
    '	AND USUARIOS.ID_USUARIO = PERMISSAO_USUARIO.ID_USUARIO    '+#13#10+
    '	AND usuarios.usuario = :USUARIO                           '+#13#10+
    '	AND PERMISSAO.rotina = :ROTINA                            ';
  try
    dm.fQuery.Close;
    dm.fQuery.CommandText:=sql;
    dm.fQuery.ParamByName('USUARIO').asstring := usuario;
    dm.fQuery.ParamByName('ROTINA').asstring  := rotina;
    dm.fQuery.Open;
  finally

  end;

end;


function TFuncoes.processexists(exefilename: String): boolean;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  Result := False;
  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
    begin
      Result := True;
    end;
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);

end;



procedure TFuncoes.limparConsulta(Sender: TObject);
 begin
  if not(clientDataSetGlobal.IsEmpty) or (clientDataSetGlobal.Active=true) then
  if not(clientDataSetGlobal.State in [dsedit,dsinsert]) then clientDataSetGlobal.close;
end;

procedure TFuncoes.criaAtalhoPopupMenu(menuPopup:TPopupMenu; titulo:string; teclaAtalho:TShortCut;
evento:TNotifyEvent );
var
  menuItemPopup :TMenuItem;
begin
  menuItemPopup:= TMenuItem.Create(menuPopup);
  menuPopup.Items.add(menuItemPopup);
  menuItemPopup.Caption:=titulo;
  menuItemPopup.ShortCut:=teclaAtalho;
  menuItemPopup.OnClick:=evento;
  menuItemPopup.FreeOnRelease;
end;

procedure TFuncoes.criaAtalhoPopupMenuNavegacao(menuPopup:TPopupMenu; clientDataSet:TClientDataSet);
var
  menuItemProximo,
  menuItemAnterior,
  menuItemLimparPesquisa,
  menuItemLimparCampo :TMenuItem;
begin
  clientDataSetGlobal:=clientDataSet;

  menuItemAnterior:= TMenuItem.Create(menuPopup);
  menuPopup.Items.add(menuItemAnterior);
  menuItemAnterior.Name:= 'menuPopupNavAnterior';
  menuItemAnterior.Caption:='Ir para o registro anterior';
  menuItemAnterior.ShortCut:=VK_PRIOR;
  menuItemAnterior.OnClick:=MoveAnterior;
  menuItemAnterior.FreeOnRelease;

  menuItemProximo:= TMenuItem.Create(menuPopup);
  menuPopup.Items.add(menuItemProximo);
  menuItemProximo.Name:= 'menuPopupNavProximo';
  menuItemProximo.Caption:='Ir para o pr�ximo registro';
  menuItemProximo.ShortCut:=VK_NEXT;
  menuItemProximo.OnClick:=MoveProximo;
  menuItemProximo.FreeOnRelease;

  menuItemLimparPesquisa:= TMenuItem.Create(menuPopup);
  menuPopup.Items.add(menuItemLimparPesquisa);
  menuItemLimparPesquisa.Name:= 'menuPopupLimparPesquisa';
  menuItemLimparPesquisa.Caption:='Limpar �ltima pesquisa';
  menuItemLimparPesquisa.ShortCut:=VK_ESCAPE;
  menuItemLimparPesquisa.OnClick:= LimparConsulta;
  menuItemLimparPesquisa.FreeOnRelease;

  menuItemLimparCampo:= TMenuItem.Create(menuPopup);
  menuPopup.Items.add(menuItemLimparCampo);
  menuItemLimparCampo.Name:= 'menuPopupLimparCampo';
  menuItemLimparCampo.Caption:='Limpar dados do campo selecionado';
  menuItemLimparCampo.ShortCut:=VK_F11;
  menuItemLimparCampo.OnClick:=LimparDados;
  menuItemLimparCampo.FreeOnRelease;

end;

function TFuncoes.autoNumeracaoGenerator(conexao: TSQLConnection;generator:string):integer;
var
sql:TSQLDataSet;
begin
  try
   sql:=TSQLDataSet.Create(Application);
   sql.SQLConnection:=conexao;
   sql.CommandText:= 'select first 1 gen_id(' + generator + ', 1) from rdb$database';
   sql.Open;
   result:=sql.Fields[0].AsInteger;
  except
   Application.MessageBox('Falha na auto numera��o!','Auto Numera��o',MB_OK+MB_ICONERROR);
  end;
  sql.FreeOnRelease;
end;


function TFuncoes.autoNumeracaoSelect(conexao: TSQLConnection; tabela, campo:string):integer;
var
sql:TSQLDataSet;
begin
  try
   sql:=TSQLDataSet.Create(Application);
   sql.SQLConnection:=conexao;
   sql.CommandText:= 'select coalesce(max(' +  campo + '),0) from '+ tabela;
   sql.Open;
   result:=sql.Fields[0].AsInteger + 1;
  except
   Application.MessageBox('Falha na auto numera��o!','Auto Numera��o',MB_OK+MB_ICONERROR);
  end;
  sql.FreeOnRelease;
end;

function TFuncoes.recuperarDadosAnteriores(clientDataSet: TClientDataSet):String;
var
 contador: integer;
 dadosAnteriores:TStrings;
 tituloCampo: string;
 dadoCampo: string;
begin
  dadosAnteriores:= TStringList.Create;
  for contador:=0 to clientDataSet.Fields.Count -1 do
    begin
     tituloCampo:=clientDataSet.Fields[contador].DisplayLabel;
     try
      if (VarType(clientDataSet.Fields[contador].OldValue) <> varEmpty)
      and (VarType(clientDataSet.Fields[contador].OldValue) <> varNull) then
       dadoCampo:=VarToStr(clientDataSet.Fields[contador].OldValue);
     except
       dadoCampo:='';
     end;
     dadosAnteriores.Add('[' + tituloCampo + ' = ' + dadoCampo + ']');
  end;
 result:=dadosAnteriores.Text;
 dadosAnteriores.Free;
end;

function TFuncoes.recuperarDadosPosteriores(clientDataSet: TClientDataSet):String;
var
 contador: integer;
 dadosPosteriores:TStrings;
 tituloCampo: string;
 dadoCampo: string;
begin
  dadosPosteriores:= TStringList.Create;
  for contador:=0 to clientDataSet.Fields.Count -1 do
    begin
     tituloCampo:=clientDataSet.Fields[contador].DisplayLabel;
     dadoCampo:=clientDataSet.Fields[contador].AsString;
     dadosPosteriores.Add('[' + tituloCampo + ' = ' + dadoCampo + ']');
  end;
 result:=dadosPosteriores.Text;
 dadosPosteriores.Free;
end;

procedure TFuncoes.definirConexao(sqlConnection: TSQLConnection);
begin
  sqlConnectionGlobal:=sqlConnection;
end;

procedure TFuncoes.definirConexaohistorico(sqlConnection: TSQLConnection);
begin
  sqlConnectionGlobalHistorico:=sqlConnection;
end;

function TFuncoes.adicionar(clientDataSet:TClientDataSet):boolean;
var
resSucesso:Boolean;
begin
 resSucesso:=true;

 if clientDataSet.MasterSource <> nil then
  begin

   if clientDataSet.MasterSource.DataSet.IsEmpty then
    begin
     Application.MessageBox('� necess�rio selecionar um registro!!!','Inser��o',MB_OK+MB_ICONINFORMATION);
     abort;
   end;

   if validarTransacaoRelacionamento = true then
    begin
     if clientDataSet.MasterSource.DataSet.State in [dsEdit,dsInsert] then
      begin
       Application.MessageBox('Registro j� em transa��o � necess�rio gravar ou cancelar!','Inser��o',MB_OK+MB_ICONINFORMATION);
       abort;
     end;
    end;

 end;

 if clientDataSet.Active=false then clientDataSet.Open;

 if clientDataSet.State in [dsEdit,dsInsert] then
  begin
   Application.MessageBox('Registro j� em transa��o � necess�rio gravar ou cancelar!','Inser��o',MB_OK+MB_ICONINFORMATION);
   abort;
 end;

 Application.MainForm.KeyPreview:=true;
 try
  clientDataSet.Insert;
 except
  resSucesso:=false;
  Application.MessageBox('Erro na inser��o!','Inser��o',MB_OK+MB_ICONERROR);
 end;

Result:=resSucesso;
end;

function TFuncoes.alterar(clientDataSet:TClientDataSet):boolean;
var
resSucesso:Boolean;
begin
 resSucesso:=true;

 if clientDataSet.MasterSource <> nil then
  begin
   if validarTransacaoRelacionamento = true then
    begin
     if clientDataSet.MasterSource.DataSet.State in [dsEdit,dsInsert] then
      begin
       Application.MessageBox('Registro j� em transa��o � necess�rio gravar ou cancelar!','Inser��o',MB_OK+MB_ICONINFORMATION);
       abort;
     end;
    end;
 end;

 if clientDataSet.Active=false then
  begin
   Application.MessageBox('� necess�rio informar o registro a ser alterado!','Altera��o',MB_OK+MB_ICONINFORMATION);
   abort;
 end;

 if clientDataSet.State in [dsEdit,dsInsert] then
  begin
   Application.MessageBox('Registro j� em transa��o � necess�rio gravar ou cancelar!','Altera��o',MB_OK+MB_ICONINFORMATION);
   abort;
 end;

 if clientDataSet.IsEmpty=true then
  begin
   Application.MessageBox('� necess�rio informar o registro a ser alterado!','Altera��o',MB_OK+MB_ICONINFORMATION);
   exit;
 end;

 Application.MainForm.KeyPreview:=true;
 try
  clientDataSet.edit;
 except
  resSucesso:=false;
  Application.MessageBox('Erro na altera��o!','Altera��o',MB_OK+MB_ICONERROR);
 end;

Result:=resSucesso;
end;

function TFuncoes.anoanterior(hoje: TDateTime): TStringList;
var
 datas       :TStringList;
begin
  try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheYear(date-365)));
     datas.Add(DatetoStr(EndofTheYear(date-365)));

   finally
     result := datas;
   end;
end;

function TFuncoes.anoantual(hoje: TDateTime): TStringList;
var
 datas       :TStringList;
begin
  try
     datas     :=TStringList.Create;
     datas.Add(DatetoStr(StartOfTheYear(date)));
     datas.Add(DatetoStr(EndofTheYear(date)));

   finally
     result := datas;
   end;
end;

function TFuncoes.gravar(clientDataSet:TClientDataSet):boolean;
var
 resSucesso:Boolean;
 sqlHistorico: string;
 formularioNome, formularioTitulo: string;
 dadosAnteriores, tipoOperacao: string;
begin
  TDialogMessage.ShowWaitMessage('Gravando dados...',
  procedure
  begin
     resSucesso:=true;

     if clientDataSet.Active=false then
      begin
      Application.MessageBox('� necess�rio informar o registro a ser gravado!!!','Grava��o',MB_OK+MB_ICONINFORMATION);
      exit;
     end;

     if clientDataSet.IsEmpty=true then
      begin
       Application.MessageBox('� necess�rio informar o registro a ser gravado!!!','Grava��o',MB_OK+MB_ICONINFORMATION);
       exit;
     end;

     try
      if clientDataSet.State in [dsEdit,dsInsert] then
       begin

        case clientDataSet.State of
         dsEdit:
          begin
           tipoOperacao:='Altera��o';
           dadosAnteriores:=pchar(recuperarDadosAnteriores(clientDataSet));
          end;
         dsInsert:
          begin
           tipoOperacao:='Inser��o';
          end;
        end;

        clientDataSet.Post;
        if autoAplicarAtualizacoesBancoDados = true then
        begin
          clientDataSet.ApplyUpdates(-1);
          if clientDataSet.UpdateStatus = usUnmodified then
           clientDataSet.Refreshrecord;

        end
        else
         clientDataSet.ApplyUpdates(0);

        if sqlConnectionGlobal<>nil then
         begin

          if formularioGlobal<>nil then
           begin

             formularioNome:=formularioGlobal.Name;
             formularioTitulo:=formularioGlobal.Caption;
          end;

          sqlHistorico:='insert into usuarios_historico (modulo_nome, modulo_titulo,'
          + 'dados_anteriores, dados_posteriores, computador, computador_ip, tipo)'
          + 'values ('
          + #39 + formularioNome + #39 + ','
          + #39 + formularioTitulo + #39 + ','
          + #39 + dadosAnteriores + #39 + ','
          + #39 + pchar(recuperarDadosPosteriores(clientDataSet)) + #39 + ','
          + #39 + recuperarNomeComputador + #39 + ','
          + #39 + recuperarIP + #39 + ','
          + #39 + tipoOperacao + #39 +');';
          sqlConnectionGlobalHistorico.ExecuteDirect(sqlHistorico);
        end;

      end;
     except
       on e : exception do
       begin
          application.MessageBox(pchar('Erro ao gravar registro, motivo:'+e.Message),'Grava��o de registro.',MB_ICONEXCLAMATION);
          clientDataSet.Close;
          resSucesso:=false;
       end;
     end;

  end);
    Result:=resSucesso;
end;

procedure TFuncoes.gravarprocesso(sqlConnection: TSQLConnection;tabela: String);
begin
   try
    if not sqlConnection.Connected then
     exit;
     sqlConnection.ExecuteDirect('insert into sigprocessos(DESCRICAO)values ('+QuotedStr('Importa��o '+tabela)+')');

   except
    on e : Exception do
      Application.MessageBox(pchar(e.Message), 'Erro ao gravar processo', MB_ICONEXCLAMATION);
   end;
end;


procedure TFuncoes.habilitar_clientdataset(clientDataSet: TClientDataSet);
begin
 if clientDataSet.State in[dsBrowse]  then
   clientDataSet.EnableControls;

end;

procedure TFuncoes.importartoStringgrid(stringGrid: Tstringgrid);
var
 opnPlanilha : TSaveDialog;
begin
try
    try
      opnPlanilha := TSaveDialog.Create(nil);
      opnPlanilha.Filter :=
          'Excel (*.xls) |*.xls|XML (*.xml) |*.xml|Arquivo Texto (*.txt) |*.txt|P�gina Web (*.html)|*.html';
        opnPlanilha.Title := 'Importar Dados';
        opnPlanilha.DefaultExt := 'xls';
      if opnPlanilha.Execute then
      begin
        XlsToStringGrid(stringGrid, opnPlanilha.FileName);
      end;
      if FileExists(opnPlanilha.FileName) then
        application.MessageBox('Processo de importa��o conclu�do com sucesso.', 'Exporta��o.', MB_ICONEXCLAMATION + MB_OK);
    finally
      FreeAndNil(opnPlanilha);
    end;
  except
    on e: exception do
    begin
      application.MessageBox(pchar('Processo de importa��o n�o conclu�do.Motivo: ' + e.Message), 'Processo manual', MB_OK + 64);
    end;
  end;
end;

function TFuncoes.importartoStringgrid_agrupado(stringGrid: Tstringgrid): string;
var
 opnPlanilha : TSaveDialog;
begin
  try

   try
      opnPlanilha := TSaveDialog.Create(nil);
      opnPlanilha.Filter :=
          'Excel (*.xls) |*.xls|XML (*.xml) |*.xml|Arquivo Texto (*.txt) |*.txt|P�gina Web (*.html)|*.html';
        opnPlanilha.Title := 'Importar Dados';
        opnPlanilha.DefaultExt := 'xls';
      if opnPlanilha.Execute then
      begin
        XlsToStringGrid(stringGrid, opnPlanilha.FileName);
      end;
      if FileExists(opnPlanilha.FileName) then
        application.MessageBox('Processo de importa��o conclu�do com sucesso.', 'Exporta��o.', MB_ICONEXCLAMATION + MB_OK);
    finally
      result := opnPlanilha.FileName;
      FreeAndNil(opnPlanilha);
    end;
  except
    on e: exception do
    begin
      application.MessageBox(pchar('Processo de importa��o n�o conclu�do.Motivo: ' + e.Message), 'Processo manual', MB_OK + 64);
    end;
  end;
end;

function TFuncoes.inativar(clientDataSet: TClientDataSet;ocultarPergunta: Boolean): boolean;
var
resSucesso:Boolean;
begin
 resSucesso:=true;
 Application.MainForm.KeyPreview:=true;
 try
  if Application.MessageBox('Deseja realmente Inativar o registro?','Inativa��o',MB_YESNO+MB_ICONQUESTION)=idyes then
  begin
    alterar(clientDataSet);
    clientDataSet.FieldByName('STATUS').AsString :='Inativo';
    gravar(clientDataSet);
    clientDataSet.Filtered:=false;
    clientDataSet.Filtered:=true;

  end;
 except
  resSucesso:=false;
  Application.MessageBox('Erro na inativa��o do registro!','Inativa��o.',MB_OK+MB_ICONERROR);
 end;
 Application.MainForm.KeyPreview:=false;
Result:=resSucesso;

end;

function TFuncoes.KillTask(ExeFileName: string): Integer;
  const
      PROCESS_TERMINATE = $0001;
    var
      ContinueLoop: BOOL;
      FSnapshotHandle: THandle;
      FProcessEntry32: TProcessEntry32;
begin
     Result := 0;
      FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
      FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
      ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
      while Integer(ContinueLoop) <> 0 do
      begin
        if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
          UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
          UpperCase(ExeFileName))) then
          Result := Integer(TerminateProcess(
                            OpenProcess(PROCESS_TERMINATE,
                                        BOOL(0),
                                        FProcessEntry32.th32ProcessID),
                                        0));
         ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
      end;
      CloseHandle(FSnapshotHandle);
end;

function TFuncoes.cancelar(clientDataSet:TClientDataSet):boolean;
var
resSucesso:Boolean;
begin
 resSucesso:=true;

 if clientDataSet.Active=false then
  begin
   Application.MessageBox('� necess�rio informar o registro a ser cancelado!','Grava��o',MB_OK+MB_ICONINFORMATION);
   clientDataSet.Cancel;
   resSucesso:=true;;
 end;

 if clientDataSet.IsEmpty=true then
  begin
   Application.MessageBox('� necess�rio informar o registro a ser cancelado!','Grava��o',MB_OK+MB_ICONINFORMATION);
   clientDataSet.Cancel;
   resSucesso:=true;;
 end;

 try
  if clientDataSet.State in [dsEdit,dsInsert] then
   begin
    clientDataSet.Cancel;
  end;
 except
  resSucesso:=false;
  Application.MessageBox('Erro no cancelamento!','Grava��o',MB_OK+MB_ICONERROR);
 end;
 Result:=resSucesso;
end;


procedure TFuncoes.carregarfiltros(pesquisasLista: array of TPesquisas; clientdataset: TclientDataset;painelOpcoes:TFlowPanel);
var i : integer; typefield  : TFieldType;
begin

  try
    clientdataset.FieldDefs.Clear;
    clientdataset.FieldDefs.Add('Campo', ftString,100,false);
    clientdataset.FieldDefs.Add('Valor', ftString,100,false);
  finally
    clientdataset.CreateDataSet;
  end;

end;



procedure TFuncoes.carregarGridespersonalizadas(pForm : string;
                                                pGrid: TcxGridDBTableView; bCarregarCaption: Boolean = False);
var
  vNome: string;
  i: Integer;
begin
   { Localizando os grids do formul�rios }
  vNome := GetNomeArquivoConfg(pForm,pGrid);
  if FileExists(vNome) then
    pGrid.RestoreFromIniFile(vNome,
                             True,
                             False,
                             [gsoUseFilter..gsoUseSummary],
                             pGrid.Name);


end;

procedure TFuncoes.clonar_valores(clientdataset: TclientDataSet; campos,
  sOrigem: String; IdOrigem: Integer; bValidaNumero: Boolean);
  var
  vlr:  Array of Variant;
  i:  Integer;
  b:  TBookmark;
  tCampos:  TStringList;
begin
    if clientdataset.FieldByName('selecionado').AsString <>  'S' then
    Exit;

    b       :=  Nil;
    tCampos :=  TStringList.Create;
    try
        clientdataset.DisableControls;
        b   :=  clientdataset.GetBookmark;

        tCampos.Delimiter     :=  ';';
        tCampos.DelimitedText :=  campos;
        SetLength(vlr, tCampos.Count);

        for I := 0 to tCampos.Count - 1 do
            vlr[i]  :=  clientdataset.FieldByName(tCampos.Strings[i]).AsVariant;

        clientdataset.Filter := 'SELECIONADO = ''S'' ';

        if (sOrigem <> '') and (IdOrigem > 0) then
              clientdataset.Filter   :=  clientdataset.Filter+' AND ('+sOrigem+' = '+IntToStr(IdOrigem)+' OR '+sOrigem+' IS NULL )';


        clientdataset.Filtered :=  True;
        clientdataset.First;
        while not clientdataset.eof do
        begin

            clientdataset.Edit;
            for I := 0 to tCampos.Count - 1 do
            begin
              clientdataset.FieldByName(tCampos.Strings[i]).AsVariant  :=  VLR[i];
            end;
            clientdataset.Post;
            clientdataset.Next;
        end;

    finally
        FreeAndNil(tCampos);
        clientdataset.Filter   :=  '';
        clientdataset.Filtered :=  False;

        if clientdataset.BookmarkValid(b) then
            clientdataset.GotoBookmark(b);

        clientdataset.FreeBookmark(b);
        clientdataset.EnableControls;
    end;
end;

procedure TFuncoes.configurarGridesFormulario(pForm: TForm;psalvar, pcarregar : boolean);
var i : integer;
begin
 for i := 0 to pForm.ComponentCount-1 do
 begin
    if (pForm.Components[i] is TcxGridDBTableView)  then
    begin
     (pForm.Components[i] as TcxGridDBTableView).Styles.Content   := dm.Silver_singular;
     (pForm.Components[i] as TcxGridDBTableView).Styles.Selection := dm.Silverfraco_singular;
     (pForm.Components[i] as TcxGridDBTableView).Styles.Header    := dm.clWindow_singular;
     if psalvar then
      SalvarGridespersonalizadas(pForm.ToString ,(pForm.Components[i] as TcxGridDBTableView) , true)
      else
      carregarGridespersonalizadas(pForm.ToString,(pForm.Components[i] as TcxGridDBTableView), true);
    end;
 end;

end;

function TFuncoes.enviar_email(parametros: Tmail): boolean;
var
  IdSSLIOHandlerSocket: TIdSSLIOHandlerSocketOpenSSL;
  IdSMTP: TIdSMTP;
  IdMessage: TIdMessage;
  IdText: TIdText;
  sAnexo: string;
begin
  result :=false;
  IdSSLIOHandlerSocket := TIdSSLIOHandlerSocketOpenSSL.Create(Self);
  IdSMTP               := TIdSMTP.Create(Self);
  IdMessage            := TIdMessage.Create(Self);

  try
    // Configura��o do protocolo SSL (TIdSSLIOHandlerSocketOpenSSL)
    IdSSLIOHandlerSocket                   := TIdSSLIOHandlerSocketOpenSSL.Create(Self);
    IdSSLIOHandlerSocket.SSLOptions.Method := sslvSSLv23;
    IdSSLIOHandlerSocket.SSLOptions.Mode   := sslmClient;

    // Configura��o do servidor SMTP (TIdSMTP)

    IdSMTP.IOHandler                 := IdSSLIOHandlerSocket;
    IdSMTP.UseTLS                    := utUseImplicitTLS;
    IdSMTP.AuthType                  := satDefault;
    IdSMTP.Host                      := parametros.host;
    IdSMTP.Port                      := parametros.porta;
    IdSMTP.Username                  := parametros.username;
    IdSMTP.Password                  := parametros.senha;

    // Configura��o da mensagem (TIdMessage)
    IdMessage.CharSet      := 'utf-8';
    IdMessage.From.Address :=parametros.destinatario;
    IdMessage.From.Name    :=parametros.nome_remetente;
    IdMessage.ReplyTo.EMailAddresses := IdMessage.From.Address;
    IdMessage.Recipients.Add.Text := parametros.destinatario;
    IdMessage.Subject             := parametros.assunto;
    IdMessage.Encoding            := meMIME;


    { Destinat�rios em c�pia }
    IdMessage.CCList.EMailAddresses := parametros.copia;

    { Destinat�rios em c�pia oculta }
    IdMessage.BccList.EMailAddresses := parametros.copia_oculta;

    // Configura��o do corpo do email (TIdText)
    IdText := TIdText.Create(IdMessage.MessageParts);
    IdText.Body.Add(parametros.corpo);
    IdText.ContentType := 'text/plain; charset=iso-8859-1';

    // Opcional - Anexo da mensagem (TIdAttachmentFile)
    sAnexo := parametros.anexo;
    if FileExists(sAnexo) then
    begin
      TIdAttachmentFile.Create(IdMessage.MessageParts, sAnexo);
    end;

    // Conex�o e autentica��o
    try
      IdSMTP.Connect;
      IdSMTP.Authenticate;
    except
      on E:Exception do
      begin
        MessageDlg('Erro na conex�o ou autentica��o: ' +E.Message, mtWarning, [mbOK], 0);
        Exit;
      end;
    end;

    // Envio da mensagem
    try
      IdSMTP.Send(IdMessage);
      result :=true;
    except
      On E:Exception do
      begin
        MessageDlg('Erro ao enviar a mensagem: ' +E.Message, mtWarning, [mbOK], 0);
      end;
    end;
  finally
    // desconecta do servidor
    IdSMTP.Disconnect;
    // libera��o da DLL
    UnLoadOpenSSLLibrary;
    // libera��o dos objetos da mem�ria
    FreeAndNil(IdMessage);
    FreeAndNil(IdSSLIOHandlerSocket);
    FreeAndNil(IdSMTP);
  end;
end;

function TFuncoes.excluir(clientDataSet:TClientDataSet; ocultarPergunta :Boolean = false):boolean;
var
 resSucesso,pergunta:Boolean;
 sqlHistorico: string;
 formularioNome, formularioTitulo: string;
 dadosAnteriores, tipoOperacao: string;
begin
 resSucesso:=true;
 if clientDataSet.Active=false then
  begin
   application.MessageBox('� necess�rio informar o registro a ser exclu�do','Exclus�o.',MB_OK);
   exit;
 end;

 if clientDataSet.IsEmpty=true then
  begin
   application.MessageBox('� necess�rio informar o registro a ser exclu�do','Exclus�o.',MB_OK);
   //Result:=false;
   exit;
 end;

 try
        if ocultarPergunta=false then
        begin
        if Application.MessageBox('Deseja realmente excluir??','Exclus�o',MB_YESNO+MB_ICONQUESTION)=IDYES then
        begin
            if not (clientDataSet.State in [dsEdit,dsInsert])  then
            begin
              dadosAnteriores:=pchar(recuperarDadosAnteriores(clientDataSet));
              tipoOperacao:='Exclus�o';
            end;

            clientDataSet.Delete;
            clientDataSet.ApplyUpdates(0);

            if sqlConnectionGlobal<>nil then
             begin

              if formularioGlobal<>nil then
               begin

                 formularioNome:=formularioGlobal.Name;
                 formularioTitulo:=formularioGlobal.Caption;
              end;

              sqlHistorico:='insert into usuarios_historico (modulo_nome, modulo_titulo,'
              + 'dados_anteriores, dados_posteriores, computador, computador_ip, tipo)'
              + 'values ('
              + #39 + formularioNome + #39 + ','
              + #39 + formularioTitulo + #39 + ','
              + #39 + dadosAnteriores + #39 + ','
              + #39 + pchar('Registro exclu�do.') + #39 + ','
              + #39 + recuperarNomeComputador + #39 + ','
              + #39 + recuperarIP + #39 + ','
              + #39 + tipoOperacao + #39 +');';
              sqlConnectionGlobalHistorico.ExecuteDirect(sqlHistorico);
            end;

        end;
       end;

      if ocultarPergunta=true then
      begin
       if not (clientDataSet.State in [dsEdit,dsInsert])  then
        begin
          dadosAnteriores:=pchar(recuperarDadosAnteriores(clientDataSet));
          tipoOperacao:='Exclus�o';
        end;

        clientDataSet.Delete;
        clientDataSet.ApplyUpdates(0);

        if sqlConnectionGlobal<>nil then
         begin

          if formularioGlobal<>nil then
           begin

             formularioNome:=formularioGlobal.Name;
             formularioTitulo:=formularioGlobal.Caption;
          end;

          sqlHistorico:='insert into usuarios_historico (modulo_nome, modulo_titulo,'
          + 'dados_anteriores, dados_posteriores, computador, computador_ip, tipo)'
          + 'values ('
          + #39 + formularioNome + #39 + ','
          + #39 + formularioTitulo + #39 + ','
          + #39 + dadosAnteriores + #39 + ','
          + #39 + pchar('Registro exclu�do.') + #39 + ','
          + #39 + recuperarNomeComputador + #39 + ','
          + #39 + recuperarIP + #39 + ','
          + #39 + tipoOperacao + #39 +');';
          sqlConnectionGlobalHistorico.ExecuteDirect(sqlHistorico);
        end;

      end;

 except
  resSucesso:=false;
  Application.MessageBox('Erro na exclus�o!','Exclus�o',MB_OK+MB_ICONERROR);
 end;
Result:=resSucesso;
end;

function TFuncoes.verificarEmTransacao(clientDataSet:TClientDataSet):boolean;
var
resInTrasacao:Boolean;
begin
 resInTrasacao:=False;
 if clientDataSet.State in [dsedit,dsinsert]  then
  begin
   Application.MessageBox('Registro em transa��o, � necess�rio gravar ou cancelar!','Aviso',MB_OK+MB_ICONWARNING);
   abort;
   resInTrasacao:=true;
 end;
 Result:=resInTrasacao;
end;

procedure TFuncoes.ativarModoEdicao(objetosModoEdicaoAtivos, objetosModoEdicaoInativos : Array of TControl; ativar:Boolean);
var
  i: Integer;
begin
 for i := 0 to Length(objetosModoEdicaoAtivos) - 1 do
  begin
    if objetosModoEdicaoAtivos[i] <> nil then
      objetosModoEdicaoAtivos[i].Enabled:=ativar;
 end;

 for i := 0 to Length(objetosModoEdicaoInativos) - 1 do
  begin
    if objetosModoEdicaoInativos[i]<> nil then
      objetosModoEdicaoInativos[i].Enabled:=not ativar;
 end;
end;

procedure TFuncoes.ativarModoEdicao(objetosModoEdicaoInativos: array of TControl; ativar: Boolean);
var
  i: Integer;
begin
 for i := 0 to Length(objetosModoEdicaoInativos) - 1 do
  begin
    if objetosModoEdicaoInativos[i]<> nil then objetosModoEdicaoInativos[i].Enabled:=not ativar;
 end;
end;

function TFuncoes.atualizacao_versao(parametros: TStringList): boolean;
begin
    try
        if (fprocessoatualizacao = nil) then Application.CreateForm(Tfprocessoatualizacao, fprocessoatualizacao);
        fprocessoatualizacao.Parametros:= parametros;

      { fprocessoatualizacao.Parametros[1]:= parametros[1];
        fprocessoatualizacao.Parametros[2]:= parametros[2];
        fprocessoatualizacao.Parametros[3]:= parametros[3];
        fprocessoatualizacao.Parametros[4]:= parametros[4];
        fprocessoatualizacao.Parametros[5]:= parametros[5];
        fprocessoatualizacao.Parametros[6]:= parametros[6];
        fprocessoatualizacao.Parametros[7]:= parametros[7];
        fprocessoatualizacao.Parametros[8]:= parametros[8];}

        fprocessoatualizacao.Show;

    finally
        fprocessoatualizacao.Free
    end

end;

function TFuncoes.atualizar(clientDataSet: TClientDataSet): boolean;
begin
 if not clientDataSet.IsEmpty then
 begin
    if  not (clientDataSet.State  in [dsEdit, dsInsert]) then
    clientDataSet.RefreshRecord;

 end;

end;

procedure TFuncoes.verificarCamposObrigatorios(clientDataSet:TClientDataSet; camposObrigatorios : Array of TWinControl; pagecontrol : TcxPageControl=nil);
var
  i: Integer;
begin
  if not(clientDataSet.State in [dsEdit,dsInsert]) then
   begin
    abort;
  end;

  Application.MainForm.ActiveMDIChild.Perform(WM_nextdlgctl,0,0);
  for i := 0 to Length(camposObrigatorios) - 1 do
   begin
    if (camposObrigatorios[i] is TDBEdit) then
     begin
       if ((camposObrigatorios[i] as TDBEdit).Field.AsString = '') and ((camposObrigatorios[i] as TDBEdit).Enabled=true) then
        begin
        application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
        if (camposObrigatorios[i] as TDBEdit).Tag>0 then
        begin
         pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TDBEdit).Tag;
        end
        else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as TDBEdit).SetFocus;

        abort;
       end;
    end;

    if (camposObrigatorios[i] is Tedit) then
     begin
       if ((camposObrigatorios[i] as Tedit).text = '') and ((camposObrigatorios[i] as Tedit).Enabled=true) then
        begin
        application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
        if (camposObrigatorios[i] as Tedit).Tag>0 then
        begin
         pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as Tedit).Tag;
        end
        else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as Tedit).SetFocus;
        abort;
       end;
    end;

    if (camposObrigatorios[i] is TDBComboBox) then
     begin
       if ((camposObrigatorios[i] as TDBComboBox).Field.AsString = '')  and ((camposObrigatorios[i] as TDBComboBox).Enabled=true) then
        begin
        application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
        if (camposObrigatorios[i] as TDBComboBox).Tag>0 then
        begin
         pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TDBComboBox).Tag;
        end
        else
         pagecontrol.ActivePageIndex:=0;
        (camposObrigatorios[i] as TDBComboBox).SetFocus;
        abort;
       end;
    end;
    if (camposObrigatorios[i] is TComboBox) then
     begin
       if ((camposObrigatorios[i] as TComboBox).text = '')  and ((camposObrigatorios[i] as TComboBox).Enabled=true) then
        begin
         application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
        if (camposObrigatorios[i] as TDBComboBox).Tag>0 then
        begin
         pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TDBComboBox).Tag;
        end
        else
         pagecontrol.ActivePageIndex:=0;
        (camposObrigatorios[i] as TComboBox).SetFocus;
        abort;
       end;
    end;

    if (camposObrigatorios[i] is TDBCheckBox) then
     begin
       if ((camposObrigatorios[i] as TDBCheckBox).Field.AsString = '')  and ((camposObrigatorios[i] as TDBCheckBox).Enabled=true) then
        begin
        application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
        if (camposObrigatorios[i] as TDBCheckBox).Tag>0 then
        begin
         pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TDBCheckBox).Tag;
        end
        else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as TDBCheckBox).SetFocus;
        abort;
       end;
    end;

    if (camposObrigatorios[i] is TCheckBox) then
     begin
       if ((camposObrigatorios[i] as TCheckBox).Caption = '')  and ((camposObrigatorios[i] as TCheckBox).Enabled=true) then
        begin
         application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
         if (camposObrigatorios[i] as TCheckBox).Tag>0 then
         begin
          pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TCheckBox).Tag;
         end
         else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as TCheckBox).SetFocus;
        abort;
       end;
    end;


    if (camposObrigatorios[i] is TDBMemo) then
     begin
       if ((camposObrigatorios[i] as TDBMemo).Field.AsString = '')  and ((camposObrigatorios[i] as TDBMemo).Enabled=true) then
        begin
         application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
         if (camposObrigatorios[i] as TDBMemo).Tag>0 then
         begin
          pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TDBMemo).Tag;
         end
         else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as TDBMemo).SetFocus;
        abort;
       end;
    end;

    if (camposObrigatorios[i] is TMemo) then
     begin
       if ((camposObrigatorios[i] as TMemo).text = '')  and ((camposObrigatorios[i] as TMemo).Enabled=true) then
        begin
         application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
         if (camposObrigatorios[i] as TMemo).Tag>0 then
         begin
          pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TMemo).Tag;
         end
         else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as TMemo).SetFocus;
        abort;
       end;
    end;

    if (camposObrigatorios[i] is tcxDateEdit) then
     begin
       if ((camposObrigatorios[i] as tcxDateEdit).text = '')  and ((camposObrigatorios[i] as tcxDateEdit).Enabled=true) then
        begin
        application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
         if (camposObrigatorios[i] as tcxDateEdit).Tag>0 then
         begin
          pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as tcxDateEdit).Tag;
         end
         else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as tcxDateEdit).SetFocus;
        abort;
       end;
    end;



    if (camposObrigatorios[i] is TcxDBLookupComboBox) then
     begin
       if ((camposObrigatorios[i] as TcxDBLookupComboBox).text = '')  and ((camposObrigatorios[i] as TcxDBLookupComboBox).Enabled=true) then
        begin
         application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
         if (camposObrigatorios[i] as TcxDBLookupComboBox).Tag>0 then
         begin
          pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as TcxDBLookupComboBox).Tag;
         end
         else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as TcxDBLookupComboBox).SetFocus;
        abort;
       end;
    end;

     if (camposObrigatorios[i] is Tcxcurrencyedit) then
     begin
       if ((camposObrigatorios[i] as Tcxcurrencyedit).text = '')  and ((camposObrigatorios[i] as Tcxcurrencyedit).Enabled=true) then
        begin
         application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados','Aten��o.',MB_ICONEXCLAMATION+MB_OK);
         if (camposObrigatorios[i] as Tcxcurrencyedit).Tag>0 then
         begin
          pagecontrol.ActivePageIndex:=(camposObrigatorios[i] as Tcxcurrencyedit).Tag;
         end
         else
         pagecontrol.ActivePageIndex:=0;

        (camposObrigatorios[i] as Tcxcurrencyedit).SetFocus;
        abort;
       end;
    end;

  end;

end;

function defineFormatoDuplicidade(tipo :TFieldType; dado: string):string;
var
resTipo:string;
begin
  resTipo:=dado;
  if tipo=ftInteger then resTipo:= dado;
  if tipo=ftFMTBcd then resTipo:= dado;
  if tipo=ftBlob then resTipo:= #39 + dado + #39;
  if tipo=ftString then resTipo:= #39 + dado + #39;
  if tipo=ftFMTBcd then resTipo:= dado;
  if tipo=ftFloat then resTipo:= dado;
  if tipo=ftFloat then resTipo:= dado;
  if tipo=ftFMTBcd then resTipo:= dado;
  if tipo=ftSmallint then resTipo:= dado;
  if tipo=ftTimeStamp then resTipo:= dado;
  if tipo=ftString then resTipo:= #39 + dado + #39;
  if tipo=ftDate then resTipo:= #39 + dado + #39;
  if tipo=ftTime then resTipo:= #39 + dado + #39;
  if tipo=ftString then resTipo:= #39 + dado + #39;
  result:=resTipo;
end;

{verifica duplicidade de dados em um determinado campo}
procedure TFuncoes.verificarDuplicidade(conexao: TFDConnection; clientDataSet:TClientDataSet; tabelaNome, chavePrimaria: string; duplicidadeLista: Array of TDuplicidade);
var
 i :Integer;
 agrupadorAnterior,sql, sqlWhere,campoNome,dado: string;
 campoTipo:TFieldType;
 sqlVerificaDuplicidade:TFDQuery;
 objeto: TWinControl;
 dataSet: TDataset;
 label
  inicio, fim, montarSQL, executarSQL;
begin

  if not (clientDataSet.State in [dsEdit,dsInsert]) then
   begin
    abort;
  end;

  sqlVerificaDuplicidade:=TFDQuery.Create(Application);
  sqlVerificaDuplicidade.Connection:=conexao;

  sql:=('select count(*) from ' + tabelaNome + ' ');
  sqlWhere:='';

  for i := 0 to Length(duplicidadeLista) - 1 do
   begin

    inicio:
    begin
     if duplicidadeLista[i] = nil then
      begin
       Application.MessageBox('Falha na dimens�o da lista de verifica��o de duplicidade!','Duplicidade',MB_OK+MB_ICONINFORMATION);
       abort;
     end;
     if ((i > 0) and (agrupadorAnterior <> duplicidadeLista[i].agrupador)) then
      goto executarSQL
     else goto montarSQL;
    end;

    montarSQL:
    begin
     objeto:=duplicidadeLista[i].objeto;

     if (objeto) is TDBEdit then
      begin
       dataSet :=(objeto as TDBEdit).DataSource.DataSet;
       campoNome:=(objeto as TDBEdit).Field.FieldName;
       dado:=(objeto as TDBEdit).Field.AsString;
       campoTipo:=(objeto as TDBEdit).Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TDBEdit).DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;
     if (objeto) is TcxDBButtonEdit then
      begin
       dataSet:=(objeto as TcxDBButtonEdit).DataBinding.DataSource.DataSet;
       campoNome:=(objeto as TcxDBButtonEdit).DataBinding.Field.FieldName;
       dado:=(objeto as TcxDBButtonEdit).DataBinding.Field.AsString;
       campoTipo:=(objeto as TcxDBButtonEdit).DataBinding.Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TcxDBButtonEdit).DataBinding.DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;

     if (objeto) is TDBComboBox then
      begin
       campoNome:=(objeto as TDBComboBox).Field.FieldName;
       dado:=(objeto as TDBComboBox).Field.AsString;
       campoTipo:=(objeto as TDBComboBox).Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TDBComboBox).DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;

     if (objeto) is TcxDbDateEdit then
      begin
       campoNome:=(objeto as TcxDbDateEdit).DataBinding.Field.FieldName;
       dado:=(objeto as TcxDbDateEdit).DataBinding.Field.AsString;
       campoTipo:=(objeto as TcxDbDateEdit).DataBinding.Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TcxDbDateEdit).DataBinding.DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;



     if (objeto) is TDBComboBox then
      begin
       campoNome:=(objeto as TDBComboBox).Field.FieldName;
       dado:=(objeto as TDBComboBox).Field.AsString;
       campoTipo:=(objeto as TDBComboBox).Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TDBComboBox).DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;

     if (objeto) is TcxdbLookupComboBox then
      begin
       campoNome:=(objeto as TcxdbLookupComboBox).databinding.Field.FieldName;
       dado:=(objeto as TcxdbLookupComboBox).databinding.Field.AsString;
       campoTipo:=(objeto as TcxdbLookupComboBox).databinding.Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TcxdbLookupComboBox).databinding.DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;

     if (objeto) is TcxDBComboBox then
      begin
       campoNome:=(objeto as TcxDBComboBox).databinding.Field.FieldName;
       dado:=(objeto as TcxDBComboBox).databinding.Field.AsString;
       campoTipo:=(objeto as TcxDBComboBox).databinding.Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TcxDBComboBox).databinding.DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;


     if (objeto) is TDBMemo then
      begin
       campoNome:=(objeto as TDBMemo).Field.FieldName;
       dado:=(objeto as TDBMemo).Field.AsString;
       campoTipo:=(objeto as TDBMemo).Field.DataType;
       if (sqlWhere = '') then
        sqlWhere:='where ' + campoNome + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado)
       else
        sqlWhere:=sqlWhere + ' and ' + (objeto as TDBMemo).DataField + ' = ' +
        defineFormatoDuplicidade(campoTipo, dado);
     end;

     if (i < (Length(duplicidadeLista)-1)) then goto fim else goto executarSQL;
    end;


    executarSQL:
    begin
      if (dataSet.State in [dsedit]) then
       begin
        sqlWhere:= sqlWhere + ' and ' + chavePrimaria + ' <> ' +
        defineFormatoDuplicidade(dataSet.FieldByName(chavePrimaria).DataType,
        dataSet.FieldByName(chavePrimaria).AsString);
      end;

      sqlVerificaDuplicidade.Close;
      sqlVerificaDuplicidade.SQL.Clear;
      sqlVerificaDuplicidade.sql.Add (sql + sqlWhere);
      sqlVerificaDuplicidade.Open;

      if sqlVerificaDuplicidade.Fields[0].Value <> 0 then
       begin
        Application.MessageBox('Dados em duplicidade, j� existe este dado cadastrado!','Duplicidade',MB_OK+MB_ICONINFORMATION);
        //if objeto.Enabled=true then
        //objeto.SetFocus;

        abort;
      end else begin
        sqlWhere:='';
        if  (duplicidadeLista[i].objeto <> objeto) then goto montarSQL;
      end;
    end;

    fim:
    begin
     agrupadorAnterior:=duplicidadeLista[i].agrupador;
    end;
  end;
end;

function defineFormatoPesquisas(tipo :string; dado: string):string;
var
resTipo:string;
begin
  resTipo:=tipo;
  if tipo='Integer' then resTipo:= dado;
  if tipo='BigInt' then resTipo:= dado;
  if tipo='Blob Text' then resTipo:= #39 + dado + #39;
  if tipo='Blob Image' then resTipo:= #39 + dado + #39;
  if tipo='Char' then resTipo:= #39 + dado + #39;
  if tipo='Decimal' then resTipo:= dado;
  if tipo='Double Precision' then resTipo:= dado;
  if tipo='Float' then resTipo:= dado;
  if tipo='Numeric' then resTipo:= dado;
  if tipo='SmallInt' then resTipo:= dado;
  if tipo='TimeStamp' then resTipo:= #39 + dado + #39;
  if tipo='Varchar' then resTipo:= #39 + dado + #39;
  if tipo='Date' then resTipo:= #39 + dado + #39;
  if tipo='Time' then resTipo:= #39 + dado + #39;
  //if tipo='Varchar' then resTipo:= #39 + dado + #39;
  result:=resTipo;
end;

procedure TFuncoes.defineTratamentoImputacao(tipo: string; objeto: TWinControl);

begin
  if objeto is TEdit then
   begin
    if tipo='Integer' then (objeto as TEdit).OnKeyPress:=SomenteNumeros;
    if tipo='BigInt' then (objeto as TEdit).OnKeyPress:=SomenteNumeros;
    if tipo='SmallInt' then (objeto as TEdit).OnKeyPress:=SomenteNumeros;

    if tipo='Decimal' then (objeto as TEdit).OnKeyPress:=SomenteDouble;
    if tipo='Double Precision' then (objeto as TEdit).OnKeyPress:=SomenteDouble;
    if tipo='Float' then (objeto as TEdit).OnKeyPress:=SomenteDouble;
    if tipo='Numeric' then (objeto as TEdit).OnKeyPress:=SomenteDouble;
  end;


end;

procedure TFuncoes.executarPesquisa(Sender: TObject);
var
  i: Integer;
  idArray:integer;
  objetoDado: String;
  objeto: TWinControl;
  filtros: TControl;
  sqlWhere: String;
  rdbtn: TRadioButton;
  cont: SmallInt;
  numreg: SmallInt;
begin
  //Limpar DataSet virtual de filtros antes de carregar novamente
  clientDataSetPesquisaGlobal.EmptyDataSet;
  for i := 0 to painelPesquisasGlobal.ComponentCount - 1 do
   begin
     objeto:=(painelPesquisasGlobal.Components[i]  as TWinControl);

//     if (objeto is TEditBtn) then
//      begin
//       if trim((objeto as TEditBtn).Lookup_Valor_ID)='' then
//        begin
//         Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Aten��o',MB_ICONEXCLAMATION+MB_OK);
//         objeto.SetFocus;
//         abort;
//       end;
//       idArray:=(objeto as TEditBtn).Tag;
//       objetoDado:=(objeto as TEditBtn).Lookup_Valor_ID;
//       if (sqlWhereGlobal <> '') or (sqlWhere <> '') then
//        sqlWhere:= sqlWhere +  ' and ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado)
//       else
//       sqlWhere:=' where ' + pesquisasListaGlobal[idArray].lookupCampoID + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado);
//     end
//     else
     if (objeto is TEdit) then
      begin
       if trim((objeto as TEdit).Text)='' then
        begin
         Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Aten��o',MB_ICONEXCLAMATION+MB_OK);
         objeto.SetFocus;
         abort;
       end;
       idArray:=(objeto as TEdit).Tag;
       objetoDado:=(objeto as TEdit).Text;

       if trim(objeto.Hint)='LIKE' then
        begin
        objeto.Hint:=' LIKE ';
        objetoDado:= '%' + objetoDado + '%';
       end;

       if (sqlWhereGlobal <> '') or (sqlWhere <> '') then
        sqlWhere:= sqlWhere +  ' and ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado)


       else
       sqlWhere:=' where ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado);
     end

     else if (objeto is TComboBox) Or (objeto is TDateTimePicker) then
     begin

        if (objeto is TComboBox) then
        begin
         if trim((objeto as TComboBox).Text)='' then
          begin
           Application.MessageBox('Campo obrigat�rio, � necess�rio inserir dados!','Aten��o',MB_ICONEXCLAMATION+MB_OK);
           objeto.SetFocus;
           abort;
         end;
         idArray:=(objeto as TComboBox).Tag;
         objetoDado:=(objeto as TComboBox).Text;
         if (sqlWhereGlobal <> '') or (sqlWhere <> '') then
          sqlWhere:= sqlWhere +  ' and ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado)
         else
         sqlWhere:=' where ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado);
         end
        else if (objeto is TDateTimePicker) then
        begin
         idArray:=(objeto as TDateTimePicker).Tag;
         objetoDado:=FormatDateTime('dd.mm.yyyy',(objeto as TDateTimePicker).Date);
         if (sqlWhereGlobal <> '') or (sqlWhere <> '') then
          sqlWhere:= sqlWhere +  ' and ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado)
         else
         sqlWhere:=' where ' + pesquisasListaGlobal[idArray].campo + objeto.Hint + defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado);

         end;

     end;

     //adicionar filtros DataSet virtual
     filtros:=(painelPesquisasGlobal.Components[i]  as TControl );
     if (filtros is TDateTimePicker) Or (filtros is TDateTimePicker) Or (filtros is  TEdit) Or (filtros is TComboBox)  then
     begin
            idArray:= filtros.Tag;
            clientDataSetPesquisaGlobal.Insert;
            clientDataSetPesquisaGlobal.FieldByName('Campo').Value := pesquisasListaGlobal[idArray].titulo;
            //Formatar Data para impress�o do relt�rio
            if objeto is TDateTimePicker then
            begin
              objetoDado:=FormatDateTime('dd/mm/yyyy',(filtros as TDateTimePicker).Date);

              clientDataSetPesquisaGlobal.FieldByName('Valor').Value := StringReplace( defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado), '''', '', [rfReplaceAll]) ;
            end
            else
            begin
              clientDataSetPesquisaGlobal.FieldByName('Valor').Value := StringReplace(StringReplace( defineFormatoPesquisas(pesquisasListaGlobal[idArray].tipoFDB,objetoDado), '''', '', [rfReplaceAll]), '%', '', [rfReplaceAll]);
              clientDataSetPesquisaGlobal.Post;
            end;
     end;
    end;

    //Verifica campos que n�o tem consulta para DataSet do Filtro
    for cont := 0 to length(pesquisasListaGlobal)-1 do
    begin

          if pesquisasListaGlobal[cont] <> nil then
          begin

            if clientDataSetPesquisaGlobal.Locate('Campo',  pesquisasListaGlobal[cont].titulo, [lopartialkey,locaseinsensitive]) then
            begin
               //N�o fazer nada se localizar compo igual.
            end
            else
            begin
              clientDataSetPesquisaGlobal.Insert;
              clientDataSetPesquisaGlobal.FieldByName('Campo').Value :=   pesquisasListaGlobal[cont].titulo;
              clientDataSetPesquisaGlobal.FieldByName('Valor').Value := 'Todos os registros';
              clientDataSetPesquisaGlobal.Post;
            end
          end;
    end;

  //clientDataSetPesquisaGlobal.indexfieldname := 'NomedoCampo';
  TDialogMessage.ShowWaitMessage('Executando pesquisa...', procedure
  begin
   sleep(1500);
   clientDataSetGlobal.Close;
   sqlDataSetGlobal. CommandText:=sqlGlobal + ' ' + sqlWhereGlobal + ' ' + sqlWhere + ' ' + sqlOrderbyGlobal;
   clientDataSetGlobal.Open;
  end);


  if clientDataSetGlobal.IsEmpty then
   begin
    Application.MessageBox('Nenhum registro encontrado!','Aten��o',MB_ICONEXCLAMATION+MB_OK);
  end else begin
    for i := 0 to painelPesquisasGlobal.ComponentCount - 1 do
     begin
     { if (painelPesquisasGlobal.Components[i] is TEditBtn)  then
    (painelPesquisasGlobal.Components[i] as TEditBtn).Limpar;

       if (painelPesquisasGlobal.Components[i] is TEdit)  then
       (painelPesquisasGlobal.Components[i] as TEdit).Clear;

       if (painelPesquisasGlobal.Components[i] is TComboBox)  then
       (painelPesquisasGlobal.Components[i] as TComboBox).Text:=''; }
    end;
    Application.MainForm.ActiveMDIChild.Perform(WM_nextdlgctl,0,0);
    if Assigned(funcaoPosPesquisaGlobal) then funcaoPosPesquisaGlobal(Self);
   application.ProcessMessages;
  end;
end;


procedure TFuncoes.criaPesquisas(Sender: TObject);
var
  i: Integer;
  rdbtn: TRadioButton;
  pnlfundo, pnlTitulo: TPanel;
  edtPesquisaInicial, edtPesquisaFinal: TEdit;
  cboPesquisa: TComboBox;
  dtpPesquisaInicial,dtpPesquisaFinal: TDateTimePicker;
  edtbtnPesquisa: TcxDBButtonEdit;
  btnPesquisar: TButton;
begin
 if (sender is TRadioButton) then
  begin
   rdbtn:= (Sender as TRadioButton);
   for i := 0 to Length(pesquisasListaGlobal) - 1 do
    begin
     if pesquisasListaGlobal[i]<> nil then
      begin
       if (painelPesquisasGlobal.FindComponent('pnlPesFundo'+pesquisasListaGlobal[i].agrupador + inttostr(i)) <> nil) then
        begin
         painelPesquisasGlobal.FindComponent('pnlPesFundo' + pesquisasListaGlobal[i].agrupador + inttostr(i)).Destroy;
       end;

       if (pesquisasListaGlobal[i].agrupador) = rdbtn.Hint then
        begin
         pnlfundo:=TPanel.Create(painelPesquisasGlobal);
         pnlfundo.Name:='pnlPesFundo' + pesquisasListaGlobal[i].agrupador + inttostr(i);
         pnlfundo.Caption:='';
         pnlfundo.Parent:=painelPesquisasGlobal;
         pnlfundo.Width:=pesquisasListaGlobal[i].tamanho + 40;
         pnlfundo.BevelOuter:=bvNone;
         pnlfundo.FreeOnRelease;

         pnlTitulo:=TPanel.Create(painelPesquisasGlobal);
         pnlTitulo.Name:='pnlPesTitulo' + pesquisasListaGlobal[i].agrupador + inttostr(i);
         pnlTitulo.Caption:='  ' + pesquisasListaGlobal[i].titulo;
         pnlTitulo.Height:=15;
         pnlTitulo.align:=alTop;
         pnlTitulo.Alignment:=taLeftJustify;
         pnlTitulo.Parent:=pnlfundo;
         pnlTitulo.BevelOuter:=bvNone;
         pnlTitulo.FreeOnRelease;



         if (pesquisasListaGlobal[i].lookupSelect <> '') then
          begin
           edtbtnPesquisa:=TcxDBButtonEdit.Create(painelPesquisasGlobal);
           edtbtnPesquisa.Parent:=pnlfundo;
           edtbtnPesquisa.Name := 'edtBtnPesquisa' + pesquisasListaGlobal[i].agrupador + inttostr(i);
           edtbtnPesquisa.Text:='';
           edtbtnPesquisa.Left:=5;
           edtbtnPesquisa.Top:=17;
           edtbtnPesquisa.Tag:=i;
           edtbtnPesquisa.Hint:=pesquisasListaGlobal[i].criterio;
           edtbtnPesquisa.Properties.LookupItems:=pesquisasListaGlobal[i].lookupGridItens;

//           edtbtnPesquisa.Lookup_Campo_ID:=pesquisasListaGlobal[i].lookupCampoID;
//           edtbtnPesquisa.Lookup_Campo_ID_Tipo:=pesquisasListaGlobal[i].lookupCampoTipoID;
//           edtbtnPesquisa.Lookup_Campo_Descricao:=pesquisasListaGlobal[i].lookupCampoDescricao;
//           edtbtnPesquisa.Lookup_SQL_Select:=pesquisasListaGlobal[i].lookupSelect;
//           edtbtnPesquisa.Lookup_SQL_Where:=FLookupSQLWhere1;

           edtbtnPesquisa.Width:=pesquisasListaGlobal[i].tamanho;

         end else if (pesquisasListaGlobal[i].comboItens.Count <> 0) then
          begin
           cboPesquisa:=TComboBox.Create(painelPesquisasGlobal);
           cboPesquisa.Parent:=pnlfundo;
           cboPesquisa.Name := 'cboPesquisa' + pesquisasListaGlobal[i].agrupador + inttostr(i);
           cboPesquisa.Text:='';
           cboPesquisa.Left:=5;
           cboPesquisa.Top:=17;
           cboPesquisa.Tag:=i;
           cboPesquisa.Sorted:=false;
           cboPesquisa.Color   :=$00E6DACC;
           cboPesquisa.Items:=pesquisasListaGlobal[i].comboItens;
           cboPesquisa.Style:=TComboBoxStyle(csOwnerDrawFixed);
           cboPesquisa.Width:=pesquisasListaGlobal[i].tamanho;
           cboPesquisa.Hint:=pesquisasListaGlobal[i].criterio;
           cboPesquisa.SetFocus;
         end else if (pesquisasListaGlobal[i].tipoFDB = 'Date') then
          begin
           dtpPesquisaInicial:=TDateTimePicker.Create(painelPesquisasGlobal);
           dtpPesquisaInicial.Parent:=pnlfundo;
           dtpPesquisaInicial.Name := 'dtpPesInicial' + pesquisasListaGlobal[i].agrupador + inttostr(i);
           dtpPesquisaInicial.Left:=5;
           dtpPesquisaInicial.Top:=17;
           dtpPesquisaInicial.Width:=100;
           dtpPesquisaInicial.Tag:=i;
           dtpPesquisaInicial.Hint:=pesquisasListaGlobal[i].criterio;
           dtpPesquisaInicial.FreeOnRelease;
           dtpPesquisaInicial.SetFocus;

           if pesquisasListaGlobal[i].intervalo='S' then
            begin
             dtpPesquisaInicial.Hint:='>=';
             pnlfundo.Width:=pnlfundo.Width * 2;
             dtpPesquisaFinal:=TDateTimePicker.Create(painelPesquisasGlobal);
             dtpPesquisaFinal.Parent:=pnlfundo;
             dtpPesquisaFinal.Name := 'dtpPesFinal' + pesquisasListaGlobal[i].agrupador + inttostr(i);
             dtpPesquisaFinal.Left:=115;
             dtpPesquisaFinal.Top:=17;
             dtpPesquisaFinal.Width:=100;
             dtpPesquisaFinal.Tag:=i;
             dtpPesquisaFinal.Hint:='<=';
             dtpPesquisaFinal.FreeOnRelease;
           end;

         end else begin
           edtPesquisaInicial:=TEdit.Create(painelPesquisasGlobal);
           edtPesquisaInicial.Name:='edtPesInicial' + pesquisasListaGlobal[i].agrupador + inttostr(i);
           edtPesquisaInicial.Text:='';
           edtPesquisaInicial.Parent:=pnlfundo;
           edtPesquisaInicial.Width:=pesquisasListaGlobal[i].tamanho;
           edtPesquisaInicial.Top:=17;
           edtPesquisaInicial.Left:=5;
           edtPesquisaInicial.Tag:=i;
           edtPesquisaInicial.Hint:=pesquisasListaGlobal[i].criterio;
           defineTratamentoImputacao(pesquisasListaGlobal[i].tipoFDB,edtPesquisaInicial);
           edtPesquisaInicial.FreeOnRelease;
           edtPesquisaInicial.SetFocus;

           if pesquisasListaGlobal[i].intervalo='S' then
            begin
             edtPesquisaInicial.Hint:='>=';
             pnlfundo.Width:=pnlfundo.Width * 2;
             edtPesquisaFinal:=TEdit.Create(painelPesquisasGlobal);
             edtPesquisaFinal.Name:='edtPesFinal' + pesquisasListaGlobal[i].agrupador + inttostr(i);
             edtPesquisaFinal.Text:='';
             edtPesquisaFinal.Parent:=pnlfundo;
             edtPesquisaFinal.Width:=pesquisasListaGlobal[i].tamanho;
             edtPesquisaFinal.Top:=17;
             edtPesquisaFinal.Left:=pesquisasListaGlobal[i].tamanho+15;
             edtPesquisaFinal.Tag:=i;
             edtPesquisaFinal.Hint:='<=';
             defineTratamentoImputacao(pesquisasListaGlobal[i].tipoFDB,edtPesquisaFinal);
             edtPesquisaFinal.FreeOnRelease;

           end;
         end;
        end;
     end;
   end;

   if (painelPesquisasGlobal.FindComponent('pnlPesquisar') <> nil) then
     painelPesquisasGlobal.FindComponent('pnlPesquisar').Destroy;

   if (painelPesquisasGlobal.FindComponent('btnPesquisar') <> nil) then
     painelPesquisasGlobal.FindComponent('btnPesquisar').Destroy;

   if (sender is TRadioButton) then
    begin
     rdbtn:= (Sender as TRadioButton);
     if rdbtn.Name='rdbtnMostarTodos' then
      begin
       begin
             TDialogMessage.ShowWaitMessage('Mostrando todos...',
              procedure
              begin
                 executarPesquisa(Self);
              end);
       end;
       //Evitar bug do mostrar todos
       rdbtn.Checked:=true;
       exit;
     end;
   end;


   pnlfundo:=TPanel.Create(painelPesquisasGlobal);
   pnlfundo.Name:='pnlPesquisar';
   pnlfundo.Caption:='';
   pnlfundo.Parent:=painelPesquisasGlobal;
   pnlfundo.Width:=80;
   pnlfundo.BevelOuter:=bvNone;
   pnlfundo.FreeOnRelease;

   btnPesquisar:=TButton.Create(painelPesquisasGlobal);
   btnPesquisar.Caption:='Pesquisar';
   btnPesquisar.Name:='btnPesquisar';
   btnPesquisar.Parent:=pnlfundo;
   btnPesquisar.Top:=16;
   btnPesquisar.Left:=5;
   btnPesquisar.Height:=22;
   btnPesquisar.Width:=70;
   btnPesquisar.OnClick:=executarPesquisa;
   btnPesquisar.FreeOnRelease;

   Application.MainForm.ActiveMDIChild.Perform(WM_nextdlgctl,0,0);
 end;
end;




procedure TFuncoes.criarPesquisas(sqlDataSet: TSQLDataSet; clientDataSet: TClientDataSet; sql, sqlWhere, sqlOrderby: string; painelOpcoes:TFlowPanel; painelPesquisas:TFlowPanel; pesquisasLista: Array of TPesquisas;funcaoPosPesquisa: TNotifyEvent;clientDataSetPesquisa: TClientDataSet);
var
  i: Integer;
  rdbtn: TRadioButton;
  btnMostrarTodos: TButton;
  strAtalho: string;
  //listaAtalhoAlpha: array['a','b','c','d','e','f','g','h','i','j','l','m','n','','','','','','','','','','','','','','',''] of string;

begin
 SetLength(pesquisasListaGlobal,Length(pesquisasLista));

 painelPesquisasGlobal:=painelPesquisas;
 clientDataSetGlobal:=clientDataSet;
 clientDataSetPesquisaGlobal:=clientDataSetPesquisa;
 sqlDataSetGlobal:=sqlDataSet;
 sqlGlobal:=sql;
 sqlWhereGlobal:=sqlWhere;
 sqlOrderbyGlobal:=sqlOrderby;
 funcaoPosPesquisaGlobal:= funcaoPosPesquisa;

 for i := 0 to Length(pesquisasLista) - 1 do
  begin
   if pesquisasLista[i] <> nil then
    begin
     if painelOpcoes.FindComponent( 'rdbtn' + pesquisasLista[i].agrupador)= nil then
      begin
       rdbtn := TRadioButton.Create(painelOpcoes);
//       strAtalho:='&' + chr(65+i) +' - ';
       rdbtn.Caption:= strAtalho + pesquisasLista[i].titulo;
       rdbtn.Width:=((Length(pesquisasLista[i].titulo)* 4)+60);
       rdbtn.Name:= 'rdbtn' + pesquisasLista[i].agrupador;
       rdbtn.Hint:=pesquisasLista[i].agrupador;
       rdbtn.Tag:=i;
       rdbtn.OnClick:=criaPesquisas;
       rdbtn.Parent:=painelOpcoes;
      end;
      pesquisasListaGlobal[i]:=pesquisasLista[i];
    end;
 end;

 if not Assigned(painelOpcoes.FindComponent('rdbtnMostarTodos')) then
 begin
   rdbtn := TRadioButton.Create(painelOpcoes);
   rdbtn.Caption:= 'Mostar &Todos';
   rdbtn.Width:= 100;
   rdbtn.Name:= 'rdbtnMostarTodos' ;
   rdbtn.Hint:='Mostrar todos os registros';
   rdbtn.OnClick:=criaPesquisas;
   rdbtn.Parent:=painelOpcoes;
 end;

end;

procedure TFuncoes.definirMascaraCampos(clientDataSet:TClientDataSet);
var
 i:Integer;
 campo:TObject;
begin
   for i:=0 to clientDataSet.FieldCount-1 do
    begin

     campo:= clientDataSet.Fields[i];

     if (campo is TDateField) then
      begin
       (campo as TDateField).EditMask:='!99/99/9999;1;_';
     end;
     if (campo is TFMTBCDField) then
      begin
       (campo as TFMTBCDField).DisplayFormat:='0.000000';
     end;
     if (campo is TCurrencyField) then
      begin
       (campo as TCurrencyField).DisplayFormat:='R$ ,0.00';
     end;
     if ((campo is TSingleField) and (clientDataSet.Fields[i].Tag=1)) then
      begin
       (campo as TSingleField).DisplayFormat:='0.00%';
     end;

     if (campo is TFloatField)  then
      begin
       (campo as TFloatField).DisplayFormat:='0.000000';
     end;

     if ((campo is TFloatField) and (clientDataSet.Fields[i].Tag=1))  then
      begin
       (campo as TFloatField).DisplayFormat:='R$ ,0.00';
     end;

     if ((campo is TAggregateField) and (clientDataSet.Fields[i].Tag=1))  then
      begin
       (campo as TAggregateField).DisplayFormat:='R$ ,0.00';
     end;

     if ((campo is TFloatField) and (clientDataSet.Fields[i].Tag=2))  then
      begin
       (campo as TFloatField).DisplayFormat:='0.00%';
     end;

     if (campo is TStringField) then
      begin
       if pos('CEP', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='99.999-999;0; ';
       end;
       if pos('FAX', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if pos('CELULAR', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if pos('FONE', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if pos('TELEFONE1', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if pos('TELEFONE2', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if pos('CNPJ', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='99.999.999/0001-99';
       end;
           if pos('CPF', UpperCase((campo as TStringField).FieldName)) > 0 then
        begin
         (campo as TStringField).EditMask:='999.999.999-99;0;';
       end;
       {if (campo as TStringField).DisplayLabel='FONE1' then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if (campo as TStringField).DisplayLabel='FONE2' then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if (campo as TStringField).DisplayLabel='FONE3' then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end;
       if (campo as TStringField).DisplayLabel='FONE4' then
        begin
         (campo as TStringField).EditMask:='(99)9999-999999999;0; ';
       end; }
     end;
   end;
end;

procedure TFuncoes.desabilitar_clientdataset(clientDataSet: TClientDataSet);
begin
   if clientDataSet.State in[dsBrowse]  then
   clientDataSet.DisableControls;
end;

procedure TFuncoes.visualizarRelatorios(relatorio: TfrxReport;exportar_pdf : boolean =false);
begin
 Application.CreateForm(Tfpreview,fpreview);
 relatorio.Preview:=fpreview.frPreview;
 if exportar_pdf then
 fpreview.frPreview.Export(fpreview.frxExportPDF)
 else
 relatorio.ShowReport;
end;

function TFuncoes.XlsToStringGrid(AGrid: TStringGrid;
  AXLSFile: string): Boolean;
    const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;
begin
 Result := False;
  //Cria Excel- OLE Object
  XLApp := CreateOleObject('Excel.Application');
  try
    //Esconde Excel
    XLApp.Visible := False;
    //Abre o Workbook
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    //Pegar o n�mero da �ltima linha
    x := XLApp.ActiveCell.Row;
    //Pegar o n�mero da �ltima coluna
    y := XLApp.ActiveCell.Column;
    //Seta Stringgrid linha e coluna
    AGrid.RowCount := x;
    AGrid.ColCount := y;
    //Associaca a variant WorkSheet com a variant do Delphi
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    //Cria o loop para listar os registros no TStringGrid
    k := 1;
    repeat
      for r := 1 to y do
      begin

        AGrid.Cells[(r - 1), (k - 1)] := RangeMatrix[K, R];

      end;
      Inc(k, 1);
    until k > x;
    RangeMatrix := Unassigned;
  finally
    //Fecha o Excel
    if not VarIsEmpty(XLApp) then
      begin
        XLApp.Quit;
        XLAPP := Unassigned;
        Sheet := Unassigned;
        Result := True;
      end;
  end;
end;

function TFuncoes.exibirLogin(sqlConnection: TSQLConnection; titulo: string; usuario: string) : TStringList;
var sucesso : boolean;
begin
 try
    sucesso := true;
    result  := TStringList.Create;
    if (flogin = nil) then Application.CreateForm(Tflogin,flogin);
    flogin.sqlConnection := sqlConnection;
      flogin.titulo := titulo;
      flogin.ShowModal;
 finally
  result.Add(flogin.edtUsuario.Text);
  result.Add(flogin.cmbprojetos.Text);
 end;

end;

  procedure TFuncoes.exportarGrides(formulario: TForm; gride: TcxGrid);
  var
    FileExt: String;
    SaveDialog: TSaveDialog;
  begin
    try
      try
        SaveDialog :=TSaveDialog.Create(nil);
      SaveDialog.Filter :=
          'Excel (*.xls) |*.xls|XML (*.xml) |*.xml|Arquivo Texto (*.txt) |*.txt|P�gina Web (*.html)|*.html';
        SaveDialog.Title := 'Exportar Dados';
        SaveDialog.DefaultExt := 'xls';

        if SaveDialog.Execute then
        begin
          FileExt := LowerCase(ExtractFileExt(SaveDialog.FileName));
          if FileExt = '.xls' then
            ExportGridToExcel(SaveDialog.FileName, gride, false)
          else if FileExt = '.xml' then
            ExportGridToExcel(SaveDialog.FileName, gride, false)
          else if FileExt = '.txt' then
            ExportGridToExcel(SaveDialog.FileName, gride, false)
          else if FileExt = '.html' then
            ExportGridToExcel(SaveDialog.FileName, gride, false);

          ShellExecute(formulario.Handle, 'open', pchar(SaveDialog.FileName),
            nil, nil, SW_SHOW);
        end;

        if FileExists(SaveDialog.FileName) then
         application.MessageBox('Processo de exporta��o conclu�do com sucesso','Exporta��o.',MB_ICONEXCLAMATION+MB_OK);
      finally
        FreeAndNil(SaveDialog);
      end;

    except
      on e: exception do
      begin
        application.MessageBox(pchar('Processo de exporta��o n�o conclu�do.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
      end;
    end;
  end;

function TFuncoes.fecharconexao(dm: TDataModule): boolean;
var i : integer;
begin
 try
   result  :=true;
    for I := 0 to dm.ComponentCount-1 do
    begin
      if (dm.Components[i] is Tsqlconnection)   then
         (dm.Components[i] as Tsqlconnection).close;
      if (dm.Components[i] is TFDConnection)   then
         (dm.Components[i] as TFDConnection).close;
    end;

 except
  result :=false;
 end;
end;

function TFuncoes.filtrar(clientDataSet: TClientDataSet;
  filter: string): boolean;
begin

     clientDataSet.DisableControls;
      try
        clientDataSet.Filtered := False;
        clientDataSet.FilterOptions := [foNoPartialCompare];
        clientDataSet.Filter :=filter;
        clientDataSet.Filtered := True;
      finally
        clientDataSet.First;
        clientDataSet.EnableControls;
        result:=  clientDataSet.RecordCount>0;

      end;
end;

function TFuncoes.fecharconexao(usuario, ipcomputador: string; conexao : Tsqlconnection): boolean;
var sql:string;
sucesso  : boolean;
begin
  try

   try
     sucesso :=true;
     sql:='update usuarios set conectado='+QuotedStr('N')+' where usuario=' + QuotedStr(usuario)+' and COMPUTADOR_IP='+QuotedStr(ipcomputador);
     conexao.ExecuteDirect(sql);
   except
     sucesso :=false;
   end;
  finally
    result :=sucesso;
  end;

end;

function TFuncoes.GetNomeArquivoConfg(pform: String;tcxgriddbTableView: tcxgriddbTableView): string;
begin
    if not DirectoryExists(cDIRSPOOL + '\spool\' + pform) then
      CreateDir(Pchar(cDIRSPOOL +'\spool\'+pform));

 Result := cDIRSPOOL +'\spool\'+pform+'\'+tcxgriddbTableView.Name + cEXTENSAOGRIDS;
end;

procedure TFuncoes.socketServidorClientRead(Sender: TObject; Socket: TCustomWinSocket);
begin
  if Socket.ReceiveText='FECHAR' then
   begin
    Application.MessageBox('FECHAR!!!', 'Aten��o', MB_OK+MB_ICONEXCLAMATION);
  end;

  if Socket.ReceiveText='OK' then
   begin
    Application.MessageBox('OK!!!', 'Aten��o', MB_OK+MB_ICONEXCLAMATION);
  end;
end;

procedure TFuncoes.socketServidorInicia(porta: Integer);
begin
  socketServidor:=TServerSocket.Create(Self);
  socketServidor.Port:= porta;
  socketServidor.OnClientRead:=socketServidorClientRead;
  socketServidor.Active:=True;
end;

procedure TFuncoes.socketServidorFinaliza();
begin
  socketServidor.Free;
  socketServidor.Destroy;
end;

{ TCustomeditHelper }

function TCustomeditHelper.IsEmpty: boolean;
begin
 result := Trim(Self.Text) = EmptyStr;
end;

procedure TFuncoes.Menu(Sender: TObject; PopupOpcao: TPopupMenu);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    PopupOpcao.Popup(X, Y);
  end;
end;

procedure TFuncoes.Relatorio(Sender: TObject; frxrelatorio: TfrxReport; cdsrelatorio: TClientDataSet );
begin

  if not cdsrelatorio.IsEmpty then
 	visualizarRelatorios(frxrelatorio)
  else
  application.MessageBox('Sem registros para impress�o','Impress�o de relat�rio.',MB_OK+64);
end;

end.
