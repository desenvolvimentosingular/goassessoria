unit udm;

interface

uses
SysUtils, Classes, WideStrings, DB, SqlExpr, Data.DBXFirebird, Data.FMTBcd, Datasnap.Provider,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.Comp.ScriptCommands, FireDAC.Stan.Util, FireDAC.Comp.Script, cxStyles,
  cxClasses, Datasnap.DBClient, frxClass, frxDBSet, cxLocalization,
  FireDAC.Phys.Oracle, FireDAC.Phys.OracleDef, Vcl.ExtCtrls, frxExportPDF;

type
   Tdm = class(TDataModule)
   conexao: TSQLConnection;
    fQuery: TSQLDataSet;
    FDConexao: TFDConnection;
    FDScript: TFDScript;
    FDQuery: TFDQuery;
    cxStyleRepository: TcxStyleRepository;
    clGradientActiveCaption_singular: TcxStyle;
    Silver_singular: TcxStyle;
    sqlempresa: TSQLDataSet;
    dspempresa: TDataSetProvider;
    cdsempresa: TClientDataSet;
    cdsempresaID_EMPRESA: TIntegerField;
    cdsempresaRAZAO_SOCIAL: TStringField;
    cdsempresaNOME_FANTASIA: TStringField;
    cdsempresaRESPONSAVEL: TStringField;
    cdsempresaCNPJ: TStringField;
    cdsempresaIE: TStringField;
    cdsempresaENDERECO: TStringField;
    cdsempresaCOMPLEMENTO: TStringField;
    cdsempresaBAIRRO: TStringField;
    cdsempresaCIDADE: TStringField;
    cdsempresaUF: TStringField;
    cdsempresaCEP: TStringField;
    cdsempresaEMAIL: TStringField;
    cdsempresaSITE: TStringField;
    cdsempresaFONE1: TStringField;
    cdsempresaFONE2: TStringField;
    cdsempresaFAX: TStringField;
    dsempresa: TDataSource;
    cdsempresaLOGO_IMG: TBlobField;
    frxDBempresa: TfrxDBDataset;
    clWindow_singular: TcxStyle;
    cxLocalizer: TcxLocalizer;
    silverfraco_singular: TcxStyle;
    silver_fraco_singular: TcxStyle;
    clfootercalculado: TcxStyle;
    FDParametros: TFDQuery;
    FDParametrosID_PROJETO: TIntegerField;
    FDParametrosPROJETO: TStringField;
    FDParametrosID_FILIAL: TIntegerField;
    FDParametrosFILIAL: TStringField;
    FDParametrosCOALESCE: TIntegerField;
    FDParametrosFUNDO: TStringField;
    FDParametrosPARAMETRO: TStringField;
    FDParametrosVALOR: TStringField;
    FDParametrosTIPO: TStringField;
    tmconexao: TTimer;
    FDRelatorios: TFDQuery;
    FDRelatoriosID: TIntegerField;
    FDRelatoriosDT_CADASTRO: TDateField;
    FDRelatoriosHS_CADASTRO: TTimeField;
    FDRelatoriosDESCRICAO: TStringField;
    FDRelatoriosEMAIL: TStringField;
    FDRelatoriosCODADM: TIntegerField;
    FDRelatoriosARQUIVO: TMemoField;
    FDRelatoriosDESCRICAOI: TStringField;
    FDConSaveCloud: TFDConnection;
    FDSaveCloud: TFDQuery;
    FDreplicador: TFDQuery;
    FDreplicadorover: TFDQuery;
    FDreplicadorlocal: TFDQuery;
    FDRecebivelFundo: TFDQuery;
    frxdbRecebivelFundo: TfrxDBDataset;
    frxdbRecebivelProjeto: TfrxDBDataset;
    FDRecebivelProjeto: TFDQuery;
    dsRecebivelFundo: TDataSource;
    frxExportPDF: TfrxPDFExport;
    frxanaliseportadorprojeto: TfrxReport;
    frxgodisponibilidade: TfrxReport;
    frxDBgodisponibilidade: TfrxDBDataset;
    FDDisponibilidade: TFDQuery;
    FDDisponibilidadeID: TIntegerField;
    FDDisponibilidadeDT_CADASTRO: TDateField;
    FDDisponibilidadeHS_CADASTRO: TTimeField;
    FDDisponibilidadeDTCOMPETENCIA: TDateField;
    FDDisponibilidadeVLFOMENTOMP: TFloatField;
    FDDisponibilidadeVLMOVIMENTODIA: TFloatField;
    FDDisponibilidadeVLCAIXAGERAL: TFloatField;
    FDDisponibilidadeVLBANCOCONTACORRENTE: TFloatField;
    FDDisponibilidadeVLCHEQUES: TFloatField;
    FDDisponibilidadeVLDUPLICATAS: TFloatField;
    FDDisponibilidadeVLAVISTA: TFloatField;
    FDDisponibilidadeVLCARTEIRASIMPLES: TFloatField;
    FDDisponibilidadeVLESTMPCOMPRA: TFloatField;
    FDDisponibilidadeVLESTMPCOMPRACURVAA: TFloatField;
    FDDisponibilidadeVLESTMPCOMPRACURVAB: TFloatField;
    FDDisponibilidadeVLESTMPCOMPRACURVAC: TFloatField;
    FDDisponibilidadeVLESTMPSEMGIRO: TFloatField;
    FDDisponibilidadeVLESTEMCURVAA: TFloatField;
    FDDisponibilidadeVLESTEMCURVAB: TFloatField;
    FDDisponibilidadeVLESTEMCURVAC: TFloatField;
    FDDisponibilidadeVLESTEMSEMGIRO: TFloatField;
    FDDisponibilidadeVLESTPACURVAA: TFloatField;
    FDDisponibilidadeVLESTPACURVAB: TFloatField;
    FDDisponibilidadeVLESTPAFORALINHA: TFloatField;
    FDDisponibilidadeVLESTPACURVANICHO: TFloatField;
    FDDisponibilidadeVLPAESTLOWPRICE: TFloatField;
    FDDisponibilidadeVLESTPATERCEIRO: TFloatField;
    FDDisponibilidadeVLLANCAMENTO_PROJETO: TFloatField;
    FDDisponibilidadeVLESTTRANSITO: TFloatField;
    FDDisponibilidadeVLCONTAPAGARVENCIDO: TFloatField;
    FDDisponibilidadeVLIMPOSTOSVENCIDOS: TFloatField;
    FDDisponibilidadeVLCONTASPAGAR30: TFloatField;
    FDDisponibilidadeVLCONTASPAGAR60: TFloatField;
    FDDisponibilidadeVLATIVOS: TFloatField;
    FDDisponibilidadeCODPROJETO: TIntegerField;
    FDDisponibilidadePROJETO: TStringField;
    FDDisponibilidadeCODFILIAL: TIntegerField;
    FDDisponibilidadeFILIAL: TStringField;
    FDDisponibilidadeVLTOTALDIVIDASCURTOPRAZO: TCurrencyField;
    FDDisponibilidadeVLTOTALCREDITOCURTOPRAZO: TCurrencyField;
    FDDisponibilidadeVLTOTALSALDOLIQUIDO1: TCurrencyField;
    FDDisponibilidadeVLTOTALESTOQUES: TCurrencyField;
    FDDisponibilidadeVLTOTALSALDOLIQUIDO2: TCurrencyField;
    FDDisponibilidadeVLTOTALCONTASPAGAR: TCurrencyField;
    FDDisponibilidadeVLTOTALSALDOLIQUIDO3: TCurrencyField;
    FDDisponibilidadeVLTOTALNEGOCIACOESPAGAR: TCurrencyField;
    FDDisponibilidadeVLTOTALSALDOLIQUIDO4: TCurrencyField;
    FDDisponibilidadeVLSALARIOSPJS: TFloatField;
    FDDisponibilidadeVLESTPACURVAC: TFloatField;
    FDDisponibilidadeVLCONTASPAGARACIMA60: TFloatField;
    FDDisponibilidadeVLRENEGOCIACOES: TFloatField;
    FDProjetosDisponibilidade: TFDQuery;
    FDProjetosDisponibilidadeCODPROJETO: TIntegerField;
    FDProjetosDisponibilidadeDTCOMPETENCIA: TDateField;
    FDRecebivel: TFDQuery;
    frxdbrecebivel: TfrxDBDataset;
    frxanaliseportador: TfrxReport;
    frxanaliseprojeto: TfrxReport;
    FDRecebivelProjetos: TFDQuery;
    FrxdbRecebivelProjetos: TfrxDBDataset;
    FDRecebivelFundos: TFDQuery;
    FDRecebivelFundosTOTAL_BORDERO: TIntegerField;
    FDRecebivelFundosQTTITULOS: TLargeintField;
    FDRecebivelFundosVLMEDIORECEBIVEIS: TFloatField;
    FDRecebivelFundosPM_EMPRESA: TFloatField;
    FDRecebivelFundosVLBRUTOBORDERO: TFloatField;
    FDRecebivelFundosVLLIQUIDOCALCULADO: TFloatField;
    FDRecebivelFundosDESPESAS_ENCARGOS: TFloatField;
    FrxdbRecebivelFundos: TfrxDBDataset;
    FDRecebivelFundosTAXA_NOMINAL: TStringField;
    FDRecebivelFundosCUSTOEFETIVO: TStringField;
    FDRecebivelFundoCODPORTADOR: TIntegerField;
    FDRecebivelFundoPORTADOR: TStringField;
    FDRecebivelFundoTOTAL_BORDERO: TIntegerField;
    FDRecebivelFundoQTTITULOS: TLargeintField;
    FDRecebivelFundoVLMEDIORECEBIVEIS: TFloatField;
    FDRecebivelFundoPM_EMPRESA: TFloatField;
    FDRecebivelFundoVLBRUTOBORDERO: TFloatField;
    FDRecebivelFundoVLLIQUIDOCALCULADO: TFloatField;
    FDRecebivelFundoDESPESAS_ENCARGOS: TFloatField;
    FDRecebivelFundoTAXA_NOMINAL: TStringField;
    FDRecebivelFundoCUSTOEFETIVO: TStringField;
    FDRecebivelProjetoCODPROJETO: TIntegerField;
    FDRecebivelProjetoPROJETO: TStringField;
    FDRecebivelProjetoTOTAL_BORDERO: TIntegerField;
    FDRecebivelProjetoQTTITULOS: TLargeintField;
    FDRecebivelProjetoVLMEDIORECEBIVEIS: TFloatField;
    FDRecebivelProjetoPM_EMPRESA: TFloatField;
    FDRecebivelProjetoVLBRUTOBORDERO: TFloatField;
    FDRecebivelProjetoVLLIQUIDOCALCULADO: TFloatField;
    FDRecebivelProjetoDESPESAS_ENCARGOS: TFloatField;
    FDRecebivelProjetoTAXA_NOMINAL: TStringField;
    FDRecebivelProjetoCUSTOEFETIVO: TStringField;
    FDRecebivelTOTAL_BORDERO: TIntegerField;
    FDRecebivelQTTITULOS: TLargeintField;
    FDRecebivelVLMEDIORECEBIVEIS: TFloatField;
    FDRecebivelPM_EMPRESA: TFloatField;
    FDRecebivelVLBRUTOBORDERO: TFloatField;
    FDRecebivelVLLIQUIDOCALCULADO: TFloatField;
    FDRecebivelDESPESAS_ENCARGOS: TFloatField;
    FDRecebivelTAXA_NOMINAL: TStringField;
    FDRecebivelCUSTOEFETIVO: TStringField;
    FDRecebivelProjetosCODPROJETO: TIntegerField;
    FDRecebivelProjetosPROJETO: TStringField;
    FDRecebivelProjetosTOTAL_BORDERO: TIntegerField;
    FDRecebivelProjetosQTTITULOS: TLargeintField;
    FDRecebivelProjetosVLMEDIORECEBIVEIS: TFloatField;
    FDRecebivelProjetosPM_EMPRESA: TFloatField;
    FDRecebivelProjetosVLBRUTOBORDERO: TFloatField;
    FDRecebivelProjetosVLLIQUIDOCALCULADO: TFloatField;
    FDRecebivelProjetosDESPESAS_ENCARGOS: TFloatField;
    FDRecebivelProjetosTAXA_NOMINAL: TStringField;
    FDRecebivelProjetosCUSTOEFETIVO: TStringField;
    frxgofaturamentogrupo: TfrxReport;
    frxDBgofaturamentogrupo: TfrxDBDataset;
    FDFaturamentoGrupo: TFDQuery;
    FDFaturamentoGrupoCODPROJETO: TIntegerField;
    FDFaturamentoGrupoPROJETO: TStringField;
    FDFaturamentoGrupoCODFILIAL: TIntegerField;
    FDFaturamentoGrupoFILIAL: TStringField;
    FDFaturamentoGrupoDT_LANCAMENTO: TDateField;
    FDFaturamentoGrupoDIA: TStringField;
    FDFaturamentoGrupoDIA_SEMANA: TStringField;
    FDFaturamentoGrupoDESCRICAO_DIA_UTIL: TStringField;
    FDFaturamentoGrupoGRUPO_PRODUTO: TStringField;
    FDFaturamentoGrupoALIQUOTA: TFloatField;
    FDFaturamentoGrupoVLFATURADO: TFloatField;
    FDFaturamentoGrupoTOTAL_UTEIS_YERSTEDAY: TIntegerField;
    FDFaturamentoGrupoTOTAL_UTEIS_TOMORROW: TIntegerField;
    FDFaturamentoGrupoMETA: TFloatField;
    dspFaturamentoGrupo: TDataSetProvider;
    CDSFaturamentoGrupo: TClientDataSet;
    CDSFaturamentoGrupoCODPROJETO: TIntegerField;
    CDSFaturamentoGrupoPROJETO: TStringField;
    CDSFaturamentoGrupoCODFILIAL: TIntegerField;
    CDSFaturamentoGrupoFILIAL: TStringField;
    CDSFaturamentoGrupoDT_LANCAMENTO: TDateField;
    CDSFaturamentoGrupoDIA: TStringField;
    CDSFaturamentoGrupoDIA_SEMANA: TStringField;
    CDSFaturamentoGrupoDESCRICAO_DIA_UTIL: TStringField;
    CDSFaturamentoGrupoGRUPO_PRODUTO: TStringField;
    CDSFaturamentoGrupoALIQUOTA: TFloatField;
    CDSFaturamentoGrupoVLFATURADO: TFloatField;
    CDSFaturamentoGrupoTOTAL_UTEIS_YERSTEDAY: TIntegerField;
    CDSFaturamentoGrupoTOTAL_UTEIS_TOMORROW: TIntegerField;
    CDSFaturamentoGrupoMETA: TFloatField;
    FDQuerygoprojetofilial: TFDQuery;
    FDQuerygoprojetofilialCODPROJETO: TIntegerField;
    FDQuerygoprojetofilialCODFILIAL: TIntegerField;
    FDQuerygoprojetofilialPROJETO: TStringField;
    FDQuerygoprojetofilialFILIAL: TStringField;
    frxbase: TfrxReport;
    frxDBgopainelrisco: TfrxDBDataset;
    cdsgopainelrisco: TClientDataSet;
    cdsgopainelriscoID: TIntegerField;
    cdsgopainelriscoDT_CADASTRO: TDateField;
    cdsgopainelriscoHS_CADASTRO: TTimeField;
    cdsgopainelriscoDESCRICAO: TStringField;
    cdsgopainelriscoCODPROJETO: TIntegerField;
    cdsgopainelriscoPROJETO: TStringField;
    cdsgopainelriscoCODFILIAL: TIntegerField;
    cdsgopainelriscoFILIAL: TStringField;
    cdsgopainelriscoVLIMITE: TFloatField;
    cdsgopainelriscoDATA: TDateField;
    cdsgopainelriscoVLVENCIDOS: TFloatField;
    cdsgopainelriscoVLVENCER: TFloatField;
    cdsgopainelriscoCODPORTADOR: TIntegerField;
    cdsgopainelriscoPORTADOR: TStringField;
    cdsgopainelriscoVLIMITEDISPONIVEL: TFloatField;
    cdsgopainelriscoVLIMITEUTILIZADO: TFloatField;
    cdsgopainelriscoVLLIMITEDESCONTO: TFloatField;
    cdsgopainelriscoVLVENCIDOSDESCONTO: TFloatField;
    cdsgopainelriscoVLVENCERDESCONTO: TFloatField;
    cdsgopainelriscoVLLIMITECOMISSSARIA: TFloatField;
    cdsgopainelriscoVLVENCIDOSCOMISSARIA: TFloatField;
    cdsgopainelriscoVLVENCERCOMISSARIA: TFloatField;
    cdsgopainelriscoVLIMITEUTILIZADODESCONTO: TFloatField;
    cdsgopainelriscoVLIMITEDISPONIVELDESCONTO: TFloatField;
    cdsgopainelriscoVLIMITEUTILIZADOCOMISSARIA: TFloatField;
    cdsgopainelriscoVLIMITEDISPONIVELCOMISSARIA: TFloatField;
    cdsgopainelriscoAGRUPADOR: TStringField;
    cdsgopainelriscoselecionado: TBooleanField;
    FDpainelrisco: TFDQuery;
    FDpainelriscoID: TIntegerField;
    FDpainelriscoDT_CADASTRO: TDateField;
    FDpainelriscoHS_CADASTRO: TTimeField;
    FDpainelriscoDATA: TDateField;
    FDpainelriscoDESCRICAO: TStringField;
    FDpainelriscoCODPROJETO: TIntegerField;
    FDpainelriscoCODPORTADOR: TIntegerField;
    FDpainelriscoPORTADOR: TStringField;
    FDpainelriscoPROJETO: TStringField;
    FDpainelriscoCODFILIAL: TIntegerField;
    FDpainelriscoFILIAL: TStringField;
    FDpainelriscoVLIMITE: TFloatField;
    FDpainelriscoVLVENCIDOS: TFloatField;
    FDpainelriscoVLVENCER: TFloatField;
    FDpainelriscoVLLIMITEDESCONTO: TFloatField;
    FDpainelriscoVLVENCIDOSDESCONTO: TFloatField;
    FDpainelriscoVLVENCERDESCONTO: TFloatField;
    FDpainelriscoVLLIMITECOMISSSARIA: TFloatField;
    FDpainelriscoVLVENCIDOSCOMISSARIA: TFloatField;
    FDpainelriscoVLVENCERCOMISSARIA: TFloatField;
    FDpainelriscoREPLICADOR: TStringField;
    dspgopainelrisco: TDataSetProvider;
    procedure DataModuleCreate(Sender: TObject);
    procedure tmconexaoTimer(Sender: TObject);
    procedure FDRecebivelFundoAfterScroll(DataSet: TDataSet);
    procedure FDDisponibilidadeCalcFields(DataSet: TDataSet);
    procedure cdsgopainelriscoCalcFields(DataSet: TDataSet);
private

{ Private declarations }

public
{ Public declarations }
var cor : boolean;

end;

var
dm: Tdm;

implementation

uses
  Vcl.Forms, System.IniFiles, Vcl.Graphics;

{$R *.dfm}

procedure Tdm.cdsgopainelriscoCalcFields(DataSet: TDataSet);
begin
  cdsgopainelriscoVLIMITEUTILIZADO.AsString := FloatToStr((cdsgopainelriscoVLVENCIDOS.AsFloat + cdsgopainelriscoVLVENCER.AsFloat));
  cdsgopainelriscoVLIMITEDISPONIVEL.AsFloat := (cdsgopainelriscoVLIMITE.AsFloat - cdsgopainelriscoVLIMITEUTILIZADO.AsFloat);

  cdsgopainelriscoVLIMITEUTILIZADODESCONTO.AsString := FloatToStr((cdsgopainelriscoVLVENCIDOSDESCONTO.AsFloat + cdsgopainelriscoVLVENCERDESCONTO.AsFloat));
  cdsgopainelriscoVLIMITEDISPONIVELDESCONTO.AsFloat := (cdsgopainelriscoVLLIMITEDESCONTO.AsFloat - cdsgopainelriscoVLIMITEUTILIZADODESCONTO.AsFloat);

  cdsgopainelriscoVLIMITEUTILIZADOCOMISSARIA.AsString := FloatToStr((cdsgopainelriscoVLVENCIDOSCOMISSARIA.AsFloat + cdsgopainelriscoVLVENCERCOMISSARIA.AsFloat));
  cdsgopainelriscoVLIMITEDISPONIVELCOMISSARIA.AsFloat := (cdsgopainelriscoVLLIMITECOMISSSARIA.AsFloat - cdsgopainelriscoVLIMITEUTILIZADOCOMISSARIA.AsFloat);

 cdsgopainelriscoAGRUPADOR.AsString:=   cdsgopainelriscoCODPROJETO.AsString+'-'+ cdsgopainelriscoDATA.AsString;

end;

procedure Tdm.DataModuleCreate(Sender: TObject);
var arquivoini: tinifile;
    caminho_banco  : string;
begin

  try


       // arquivoini      :=Tinifile.Create('C:\Users\singular\Documents\projetos\goassessoria\parametros.ini');
        arquivoini      :=Tinifile.Create(GetCurrentDir+'\parametros.ini');
        caminho_banco   :=arquivoini.ReadString('local1','propriedade1','Erro ao ler par�metro');

        fdConexao.Params.Values['Database']  :=  caminho_banco+'\goassessoria.fdb';
        fdConexao.Params.Values['User_name'] := 'sysdba';
        fdConexao.Params.Values['password']  := 'masterkey';

        conexao.Params.Values['Database']    :=  caminho_banco+'\goassessoria.fdb';
        conexao.Params.Values['User_name']   := 'sysdba';
        conexao.Params.Values['password']    := 'masterkey';


    except
     on e : exception do
     begin
        raise Exception.Create(e.Message)
     end;
  end;


end;

procedure Tdm.FDDisponibilidadeCalcFields(DataSet: TDataSet);
begin
  {Dividas Curto Prazo}
  fddisponibilidadeVLTOTALDIVIDASCURTOPRAZO.AsFloat := (
    fddisponibilidadeVLMOVIMENTODIA.AsFloat +
    fddisponibilidadeVLFOMENTOMP.AsFloat
  );

  {Creditos Curto Prazo}
  fddisponibilidadeVLTOTALCREDITOCURTOPRAZO.AsFloat :=  (
    fddisponibilidadeVLCAIXAGERAL.AsFloat +
    fddisponibilidadeVLBANCOCONTACORRENTE.AsFloat +
    fddisponibilidadeVLCHEQUES.AsFloat +
    fddisponibilidadeVLDUPLICATAS.AsFloat +
    fddisponibilidadeVLAVISTA.AsFloat +
    fddisponibilidadeVLCARTEIRASIMPLES.AsFloat
  );

  {Saldo Liquido 1}
  fddisponibilidadeVLTOTALSALDOLIQUIDO1.AsFloat := (
    fddisponibilidadeVLTOTALCREDITOCURTOPRAZO.AsFloat -
    fddisponibilidadeVLTOTALDIVIDASCURTOPRAZO.AsFloat
  );

  {Estoques}
  fddisponibilidadeVLTOTALESTOQUES.AsFloat :=  (
    fddisponibilidadeVLESTMPCOMPRA.AsFloat +
    fddisponibilidadeVLESTMPCOMPRACURVAA.AsFloat +
    fddisponibilidadeVLESTMPCOMPRACURVAB.AsFloat +
    fddisponibilidadeVLESTMPCOMPRACURVAC.AsFloat +
    fddisponibilidadeVLESTMPSEMGIRO.AsFloat +
    fddisponibilidadeVLESTEMCURVAA.AsFloat +
    fddisponibilidadeVLESTEMCURVAB.AsFloat +
    fddisponibilidadeVLESTEMCURVAC.AsFloat +
    fddisponibilidadeVLESTEMSEMGIRO.AsFloat +
    fddisponibilidadeVLESTPACURVAA.AsFloat +
    fddisponibilidadeVLESTPACURVAB.AsFloat +
    fddisponibilidadeVLESTPACURVAC.AsFloat +
    fddisponibilidadeVLESTPAFORALINHA.AsFloat +
    fddisponibilidadeVLESTPACURVANICHO.AsFloat +
    fddisponibilidadeVLPAESTLOWPRICE.AsFloat +
    fddisponibilidadeVLESTPATERCEIRO.AsFloat +
    fddisponibilidadeVLLANCAMENTO_PROJETO.AsFloat +
    fddisponibilidadeVLESTTRANSITO.AsFloat
  );

  {Saldo Liquido 2}
  fddisponibilidadeVLTOTALSALDOLIQUIDO2.AsFloat := (
    fddisponibilidadeVLTOTALSALDOLIQUIDO1.AsFloat +
    fddisponibilidadeVLTOTALESTOQUES.AsFloat
  );

  {Contas a Pagar}
  fddisponibilidadeVLTOTALCONTASPAGAR.AsFloat :=  (
    fddisponibilidadeVLSALARIOSPJS.AsFloat +
    fddisponibilidadeVLCONTAPAGARVENCIDO.AsFloat +
    fddisponibilidadeVLIMPOSTOSVENCIDOS.AsFloat +
    fddisponibilidadeVLCONTASPAGAR30.AsFloat +
    fddisponibilidadeVLCONTASPAGAR60.AsFloat +
    fddisponibilidadeVLCONTASPAGARACIMA60.AsFloat);

  {Saldo Liquido 3}
  fddisponibilidadeVLTOTALSALDOLIQUIDO3.AsFloat := (
    fddisponibilidadeVLTOTALSALDOLIQUIDO2.AsFloat -
    fddisponibilidadeVLTOTALCONTASPAGAR.AsFloat
  );

  {Total Renegociacoes a Pagar}
  fddisponibilidadeVLTOTALNEGOCIACOESPAGAR.AsFloat := fddisponibilidadeVLRENEGOCIACOES.AsFloat;
  {Saldo Liquido 4}
  fddisponibilidadeVLTOTALSALDOLIQUIDO4.AsFloat := ((fddisponibilidadeVLTOTALSALDOLIQUIDO3.AsFloat + fddisponibilidadeVLATIVOS.AsFloat) -  fddisponibilidadeVLTOTALNEGOCIACOESPAGAR.AsFloat);

end;

procedure Tdm.FDRecebivelFundoAfterScroll(DataSet: TDataSet);
begin
  FDRecebivelProjeto.close;
  FDRecebivelProjeto.ParamByName('codportador').AsInteger:= FDRecebivelFundoCODPORTADOR.AsInteger;
  FDRecebivelProjeto.open;
end;

procedure Tdm.tmconexaoTimer(Sender: TObject);
begin
   if not conexao.InTransaction then
   begin
     conexao.Close;
     conexao.Connected:= true;
     FDConexao.Close;
     FDConexao.Connected:= true;
   end;

end;

end.
