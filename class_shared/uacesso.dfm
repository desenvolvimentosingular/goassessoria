object facesso: Tfacesso
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Acesso Supervisionado'
  ClientHeight = 133
  ClientWidth = 291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object gpxdados: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    BiDiMode = bdLeftToRight
    PanelStyle.Active = True
    ParentBackground = False
    ParentBiDiMode = False
    RedrawOnResize = False
    Style.BorderStyle = ebsOffice11
    Style.Edges = [bLeft, bRight, bBottom]
    Style.Shadow = False
    Style.TransparentBorder = False
    StyleDisabled.BorderStyle = ebsOffice11
    TabOrder = 0
    Height = 133
    Width = 291
    object pnlTitulo: TPanel
      Left = 2
      Top = 2
      Width = 287
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Color = 11503182
      ParentBackground = False
      TabOrder = 0
      OnMouseDown = pnlTituloMouseDown
      object lblSite: TLabel
        Left = 177
        Top = 0
        Width = 110
        Height = 11
        Align = alRight
        Alignment = taCenter
        Caption = ' www.gosoftware.com.br '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
      object lblTitulo: TLabel
        Left = -2
        Top = 11
        Width = 154
        Height = 16
        Caption = ' Acesso Supervisionado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object pnlLayoutLinhaspermissao1: TPanel
      Left = 2
      Top = 43
      Width = 287
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object pnlLayoutCampospermissaoid: TPanel
        Left = 0
        Top = 0
        Width = 70
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Usu'#225'rio'
        TabOrder = 1
      end
      object pnlLayoutLinhaspermissaolinha1: TPanel
        Left = 70
        Top = 0
        Width = 400
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 0
        object dbepermissao_usuarioUSUARIO: TEdit
          Left = 5
          Top = 3
          Width = 189
          Height = 21
          Hint = 'Usu'#225'rio'
          AutoSelect = False
          AutoSize = False
          BiDiMode = bdLeftToRight
          CharCase = ecUpperCase
          Color = 15129292
          MaxLength = 30
          ParentBiDiMode = False
          TabOrder = 0
          OnExit = dbepermissao_usuarioUSUARIOExit
        end
      end
    end
    object pnlLayoutLinhaspermissao2: TPanel
      Left = 2
      Top = 73
      Width = 287
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object pnlLayoutCampospermissaodestino: TPanel
        Left = 0
        Top = 0
        Width = 70
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Senha'
        TabOrder = 0
      end
      object pnlLayoutLinhaspermissaolinha2: TPanel
        Left = 70
        Top = 0
        Width = 400
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 1
        object dbepermissao_usuarioSENHA: TEdit
          Left = 5
          Top = 3
          Width = 189
          Height = 21
          Hint = 'C'#243'digo Usu'#225'rio'
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          Color = 15129292
          MaxLength = 30
          PasswordChar = '*'
          TabOrder = 0
          OnExit = dbepermissao_usuarioSENHAExit
        end
      end
    end
    object spdcancelar: TButton
      Left = 204
      Top = 108
      Width = 60
      Height = 22
      Hint = 'Cancelar'
      Caption = 'Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = spdcancelarClick
    end
    object spdconfirmar: TButton
      Left = 138
      Top = 108
      Width = 60
      Height = 22
      Hint = 'Confirmar'
      Caption = 'Confirmar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = spdconfirmarClick
    end
  end
end
