unit uaviso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxImage, dxGDIPlusClasses,ClipBrd, dxSkinsCore, dxSkinSevenClassic;

type
  TFaviso = class(TForm)
    pnlbase: TPanel;
    cxm: TcxImage;
    flwpbase: TFlowPanel;
    pnlcontroles: TPanel;
    spdok: TButton;
    pnlLinhaTitulo: TPanel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    pnlLogo: TPanel;
    spdfechar: TSpeedButton;
    lblSistema: TLabel;
    procedure spdfecharClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spdokClick(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Faviso: TFaviso;

implementation

{$R *.dfm}

procedure TFaviso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Faviso:=nil;
	Action:=cafree;
end;

procedure TFaviso.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TFaviso.FormShow(Sender: TObject);
begin
 spdok.SetFocus;
end;

procedure TFaviso.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TFaviso.spdokClick(Sender: TObject);
begin
 Clipboard.astext := flwpbase.Caption;
 ModalResult := mrOK;
end;

procedure TFaviso.spdfecharClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

end.
