object fclonarpermissao: Tfclonarpermissao
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 151
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gpxdados: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    BiDiMode = bdLeftToRight
    PanelStyle.Active = True
    ParentBackground = False
    ParentBiDiMode = False
    RedrawOnResize = False
    Style.BorderStyle = ebsOffice11
    Style.Edges = [bLeft, bRight, bBottom]
    Style.LookAndFeel.NativeStyle = False
    Style.Shadow = False
    Style.TransparentBorder = False
    StyleDisabled.BorderStyle = ebsOffice11
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 0
    Height = 151
    Width = 431
    object pnlTitulo: TPanel
      Left = 1
      Top = 0
      Width = 429
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Color = 11503182
      ParentBackground = False
      TabOrder = 0
      OnMouseDown = pnlTituloMouseDown
      object lblTitulo: TLabel
        Left = 0
        Top = 9
        Width = 266
        Height = 16
        Caption = '  Permiss'#227'o>>Clonar permiss'#245'es usuario'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblSite: TLabel
        Left = 319
        Top = 0
        Width = 110
        Height = 41
        Align = alRight
        Alignment = taCenter
        Caption = ' www.gosoftware.com.br '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
        ExplicitHeight = 11
      end
    end
    object pnlLayoutLinhaspermissao1: TPanel
      Left = 1
      Top = 41
      Width = 429
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object pnlLayoutCampospermissaoid: TPanel
        Left = 0
        Top = 0
        Width = 70
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  De Usu'#225'rio'
        TabOrder = 0
      end
      object pnlLayoutLinhaspermissaolinha1: TPanel
        Left = 70
        Top = 0
        Width = 400
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 1
        object btnpesquisarusuario: TButton
          Left = 79
          Top = 3
          Width = 20
          Height = 20
          Caption = '...'
          TabOrder = 1
        end
        object dbepermissao_usuarionome: TEdit
          Left = 105
          Top = 3
          Width = 250
          Height = 21
          Hint = 'Nome Usu'#225'rio'
          Enabled = False
          TabOrder = 2
        end
        object dbepermissao_usuarioID_USUARIO: TEdit
          Left = 5
          Top = 3
          Width = 72
          Height = 21
          Hint = 'C'#243'digo Usu'#225'rio'
          Alignment = taRightJustify
          Color = 15129292
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 0
          OnKeyPress = dbepermissao_usuarioID_USUARIOKeyPress
        end
      end
    end
    object pnlLayoutLinhaspermissao2: TPanel
      Left = 1
      Top = 71
      Width = 429
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object pnlLayoutCampospermissaodestino: TPanel
        Left = 0
        Top = 0
        Width = 70
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Para Usu'#225'rio'
        TabOrder = 0
      end
      object pnlLayoutLinhaspermissaolinha2: TPanel
        Left = 70
        Top = 0
        Width = 400
        Height = 30
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 1
        object btnpesquisarusuariodestino: TButton
          Left = 79
          Top = 3
          Width = 20
          Height = 20
          Caption = '...'
          TabOrder = 0
        end
        object dbepermissao_usuarionome_destino: TEdit
          Left = 105
          Top = 3
          Width = 250
          Height = 21
          Hint = 'Nome Usu'#225'rio'
          Enabled = False
          TabOrder = 2
        end
        object dbepermissao_usuarioID_USUARIO_DESTINO: TEdit
          Left = 5
          Top = 3
          Width = 72
          Height = 21
          Hint = 'C'#243'digo Usu'#225'rio'
          Alignment = taRightJustify
          Color = 15129292
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 1
          OnKeyPress = dbepermissao_usuarioID_USUARIO_DESTINOKeyPress
        end
      end
    end
    object spdcancelar: TButton
      Left = 299
      Top = 126
      Width = 60
      Height = 22
      Hint = 'Cancelar'
      Caption = 'Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = spdcancelarClick
    end
    object spdconfirmar: TButton
      Left = 362
      Top = 126
      Width = 60
      Height = 22
      Hint = 'Confirmar'
      Caption = 'Confirmar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = spdconfirmarClick
    end
  end
  object pesqusuario1: TPesquisa
    Colunas = <
      item
        Expanded = False
        FieldName = 'ID_USUARIO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Descri'#231#227'o rotina'
        Width = 200
        Visible = True
      end>
    CampoResultado = 'ID_USUARIO'
    CampoDescricao = 'NOME'
    SQL.Strings = (
      'select id_usuario, nome from gousuarios where 1=1')
    MultiSelecao = False
    Conexao = dm.conexao
    AbrirAuto = False
    PesquisaPadrao = 1
    Filtros = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptUnknown
        Value = 'C'#243'digo'
      end
      item
        DataType = ftWideString
        Name = 'NOME'
        ParamType = ptUnknown
        Value = 'Nome'
      end>
    EditCodigo = dbepermissao_usuarioID_USUARIO
    EditDescricao = dbepermissao_usuarionome
    BotaoPesquisa = btnpesquisarusuario
    Left = 171
    Top = 43
  end
  object pesqusuario2: TPesquisa
    Colunas = <
      item
        Expanded = False
        FieldName = 'ID_USUARIO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Descri'#231#227'o rotina'
        Width = 200
        Visible = True
      end>
    CampoResultado = 'ID_USUARIO'
    CampoDescricao = 'NOME'
    SQL.Strings = (
      'select id_usuario, nome from gousuarios where 1=1')
    MultiSelecao = False
    Conexao = dm.conexao
    AbrirAuto = False
    PesquisaPadrao = 1
    Filtros = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptUnknown
        Value = 'C'#243'digo'
      end
      item
        DataType = ftWideString
        Name = 'NOME'
        ParamType = ptUnknown
        Value = 'Nome'
      end>
    EditCodigo = dbepermissao_usuarioID_USUARIO_DESTINO
    EditDescricao = dbepermissao_usuarionome_destino
    BotaoPesquisa = btnpesquisarusuariodestino
    Left = 171
    Top = 75
  end
end
