unit uclonarpermissao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.StdCtrls, Vcl.ExtCtrls,
  cxGroupBox, uPesquisa;

type
  Tfclonarpermissao = class(TForm)
    gpxdados: TcxGroupBox;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    lblSite: TLabel;
    pnlLayoutLinhaspermissao1: TPanel;
    pnlLayoutCampospermissaoid: TPanel;
    pnlLayoutLinhaspermissaolinha1: TPanel;
    btnpesquisarusuario: TButton;
    dbepermissao_usuarionome: TEdit;
    dbepermissao_usuarioID_USUARIO: TEdit;
    pnlLayoutLinhaspermissao2: TPanel;
    pnlLayoutCampospermissaodestino: TPanel;
    pnlLayoutLinhaspermissaolinha2: TPanel;
    btnpesquisarusuariodestino: TButton;
    dbepermissao_usuarionome_destino: TEdit;
    dbepermissao_usuarioID_USUARIO_DESTINO: TEdit;
    spdcancelar: TButton;
    spdconfirmar: TButton;
    pesqusuario1: TPesquisa;
    pesqusuario2: TPesquisa;
    procedure spdcancelarClick(Sender: TObject);
    procedure spdconfirmarClick(Sender: TObject);
    procedure dbepermissao_usuarioID_USUARIOKeyPress(Sender: TObject;
      var Key: Char);
    procedure dbepermissao_usuarioID_USUARIO_DESTINOKeyPress(Sender: TObject;
      var Key: Char);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fclonarpermissao: Tfclonarpermissao;

implementation

{$R *.dfm}

procedure Tfclonarpermissao.dbepermissao_usuarioID_USUARIOKeyPress(
  Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
  key := #0;

 if key = #8 then
 begin
  dbepermissao_usuarionome.Text  :=EmptyStr;
 end;

end;

procedure Tfclonarpermissao.dbepermissao_usuarioID_USUARIO_DESTINOKeyPress(
  Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
  key := #0;

 if key = #8 then
 begin
  dbepermissao_usuarionome_destino.Text  :=EmptyStr;
 end;

end;

procedure Tfclonarpermissao.FormShow(Sender: TObject);
begin
 dbepermissao_usuarioID_USUARIO.Text         := EmptyStr;
 dbepermissao_usuarioID_USUARIO_DESTINO.Text := EmptyStr;
 dbepermissao_usuarionome .Text              := EmptyStr;
 dbepermissao_usuarionome_destino.Text       := EmptyStr;
end;

procedure Tfclonarpermissao.pnlTituloMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tfclonarpermissao.spdcancelarClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

procedure Tfclonarpermissao.spdconfirmarClick(Sender: TObject);
begin
 if dbepermissao_usuarioID_USUARIO.Text<>dbepermissao_usuarioID_USUARIO_DESTINO.Text then
   ModalResult := mrok
 else
  begin
   Application.MessageBox('Usu�rios id�nticos', 'Clonar permiss�o',MB_OK+MB_ICONINFORMATION);
   abort;
  end;
end;

end.
