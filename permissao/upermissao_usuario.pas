unit upermissao_usuario;

{*Autor- Rychard Silva *}
{*Data 08/05/2020*}
{*Hora 20:08:14*}
{*Unit permissao_usuario *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes,Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxPC, Data.SqlExpr,
  cxGridLevel, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxGDIPlusClasses,
  cxButtonEdit, cxDBEdit, uPesquisa, frxClass, frxDBSet, cxCheckBox,
  dxSkinsCore, dxSkinSevenClassic, dxSkinscxPCPainter, Vcl.DialogMessage,
  uclonarpermissao, uacesso;

type                                                                                        
	Tfpermissao_usuario = class(TForm)
  cdspermissao_usuario: TClientDataSet;
  dspermissao_usuario: TDataSource;
  popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
  Popuppermissao_usuario: TPopupMenu;
  pgcPrincipal: TcxPageControl;
  pesqrotina: TPesquisa;
  SCRpermissao: TScrollBox;
  pnlDadospermissao: TPanel;
  pnlLayoutLinhaspermissao1: TPanel;
  pnlLayoutCampospermissaoid: TPanel;
  Panel1: TPanel;
  btnpesquisarusuario: TButton;
  pnlLayoutLinhaspermissao5: TPanel;
  pnlLayoutTitulospermissaorotina: TPanel;
  pnlLayoutCampospermissaorotina: TPanel;
  btnPesquisarrotina: TButton;
  pnlLayoutLinhaspermissao6: TPanel;
  pesqusuario: TPesquisa;
  dbepermissao_usuarionome: TEdit;
  dbepermissao_usuarioRotina: TEdit;
  dbepermissao_usuarioID_ROTINA: TEdit;
    GridPermissoes: TcxGrid;
    GridPermissoesDBTV: TcxGridDBTableView;
    GridPermissoesLevel1: TcxGridLevel;
    dsppermissao_usuario: TDataSetProvider;
    sqlpermissao_usuario: TSQLDataSet;
    dbepermissao_usuarioID_USUARIO: TEdit;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    imglogo: TImage;
    Label2: TLabel;
    sqlpermissao_usuarioUSUARIO: TStringField;
    sqlpermissao_usuarioACESSO: TStringField;
    sqlpermissao_usuarioROTINA: TStringField;
    sqlpermissao_usuarioDESCRICAO: TStringField;
    cdspermissao_usuarioUSUARIO: TStringField;
    cdspermissao_usuarioACESSO: TStringField;
    cdspermissao_usuarioROTINA: TStringField;
    cdspermissao_usuarioDESCRICAO: TStringField;
    spdpesquisarpermissao_usuario: TButton;
    GridPermissoesDBTVUSUARIO: TcxGridDBColumn;
    GridPermissoesDBTVROTINA: TcxGridDBColumn;
    GridPermissoesDBTVDESCRICAO: TcxGridDBColumn;
    GridPermissoesDBTVACESSO: TcxGridDBColumn;
    sqlpermissao_usuarioID_USUARIO: TIntegerField;
    cdspermissao_usuarioID_USUARIO: TIntegerField;
    GridPermissoesusuarios: TcxGrid;
    GridPermissoes_acessoDBTV: TcxGridDBTableView;
    GridPermissoesLevel2: TcxGridLevel;
    pnlLinhaBotoespermissao: TPanel;
    spdclonarpermissao: TButton;
    pnlcontrolepermissoesusuario: TPanel;
    spdadicionarpermissao: TButton;
    spdadicionarpermissaoagruapda: TButton;
    spdExcluirpermissao: TButton;
    spdexcluirpermissaoagrupada: TButton;
    sqlpermissao_usuario_acesso: TSQLDataSet;
    dsppermissao_usuario_acesso: TDataSetProvider;
    cdspermissao_usuario_acesso: TClientDataSet;
    dspermissao_usuario_acesso: TDataSource;
    sqlpermissao_usuario_acessoID_USUARIO: TIntegerField;
    sqlpermissao_usuario_acessoUSUARIO: TStringField;
    sqlpermissao_usuario_acessoACESSO: TStringField;
    sqlpermissao_usuario_acessoROTINA: TStringField;
    sqlpermissao_usuario_acessoDESCRICAO: TStringField;
    cdspermissao_usuario_acessoID_USUARIO: TIntegerField;
    cdspermissao_usuario_acessoUSUARIO: TStringField;
    cdspermissao_usuario_acessoACESSO: TStringField;
    cdspermissao_usuario_acessoROTINA: TStringField;
    cdspermissao_usuario_acessoDESCRICAO: TStringField;
    GridPermissoes_acessoDBTVUSUARIO: TcxGridDBColumn;
    GridPermissoes_acessoDBTVROTINA: TcxGridDBColumn;
    GridPermissoes_acessoDBTVDESCRICAO: TcxGridDBColumn;
    GridPermissoes_acessoDBTVACESSO: TcxGridDBColumn;
    sqlpermissao_usuarioID_PERMISSAO: TIntegerField;
    cdspermissao_usuarioID_PERMISSAO: TIntegerField;
    sqlpermissao_usuario_acessoID_PERMISSAO: TIntegerField;
    cdspermissao_usuario_acessoID_PERMISSAO: TIntegerField;
    lblpermissaodisponivel: TLabel;
    lblpermissaovinculada: TLabel;
    lblTituloversao: TLabel;
    lblProjetoresultado: TLabel;
    lblusuario: TLabel;
    lblusuarioresultado: TLabel;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure spdImpressaoClick(Sender: TObject);
  procedure FormShow(Sender: TObject);
    procedure spdpesquisarpermissao_usuarioClick(Sender: TObject);
    procedure spdadicionarpermissaoClick(Sender: TObject);
    procedure spdExcluirpermissaoClick(Sender: TObject);
    procedure GridPermissoesDBTVDblClick(Sender: TObject);
    procedure GridPermissoes_acessoDBTVDblClick(Sender: TObject);
    procedure spdadicionarpermissaoagruapdaClick(Sender: TObject);
    procedure spdexcluirpermissaoagrupadaClick(Sender: TObject);
    procedure dbepermissao_usuarioID_USUARIOKeyPress(Sender: TObject;
      var Key: Char);
    procedure dbepermissao_usuarioID_ROTINAKeyPress(Sender: TObject;
      var Key: Char);
    procedure spdclonarpermissaoClick(Sender: TObject);

	private

  {array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriospermissao: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
  objetosModoEdicaoAtivospermissao: Array[0..10]  of TControl;
  objetosModoEdicaoInativospermissao: Array[0..10]  of TControl;
  {array de componentes para verifica��o de duplicidade}
	duplicidadeListapermissao: Array[0..2]  of TDuplicidade;
	duplicidadeCampopermissao: TDuplicidade;


	pesquisasItem: TPesquisas;
    procedure sessao;
    procedure atualizar_permissoes;
    procedure adicionar_permissao;
    procedure excluir_permissao;
  //procedure buscarPermissoes;
	public
  fncpermissao_usuario: TFuncoes;
  const versao=1;
  const id_modulo='1';
  const id_rotina='81';
  const rotina='Permiss�o Usu�rio';


end;

var
 fpermissao_usuario: Tfpermissao_usuario;

implementation

uses udm, System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfpermissao_usuario.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfpermissao_usuario.FormShow(Sender: TObject);
begin
  if (facesso = nil) then Application.CreateForm(Tfacesso, facesso);
   facesso.ShowModal;

  if facesso.ModalResult=mrOk then
  begin
   facesso.Destroy;
   fncpermissao_usuario.ativarModoEdicao(objetosModoEdicaoAtivospermissao,objetosModoEdicaoInativospermissao,true);
   dbepermissao_usuarioID_USUARIO.SetFocus;
  end
  else
   application.Terminate;
end;

procedure Tfpermissao_usuario.GridPermissoesDBTVDblClick(Sender: TObject);
begin
 adicionar_permissao;
end;

procedure Tfpermissao_usuario.GridPermissoes_acessoDBTVDblClick(
  Sender: TObject);
begin
  excluir_permissao;
end;

procedure Tfpermissao_usuario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
   fncpermissao_usuario.configurarGridesFormulario(fpermissao_usuario, true, false);
  	fncpermissao_usuario.verificarEmTransacao(cdspermissao_usuario);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncpermissao_usuario.Free;
	{eliminando container de fun��es da memoria<<<}

	fpermissao_usuario:=nil;
	Action:=cafree;
end;

procedure Tfpermissao_usuario.FormCreate(Sender: TObject);
var caminho_traducao: string; arquivo: tinifile;
begin
   sessao;

   arquivo            :=tinifile.Create('C:\gosoftware\parametros.ini');
   caminho_traducao   :=arquivo.ReadString('local3','propriedade3','Erro ao ler par�metro');
   {>>>container de tradu�a� de compomentes}
   if FileExists(caminho_traducao+'Tradu��oDev.ini') then
   begin
     dm.cxLocalizer.LoadFromFile(caminho_traducao+'Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := true;
   end;
  {>>>container de tradu�a� de compomentes}


  {>>>campos obrigatorios permissao}
	camposObrigatoriospermissao[0] :=dbepermissao_usuarioID_USUARIO;
	{campos obrigatorios permissao<<<}

	{>>>objetos ativos no modo de manipula��o de dados permissao}
	objetosModoEdicaoAtivospermissao[0] := pnlDadospermissao;
  objetosModoEdicaoAtivospermissao[1] := spdpesquisarpermissao_usuario;

  {>>>objetos inativos no modo de manipula��o de dados permissao}
	objetosModoEdicaoInativospermissao[0]:=spdOpcoes;
	{objetos inativos no modo de manipula��o de dados permissao<<<}

	{>>>container de fun��es}
	fncpermissao_usuario:= TFuncoes.Create(Self);
	fncpermissao_usuario.definirConexao(dm.conexao);
	fncpermissao_usuario.definirFormulario(Self);
	fncpermissao_usuario.validarTransacaoRelacionamento:=true;
	fncpermissao_usuario.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  {<<vers�o da rotina>>}
  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncpermissao_usuario.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}

  {>>>verificar campos duplicidade}
  duplicidadeCampopermissao:=TDuplicidade.Create;
	duplicidadeCampopermissao.agrupador:='';
	duplicidadeCampopermissao.objeto :=dbepermissao_usuarioID_USUARIO;
	duplicidadeListapermissao[0]     :=duplicidadeCampopermissao;

 // cdspermissao_usuario.open();

	fncpermissao_usuario.criaAtalhoPopupMenuNavegacao(Popuppermissao_usuario,cdspermissao_usuario);
	fncpermissao_usuario.definirMascaraCampos(cdspermissao_usuario);
  fncpermissao_usuario.configurarGridesFormulario(fpermissao_usuario, false, true);

end;

procedure Tfpermissao_usuario.sessao;
var host: string; arquivo: tinifile;
begin
//  try
//    dm.conexao.Close;
//    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
//    dm.conexao.Params.Values['password'] := ParamStr(2);
//
//    dm.fdConexao.Close;
//    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
//    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
//    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
//    dm.conexao.Connected := True;
//    dm.fdConexao.Connected := True;
//
//    except
//     on e : exception do
//     begin
//        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
//        application.Terminate;
//     end;
// end;
  try
   //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);


    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfpermissao_usuario.atualizar_permissoes;
begin
  cdspermissao_usuario.Close;
  cdspermissao_usuario.Open;
  cdspermissao_usuario_acesso.Close;
  cdspermissao_usuario_acesso.Open;
end;

procedure Tfpermissao_usuario.dbepermissao_usuarioID_ROTINAKeyPress(
  Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
 key := #0;

 if key = #8 then
 begin
  dbepermissao_usuarioRotina.Text  :=EmptyStr;
 end;

end;

procedure Tfpermissao_usuario.dbepermissao_usuarioID_USUARIOKeyPress(
  Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
 key := #0;

 if key = #8 then
 begin
  dbepermissao_usuarionome.Text  :=EmptyStr;
 end;
end;

procedure Tfpermissao_usuario.adicionar_permissao;
begin
  if not cdspermissao_usuario.IsEmpty then
  begin
    with dm.fQuery do
    begin
      dm.fQuery.Close;
      dm.fQuery.CommandText := 'update gopermissao_usuario set acesso=''S'' WHERE ID_USUARIO=' + cdspermissao_usuarioID_USUARIO.asString + ' AND ID_PERMISSAO=' + cdspermissao_usuarioID_PERMISSAO.asString;
      dm.fQuery.ExecSQL(true);
      atualizar_permissoes;
    end;
  end;
end;

procedure Tfpermissao_usuario.excluir_permissao;
begin
  if not cdspermissao_usuario_acesso.IsEmpty then
  begin
    with dm.fQuery do
    begin
      dm.fQuery.Close;
      dm.fQuery.CommandText := 'update gopermissao_usuario set acesso=''N'' WHERE ID_USUARIO=' + cdspermissao_usuario_acessoID_USUARIO.asString + ' AND ID_PERMISSAO=' + cdspermissao_usuario_acessoID_PERMISSAO.asString;
      dm.fQuery.ExecSQL(true);
      atualizar_permissoes;
    end;
  end;
end;

procedure Tfpermissao_usuario.spdpesquisarpermissao_usuarioClick(Sender: TObject);
begin
   if dbepermissao_usuarioID_USUARIO.Text =EmptyStr then
   begin
    abort;
   end;

  if dbepermissao_usuarioID_USUARIO.Text <>EmptyStr then
  begin
     cdspermissao_usuario.Filtered := False;
     cdspermissao_usuario.Close;
     sqlpermissao_usuario.ParamByName('ID_USUARIO').asInteger  := strToint(dbepermissao_usuarioID_USUARIO.Text);
     cdspermissao_usuario.Open;
  end;

  if dbepermissao_usuarioID_ROTINA.Text<>EmptyStr then
  begin
     cdspermissao_usuario.DisableControls;
      try
        cdspermissao_usuario.Filtered := False;
        cdspermissao_usuario.FilterOptions := [foNoPartialCompare];
        cdspermissao_usuario.Filter := 'UPPER(ROTINA) LIKE ''%' + UPPERCASE(dbepermissao_usuarioRotina.Text) + '%''';
        cdspermissao_usuario.Filtered := True;
      finally
        cdspermissao_usuario.First;
        cdspermissao_usuario.EnableControls;
      end;
  end;


  if dbepermissao_usuarioID_USUARIO.Text <>EmptyStr then
  begin
     cdspermissao_usuario_acesso.Filtered := False;
     cdspermissao_usuario_acesso.Close;
     sqlpermissao_usuario_acesso.ParamByName('ID_USUARIO').asInteger  := strToint(dbepermissao_usuarioID_USUARIO.Text);
     cdspermissao_usuario_acesso.Open;
  end;

  if dbepermissao_usuarioID_ROTINA.Text<>EmptyStr then
  begin
     cdspermissao_usuario_acesso.DisableControls;
      try
        cdspermissao_usuario_acesso.Filtered := False;
        cdspermissao_usuario_acesso.FilterOptions := [foNoPartialCompare];
        cdspermissao_usuario_acesso.Filter := 'UPPER(ROTINA) LIKE ''%' + UPPERCASE(dbepermissao_usuarioRotina.Text) + '%''';
        cdspermissao_usuario_acesso.Filtered := True;
      finally
        cdspermissao_usuario_acesso.First;
        cdspermissao_usuario_acesso.EnableControls;
      end;
  end;

end;

procedure Tfpermissao_usuario.spdadicionarpermissaoagruapdaClick(
  Sender: TObject);
begin

 if Application.MessageBox('Deseja realmente ADICIONAR todas as permiss��es??','Adicionar',MB_YESNO+MB_ICONQUESTION)=IDNO then
 abort;

  TDialogMessage.ShowWaitMessage('Adicionando todas as permiss�es '+ uppercase(dbepermissao_usuarionome.Text),
  procedure
  begin
      cdspermissao_usuario.DisableControls;
      cdspermissao_usuario.First;
      while not cdspermissao_usuario.Eof do
      begin
        with dm.fQuery do
        begin
          dm.fQuery.Close;
          dm.fQuery.CommandText := 'update gopermissao_usuario set acesso=''S'' WHERE ID_USUARIO=' + cdspermissao_usuarioID_USUARIO.asString + ' AND ID_PERMISSAO=' + cdspermissao_usuarioID_PERMISSAO.asString;
          dm.fQuery.ExecSQL(true);
        end;
         cdspermissao_usuario.Next;
      end;
      cdspermissao_usuario.EnableControls;
      atualizar_permissoes;
  end);

end;

procedure Tfpermissao_usuario.spdadicionarpermissaoClick(Sender: TObject);
begin
  adicionar_permissao;
end;

procedure Tfpermissao_usuario.spdclonarpermissaoClick(Sender: TObject);
var
   dataSet: Tsqldataset;
begin
   if (fclonarpermissao = nil) then Application.CreateForm(Tfclonarpermissao, fclonarpermissao);
   fclonarpermissao.ShowModal;

   if fclonarpermissao.ModalResult=mrok then
   begin

        TDialogMessage.ShowWaitMessage('Clonando permiss�es...',
        procedure
        begin
           try
             dataSet:=TSQLDataSet.Create(Application);
             dataSet.SQLConnection:=dm.conexao;
             dataSet.CommandText:='SELECT * FROM gopermissao_usuario WHERE ID_USUARIO='+fclonarpermissao.dbepermissao_usuarioID_USUARIO.Text;
             dataSet.Open;

             while not dataSet.Eof do
             begin
               with dm.fQuery do
              begin
                dm.fQuery.Close;
                dm.fQuery.CommandText := 'update gopermissao_usuario set acesso='+QuotedStr(dataSet.FieldByName('ACESSO').AsString)+' WHERE ID_USUARIO=' + fclonarpermissao.dbepermissao_usuarioID_USUARIO_DESTINO.Text+' and ID_PERMISSAO='+dataSet.FieldByName('ID_PERMISSAO').AsString;
                dm.fQuery.ExecSQL(true);
              end;

               dataSet.Next;
             end;

            finally
             freeandnil(dataSet);
             if dbepermissao_usuarioID_USUARIO.Text<>EmptyStr then
             atualizar_permissoes;
            end;

        end);
        Application.MessageBox(pchar('Permiss�es clonadas com sucesso para o usu�rio '+uppercase(fclonarpermissao.dbepermissao_usuarionome_destino.Text)), 'Clonar permiss�o',MB_OK+MB_ICONINFORMATION);

   end;



end;

procedure Tfpermissao_usuario.spdexcluirpermissaoagrupadaClick(Sender: TObject);
begin

   if Application.MessageBox('Deseja realmente EXCLUIR todas as permiss��es??','Excluir',MB_YESNO+MB_ICONQUESTION)=IDNO then
   abort;
  TDialogMessage.ShowWaitMessage('Excluindo todas as permiss�es '+ uppercase(dbepermissao_usuarionome.Text),
  procedure
  begin
    cdspermissao_usuario_acesso.First;
    cdspermissao_usuario_acesso.DisableControls;
    while not cdspermissao_usuario_acesso.Eof do
    begin
      with dm.fQuery do
      begin
        dm.fQuery.Close;
        dm.fQuery.CommandText := 'update gopermissao_usuario set acesso=''N'' WHERE ID_USUARIO=' + cdspermissao_usuario_acessoID_USUARIO.asString + ' AND ID_PERMISSAO=' + cdspermissao_usuario_acessoID_PERMISSAO.asString;
        dm.fQuery.ExecSQL(true);
      end;
       cdspermissao_usuario_acesso.Next;
    end;
    cdspermissao_usuario_acesso.EnableControls;
    atualizar_permissoes;
  end);

end;

procedure Tfpermissao_usuario.spdExcluirpermissaoClick(Sender: TObject);
begin
   excluir_permissao;

end;

procedure Tfpermissao_usuario.spdImpressaoClick(Sender: TObject);
begin
	if fpermissao_usuario.WindowState= wsMaximized then
	 popupImpressao.Popup((Sender as TSpeedButton).Left + 15,(Sender as TSpeedButton).Top + 60)
  else
   popupImpressao.Popup((Sender as TSpeedButton).Left + 370,(Sender as TSpeedButton).Top + 150);
end;

end.

