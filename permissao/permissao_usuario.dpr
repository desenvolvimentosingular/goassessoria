program permissao_usuario;

uses
  Vcl.Forms,
  upermissao_usuario in 'upermissao_usuario.pas' {fpermissao_usuario},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  uclonarpermissao in 'uclonarpermissao.pas' {fclonarpermissao},
  uacesso in '..\class_shared\uacesso.pas' {facesso},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.Themes,
  Vcl.Styles,
  uprocessoatualizacao in '..\class_shared\uprocessoatualizacao.pas' {fprocessoatualizacao},
  uEnvioMail in '..\class_shared\uEnvioMail.pas' {FenvioMail};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfpermissao_usuario, fpermissao_usuario);
  Application.Run;
end.
