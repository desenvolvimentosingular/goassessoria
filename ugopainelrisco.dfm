object fgopainelrisco: Tfgopainelrisco
  Left = 0
  Top = 0
  Caption = 'Painel de Risco'
  ClientHeight = 461
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  PopupMenu = Popupgopainelrisco
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTitulo: TPanel
    Left = 0
    Top = 0
    Width = 564
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = 11833701
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 116
    object lblTitulo: TLabel
      Left = 16
      Top = 8
      Width = 96
      Height = 16
      Caption = 'Painel de Risco'
      Color = 11833701
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object spdOpcoes: TSpeedButton
      Left = 345
      Top = 5
      Width = 65
      Height = 22
      Hint = 'Op'#231#245'es extras do formul'#225'rio.'
      Caption = '&Op'#231#245'es'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object spdImpressao: TSpeedButton
      Left = 416
      Top = 5
      Width = 65
      Height = 22
      Hint = 'Assistente de impress'#227'o'
      Caption = '&Impress'#227'o'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      PopupMenu = popupImpressao
      ShowHint = True
      OnClick = spdImpressaoClick
    end
    object spdExportacao: TSpeedButton
      Left = 487
      Top = 5
      Width = 65
      Height = 22
      Hint = 'Assistente de exporta'#231#227'o.'
      Caption = '&Exporta'#231#227'o'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object pnlLogo: TPanel
      Left = 504
      Top = 0
      Width = 60
      Height = 54
      Align = alRight
      BevelOuter = bvNone
      Color = 5586730
      TabOrder = 0
      ExplicitLeft = 56
      object imglogo: TImage
        Left = 0
        Top = 0
        Width = 60
        Height = 54
        Hint = 'www.singularsolucao.com.br'
        Align = alClient
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 69
        ExplicitTop = 7
        ExplicitWidth = 45
        ExplicitHeight = 45
      end
    end
  end
  object pnlLinhaTitulo: TPanel
    Left = 0
    Top = 54
    Width = 564
    Height = 17
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Color = 7330047
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 116
    object lblSite: TLabel
      Left = 379
      Top = 0
      Width = 185
      Height = 17
      Align = alRight
      Caption = 'www.singularsolucao.com.br'
      Color = 7330047
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      ExplicitLeft = 623
      ExplicitHeight = 16
    end
  end
  object pgcPrincipal: TcxPageControl
    Left = 0
    Top = 71
    Width = 564
    Height = 390
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = tabgopainelrisco
    Properties.CustomButtons.Buttons = <>
    Properties.Style = 9
    Properties.TabSlants.Positions = []
    TabSlants.Positions = []
    ExplicitWidth = 116
    ExplicitHeight = 415
    ClientRectBottom = 390
    ClientRectRight = 564
    ClientRectTop = 20
    object tabPrincipal: TcxTabSheet
      Caption = 'P&rincipal'
      PopupMenu = Popupgopainelrisco
      OnShow = altenarPopupTabSheet
      object pnlTituloGridgopainelrisco: TPanel
        Left = 0
        Top = 0
        Width = 564
        Height = 19
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Painel de Risco'
        Color = 12426873
        TabOrder = 0
      end
      object Gridgopainelrisco: TcxGrid
        Left = 0
        Top = 19
        Width = 564
        Height = 351
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = False
        object GridgopainelriscoDBTV: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Insert.Enabled = False
          Navigator.Buttons.Append.Enabled = False
          Navigator.Buttons.Delete.Enabled = False
          Navigator.Buttons.Edit.Enabled = False
          Navigator.Buttons.Cancel.Enabled = False
          DataController.DataSource = dsgopainelrisco
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = 'Sem registros'
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object GRIDgopainelriscoDBTVID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
          end
          object GRIDgopainelriscoDBTVDT_CADASTRO: TcxGridDBColumn
            DataBinding.FieldName = 'DT_CADASTRO'
          end
          object GRIDgopainelriscoDBTVDESCRICAO: TcxGridDBColumn
            DataBinding.FieldName = 'DESCRICAO'
          end
          object GRIDgopainelriscoDBTVCODPROJETO: TcxGridDBColumn
            DataBinding.FieldName = 'CODPROJETO'
          end
          object GRIDgopainelriscoDBTVPROJETO: TcxGridDBColumn
            DataBinding.FieldName = 'PROJETO'
          end
          object GRIDgopainelriscoDBTVCODFILIAL: TcxGridDBColumn
            DataBinding.FieldName = 'CODFILIAL'
          end
          object GRIDgopainelriscoDBTVFILIAL: TcxGridDBColumn
            DataBinding.FieldName = 'FILIAL'
          end
          object GRIDgopainelriscoDBTVDATA: TcxGridDBColumn
            DataBinding.FieldName = 'DATA'
          end
        end
      end
    end
    object tabgopainelrisco: TcxTabSheet
      Caption = 'Painel de Risco'
      PopupMenu = Popupgopainelrisco
      OnShow = altenarPopupTabSheet
      object pnlLinhaBotoesgopainelrisco: TPanel
        Left = 0
        Top = 345
        Width = 564
        Height = 25
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object spdAdicionargopainelrisco: TButton
          Left = 5
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F4 - Adiciona um novo registro.'
          Caption = 'Adicionar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = spdAdicionargopainelriscoClick
        end
        object spdAlterargopainelrisco: TButton
          Left = 76
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F5 - Alterar registro selecionado.'
          Caption = 'Alterar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = spdAlterargopainelriscoClick
        end
        object spdGravargopainelrisco: TButton
          Left = 147
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F6 - Gravar altera'#231#245'es no registro selecionado.'
          Caption = 'Gravar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = spdGravargopainelriscoClick
        end
        object spdCancelargopainelrisco: TButton
          Left = 218
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F7 - Cancela altera'#231#245'es no registro selecionado.'
          Caption = 'Cancelar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = spdCancelargopainelriscoClick
        end
        object spdExcluirgopainelrisco: TButton
          Left = 289
          Top = 1
          Width = 65
          Height = 22
          Hint = 'CTRL + DEL - Exclui registro selecionado.'
          Caption = 'Excluir'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = spdExcluirgopainelriscoClick
        end
      end
      object SCRgopainelrisco: TScrollBox
        Left = 0
        Top = 0
        Width = 564
        Height = 345
        Align = alClient
        BorderStyle = bsNone
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        object pnlDadosgopainelrisco: TPanel
          Left = 0
          Top = 0
          Width = 547
          Height = 429
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object pnlLayoutLinhasgopainelrisco1: TPanel
            Left = 0
            Top = 0
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object pnlLayoutTitulosgopainelriscoid: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  C'#243'digo:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscoid: TPanel
              Left = 150
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoID: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'digo'
                DataField = 'ID'
                DataSource = dsgopainelrisco
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco2: TPanel
            Left = 0
            Top = 30
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object pnlLayoutTitulosgopainelriscodt_cadastro: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Data de Cadastro:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscodt_cadastro: TPanel
              Left = 150
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoDT_CADASTRO: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'Data de Cadastro'
                DataField = 'DT_CADASTRO'
                DataSource = dsgopainelrisco
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco3: TPanel
            Left = 0
            Top = 60
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object pnlLayoutTitulosgopainelriscohs_cadastro: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Hora de Cadastro:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscohs_cadastro: TPanel
              Left = 150
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoHS_CADASTRO: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'Hora de Cadastro'
                DataField = 'HS_CADASTRO'
                DataSource = dsgopainelrisco
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco4: TPanel
            Left = 0
            Top = 90
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object pnlLayoutTitulosgopainelriscodescricao: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Descri'#231#227'o:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscodescricao: TPanel
              Left = 150
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoDESCRICAO: TDBEdit
                Left = 6
                Top = 5
                Width = 480
                Height = 21
                Hint = 'Descri'#231#227'o'
                Color = 15722462
                DataField = 'DESCRICAO'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco5: TPanel
            Left = 0
            Top = 120
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 4
            object pnlLayoutTitulosgopainelriscocodprojeto: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  C'#243'digo Projeto:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscocodprojeto: TPanel
              Left = 150
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoCODPROJETO: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'digo Projeto'
                DataField = 'CODPROJETO'
                DataSource = dsgopainelrisco
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco6: TPanel
            Left = 0
            Top = 150
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 5
            object pnlLayoutTitulosgopainelriscoprojeto: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Projeto:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscoprojeto: TPanel
              Left = 150
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoPROJETO: TDBEdit
                Left = 6
                Top = 5
                Width = 480
                Height = 21
                Hint = 'Projeto'
                DataField = 'PROJETO'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco7: TPanel
            Left = 0
            Top = 180
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 6
            object pnlLayoutTitulosgopainelriscocodfilial: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  C'#243'd.Filial:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscocodfilial: TPanel
              Left = 150
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoCODFILIAL: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'd.Filial'
                DataField = 'CODFILIAL'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco8: TPanel
            Left = 0
            Top = 210
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 7
            object pnlLayoutTitulosgopainelriscofilial: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Filial:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscofilial: TPanel
              Left = 150
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoFILIAL: TDBEdit
                Left = 6
                Top = 5
                Width = 480
                Height = 21
                Hint = 'Filial'
                DataField = 'FILIAL'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco9: TPanel
            Left = 0
            Top = 240
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 8
            object pnlLayoutTitulosgopainelriscodata: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Data Compet'#234'ncia:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscodata: TPanel
              Left = 150
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoDATA: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'Data Compet'#234'ncia'
                Color = 15722462
                DataField = 'DATA'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco10: TPanel
            Left = 0
            Top = 270
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 9
            object pnlLayoutTitulosgopainelriscovlvencidos: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Total Vencidos:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscovlvencidos: TPanel
              Left = 150
              Top = 0
              Width = 100
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoVLVENCIDOS: TDBEdit
                Left = 6
                Top = 5
                Width = 90
                Height = 21
                Hint = 'Total Vencidos'
                DataField = 'VLVENCIDOS'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhasgopainelrisco11: TPanel
            Left = 0
            Top = 300
            Width = 547
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 10
            object pnlLayoutTitulosgopainelriscovlvencer: TPanel
              Left = 0
              Top = 0
              Width = 150
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Total Vencer:'
              TabOrder = 1
            end
            object pnlLayoutCamposgopainelriscovlvencer: TPanel
              Left = 150
              Top = 0
              Width = 100
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbegopainelriscoVLVENCER: TDBEdit
                Left = 6
                Top = 5
                Width = 90
                Height = 21
                Hint = 'Total Vencer'
                DataField = 'VLVENCER'
                DataSource = dsgopainelrisco
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
        end
      end
    end
    object tabPesquisas: TcxTabSheet
      Caption = 'Pesquisas'
      PopupMenu = Popupgopainelrisco
      OnShow = altenarPopupTabSheet
      ExplicitWidth = 116
      ExplicitHeight = 395
      object pnlTituloPesquisa: TPanel
        Left = 0
        Top = 0
        Width = 564
        Height = 19
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Op'#231#227'o('#245'es) de pesquisa(s)...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 116
      end
      object flwpOpcoes: TFlowPanel
        Left = 0
        Top = 19
        Width = 564
        Height = 70
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitWidth = 116
      end
      object flwpPesquisas: TFlowPanel
        Left = 0
        Top = 108
        Width = 564
        Height = 80
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitWidth = 116
      end
      object pnlTituloResultadoPesquisa: TPanel
        Left = 0
        Top = 89
        Width = 564
        Height = 19
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Resultado da pesquisa...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        ExplicitWidth = 116
      end
      object pnlResultadoPesquisas: TPanel
        Left = 0
        Top = 188
        Width = 564
        Height = 182
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 4
        ExplicitWidth = 116
        ExplicitHeight = 207
        object GridResultadoPesquisa: TcxGrid
          Left = 0
          Top = 0
          Width = 564
          Height = 182
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = False
          ExplicitWidth = 116
          ExplicitHeight = 207
          object GridResultadoPesquisaDBTV: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Insert.Enabled = False
            Navigator.Buttons.Append.Enabled = False
            Navigator.Buttons.Delete.Enabled = False
            Navigator.Buttons.Edit.Enabled = False
            Navigator.Buttons.Cancel.Enabled = False
            DataController.DataSource = dsgopainelrisco
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = 'Sem registros'
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            object GRIDResultadoPesquisaDBTVID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
            end
            object GRIDResultadoPesquisaDBTVDT_CADASTRO: TcxGridDBColumn
              DataBinding.FieldName = 'DT_CADASTRO'
            end
            object GRIDResultadoPesquisaDBTVDESCRICAO: TcxGridDBColumn
              DataBinding.FieldName = 'DESCRICAO'
            end
            object GRIDResultadoPesquisaDBTVCODPROJETO: TcxGridDBColumn
              DataBinding.FieldName = 'CODPROJETO'
            end
            object GRIDResultadoPesquisaDBTVPROJETO: TcxGridDBColumn
              DataBinding.FieldName = 'PROJETO'
            end
            object GRIDResultadoPesquisaDBTVCODFILIAL: TcxGridDBColumn
              DataBinding.FieldName = 'CODFILIAL'
            end
            object GRIDResultadoPesquisaDBTVFILIAL: TcxGridDBColumn
              DataBinding.FieldName = 'FILIAL'
            end
            object GRIDResultadoPesquisaDBTVDATA: TcxGridDBColumn
              DataBinding.FieldName = 'DATA'
            end
          end
          object GridResultadoPesquisaLevel1: TcxGridLevel
            GridView = GridResultadoPesquisaDBTV
          end
        end
      end
    end
  end
  object Popupgopainelrisco: TPopupMenu
    Left = 574
    Top = 20
  end
  object popupImpressao: TPopupMenu
    Left = 454
    Top = 20
    object menuImpRelSintetico: TMenuItem
      Caption = 'Relat'#243'rio Sint'#233'tico'
      OnClick = menuImpRelSinteticoClick
    end
  end
  object frxgopainelrisco: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40094.709455300920000000
    ReportOptions.LastChange = 40094.709455300920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 292
    Top = 134
    Datasets = <
      item
        DataSet = frxDBgopainelrisco
        DataSetName = 'frxDBgopainelrisco'
      end>
    Variables = <>
    Style = <>
  end
  object frxDBgopainelrisco: TfrxDBDataset
    UserName = 'frxDBgopainelrisco'
    CloseDataSource = False
    DataSet = cdsgopainelrisco
    BCDToCurrency = False
    Left = 322
    Top = 134
  end
  object sqlgopainelrisco: TSQLDataSet
    CommandText = 'select first 0 * from gopainelrisco'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dm.conexao
    Left = 10
    Top = 10
    object sqlgopainelriscoID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object sqlgopainelriscoDT_CADASTRO: TDateField
      FieldName = 'DT_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlgopainelriscoHS_CADASTRO: TTimeField
      FieldName = 'HS_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlgopainelriscoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 250
    end
    object sqlgopainelriscoCODPROJETO: TIntegerField
      FieldName = 'CODPROJETO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlgopainelriscoPROJETO: TStringField
      FieldName = 'PROJETO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlgopainelriscoCODFILIAL: TIntegerField
      FieldName = 'CODFILIAL'
      ProviderFlags = [pfInUpdate]
    end
    object sqlgopainelriscoFILIAL: TStringField
      FieldName = 'FILIAL'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlgopainelriscoDATA: TDateField
      FieldName = 'DATA'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object sqlgopainelriscoVLVENCIDOS: TFloatField
      FieldName = 'VLVENCIDOS'
      ProviderFlags = [pfInUpdate]
    end
    object sqlgopainelriscoVLVENCER: TFloatField
      FieldName = 'VLVENCER'
      ProviderFlags = [pfInUpdate]
    end
  end
  object dspgopainelrisco: TDataSetProvider
    DataSet = sqlgopainelrisco
    UpdateMode = upWhereKeyOnly
    Left = 40
    Top = 10
  end
  object cdsgopainelrisco: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspgopainelrisco'
    AfterOpen = cdsgopainelriscoAfterOpen
    AfterClose = cdsgopainelriscoAfterClose
    BeforePost = cdsgopainelriscoBeforePost
    Left = 70
    Top = 10
    object cdsgopainelriscoID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsgopainelriscoDT_CADASTRO: TDateField
      DisplayLabel = 'Data de Cadastro'
      FieldName = 'DT_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object cdsgopainelriscoHS_CADASTRO: TTimeField
      DisplayLabel = 'Hora de Cadastro'
      FieldName = 'HS_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object cdsgopainelriscoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 250
    end
    object cdsgopainelriscoCODPROJETO: TIntegerField
      DisplayLabel = 'C'#243'digo Projeto'
      FieldName = 'CODPROJETO'
      ProviderFlags = [pfInUpdate]
    end
    object cdsgopainelriscoPROJETO: TStringField
      DisplayLabel = 'Projeto'
      FieldName = 'PROJETO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdsgopainelriscoCODFILIAL: TIntegerField
      DisplayLabel = 'C'#243'd.Filial'
      FieldName = 'CODFILIAL'
      ProviderFlags = [pfInUpdate]
    end
    object cdsgopainelriscoFILIAL: TStringField
      DisplayLabel = 'Filial'
      FieldName = 'FILIAL'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdsgopainelriscoDATA: TDateField
      DisplayLabel = 'Data Compet'#234'ncia'
      FieldName = 'DATA'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsgopainelriscoVLVENCIDOS: TFloatField
      DisplayLabel = 'Total Vencidos'
      FieldName = 'VLVENCIDOS'
      ProviderFlags = [pfInUpdate]
    end
    object cdsgopainelriscoVLVENCER: TFloatField
      DisplayLabel = 'Total Vencer'
      FieldName = 'VLVENCER'
      ProviderFlags = [pfInUpdate]
    end
  end
  object dsgopainelrisco: TDataSource
    AutoEdit = False
    DataSet = cdsgopainelrisco
    Left = 100
    Top = 10
  end
end
