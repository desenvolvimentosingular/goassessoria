CREATE TABLE GOPAINELRISCO(
ID Integer   
,
CONSTRAINT pk_GOPAINELRISCO PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
DESCRICAO Varchar(250)  not null COLLATE PT_BR
,
CODPROJETO Integer   
,
PROJETO Varchar(250)   COLLATE PT_BR
,
CODFILIAL Integer   
,
FILIAL Varchar(250)   COLLATE PT_BR
,
DATA Date  not null 
,
VLVENCIDOS Double Precision   
,
VLVENCER Double Precision   
);
commit;
CREATE ASC INDEX I01_GOPA_RISCO_ID ON GOPAINELRISCO (ID);
CREATE ASC INDEX I02_GOPA_RISCO_DT_CADASTRO ON GOPAINELRISCO (DT_CADASTRO);
CREATE ASC INDEX I03_GOPA_RISCO_HS_CADASTRO ON GOPAINELRISCO (HS_CADASTRO);
CREATE ASC INDEX I04_GOPA_RISCO_DESCRICAO ON GOPAINELRISCO (DESCRICAO);
CREATE ASC INDEX I05_GOPA_RISCO_CODPROJETO ON GOPAINELRISCO (CODPROJETO);
CREATE ASC INDEX I06_GOPA_RISCO_PROJETO ON GOPAINELRISCO (PROJETO);
CREATE ASC INDEX I07_GOPA_RISCO_CODFILIAL ON GOPAINELRISCO (CODFILIAL);
CREATE ASC INDEX I08_GOPA_RISCO_FILIAL ON GOPAINELRISCO (FILIAL);
CREATE ASC INDEX I09_GOPA_RISCO_DATA ON GOPAINELRISCO (DATA);
CREATE ASC INDEX I10_GOPA_RISCO_VLVENCIDOS ON GOPAINELRISCO (VLVENCIDOS);
CREATE ASC INDEX I11_GOPA_RISCO_VLVENCER ON GOPAINELRISCO (VLVENCER);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'DESCRICAO';
update rdb$relation_fields set rdb$description = 'C�digo Projeto' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'CODPROJETO';
update rdb$relation_fields set rdb$description = 'Projeto' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'PROJETO';
update rdb$relation_fields set rdb$description = 'C�d.Filial' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'CODFILIAL';
update rdb$relation_fields set rdb$description = 'Filial' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'FILIAL';
update rdb$relation_fields set rdb$description = 'Data Compet�ncia' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'DATA';
update rdb$relation_fields set rdb$description = 'Total Vencidos' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'VLVENCIDOS';
update rdb$relation_fields set rdb$description = 'Total Vencer' where rdb$relation_name = 'GOPAINELRISCO' and rdb$field_name = 'VLVENCER';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPROJETO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPROJETO_GEN FOR GOGRUPOPROJETO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPROJETO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOS;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOS_GEN FOR GOPROJETOS ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOS, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOSUPERVISOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOSUPERVISOR_GEN FOR GOSUPERVISOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOSUPERVISOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOTIPOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOTIPOFUNDO_GEN FOR GOTIPOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOTIPOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPAINELRISCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPAINELRISCO_GEN FOR GOPAINELRISCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPAINELRISCO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
