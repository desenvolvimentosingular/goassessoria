CREATE TABLE goBANCO(
ID Integer   
,
CONSTRAINT pk_goBANCO PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
DESCRICAO Varchar(250)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_BANC_BANCO_ID ON BANCO (ID);
CREATE ASC INDEX I02_BANC_BANCO_DT_CADASTRO ON BANCO (DT_CADASTRO);
CREATE ASC INDEX I03_BANC_BANCO_HS_CADASTRO ON BANCO (HS_CADASTRO);
CREATE ASC INDEX I04_BANC_BANCO_DESCRICAO ON BANCO (DESCRICAO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'BANCO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'BANCO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'BANCO' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'BANCO' and rdb$field_name = 'DESCRICAO';

commit;
CREATE GENERATOR G_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER BANCO_GEN FOR goBANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
