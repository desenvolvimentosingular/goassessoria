unit ubanco;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 18/10/2021*}                                          
{*Hora 10:55:42*}                                            
{*Unit banco *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, Vcl.DialogMessage;

type                                                                                        
	Tfbanco = class(TForm)
    sqlbanco: TSQLDataSet;
	dspbanco: TDataSetProvider;
	cdsbanco: TClientDataSet;
	dsbanco: TDataSource;
    sqlCONTA_BANCO: TSQLDataSet;
	dspCONTA_BANCO: TDataSetProvider;
	cdsCONTA_BANCO: TClientDataSet;
	dsCONTA_BANCO: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxbanco: TfrxReport;
	frxDBbanco: TfrxDBDataset;
	Popupbanco: TPopupMenu;
	Popupconta_banco: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabbanco: tcxTabSheet;
	SCRbanco: TScrollBox;
	pnlDadosbanco: TPanel;
	{>>>bot�es de manipula��o de dados banco}
	pnlLinhaBotoesbanco: TPanel;
	spdAdicionarbanco: TButton;
	spdAlterarbanco: TButton;
	spdGravarbanco: TButton;
	spdCancelarbanco: TButton;
	spdExcluirbanco: TButton;

	{bot�es de manipula��o de dadosbanco<<<}
	{campo ID}
	sqlbancoID: TIntegerField;
	cdsbancoID: TIntegerField;
	pnlLayoutLinhasbanco1: TPanel;
	pnlLayoutTitulosbancoid: TPanel;
	pnlLayoutCamposbancoid: TPanel;
	dbebancoid: TDBEdit;

	{campo DT_CADASTRO}
	sqlbancoDT_CADASTRO: TDateField;
	cdsbancoDT_CADASTRO: TDateField;
	pnlLayoutLinhasbanco2: TPanel;
	pnlLayoutTitulosbancodt_cadastro: TPanel;
	pnlLayoutCamposbancodt_cadastro: TPanel;
	dbebancodt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlbancoHS_CADASTRO: TTimeField;
	cdsbancoHS_CADASTRO: TTimeField;
	pnlLayoutLinhasbanco3: TPanel;
	pnlLayoutTitulosbancohs_cadastro: TPanel;
	pnlLayoutCamposbancohs_cadastro: TPanel;
	dbebancohs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlbancoDESCRICAO: TStringField;
	cdsbancoDESCRICAO: TStringField;
	pnlLayoutLinhasbanco4: TPanel;
	pnlLayoutTitulosbancodescricao: TPanel;
	pnlLayoutCamposbancodescricao: TPanel;
	dbebancodescricao: TDBEdit;

	tabconta_banco: tcxTabSheet;
	SCRconta_banco: TScrollBox;
	pnlDadosconta_banco: TPanel;
	{>>>bot�es de manipula��o de dados conta_banco}
	pnlLinhaBotoesconta_banco: TPanel;
	spdAdicionarconta_banco: TButton;
	spdAlterarconta_banco: TButton;
	spdGravarconta_banco: TButton;
	spdCancelarconta_banco: TButton;
	spdExcluirconta_banco: TButton;

	{bot�es de manipula��o de dadosconta_banco<<<}
	{campo ID}
	sqlconta_bancoID: TIntegerField;
	cdsconta_bancoID: TIntegerField;
	pnlLayoutLinhasconta_banco1: TPanel;
	pnlLayoutTitulosconta_bancoid: TPanel;
	pnlLayoutCamposconta_bancoid: TPanel;
	dbeconta_bancoid: TDBEdit;

	{campo CODBANCO}
	sqlconta_bancoCODBANCO: TIntegerField;
	cdsconta_bancoCODBANCO: TIntegerField;
	pnlLayoutLinhasconta_banco2: TPanel;
	pnlLayoutTitulosconta_bancocodbanco: TPanel;
	pnlLayoutCamposconta_bancocodbanco: TPanel;
	dbeconta_bancocodbanco: TDBEdit;

	{campo CODPROJETO}
	sqlconta_bancoCODPROJETO: TIntegerField;
	cdsconta_bancoCODPROJETO: TIntegerField;
	pnlLayoutLinhasconta_banco3: TPanel;
	pnlLayoutTitulosconta_bancocodprojeto: TPanel;
	pnlLayoutCamposconta_bancocodprojeto: TPanel;
	dbeconta_bancocodprojeto: TDBEdit;

	{campo PROJETO}
	sqlconta_bancoPROJETO: TStringField;
	cdsconta_bancoPROJETO: TStringField;

	{campo DT_CADASTRO}
	sqlconta_bancoDT_CADASTRO: TDateField;
	cdsconta_bancoDT_CADASTRO: TDateField;
	pnlLayoutLinhasconta_banco5: TPanel;
	pnlLayoutTitulosconta_bancodt_cadastro: TPanel;
	pnlLayoutCamposconta_bancodt_cadastro: TPanel;
	dbeconta_bancodt_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlconta_bancoDESCRICAO: TStringField;
	cdsconta_bancoDESCRICAO: TStringField;
	pnlLayoutLinhasconta_banco6: TPanel;
	pnlLayoutTitulosconta_bancodescricao: TPanel;
	pnlLayoutCamposconta_bancodescricao: TPanel;
	dbeconta_bancodescricao: TDBEdit;

	{campo AGENCIA}
	sqlconta_bancoAGENCIA: TStringField;
	cdsconta_bancoAGENCIA: TStringField;
	pnlLayoutLinhasconta_banco7: TPanel;
	pnlLayoutTitulosconta_bancoagencia: TPanel;
	pnlLayoutCamposconta_bancoagencia: TPanel;
	dbeconta_bancoagencia: TDBEdit;

	{campo CC}
	sqlconta_bancoCC: TStringField;
	cdsconta_bancoCC: TStringField;
	pnlLayoutLinhasconta_banco8: TPanel;
	pnlLayoutTitulosconta_bancocc: TPanel;
	pnlLayoutCamposconta_bancocc: TPanel;
	dbeconta_bancocc: TDBEdit;

	{campo TIPO}
	sqlconta_bancoTIPO: TStringField;
	cdsconta_bancoTIPO: TStringField;
	pnlLayoutLinhasconta_banco9: TPanel;
	pnlLayoutTitulosconta_bancotipo: TPanel;
	pnlLayoutCamposconta_bancotipo: TPanel;
	cboconta_bancotipo: TDBComboBox;
    Gridconta_banco: TcxGrid;
    Gridconta_bancoDBTV: TcxGridDBTableView;
    GRIDconta_bancoDBTVID: TcxGridDBColumn;
    GRIDconta_bancoDBTVCODBANCO: TcxGridDBColumn;
    GRIDconta_bancoDBTVCODPROJETO: TcxGridDBColumn;
    GRIDconta_bancoDBTVPROJETO: TcxGridDBColumn;
    GRIDconta_bancoDBTVTIPO: TcxGridDBColumn;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    Gridconta_bancoLevel1: TcxGridLevel;
    pnlLayoutCamposconta_bancoprojeto: TPanel;
    dbeconta_bancoPROJETO: TDBEdit;
    Gridconta_bancoDBTVDESCRICAO: TcxGridDBColumn;
    cdsfiltros: TClientDataSet;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure cdsbancoAfterClose(DataSet: TDataSet);
	procedure cdsbancoAfterOpen(DataSet: TDataSet);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionarbancoClick(Sender: TObject);
	procedure spdAlterarbancoClick(Sender: TObject);
	procedure spdGravarbancoClick(Sender: TObject);
	procedure spdCancelarbancoClick(Sender: TObject);
	procedure spdExcluirbancoClick(Sender: TObject);
	procedure cdsbancoBeforePost(DataSet: TDataSet);
	procedure spdAdicionarconta_bancoClick(Sender: TObject);
	procedure spdAlterarconta_bancoClick(Sender: TObject);
	procedure spdGravarconta_bancoClick(Sender: TObject);
	procedure spdCancelarconta_bancoClick(Sender: TObject);
	procedure spdExcluirconta_bancoClick(Sender: TObject);
	procedure cdsconta_bancoBeforePost(DataSet: TDataSet);
    procedure spdOpcoesClick(Sender: TObject);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncbanco: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosbanco: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosbanco: Array[0..10]  of TControl;
	objetosModoEdicaoInativosbanco: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListabanco: Array[0..0]  of TDuplicidade;
	duplicidadeCampobanco: TDuplicidade;
	{container de funcoes}
	fncconta_banco: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosconta_banco: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosconta_banco: Array[0..10]  of TControl;
	objetosModoEdicaoInativosconta_banco: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListaconta_banco: Array[0..0]  of TDuplicidade;
	duplicidadeCampoconta_banco: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fbanco: Tfbanco;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfbanco.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfbanco.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabbanco.TabIndex;
end;

procedure Tfbanco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncbanco.verificarEmTransacao(cdsbanco);
	fncconta_banco.verificarEmTransacao(cdsconta_banco);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncbanco.configurarGridesFormulario(fbanco,true, false);

	fncbanco.Free;
	fncconta_banco.Free;
	{eliminando container de fun��es da memoria<<<}

	fbanco:=nil;
	Action:=cafree;
end;

procedure Tfbanco.FormCreate(Sender: TObject);
begin
//  sessao;
  { >>>container de fun��es }
  if FileExists(ExtractFilePath(Application.ExeName) + '\Tradu��oDev.ini') then
  begin
    dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName) +
      '\Tradu��oDev.ini');
    dm.cxLocalizer.LanguageIndex := 1;
    dm.cxLocalizer.Active := true;
  end;

  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
      {>>>container de fun��es}
      fncbanco:= TFuncoes.Create(Self);
      fncbanco.definirConexao(dm.conexao);
      fncbanco.definirConexaohistorico(dm.conexao);
      fncbanco.definirFormulario(Self);
      fncbanco.validarTransacaoRelacionamento:=true;
      fncbanco.autoAplicarAtualizacoesBancoDados:=true;

      fncconta_banco:= TFuncoes.Create(Self);
      fncconta_banco.definirConexao(dm.conexao);
      fncconta_banco.definirConexaohistorico(dm.conexao);
      fncconta_banco.definirFormulario(Self);
      fncconta_banco.validarTransacaoRelacionamento:=true;
      fncconta_banco.autoAplicarAtualizacoesBancoDados:=true;
      {container de fun��es<<<}

      {>>>verificar campos duplicidade}
      {verificar campos duplicidade<<<}

      {>>>gerando objetos de pesquisas}
      pesquisasItem:=TPesquisas.Create;
      pesquisasItem.agrupador:='pesID';
      pesquisasItem.titulo:='C�digo';
      pesquisasItem.campo:='ID';
      pesquisasItem.tipoFDB:='Integer';
      pesquisasItem.criterio:='=';
      pesquisasItem.intervalo:='S';
      pesquisasItem.tamanho:=72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect:='';
      pesquisasItem.lookupCampoID:='';
      pesquisasItem.lookupCampoTipoID:='Integer';
      pesquisasItem.lookupCampoDescricao:='';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[0]:=pesquisasItem;

      pesquisasItem:=TPesquisas.Create;
      pesquisasItem.agrupador:='pesDESCRICAO';
      pesquisasItem.titulo:='Descri��o';
      pesquisasItem.campo:='DESCRICAO';
      pesquisasItem.tipoFDB:='Varchar';
      pesquisasItem.criterio:='LIKE';
      pesquisasItem.intervalo:='N';
      pesquisasItem.tamanho:=480;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect:='';
      pesquisasItem.lookupCampoID:='';
      pesquisasItem.lookupCampoTipoID:='Integer';
      pesquisasItem.lookupCampoDescricao:='';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[1]:=pesquisasItem;

      { gerando campos no clientdatsaet de filtros }
      fncbanco.carregarfiltros(pesquisasLista, cdsfiltros, flwpOpcoes);
      { gerando campos no clientdatsaet de filtros }

      fncbanco.criarPesquisas(sqlbanco,cdsbanco,'select * from gobanco', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
      {gerando objetos de pesquisas<<<}

      {>>>campos obrigatorios banco}
      {campos obrigatorios banco<<<}

      {>>>objetos ativos no modo de manipula��o de dados banco}
      objetosModoEdicaoAtivosbanco[0]:=pnlDadosbanco;
      objetosModoEdicaoAtivosbanco[1]:=spdCancelarbanco;
      objetosModoEdicaoAtivosbanco[2]:=spdGravarbanco;
      {objetos ativos no modo de manipula��o de dados banco<<<}

      {>>>objetos inativos no modo de manipula��o de dados banco}
      objetosModoEdicaoInativosbanco[0]:=spdAdicionarbanco;
      objetosModoEdicaoInativosbanco[1]:=spdAlterarbanco;
      objetosModoEdicaoInativosbanco[2]:=spdExcluirbanco;
      objetosModoEdicaoInativosbanco[3]:=spdOpcoes;
      objetosModoEdicaoInativosbanco[4]:=tabPesquisas;
      {objetos inativos no modo de manipula��o de dados banco<<<}

      {>>>comando de adi��o de teclas de atalhos banco}
      fncbanco.criaAtalhoPopupMenu(Popupbanco,'Adicionar novo registro',VK_F4,spdAdicionarbanco.OnClick);
      fncbanco.criaAtalhoPopupMenu(Popupbanco,'Alterar registro selecionado',VK_F5,spdAlterarbanco.OnClick);
      fncbanco.criaAtalhoPopupMenu(Popupbanco,'Gravar �ltimas altera��es',VK_F6,spdGravarbanco.OnClick);
      fncbanco.criaAtalhoPopupMenu(Popupbanco,'Cancelar �ltimas altera��e',VK_F7,spdCancelarbanco.OnClick);
      fncbanco.criaAtalhoPopupMenu(Popupbanco,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirbanco.OnClick);

      {comando de adi��o de teclas de atalhos banco<<<}

      pgcPrincipal.ActivePageIndex:=0;

      {>>>abertura dos datasets}
      cdsbanco.open();
      {abertura dos datasets<<<}

      {>>>campos obrigatorios conta_banco}
      {campos obrigatorios conta_banco<<<}

      {>>>objetos ativos no modo de manipula��o de dados conta_banco}
      objetosModoEdicaoAtivosconta_banco[0]:=pnlDadosconta_banco;
      objetosModoEdicaoAtivosconta_banco[1]:=spdCancelarconta_banco;
      objetosModoEdicaoAtivosconta_banco[2]:=spdGravarconta_banco;
      {objetos ativos no modo de manipula��o de dados conta_banco<<<}

      {>>>objetos inativos no modo de manipula��o de dados conta_banco}
      objetosModoEdicaoInativosconta_banco[0]:=spdAdicionarconta_banco;
      objetosModoEdicaoInativosconta_banco[1]:=spdAlterarconta_banco;
      objetosModoEdicaoInativosconta_banco[2]:=spdExcluirconta_banco;
      objetosModoEdicaoInativosconta_banco[3]:=spdOpcoes;
      objetosModoEdicaoInativosconta_banco[4]:=tabPesquisas;
      {objetos inativos no modo de manipula��o de dados conta_banco<<<}

      {>>>comando de adi��o de teclas de atalhos conta_banco}
      fncconta_banco.criaAtalhoPopupMenu(Popupconta_banco,'Adicionar novo registro',VK_F4,spdAdicionarconta_banco.OnClick);
      fncconta_banco.criaAtalhoPopupMenu(Popupconta_banco,'Alterar registro selecionado',VK_F5,spdAlterarconta_banco.OnClick);
      fncconta_banco.criaAtalhoPopupMenu(Popupconta_banco,'Gravar �ltimas altera��es',VK_F6,spdGravarconta_banco.OnClick);
      fncconta_banco.criaAtalhoPopupMenu(Popupconta_banco,'Cancelar �ltimas altera��e',VK_F7,spdCancelarconta_banco.OnClick);
      fncconta_banco.criaAtalhoPopupMenu(Popupconta_banco,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirconta_banco.OnClick);

      {comando de adi��o de teclas de atalhos conta_banco<<<}

      pgcPrincipal.ActivePageIndex:=0;

      {>>>abertura dos datasets}
      cdsconta_banco.open();
      {abertura dos datasets<<<}

      fncbanco.criaAtalhoPopupMenuNavegacao(Popupbanco,cdsbanco);

      fncbanco.definirMascaraCampos(cdsbanco);

      fncbanco.configurarGridesFormulario(fbanco,false, true);

      fncconta_banco.definirMascaraCampos(cdsconta_banco);

     end);

end;

procedure Tfbanco.spdAdicionarbancoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabbanco.TabIndex;
	if (fncbanco.adicionar(cdsbanco)=true) then
		begin
			fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,true);
			dbebancodescricao.SetFocus;
	end
	else
		fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,false);
end;

procedure Tfbanco.spdAlterarbancoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabbanco.TabIndex;
	if (fncbanco.alterar(cdsbanco)=true) then
		begin
			fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,true);
			dbebancodescricao.SetFocus;
	end
	else
		fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,false);
end;

procedure Tfbanco.spdGravarbancoClick(Sender: TObject);
begin
	if (fncbanco.gravar(cdsbanco)=true) then
		fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,false)
	else
		fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,true);
end;

procedure Tfbanco.spdCancelarbancoClick(Sender: TObject);
begin
	if (fncbanco.cancelar(cdsbanco)=true) then
		fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,false)
	else
		fncbanco.ativarModoEdicao(objetosModoEdicaoAtivosbanco,objetosModoEdicaoInativosbanco,true);
end;

procedure Tfbanco.spdExcluirbancoClick(Sender: TObject);
begin
	fncbanco.excluir(cdsbanco);
end;

procedure Tfbanco.cdsbancoBeforePost(DataSet: TDataSet);
begin
	if cdsbanco.State in [dsinsert] then
	cdsbancoID.Value:=fncbanco.autoNumeracaoGenerator(dm.conexao,'G_banco');
end;



procedure Tfbanco.spdAdicionarconta_bancoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabconta_banco.TabIndex;
	if (fncconta_banco.adicionar(cdsconta_banco)=true) then
		begin
			fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,true);
			dbeconta_bancocodprojeto.SetFocus;
	end
	else
		fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,false);
end;

procedure Tfbanco.spdAlterarconta_bancoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabconta_banco.TabIndex;
	if (fncconta_banco.alterar(cdsconta_banco)=true) then
		begin
			fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,true);
			dbeconta_bancocodprojeto.SetFocus;
	end
	else
		fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,false);
end;

procedure Tfbanco.spdGravarconta_bancoClick(Sender: TObject);
begin
	if (fncconta_banco.gravar(cdsconta_banco)=true) then
		fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,false)
	else
		fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,true);
end;

procedure Tfbanco.spdCancelarconta_bancoClick(Sender: TObject);
begin
	if (fncconta_banco.cancelar(cdsconta_banco)=true) then
		fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,false)
	else
		fncconta_banco.ativarModoEdicao(objetosModoEdicaoAtivosconta_banco,objetosModoEdicaoInativosconta_banco,true);
end;

procedure Tfbanco.spdExcluirconta_bancoClick(Sender: TObject);
begin
	fncconta_banco.excluir(cdsconta_banco);
end;

procedure Tfbanco.cdsconta_bancoBeforePost(DataSet: TDataSet);
begin
	if cdsconta_banco.State in [dsinsert] then
	cdsconta_bancoID.Value:=fncconta_banco.autoNumeracaoGenerator(dm.conexao,'G_conta_banco');
end;



procedure Tfbanco.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fbanco.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfbanco.cdsbancoAfterOpen(DataSet: TDataSet);
begin
	cdsconta_banco.Open;
end;

procedure Tfbanco.cdsbancoAfterClose(DataSet: TDataSet);
begin
	cdsconta_banco.Close;
end;

procedure Tfbanco.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfbanco.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfbanco.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);
    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    try
      dm.conexao.Connected := true;
      dm.fdConexao.Connected := true;
    except
      Application.Terminate;
    end;
    if not (dm.conexao.Connected) then
    begin
      Application.MessageBox('Sess�o n�o estabelecida.', 'Sess�o', MB_OK + MB_ICONINFORMATION);
    end
    else
    begin
    end;
  except
    Application.Terminate;
  end;
end;

procedure Tfbanco.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsbanco.IsEmpty then
	fncbanco.visualizarRelatorios(frxbanco);
end;

end.

