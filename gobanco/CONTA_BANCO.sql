CREATE TABLE goCONTA_BANCO(
ID Integer   
,
CONSTRAINT pk_goCONTA_BANCO PRIMARY KEY (ID)
,
CODBANCO Integer   
,
CODPROJETO Integer   
,
PROJETO Varchar(250)   COLLATE PT_BR
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
DESCRICAO Varchar(250)   COLLATE PT_BR
,
AGENCIA Varchar(30)   COLLATE PT_BR
,
CC Varchar(30)   COLLATE PT_BR
,
TIPO Varchar(30)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_CONT_BANCO_ID ON CONTA_BANCO (ID);
CREATE ASC INDEX I02_CONT_BANCO_CODBANCO ON CONTA_BANCO (CODBANCO);
CREATE ASC INDEX I03_CONT_BANCO_CODPROJETO ON CONTA_BANCO (CODPROJETO);
CREATE ASC INDEX I04_CONT_BANCO_PROJETO ON CONTA_BANCO (PROJETO);
CREATE ASC INDEX I05_CONT_BANCO_DT_CADASTRO ON CONTA_BANCO (DT_CADASTRO);
CREATE ASC INDEX I06_CONT_BANCO_DESCRICAO ON CONTA_BANCO (DESCRICAO);
CREATE ASC INDEX I07_CONT_BANCO_AGENCIA ON CONTA_BANCO (AGENCIA);
CREATE ASC INDEX I08_CONT_BANCO_CC ON CONTA_BANCO (CC);
CREATE ASC INDEX I09_CONT_BANCO_TIPO ON CONTA_BANCO (TIPO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Cod.Banco' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'CODBANCO';
update rdb$relation_fields set rdb$description = 'Cod.Projeto' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'CODPROJETO';
update rdb$relation_fields set rdb$description = 'Projeto' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'PROJETO';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'DESCRICAO';
update rdb$relation_fields set rdb$description = 'Ag�ncia' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'AGENCIA';
update rdb$relation_fields set rdb$description = 'Conta' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'CC';
update rdb$relation_fields set rdb$description = 'Tipo' where rdb$relation_name = 'CONTA_BANCO' and rdb$field_name = 'TIPO';

commit;
CREATE GENERATOR G_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER BANCO_GEN FOR BANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_CONTA_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER CONTA_BANCO_GEN FOR goCONTA_BANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_CONTA_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
