program goBanco;

uses
  Vcl.Forms,
  ubanco in 'ubanco.pas' {fbanco},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Turquoise Gray');
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfbanco, fbanco);
  Application.Run;
end.
