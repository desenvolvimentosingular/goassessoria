unit ugosupervisor;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 09/08/2021*}                                          
{*Hora 20:25:41*}                                            
{*Unit gosupervisor *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr;

type                                                                                        
	Tfgosupervisor = class(TForm)
    sqlgosupervisor: TSQLDataSet;
	dspgosupervisor: TDataSetProvider;
	cdsgosupervisor: TClientDataSet;
	dsgosupervisor: TDataSource;
    sqlGOCONSULTOR: TSQLDataSet;
	dspGOCONSULTOR: TDataSetProvider;
	cdsGOCONSULTOR: TClientDataSet;
	dsGOCONSULTOR: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgosupervisor: TfrxReport;
    frxDBgoconsultor: TfrxDBDataset;
	Popupgosupervisor: TPopupMenu;
	Popupgoconsultor: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgosupervisor: tcxTabSheet;
	SCRgosupervisor: TScrollBox;
	pnlDadosgosupervisor: TPanel;
	{>>>bot�es de manipula��o de dados gosupervisor}
	pnlLinhaBotoesgosupervisor: TPanel;
	spdAdicionargosupervisor: TButton;
	spdAlterargosupervisor: TButton;
	spdGravargosupervisor: TButton;
	spdCancelargosupervisor: TButton;
	spdExcluirgosupervisor: TButton;

	{bot�es de manipula��o de dadosgosupervisor<<<}
	{campo ID}
	sqlgosupervisorID: TIntegerField;
	cdsgosupervisorID: TIntegerField;
	pnlLayoutLinhasgosupervisor1: TPanel;
	pnlLayoutTitulosgosupervisorid: TPanel;
	pnlLayoutCamposgosupervisorid: TPanel;
	dbegosupervisorid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgosupervisorDT_CADASTRO: TDateField;
	cdsgosupervisorDT_CADASTRO: TDateField;
	pnlLayoutLinhasgosupervisor2: TPanel;
	pnlLayoutTitulosgosupervisordt_cadastro: TPanel;
	pnlLayoutCamposgosupervisordt_cadastro: TPanel;
	dbegosupervisordt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgosupervisorHS_CADASTRO: TTimeField;
	cdsgosupervisorHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgosupervisor3: TPanel;
	pnlLayoutTitulosgosupervisorhs_cadastro: TPanel;
	pnlLayoutCamposgosupervisorhs_cadastro: TPanel;
	dbegosupervisorhs_cadastro: TDBEdit;

	{campo NOME}
	sqlgosupervisorNOME: TStringField;
	cdsgosupervisorNOME: TStringField;
	pnlLayoutLinhasgosupervisor4: TPanel;
	pnlLayoutTitulosgosupervisornome: TPanel;
	pnlLayoutCamposgosupervisornome: TPanel;
	dbegosupervisornome: TDBEdit;

	{campo CPF}
	sqlgosupervisorCPF: TStringField;
	cdsgosupervisorCPF: TStringField;
	pnlLayoutLinhasgosupervisor5: TPanel;
	pnlLayoutTitulosgosupervisorcpf: TPanel;
	pnlLayoutCamposgosupervisorcpf: TPanel;
	dbegosupervisorcpf: TDBEdit;

	{campo GERENTE}
	sqlgosupervisorGERENTE: TStringField;
	cdsgosupervisorGERENTE: TStringField;
	pnlLayoutLinhasgosupervisor6: TPanel;
	pnlLayoutTitulosgosupervisorgerente: TPanel;
	pnlLayoutCamposgosupervisorgerente: TPanel;
	dbegosupervisorgerente: TDBEdit;

	{campo EMAIL1}
	sqlgosupervisorEMAIL1: TStringField;
	cdsgosupervisorEMAIL1: TStringField;
	pnlLayoutLinhasgosupervisor7: TPanel;
	pnlLayoutTitulosgosupervisoremail1: TPanel;
	pnlLayoutCamposgosupervisoremail1: TPanel;
	dbegosupervisoremail1: TDBEdit;

	{campo EMAIL2}
	sqlgosupervisorEMAIL2: TStringField;
	cdsgosupervisorEMAIL2: TStringField;
	pnlLayoutLinhasgosupervisor8: TPanel;
	pnlLayoutTitulosgosupervisoremail2: TPanel;
	pnlLayoutCamposgosupervisoremail2: TPanel;
	dbegosupervisoremail2: TDBEdit;

	{campo TELEFONE1}
	sqlgosupervisorTELEFONE1: TStringField;
	cdsgosupervisorTELEFONE1: TStringField;
	pnlLayoutLinhasgosupervisor9: TPanel;
	pnlLayoutTitulosgosupervisortelefone1: TPanel;
	pnlLayoutCamposgosupervisortelefone1: TPanel;
	dbegosupervisortelefone1: TDBEdit;

	{campo TELEFONE2}
	sqlgosupervisorTELEFONE2: TStringField;
	cdsgosupervisorTELEFONE2: TStringField;
	pnlLayoutLinhasgosupervisor10: TPanel;
	pnlLayoutTitulosgosupervisortelefone2: TPanel;
	pnlLayoutCamposgosupervisortelefone2: TPanel;
	dbegosupervisortelefone2: TDBEdit;

	{campo STATUS}
	sqlgosupervisorSTATUS: TStringField;
	cdsgosupervisorSTATUS: TStringField;
	pnlLayoutLinhasgosupervisor11: TPanel;
	pnlLayoutTitulosgosupervisorstatus: TPanel;
	pnlLayoutCamposgosupervisorstatus: TPanel;
	cbogosupervisorstatus: TDBComboBox;
	tabgoconsultor: tcxTabSheet;
	SCRgoconsultor: TScrollBox;
	pnlDadosgoconsultor: TPanel;
	{>>>bot�es de manipula��o de dados goconsultor}
	pnlLinhaBotoesgoconsultor: TPanel;
	spdAdicionargoconsultor: TButton;
	spdAlterargoconsultor: TButton;
	spdGravargoconsultor: TButton;
	spdCancelargoconsultor: TButton;
	spdExcluirgoconsultor: TButton;

	{bot�es de manipula��o de dadosgoconsultor<<<}
	{campo ID}
	sqlgoconsultorID: TIntegerField;
	cdsgoconsultorID: TIntegerField;
	pnlLayoutLinhasgoconsultor1: TPanel;
	pnlLayoutTitulosgoconsultorid: TPanel;
	pnlLayoutCamposgoconsultorid: TPanel;
	dbegoconsultorid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoconsultorDT_CADASTRO: TDateField;
	cdsgoconsultorDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoconsultor2: TPanel;
	pnlLayoutTitulosgoconsultordt_cadastro: TPanel;
	pnlLayoutCamposgoconsultordt_cadastro: TPanel;
	dbegoconsultordt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoconsultorHS_CADASTRO: TTimeField;
	cdsgoconsultorHS_CADASTRO: TTimeField;

	{campo COD_SUPERVISOR}
	sqlgoconsultorCOD_SUPERVISOR: TIntegerField;
	cdsgoconsultorCOD_SUPERVISOR: TIntegerField;
	pnlLayoutLinhasgoconsultor4: TPanel;
	pnlLayoutTitulosgoconsultorcod_supervisor: TPanel;
	pnlLayoutCamposgoconsultorcod_supervisor: TPanel;
	dbegoconsultorcod_supervisor: TDBEdit;

	{campo SUPERVISOR}
	sqlgoconsultorSUPERVISOR: TStringField;
	cdsgoconsultorSUPERVISOR: TStringField;

	{campo NOME}
	sqlgoconsultorNOME: TStringField;
	cdsgoconsultorNOME: TStringField;
	pnlLayoutLinhasgoconsultor6: TPanel;
	pnlLayoutTitulosgoconsultornome: TPanel;
	pnlLayoutCamposgoconsultornome: TPanel;
	dbegoconsultornome: TDBEdit;

	{campo CPF}
	sqlgoconsultorCPF: TStringField;
	cdsgoconsultorCPF: TStringField;
	pnlLayoutLinhasgoconsultor7: TPanel;
	pnlLayoutTitulosgoconsultorcpf: TPanel;
	pnlLayoutCamposgoconsultorcpf: TPanel;
	dbegoconsultorcpf: TDBEdit;

	{campo E_MAIL1}
	sqlgoconsultorE_MAIL1: TStringField;
	cdsgoconsultorE_MAIL1: TStringField;

	{campo E_MAIL2}
	sqlgoconsultorE_MAIL2: TStringField;
	cdsgoconsultorE_MAIL2: TStringField;

	{campo CLASSIFICACAO}
	sqlgoconsultorCLASSIFICACAO: TStringField;
	cdsgoconsultorCLASSIFICACAO: TStringField;
	pnlLayoutLinhasgoconsultor10: TPanel;
	pnlLayoutTitulosgoconsultorclassificacao: TPanel;
	pnlLayoutCamposgoconsultorclassificacao: TPanel;
	cbogoconsultorclassificacao: TDBComboBox;

	{campo ENDERECO}
	sqlgoconsultorENDERECO: TStringField;
	cdsgoconsultorENDERECO: TStringField;
	pnlLayoutLinhasgoconsultor11: TPanel;
	pnlLayoutTitulosgoconsultorendereco: TPanel;
	pnlLayoutCamposgoconsultorendereco: TPanel;
	dbegoconsultorendereco: TDBEdit;

	{campo UF}
	sqlgoconsultorUF: TStringField;
	cdsgoconsultorUF: TStringField;

	{campo CIDADE}
	sqlgoconsultorCIDADE: TStringField;
	cdsgoconsultorCIDADE: TStringField;

	{campo CEP}
	sqlgoconsultorCEP: TStringField;
	cdsgoconsultorCEP: TStringField;
	pnlLayoutLinhasgoconsultor14: TPanel;
	pnlLayoutTitulosgoconsultorcep: TPanel;
	pnlLayoutCamposgoconsultorcep: TPanel;
	dbegoconsultorcep: TDBEdit;

	{campo STATUS}
	sqlgoconsultorSTATUS: TStringField;
	cdsgoconsultorSTATUS: TStringField;
	pnlLayoutLinhasgoconsultor15: TPanel;
	pnlLayoutTitulosgoconsultorstatus: TPanel;
	pnlLayoutCamposgoconsultorstatus: TPanel;
    Gridgoconsultor: TcxGrid;
    GridgoconsultorDBTV: TcxGridDBTableView;
    GRIDgoconsultorDBTVID: TcxGridDBColumn;
    GRIDgoconsultorDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDgoconsultorDBTVNOME: TcxGridDBColumn;
    GRIDgoconsultorDBTVE_MAIL1: TcxGridDBColumn;
    GRIDgoconsultorDBTVE_MAIL2: TcxGridDBColumn;
    GRIDgoconsultorDBTVCLASSIFICACAO: TcxGridDBColumn;
    GRIDgoconsultorDBTVENDERECO: TcxGridDBColumn;
    GRIDgoconsultorDBTVUF: TcxGridDBColumn;
    GRIDgoconsultorDBTVCIDADE: TcxGridDBColumn;
    GRIDgoconsultorDBTVCEP: TcxGridDBColumn;
    GRIDgoconsultorDBTVSTATUS: TcxGridDBColumn;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlLayoutTitulosgoconsultorhs_cadastro: TPanel;
    pnlLayoutCamposgoconsultorhs_cadastro: TPanel;
    dbegoconsultorHS_CADASTRO: TDBEdit;
    pnlLayoutCamposgoconsultorsupervisor: TPanel;
    dbegoconsultorSUPERVISOR: TDBEdit;
    cbogoconsultorSTATUS: TDBComboBox;
    GridgoconsultorLevel1: TcxGridLevel;
    pnlLayoutTitulosgoconsultore_mail1: TPanel;
    pnlLayoutCamposgoconsultore_mail1: TPanel;
    dbegoconsultorE_MAIL1: TDBEdit;
    pnlLayoutTitulosgoconsultore_mail2: TPanel;
    pnlLayoutCamposgoconsultore_mail2: TPanel;
    dbegoconsultorE_MAIL2: TDBEdit;
    pnlLayoutTitulosgoconsultoruf: TPanel;
    pnlLayoutCamposgoconsultoruf: TPanel;
    dbegoconsultorUF: TDBComboBox;
    pnlLayoutTitulosgoconsultorcidade: TPanel;
    pnlLayoutCamposgoconsultorcidade: TPanel;
    dbegoconsultorCIDADE: TDBEdit;
    cdsfiltros: TClientDataSet;
    frxDBDfiltros: TfrxDBDataset;
    spdDuplicargoconsultor: TButton;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure cdsgosupervisorAfterClose(DataSet: TDataSet);
	procedure cdsgosupervisorAfterOpen(DataSet: TDataSet);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargosupervisorClick(Sender: TObject);
	procedure spdAlterargosupervisorClick(Sender: TObject);
	procedure spdGravargosupervisorClick(Sender: TObject);
	procedure spdCancelargosupervisorClick(Sender: TObject);
	procedure spdExcluirgosupervisorClick(Sender: TObject);
	procedure spdAdicionargoconsultorClick(Sender: TObject);
	procedure spdAlterargoconsultorClick(Sender: TObject);
	procedure spdGravargoconsultorClick(Sender: TObject);
	procedure spdCancelargoconsultorClick(Sender: TObject);
	procedure spdExcluirgoconsultorClick(Sender: TObject);
	procedure grdregistrostbwgoconsultorDblClick(Sender: TObject);
    procedure cdsgoconsultorNewRecord(DataSet: TDataSet);
    procedure cdsgosupervisorNewRecord(DataSet: TDataSet);
    procedure spdDuplicargoconsultorClick(Sender: TObject);
    procedure dbegoconsultorCPFExit(Sender: TObject);
    procedure spdOpcoesClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgosupervisor: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgosupervisor: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgosupervisor: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgosupervisor: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagosupervisor: Array[0..0]  of TDuplicidade;
	duplicidadeCampogosupervisor: TDuplicidade;
	{container de funcoes}
	fncgoconsultor: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoconsultor: Array[0..2]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoconsultor: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoconsultor: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoconsultor: Array[0..0]  of TDuplicidade;
	duplicidadeCampogoconsultor: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure GridgoconsultorDblClick(Sender: TObject);
    procedure GridgosupervisorDblClick(Sender: TObject);
	public
  procedure sessao;

end;

var
fgosupervisor: Tfgosupervisor;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgosupervisor.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgosupervisor.grdregistrostbwgoconsultorDblClick(Sender: TObject);
begin
     try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);
    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    try
      dm.conexao.Connected := True;
      dm.fdConexao.Connected := True;
    except
      application.Terminate;
    end;
    if not (dm.conexao.Connected) then
    begin
      Application.MessageBox('Sess�o n�o estabelecida.', 'Sess�o', MB_OK + MB_ICONINFORMATION);
    end
    else
    begin
    end;
  except
    application.Terminate;
  end;
end;

procedure Tfgosupervisor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgosupervisor.verificarEmTransacao(cdsgosupervisor);
	fncgoconsultor.verificarEmTransacao(cdsgoconsultor);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgosupervisor.configurarGridesFormulario(fgosupervisor,true, false);

	fncgosupervisor.Free;
	fncgoconsultor.Free;
	{eliminando container de fun��es da memoria<<<}

	fgosupervisor:=nil;
	Action:=cafree;
end;

procedure Tfgosupervisor.FormCreate(Sender: TObject);
begin
//  sessao;
  {>>>container de tradu�a� de compomentes}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
   begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
   end;
  {>>>container de tradu�a� de compomentes}
	{>>>container de fun��es}
	fncgosupervisor:= TFuncoes.Create(Self);
	fncgosupervisor.definirConexao(dm.conexao);
	fncgosupervisor.definirConexaohistorico(dm.conexao);
	fncgosupervisor.definirFormulario(Self);
	fncgosupervisor.validarTransacaoRelacionamento:=true;
	fncgosupervisor.autoAplicarAtualizacoesBancoDados:=true;

	fncgoconsultor:= TFuncoes.Create(Self);
	fncgoconsultor.definirConexao(dm.conexao);
	fncgoconsultor.definirConexaohistorico(dm.conexao);
	fncgoconsultor.definirFormulario(Self);
	fncgoconsultor.validarTransacaoRelacionamento:=true;
	fncgoconsultor.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  lblTituloversao.Caption :=lblTituloversao.Caption+' - '+fncgoconsultor.recuperarVersaoExecutavel;
  lblusuario.Caption      :=lblusuario.Caption + ParamStr(3);

	{>>>verificar campos duplicidade}
  duplicidadeCampogosupervisor:=TDuplicidade.Create;
	duplicidadeCampogosupervisor.agrupador:='';
	duplicidadeCampogosupervisor.objeto :=dbegosupervisorCPF;
	duplicidadeListagosupervisor [0]:=duplicidadeCampogosupervisor;
	{verificar campos duplicidade<<<}

  {>>>verificar campos duplicidade}
  duplicidadeCampogoconsultor:=TDuplicidade.Create;
	duplicidadeCampogoconsultor.agrupador:='';
	duplicidadeCampogoconsultor.objeto :=dbegoconsultorCPF;
	duplicidadeListagoconsultor [0]:=duplicidadeCampogoconsultor;
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesNOME';
	pesquisasItem.titulo:='Nome';
	pesquisasItem.campo:='NOME';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCPF';
	pesquisasItem.titulo:='CPF';
	pesquisasItem.campo:='CPF';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesGERENTE';
	pesquisasItem.titulo:='Gerente';
	pesquisasItem.campo:='GERENTE';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesSTATUS';
	pesquisasItem.titulo:='Status';
	pesquisasItem.campo:='STATUS';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('ATIVO');
	pesquisasItem.comboItens.add('INATIVO');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncgosupervisor.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}
	fncgosupervisor.criarPesquisas(sqlgosupervisor,cdsgosupervisor,'select * from gosupervisor', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gosupervisor}
	camposObrigatoriosgosupervisor[0] :=dbegosupervisornome;
	camposObrigatoriosgosupervisor[1] :=dbegosupervisorcpf;
	{campos obrigatorios gosupervisor<<<}

	{>>>objetos ativos no modo de manipula��o de dados gosupervisor}
	objetosModoEdicaoAtivosgosupervisor[0]:=spdCancelargosupervisor;
	objetosModoEdicaoAtivosgosupervisor[1]:=spdGravargosupervisor;
	{objetos ativos no modo de manipula��o de dados gosupervisor<<<}

	{>>>objetos inativos no modo de manipula��o de dados gosupervisor}
	objetosModoEdicaoInativosgosupervisor[0]:=spdAdicionargosupervisor;
	objetosModoEdicaoInativosgosupervisor[1]:=spdAlterargosupervisor;
	objetosModoEdicaoInativosgosupervisor[2]:=spdExcluirgosupervisor;
	objetosModoEdicaoInativosgosupervisor[3]:=spdOpcoes;
	objetosModoEdicaoInativosgosupervisor[4]:=tabPesquisas;
  objetosModoEdicaoInativosgosupervisor[5]:=tabgoconsultor;
	{objetos inativos no modo de manipula��o de dados gosupervisor<<<}

	{>>>comando de adi��o de teclas de atalhos gosupervisor}
	fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Adicionar novo registro',VK_F4,spdAdicionargosupervisor.OnClick);
	fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Alterar registro selecionado',VK_F5,spdAlterargosupervisor.OnClick);
	fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Gravar �ltimas altera��es',VK_F6,spdGravargosupervisor.OnClick);
	fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Cancelar �ltimas altera��e',VK_F7,spdCancelargosupervisor.OnClick);
	fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgosupervisor.OnClick);

	{comando de adi��o de teclas de atalhos gosupervisor<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgosupervisor.open();
	{abertura dos datasets<<<}

	{>>>campos obrigatorios goconsultor}
	camposObrigatoriosgoconsultor[0] :=dbegoconsultornome;
	camposObrigatoriosgoconsultor[1] :=dbegoconsultorcpf;
	camposObrigatoriosgoconsultor[2] :=cbogoconsultorclassificacao;
	{campos obrigatorios goconsultor<<<}

	{>>>objetos ativos no modo de manipula��o de dados goconsultor}
	objetosModoEdicaoAtivosgoconsultor[0]:=spdCancelargoconsultor;
	objetosModoEdicaoAtivosgoconsultor[1]:=spdGravargoconsultor;
	{objetos ativos no modo de manipula��o de dados goconsultor<<<}

	{>>>objetos inativos no modo de manipula��o de dados goconsultor}
	objetosModoEdicaoInativosgoconsultor[0]:=spdAdicionargoconsultor;
	objetosModoEdicaoInativosgoconsultor[1]:=spdAlterargoconsultor;
	objetosModoEdicaoInativosgoconsultor[2]:=spdExcluirgoconsultor;
	objetosModoEdicaoInativosgoconsultor[3]:=spdOpcoes;
	objetosModoEdicaoInativosgoconsultor[4]:=tabPesquisas;
  objetosModoEdicaoInativosgoconsultor[5]:=tabgosupervisor;
	{objetos inativos no modo de manipula��o de dados goconsultor<<<}

	{>>>comando de adi��o de teclas de atalhos goconsultor}
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Adicionar novo registro',VK_F4,spdAdicionargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Alterar registro selecionado',VK_F5,spdAlterargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Gravar �ltimas altera��es',VK_F6,spdGravargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoconsultor.OnClick);
  fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Duplicar registro selecionado',ShortCut(VK_F12,[ssCtrl]),spdDuplicargoconsultor.OnClick);

	{comando de adi��o de teclas de atalhos goconsultor<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgoconsultor.open();
	{abertura dos datasets<<<}

	fncgosupervisor.criaAtalhoPopupMenuNavegacao(Popupgosupervisor,cdsgosupervisor);

	fncgosupervisor.definirMascaraCampos(cdsgosupervisor);

	fncgosupervisor.configurarGridesFormulario(fgosupervisor,false, true);

	fncgoconsultor.definirMascaraCampos(cdsgoconsultor);

end;

procedure Tfgosupervisor.spdAdicionargosupervisorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgosupervisor.TabIndex;
	if (fncgosupervisor.adicionar(cdsgosupervisor)=true) then
		begin
			fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
			dbegosupervisornome.SetFocus;
	end
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false);
end;

procedure Tfgosupervisor.spdAlterargosupervisorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgosupervisor.TabIndex;
	if (fncgosupervisor.alterar(cdsgosupervisor)=true) then
		begin
			fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
			dbegosupervisornome.SetFocus;
	end
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false);
end;

procedure Tfgosupervisor.spdGravargosupervisorClick(Sender: TObject);
begin
	fncgosupervisor.verificarCamposObrigatorios(cdsgosupervisor, camposObrigatoriosgosupervisor);
  fncgosupervisor.verificarDuplicidade(dm.FDConexao, cdsgosupervisor, 'gosupervisor', 'ID', duplicidadeListagosupervisor);
	if (fncgosupervisor.gravar(cdsgosupervisor)=true) then
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false)
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
end;

procedure Tfgosupervisor.spdCancelargosupervisorClick(Sender: TObject);
begin
	if (fncgosupervisor.cancelar(cdsgosupervisor)=true) then
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false)
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
end;

procedure Tfgosupervisor.spdDuplicargoconsultorClick(Sender: TObject);
begin
  fncgoconsultor.Duplicarregistro(cdsgoconsultor, true,'ID','G_goconsultor');
end;

procedure Tfgosupervisor.spdExcluirgosupervisorClick(Sender: TObject);
begin
	fncgosupervisor.excluir(cdsgosupervisor);
end;

procedure Tfgosupervisor.GridgosupervisorDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgosupervisor.TabIndex;
end;

procedure Tfgosupervisor.sessao;
begin
   try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;

end;

procedure Tfgosupervisor.spdAdicionargoconsultorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoconsultor.TabIndex;
	if (fncgoconsultor.adicionar(cdsgoconsultor)=true) then
		begin
			fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
			dbegoconsultorNOME.SetFocus;
	end
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false);
end;

procedure Tfgosupervisor.spdAlterargoconsultorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoconsultor.TabIndex;
	if (fncgoconsultor.alterar(cdsgoconsultor)=true) then
		begin
			fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
			dbegoconsultorNOME.SetFocus;
	end
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false);
end;

procedure Tfgosupervisor.spdGravargoconsultorClick(Sender: TObject);
begin
	fncgoconsultor.verificarCamposObrigatorios(cdsgoconsultor, camposObrigatoriosgoconsultor);
  fncgoconsultor.verificarDuplicidade(dm.FDConexao, cdsgoconsultor, 'goconsultor', 'ID', duplicidadeListagoconsultor);
	if (fncgoconsultor.gravar(cdsgoconsultor)=true) then
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false)
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
end;

procedure Tfgosupervisor.spdCancelargoconsultorClick(Sender: TObject);
begin
	if (fncgoconsultor.cancelar(cdsgoconsultor)=true) then
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false)
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
end;

procedure Tfgosupervisor.spdExcluirgoconsultorClick(Sender: TObject);
begin
	fncgoconsultor.excluir(cdsgoconsultor);
end;

procedure Tfgosupervisor.GridgoconsultorDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoconsultor.TabIndex;
end;

procedure Tfgosupervisor.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgosupervisor.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgosupervisor.cdsgosupervisorAfterOpen(DataSet: TDataSet);
begin
	cdsgoconsultor.Open;
end;

procedure Tfgosupervisor.cdsgosupervisorNewRecord(DataSet: TDataSet);
begin
	if cdsgosupervisor.State in [dsinsert] then
	cdsgosupervisorID.Value:=fncgosupervisor.autoNumeracaoGenerator(dm.conexao,'G_gosupervisor');
end;

procedure Tfgosupervisor.dbegoconsultorCPFExit(Sender: TObject);
begin
  if ((cdsgoconsultorCEP.AsString<>EmptyStr) and (cdsgoconsultor.State in [dsinsert, dsedit])) then
   begin
    dm.FDQuery.Close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.sql.Add('SELECT CIDADE, UF, BAIRRO, RUA FROM CEP WHERE CEP=:CEP');
    dm.FDQuery.ParamByName('CEP').AsString  := cdsgoconsultorCEP.AsString;
    dm.FDQuery.Open();
    cdsgoconsultorCIDADE.AsString  := dm.FDQuery.FieldByName('CIDADE').AsString;
    cdsgoconsultorUF.AsString  := dm.FDQuery.FieldByName('UF').AsString;
   end;
end;

procedure Tfgosupervisor.cdsgoconsultorNewRecord(DataSet: TDataSet);
begin
	if cdsgoconsultor.State in [dsinsert] then
	cdsgoconsultorID.Value:=fncgoconsultor.autoNumeracaoGenerator(dm.conexao,'G_goconsultor');
  cdsgoconsultorSUPERVISOR.AsString:= cdsgosupervisorNOME.AsString;

end;

procedure Tfgosupervisor.cdsgosupervisorAfterClose(DataSet: TDataSet);
begin
	cdsgoconsultor.Close;
end;

procedure Tfgosupervisor.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgosupervisor.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgosupervisor.menuImpRelSinteticoClick(Sender: TObject);
begin
  if not cdsgosupervisor.IsEmpty then
	fncgosupervisor.visualizarRelatorios(frxgosupervisor);
end;

end.

