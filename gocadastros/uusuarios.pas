unit uusuarios;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 14/02/2020*}                                          
{*Hora 21:08:36*}                                            
{*Unit usuarios *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, Vcl.Menus, frxClass, frxDBSet,
  dxGDIPlusClasses, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxDBEdit;

type                                                                                        
	Tfusuarios = class(TForm)
  sqlusuarios: TSQLDataSet;
	dspusuarios: TDataSetProvider;
	cdsusuarios: TClientDataSet;
	dsusuarios: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloPesquisa: TPanel;
	Popupusuarios: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabusuarios: tcxTabSheet;
	SCRusuarios: TScrollBox;
	pnlDadosusuarios: TPanel;
	{>>>bot�es de manipula��o de dados usuarios}
	pnlLinhaBotoesusuarios: TPanel;
	spdAdicionarusuarios: TButton;
	spdAlterarusuarios: TButton;
	spdGravarusuarios: TButton;
	spdCancelarusuarios: TButton;
	spdExcluirusuarios: TButton;
	cdsusuariosID_USUARIO: TIntegerField;
	pnlLayoutLinhasusuarios1: TPanel;
	pnlLayoutTitulosusuariosid_usuario: TPanel;
	pnlLayoutCamposusuariosid_usuario: TPanel;
	dbeusuariosid_usuario: TDBEdit;
	cdsusuariosDT_ULT_ACESSO: TDateField;
	pnlLayoutLinhasusuarios2: TPanel;
	pnlLayoutTitulosusuariosdt_ult_acesso: TPanel;
	pnlLayoutCamposusuariosdt_ult_acesso: TPanel;
	dbeusuariosdt_ult_acesso: TDBEdit;
	cdsusuariosHS_ULT_ACESSO: TTimeField;
	pnlLayoutLinhasusuarios3: TPanel;
	pnlLayoutTitulosusuarioshs_ult_acesso: TPanel;
	pnlLayoutCamposusuarioshs_ult_acesso: TPanel;
	dbeusuarioshs_ult_acesso: TDBEdit;
	cdsusuariosSOFTWARE: TStringField;
	pnlLayoutLinhasusuarios4: TPanel;
	pnlLayoutTitulosusuariossoftware: TPanel;
	pnlLayoutCamposusuariossoftware: TPanel;
	dbeusuariossoftware: TDBEdit;
	cdsusuariosNOME: TStringField;
	pnlLayoutLinhasusuarios5: TPanel;
	pnlLayoutTitulosusuariosnome: TPanel;
	pnlLayoutCamposusuariosnome: TPanel;
	dbeusuariosnome: TDBEdit;
	cdsusuariosUSUARIO: TStringField;
	pnlLayoutLinhasusuarios6: TPanel;
	pnlLayoutTitulosusuariosusuario: TPanel;
	pnlLayoutCamposusuariosusuario: TPanel;
	dbeusuariosusuario: TDBEdit;
	cdsusuariosSENHA: TStringField;
	pnlLayoutLinhasusuarios7: TPanel;
	pnlLayoutTitulosusuariossenha: TPanel;
	pnlLayoutCamposusuariossenha: TPanel;
	dbeusuariossenha: TDBEdit;
	cdsusuariosEMAIL: TStringField;
	pnlLayoutLinhasusuarios8: TPanel;
	pnlLayoutTitulosusuariosemail: TPanel;
	pnlLayoutCamposusuariosemail: TPanel;
	dbeusuariosemail: TDBEdit;
	cdsusuariosCOMPUTADOR: TStringField;
	pnlLayoutLinhasusuarios9: TPanel;
	pnlLayoutTitulosusuarioscomputador: TPanel;
	pnlLayoutCamposusuarioscomputador: TPanel;
	dbeusuarioscomputador: TDBEdit;
	cdsusuariosCOMPUTADOR_IP: TStringField;
	pnlLayoutLinhasusuarios10: TPanel;
	pnlLayoutTitulosusuarioscomputador_ip: TPanel;
	pnlLayoutCamposusuarioscomputador_ip: TPanel;
	dbeusuarioscomputador_ip: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlTituloResultadoPesquisa: TPanel;
    spdDuplicarusuarios: TButton;
    frxusuarios: TfrxReport;
    frxDBusuarios: TfrxDBDataset;
    cdsfiltros: TClientDataSet;
    frxDBDfiltros: TfrxDBDataset;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    pnlLayoutTitulosusuariosstatus: TPanel;
    pnlLayoutCamposusuariosstatus: TPanel;
    cdsusuariosCONECTADO: TStringField;
    cdsusuariosSTATUS: TStringField;
    pnlLayoutTitulosusuariosconectado: TPanel;
    pnlLayoutCamposusuariosconectado: TPanel;
    cmbstatus: TDBComboBox;
    cmbconectado: TDBComboBox;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionarusuariosClick(Sender: TObject);
	procedure spdAlterarusuariosClick(Sender: TObject);
	procedure spdGravarusuariosClick(Sender: TObject);
	procedure spdCancelarusuariosClick(Sender: TObject);
	procedure spdExcluirusuariosClick(Sender: TObject);
	procedure cdsusuariosBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure spdDuplicarusuariosClick(Sender: TObject);
    procedure spdExportacaoClick(Sender: TObject);
    procedure cdsusuariosNewRecord(DataSet: TDataSet);

    procedure frxusuariosBeforePrint(Sender: TfrxReportComponent);
    procedure spdOpcoesClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}


	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosusuarios: Array[0..4]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosusuarios: Array[0..10]  of TControl;
	objetosModoEdicaoInativosusuarios: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListausuarios: Array[0..0]  of TDuplicidade;
	duplicidadeCampousuarios: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
    procedure permissao_agrupada;
    procedure permissao;
	public
  fncusuarios: TFuncoes;
  const rotina='Cadastro de Usu�rio';

end;

var
fusuarios: Tfusuarios;

implementation

uses udm, uacesso;

{$R *.dfm}
//{$R logo.res}

procedure Tfusuarios.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfusuarios.frxusuariosBeforePrint(Sender: TfrxReportComponent);
begin
// carregarlogomarcarelatorio;
end;

procedure Tfusuarios.sessao;
begin
   try
    {dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);  }
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;

end;

procedure Tfusuarios.permissao_agrupada;
begin

  spdAdicionarusuarios.Enabled := fncusuarios.permissao('gousuarios', 'Adicionar', ParamStr(3));
  spdAlterarusuarios.Enabled := fncusuarios.permissao('gousuarios', 'Alterar', ParamStr(3));
  spdExcluirusuarios.Enabled := fncusuarios.permissao('gousuarios', 'Excluir', ParamStr(3));
  menuImpRelSintetico.Enabled := fncusuarios.permissao('gousuarios', 'Relatorio', ParamStr(3));
  Popupusuarios.Items[0].Enabled := fncusuarios.permissao('gousuarios', 'Adicionar', ParamStr(3));
  Popupusuarios.Items[1].Enabled := fncusuarios.permissao('gousuarios', 'Alterar', ParamStr(3));
  Popupusuarios.Items[4].Enabled := fncusuarios.permissao('gousuarios', 'Excluir', ParamStr(3));

end;

procedure Tfusuarios.permissao;
begin
  {comando de adi��o de teclas de atalhos usuarios<<<}
 { if fncusuarios.permissao('gousuarios', 'Acesso', ParamStr(3)) = false then
  begin
    application.MessageBox('Sem permiss�o de acesso.', 'Permiss�o', MB_ICONEXCLAMATION);
    close;
  end;}
  spdAdicionarusuarios.Enabled := fncusuarios.permissao('gousuarios', 'Adicionar', ParamStr(3));
  spdAlterarusuarios.Enabled := fncusuarios.permissao('gousuarios', 'Alterar', ParamStr(3));
  spdExcluirusuarios.Enabled := fncusuarios.permissao('gousuarios', 'Excluir', ParamStr(3));
  menuImpRelSintetico.Enabled := fncusuarios.permissao('gousuarios', 'Relatorio', ParamStr(3));
  Popupusuarios.Items[0].Enabled := fncusuarios.permissao('gousuarios', 'Adicionar', ParamStr(3));
  Popupusuarios.Items[1].Enabled := fncusuarios.permissao('gousuarios', 'Alterar', ParamStr(3));
  Popupusuarios.Items[4].Enabled := fncusuarios.permissao('gousuarios', 'Excluir', ParamStr(3));
end;

procedure Tfusuarios.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabusuarios.TabIndex;
end;

procedure Tfusuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fncusuarios.configurarGridesFormulario(fusuarios, true, false);
	{>>>verificando exist�ncia de registros em transa��o}
	fncusuarios.verificarEmTransacao(cdsusuarios);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
 	fncusuarios.Free;
	{eliminando container de fun��es da memoria<<<}

  fusuarios:=nil;
	Action:=cafree;

end;

procedure Tfusuarios.FormCreate(Sender: TObject);
begin

  //sessao;
   {>>>container de tradu�a� de compomentes}
   if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
   begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := true;
   end;
  {>>>container de tradu�a� de compomentes}

	{>>>container de fun��es}
	fncusuarios:= TFuncoes.Create(Self);
	fncusuarios.definirConexao(dm.conexao);
  fncusuarios.definirConexaohistorico(dm.conexao);
	fncusuarios.definirFormulario(Self);
	fncusuarios.validarTransacaoRelacionamento:=true;
	fncusuarios.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  {<<vers�o da rotina>>}
  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncusuarios.recuperarVersaoExecutavel;
  lblusuario.Caption := lblusuario.Caption + ParamStr(3);
  {<<vers�o da rotina>>}


	{>>>verificar campos duplicidade}
  {>>>verificar campos duplicidade}
  duplicidadeCampousuarios:=TDuplicidade.Create;
	duplicidadeCampousuarios.agrupador:='';
	duplicidadeCampousuarios.objeto :=dbeusuariosusuario;
  duplicidadeListausuarios [0]:=duplicidadeCampousuarios;
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesNOME';
	pesquisasItem.titulo:='Nome';
	pesquisasItem.campo:='NOME';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUSUARIO';
	pesquisasItem.titulo:='Usu�rio';
	pesquisasItem.campo:='USUARIO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncusuarios.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}


	fncusuarios.criarPesquisas(sqlusuarios,cdsusuarios,'select * from gousuarios', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios usuarios}
  	{>>>campos obrigatorios usuarios_historico}

	camposObrigatoriosusuarios[0] :=dbeusuariosNOME;
	camposObrigatoriosusuarios[1] :=dbeusuariosUSUARIO;
  camposObrigatoriosusuarios[2] :=dbeusuariosSENHA;
  camposObrigatoriosusuarios[4] :=dbeusuariosSOFTWARE;
	{campos obrigatorios usuarios_historico<<<}
	{campos obrigatorios usuarios<<<}
	{>>>objetos ativos no modo de manipula��o de dados usuarios}
	objetosModoEdicaoAtivosusuarios[0]:=spdCancelarusuarios;
	objetosModoEdicaoAtivosusuarios[1]:=spdGravarusuarios;
	{objetos ativos no modo de manipula��o de dados usuarios<<<}

	{>>>objetos inativos no modo de manipula��o de dados usuarios}
	objetosModoEdicaoInativosusuarios[0]:=spdAdicionarusuarios;
	objetosModoEdicaoInativosusuarios[1]:=spdAlterarusuarios;
	objetosModoEdicaoInativosusuarios[2]:=spdExcluirusuarios;
	objetosModoEdicaoInativosusuarios[3]:=spdOpcoes;
	objetosModoEdicaoInativosusuarios[4]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados usuarios<<<}

	{>>>comando de adi��o de teclas de atalhos usuarios}
	fncusuarios.criaAtalhoPopupMenu(Popupusuarios,'Adicionar novo registro',VK_F4,spdAdicionarusuarios.OnClick);
	fncusuarios.criaAtalhoPopupMenu(Popupusuarios,'Alterar registro selecionado',VK_F5,spdAlterarusuarios.OnClick);
	fncusuarios.criaAtalhoPopupMenu(Popupusuarios,'Gravar �ltimas altera��es',VK_F6,spdGravarusuarios.OnClick);
	fncusuarios.criaAtalhoPopupMenu(Popupusuarios,'Cancelar �ltimas altera��e',VK_F7,spdCancelarusuarios.OnClick);
	fncusuarios.criaAtalhoPopupMenu(Popupusuarios,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirusuarios.OnClick);
  fncusuarios.criaAtalhoPopupMenu(Popupusuarios,'DUPLICAR registro selecionado',ShortCut(VK_F12,[ssCtrl]),spdDuplicarusuarios.OnClick);

  permissao;

 	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsusuarios.open();
	{abertura dos datasets<<<}

	fncusuarios.criaAtalhoPopupMenuNavegacao(Popupusuarios,cdsusuarios);

	fncusuarios.definirMascaraCampos(cdsusuarios);

  fncusuarios.configurarGridesFormulario(fusuarios, false, true);

end;

procedure Tfusuarios.spdAdicionarusuariosClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabusuarios.TabIndex;
	if (fncusuarios.adicionar(cdsusuarios)=true) then
		begin
			fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,true);
			dbeusuariosNOME.SetFocus;
	end
	else
		fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,false);
end;

procedure Tfusuarios.spdAlterarusuariosClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabusuarios.TabIndex;
	if (fncusuarios.alterar(cdsusuarios)=true) then
		begin
			fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,true);
			dbeusuariosNOME.SetFocus;
	end
	else
		fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,false);
end;

procedure Tfusuarios.spdGravarusuariosClick(Sender: TObject);
begin
  fncusuarios.verificarCamposObrigatorios(cdsusuarios, camposObrigatoriosusuarios,pgcPrincipal);
  fncusuarios.verificarDuplicidade(dm.fdconexao ,cdsusuarios,'gousuarios','ID_USUARIO', duplicidadeListausuarios);

	if (fncusuarios.gravar(cdsusuarios)=true) then
  begin
		fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,false);
    permissao;
  end
	else
		fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,true);
end;

procedure Tfusuarios.spdCancelarusuariosClick(Sender: TObject);
begin
	if (fncusuarios.cancelar(cdsusuarios)=true) then
  begin
		fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,false);
    permissao;
  end
	else
		fncusuarios.ativarModoEdicao(objetosModoEdicaoAtivosusuarios,objetosModoEdicaoInativosusuarios,true);
end;

procedure Tfusuarios.spdDuplicarusuariosClick(Sender: TObject);
begin
 fncusuarios.Duplicarregistro(cdsusuarios,true,'ID_USUARIO');
end;

procedure Tfusuarios.spdExcluirusuariosClick(Sender: TObject);
begin
	fncusuarios.excluir(cdsusuarios);
end;

procedure Tfusuarios.spdExportacaoClick(Sender: TObject);
begin
 if not cdsusuarios.IsEmpty then
   fncusuarios.exportarGrides(fusuarios, GridResultadoPesquisa);

end;

procedure Tfusuarios.cdsusuariosBeforePost(DataSet: TDataSet);
begin
	if cdsusuarios.State in [dsinsert] then
	cdsusuariosID_USUARIO.Value:=fncusuarios.autoNumeracaoGenerator(dm.conexao,'G_usuarios');
end;


procedure Tfusuarios.cdsusuariosNewRecord(DataSet: TDataSet);
begin
  cdsusuariosSOFTWARE.AsString     :='goAssessoria';
  cdsusuariosCOMPUTADOR.AsString   := fncusuarios.recuperarNomeComputador;
  cdsusuariosCOMPUTADOR_IP.AsString:= fncusuarios.recuperarIP;

end;

procedure Tfusuarios.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fusuarios.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfusuarios.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Width, TSpeedButton(Sender).Height)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfusuarios.menuImpRelSinteticoClick(Sender: TObject);
begin
 fncusuarios.Relatorio(self, frxusuarios, cdsusuarios);
end;

end.

