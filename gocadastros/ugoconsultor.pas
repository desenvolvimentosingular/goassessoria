unit ugoconsultor;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 09/08/2021*}                                          
{*Hora 20:24:59*}                                            
{*Unit goconsultor *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa;

type                                                                                        
	Tfgoconsultor = class(TForm)
    sqlgoconsultor: TSQLDataSet;
	dspgoconsultor: TDataSetProvider;
	cdsgoconsultor: TClientDataSet;
	dsgoconsultor: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgoconsultor: TfrxReport;
	frxDBgoconsultor: TfrxDBDataset;
	Popupgoconsultor: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgoconsultor: tcxTabSheet;
	SCRgoconsultor: TScrollBox;
	pnlDadosgoconsultor: TPanel;
	{>>>bot�es de manipula��o de dados goconsultor}
	pnlLinhaBotoesgoconsultor: TPanel;
	spdAdicionargoconsultor: TButton;
	spdAlterargoconsultor: TButton;
	spdGravargoconsultor: TButton;
	spdCancelargoconsultor: TButton;
	spdExcluirgoconsultor: TButton;

	{bot�es de manipula��o de dadosgoconsultor<<<}
	{campo ID}
	sqlgoconsultorID: TIntegerField;
	cdsgoconsultorID: TIntegerField;
	pnlLayoutLinhasgoconsultor1: TPanel;
	pnlLayoutTitulosgoconsultorid: TPanel;
	pnlLayoutCamposgoconsultorid: TPanel;
	dbegoconsultorid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoconsultorDT_CADASTRO: TDateField;
	cdsgoconsultorDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoconsultor2: TPanel;
	pnlLayoutTitulosgoconsultordt_cadastro: TPanel;
	pnlLayoutCamposgoconsultordt_cadastro: TPanel;
	dbegoconsultordt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoconsultorHS_CADASTRO: TTimeField;
	cdsgoconsultorHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgoconsultor3: TPanel;
	pnlLayoutTitulosgoconsultorhs_cadastro: TPanel;
	pnlLayoutCamposgoconsultorhs_cadastro: TPanel;
	dbegoconsultorhs_cadastro: TDBEdit;

	{campo COD_SUPERVISOR}
	sqlgoconsultorCOD_SUPERVISOR: TIntegerField;
	cdsgoconsultorCOD_SUPERVISOR: TIntegerField;
	pnlLayoutLinhasgoconsultor4: TPanel;
	pnlLayoutTitulosgoconsultorcod_supervisor: TPanel;
	pnlLayoutCamposgoconsultorcod_supervisor: TPanel;
	dbegoconsultorcod_supervisor: TDBEdit;

	{campo SUPERVISOR}
	sqlgoconsultorSUPERVISOR: TStringField;
	cdsgoconsultorSUPERVISOR: TStringField;

	{campo NOME}
	sqlgoconsultorNOME: TStringField;
	cdsgoconsultorNOME: TStringField;
	pnlLayoutLinhasgoconsultor6: TPanel;
	pnlLayoutTitulosgoconsultornome: TPanel;
	pnlLayoutCamposgoconsultornome: TPanel;
	dbegoconsultornome: TDBEdit;

	{campo CPF}
	sqlgoconsultorCPF: TStringField;
	cdsgoconsultorCPF: TStringField;
	pnlLayoutLinhasgoconsultor7: TPanel;
	pnlLayoutTitulosgoconsultorcpf: TPanel;
	pnlLayoutCamposgoconsultorcpf: TPanel;
	dbegoconsultorcpf: TDBEdit;

	{campo E_MAIL1}
	sqlgoconsultorE_MAIL1: TStringField;
	cdsgoconsultorE_MAIL1: TStringField;
	pnlLayoutLinhasgoconsultor8: TPanel;
	pnlLayoutTitulosgoconsultore_mail1: TPanel;
	pnlLayoutCamposgoconsultore_mail1: TPanel;
	dbegoconsultore_mail1: TDBEdit;

	{campo E_MAIL2}
	sqlgoconsultorE_MAIL2: TStringField;
	cdsgoconsultorE_MAIL2: TStringField;
	pnlLayoutLinhasgoconsultor9: TPanel;
	pnlLayoutTitulosgoconsultore_mail2: TPanel;
	pnlLayoutCamposgoconsultore_mail2: TPanel;
	dbegoconsultore_mail2: TDBEdit;

	{campo CLASSIFICACAO}
	sqlgoconsultorCLASSIFICACAO: TStringField;
	cdsgoconsultorCLASSIFICACAO: TStringField;
	pnlLayoutLinhasgoconsultor10: TPanel;
	pnlLayoutTitulosgoconsultorclassificacao: TPanel;
	pnlLayoutCamposgoconsultorclassificacao: TPanel;
	cbogoconsultorclassificacao: TDBComboBox;

	{campo ENDERECO}
	sqlgoconsultorENDERECO: TStringField;
	cdsgoconsultorENDERECO: TStringField;
	pnlLayoutLinhasgoconsultor11: TPanel;
	pnlLayoutTitulosgoconsultorendereco: TPanel;
	pnlLayoutCamposgoconsultorendereco: TPanel;
	dbegoconsultorendereco: TDBEdit;

	{campo UF}
	sqlgoconsultorUF: TStringField;
	cdsgoconsultorUF: TStringField;
	pnlLayoutLinhasgoconsultor12: TPanel;
	pnlLayoutTitulosgoconsultoruf: TPanel;
	pnlLayoutCamposgoconsultoruf: TPanel;
	dbegoconsultoruf: TDBEdit;

	{campo CIDADE}
	sqlgoconsultorCIDADE: TStringField;
	cdsgoconsultorCIDADE: TStringField;
	pnlLayoutLinhasgoconsultor13: TPanel;
	pnlLayoutTitulosgoconsultorcidade: TPanel;
	pnlLayoutCamposgoconsultorcidade: TPanel;
	dbegoconsultorcidade: TDBEdit;

	{campo CEP}
	sqlgoconsultorCEP: TStringField;
	cdsgoconsultorCEP: TStringField;
	pnlLayoutLinhasgoconsultor14: TPanel;
	pnlLayoutTitulosgoconsultorcep: TPanel;
	pnlLayoutCamposgoconsultorcep: TPanel;
	dbegoconsultorcep: TDBEdit;

	{campo STATUS}
	sqlgoconsultorSTATUS: TStringField;
	cdsgoconsultorSTATUS: TStringField;
	pnlLayoutLinhasgoconsultor15: TPanel;
	pnlLayoutTitulosgoconsultorstatus: TPanel;
	pnlLayoutCamposgoconsultorstatus: TPanel;
	cbogoconsultorstatus: TDBComboBox;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    cdsfiltros: TClientDataSet;
    pnlLayoutCamposgoconsultorsupervisor: TPanel;
    dbegoconsultorSUPERVISOR: TDBEdit;
    psqsupervisor: TPesquisa;
    btnpesquisarsupervisor: TSpeedButton;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargoconsultorClick(Sender: TObject);
	procedure spdAlterargoconsultorClick(Sender: TObject);
	procedure spdGravargoconsultorClick(Sender: TObject);
	procedure spdCancelargoconsultorClick(Sender: TObject);
	procedure spdExcluirgoconsultorClick(Sender: TObject);
	procedure cdsgoconsultorBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgoconsultor: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoconsultor: Array[0..2]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoconsultor: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoconsultor: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoconsultor: Array[0..0]  of TDuplicidade;
	duplicidadeCampogoconsultor: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
	public

end;

var
fgoconsultor: Tfgoconsultor;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgoconsultor.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgoconsultor.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
   	pgcPrincipal.ActivePageIndex:=tabgoconsultor.TabIndex;
end;

procedure Tfgoconsultor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgoconsultor.verificarEmTransacao(cdsgoconsultor);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgoconsultor.configurarGridesFormulario(fgoconsultor,true, false);

	fncgoconsultor.Free;
	{eliminando container de fun��es da memoria<<<}

	fgoconsultor:=nil;
	Action:=cafree;
end;

procedure Tfgoconsultor.FormCreate(Sender: TObject);
begin
	{>>>container de fun��es}
	fncgoconsultor:= TFuncoes.Create(Self);
	fncgoconsultor.definirConexao(dm.conexao);
	fncgoconsultor.definirConexaohistorico(dm.conexao);
	fncgoconsultor.definirFormulario(Self);
	fncgoconsultor.validarTransacaoRelacionamento:=true;
	fncgoconsultor.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}



  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncgoconsultor.recuperarVersaoExecutavel;
  lblusuario.Caption := lblusuario.Caption + ParamStr(3);

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesNOME';
	pesquisasItem.titulo:='Nome';
	pesquisasItem.campo:='NOME';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesSTATUS';
	pesquisasItem.titulo:='Status';
	pesquisasItem.campo:='STATUS';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=60;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('ATIVO');
	pesquisasItem.comboItens.add('INATIVO');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;
  {gerando campos no clientdatsaet de filtros}
  fncgoconsultor.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncgoconsultor.criarPesquisas(sqlgoconsultor,cdsgoconsultor,'select * from goconsultor', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios goconsultor}
	camposObrigatoriosgoconsultor[0] :=dbegoconsultornome;
	camposObrigatoriosgoconsultor[1] :=dbegoconsultorcpf;
	camposObrigatoriosgoconsultor[2] :=cbogoconsultorclassificacao;
	{campos obrigatorios goconsultor<<<}

	{>>>objetos ativos no modo de manipula��o de dados goconsultor}
	objetosModoEdicaoAtivosgoconsultor[0]:=spdCancelargoconsultor;
	objetosModoEdicaoAtivosgoconsultor[1]:=spdGravargoconsultor;
  objetosModoEdicaoAtivosgoconsultor[2]:=dbegoconsultorCOD_SUPERVISOR;
  objetosModoEdicaoAtivosgoconsultor[3]:=btnpesquisarsupervisor;
	{objetos ativos no modo de manipula��o de dados goconsultor<<<}

	{>>>objetos inativos no modo de manipula��o de dados goconsultor}
	objetosModoEdicaoInativosgoconsultor[0]:=spdAdicionargoconsultor;
	objetosModoEdicaoInativosgoconsultor[1]:=spdAlterargoconsultor;
	objetosModoEdicaoInativosgoconsultor[2]:=spdExcluirgoconsultor;
	objetosModoEdicaoInativosgoconsultor[3]:=spdOpcoes;
	objetosModoEdicaoInativosgoconsultor[4]:=tabPesquisas;
  objetosModoEdicaoInativosgoconsultor[5]:=dbegoconsultorSUPERVISOR;
	{objetos inativos no modo de manipula��o de dados goconsultor<<<}

	{>>>comando de adi��o de teclas de atalhos goconsultor}
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Adicionar novo registro',VK_F4,spdAdicionargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Alterar registro selecionado',VK_F5,spdAlterargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Gravar �ltimas altera��es',VK_F6,spdGravargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoconsultor.OnClick);
	fncgoconsultor.criaAtalhoPopupMenu(Popupgoconsultor,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoconsultor.OnClick);

	{comando de adi��o de teclas de atalhos goconsultor<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgoconsultor.open();
	{abertura dos datasets<<<}

	fncgoconsultor.criaAtalhoPopupMenuNavegacao(Popupgoconsultor,cdsgoconsultor);

	fncgoconsultor.definirMascaraCampos(cdsgoconsultor);

	fncgoconsultor.configurarGridesFormulario(fgoconsultor,false, true);

end;

procedure Tfgoconsultor.spdAdicionargoconsultorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoconsultor.TabIndex;
	if (fncgoconsultor.adicionar(cdsgoconsultor)=true) then
		begin
			fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
			dbegoconsultorCOD_SUPERVISOR.SetFocus;
	end
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false);
end;

procedure Tfgoconsultor.spdAlterargoconsultorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoconsultor.TabIndex;
	if (fncgoconsultor.alterar(cdsgoconsultor)=true) then
		begin
			fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
			dbegoconsultorCOD_SUPERVISOR.SetFocus;
	end
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false);
end;

procedure Tfgoconsultor.spdGravargoconsultorClick(Sender: TObject);
begin
	fncgoconsultor.verificarCamposObrigatorios(cdsgoconsultor, camposObrigatoriosgoconsultor);

	if (fncgoconsultor.gravar(cdsgoconsultor)=true) then
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false)
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
end;

procedure Tfgoconsultor.spdCancelargoconsultorClick(Sender: TObject);
begin
	if (fncgoconsultor.cancelar(cdsgoconsultor)=true) then
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,false)
	else
		fncgoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoconsultor,objetosModoEdicaoInativosgoconsultor,true);
end;

procedure Tfgoconsultor.spdExcluirgoconsultorClick(Sender: TObject);
begin
	fncgoconsultor.excluir(cdsgoconsultor);
end;

procedure Tfgoconsultor.cdsgoconsultorBeforePost(DataSet: TDataSet);
begin
	if cdsgoconsultor.State in [dsinsert] then
	cdsgoconsultorID.Value:=fncgoconsultor.autoNumeracaoGenerator(dm.conexao,'G_goconsultor');
end;



procedure Tfgoconsultor.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgoconsultor.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgoconsultor.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgoconsultor.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgoconsultor.IsEmpty then 
	fncgoconsultor.visualizarRelatorios(frxgoconsultor);
end;

end.

