unit uprincipal;

{ *Autor Singular Solu��o Ind�stria - Waldes Alves * }
{ *Data 26/11/2021* }
{ *Hora 01:05:33* }
{ *Unit principal * }

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, uFuncoes, dxGDIPlusClasses, Vcl.ExtCtrls, Vcl.Buttons,
  Vcl.StdCtrls, ugorelatorios_automaticos,ShellAPi;

type
  Tfprincipal = class(TForm)
    menuPrincipal: TMainMenu;
    menuCadastro: TMenuItem;
    menucad_banco: TMenuItem;
    menucad_gocarteira: TMenuItem;
    menucad_gocliente: TMenuItem;
    menucad_goconsultor: TMenuItem;
    menucad_gofilial: TMenuItem;
    menucad_gogrupoproduto: TMenuItem;
    menucad_gogrupoprojeto: TMenuItem;
    menucad_gomoeda: TMenuItem;
    menucad_goportador: TMenuItem;
    menucad_goprojetos: TMenuItem;
    Empresa1: TMenuItem;
    menu_supervisor: TMenuItem;
    menu_Usurios: TMenuItem;
    Ajustes1: TMenuItem;
    Fechartodos1: TMenuItem;
    Parmetros1: TMenuItem;
    imglogo: TImage;
    pnlmenubase: TPanel;
    pnlopcao: TPanel;
    pnldiv: TPanel;
    spbprojetos: TSpeedButton;
    sbpgrupprojeto: TSpeedButton;
    spbbancos: TSpeedButton;
    spbcarteira: TSpeedButton;
    spbClientes: TSpeedButton;
    spbConsultor: TSpeedButton;
    spbfilial: TSpeedButton;
    spbmoeda: TSpeedButton;
    spbportador: TSpeedButton;
    spbusuarios: TSpeedButton;
    spbgrupoproduto: TSpeedButton;
    sbpopcao: TSpeedButton;
    mmoculto: TMainMenu;
    spbrelatorios: TSpeedButton;
    spbhistorico: TSpeedButton;
    menuhistorico: TMenuItem;
    procedure menucad_bancoClick(Sender: TObject);
    procedure menucad_gocarteiraClick(Sender: TObject);
    procedure menucad_goclienteClick(Sender: TObject);
    procedure menucad_goconsultorClick(Sender: TObject);
    procedure menucad_gofilialClick(Sender: TObject);
    procedure menucad_gogrupoprodutoClick(Sender: TObject);
    procedure menucad_gomoedaClick(Sender: TObject);
    procedure menucad_goportadorClick(Sender: TObject);
    procedure menucad_goprojetosClick(Sender: TObject);
    procedure menucad_gosupervisorClick(Sender: TObject);
    procedure Empresa1Click(Sender: TObject);
    procedure menu_supervisorClick(Sender: TObject);
    procedure menu_UsuriosClick(Sender: TObject);
    procedure menucad_gogrupoprojetoClick(Sender: TObject);
    procedure Fechartodos1Click(Sender: TObject);
    procedure Parmetros1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbpgrupprojetoClick(Sender: TObject);
    procedure spbprojetosClick(Sender: TObject);
    procedure spbbancosClick(Sender: TObject);
    procedure spbcarteiraClick(Sender: TObject);
    procedure spbmoedaClick(Sender: TObject);
    procedure spbConsultorClick(Sender: TObject);
    procedure spbfilialClick(Sender: TObject);
    procedure spbportadorClick(Sender: TObject);
    procedure spbusuariosClick(Sender: TObject);
    procedure spbgrupoprodutoClick(Sender: TObject);
    procedure sbpopcaoClick(Sender: TObject);
    procedure spbClientesClick(Sender: TObject);
    procedure spbrelatoriosClick(Sender: TObject);
    procedure menuhistoricoClick(Sender: TObject);
    procedure spbhistoricoClick(Sender: TObject);
  private
    { container de funcoes }
    fncPrincipal: TFuncoes;
    procedure menu_retraido;
  public
    { Public declarations }
    posicao_menu : boolean;
  end;

var
  fprincipal: Tfprincipal;

implementation

uses udm, ubanco, uempresa, ugocarteira, ugocliente, ugoconsultor, ugofilial,
  ugogrupoproduto, ugomoeda, ugoportador, ugoprojetos, ugosupervisor, ulogin,
  upreview, uprocesso, utrocarsenha, uusuarios, Vcl.DialogMessage,
  ugogrupoprojeto, ugoparametro;

{$R *.dfm}

procedure Tfprincipal.Empresa1Click(Sender: TObject);
begin
   if (fempresa = nil) then Application.CreateForm(Tfempresa, fempresa);
   fempresa.Show;
end;

procedure Tfprincipal.Fechartodos1Click(Sender: TObject);
var i : integer;
begin
  for i := self.MDIChildCount -1 downto 0 do self.MDIChildren[i].Close;
end;

procedure Tfprincipal.FormCreate(Sender: TObject);
begin
  if ((ParamStr(3)='NATALIA.BETA') or  (ParamStr(3)='INTEGRACAO'))  then
   Parmetros1.Enabled  :=true
  else
   Parmetros1.Enabled  :=false;

end;

procedure Tfprincipal.menucad_bancoClick(Sender: TObject);
begin
  if (fbanco = nil) then Application.CreateForm(Tfbanco, fbanco);
  fbanco.Show;
end;

procedure Tfprincipal.menucad_gocarteiraClick(Sender: TObject);
begin
  if (fgocarteira = nil) then
    Application.CreateForm(Tfgocarteira, fgocarteira);
  fgocarteira.Show;
end;

procedure Tfprincipal.menucad_goclienteClick(Sender: TObject);
begin
  if (fgocliente = nil) then
    Application.CreateForm(Tfgocliente, fgocliente);
  fgocliente.Show;
end;

procedure Tfprincipal.menucad_goconsultorClick(Sender: TObject);
begin
  if (fgoconsultor = nil) then
    Application.CreateForm(Tfgoconsultor, fgoconsultor);
  fgoconsultor.Show;
end;

procedure Tfprincipal.menucad_gofilialClick(Sender: TObject);
begin
  if (fgofilial = nil) then
    Application.CreateForm(Tfgofilial, fgofilial);
  fgofilial.Show;
end;

procedure Tfprincipal.menucad_gogrupoprodutoClick(Sender: TObject);
begin
  if (fgogrupoproduto = nil) then
    Application.CreateForm(Tfgogrupoproduto, fgogrupoproduto);
  fgogrupoproduto.Show;
end;

procedure Tfprincipal.menucad_gogrupoprojetoClick(Sender: TObject);
begin
 if (fgogrupoprojeto = nil) then Application.CreateForm(Tfgogrupoprojeto, fgogrupoprojeto);
  fgogrupoprojeto.Show;
end;

procedure Tfprincipal.menucad_gomoedaClick(Sender: TObject);
begin
  if (fgomoeda = nil) then Application.CreateForm(Tfgomoeda, fgomoeda);
  fgomoeda.Show;
end;

procedure Tfprincipal.menucad_goportadorClick(Sender: TObject);
begin
  if (fgoportador = nil) then Application.CreateForm(Tfgoportador, fgoportador);
  fgoportador.Show;
end;

procedure Tfprincipal.menucad_goprojetosClick(Sender: TObject);
begin
  if (fgoprojetos = nil) then
    Application.CreateForm(Tfgoprojetos, fgoprojetos);
  fgoprojetos.Show;
end;

procedure Tfprincipal.menucad_gosupervisorClick(Sender: TObject);
begin
  if (fgosupervisor = nil) then
    Application.CreateForm(Tfgosupervisor, fgosupervisor);
  fgosupervisor.Show;
end;

procedure Tfprincipal.menuhistoricoClick(Sender: TObject);
begin
   try
    fncPrincipal.KillTask('historico_usuario.exe');
    if FileExists(ExtractFilePath(Application.ExeName)+'\executaveis\historico_usuario.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName) +'\executaveis\historico_usuario.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']), nil, SW_SHOWNORMAL);
      //ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName) +'executaveis\'+TButton(sender).Name + '.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure Tfprincipal.Parmetros1Click(Sender: TObject);
begin
  if (fgoparametro = nil) then Application.CreateForm(Tfgoparametro, fgoparametro);
   fgoparametro.Show;
end;

procedure Tfprincipal.sbpgrupprojetoClick(Sender: TObject);
begin
 menucad_gogrupoprojeto.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbbancosClick(Sender: TObject);
begin
 menucad_banco.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbcarteiraClick(Sender: TObject);
begin
 menucad_gocarteira.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbClientesClick(Sender: TObject);
begin
 menucad_gocliente.Click;
  menu_retraido;
end;

procedure Tfprincipal.spbConsultorClick(Sender: TObject);
begin
 menu_supervisor.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbfilialClick(Sender: TObject);
begin
 menucad_gofilial.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbgrupoprodutoClick(Sender: TObject);
begin
 menucad_gogrupoproduto.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbhistoricoClick(Sender: TObject);
begin
 menuhistorico.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbmoedaClick(Sender: TObject);
begin
 menucad_gomoeda.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbportadorClick(Sender: TObject);
begin
 menucad_goportador.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbprojetosClick(Sender: TObject);
begin
 menucad_goprojetos.Click;
 menu_retraido;
end;

procedure Tfprincipal.spbrelatoriosClick(Sender: TObject);
begin
  if (fgorelatorios_automaticos = nil) then Application.CreateForm(Tfgorelatorios_automaticos, fgorelatorios_automaticos);
   fgorelatorios_automaticos.Show;
end;

procedure Tfprincipal.spbusuariosClick(Sender: TObject);
begin
 menu_Usurios.Click;
 menu_retraido;
end;

procedure Tfprincipal.sbpopcaoClick(Sender: TObject);
begin
 if posicao_menu  then
  pnlmenubase.Width:=28
 else
 pnlmenubase.Width:=120;

 posicao_menu := not posicao_menu;

end;

procedure Tfprincipal.menu_retraido;
begin
  pnlmenubase.Width := 30;
end;

procedure Tfprincipal.menu_supervisorClick(Sender: TObject);
begin
   if (fgosupervisor = nil) then Application.CreateForm(Tfgosupervisor, fgosupervisor);
   fgosupervisor.Show;
end;

procedure Tfprincipal.menu_UsuriosClick(Sender: TObject);
begin
  if (fusuarios = nil) then Application.CreateForm(Tfusuarios,fusuarios);
   fusuarios.Show;
end;

end.
