unit uusuarios_historico;

{*Autor- Waldes Alves de Souza *}
{*Data 19/10/2019*}
{*Hora 22:38:39*}
{*Unit usuarios_historico *}

interface

uses
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider,
Buttons, ComCtrls, uFuncoes,
FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
FireDAC.Comp.DataSet, FireDAC.Comp.Client,midaslib,
cxGraphics, cxControls, cxLookAndFeels, cxPC,
cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
Grids, Menus, dxBarBuiltInMenu, cxGrid,Clipbrd, Data.SqlExpr,cxGridExportLink, Winapi.ShellAPI,
  dxGDIPlusClasses, cxSplitter, frxClass, frxDBSet, uacesso;

type
	Tfusuarios_historico = class(TForm)
    sqlusuarios_historico: TSQLDataSet;
	dspusuarios_historico: TDataSetProvider;
	cdsusuarios_historico: TClientDataSet;
	dsusuarios_historico: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	Popupusuarios_historico: TPopupMenu;
	{objetos gerais<<<}

	{>>>objetos de manipula��o de dados}
	pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabusuarios_historico: tcxTabSheet;
	SCRusuarios_historico: TScrollBox;
	pnlDadosusuarios_historico: TPanel;
	{>>>bot�es de manipula��o de dados usuarios_historico}
	pnlLinhaBotoesusuarios_historico: TPanel;
	spdAdicionarusuarios_historico: TButton;
	spdAlterarusuarios_historico: TButton;
	spdGravarusuarios_historico: TButton;
	spdCancelarusuarios_historico: TButton;
	spdExcluirusuarios_historico: TButton;

	{bot�es de manipula��o de dadosusuarios_historico<<<}
	{campo ID}
	sqlusuarios_historicoID: TIntegerField;
	cdsusuarios_historicoID: TIntegerField;
	pnlLayoutLinhasusuarios_historico1: TPanel;
	pnlLayoutTitulosusuarios_historicoid: TPanel;
	pnlLayoutCamposusuarios_historicoid: TPanel;
	dbeusuarios_historicoid: TDBEdit;

	{campo ID_USUARIO}
	sqlusuarios_historicoID_USUARIO: TIntegerField;
	cdsusuarios_historicoID_USUARIO: TIntegerField;
	pnlLayoutLinhasusuarios_historico2: TPanel;
	pnlLayoutTitulosusuarios_historicoid_usuario: TPanel;
	pnlLayoutCamposusuarios_historicoid_usuario: TPanel;
	dbeusuarios_historicoid_usuario: TDBEdit;

	{campo USUARIO}
	sqlusuarios_historicoUSUARIO: TStringField;
	cdsusuarios_historicoUSUARIO: TStringField;
	pnlLayoutLinhasusuarios_historico3: TPanel;
	pnlLayoutTitulosusuarios_historicousuario: TPanel;
	pnlLayoutCamposusuarios_historicousuario: TPanel;
	dbeusuarios_historicousuario: TDBEdit;

	{campo DT_MOVIMENTO}
	sqlusuarios_historicoDT_MOVIMENTO: TDateField;
	cdsusuarios_historicoDT_MOVIMENTO: TDateField;
	pnlLayoutLinhasusuarios_historico4: TPanel;
	pnlLayoutTitulosusuarios_historicodt_movimento: TPanel;
	pnlLayoutCamposusuarios_historicodt_movimento: TPanel;
	dbeusuarios_historicodt_movimento: TDBEdit;

	{campo HS_MOVIMENTO}
	sqlusuarios_historicoHS_MOVIMENTO: TTimeField;
	cdsusuarios_historicoHS_MOVIMENTO: TTimeField;
	pnlLayoutLinhasusuarios_historico5: TPanel;
	pnlLayoutTitulosusuarios_historicohs_movimento: TPanel;
	pnlLayoutCamposusuarios_historicohs_movimento: TPanel;
	dbeusuarios_historicohs_movimento: TDBEdit;

	{campo MODULO_NOME}
	sqlusuarios_historicoMODULO_NOME: TStringField;
	cdsusuarios_historicoMODULO_NOME: TStringField;
	pnlLayoutLinhasusuarios_historico6: TPanel;
	pnlLayoutTitulosusuarios_historicomodulo_nome: TPanel;
	pnlLayoutCamposusuarios_historicomodulo_nome: TPanel;
	dbeusuarios_historicomodulo_nome: TDBEdit;

	{campo MODULO_TITULO}
	sqlusuarios_historicoMODULO_TITULO: TStringField;
	cdsusuarios_historicoMODULO_TITULO: TStringField;
	pnlLayoutLinhasusuarios_historico7: TPanel;
	pnlLayoutTitulosusuarios_historicomodulo_titulo: TPanel;
	pnlLayoutCamposusuarios_historicomodulo_titulo: TPanel;
	dbeusuarios_historicomodulo_titulo: TDBEdit;
	pnlLayoutLinhasusuarios_historico8: TPanel;
	pnlLayoutTitulosusuarios_historicodados_anteriores: TPanel;
	pnlLayoutCamposusuarios_historicodados_anteriores: TPanel;
	pnlLayoutLinhasusuarios_historico9: TPanel;
	pnlLayoutTitulosusuarios_historicodados_posteriores: TPanel;
	pnlLayoutCamposusuarios_historicodados_posteriores: TPanel;

	{campo COMPUTADOR}
	sqlusuarios_historicoCOMPUTADOR: TStringField;
	cdsusuarios_historicoCOMPUTADOR: TStringField;
	pnlLayoutLinhasusuarios_historico10: TPanel;
	pnlLayoutTitulosusuarios_historicocomputador: TPanel;
	pnlLayoutCamposusuarios_historicocomputador: TPanel;
	dbeusuarios_historicocomputador: TDBEdit;

	{campo COMPUTADOR_IP}
	sqlusuarios_historicoCOMPUTADOR_IP: TStringField;
	cdsusuarios_historicoCOMPUTADOR_IP: TStringField;
	pnlLayoutLinhasusuarios_historico11: TPanel;
	pnlLayoutTitulosusuarios_historicocomputador_ip: TPanel;
	pnlLayoutCamposusuarios_historicocomputador_ip: TPanel;
	dbeusuarios_historicocomputador_ip: TDBEdit;

	{campo TIPO}
	sqlusuarios_historicoTIPO: TStringField;
	cdsusuarios_historicoTIPO: TStringField;
	pnlLayoutLinhasusuarios_historico12: TPanel;
	pnlLayoutTitulosusuarios_historicotipo: TPanel;
	pnlLayoutCamposusuarios_historicotipo: TPanel;
	dbeusuarios_historicotipo: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    dbeusuarios_historicoDADOS_ANTERIORES: TDBMemo;
    dbeusuarios_historicoDADOS_POSTERIORES: TDBMemo;
    sqlusuarios_historicoDADOS_ANTERIORES: TStringField;
    sqlusuarios_historicoDADOS_POSTERIORES: TStringField;
    cdsusuarios_historicoDADOS_ANTERIORES: TStringField;
    cdsusuarios_historicoDADOS_POSTERIORES: TStringField;
    sptipesquisa: TcxSplitter;
    PopupOpcao: TPopupMenu;
    ExportaoXLS1: TMenuItem;
    cdsfiltros: TClientDataSet;
    frxDBDfiltros: TfrxDBDataset;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionarusuarios_historicoClick(Sender: TObject);
	procedure spdAlterarusuarios_historicoClick(Sender: TObject);
	procedure spdGravarusuarios_historicoClick(Sender: TObject);
	procedure spdCancelarusuarios_historicoClick(Sender: TObject);
	procedure spdExcluirusuarios_historicoClick(Sender: TObject);
	procedure cdsusuarios_historicoBeforePost(DataSet: TDataSet);

    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure Gridusuarios_historicoDBTVDblClick(Sender: TObject);
    procedure exportargride(formulario : string);
    procedure spdOpcoesClick(Sender: TObject);
    procedure ExportaoXLS1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}


	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosusuarios_historico: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosusuarios_historico: Array[0..10]  of TControl;
	objetosModoEdicaoInativosusuarios_historico: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListausuarios_historico: Array[0..0]  of TDuplicidade;
	duplicidadeCampousuarios_historico: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
  procedure WMMove(var Msg: TWMMove); message WM_MOVE;
    procedure sessao;
	public

  fncusuarios_historico: TFuncoes;
  const rotina='Hist�rico de Usu�rios';

end;

var
fusuarios_historico: Tfusuarios_historico;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfusuarios_historico.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfusuarios_historico.FormShow(Sender: TObject);
begin
 if (facesso = nil) then Application.CreateForm(Tfacesso, facesso);
   facesso.ShowModal;
  if facesso.ModalResult=mrOk then  facesso.Destroy
  else
   application.Terminate;
end;

procedure Tfusuarios_historico.GridResultadoPesquisaDBTVDblClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabusuarios_historico.TabIndex;
end;

procedure Tfusuarios_historico.Gridusuarios_historicoDBTVDblClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabusuarios_historico.TabIndex;
end;

procedure Tfusuarios_historico.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fncusuarios_historico.configurarGridesFormulario(fusuarios_historico, true, false);
	{>>>verificando exist�ncia de registros em transa��o}
	fncusuarios_historico.verificarEmTransacao(cdsusuarios_historico);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncusuarios_historico.Free;
	{eliminando container de fun��es da memoria<<<}

	fusuarios_historico:=nil;
	Action:=cafree;
end;

procedure Tfusuarios_historico.FormCreate(Sender: TObject);
begin
  sessao;
   {>>>container de tradu�a� de compomentes}
    if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
    begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
    end;
  {>>>container de tradu�a� de compomentes}
	{>>>container de fun��es}
	fncusuarios_historico:= TFuncoes.Create(Self);
	fncusuarios_historico.definirConexao(dm.conexao);
	fncusuarios_historico.definirFormulario(Self);
	fncusuarios_historico.validarTransacaoRelacionamento:=true;
	fncusuarios_historico.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  {<<vers�o da rotina>>}
  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncusuarios_historico.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}


	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID_USUARIO';
	pesquisasItem.titulo:='C�digo usu�rio';
	pesquisasItem.campo:='ID_USUARIO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUSUARIO';
	pesquisasItem.titulo:='Usu�rio';
	pesquisasItem.campo:='USUARIO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=240;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_MOVIMENTO';
	pesquisasItem.titulo:='Dt.movimento';
	pesquisasItem.campo:='DT_MOVIMENTO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesHS_MOVIMENTO';
	pesquisasItem.titulo:='Hs.movimento';
	pesquisasItem.campo:='HS_MOVIMENTO';
	pesquisasItem.tipoFDB:='Time';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesMODULO_NOME';
	pesquisasItem.titulo:='M�dulo';
	pesquisasItem.campo:='MODULO_NOME';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=240;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesMODULO_TITULO';
	pesquisasItem.titulo:='Titulo m�dulo';
	pesquisasItem.campo:='MODULO_TITULO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=240;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDADOS_ANTERIORES';
	pesquisasItem.titulo:='Dados anteriores';
	pesquisasItem.campo:='DADOS_ANTERIORES';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[6]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDADOS_POSTERIORES';
	pesquisasItem.titulo:='Dados posteriores';
	pesquisasItem.campo:='DADOS_POSTERIORES';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[7]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCOMPUTADOR';
	pesquisasItem.titulo:='Computador';
	pesquisasItem.campo:='COMPUTADOR';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=240;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[8]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCOMPUTADOR_IP';
	pesquisasItem.titulo:='Ip computador';
	pesquisasItem.campo:='COMPUTADOR_IP';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=240;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[9]:=pesquisasItem;

  pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesTIPO';
  pesquisasItem.titulo:='Tipo de movimento';
	pesquisasItem.campo:='TIPO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=150;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('Inser��o');
	pesquisasItem.comboItens.add('Altera��o');
	pesquisasItem.comboItens.add('Exclus�o');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[10]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncusuarios_historico.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncusuarios_historico.criarPesquisas(sqlusuarios_historico,cdsusuarios_historico,'select * from USUARIOS_HISTORICO', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios usuarios_historico}
	camposObrigatoriosusuarios_historico[0] :=dbeusuarios_historicousuario;
	camposObrigatoriosusuarios_historico[1] :=dbeusuarios_historicomodulo_nome;
	{campos obrigatorios usuarios_historico<<<}

	{>>>objetos ativos no modo de manipula��o de dados usuarios_historico}
	objetosModoEdicaoAtivosusuarios_historico[0]:=pnlDadosusuarios_historico;
	objetosModoEdicaoAtivosusuarios_historico[1]:=spdCancelarusuarios_historico;
	objetosModoEdicaoAtivosusuarios_historico[2]:=spdGravarusuarios_historico;
	{objetos ativos no modo de manipula��o de dados usuarios_historico<<<}

	{>>>objetos inativos no modo de manipula��o de dados usuarios_historico}
	objetosModoEdicaoInativosusuarios_historico[0]:=spdAdicionarusuarios_historico;
	objetosModoEdicaoInativosusuarios_historico[1]:=spdAlterarusuarios_historico;
	objetosModoEdicaoInativosusuarios_historico[2]:=spdExcluirusuarios_historico;
	objetosModoEdicaoInativosusuarios_historico[3]:=spdOpcoes;
	objetosModoEdicaoInativosusuarios_historico[6]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados usuarios_historico<<<}

	{>>>comando de adi��o de teclas de atalhos usuarios_historico}
	fncusuarios_historico.criaAtalhoPopupMenu(Popupusuarios_historico,'Copiar registro selecionado',VK_F5,spdAlterarusuarios_historico.OnClick);
	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsusuarios_historico.open();
	{abertura dos datasets<<<}

	fncusuarios_historico.criaAtalhoPopupMenuNavegacao(Popupusuarios_historico,cdsusuarios_historico);

	fncusuarios_historico.definirMascaraCampos(cdsusuarios_historico);

  fncusuarios_historico.configurarGridesFormulario(fusuarios_historico, false, true);



end;

procedure Tfusuarios_historico.spdAdicionarusuarios_historicoClick(Sender: TObject);
begin
//	pgcPrincipal.ActivePageIndex:=tabusuarios_historico.TabIndex;
//	if (fncusuarios_historico.adicionar(cdsusuarios_historico)=true) then
//		begin
//			fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,true);
//			dbeusuarios_historicousuario.SetFocus;
//	end
//	else
//		fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,false);
  fncusuarios_historico.configurarGridesFormulario(fusuarios_historico, true, false);
end;

procedure Tfusuarios_historico.spdAlterarusuarios_historicoClick(Sender: TObject);
begin
//	pgcPrincipal.ActivePageIndex:=tabusuarios_historico.TabIndex;
//	if (fncusuarios_historico.alterar(cdsusuarios_historico)=true) then
//		begin
//			fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,true);
//			dbeusuarios_historicousuario.SetFocus;
//	end
//	else
//		fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,false);

 Clipboard.AsText  := '==================Dados Anteriores===================:'+#13+cdsusuarios_historicoDADOS_ANTERIORES.AsString+
                      '==================Dados Posteriores==================:'+#13+cdsusuarios_historicoDADOS_POSTERIORES.AsString;
 application.MessageBox(pchar('Registro copiado para a �rea de transfer�ncia.'),'Alerta', MB_OK+64);

end;

procedure Tfusuarios_historico.spdGravarusuarios_historicoClick(Sender: TObject);
begin
  fncusuarios_historico.verificarCamposObrigatorios(cdsusuarios_historico, camposObrigatoriosusuarios_historico);

	if (fncusuarios_historico.gravar(cdsusuarios_historico)=true) then
		fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,false)
	else
		fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,true);
end;

procedure Tfusuarios_historico.spdCancelarusuarios_historicoClick(Sender: TObject);
begin
	if (fncusuarios_historico.cancelar(cdsusuarios_historico)=true) then
		fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,false)
	else
		fncusuarios_historico.ativarModoEdicao(objetosModoEdicaoAtivosusuarios_historico,objetosModoEdicaoInativosusuarios_historico,true);
end;

procedure Tfusuarios_historico.spdExcluirusuarios_historicoClick(Sender: TObject);
begin
	fncusuarios_historico.excluir(cdsusuarios_historico);
end;

procedure Tfusuarios_historico.cdsusuarios_historicoBeforePost(DataSet: TDataSet);
begin
	if cdsusuarios_historico.State in [dsinsert] then
	cdsusuarios_historicoID.Value:=fncusuarios_historico.autoNumeracaoGenerator(dm.conexao,'G_usuarios_historico');
end;


procedure Tfusuarios_historico.ExportaoXLS1Click(Sender: TObject);
begin
 if not cdsusuarios_historico.IsEmpty  then
 exportargride(fusuarios_historico.Caption);
end;

procedure Tfusuarios_historico.exportargride(formulario: string);
var
 FileExt: String;
 SaveDialog  : TSaveDialog;
begin
try
    try
      SaveDialog  :=TSaveDialog.Create(nil);
      SaveDialog.Filter := 'Excel (*.xls) |*.xls|XML (*.xml) |*.xml|Arquivo Texto (*.txt) |*.txt|P�gina Web (*.html)|*.html';
      SaveDialog.Title := 'Exportar Dados';
      SaveDialog.DefaultExt:= 'xls';

     if SaveDialog.Execute then
      begin
        FileExt := LowerCase(ExtractFileExt(SaveDialog.FileName));
        if FileExt = '.xls' then
              ExportGridToExcel(SaveDialog.FileName,GridResultadoPesquisa, False)
        else if FileExt = '.xml' then
              ExportGridToExcel(SaveDialog.FileName,GridResultadoPesquisa, False)
        else if FileExt = '.txt' then
              ExportGridToExcel(SaveDialog.FileName,GridResultadoPesquisa, False)
        else if FileExt = '.html' then
              ExportGridToExcel(SaveDialog.FileName,GridResultadoPesquisa, False);
        ShellExecute(Handle, 'open', pchar(SaveDialog.FileName), nil, nil, SW_SHOW);
     end;

     if FileExists(SaveDialog.FileName) then
       Application.MessageBox('Processo de exporta��o conclu�do com sucesso...', 'Processo manual', MB_OK+64);
    finally
      FreeandNil(SaveDialog);
    end;

 except
 on e : Exception do
  begin
   Application.MessageBox(Pchar('Processo de exporta��o n�o conclu�do.Motivo: '+e.Message), 'Processo manual', MB_OK+64);
  end;
 end;

end;

procedure Tfusuarios_historico.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fusuarios_historico.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfusuarios_historico.spdImpressaoClick(Sender: TObject);
begin
  if fusuarios_historico.WindowState= wsMaximized then
	 popupImpressao.Popup((Sender as TSpeedButton).Left + 15,(Sender as TSpeedButton).Top + 60)
  else
   popupImpressao.Popup((Sender as TSpeedButton).Left + 370,(Sender as TSpeedButton).Top + 150);

end;

procedure Tfusuarios_historico.spdOpcoesClick(Sender: TObject);
begin
with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    PopupOpcao.Popup(X, Y);
  end;
end;

procedure Tfusuarios_historico.WMMove(var Msg: TWMMove);
begin
end;

procedure Tfusuarios_historico.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

end.

