program gocadastros;

uses
  Vcl.Forms,
  uprincipal in 'uprincipal.pas' {fprincipal},
  Vcl.Themes,
  Vcl.Styles,
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  ubanco in 'ubanco.pas' {fbanco},
  uempresa in 'uempresa.pas' {fempresa},
  ugocarteira in 'ugocarteira.pas' {fgocarteira},
  ugocliente in 'ugocliente.pas' {fgocliente},
  ugoconsultor in 'ugoconsultor.pas' {fgoconsultor},
  ugofilial in 'ugofilial.pas' {fgofilial},
  ugogrupoproduto in 'ugogrupoproduto.pas' {fgogrupoproduto},
  ugomoeda in 'ugomoeda.pas' {fgomoeda},
  ugoportador in 'ugoportador.pas' {fgoportador},
  ugoprojetos in 'ugoprojetos.pas' {fgoprojetos},
  ugosupervisor in 'ugosupervisor.pas' {fgosupervisor},
  uusuarios in 'uusuarios.pas' {fusuarios},
  ugogrupoprojeto in 'ugogrupoprojeto.pas' {fgogrupoprojeto},
  uacesso in '..\class_shared\uacesso.pas' {facesso},
  ugoparametro in 'ugoparametro.pas' {fgoparametro},
  ugorelatorios_automaticos in 'ugorelatorios_automaticos.pas' {fgorelatorios_automaticos},
  ufiltro_avancado in 'ufiltro_avancado.pas' {ffiltro_avancado},
  uprocessoatualizacao in '..\class_shared\uprocessoatualizacao.pas' {fprocessoatualizacao},
  uEnvioMail in '..\class_shared\uEnvioMail.pas' {FenvioMail};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfprincipal, fprincipal);
  Application.Run;
end.
