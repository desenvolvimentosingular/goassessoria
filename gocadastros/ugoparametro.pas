unit ugoparametro;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 11/12/2021*}                                          
{*Hora 21:59:53*}                                            
{*Unit goparametro *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, dxGDIPlusClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBEdit;

type                                                                                        
	Tfgoparametro = class(TForm)
    sqlgoparametro: TSQLDataSet;
	dspgoparametro: TDataSetProvider;
	cdsgoparametro: TClientDataSet;
	dsgoparametro: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgoparametro: TfrxReport;
	frxDBgoparametro: TfrxDBDataset;
	Popupgoparametro: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgoparametro: tcxTabSheet;
	SCRgoparametro: TScrollBox;
	pnlDadosgoparametro: TPanel;
	{>>>bot�es de manipula��o de dados goparametro}
	pnlLinhaBotoesgoparametro: TPanel;
	spdAdicionargoparametro: TButton;
	spdAlterargoparametro: TButton;
	spdGravargoparametro: TButton;
	spdCancelargoparametro: TButton;
	spdExcluirgoparametro: TButton;

	{bot�es de manipula��o de dadosgoparametro<<<}
	{campo ID}
	sqlgoparametroID: TIntegerField;
	cdsgoparametroID: TIntegerField;
	pnlLayoutLinhasgoparametro1: TPanel;
	pnlLayoutTitulosgoparametroid: TPanel;
	pnlLayoutCamposgoparametroid: TPanel;
	dbegoparametroid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoparametroDT_CADASTRO: TDateField;
	cdsgoparametroDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoparametro2: TPanel;
	pnlLayoutTitulosgoparametrodt_cadastro: TPanel;
	pnlLayoutCamposgoparametrodt_cadastro: TPanel;
	dbegoparametrodt_cadastro: TDBEdit;

	{campo ID_PROJETO}
	sqlgoparametroID_PROJETO: TIntegerField;
	cdsgoparametroID_PROJETO: TIntegerField;
	pnlLayoutLinhasgoparametro3: TPanel;
	pnlLayoutTitulosgoparametroid_projeto: TPanel;
	pnlLayoutCamposgoparametroid_projeto: TPanel;
	dbegoparametroid_projeto: TDBEdit;

	{campo PROJETO}
	sqlgoparametroPROJETO: TStringField;
	cdsgoparametroPROJETO: TStringField;

	{campo ID_FILIAL}
	sqlgoparametroID_FILIAL: TIntegerField;
	cdsgoparametroID_FILIAL: TIntegerField;
	pnlLayoutLinhasgoparametro5: TPanel;
	pnlLayoutTitulosgoparametroid_filial: TPanel;
	pnlLayoutCamposgoparametroid_filial: TPanel;
	dbegoparametroid_filial: TDBEdit;

	{campo FILIAL}
	sqlgoparametroFILIAL: TStringField;
	cdsgoparametroFILIAL: TStringField;

	{campo ID_FUNDO}
	sqlgoparametroID_FUNDO: TIntegerField;
	cdsgoparametroID_FUNDO: TIntegerField;
	pnlLayoutLinhasgoparametro7: TPanel;
	pnlLayoutTitulosgoparametroid_fundo: TPanel;
	pnlLayoutCamposgoparametroid_fundo: TPanel;
	dbegoparametroid_fundo: TDBEdit;

	{campo FUNDO}
	sqlgoparametroFUNDO: TStringField;
	cdsgoparametroFUNDO: TStringField;

	{campo PARAMETRO}
	sqlgoparametroPARAMETRO: TStringField;
	cdsgoparametroPARAMETRO: TStringField;
	pnlLayoutLinhasgoparametro9: TPanel;
	pnlLayoutTitulosgoparametroparametro: TPanel;
	pnlLayoutCamposgoparametroparametro: TPanel;
	dbegoparametroparametro: TDBEdit;

	{campo VALOR}
	sqlgoparametroVALOR: TStringField;
	cdsgoparametroVALOR: TStringField;
	pnlLayoutLinhasgoparametro10: TPanel;
	pnlLayoutTitulosgoparametrovalor: TPanel;
	pnlLayoutCamposgoparametrovalor: TPanel;
	dbegoparametrovalor: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    pnlLayoutCamposgoparametroprojeto: TPanel;
    dbegoparametroPROJETO: TDBEdit;
    pnlLayoutCamposgoparametrofilial: TPanel;
    dbegoparametroFILIAL: TDBEdit;
    pnlLayoutCamposgoparametrofundo: TPanel;
    dbegoparametroFUNDO: TDBEdit;
    btnprojeto: TButton;
    btnfilial: TButton;
    btnportador: TButton;
    psqprojeto: TPesquisa;
    psqfilial: TPesquisa;
    psqportador: TPesquisa;
    cdsfiltros: TClientDataSet;
    pnlLayoutLinhasgoparametro4: TPanel;
    pnlLayoutTitulosgoparametroTIPO: TPanel;
    pnlLayoutCamposgoparametrotipo: TPanel;
    cmbtipo: TcxDBComboBox;
    sqlgoparametroTIPO: TStringField;
    cdsgoparametroTIPO: TStringField;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargoparametroClick(Sender: TObject);
	procedure spdAlterargoparametroClick(Sender: TObject);
	procedure spdGravargoparametroClick(Sender: TObject);
	procedure spdCancelargoparametroClick(Sender: TObject);
	procedure spdExcluirgoparametroClick(Sender: TObject);
	procedure cdsgoparametroBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cmbtipoPropertiesChange(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgoparametro: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoparametro: Array[0..4]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoparametro: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoparametro: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoparametro: Array[0..3]  of TDuplicidade;
	duplicidadeCampogoparametro: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
	public

end;

var
fgoparametro: Tfgoparametro;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgoparametro.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgoparametro.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoparametro.TabIndex;
end;

procedure Tfgoparametro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgoparametro.verificarEmTransacao(cdsgoparametro);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgoparametro.configurarGridesFormulario(fgoparametro,true, false);

	fncgoparametro.Free;
	{eliminando container de fun��es da memoria<<<}

	fgoparametro:=nil;
	Action:=cafree;
end;

procedure Tfgoparametro.FormCreate(Sender: TObject);
begin
	{>>>container de fun��es}
	fncgoparametro:= TFuncoes.Create(Self);
	fncgoparametro.definirConexao(dm.conexao);
	fncgoparametro.definirConexaohistorico(dm.conexao);
	fncgoparametro.definirFormulario(Self);
	fncgoparametro.validarTransacaoRelacionamento:=true;
	fncgoparametro.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  lblTituloversao.Caption :=lblTituloversao.Caption+' - '+fncgoparametro.recuperarVersaoExecutavel;
  lblusuario.Caption      :=lblusuario.Caption + ParamStr(3);



	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}



  duplicidadeCampogoparametro:=TDuplicidade.Create;
  duplicidadeCampogoparametro.agrupador:='';
  duplicidadeCampogoparametro.objeto  :=dbegoparametroID_PROJETO;
  duplicidadeListagoparametro[0]      :=duplicidadeCampogoparametro;

  duplicidadeCampogoparametro:=TDuplicidade.Create;
  duplicidadeCampogoparametro.agrupador:='';
  duplicidadeCampogoparametro.objeto  :=dbegoparametroID_FILIAL;
  duplicidadeListagoparametro[1]      :=duplicidadeCampogoparametro;

  duplicidadeCampogoparametro:=TDuplicidade.Create;
  duplicidadeCampogoparametro.agrupador:='';
  duplicidadeCampogoparametro.objeto  :=dbegoparametroID_FUNDO;
  duplicidadeListagoparametro[2]      :=duplicidadeCampogoparametro;


  duplicidadeCampogoparametro:=TDuplicidade.Create;
  duplicidadeCampogoparametro.agrupador:='';
  duplicidadeCampogoparametro.objeto  :=dbegoparametroPARAMETRO;
  duplicidadeListagoparametro[3]      :=duplicidadeCampogoparametro;

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPROJETO';
	pesquisasItem.titulo:='Projeto';
	pesquisasItem.campo:='PROJETO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesFILIAL';
	pesquisasItem.titulo:='Filial';
	pesquisasItem.campo:='FILIAL';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesFUNDO';
	pesquisasItem.titulo:='Fundo';
	pesquisasItem.campo:='FUNDO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPARAMETRO';
	pesquisasItem.titulo:='Par�metro';
	pesquisasItem.campo:='PARAMETRO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=210;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesVALOR';
	pesquisasItem.titulo:='Valor';
	pesquisasItem.campo:='VALOR';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=210;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[6]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncgoparametro.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}


	fncgoparametro.criarPesquisas(sqlgoparametro,cdsgoparametro,'select * from goparametro', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios goparametro}
	camposObrigatoriosgoparametro[0] :=dbegoparametroID_PROJETO;
	camposObrigatoriosgoparametro[1] :=dbegoparametroID_FILIAL;
  camposObrigatoriosgoparametro[2] :=dbegoparametroID_FUNDO;
  camposObrigatoriosgoparametro[3] :=dbegoparametroPARAMETRO;
  camposObrigatoriosgoparametro[4] :=dbegoparametroVALOR;
	{campos obrigatorios goparametro<<<}

	{>>>objetos ativos no modo de manipula��o de dados goparametro}
	objetosModoEdicaoAtivosgoparametro[0]:=spdCancelargoparametro;
	objetosModoEdicaoAtivosgoparametro[1]:=spdGravargoparametro;
  objetosModoEdicaoAtivosgoparametro[2]:=dbegoparametroID_PROJETO;
  objetosModoEdicaoAtivosgoparametro[3]:=dbegoparametroID_FILIAL;
  objetosModoEdicaoAtivosgoparametro[4]:=dbegoparametroID_FUNDO;
  objetosModoEdicaoAtivosgoparametro[5]:=btnprojeto;
  objetosModoEdicaoAtivosgoparametro[6]:=btnfilial;
  objetosModoEdicaoAtivosgoparametro[7]:=btnportador;
	{objetos ativos no modo de manipula��o de dados goparametro<<<}

	{>>>objetos inativos no modo de manipula��o de dados goparametro}
	objetosModoEdicaoInativosgoparametro[0]:=spdAdicionargoparametro;
	objetosModoEdicaoInativosgoparametro[1]:=spdAlterargoparametro;
	objetosModoEdicaoInativosgoparametro[2]:=spdExcluirgoparametro;
	objetosModoEdicaoInativosgoparametro[3]:=spdOpcoes;
	objetosModoEdicaoInativosgoparametro[4]:=tabPesquisas;
  objetosModoEdicaoInativosgoparametro[5]:=dbegoparametroPROJETO;
  objetosModoEdicaoInativosgoparametro[6]:=dbegoparametroFILIAL;
  objetosModoEdicaoInativosgoparametro[7]:=dbegoparametroFUNDO;
	{objetos inativos no modo de manipula��o de dados goparametro<<<}

	{>>>comando de adi��o de teclas de atalhos goparametro}
	fncgoparametro.criaAtalhoPopupMenu(Popupgoparametro,'Adicionar novo registro',VK_F4,spdAdicionargoparametro.OnClick);
	fncgoparametro.criaAtalhoPopupMenu(Popupgoparametro,'Alterar registro selecionado',VK_F5,spdAlterargoparametro.OnClick);
	fncgoparametro.criaAtalhoPopupMenu(Popupgoparametro,'Gravar �ltimas altera��es',VK_F6,spdGravargoparametro.OnClick);
	fncgoparametro.criaAtalhoPopupMenu(Popupgoparametro,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoparametro.OnClick);
	fncgoparametro.criaAtalhoPopupMenu(Popupgoparametro,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoparametro.OnClick);

	{comando de adi��o de teclas de atalhos goparametro<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgoparametro.open();
	{abertura dos datasets<<<}

	fncgoparametro.criaAtalhoPopupMenuNavegacao(Popupgoparametro,cdsgoparametro);

	fncgoparametro.definirMascaraCampos(cdsgoparametro);

	fncgoparametro.configurarGridesFormulario(fgoparametro,false, true);

end;

procedure Tfgoparametro.spdAdicionargoparametroClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoparametro.TabIndex;
	if (fncgoparametro.adicionar(cdsgoparametro)=true) then
		begin
			fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,true);
			cmbtipo.SetFocus;
	end
	else
		fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,false);
end;

procedure Tfgoparametro.spdAlterargoparametroClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoparametro.TabIndex;
	if (fncgoparametro.alterar(cdsgoparametro)=true) then
		begin
			fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,true);
			cmbtipo.SetFocus;
	end
	else
		fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,false);
end;

procedure Tfgoparametro.spdGravargoparametroClick(Sender: TObject);
begin
  if  not (cdsgoparametroTIPO.AsString='Geral') then
  begin
 	 fncgoparametro.verificarCamposObrigatorios(cdsgoparametro, camposObrigatoriosgoparametro);
   fncgoparametro.verificarDuplicidade(dm.fdconexao, cdsgoparametro, 'goparametro', 'ID', duplicidadeListagoparametro);
  end;

	if (fncgoparametro.gravar(cdsgoparametro)=true) then
		fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,false)
	else
		fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,true);

end;

procedure Tfgoparametro.spdCancelargoparametroClick(Sender: TObject);
begin
	if (fncgoparametro.cancelar(cdsgoparametro)=true) then
		fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,false)
	else
		fncgoparametro.ativarModoEdicao(objetosModoEdicaoAtivosgoparametro,objetosModoEdicaoInativosgoparametro,true);
end;

procedure Tfgoparametro.spdExcluirgoparametroClick(Sender: TObject);
begin
	fncgoparametro.excluir(cdsgoparametro);
end;

procedure Tfgoparametro.cdsgoparametroBeforePost(DataSet: TDataSet);
begin
	if cdsgoparametro.State in [dsinsert] then
	cdsgoparametroID.Value:=fncgoparametro.autoNumeracaoGenerator(dm.conexao,'G_goparametro');
end;

procedure Tfgoparametro.cmbtipoPropertiesChange(Sender: TObject);
begin

  if cmbtipo.Text ='Geral' then
  begin
    dbegoparametroID_PROJETO.Enabled:=false;
    dbegoparametroID_FILIAL.Enabled :=false;
    dbegoparametroID_FUNDO.Enabled  :=false;
    btnprojeto.Enabled              :=false;
    btnfilial.Enabled               :=false;
    btnportador.Enabled             :=false;
  end
  else
  begin
    dbegoparametroID_PROJETO.Enabled:=True;
    dbegoparametroID_FILIAL.Enabled :=True;
    dbegoparametroID_FUNDO.Enabled  :=True;
    btnprojeto.Enabled              :=True;
    btnfilial.Enabled               :=True;
    btnportador.Enabled             :=True;

  end;
end;

procedure Tfgoparametro.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgoparametro.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgoparametro.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgoparametro.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgoparametro.IsEmpty then 
	fncgoparametro.visualizarRelatorios(frxgoparametro);
end;

end.

