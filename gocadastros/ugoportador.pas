unit ugoportador;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 09/08/2021*}                                          
{*Hora 20:25:20*}                                            
{*Unit goportador *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa;

type                                                                                        
	Tfgoportador = class(TForm)
    sqlgoportador: TSQLDataSet;
	dspgoportador: TDataSetProvider;
	cdsgoportador: TClientDataSet;
	dsgoportador: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgoportador: TfrxReport;
	frxDBgoportador: TfrxDBDataset;
	Popupgoportador: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgoportador: tcxTabSheet;
	SCRgoportador: TScrollBox;
	pnlDadosgoportador: TPanel;
	{>>>bot�es de manipula��o de dados goportador}
	pnlLinhaBotoesgoportador: TPanel;
	spdAdicionargoportador: TButton;
	spdAlterargoportador: TButton;
	spdGravargoportador: TButton;
	spdCancelargoportador: TButton;
	spdExcluirgoportador: TButton;

	{bot�es de manipula��o de dadosgoportador<<<}
	{campo ID}
	sqlgoportadorID: TIntegerField;
	cdsgoportadorID: TIntegerField;
	pnlLayoutLinhasgoportador1: TPanel;
	pnlLayoutTitulosgoportadorid: TPanel;
	pnlLayoutCamposgoportadorid: TPanel;
	dbegoportadorid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoportadorDT_CADASTRO: TDateField;
	cdsgoportadorDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoportador2: TPanel;
	pnlLayoutTitulosgoportadordt_cadastro: TPanel;
	pnlLayoutCamposgoportadordt_cadastro: TPanel;
	dbegoportadordt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoportadorHS_CADASTRO: TTimeField;
	cdsgoportadorHS_CADASTRO: TTimeField;

	{campo DESCRICAO}
	sqlgoportadorDESCRICAO: TStringField;
	cdsgoportadorDESCRICAO: TStringField;
	pnlLayoutLinhasgoportador4: TPanel;
	pnlLayoutTitulosgoportadordescricao: TPanel;
	pnlLayoutCamposgoportadordescricao: TPanel;
	dbegoportadordescricao: TDBEdit;

	{campo CNPJ}
	sqlgoportadorCNPJ: TStringField;
	cdsgoportadorCNPJ: TStringField;
	pnlLayoutLinhasgoportador5: TPanel;
	pnlLayoutTitulosgoportadorcnpj: TPanel;
	pnlLayoutCamposgoportadorcnpj: TPanel;
	dbegoportadorcnpj: TDBEdit;

	{campo ENDERECO}
	sqlgoportadorENDERECO: TStringField;
	cdsgoportadorENDERECO: TStringField;
	pnlLayoutLinhasgoportador6: TPanel;
	pnlLayoutTitulosgoportadorendereco: TPanel;
	pnlLayoutCamposgoportadorendereco: TPanel;
	dbegoportadorendereco: TDBEdit;

	{campo CEP}
	sqlgoportadorCEP: TStringField;
	cdsgoportadorCEP: TStringField;
	pnlLayoutLinhasgoportador7: TPanel;
	pnlLayoutTitulosgoportadorcep: TPanel;
	pnlLayoutCamposgoportadorcep: TPanel;
	dbegoportadorcep: TDBEdit;

	{campo UF}
	sqlgoportadorUF: TStringField;
	cdsgoportadorUF: TStringField;

	{campo CIDADE}
	sqlgoportadorCIDADE: TStringField;
	cdsgoportadorCIDADE: TStringField;
	pnlLayoutLinhasgoportador9: TPanel;
	pnlLayoutTitulosgoportadorcidade: TPanel;
	pnlLayoutCamposgoportadorcidade: TPanel;
	dbegoportadorcidade: TDBEdit;

	{campo REPRESENTANTE}
	sqlgoportadorREPRESENTANTE: TStringField;
	cdsgoportadorREPRESENTANTE: TStringField;
	pnlLayoutLinhasgoportador10: TPanel;
	pnlLayoutTitulosgoportadorrepresentante: TPanel;
	pnlLayoutCamposgoportadorrepresentante: TPanel;
	dbegoportadorrepresentante: TDBEdit;

	{campo EMAIL1}
	sqlgoportadorEMAIL1: TStringField;
	cdsgoportadorEMAIL1: TStringField;
	pnlLayoutLinhasgoportador11: TPanel;
	pnlLayoutTitulosgoportadoremail1: TPanel;
	pnlLayoutCamposgoportadoremail1: TPanel;
	dbegoportadoremail1: TDBEdit;

	{campo EMAIL2}
	sqlgoportadorEMAIL2: TStringField;
	cdsgoportadorEMAIL2: TStringField;
	pnlLayoutLinhasgoportador12: TPanel;
	pnlLayoutTitulosgoportadoremail2: TPanel;
	pnlLayoutCamposgoportadoremail2: TPanel;
	dbegoportadoremail2: TDBEdit;

	{campo FONE1}
	sqlgoportadorFONE1: TStringField;
	cdsgoportadorFONE1: TStringField;
	pnlLayoutLinhasgoportador13: TPanel;
	pnlLayoutTitulosgoportadorfone1: TPanel;
	pnlLayoutCamposgoportadorfone1: TPanel;
	dbegoportadorfone1: TDBEdit;

	{campo FONE2}
	sqlgoportadorFONE2: TStringField;
	cdsgoportadorFONE2: TStringField;
	pnlLayoutLinhasgoportador14: TPanel;
	pnlLayoutTitulosgoportadorfone2: TPanel;
	pnlLayoutCamposgoportadorfone2: TPanel;
	dbegoportadorfone2: TDBEdit;

	{campo CODTIPO}
	sqlgoportadorCODTIPO: TIntegerField;
	cdsgoportadorCODTIPO: TIntegerField;
	pnlLayoutLinhasgoportador15: TPanel;
	pnlLayoutTitulosgoportadorcodtipo: TPanel;
	pnlLayoutCamposgoportadorcodtipo: TPanel;
	dbegoportadorcodtipo: TDBEdit;

	{campo TIPO}
	sqlgoportadorTIPO: TStringField;
	cdsgoportadorTIPO: TStringField;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlLayoutTitulosgoportadoruf: TPanel;
    pnlLayoutCamposgoportadoruf: TPanel;
    cbogoportadorUF: TDBComboBox;
    pnlLayoutTitulosgoportadorhs_cadastro: TPanel;
    pnlLayoutCamposgoportadorhs_cadastro: TPanel;
    dbegoportadorHS_CADASTRO: TDBEdit;
    frxDBDfiltros: TfrxDBDataset;
    cdsfiltros: TClientDataSet;
    pnlLayoutCamposgoportadortipo: TPanel;
    dbegoportadorTIPO: TDBEdit;
    btnportador: TButton;
    psqtipofundo: TPesquisa;
    pnlLayoutLinhasgofilial10: TPanel;
    pnlLayoutTitulosgofilialcnae: TPanel;
    pnlLayoutCamposgofilialcnae: TPanel;
    dbegofilialCNAE: TDBEdit;
    dbegofilialCODCNAE: TDBEdit;
    btnCNAE: TButton;
    sqlgoportadorCODCNAE: TStringField;
    sqlgoportadorCNAE: TStringField;
    cdsgoportadorCODCNAE: TStringField;
    cdsgoportadorCNAE: TStringField;
    psqCNAE: TPesquisa;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargoportadorClick(Sender: TObject);
	procedure spdAlterargoportadorClick(Sender: TObject);
	procedure spdGravargoportadorClick(Sender: TObject);
	procedure spdCancelargoportadorClick(Sender: TObject);
	procedure spdExcluirgoportadorClick(Sender: TObject);
	procedure cdsgoportadorBeforePost(DataSet: TDataSet);

    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure dbegoportadorCEPExit(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgoportador: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoportador: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoportador: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoportador: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoportador: Array[0..1]  of TDuplicidade;
	duplicidadeCampogoportador: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fgoportador: Tfgoportador;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgoportador.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgoportador.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoportador.TabIndex;
end;

procedure Tfgoportador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgoportador.verificarEmTransacao(cdsgoportador);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgoportador.configurarGridesFormulario(fgoportador,true, false);

	fncgoportador.Free;
	{eliminando container de fun��es da memoria<<<}

	fgoportador:=nil;
	Action:=cafree;
end;

procedure Tfgoportador.FormCreate(Sender: TObject);
begin
///  sessao;
    {>>>container de tradu�a� de compomentes}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
   begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
   end;
  {>>>container de tradu�a� de compomentes}
	{>>>container de fun��es}
	fncgoportador:= TFuncoes.Create(Self);
	fncgoportador.definirConexao(dm.conexao);
	fncgoportador.definirConexaohistorico(dm.conexao);
	fncgoportador.definirFormulario(Self);
	fncgoportador.validarTransacaoRelacionamento:=true;
	fncgoportador.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  lblTituloversao.Caption :=lblTituloversao.Caption+' - '+fncgoportador.recuperarVersaoExecutavel;
  lblusuario.Caption      :=lblusuario.Caption + ParamStr(3);

	{>>>verificar campos duplicidade}
	duplicidadeCampogoportador:=TDuplicidade.Create;
	duplicidadeCampogoportador.agrupador:='';
	duplicidadeCampogoportador.objeto :=dbegoportadordescricao;
	duplicidadeListagoportador[0]:=duplicidadeCampogoportador;

	duplicidadeCampogoportador:=TDuplicidade.Create;
	duplicidadeCampogoportador.agrupador:='';
	duplicidadeCampogoportador.objeto :=dbegoportadorcnpj;
	duplicidadeListagoportador[1]:=duplicidadeCampogoportador;

	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=60;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUF';
	pesquisasItem.titulo:='Estado';
	pesquisasItem.campo:='UF';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=60;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCIDADE';
	pesquisasItem.titulo:='Cidade';
	pesquisasItem.campo:='CIDADE';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesTIPO';
	pesquisasItem.titulo:='Tipo';
	pesquisasItem.campo:='TIPO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

    {gerando campos no clientdatsaet de filtros}
  fncgoportador.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncgoportador.criarPesquisas(sqlgoportador,cdsgoportador,'select * from goportador', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros );
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios goportador}
	camposObrigatoriosgoportador[0] :=dbegoportadortipo;
	{campos obrigatorios goportador<<<}

	{>>>objetos ativos no modo de manipula��o de dados goportador}
	objetosModoEdicaoAtivosgoportador[0]:=spdCancelargoportador;
	objetosModoEdicaoAtivosgoportador[1]:=spdGravargoportador;
  objetosModoEdicaoAtivosgoportador[2]:=dbegoportadorCODTIPO;
  objetosModoEdicaoAtivosgoportador[3]:=btnportador;
  objetosModoEdicaoAtivosgoportador[4]:=dbegofilialCODCNAE;
  objetosModoEdicaoAtivosgoportador[5]:=btnCNAE;

	{objetos ativos no modo de manipula��o de dados goportador<<<}

	{>>>objetos inativos no modo de manipula��o de dados goportador}
	objetosModoEdicaoInativosgoportador[0]:=spdAdicionargoportador;
	objetosModoEdicaoInativosgoportador[1]:=spdAlterargoportador;
	objetosModoEdicaoInativosgoportador[2]:=spdExcluirgoportador;
	objetosModoEdicaoInativosgoportador[3]:=spdOpcoes;
	objetosModoEdicaoInativosgoportador[4]:=tabPesquisas;
  objetosModoEdicaoInativosgoportador[5]:=dbegoportadorTIPO;
	{objetos inativos no modo de manipula��o de dados goportador<<<}

	{>>>comando de adi��o de teclas de atalhos goportador}
	fncgoportador.criaAtalhoPopupMenu(Popupgoportador,'Adicionar novo registro',VK_F4,spdAdicionargoportador.OnClick);
	fncgoportador.criaAtalhoPopupMenu(Popupgoportador,'Alterar registro selecionado',VK_F5,spdAlterargoportador.OnClick);
	fncgoportador.criaAtalhoPopupMenu(Popupgoportador,'Gravar �ltimas altera��es',VK_F6,spdGravargoportador.OnClick);
	fncgoportador.criaAtalhoPopupMenu(Popupgoportador,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoportador.OnClick);
	fncgoportador.criaAtalhoPopupMenu(Popupgoportador,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoportador.OnClick);

	{comando de adi��o de teclas de atalhos goportador<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgoportador.open();
	{abertura dos datasets<<<}

	fncgoportador.criaAtalhoPopupMenuNavegacao(Popupgoportador,cdsgoportador);

	fncgoportador.definirMascaraCampos(cdsgoportador);

	fncgoportador.configurarGridesFormulario(fgoportador,false, true);

end;

procedure Tfgoportador.spdAdicionargoportadorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoportador.TabIndex;
	if (fncgoportador.adicionar(cdsgoportador)=true) then
		begin
			fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,true);
			dbegoportadorCODTIPO.SetFocus;
	end
	else
		fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,false);
end;

procedure Tfgoportador.spdAlterargoportadorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoportador.TabIndex;
	if (fncgoportador.alterar(cdsgoportador)=true) then
		begin
			fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,true);
			dbegoportadorCODTIPO.SetFocus;
	end
	else
		fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,false);
end;

procedure Tfgoportador.spdGravargoportadorClick(Sender: TObject);
begin
	fncgoportador.verificarCamposObrigatorios(cdsgoportador, camposObrigatoriosgoportador, pgcPrincipal);
	fncgoportador.verificarDuplicidade(dm.fdconexao, cdsgoportador, 'goportador', 'ID', duplicidadeListagoportador);

	if (fncgoportador.gravar(cdsgoportador)=true) then
		fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,false)
	else
		fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,true);
end;

procedure Tfgoportador.spdCancelargoportadorClick(Sender: TObject);
begin
	if (fncgoportador.cancelar(cdsgoportador)=true) then
		fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,false)
	else
		fncgoportador.ativarModoEdicao(objetosModoEdicaoAtivosgoportador,objetosModoEdicaoInativosgoportador,true);
end;

procedure Tfgoportador.spdExcluirgoportadorClick(Sender: TObject);
begin
	fncgoportador.excluir(cdsgoportador);
end;

procedure Tfgoportador.cdsgoportadorBeforePost(DataSet: TDataSet);
begin
	if cdsgoportador.State in [dsinsert] then
	cdsgoportadorID.Value:=fncgoportador.autoNumeracaoGenerator(dm.conexao,'G_goportador');
end;

procedure Tfgoportador.dbegoportadorCEPExit(Sender: TObject);
begin
 if ((cdsgoportadorCEP.AsString<>EmptyStr) and (cdsgoportador.State in [dsinsert, dsedit])) then
   begin
    dm.FDQuery.Close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.sql.Add('SELECT CIDADE, UF, BAIRRO, RUA FROM CEP WHERE CEP=:CEP');
    dm.FDQuery.ParamByName('CEP').AsString  := cdsgoportadorCEP.AsString;
    dm.FDQuery.Open();
    cdsgoportadorCIDADE.AsString  := dm.FDQuery.FieldByName('CIDADE').AsString;
    cdsgoportadorUF.AsString  := dm.FDQuery.FieldByName('UF').AsString;
   end;
end;

procedure Tfgoportador.sessao;
begin
    try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfgoportador.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgoportador.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgoportador.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgoportador.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgoportador.IsEmpty then 
	fncgoportador.visualizarRelatorios(frxgoportador);
end;

end.

