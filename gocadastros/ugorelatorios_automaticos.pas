unit ugorelatorios_automaticos;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 15/01/2022*}                                          
{*Hora 12:25:35*}                                            
{*Unit gorelatorios_automaticos *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, dxGDIPlusClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type                                                                                        
	Tfgorelatorios_automaticos = class(TForm)
    sqlgorelatorios_automaticos: TSQLDataSet;
	dspgorelatorios_automaticos: TDataSetProvider;
	cdsgorelatorios_automaticos: TClientDataSet;
	dsgorelatorios_automaticos: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgorelatorios_automaticos: TfrxReport;
	frxDBgorelatorios_automaticos: TfrxDBDataset;
	Popupgorelatorios_automaticos: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgorelatorios_automaticos: tcxTabSheet;
	SCRgorelatorios_automaticos: TScrollBox;
	pnlDadosgorelatorios_automaticos: TPanel;
	{>>>bot�es de manipula��o de dados gorelatorios_automaticos}
	pnlLinhaBotoesgorelatorios_automaticos: TPanel;
	spdAdicionargorelatorios_automaticos: TButton;
	spdAlterargorelatorios_automaticos: TButton;
	spdGravargorelatorios_automaticos: TButton;
	spdCancelargorelatorios_automaticos: TButton;
	spdExcluirgorelatorios_automaticos: TButton;

	{bot�es de manipula��o de dadosgorelatorios_automaticos<<<}
	{campo ID}
	sqlgorelatorios_automaticosID: TIntegerField;
	cdsgorelatorios_automaticosID: TIntegerField;
	pnlLayoutLinhasgorelatorios_automaticos1: TPanel;
	pnlLayoutTitulosgorelatorios_automaticosid: TPanel;
	pnlLayoutCamposgorelatorios_automaticosid: TPanel;
	dbegorelatorios_automaticosid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgorelatorios_automaticosDT_CADASTRO: TDateField;
	cdsgorelatorios_automaticosDT_CADASTRO: TDateField;
	pnlLayoutLinhasgorelatorios_automaticos2: TPanel;
	pnlLayoutTitulosgorelatorios_automaticosdt_cadastro: TPanel;
	pnlLayoutCamposgorelatorios_automaticosdt_cadastro: TPanel;
	dbegorelatorios_automaticosdt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgorelatorios_automaticosHS_CADASTRO: TTimeField;
	cdsgorelatorios_automaticosHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgorelatorios_automaticos3: TPanel;
	pnlLayoutTitulosgorelatorios_automaticoshs_cadastro: TPanel;
	pnlLayoutCamposgorelatorios_automaticoshs_cadastro: TPanel;
	dbegorelatorios_automaticoshs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlgorelatorios_automaticosDESCRICAO: TStringField;
	cdsgorelatorios_automaticosDESCRICAO: TStringField;
	pnlLayoutLinhasgorelatorios_automaticos4: TPanel;
	pnlLayoutTitulosgorelatorios_automaticosdescricao: TPanel;
	pnlLayoutCamposgorelatorios_automaticosdescricao: TPanel;
	dbegorelatorios_automaticosdescricao: TDBEdit;

	{campo EMAIL1}
	sqlgorelatorios_automaticosEMAIL1: TStringField;
	cdsgorelatorios_automaticosEMAIL1: TStringField;
	pnlLayoutLinhasgorelatorios_automaticos5: TPanel;
	pnlLayoutTitulosgorelatorios_automaticosemail1: TPanel;
	pnlLayoutCamposgorelatorios_automaticosemail1: TPanel;
	dbegorelatorios_automaticosemail1: TDBEdit;

	{campo EMAIL2}
	sqlgorelatorios_automaticosEMAIL2: TStringField;
	cdsgorelatorios_automaticosEMAIL2: TStringField;
	pnlLayoutLinhasgorelatorios_automaticos6: TPanel;
	pnlLayoutTitulosgorelatorios_automaticosemail2: TPanel;
	pnlLayoutCamposgorelatorios_automaticosemail2: TPanel;
	dbegorelatorios_automaticosemail2: TDBEdit;

	{campo HORA}
	sqlgorelatorios_automaticosHORA: TTimeField;
	cdsgorelatorios_automaticosHORA: TTimeField;
	pnlLayoutLinhasgorelatorios_automaticos7: TPanel;
	pnlLayoutTitulosgorelatorios_automaticoshora: TPanel;
	pnlLayoutCamposgorelatorios_automaticoshora: TPanel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    dbegorelatorios_automaticosHORA: TcxDBDateEdit;
    cdsfiltros: TClientDataSet;
    pnl_dados_controles: TPanel;
    gridrelatoriosincluir: TcxGrid;
    cxGridDBTVrelatoriosincluir: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    gridrelatoriosinclusos: TcxGrid;
    cxGridDBTVRELATORIOSINCLUSOS: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    splcontroles: TSplitter;
    cxGridDBTVrelatoriosincluirdescricao: TcxGridDBColumn;
    cxGridDBTVRELATORIOSINCLUSOSdescricao: TcxGridDBColumn;
    FDRelatoriosInclusos: TFDQuery;
    FDRelatoriosIncluir: TFDQuery;
    dsrelatoriosinclusos: TDataSource;
    dsrelatoriosincluir: TDataSource;
    FDRelatoriosInclusosID: TIntegerField;
    FDRelatoriosInclusosDT_CADASTRO: TDateField;
    FDRelatoriosInclusosHS_CADASTRO: TTimeField;
    FDRelatoriosInclusosDESCRICAO: TStringField;
    FDRelatoriosInclusosEMAIL: TStringField;
    FDRelatoriosIncluirID: TIntegerField;
    FDRelatoriosIncluirDT_CADASTRO: TDateField;
    FDRelatoriosIncluirHS_CADASTRO: TTimeField;
    FDRelatoriosIncluirDESCRICAO: TStringField;
    FDRelatoriosIncluirEMAIL: TStringField;
    FDRelatoriosInclusosCODADM: TIntegerField;
    FDRelatoriosIncluirCODADM: TIntegerField;
    spdadicionarrelatorio: TButton;
    spdadicionarrelatorioagruapda: TButton;
    spdExcluirrelatorio: TButton;
    spdexcluirrelatorioagrupada: TButton;
    OpesdeEnvio1: TMenuItem;
    Panel1: TPanel;
    pnlLayoutTitulosgorelatorios_automaticospasta: TPanel;
    pnlLayoutCamposgorelatorios_automaticospasta: TPanel;
    dbegorelatorios_automaticospasta: TDBEdit;
    sqlgorelatorios_automaticosCAMINHO_PASTA: TStringField;
    cdsgorelatorios_automaticosCAMINHO_PASTA: TStringField;
    btncaminhopasta: TButton;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargorelatorios_automaticosClick(Sender: TObject);
	procedure spdAlterargorelatorios_automaticosClick(Sender: TObject);
	procedure spdGravargorelatorios_automaticosClick(Sender: TObject);
	procedure spdCancelargorelatorios_automaticosClick(Sender: TObject);
	procedure spdExcluirgorelatorios_automaticosClick(Sender: TObject);
	procedure cdsgorelatorios_automaticosBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdsgorelatorios_automaticosAfterClose(DataSet: TDataSet);
    procedure cdsgorelatorios_automaticosAfterOpen(DataSet: TDataSet);
    procedure spdExcluirrelatorioClick(Sender: TObject);
    procedure spdadicionarrelatorioClick(Sender: TObject);
    procedure spdadicionarrelatorioagruapdaClick(Sender: TObject);
    procedure spdexcluirrelatorioagrupadaClick(Sender: TObject);
    procedure cxGridDBTVRELATORIOSINCLUSOSDblClick(Sender: TObject);
    procedure cxGridDBTVrelatoriosincluirDblClick(Sender: TObject);
    procedure spdOpcoesClick(Sender: TObject);
    procedure OpesdeEnvio1Click(Sender: TObject);
    procedure btncaminhopastaClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgorelatorios_automaticos: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgorelatorios_automaticos: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgorelatorios_automaticos: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgorelatorios_automaticos: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagorelatorios_automaticos: Array[0..0]  of TDuplicidade;
	duplicidadeCampogorelatorios_automaticos: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure incluir_relatorio;
    procedure excluir_relatorio;
    procedure excluir_relatorio_todos;
    procedure incluir_relatorios_todos;
	public

end;

var
fgorelatorios_automaticos: Tfgorelatorios_automaticos;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgorelatorios_automaticos.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgorelatorios_automaticos.GridResultadoPesquisaDBTVDblClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgorelatorios_automaticos.TabIndex;
end;

procedure Tfgorelatorios_automaticos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgorelatorios_automaticos.verificarEmTransacao(cdsgorelatorios_automaticos);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgorelatorios_automaticos.configurarGridesFormulario(fgorelatorios_automaticos,true, false);

	fncgorelatorios_automaticos.Free;
	{eliminando container de fun��es da memoria<<<}

	fgorelatorios_automaticos:=nil;
	Action:=cafree;
end;

procedure Tfgorelatorios_automaticos.FormCreate(Sender: TObject);
begin

	{>>>container de fun��es}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  {<<>> fun��o de tradu�a� de componentes>>>}

	fncgorelatorios_automaticos:= TFuncoes.Create(Self);
	fncgorelatorios_automaticos.definirConexao(dm.conexao);
	fncgorelatorios_automaticos.definirConexaohistorico(dm.conexao);
	fncgorelatorios_automaticos.definirFormulario(Self);
	fncgorelatorios_automaticos.validarTransacaoRelacionamento:=true;
	fncgorelatorios_automaticos.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesHS_CADASTRO';
	pesquisasItem.titulo:='Hora de Cadastro';
	pesquisasItem.campo:='HS_CADASTRO';
	pesquisasItem.tipoFDB:='Time';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

    {gerando campos no clientdatsaet de filtros}
  fncgorelatorios_automaticos.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}
	fncgorelatorios_automaticos.criarPesquisas(sqlgorelatorios_automaticos,cdsgorelatorios_automaticos,'select * from gorelatorios_automaticos', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gorelatorios_automaticos}
	{campos obrigatorios gorelatorios_automaticos<<<}

	{>>>objetos ativos no modo de manipula��o de dados gorelatorios_automaticos}
	objetosModoEdicaoAtivosgorelatorios_automaticos[0]:=spdCancelargorelatorios_automaticos;
	objetosModoEdicaoAtivosgorelatorios_automaticos[1]:=spdGravargorelatorios_automaticos;
	{objetos ativos no modo de manipula��o de dados gorelatorios_automaticos<<<}

	{>>>objetos inativos no modo de manipula��o de dados gorelatorios_automaticos}
	objetosModoEdicaoInativosgorelatorios_automaticos[0]:=spdAdicionargorelatorios_automaticos;
	objetosModoEdicaoInativosgorelatorios_automaticos[1]:=spdAlterargorelatorios_automaticos;
	objetosModoEdicaoInativosgorelatorios_automaticos[2]:=spdExcluirgorelatorios_automaticos;
	objetosModoEdicaoInativosgorelatorios_automaticos[3]:=spdOpcoes;
	objetosModoEdicaoInativosgorelatorios_automaticos[4]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gorelatorios_automaticos<<<}

	{>>>comando de adi��o de teclas de atalhos gorelatorios_automaticos}
	fncgorelatorios_automaticos.criaAtalhoPopupMenu(Popupgorelatorios_automaticos,'Adicionar novo registro',VK_F4,spdAdicionargorelatorios_automaticos.OnClick);
	fncgorelatorios_automaticos.criaAtalhoPopupMenu(Popupgorelatorios_automaticos,'Alterar registro selecionado',VK_F5,spdAlterargorelatorios_automaticos.OnClick);
	fncgorelatorios_automaticos.criaAtalhoPopupMenu(Popupgorelatorios_automaticos,'Gravar �ltimas altera��es',VK_F6,spdGravargorelatorios_automaticos.OnClick);
	fncgorelatorios_automaticos.criaAtalhoPopupMenu(Popupgorelatorios_automaticos,'Cancelar �ltimas altera��e',VK_F7,spdCancelargorelatorios_automaticos.OnClick);
	fncgorelatorios_automaticos.criaAtalhoPopupMenu(Popupgorelatorios_automaticos,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgorelatorios_automaticos.OnClick);

	{comando de adi��o de teclas de atalhos gorelatorios_automaticos<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgorelatorios_automaticos.open();
	{abertura dos datasets<<<}

	fncgorelatorios_automaticos.criaAtalhoPopupMenuNavegacao(Popupgorelatorios_automaticos,cdsgorelatorios_automaticos);

	fncgorelatorios_automaticos.definirMascaraCampos(cdsgorelatorios_automaticos);

	fncgorelatorios_automaticos.configurarGridesFormulario(fgorelatorios_automaticos,false, true);

end;

procedure Tfgorelatorios_automaticos.spdAdicionargorelatorios_automaticosClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgorelatorios_automaticos.TabIndex;
	if (fncgorelatorios_automaticos.adicionar(cdsgorelatorios_automaticos)=true) then
		begin
			fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,true);
			dbegorelatorios_automaticosDESCRICAO.SetFocus;
	end
	else
		fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,false);
end;

procedure Tfgorelatorios_automaticos.spdadicionarrelatorioagruapdaClick(
  Sender: TObject);
begin
 if not cdsgorelatorios_automaticos.IsEmpty then
 begin
    excluir_relatorio_todos;
 end;
end;

procedure Tfgorelatorios_automaticos.spdadicionarrelatorioClick(
  Sender: TObject);
begin
 if not cdsgorelatorios_automaticos.IsEmpty then
 begin
    excluir_relatorio;
 end;

end;

procedure Tfgorelatorios_automaticos.spdAlterargorelatorios_automaticosClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgorelatorios_automaticos.TabIndex;
	if (fncgorelatorios_automaticos.alterar(cdsgorelatorios_automaticos)=true) then
		begin
			fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,true);
			dbegorelatorios_automaticosDESCRICAO.SetFocus;
	end
	else
		fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,false);
end;

procedure Tfgorelatorios_automaticos.spdGravargorelatorios_automaticosClick(Sender: TObject);
begin


	if (fncgorelatorios_automaticos.gravar(cdsgorelatorios_automaticos)=true) then
		fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,false)
	else
		fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,true);
end;

procedure Tfgorelatorios_automaticos.spdCancelargorelatorios_automaticosClick(Sender: TObject);
begin
	if (fncgorelatorios_automaticos.cancelar(cdsgorelatorios_automaticos)=true) then
		fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,false)
	else
		fncgorelatorios_automaticos.ativarModoEdicao(objetosModoEdicaoAtivosgorelatorios_automaticos,objetosModoEdicaoInativosgorelatorios_automaticos,true);
end;

procedure Tfgorelatorios_automaticos.spdExcluirgorelatorios_automaticosClick(Sender: TObject);
begin
	fncgorelatorios_automaticos.excluir(cdsgorelatorios_automaticos);
end;

procedure Tfgorelatorios_automaticos.spdexcluirrelatorioagrupadaClick(
  Sender: TObject);
begin
 if not cdsgorelatorios_automaticos.IsEmpty then
 begin
    incluir_relatorios_todos;
 end;

end;

procedure Tfgorelatorios_automaticos.spdExcluirrelatorioClick(Sender: TObject);
begin
 if not cdsgorelatorios_automaticos.IsEmpty then
 begin
    incluir_relatorio;
 end;

end;

procedure Tfgorelatorios_automaticos.incluir_relatorio;
begin
  dm.FDQuery.Close;
  dm.FDQuery.sql.Clear;
  dm.FDQuery.sql.Add('update GORELATORIO set email=:email where descricao=:descricao');
  dm.FDQuery.ParamByName('email').AsString := 'S';
  dm.FDQuery.ParamByName('descricao').AsString := FDRelatoriosIncluirDESCRICAO.AsString;
  dm.FDQuery.ExecSQL;
  FDRelatoriosIncluir.Close;
  FDRelatoriosInclusos.Close;
  FDRelatoriosIncluir.Open;
  FDRelatoriosInclusos.Open;
end;

procedure Tfgorelatorios_automaticos.excluir_relatorio;
begin
  dm.FDQuery.Close;
  dm.FDQuery.sql.Clear;
  dm.FDQuery.sql.Add('update GORELATORIO set email=:email where descricao=:descricao');
  dm.FDQuery.ParamByName('email').AsString := 'N';
  dm.FDQuery.ParamByName('descricao').AsString := FDRelatoriosInclusosDESCRICAO.AsString;
  dm.FDQuery.ExecSQL;
  FDRelatoriosIncluir.Close;
  FDRelatoriosInclusos.Close;
  FDRelatoriosIncluir.Open;
  FDRelatoriosInclusos.Open;
end;

procedure Tfgorelatorios_automaticos.excluir_relatorio_todos;
begin
  dm.FDQuery.Close;
  dm.FDQuery.sql.Clear;
  dm.FDQuery.sql.Add('update GORELATORIO set email=:email');
  dm.FDQuery.ParamByName('email').AsString := 'N';
  dm.FDQuery.ExecSQL;
  FDRelatoriosIncluir.Close;
  FDRelatoriosInclusos.Close;
  FDRelatoriosIncluir.Open;
  FDRelatoriosInclusos.Open;
end;

procedure Tfgorelatorios_automaticos.incluir_relatorios_todos;
begin
  dm.FDQuery.Close;
  dm.FDQuery.sql.Clear;
  dm.FDQuery.sql.Add('update GORELATORIO set email=:email');
  dm.FDQuery.ParamByName('email').AsString := 'S';
  dm.FDQuery.ExecSQL;
  FDRelatoriosIncluir.Close;
  FDRelatoriosInclusos.Close;
  FDRelatoriosIncluir.Open;
  FDRelatoriosInclusos.Open;
end;

procedure Tfgorelatorios_automaticos.btncaminhopastaClick(Sender: TObject);
begin
  try
    if cdsgorelatorios_automaticos.State in [dsinsert, dsedit] then
     cdsgorelatorios_automaticosCAMINHO_PASTA.AsString:= fncgorelatorios_automaticos.selecionarDiretorio('Selecione um diret�rio.');
  except

  end;
end;

procedure Tfgorelatorios_automaticos.cdsgorelatorios_automaticosAfterClose(
  DataSet: TDataSet);
begin
  FDRelatoriosInclusos.Close;
  FDRelatoriosIncluir.close;
end;

procedure Tfgorelatorios_automaticos.cdsgorelatorios_automaticosAfterOpen(
  DataSet: TDataSet);
begin
  FDRelatoriosInclusos.Open;
  FDRelatoriosIncluir.Open;
end;

procedure Tfgorelatorios_automaticos.cdsgorelatorios_automaticosBeforePost(DataSet: TDataSet);
begin
	if cdsgorelatorios_automaticos.State in [dsinsert] then
	cdsgorelatorios_automaticosID.Value:=fncgorelatorios_automaticos.autoNumeracaoGenerator(dm.conexao,'G_gorelatorios_automaticos');
end;



procedure Tfgorelatorios_automaticos.cxGridDBTVrelatoriosincluirDblClick(
  Sender: TObject);
begin
  incluir_relatorio;
end;

procedure Tfgorelatorios_automaticos.cxGridDBTVRELATORIOSINCLUSOSDblClick(
  Sender: TObject);
begin
 excluir_relatorio;
end;

procedure Tfgorelatorios_automaticos.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgorelatorios_automaticos.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgorelatorios_automaticos.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgorelatorios_automaticos.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgorelatorios_automaticos.menuImpRelSinteticoClick(Sender: TObject);
begin
 if not cdsgorelatorios_automaticos.IsEmpty then
  fncgorelatorios_automaticos.visualizarRelatorios(frxgorelatorios_automaticos);
end;

procedure Tfgorelatorios_automaticos.OpesdeEnvio1Click(Sender: TObject);
begin
//   if (ffiltro_avancado = nil) then Application.CreateForm(Tffiltro_avancado, ffiltro_avancado);
//    ffiltro_avancado.ShowModal;
end;

end.

