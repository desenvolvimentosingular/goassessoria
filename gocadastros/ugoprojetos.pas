unit ugoprojetos;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 09/08/2021*}                                          
{*Hora 16:43:46*}                                            
{*Unit goprojetos *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, uPesquisa;

type                                                                                        
	Tfgoprojetos = class(TForm)
    sqlgoprojetos: TSQLDataSet;
	dspgoprojetos: TDataSetProvider;
	cdsgoprojetos: TClientDataSet;
	dsgoprojetos: TDataSource;
    sqlGOSUPERVISOR: TSQLDataSet;
	dspGOSUPERVISOR: TDataSetProvider;
	cdsGOSUPERVISOR: TClientDataSet;
	dsGOSUPERVISOR: TDataSource;
    sqlGOPROJETOCONSULTOR: TSQLDataSet;
	dspGOPROJETOCONSULTOR: TDataSetProvider;
	cdsGOPROJETOCONSULTOR: TClientDataSet;
	dsGOPROJETOCONSULTOR: TDataSource;
    sqlGOPROJETOFILIAL: TSQLDataSet;
	dspGOPROJETOFILIAL: TDataSetProvider;
	cdsGOPROJETOFILIAL: TClientDataSet;
	dsGOPROJETOFILIAL: TDataSource;
    sqlGOPROJETOFUNDO: TSQLDataSet;
	dspGOPROJETOFUNDO: TDataSetProvider;
	cdsGOPROJETOFUNDO: TClientDataSet;
	dsGOPROJETOFUNDO: TDataSource;
	popupImpressao: TPopupMenu;
    menuImpRelSintetico1: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgoprojetos: TfrxReport;
	frxDBgoprojetos: TfrxDBDataset;
	Popupgoprojetos: TPopupMenu;
	Popupgosupervisor: TPopupMenu;
	Popupgoprojetoconsultor: TPopupMenu;
	Popupgoprojetofilial: TPopupMenu;
	Popupgoprojetofundo: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgoprojetos: tcxTabSheet;
	SCRgoprojetos: TScrollBox;
	pnlDadosgoprojetos: TPanel;
	{>>>bot�es de manipula��o de dados goprojetos}
	pnlLinhaBotoesgoprojetos: TPanel;
	spdAdicionargoprojetos: TButton;
	spdAlterargoprojetos: TButton;
	spdGravargoprojetos: TButton;
	spdCancelargoprojetos: TButton;
	spdExcluirgoprojetos: TButton;

	{bot�es de manipula��o de dadosgoprojetos<<<}
	{campo ID}
	sqlgoprojetosID: TIntegerField;
	cdsgoprojetosID: TIntegerField;
	pnlLayoutLinhasgoprojetos1: TPanel;
	pnlLayoutTitulosgoprojetosid: TPanel;
	pnlLayoutCamposgoprojetosid: TPanel;
	dbegoprojetosid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoprojetosDT_CADASTRO: TDateField;
	cdsgoprojetosDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoprojetos2: TPanel;
	pnlLayoutTitulosgoprojetosdt_cadastro: TPanel;
	pnlLayoutCamposgoprojetosdt_cadastro: TPanel;
	dbegoprojetosdt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoprojetosHS_CADASTRO: TTimeField;
	cdsgoprojetosHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgoprojetos3: TPanel;
	pnlLayoutTitulosgoprojetoshs_cadastro: TPanel;
	pnlLayoutCamposgoprojetoshs_cadastro: TPanel;
	dbegoprojetoshs_cadastro: TDBEdit;

	{campo CODGRUPO}
	sqlgoprojetosCODGRUPO: TIntegerField;
	cdsgoprojetosCODGRUPO: TIntegerField;
	pnlLayoutLinhasgoprojetos4: TPanel;
	pnlLayoutTitulosgoprojetoscodgrupo: TPanel;
	pnlLayoutCamposgoprojetoscodgrupo: TPanel;
	dbegoprojetoscodgrupo: TDBEdit;

	{campo GRUPO}
	sqlgoprojetosGRUPO: TStringField;
	cdsgoprojetosGRUPO: TStringField;

	{campo DESCRICAO}
	sqlgoprojetosDESCRICAO: TStringField;
	cdsgoprojetosDESCRICAO: TStringField;
	pnlLayoutLinhasgoprojetos6: TPanel;
	pnlLayoutTitulosgoprojetosdescricao: TPanel;
	pnlLayoutCamposgoprojetosdescricao: TPanel;
	dbegoprojetosdescricao: TDBEdit;

	{campo TIPO}
	sqlgoprojetosTIPO: TStringField;
	cdsgoprojetosTIPO: TStringField;
	pnlLayoutLinhasgoprojetos7: TPanel;
	pnlLayoutTitulosgoprojetostipo: TPanel;
	pnlLayoutCamposgoprojetostipo: TPanel;
	cbogoprojetostipo: TDBComboBox;

	{campo DT_INICIO}
	sqlgoprojetosDT_INICIO: TDateField;
	cdsgoprojetosDT_INICIO: TDateField;
	pnlLayoutLinhasgoprojetos8: TPanel;
	pnlLayoutTitulosgoprojetosdt_inicio: TPanel;
	pnlLayoutCamposgoprojetosdt_inicio: TPanel;

	{campo DT_TERMINO}
	sqlgoprojetosDT_TERMINO: TDateField;
	cdsgoprojetosDT_TERMINO: TDateField;
	pnlLayoutLinhasgoprojetos9: TPanel;
	pnlLayoutTitulosgoprojetosdt_termino: TPanel;
	pnlLayoutCamposgoprojetosdt_termino: TPanel;

	{campo STATUS}
	sqlgoprojetosSTATUS: TStringField;
	cdsgoprojetosSTATUS: TStringField;
	pnlLayoutLinhasgoprojetos10: TPanel;
	pnlLayoutTitulosgoprojetosstatus: TPanel;
	pnlLayoutCamposgoprojetosstatus: TPanel;
	cbogoprojetosstatus: TDBComboBox;
	tabgosupervisor: tcxTabSheet;
	SCRgosupervisor: TScrollBox;
	pnlDadosgosupervisor: TPanel;
	{>>>bot�es de manipula��o de dados gosupervisor}
	pnlLinhaBotoesgosupervisor: TPanel;
	spdAdicionargosupervisor: TButton;
	spdAlterargosupervisor: TButton;
	spdGravargosupervisor: TButton;
	spdCancelargosupervisor: TButton;
	spdExcluirgosupervisor: TButton;

	{bot�es de manipula��o de dadosgosupervisor<<<}
	{campo ID}
	sqlgosupervisorID: TIntegerField;
	cdsgosupervisorID: TIntegerField;
	pnlLayoutLinhasgosupervisor1: TPanel;
	pnlLayoutTitulosgosupervisorid: TPanel;
	pnlLayoutCamposgosupervisorid: TPanel;
	dbegosupervisorid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgosupervisorDT_CADASTRO: TDateField;
	cdsgosupervisorDT_CADASTRO: TDateField;
	pnlLayoutLinhasgosupervisor2: TPanel;
	pnlLayoutTitulosgosupervisordt_cadastro: TPanel;
	pnlLayoutCamposgosupervisordt_cadastro: TPanel;
	dbegosupervisordt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgosupervisorHS_CADASTRO: TTimeField;
	cdsgosupervisorHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgosupervisor3: TPanel;
	pnlLayoutTitulosgosupervisorhs_cadastro: TPanel;
	pnlLayoutCamposgosupervisorhs_cadastro: TPanel;
	dbegosupervisorhs_cadastro: TDBEdit;

	{campo NOME}
	sqlgosupervisorNOME: TStringField;
	cdsgosupervisorNOME: TStringField;
	pnlLayoutLinhasgosupervisor4: TPanel;
	pnlLayoutTitulosgosupervisornome: TPanel;
	pnlLayoutCamposgosupervisornome: TPanel;
	dbegosupervisornome: TDBEdit;

	{campo CPF}
	sqlgosupervisorCPF: TStringField;
	cdsgosupervisorCPF: TStringField;
	pnlLayoutLinhasgosupervisor5: TPanel;
	pnlLayoutTitulosgosupervisorcpf: TPanel;
	pnlLayoutCamposgosupervisorcpf: TPanel;
	dbegosupervisorcpf: TDBEdit;

	{campo GERENTE}
	sqlgosupervisorGERENTE: TStringField;
	cdsgosupervisorGERENTE: TStringField;
	pnlLayoutLinhasgosupervisor6: TPanel;
	pnlLayoutTitulosgosupervisorgerente: TPanel;
	pnlLayoutCamposgosupervisorgerente: TPanel;
	dbegosupervisorgerente: TDBEdit;

	{campo EMAIL1}
	sqlgosupervisorEMAIL1: TStringField;
	cdsgosupervisorEMAIL1: TStringField;
	pnlLayoutLinhasgosupervisor7: TPanel;
	pnlLayoutTitulosgosupervisoremail1: TPanel;
	pnlLayoutCamposgosupervisoremail1: TPanel;
	dbegosupervisoremail1: TDBEdit;

	{campo EMAIL2}
	sqlgosupervisorEMAIL2: TStringField;
	cdsgosupervisorEMAIL2: TStringField;
	pnlLayoutLinhasgosupervisor8: TPanel;
	pnlLayoutTitulosgosupervisoremail2: TPanel;
	pnlLayoutCamposgosupervisoremail2: TPanel;
	dbegosupervisoremail2: TDBEdit;

	{campo TELEFONE1}
	sqlgosupervisorTELEFONE1: TStringField;
	cdsgosupervisorTELEFONE1: TStringField;
	pnlLayoutLinhasgosupervisor9: TPanel;
	pnlLayoutTitulosgosupervisortelefone1: TPanel;
	pnlLayoutCamposgosupervisortelefone1: TPanel;
	dbegosupervisortelefone1: TDBEdit;

	{campo TELEFONE2}
	sqlgosupervisorTELEFONE2: TStringField;
	cdsgosupervisorTELEFONE2: TStringField;
	pnlLayoutLinhasgosupervisor10: TPanel;
	pnlLayoutTitulosgosupervisortelefone2: TPanel;
	pnlLayoutCamposgosupervisortelefone2: TPanel;
	dbegosupervisortelefone2: TDBEdit;

	{campo STATUS}
	sqlgosupervisorSTATUS: TStringField;
	cdsgosupervisorSTATUS: TStringField;
	pnlLayoutLinhasgosupervisor11: TPanel;
	pnlLayoutTitulosgosupervisorstatus: TPanel;
	pnlLayoutCamposgosupervisorstatus: TPanel;
	cbogosupervisorstatus: TDBComboBox;
	tabgoprojetoconsultor: tcxTabSheet;
	SCRgoprojetoconsultor: TScrollBox;
	pnlDadosgoprojetoconsultor: TPanel;
	{>>>bot�es de manipula��o de dados goprojetoconsultor}
	pnlLinhaBotoesgoprojetoconsultor: TPanel;
	spdAdicionargoprojetoconsultor: TButton;
	spdAlterargoprojetoconsultor: TButton;
	spdGravargoprojetoconsultor: TButton;
	spdCancelargoprojetoconsultor: TButton;
	spdExcluirgoprojetoconsultor: TButton;

	{bot�es de manipula��o de dadosgoprojetoconsultor<<<}
	{campo ID}
	sqlgoprojetoconsultorID: TIntegerField;
	cdsgoprojetoconsultorID: TIntegerField;
	pnlLayoutLinhasgoprojetoconsultor1: TPanel;
	pnlLayoutTitulosgoprojetoconsultorid: TPanel;
	pnlLayoutCamposgoprojetoconsultorid: TPanel;
	dbegoprojetoconsultorid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoprojetoconsultorDT_CADASTRO: TDateField;
	cdsgoprojetoconsultorDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoprojetoconsultor2: TPanel;
	pnlLayoutTitulosgoprojetoconsultordt_cadastro: TPanel;
	pnlLayoutCamposgoprojetoconsultordt_cadastro: TPanel;
	dbegoprojetoconsultordt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoprojetoconsultorHS_CADASTRO: TTimeField;
	cdsgoprojetoconsultorHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgoprojetoconsultor3: TPanel;
	pnlLayoutTitulosgoprojetoconsultorhs_cadastro: TPanel;
	pnlLayoutCamposgoprojetoconsultorhs_cadastro: TPanel;
	dbegoprojetoconsultorhs_cadastro: TDBEdit;

	{campo CODPROJETO}
	sqlgoprojetoconsultorCODPROJETO: TIntegerField;
	cdsgoprojetoconsultorCODPROJETO: TIntegerField;
	pnlLayoutLinhasgoprojetoconsultor4: TPanel;
	pnlLayoutTitulosgoprojetoconsultorcodprojeto: TPanel;
	pnlLayoutCamposgoprojetoconsultorcodprojeto: TPanel;
	dbegoprojetoconsultorcodprojeto: TDBEdit;

	{campo CODCONSULTOR}
	sqlgoprojetoconsultorCODCONSULTOR: TIntegerField;
	cdsgoprojetoconsultorCODCONSULTOR: TIntegerField;
	pnlLayoutLinhasgoprojetoconsultor5: TPanel;
	pnlLayoutTitulosgoprojetoconsultorcodconsultor: TPanel;
	pnlLayoutCamposgoprojetoconsultorcodconsultor: TPanel;
	dbegoprojetoconsultorcodconsultor: TDBEdit;

	{campo CONSULTOR}
	sqlgoprojetoconsultorCONSULTOR: TStringField;
	cdsgoprojetoconsultorCONSULTOR: TStringField;

	{campo DT_ENTRADA}
	sqlgoprojetoconsultorDT_ENTRADA: TDateField;
	cdsgoprojetoconsultorDT_ENTRADA: TDateField;
	pnlLayoutLinhasgoprojetoconsultor7: TPanel;
	pnlLayoutTitulosgoprojetoconsultordt_entrada: TPanel;
	pnlLayoutCamposgoprojetoconsultordt_entrada: TPanel;

	{campo DT_SAIDA}
	sqlgoprojetoconsultorDT_SAIDA: TDateField;
	cdsgoprojetoconsultorDT_SAIDA: TDateField;
	pnlLayoutLinhasgoprojetoconsultor8: TPanel;
	pnlLayoutTitulosgoprojetoconsultordt_saida: TPanel;
	pnlLayoutCamposgoprojetoconsultordt_saida: TPanel;

	tabgoprojetofilial: tcxTabSheet;
	SCRgoprojetofilial: TScrollBox;
	pnlDadosgoprojetofilial: TPanel;
	{>>>bot�es de manipula��o de dados goprojetofilial}
	pnlLinhaBotoesgoprojetofilial: TPanel;
	spdAdicionargoprojetofilial: TButton;
	spdAlterargoprojetofilial: TButton;
	spdGravargoprojetofilial: TButton;
	spdCancelargoprojetofilial: TButton;
	spdExcluirgoprojetofilial: TButton;

	{bot�es de manipula��o de dadosgoprojetofilial<<<}
	{campo ID}
	sqlgoprojetofilialID: TIntegerField;
	cdsgoprojetofilialID: TIntegerField;
	pnlLayoutLinhasgoprojetofilial1: TPanel;
	pnlLayoutTitulosgoprojetofilialid: TPanel;
	pnlLayoutCamposgoprojetofilialid: TPanel;
	dbegoprojetofilialid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoprojetofilialDT_CADASTRO: TDateField;
	cdsgoprojetofilialDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoprojetofilial2: TPanel;
	pnlLayoutTitulosgoprojetofilialdt_cadastro: TPanel;
	pnlLayoutCamposgoprojetofilialdt_cadastro: TPanel;
	dbegoprojetofilialdt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoprojetofilialHS_CADASTRO: TTimeField;
	cdsgoprojetofilialHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgoprojetofilial3: TPanel;
	pnlLayoutTitulosgoprojetofilialhs_cadastro: TPanel;
	pnlLayoutCamposgoprojetofilialhs_cadastro: TPanel;
	dbegoprojetofilialhs_cadastro: TDBEdit;

	{campo CODFILIAL}
	sqlgoprojetofilialCODFILIAL: TIntegerField;
	cdsgoprojetofilialCODFILIAL: TIntegerField;
	pnlLayoutLinhasgoprojetofilial4: TPanel;
	pnlLayoutTitulosgoprojetofilialcodfilial: TPanel;
	pnlLayoutCamposgoprojetofilialcodfilial: TPanel;
	dbegoprojetofilialcodfilial: TDBEdit;

	{campo FILIAL}
	sqlgoprojetofilialFILIAL: TStringField;
	cdsgoprojetofilialFILIAL: TStringField;

	{campo DT_ENTRADA}
	sqlgoprojetofilialDT_ENTRADA: TDateField;
	cdsgoprojetofilialDT_ENTRADA: TDateField;
	pnlLayoutLinhasgoprojetofilial6: TPanel;
	pnlLayoutTitulosgoprojetofilialdt_entrada: TPanel;
	pnlLayoutCamposgoprojetofilialdt_entrada: TPanel;

	{campo DT_SAIDA}
	sqlgoprojetofilialDT_SAIDA: TDateField;
	cdsgoprojetofilialDT_SAIDA: TDateField;
	pnlLayoutLinhasgoprojetofilial7: TPanel;
	pnlLayoutTitulosgoprojetofilialdt_saida: TPanel;
	pnlLayoutCamposgoprojetofilialdt_saida: TPanel;

	{campo STATUS}
	sqlgoprojetofilialSTATUS: TStringField;
	cdsgoprojetofilialSTATUS: TStringField;
	pnlLayoutLinhasgoprojetofilial8: TPanel;
	pnlLayoutTitulosgoprojetofilialstatus: TPanel;
	pnlLayoutCamposgoprojetofilialstatus: TPanel;
	pnlLayoutLinhasgoprojetofilial9: TPanel;
	pnlLayoutTitulosgoprojetofilialcodprojeto: TPanel;
	pnlLayoutCamposgoprojetofilialcodprojeto: TPanel;
	dbegoprojetofilialcodprojeto: TDBEdit;

	tabgoprojetofundo: tcxTabSheet;
	SCRgoprojetofundo: TScrollBox;
	pnlDadosgoprojetofundo: TPanel;
	{>>>bot�es de manipula��o de dados goprojetofundo}
	pnlLinhaBotoesgoprojetofundo: TPanel;
	spdAdicionargoprojetofundo: TButton;
	spdAlterargoprojetofundo: TButton;
	spdGravargoprojetofundo: TButton;
	spdCancelargoprojetofundo: TButton;
	spdExcluirgoprojetofundo: TButton;

	{bot�es de manipula��o de dadosgoprojetofundo<<<}
	{campo ID}
	sqlgoprojetofundoID: TIntegerField;
	cdsgoprojetofundoID: TIntegerField;
	pnlLayoutLinhasgoprojetofundo1: TPanel;
	pnlLayoutTitulosgoprojetofundoid: TPanel;
	pnlLayoutCamposgoprojetofundoid: TPanel;
	dbegoprojetofundoid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoprojetofundoDT_CADASTRO: TDateField;
	cdsgoprojetofundoDT_CADASTRO: TDateField;
	pnlLayoutLinhasgoprojetofundo2: TPanel;
	pnlLayoutTitulosgoprojetofundodt_cadastro: TPanel;
	pnlLayoutCamposgoprojetofundodt_cadastro: TPanel;
	dbegoprojetofundodt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoprojetofundoHS_CADASTRO: TTimeField;
	cdsgoprojetofundoHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgoprojetofundo3: TPanel;
	pnlLayoutTitulosgoprojetofundohs_cadastro: TPanel;
	pnlLayoutCamposgoprojetofundohs_cadastro: TPanel;
	dbegoprojetofundohs_cadastro: TDBEdit;

	{campo CODPROJETO}
	sqlgoprojetofundoCODPROJETO: TIntegerField;
	cdsgoprojetofundoCODPROJETO: TIntegerField;
	pnlLayoutLinhasgoprojetofundo4: TPanel;
	pnlLayoutTitulosgoprojetofundocodprojeto: TPanel;
	pnlLayoutCamposgoprojetofundocodprojeto: TPanel;
	dbegoprojetofundocodprojeto: TDBEdit;

	{campo PROJETO}
	sqlgoprojetofundoPROJETO: TStringField;
	cdsgoprojetofundoPROJETO: TStringField;

	{campo CODPORTADOR}
	sqlgoprojetofundoCODPORTADOR: TIntegerField;
	cdsgoprojetofundoCODPORTADOR: TIntegerField;
	pnlLayoutLinhasgoprojetofundo6: TPanel;
	pnlLayoutTitulosgoprojetofundocodportador: TPanel;
	pnlLayoutCamposgoprojetofundocodportador: TPanel;
	dbegoprojetofundocodportador: TDBEdit;

	{campo PORTADOR}
	sqlgoprojetofundoPORTADOR: TStringField;
	cdsgoprojetofundoPORTADOR: TStringField;

	{campo LIMITE_DESCONTO}
	sqlgoprojetofundoLIMITE_DESCONTO: TFloatField;
	cdsgoprojetofundoLIMITE_DESCONTO: TFloatField;
	pnlLayoutLinhasgoprojetofundo8: TPanel;
	pnlLayoutTitulosgoprojetofundolimite_desconto: TPanel;
	pnlLayoutCamposgoprojetofundolimite_desconto: TPanel;
	dbegoprojetofundolimite_desconto: TDBEdit;

	{campo TAXA_DESCONTO}
	sqlgoprojetofundoTAXA_DESCONTO: TFloatField;
	cdsgoprojetofundoTAXA_DESCONTO: TFloatField;
	pnlLayoutLinhasgoprojetofundo9: TPanel;
	pnlLayoutTitulosgoprojetofundotaxa_desconto: TPanel;
	pnlLayoutCamposgoprojetofundotaxa_desconto: TPanel;
	dbegoprojetofundotaxa_desconto: TDBEdit;

	{campo LIMITE_FOMENTO}
	sqlgoprojetofundoLIMITE_FOMENTO: TFloatField;
	cdsgoprojetofundoLIMITE_FOMENTO: TFloatField;
	pnlLayoutLinhasgoprojetofundo10: TPanel;
	pnlLayoutTitulosgoprojetofundolimite_fomento: TPanel;
	pnlLayoutCamposgoprojetofundolimite_fomento: TPanel;
	dbegoprojetofundolimite_fomento: TDBEdit;

	{campo TAXA_FOMENTO}
	sqlgoprojetofundoTAXA_FOMENTO: TFloatField;
	cdsgoprojetofundoTAXA_FOMENTO: TFloatField;
	pnlLayoutLinhasgoprojetofundo11: TPanel;
	pnlLayoutTitulosgoprojetofundotaxa_fomento: TPanel;
	pnlLayoutCamposgoprojetofundotaxa_fomento: TPanel;
	dbegoprojetofundotaxa_fomento: TDBEdit;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    Gridgoprojetofundo: TcxGrid;
    GridgoprojetofundoDBTV: TcxGridDBTableView;
    GRIDgoprojetofundoDBTVID: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVCODPROJETO: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVPROJETO: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVCODPORTADOR: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVPORTADOR: TcxGridDBColumn;
    GRIDgoprojetofundoDBTVLIMITE_DESCONTO: TcxGridDBColumn;
    Gridgoprojetofilial: TcxGrid;
    GridgoprojetofilialDBTV: TcxGridDBTableView;
    GRIDgoprojetofilialDBTVID: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVCODFILIAL: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVFILIAL: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVDT_ENTRADA: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVDT_SAIDA: TcxGridDBColumn;
    GRIDgoprojetofilialDBTVCODPROJETO: TcxGridDBColumn;
    Gridgoprojetoconsultor: TcxGrid;
    GridgoprojetoconsultorDBTV: TcxGridDBTableView;
    GRIDgoprojetoconsultorDBTVID: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVCODPROJETO: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVCODCONSULTOR: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVCONSULTOR: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVDT_ENTRADA: TcxGridDBColumn;
    GRIDgoprojetoconsultorDBTVDT_SAIDA: TcxGridDBColumn;
    Gridgosupervisor: TcxGrid;
    GridgosupervisorDBTV: TcxGridDBTableView;
    GRIDgosupervisorDBTVID: TcxGridDBColumn;
    GRIDgosupervisorDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDgosupervisorDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDgosupervisorDBTVNOME: TcxGridDBColumn;
    GRIDgosupervisorDBTVCPF: TcxGridDBColumn;
    GRIDgosupervisorDBTVGERENTE: TcxGridDBColumn;
    GRIDgosupervisorDBTVTELEFONE1: TcxGridDBColumn;
    GRIDgosupervisorDBTVTELEFONE2: TcxGridDBColumn;
    GridgosupervisorLevel1: TcxGridLevel;
    GridgoprojetoconsultorLevel1: TcxGridLevel;
    GridgoprojetofilialLevel1: TcxGridLevel;
    GridgoprojetofundoLevel1: TcxGridLevel;
    btngrupoprojeto: TButton;
    pnlLayoutCamposgoprojetosgrupo: TPanel;
    dbegoprojetosGRUPO: TDBEdit;
    dbegoprojetosDT_INICIO: TcxDBDateEdit;
    dbegoprojetosDT_FIM: TcxDBDateEdit;
    frxDBDfiltros: TfrxDBDataset;
    cdsfiltros: TClientDataSet;
    psqgrupoprojeto: TPesquisa;
    btnconsultor: TButton;
    pnlLayoutCamposgoprojetoconsultorconsultor: TPanel;
    dbegoprojetoconsultorCONSULTOR: TDBEdit;
    psqconsultor: TPesquisa;
    dbegoprojetoconsultorDT_ENTRADA: TcxDBDateEdit;
    dbegoprojetoconsultorDT_SAIDA: TcxDBDateEdit;
    btnfilial: TButton;
    pnlLayoutCamposgoprojetofilialfilial: TPanel;
    dbegoprojetofilialFILIAL: TDBEdit;
    dbegoprojetofilialDT_ENTRADA: TcxDBDateEdit;
    dbegoprojetofilialDT_SAIDA: TcxDBDateEdit;
    pnlLayoutCamposgoprojetofundoprojeto: TPanel;
    dbegoprojetofundoPROJETO: TDBEdit;
    pnlLayoutCamposgoprojetofundoportador: TPanel;
    dbegoprojetofundoPORTADOR: TDBEdit;
    btnportador: TButton;
    psqfilial: TPesquisa;
    sqlgoprojetofilialCODPROJETO: TIntegerField;
    cdsgoprojetofilialCODPROJETO: TIntegerField;
    psqportador: TPesquisa;
    dbegoprojetofilialSTATUS: TDBComboBox;
    frxgoprojetofilial: TfrxReport;
    frxDBgoprojetofilial: TfrxDBDataset;
    frxgoprojetofundo: TfrxReport;
    frxDBgoprojetofundo: TfrxDBDataset;
    frxgoprojetoconsultor: TfrxReport;
    frxDBgoprojetoconsultor: TfrxDBDataset;
    menuImpRelSintetico2: TMenuItem;
    menuImpRelSintetico3: TMenuItem;
    menuImpRelSintetico4: TMenuItem;
    sqlgoprojetofilialPROJETO: TStringField;
    cdsgoprojetofilialPROJETO: TStringField;
    pnlLayoutTitulosgoprojetofilialmeta: TPanel;
    pnlLayoutCamposgoprojetofilialmeta: TPanel;
    dbegoprojetofilialmeta: TDBEdit;
    sqlgoprojetofilialMETA_FATURAMENTO: TFloatField;
    cdsgoprojetofilialMETA_FATURAMENTO: TFloatField;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure cdsgoprojetosAfterClose(DataSet: TDataSet);
	procedure cdsgoprojetosAfterOpen(DataSet: TDataSet);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSintetico1Click(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargoprojetosClick(Sender: TObject);
	procedure spdAlterargoprojetosClick(Sender: TObject);
	procedure spdGravargoprojetosClick(Sender: TObject);
	procedure spdCancelargoprojetosClick(Sender: TObject);
	procedure spdExcluirgoprojetosClick(Sender: TObject);
	procedure cdsgoprojetosBeforePost(DataSet: TDataSet);

	procedure spdAdicionargosupervisorClick(Sender: TObject);
	procedure spdAlterargosupervisorClick(Sender: TObject);
	procedure spdGravargosupervisorClick(Sender: TObject);
	procedure spdCancelargosupervisorClick(Sender: TObject);
	procedure spdExcluirgosupervisorClick(Sender: TObject);
	procedure cdsgosupervisorBeforePost(DataSet: TDataSet);
	procedure spdAdicionargoprojetoconsultorClick(Sender: TObject);
	procedure spdAlterargoprojetoconsultorClick(Sender: TObject);
	procedure spdGravargoprojetoconsultorClick(Sender: TObject);
	procedure spdCancelargoprojetoconsultorClick(Sender: TObject);
	procedure spdExcluirgoprojetoconsultorClick(Sender: TObject);
	procedure spdAdicionargoprojetofilialClick(Sender: TObject);
	procedure spdAlterargoprojetofilialClick(Sender: TObject);
	procedure spdGravargoprojetofilialClick(Sender: TObject);
	procedure spdCancelargoprojetofilialClick(Sender: TObject);
	procedure spdExcluirgoprojetofilialClick(Sender: TObject);
	procedure spdAdicionargoprojetofundoClick(Sender: TObject);
	procedure spdAlterargoprojetofundoClick(Sender: TObject);
	procedure spdGravargoprojetofundoClick(Sender: TObject);
	procedure spdCancelargoprojetofundoClick(Sender: TObject);
	procedure spdExcluirgoprojetofundoClick(Sender: TObject);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdsgoprojetofundoNewRecord(DataSet: TDataSet);
    procedure cdsgoprojetofilialNewRecord(DataSet: TDataSet);
    procedure cdsgoprojetoconsultorNewRecord(DataSet: TDataSet);
    procedure spdOpcoesClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgoprojetos: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoprojetos: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoprojetos: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoprojetos: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoprojetos: Array[0..2]  of TDuplicidade;
	duplicidadeCampogoprojetos: TDuplicidade;
	{container de funcoes}
	fncgosupervisor: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgosupervisor: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgosupervisor: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgosupervisor: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagosupervisor: Array[0..0]  of TDuplicidade;
	duplicidadeCampogosupervisor: TDuplicidade;
	{container de funcoes}
	fncgoprojetoconsultor: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoprojetoconsultor: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoprojetoconsultor: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoprojetoconsultor: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoprojetoconsultor: Array[0..1]  of TDuplicidade;
	duplicidadeCampogoprojetoconsultor: TDuplicidade;
	{container de funcoes}
	fncgoprojetofilial: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoprojetofilial: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoprojetofilial: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoprojetofilial: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoprojetofilial: Array[0..2]  of TDuplicidade;
	duplicidadeCampogoprojetofilial: TDuplicidade;
	{container de funcoes}
	fncgoprojetofundo: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgoprojetofundo: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgoprojetofundo: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgoprojetofundo: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagoprojetofundo: Array[0..1]  of TDuplicidade;
	duplicidadeCampogoprojetofundo: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fgoprojetos: Tfgoprojetos;

implementation

uses udm, Vcl.DialogMessage, uacesso;

{$R *.dfm}
//{$R logo.res}

procedure Tfgoprojetos.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgoprojetos.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetos.TabIndex;
end;

procedure Tfgoprojetos.sessao;
begin
   try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfgoprojetos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgoprojetos.verificarEmTransacao(cdsgoprojetos);
	fncgosupervisor.verificarEmTransacao(cdsgosupervisor);
	fncgoprojetoconsultor.verificarEmTransacao(cdsgoprojetoconsultor);
	fncgoprojetofilial.verificarEmTransacao(cdsgoprojetofilial);
	fncgoprojetofundo.verificarEmTransacao(cdsgoprojetofundo);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgoprojetos.configurarGridesFormulario(fgoprojetos,true, false);

	fncgoprojetos.Free;
	fncgosupervisor.Free;
	fncgoprojetoconsultor.Free;
	fncgoprojetofilial.Free;
	fncgoprojetofundo.Free;
	{eliminando container de fun��es da memoria<<<}

	fgoprojetos:=nil;
	Action:=cafree;
end;

procedure Tfgoprojetos.FormCreate(Sender: TObject);
begin

//  sessao;

  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
    if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
     begin
       dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
       dm.cxLocalizer.LanguageIndex := 1;
       dm.cxLocalizer.Active := TRUE;
     end;
    {>>>container de fun��es}
    fncgoprojetos:= TFuncoes.Create(Self);
    fncgoprojetos.definirConexao(dm.conexao);
    fncgoprojetos.definirConexaohistorico(dm.conexao);
    fncgoprojetos.definirFormulario(Self);
    fncgoprojetos.validarTransacaoRelacionamento:=true;
    fncgoprojetos.autoAplicarAtualizacoesBancoDados:=true;

    fncgosupervisor:= TFuncoes.Create(Self);
    fncgosupervisor.definirConexao(dm.conexao);
    fncgosupervisor.definirConexaohistorico(dm.conexao);
    fncgosupervisor.definirFormulario(Self);
    fncgosupervisor.validarTransacaoRelacionamento:=true;
    fncgosupervisor.autoAplicarAtualizacoesBancoDados:=true;

    fncgoprojetoconsultor:= TFuncoes.Create(Self);
    fncgoprojetoconsultor.definirConexao(dm.conexao);
    fncgoprojetoconsultor.definirConexaohistorico(dm.conexao);
    fncgoprojetoconsultor.definirFormulario(Self);
    fncgoprojetoconsultor.validarTransacaoRelacionamento:=true;
    fncgoprojetoconsultor.autoAplicarAtualizacoesBancoDados:=true;

    fncgoprojetofilial:= TFuncoes.Create(Self);
    fncgoprojetofilial.definirConexao(dm.conexao);
    fncgoprojetofilial.definirConexaohistorico(dm.conexao);
    fncgoprojetofilial.definirFormulario(Self);
    fncgoprojetofilial.validarTransacaoRelacionamento:=true;
    fncgoprojetofilial.autoAplicarAtualizacoesBancoDados:=true;

    fncgoprojetofundo:= TFuncoes.Create(Self);
    fncgoprojetofundo.definirConexao(dm.conexao);
    fncgoprojetofundo.definirConexaohistorico(dm.conexao);
    fncgoprojetofundo.definirFormulario(Self);
    fncgoprojetofundo.validarTransacaoRelacionamento:=true;
    fncgoprojetofundo.autoAplicarAtualizacoesBancoDados:=true;
    {container de fun��es<<<}


    lblTituloversao.Caption :=lblTituloversao.Caption+' - '+fncgoprojetofundo.recuperarVersaoExecutavel;
    lblusuario.Caption      :=lblusuario.Caption + ParamStr(3);

    {>>>verificar campos duplicidade}
    duplicidadeCampogoprojetos:=TDuplicidade.Create;
    duplicidadeCampogoprojetos.agrupador:='';
    duplicidadeCampogoprojetos.objeto :=dbegoprojetosCODGRUPO;
    duplicidadeListagoprojetos [0]:=duplicidadeCampogoprojetos;

    duplicidadeCampogoprojetos:=TDuplicidade.Create;
    duplicidadeCampogoprojetos.agrupador:='';
    duplicidadeCampogoprojetos.objeto :=dbegoprojetosDESCRICAO;
    duplicidadeListagoprojetos [1]:=duplicidadeCampogoprojetos;

    duplicidadeCampogoprojetos:=TDuplicidade.Create;
    duplicidadeCampogoprojetos.agrupador:='';
    duplicidadeCampogoprojetos.objeto :=cbogoprojetosTIPO;
    duplicidadeListagoprojetos [2]:=duplicidadeCampogoprojetos;
    {>>>verificar campos duplicidade}


    {>>>verificar campos duplicidade}
    duplicidadeCampogoprojetofundo:=TDuplicidade.Create;
    duplicidadeCampogoprojetofundo.agrupador:='';
    duplicidadeCampogoprojetofundo.objeto :=dbegoprojetofundoCODPORTADOR;
    duplicidadeListagoprojetofundo [0]:=duplicidadeCampogoprojetofundo;

    duplicidadeCampogoprojetofundo:=TDuplicidade.Create;
    duplicidadeCampogoprojetofundo.agrupador:='';
    duplicidadeCampogoprojetofundo.objeto :=dbegoprojetofundoCODPROJETO;
    duplicidadeListagoprojetofundo [1]:=duplicidadeCampogoprojetofundo;
    {>>>verificar campos duplicidade}

    {>>>verificar campos duplicidade}
    duplicidadeCampogoprojetoconsultor:=TDuplicidade.Create;
    duplicidadeCampogoprojetoconsultor.agrupador:='';
    duplicidadeCampogoprojetoconsultor.objeto :=dbegoprojetoconsultorCODPROJETO;
    duplicidadeListagoprojetoconsultor [0]:=duplicidadeCampogoprojetoconsultor;

    duplicidadeCampogoprojetoconsultor:=TDuplicidade.Create;
    duplicidadeCampogoprojetoconsultor.agrupador:='';
    duplicidadeCampogoprojetoconsultor.objeto :=dbegoprojetosDESCRICAO;
    duplicidadeListagoprojetoconsultor [1]:=duplicidadeCampogoprojetoconsultor;
    {>>>verificar campos duplicidade}




    {>>>verificar campos duplicidade}
    duplicidadeCampogoprojetoconsultor:=TDuplicidade.Create;
    duplicidadeCampogoprojetoconsultor.agrupador:='';
    duplicidadeCampogoprojetoconsultor.objeto :=dbegoprojetoconsultorcodprojeto;
    duplicidadeListagoprojetoconsultor[0]:=duplicidadeCampogoprojetoconsultor;

    duplicidadeCampogoprojetoconsultor:=TDuplicidade.Create;
    duplicidadeCampogoprojetoconsultor.agrupador:='';
    duplicidadeCampogoprojetoconsultor.objeto :=dbegoprojetoconsultorcodconsultor;
    duplicidadeListagoprojetoconsultor[1]:=duplicidadeCampogoprojetoconsultor;

    duplicidadeCampogoprojetofilial:=TDuplicidade.Create;
    duplicidadeCampogoprojetofilial.agrupador:='';
    duplicidadeCampogoprojetofilial.objeto :=dbegoprojetofilialfilial;
    duplicidadeListagoprojetofilial[0]:=duplicidadeCampogoprojetofilial;

    duplicidadeCampogoprojetofilial:=TDuplicidade.Create;
    duplicidadeCampogoprojetofilial.agrupador:='';
    duplicidadeCampogoprojetofilial.objeto :=dbegoprojetofilialcodfilial;
    duplicidadeListagoprojetofilial[1]:=duplicidadeCampogoprojetofilial;

    duplicidadeCampogoprojetofilial:=TDuplicidade.Create;
    duplicidadeCampogoprojetofilial.agrupador:='';
    duplicidadeCampogoprojetofilial.objeto :=dbegoprojetofilialCODPROJETO;
    duplicidadeListagoprojetofilial[2]:=duplicidadeCampogoprojetofilial;

    {verificar campos duplicidade<<<}

    {>>>gerando objetos de pesquisas}
    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesID';
    pesquisasItem.titulo:='C�digo';
    pesquisasItem.campo:='ID';
    pesquisasItem.tipoFDB:='Integer';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[0]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDT_CADASTRO';
    pesquisasItem.titulo:='Data de Cadastro';
    pesquisasItem.campo:='DT_CADASTRO';
    pesquisasItem.tipoFDB:='Date';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[1]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDESCRICAO';
    pesquisasItem.titulo:='Descri��o';
    pesquisasItem.campo:='DESCRICAO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='LIKE';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[2]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesTIPO';
    pesquisasItem.titulo:='Tipo';
    pesquisasItem.campo:='TIPO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.comboItens.add('Financeiro');
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[3]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesSTATUS';
    pesquisasItem.titulo:='Status';
    pesquisasItem.campo:='STATUS';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=180;
    pesquisasItem.comboItens.clear;
    pesquisasItem.comboItens.add('Iniciado');
    pesquisasItem.comboItens.add('Em andamento');
    pesquisasItem.comboItens.add('Pausado');
    pesquisasItem.comboItens.add('Cancelado');
    pesquisasItem.comboItens.add('Finalizado');
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[4]:=pesquisasItem;

    {gerando campos no clientdatsaet de filtros}
    fncgoprojetos.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
    {gerando campos no clientdatsaet de filtros}

    fncgoprojetos.criarPesquisas(sqlgoprojetos,cdsgoprojetos,'select * from goprojetos', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
    {gerando objetos de pesquisas<<<}

    {>>>campos obrigatorios goprojetos}
    camposObrigatoriosgoprojetos[0] :=dbegoprojetosdescricao;
    camposObrigatoriosgoprojetos[1] :=cbogoprojetostipo;
    {campos obrigatorios goprojetos<<<}

    {>>>objetos ativos no modo de manipula��o de dados goprojetos}
    objetosModoEdicaoAtivosgoprojetos[0]:=spdCancelargoprojetos;
    objetosModoEdicaoAtivosgoprojetos[1]:=spdGravargoprojetos;
    objetosModoEdicaoAtivosgoprojetos[2]:=dbegoprojetosCODGRUPO;
    objetosModoEdicaoAtivosgoprojetos[3]:=btngrupoprojeto;
    objetosModoEdicaoAtivosgoprojetos[4]:=dbegoprojetosCODGRUPO;
    objetosModoEdicaoAtivosgoprojetos[5]:=btngrupoprojeto;
    {objetos ativos no modo de manipula��o de dados goprojetos<<<}

    {>>>objetos inativos no modo de manipula��o de dados goprojetos}
    objetosModoEdicaoInativosgoprojetos[0]:=spdAdicionargoprojetos;
    objetosModoEdicaoInativosgoprojetos[1]:=spdAlterargoprojetos;
    objetosModoEdicaoInativosgoprojetos[2]:=spdExcluirgoprojetos;
    objetosModoEdicaoInativosgoprojetos[3]:=spdOpcoes;
    objetosModoEdicaoInativosgoprojetos[4]:=tabPesquisas;
    objetosModoEdicaoInativosgoprojetos[5]:=tabgoprojetofundo;
    objetosModoEdicaoInativosgoprojetos[6]:=tabgoprojetofilial;
    objetosModoEdicaoInativosgoprojetos[7]:=tabgoprojetoconsultor;
    objetosModoEdicaoInativosgoprojetos[8]:=dbegoprojetosGRUPO;


    {objetos inativos no modo de manipula��o de dados goprojetos<<<}

    {>>>comando de adi��o de teclas de atalhos goprojetos}
    fncgoprojetos.criaAtalhoPopupMenu(Popupgoprojetos,'Adicionar novo registro',VK_F4,spdAdicionargoprojetos.OnClick);
    fncgoprojetos.criaAtalhoPopupMenu(Popupgoprojetos,'Alterar registro selecionado',VK_F5,spdAlterargoprojetos.OnClick);
    fncgoprojetos.criaAtalhoPopupMenu(Popupgoprojetos,'Gravar �ltimas altera��es',VK_F6,spdGravargoprojetos.OnClick);
    fncgoprojetos.criaAtalhoPopupMenu(Popupgoprojetos,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoprojetos.OnClick);
    fncgoprojetos.criaAtalhoPopupMenu(Popupgoprojetos,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoprojetos.OnClick);

    {comando de adi��o de teclas de atalhos goprojetos<<<}

    pgcPrincipal.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdsgoprojetos.open();
    {abertura dos datasets<<<}

    {>>>campos obrigatorios gosupervisor}
    camposObrigatoriosgosupervisor[0] :=dbegosupervisornome;
    camposObrigatoriosgosupervisor[1] :=dbegosupervisorcpf;
    {campos obrigatorios gosupervisor<<<}

    {>>>objetos ativos no modo de manipula��o de dados gosupervisor}
    objetosModoEdicaoAtivosgosupervisor[0]:=spdCancelargosupervisor;
    objetosModoEdicaoAtivosgosupervisor[1]:=spdGravargosupervisor;
    {objetos ativos no modo de manipula��o de dados gosupervisor<<<}

    {>>>objetos inativos no modo de manipula��o de dados gosupervisor}
    objetosModoEdicaoInativosgosupervisor[0]:=spdAdicionargosupervisor;
    objetosModoEdicaoInativosgosupervisor[1]:=spdAlterargosupervisor;
    objetosModoEdicaoInativosgosupervisor[2]:=spdExcluirgosupervisor;
    objetosModoEdicaoInativosgosupervisor[3]:=spdOpcoes;
    objetosModoEdicaoInativosgosupervisor[4]:=tabPesquisas;
    objetosModoEdicaoInativosgosupervisor[5]:=tabgoprojetos;
    objetosModoEdicaoInativosgosupervisor[6]:=tabgoprojetoconsultor;
    objetosModoEdicaoInativosgosupervisor[7]:=tabgoprojetofilial;
    objetosModoEdicaoInativosgosupervisor[8]:=tabgoprojetofundo;
    {objetos inativos no modo de manipula��o de dados gosupervisor<<<}

    {>>>comando de adi��o de teclas de atalhos gosupervisor}
    fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Adicionar novo registro',VK_F4,spdAdicionargosupervisor.OnClick);
    fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Alterar registro selecionado',VK_F5,spdAlterargosupervisor.OnClick);
    fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Gravar �ltimas altera��es',VK_F6,spdGravargosupervisor.OnClick);
    fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Cancelar �ltimas altera��e',VK_F7,spdCancelargosupervisor.OnClick);
    fncgosupervisor.criaAtalhoPopupMenu(Popupgosupervisor,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgosupervisor.OnClick);
    {comando de adi��o de teclas de atalhos gosupervisor<<<}

    pgcPrincipal.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdsgosupervisor.open();
    {abertura dos datasets<<<}

    {>>>campos obrigatorios goprojetoconsultor}
    camposObrigatoriosgoprojetoconsultor[0] :=dbegoprojetoconsultorcodconsultor;
    {campos obrigatorios goprojetoconsultor<<<}

    {>>>objetos ativos no modo de manipula��o de dados goprojetoconsultor}
    objetosModoEdicaoAtivosgoprojetoconsultor[0]:=spdCancelargoprojetoconsultor;
    objetosModoEdicaoAtivosgoprojetoconsultor[1]:=spdGravargoprojetoconsultor;
    objetosModoEdicaoAtivosgoprojetoconsultor[2]:=dbegoprojetoconsultorCODCONSULTOR;
    objetosModoEdicaoAtivosgoprojetoconsultor[3]:= btnconsultor;
    {objetos ativos no modo de manipula��o de dados goprojetoconsultor<<<}

    {>>>objetos inativos no modo de manipula��o de dados goprojetoconsultor}
    objetosModoEdicaoInativosgoprojetoconsultor[0]:=spdAdicionargoprojetoconsultor;
    objetosModoEdicaoInativosgoprojetoconsultor[1]:=spdAlterargoprojetoconsultor;
    objetosModoEdicaoInativosgoprojetoconsultor[2]:=spdExcluirgoprojetoconsultor;
    objetosModoEdicaoInativosgoprojetoconsultor[3]:=spdOpcoes;
    objetosModoEdicaoInativosgoprojetoconsultor[4]:=tabPesquisas;
    objetosModoEdicaoInativosgoprojetoconsultor[5]:=tabgoprojetos;
    objetosModoEdicaoInativosgoprojetoconsultor[6]:=tabgosupervisor;
    objetosModoEdicaoInativosgoprojetoconsultor[7]:=tabgoprojetofilial;
    objetosModoEdicaoInativosgoprojetoconsultor[8]:=tabgoprojetofundo;
    {objetos inativos no modo de manipula��o de dados goprojetoconsultor<<<}

    {>>>comando de adi��o de teclas de atalhos goprojetoconsultor}
    fncgoprojetoconsultor.criaAtalhoPopupMenu(Popupgoprojetoconsultor,'Adicionar novo registro',VK_F4,spdAdicionargoprojetoconsultor.OnClick);
    fncgoprojetoconsultor.criaAtalhoPopupMenu(Popupgoprojetoconsultor,'Alterar registro selecionado',VK_F5,spdAlterargoprojetoconsultor.OnClick);
    fncgoprojetoconsultor.criaAtalhoPopupMenu(Popupgoprojetoconsultor,'Gravar �ltimas altera��es',VK_F6,spdGravargoprojetoconsultor.OnClick);
    fncgoprojetoconsultor.criaAtalhoPopupMenu(Popupgoprojetoconsultor,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoprojetoconsultor.OnClick);
    fncgoprojetoconsultor.criaAtalhoPopupMenu(Popupgoprojetoconsultor,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoprojetoconsultor.OnClick);

    {comando de adi��o de teclas de atalhos goprojetoconsultor<<<}

    pgcPrincipal.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdsgoprojetoconsultor.open();
    {abertura dos datasets<<<}

    {>>>campos obrigatorios goprojetofilial}
    camposObrigatoriosgoprojetofilial[0]:= dbegoprojetofilialCODFILIAL;
    {campos obrigatorios goprojetofilial<<<}

    {>>>objetos ativos no modo de manipula��o de dados goprojetofilial}
    objetosModoEdicaoAtivosgoprojetofilial[0]:=spdCancelargoprojetofilial;
    objetosModoEdicaoAtivosgoprojetofilial[1]:=spdGravargoprojetofilial;
    objetosModoEdicaoAtivosgoprojetofilial[2]:=dbegoprojetofilialCODFILIAL;
    objetosModoEdicaoAtivosgoprojetofilial[3]:=btnfilial;
    {objetos ativos no modo de manipula��o de dados goprojetofilial<<<}

    {>>>objetos inativos no modo de manipula��o de dados goprojetofilial}
    objetosModoEdicaoInativosgoprojetofilial[0]:=spdAdicionargoprojetofilial;
    objetosModoEdicaoInativosgoprojetofilial[1]:=spdAlterargoprojetofilial;
    objetosModoEdicaoInativosgoprojetofilial[2]:=spdExcluirgoprojetofilial;
    objetosModoEdicaoInativosgoprojetofilial[3]:=spdOpcoes;
    objetosModoEdicaoInativosgoprojetofilial[4]:=tabPesquisas;
    objetosModoEdicaoInativosgoprojetofilial[5]:=tabgoprojetos;
    objetosModoEdicaoInativosgoprojetofilial[6]:=tabgosupervisor;
    objetosModoEdicaoInativosgoprojetofilial[7]:=tabgoprojetoconsultor;
    objetosModoEdicaoInativosgoprojetofilial[8]:=tabgoprojetofundo;
    {objetos inativos no modo de manipula��o de dados goprojetofilial<<<}

    {>>>comando de adi��o de teclas de atalhos goprojetofilial}
    fncgoprojetofilial.criaAtalhoPopupMenu(Popupgoprojetofilial,'Adicionar novo registro',VK_F4,spdAdicionargoprojetofilial.OnClick);
    fncgoprojetofilial.criaAtalhoPopupMenu(Popupgoprojetofilial,'Alterar registro selecionado',VK_F5,spdAlterargoprojetofilial.OnClick);
    fncgoprojetofilial.criaAtalhoPopupMenu(Popupgoprojetofilial,'Gravar �ltimas altera��es',VK_F6,spdGravargoprojetofilial.OnClick);
    fncgoprojetofilial.criaAtalhoPopupMenu(Popupgoprojetofilial,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoprojetofilial.OnClick);
    fncgoprojetofilial.criaAtalhoPopupMenu(Popupgoprojetofilial,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoprojetofilial.OnClick);

    {comando de adi��o de teclas de atalhos goprojetofilial<<<}

    pgcPrincipal.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdsgoprojetofilial.open();
    {abertura dos datasets<<<}

    {>>>campos obrigatorios goprojetofundo}
    {campos obrigatorios goprojetofundo<<<}

    {>>>objetos ativos no modo de manipula��o de dados goprojetofundo}
    objetosModoEdicaoAtivosgoprojetofundo[0]:=spdCancelargoprojetofundo;
    objetosModoEdicaoAtivosgoprojetofundo[1]:=spdGravargoprojetofundo;
    objetosModoEdicaoAtivosgoprojetofundo[2]:=dbegoprojetofundoCODPORTADOR;
    objetosModoEdicaoAtivosgoprojetofundo[3]:=btnportador;
    {objetos ativos no modo de manipula��o de dados goprojetofundo<<<}

    {>>>objetos inativos no modo de manipula��o de dados goprojetofundo}
    objetosModoEdicaoInativosgoprojetofundo[0]:=spdAdicionargoprojetofundo;
    objetosModoEdicaoInativosgoprojetofundo[1]:=spdAlterargoprojetofundo;
    objetosModoEdicaoInativosgoprojetofundo[2]:=spdExcluirgoprojetofundo;
    objetosModoEdicaoInativosgoprojetofundo[3]:=spdOpcoes;
    objetosModoEdicaoInativosgoprojetofundo[4]:=tabPesquisas;
    objetosModoEdicaoInativosgoprojetofundo[5]:=tabgoprojetos;
    objetosModoEdicaoInativosgoprojetofundo[6]:=tabgosupervisor;
    objetosModoEdicaoInativosgoprojetofundo[7]:=tabgoprojetoconsultor;
    objetosModoEdicaoInativosgoprojetofundo[8]:=tabgoprojetofilial;
    {objetos inativos no modo de manipula��o de dados goprojetofundo<<<}

    {>>>comando de adi��o de teclas de atalhos goprojetofundo}
    fncgoprojetofundo.criaAtalhoPopupMenu(Popupgoprojetofundo,'Adicionar novo registro',VK_F4,spdAdicionargoprojetofundo.OnClick);
    fncgoprojetofundo.criaAtalhoPopupMenu(Popupgoprojetofundo,'Alterar registro selecionado',VK_F5,spdAlterargoprojetofundo.OnClick);
    fncgoprojetofundo.criaAtalhoPopupMenu(Popupgoprojetofundo,'Gravar �ltimas altera��es',VK_F6,spdGravargoprojetofundo.OnClick);
    fncgoprojetofundo.criaAtalhoPopupMenu(Popupgoprojetofundo,'Cancelar �ltimas altera��e',VK_F7,spdCancelargoprojetofundo.OnClick);
    fncgoprojetofundo.criaAtalhoPopupMenu(Popupgoprojetofundo,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgoprojetofundo.OnClick);

    {comando de adi��o de teclas de atalhos goprojetofundo<<<}
  end);
	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgoprojetofundo.open();
	{abertura dos datasets<<<}

	fncgoprojetos.criaAtalhoPopupMenuNavegacao(Popupgoprojetos,cdsgoprojetos);

	fncgoprojetos.definirMascaraCampos(cdsgoprojetos);

	fncgoprojetos.configurarGridesFormulario(fgoprojetos,false, true);

	fncgosupervisor.definirMascaraCampos(cdsgosupervisor);

	fncgoprojetoconsultor.definirMascaraCampos(cdsgoprojetoconsultor);

	fncgoprojetofilial.definirMascaraCampos(cdsgoprojetofilial);

	fncgoprojetofundo.definirMascaraCampos(cdsgoprojetofundo);

end;

procedure Tfgoprojetos.spdAdicionargoprojetosClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetos.TabIndex;
	if (fncgoprojetos.adicionar(cdsgoprojetos)=true) then
		begin
			fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,true);
			dbegoprojetosCODGRUPO.SetFocus;
	end
	else
		fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,false);
end;

procedure Tfgoprojetos.spdAlterargoprojetosClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetos.TabIndex;
	if (fncgoprojetos.alterar(cdsgoprojetos)=true) then
		begin
			fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,true);
			dbegoprojetosCODGRUPO.SetFocus;
	end
	else
		fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,false);
end;

procedure Tfgoprojetos.spdGravargoprojetosClick(Sender: TObject);
begin
	fncgoprojetos.verificarCamposObrigatorios(cdsgoprojetos, camposObrigatoriosgoprojetos,pgcPrincipal);
  fncgoprojetos.verificarDuplicidade(dm.FDConexao, cdsgoprojetos, 'goprojetos', 'ID', duplicidadeListagoprojetos);

	if (fncgoprojetos.gravar(cdsgoprojetos)=true) then
		fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,false)
	else
		fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,true);
end;

procedure Tfgoprojetos.spdCancelargoprojetosClick(Sender: TObject);
begin
	if (fncgoprojetos.cancelar(cdsgoprojetos)=true) then
		fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,false)
	else
		fncgoprojetos.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetos,objetosModoEdicaoInativosgoprojetos,true);
end;

procedure Tfgoprojetos.spdExcluirgoprojetosClick(Sender: TObject);
begin
	fncgoprojetos.excluir(cdsgoprojetos);
end;

procedure Tfgoprojetos.cdsgoprojetosBeforePost(DataSet: TDataSet);
begin
	if cdsgoprojetos.State in [dsinsert] then
	cdsgoprojetosID.Value:=fncgoprojetos.autoNumeracaoGenerator(dm.conexao,'G_goprojetos');
end;

procedure Tfgoprojetos.spdAdicionargosupervisorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgosupervisor.TabIndex;
	if (fncgosupervisor.adicionar(cdsgosupervisor)=true) then
		begin
			fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
			dbegosupervisornome.SetFocus;
	end
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false);
end;

procedure Tfgoprojetos.spdAlterargosupervisorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgosupervisor.TabIndex;
	if (fncgosupervisor.alterar(cdsgosupervisor)=true) then
		begin
			fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
			dbegosupervisornome.SetFocus;
	end
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false);
end;

procedure Tfgoprojetos.spdGravargosupervisorClick(Sender: TObject);
begin
	fncgosupervisor.verificarCamposObrigatorios(cdsgosupervisor, camposObrigatoriosgosupervisor);

	if (fncgosupervisor.gravar(cdsgosupervisor)=true) then
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false)
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
end;

procedure Tfgoprojetos.spdCancelargosupervisorClick(Sender: TObject);
begin
	if (fncgosupervisor.cancelar(cdsgosupervisor)=true) then
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,false)
	else
		fncgosupervisor.ativarModoEdicao(objetosModoEdicaoAtivosgosupervisor,objetosModoEdicaoInativosgosupervisor,true);
end;

procedure Tfgoprojetos.spdExcluirgosupervisorClick(Sender: TObject);
begin
	fncgosupervisor.excluir(cdsgosupervisor);
end;

procedure Tfgoprojetos.cdsgosupervisorBeforePost(DataSet: TDataSet);
begin
	if cdsgosupervisor.State in [dsinsert] then
	cdsgosupervisorID.Value:=fncgosupervisor.autoNumeracaoGenerator(dm.conexao,'G_gosupervisor');
end;


procedure Tfgoprojetos.spdAdicionargoprojetoconsultorClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=4;

	if (fncgoprojetoconsultor.adicionar(cdsgoprojetoconsultor)=true) then
		begin
			fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,true);
			dbegoprojetoconsultorCODCONSULTOR.SetFocus;
	end
	else
		fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,false);
end;

procedure Tfgoprojetos.spdAlterargoprojetoconsultorClick(Sender: TObject);
begin
 pgcPrincipal.ActivePageIndex:=4;
	if (fncgoprojetoconsultor.alterar(cdsgoprojetoconsultor)=true) then
		begin
			fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,true);
			dbegoprojetoconsultorCODCONSULTOR.SetFocus;
	end
	else
		fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,false);
end;

procedure Tfgoprojetos.spdGravargoprojetoconsultorClick(Sender: TObject);
begin
	fncgoprojetoconsultor.verificarCamposObrigatorios(cdsgoprojetoconsultor, camposObrigatoriosgoprojetoconsultor,pgcPrincipal);
	fncgoprojetoconsultor.verificarDuplicidade(dm.FDConexao, cdsgoprojetoconsultor, 'goprojetoconsultor', 'ID', duplicidadeListagoprojetoconsultor);

	if (fncgoprojetoconsultor.gravar(cdsgoprojetoconsultor)=true) then
		fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,false)
	else
		fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,true);
end;

procedure Tfgoprojetos.spdCancelargoprojetoconsultorClick(Sender: TObject);
begin
	if (fncgoprojetoconsultor.cancelar(cdsgoprojetoconsultor)=true) then
		fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,false)
	else
		fncgoprojetoconsultor.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetoconsultor,objetosModoEdicaoInativosgoprojetoconsultor,true);
end;

procedure Tfgoprojetos.spdExcluirgoprojetoconsultorClick(Sender: TObject);
begin
	fncgoprojetoconsultor.excluir(cdsgoprojetoconsultor);
end;

procedure Tfgoprojetos.spdAdicionargoprojetofilialClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetofilial.TabIndex;
	if (fncgoprojetofilial.adicionar(cdsgoprojetofilial)=true) then
		begin
			fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,true);
			dbegoprojetofilialcodfilial.SetFocus;
	end
	else
		fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,false);
end;

procedure Tfgoprojetos.spdAlterargoprojetofilialClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetofilial.TabIndex;
	if (fncgoprojetofilial.alterar(cdsgoprojetofilial)=true) then
		begin
			fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,true);
			dbegoprojetofilialCODFILIAL.SetFocus;
	end
	else
		fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,false);
end;

procedure Tfgoprojetos.spdGravargoprojetofilialClick(Sender: TObject);
begin
  fncgoprojetofilial.verificarCamposObrigatorios(cdsgoprojetofilial, camposObrigatoriosgoprojetofilial, pgcPrincipal);
	fncgoprojetofilial.verificarDuplicidade(dm.FDConexao, cdsgoprojetofilial, 'goprojetofilial', 'ID', duplicidadeListagoprojetofilial);

	if (fncgoprojetofilial.gravar(cdsgoprojetofilial)=true) then
		fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,false)
	else
		fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,true);
end;

procedure Tfgoprojetos.spdCancelargoprojetofilialClick(Sender: TObject);
begin
	if (fncgoprojetofilial.cancelar(cdsgoprojetofilial)=true) then
		fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,false)
	else
		fncgoprojetofilial.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofilial,objetosModoEdicaoInativosgoprojetofilial,true);
end;

procedure Tfgoprojetos.spdExcluirgoprojetofilialClick(Sender: TObject);
begin
	fncgoprojetofilial.excluir(cdsgoprojetofilial);
end;

procedure Tfgoprojetos.cdsgoprojetoconsultorNewRecord(DataSet: TDataSet);
begin
	if cdsgoprojetoconsultor.State in [dsinsert] then
	cdsgoprojetoconsultorID.Value              :=fncgoprojetoconsultor.autoNumeracaoGenerator(dm.conexao,'G_goprojetoconsultor');
  cdsgoprojetoconsultorCODPROJETO.AsInteger  :=cdsgoprojetosID.AsInteger;
end;

procedure Tfgoprojetos.cdsgoprojetofilialNewRecord(DataSet: TDataSet);
begin
  	if cdsgoprojetofilial.State in [dsinsert] then
	cdsgoprojetofilialID.Value:=fncgoprojetofilial.autoNumeracaoGenerator(dm.conexao,'G_goprojetofilial');

  cdsgoprojetofilialCODPROJETO.AsInteger := cdsgoprojetosID.AsInteger;
  cdsgoprojetofilialPROJETO.AsString     := cdsgoprojetosDESCRICAO.AsString;

end;

procedure Tfgoprojetos.cdsgoprojetofundoNewRecord(DataSet: TDataSet);
begin
	if cdsgoprojetofundo.State in [dsinsert] then
	cdsgoprojetofundoID.Value:=fncgoprojetofundo.autoNumeracaoGenerator(dm.conexao,'G_goprojetofundo');

  cdsgoprojetofundoCODPROJETO.AsInteger := cdsgoprojetosID.AsInteger;
  cdsgoprojetofundoPROJETO.AsString     := cdsgoprojetosDESCRICAO.AsString;

end;

procedure Tfgoprojetos.spdAdicionargoprojetofundoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetofundo.TabIndex;
	if (fncgoprojetofundo.adicionar(cdsgoprojetofundo)=true) then
		begin
			fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,true);
			dbegoprojetofundocodportador.SetFocus;
	end
	else
		fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,false);
end;

procedure Tfgoprojetos.spdAlterargoprojetofundoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgoprojetofundo.TabIndex;
	if (fncgoprojetofundo.alterar(cdsgoprojetofundo)=true) then
		begin
			fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,true);
			dbegoprojetofundocodportador.SetFocus;
	end
	else
		fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,false);
end;

procedure Tfgoprojetos.spdGravargoprojetofundoClick(Sender: TObject);
begin
  fncgoprojetofundo.verificarCamposObrigatorios(cdsgoprojetofundo, camposObrigatoriosgoprojetofundo,pgcPrincipal);
  fncgoprojetofundo.verificarDuplicidade(dm.FDConexao, cdsgoprojetofundo, 'goprojetofundo', 'ID', duplicidadeListagoprojetofundo);

	if (fncgoprojetofundo.gravar(cdsgoprojetofundo)=true) then
		fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,false)
	else
		fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,true);
end;

procedure Tfgoprojetos.spdCancelargoprojetofundoClick(Sender: TObject);
begin
	if (fncgoprojetofundo.cancelar(cdsgoprojetofundo)=true) then
		fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,false)
	else
		fncgoprojetofundo.ativarModoEdicao(objetosModoEdicaoAtivosgoprojetofundo,objetosModoEdicaoInativosgoprojetofundo,true);
end;

procedure Tfgoprojetos.spdExcluirgoprojetofundoClick(Sender: TObject);
begin
	fncgoprojetofundo.excluir(cdsgoprojetofundo);
end;

procedure Tfgoprojetos.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgoprojetos.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgoprojetos.cdsgoprojetosAfterOpen(DataSet: TDataSet);
begin
	cdsgoprojetoconsultor.Open;
	cdsgoprojetofilial.Open;
	cdsgoprojetofundo.Open;
end;

procedure Tfgoprojetos.cdsgoprojetosAfterClose(DataSet: TDataSet);
begin
	cdsgoprojetoconsultor.Close;
	cdsgoprojetofilial.Close;
	cdsgoprojetofundo.Close;
end;

procedure Tfgoprojetos.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgoprojetos.spdOpcoesClick(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgoprojetos.menuImpRelSintetico1Click(Sender: TObject);
begin
if not cdsgoprojetos.IsEmpty then 
	fncgoprojetos.visualizarRelatorios(frxgoprojetos);
end;

end.

