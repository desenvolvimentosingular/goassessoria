unit ugocarteira;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 07/08/2021*}                                          
{*Hora 12:11:02*}                                            
{*Unit gocarteira *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr;

type
	Tfgocarteira = class(TForm)
    sqlgocarteira: TSQLDataSet;
	dspgocarteira: TDataSetProvider;
	cdsgocarteira: TClientDataSet;
	dsgocarteira: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgocarteira: TfrxReport;
	frxDBgocarteira: TfrxDBDataset;
	Popupgocarteira: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgocarteira: tcxTabSheet;
	SCRgocarteira: TScrollBox;
	pnlDadosgocarteira: TPanel;
	{>>>bot�es de manipula��o de dados gocarteira}
	pnlLinhaBotoesgocarteira: TPanel;
	spdAdicionargocarteira: TButton;
	spdAlterargocarteira: TButton;
	spdGravargocarteira: TButton;
	spdCancelargocarteira: TButton;
	spdExcluirgocarteira: TButton;

	{bot�es de manipula��o de dadosgocarteira<<<}
	{campo ID}
	sqlgocarteiraID: TIntegerField;
	cdsgocarteiraID: TIntegerField;
	pnlLayoutLinhasgocarteira1: TPanel;
	pnlLayoutTitulosgocarteiraid: TPanel;
	pnlLayoutCamposgocarteiraid: TPanel;
	dbegocarteiraid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgocarteiraDT_CADASTRO: TDateField;
	cdsgocarteiraDT_CADASTRO: TDateField;
	pnlLayoutLinhasgocarteira2: TPanel;
	pnlLayoutTitulosgocarteiradt_cadastro: TPanel;
	pnlLayoutCamposgocarteiradt_cadastro: TPanel;
	dbegocarteiradt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgocarteiraHS_CADASTRO: TTimeField;
	cdsgocarteiraHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgocarteira3: TPanel;
	pnlLayoutTitulosgocarteirahs_cadastro: TPanel;
	pnlLayoutCamposgocarteirahs_cadastro: TPanel;
	dbegocarteirahs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlgocarteiraDESCRICAO: TStringField;
	cdsgocarteiraDESCRICAO: TStringField;
	pnlLayoutLinhasgocarteira4: TPanel;
	pnlLayoutTitulosgocarteiradescricao: TPanel;
	pnlLayoutCamposgocarteiradescricao: TPanel;
	dbegocarteiradescricao: TDBEdit;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    cdsfiltros: TClientDataSet;
    frxDBDfiltros: TfrxDBDataset;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargocarteiraClick(Sender: TObject);
	procedure spdAlterargocarteiraClick(Sender: TObject);
	procedure spdGravargocarteiraClick(Sender: TObject);
	procedure spdCancelargocarteiraClick(Sender: TObject);
	procedure spdExcluirgocarteiraClick(Sender: TObject);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdsgocarteiraNewRecord(DataSet: TDataSet);
    procedure spdOpcoesClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgocarteira: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgocarteira: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgocarteira: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgocarteira: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagocarteira: Array[0..0]  of TDuplicidade;
	duplicidadeCampogocarteira: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
    procedure permissao;
	public

end;

var
fgocarteira: Tfgocarteira;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgocarteira.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgocarteira.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
 pgcPrincipal.ActivePageIndex:=tabgocarteira.TabIndex;
end;

procedure Tfgocarteira.cdsgocarteiraNewRecord(DataSet: TDataSet);
begin
	if cdsgocarteira.State in [dsinsert] then
	cdsgocarteiraID.Value:=fncgocarteira.autoNumeracaoGenerator(dm.conexao,'G_gocarteira');
end;

procedure Tfgocarteira.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgocarteira.verificarEmTransacao(cdsgocarteira);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgocarteira.configurarGridesFormulario(fgocarteira,true, false);

	fncgocarteira.Free;
	{eliminando container de fun��es da memoria<<<}

	fgocarteira:=nil;
	Action:=cafree;

end;

procedure Tfgocarteira.FormCreate(Sender: TObject);
begin
//  sessao;

  {<<>> fun��o de tradu�a� de componentes>>>}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  {<<>> fun��o de tradu�a� de componentes>>>}

	{>>>container de fun��es}
	fncgocarteira:= TFuncoes.Create(Self);
	fncgocarteira.definirConexao(dm.conexao);
	fncgocarteira.definirConexaohistorico(dm.conexao);
	fncgocarteira.definirFormulario(Self);
	fncgocarteira.validarTransacaoRelacionamento:=true;
	fncgocarteira.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	duplicidadeCampogocarteira:=TDuplicidade.Create;
	duplicidadeCampogocarteira.agrupador:='';
	duplicidadeCampogocarteira.objeto :=dbegocarteiraDESCRICAO;
	duplicidadeListagocarteira[0]     :=duplicidadeCampogocarteira;
	{verificar campos duplicidade<<<}


  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncgocarteira.recuperarVersaoExecutavel;
  lblusuario.Caption := lblusuario.Caption + ParamStr(3);

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncgocarteira.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncgocarteira.criarPesquisas(sqlgocarteira,cdsgocarteira,'select * from gocarteira', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gocarteira}

	camposObrigatoriosgocarteira[0]:=dbegocarteiraDESCRICAO;
	{campos obrigatorios gocarteira<<<}

	{>>>objetos ativos no modo de manipula��o de dados gocarteira}
	//objetosModoEdicaoAtivosgocarteira[0]:=pnlDadosgocarteira;
	objetosModoEdicaoAtivosgocarteira[1]:=spdCancelargocarteira;
	objetosModoEdicaoAtivosgocarteira[2]:=spdGravargocarteira;
	{objetos ativos no modo de manipula��o de dados gocarteira<<<}

	{>>>objetos inativos no modo de manipula��o de dados gocarteira}
	objetosModoEdicaoInativosgocarteira[0]:=spdAdicionargocarteira;
	objetosModoEdicaoInativosgocarteira[1]:=spdAlterargocarteira;
	objetosModoEdicaoInativosgocarteira[2]:=spdExcluirgocarteira;
	objetosModoEdicaoInativosgocarteira[3]:=spdOpcoes;
	objetosModoEdicaoInativosgocarteira[4]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gocarteira<<<}

	{>>>comando de adi��o de teclas de atalhos gocarteira}
	fncgocarteira.criaAtalhoPopupMenu(Popupgocarteira,'Adicionar novo registro',VK_F4,spdAdicionargocarteira.OnClick);
	fncgocarteira.criaAtalhoPopupMenu(Popupgocarteira,'Alterar registro selecionado',VK_F5,spdAlterargocarteira.OnClick);
	fncgocarteira.criaAtalhoPopupMenu(Popupgocarteira,'Gravar �ltimas altera��es',VK_F6,spdGravargocarteira.OnClick);
	fncgocarteira.criaAtalhoPopupMenu(Popupgocarteira,'Cancelar �ltimas altera��e',VK_F7,spdCancelargocarteira.OnClick);
	fncgocarteira.criaAtalhoPopupMenu(Popupgocarteira,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgocarteira.OnClick);
  {Comandos de permiss�o}
  permissao;
  {Comandos de permiss�o}


	{comando de adi��o de teclas de atalhos gocarteira<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgocarteira.open();
	{abertura dos datasets<<<}

	fncgocarteira.criaAtalhoPopupMenuNavegacao(Popupgocarteira,cdsgocarteira);

	fncgocarteira.definirMascaraCampos(cdsgocarteira);

	fncgocarteira.configurarGridesFormulario(fgocarteira,false, true);

end;

procedure Tfgocarteira.spdAdicionargocarteiraClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocarteira.TabIndex;
	if (fncgocarteira.adicionar(cdsgocarteira)=true) then
		begin
			fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,true);
			dbegocarteiradescricao.SetFocus;
	end
	else
		fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,false);
end;

procedure Tfgocarteira.spdAlterargocarteiraClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocarteira.TabIndex;
	if (fncgocarteira.alterar(cdsgocarteira)=true) then
		begin
			fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,true);
			dbegocarteiradescricao.SetFocus;
	end
	else
		fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,false);
end;

procedure Tfgocarteira.spdGravargocarteiraClick(Sender: TObject);
begin
  fncgocarteira.verificarCamposObrigatorios(cdsgocarteira, camposObrigatoriosgocarteira, pgcPrincipal);
  fncgocarteira.verificarDuplicidade(dm.fdconexao, cdsgocarteira, 'gocarteira', 'ID', duplicidadeListagocarteira);

	if (fncgocarteira.gravar(cdsgocarteira)=true) then
  begin
		fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,false);
    permissao;
  end
	else
		fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,true);
end;

procedure Tfgocarteira.spdCancelargocarteiraClick(Sender: TObject);
begin
	if (fncgocarteira.cancelar(cdsgocarteira)=true) then
  begin
		fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,false);
    permissao;
  end
	else
		fncgocarteira.ativarModoEdicao(objetosModoEdicaoAtivosgocarteira,objetosModoEdicaoInativosgocarteira,true);
end;

procedure Tfgocarteira.spdExcluirgocarteiraClick(Sender: TObject);
begin
	fncgocarteira.excluir(cdsgocarteira);
end;

procedure Tfgocarteira.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgocarteira.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgocarteira.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgocarteira.spdOpcoesClick(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgocarteira.sessao;
begin

 try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
 end;

end;

procedure Tfgocarteira.permissao;
begin
  {comandos de permiss�o}
  if fncgocarteira.permissao('GOCARTEIRA', 'Acesso', ParamStr(3)) = false then
  begin
    application.MessageBox('Sem permiss�o de acesso.', 'Permiss�o', MB_ICONEXCLAMATION);
  end;

  spdAdicionargocarteira.Enabled := fncgocarteira.permissao('GOCARTEIRA', 'Adicionar', ParamStr(3));
  spdAlterargocarteira.Enabled := fncgocarteira.permissao('GOCARTEIRA', 'Alterar', ParamStr(3));
  spdExcluirgocarteira.Enabled := fncgocarteira.permissao('GOCARTEIRA', 'Excluir', ParamStr(3));
  Popupgocarteira.Items[0].Enabled := fncgocarteira.permissao('GOCARTEIRA', 'Adicionar', ParamStr(3));
  Popupgocarteira.Items[1].Enabled := fncgocarteira.permissao('GOCARTEIRA', 'Alterar', ParamStr(3));
  Popupgocarteira.Items[4].Enabled := fncgocarteira.permissao('GOCARTEIRA', 'Excluir', ParamStr(3));

end;

procedure Tfgocarteira.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgocarteira.IsEmpty then
	fncgocarteira.visualizarRelatorios(frxgocarteira);
end;

end.

