unit ugogrupoproduto;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 17/11/2021*}                                          
{*Hora 18:26:52*}                                            
{*Unit gogrupoproduto *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, dxGDIPlusClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa, cxCurrencyEdit;

type                                                                                        
	Tfgogrupoproduto = class(TForm)
    sqlgogrupoproduto: TSQLDataSet;
	dspgogrupoproduto: TDataSetProvider;
	cdsgogrupoproduto: TClientDataSet;
	dsgogrupoproduto: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgogrupoproduto: TfrxReport;
	frxDBgogrupoproduto: TfrxDBDataset;
	Popupgogrupoproduto: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgogrupoproduto: tcxTabSheet;
	SCRgogrupoproduto: TScrollBox;
	pnlDadosgogrupoproduto: TPanel;
	{>>>bot�es de manipula��o de dados gogrupoproduto}
	pnlLinhaBotoesgogrupoproduto: TPanel;
	spdAdicionargogrupoproduto: TButton;
	spdAlterargogrupoproduto: TButton;
	spdGravargogrupoproduto: TButton;
	spdCancelargogrupoproduto: TButton;
	spdExcluirgogrupoproduto: TButton;

	{bot�es de manipula��o de dadosgogrupoproduto<<<}
	{campo ID}
	sqlgogrupoprodutoID: TIntegerField;
	cdsgogrupoprodutoID: TIntegerField;
	pnlLayoutLinhasgogrupoproduto1: TPanel;
	pnlLayoutTitulosgogrupoprodutoid: TPanel;
	pnlLayoutCamposgogrupoprodutoid: TPanel;
	dbegogrupoprodutoid: TDBEdit;

	{campo CODPROJETO}
	sqlgogrupoprodutoCODPROJETO: TIntegerField;
	cdsgogrupoprodutoCODPROJETO: TIntegerField;
	pnlLayoutLinhasgogrupoproduto2: TPanel;
	pnlLayoutTitulosgogrupoprodutocodprojeto: TPanel;
	pnlLayoutCamposgogrupoprodutocodprojeto: TPanel;
	dbegogrupoprodutocodprojeto: TDBEdit;

	{campo PROJETO}
	sqlgogrupoprodutoPROJETO: TStringField;
	cdsgogrupoprodutoPROJETO: TStringField;

	{campo CODCLIENTE}
	sqlgogrupoprodutoCODCLIENTE: TIntegerField;
	cdsgogrupoprodutoCODCLIENTE: TIntegerField;
	pnlLayoutLinhasgogrupoproduto4: TPanel;
	pnlLayoutTitulosgogrupoprodutocodcliente: TPanel;
	pnlLayoutCamposgogrupoprodutocodcliente: TPanel;
	dbegogrupoprodutocodcliente: TDBEdit;

	{campo CLIENTE}
	sqlgogrupoprodutoCLIENTE: TStringField;
	cdsgogrupoprodutoCLIENTE: TStringField;

	{campo DESCRICAO}
	sqlgogrupoprodutoDESCRICAO: TStringField;
	cdsgogrupoprodutoDESCRICAO: TStringField;
	pnlLayoutLinhasgogrupoproduto6: TPanel;
	pnlLayoutTitulosgogrupoprodutodescricao: TPanel;
	pnlLayoutCamposgogrupoprodutodescricao: TPanel;
	dbegogrupoprodutodescricao: TDBEdit;

	{campo META}
	sqlgogrupoprodutoMETA: TFloatField;
	cdsgogrupoprodutoMETA: TFloatField;
	pnlLayoutLinhasgogrupoproduto7: TPanel;
	pnlLayoutTitulosgogrupoprodutometa: TPanel;
	pnlLayoutCamposgogrupoprodutometa: TPanel;
	dbegogrupoprodutometa: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    pnlLayoutCamposgogrupoprodutoprojeto: TPanel;
    dbegogrupoprodutoPROJETO: TDBEdit;
    pnlLayoutCamposgogrupoprodutocliente: TPanel;
    dbegogrupoprodutoCLIENTE: TDBEdit;
    cdsfiltros: TClientDataSet;
    btnPesquisarprojeto: TSpeedButton;
    btnPesquisarfilial: TSpeedButton;
    psqprojeto: TPesquisa;
    psqfilial: TPesquisa;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargogrupoprodutoClick(Sender: TObject);
	procedure spdAlterargogrupoprodutoClick(Sender: TObject);
	procedure spdGravargogrupoprodutoClick(Sender: TObject);
	procedure spdCancelargogrupoprodutoClick(Sender: TObject);
	procedure spdExcluirgogrupoprodutoClick(Sender: TObject);
	procedure cdsgogrupoprodutoBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgogrupoproduto: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgogrupoproduto: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgogrupoproduto: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgogrupoproduto: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagogrupoproduto: Array[0..0]  of TDuplicidade;
	duplicidadeCampogogrupoproduto: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fgogrupoproduto: Tfgogrupoproduto;

implementation

uses udm, System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfgogrupoproduto.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgogrupoproduto.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgogrupoproduto.TabIndex;
end;

procedure Tfgogrupoproduto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgogrupoproduto.verificarEmTransacao(cdsgogrupoproduto);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgogrupoproduto.configurarGridesFormulario(fgogrupoproduto,true, false);

	fncgogrupoproduto.Free;
	{eliminando container de fun��es da memoria<<<}

	fgogrupoproduto:=nil;
	Action:=cafree;
end;

procedure Tfgogrupoproduto.FormCreate(Sender: TObject);
var arquivoini : Tinifile;
begin
//  sessao;

   {>>>container de fun��es}
  arquivoini      :=Tinifile.Create('C:\goassessoria\parametros.ini');
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;

	{>>>container de fun��es}
	fncgogrupoproduto:= TFuncoes.Create(Self);
	fncgogrupoproduto.definirConexao(dm.conexao);
	fncgogrupoproduto.definirConexaohistorico(dm.conexao);
	fncgogrupoproduto.definirFormulario(Self);
	fncgogrupoproduto.validarTransacaoRelacionamento:=true;
	fncgogrupoproduto.autoAplicarAtualizacoesBancoDados:=true;

	{container de fun��es<<<}


  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncgogrupoproduto.recuperarVersaoExecutavel;
  lblusuario.Caption := lblusuario.Caption + ParamStr(3);

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODPROJETO';
	pesquisasItem.titulo:='C�d.Projeto';
	pesquisasItem.campo:='CODPROJETO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPROJETO';
	pesquisasItem.titulo:='Projeto';
	pesquisasItem.campo:='PROJETO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODCLIENTE';
	pesquisasItem.titulo:='Cod.Cliente';
	pesquisasItem.campo:='CODCLIENTE';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncgogrupoproduto.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncgogrupoproduto.criarPesquisas(sqlgogrupoproduto,cdsgogrupoproduto,'select * from gogrupoproduto', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gogrupoproduto}
	{campos obrigatorios gogrupoproduto<<<}

	{>>>objetos ativos no modo de manipula��o de dados gogrupoproduto}
	objetosModoEdicaoAtivosgogrupoproduto[0]:=spdCancelargogrupoproduto;
	objetosModoEdicaoAtivosgogrupoproduto[1]:=spdGravargogrupoproduto;
  objetosModoEdicaoAtivosgogrupoproduto[2]:=dbegogrupoprodutoCODPROJETO;
  objetosModoEdicaoAtivosgogrupoproduto[3]:=dbegogrupoprodutoCODCLIENTE;
  objetosModoEdicaoAtivosgogrupoproduto[4]:=btnPesquisarprojeto;
  objetosModoEdicaoAtivosgogrupoproduto[5]:=btnPesquisarfilial;
	{objetos ativos no modo de manipula��o de dados gogrupoproduto<<<}

	{>>>objetos inativos no modo de manipula��o de dados gogrupoproduto}
	objetosModoEdicaoInativosgogrupoproduto[0]:=spdAdicionargogrupoproduto;
	objetosModoEdicaoInativosgogrupoproduto[1]:=spdAlterargogrupoproduto;
	objetosModoEdicaoInativosgogrupoproduto[2]:=spdExcluirgogrupoproduto;
	objetosModoEdicaoInativosgogrupoproduto[3]:=spdOpcoes;
	objetosModoEdicaoInativosgogrupoproduto[4]:=tabPesquisas;
  objetosModoEdicaoInativosgogrupoproduto[5]:=dbegogrupoprodutoPROJETO;
  objetosModoEdicaoInativosgogrupoproduto[6]:=dbegogrupoprodutoCLIENTE;
	{objetos inativos no modo de manipula��o de dados gogrupoproduto<<<}

	{>>>comando de adi��o de teclas de atalhos gogrupoproduto}
	fncgogrupoproduto.criaAtalhoPopupMenu(Popupgogrupoproduto,'Adicionar novo registro',VK_F4,spdAdicionargogrupoproduto.OnClick);
	fncgogrupoproduto.criaAtalhoPopupMenu(Popupgogrupoproduto,'Alterar registro selecionado',VK_F5,spdAlterargogrupoproduto.OnClick);
	fncgogrupoproduto.criaAtalhoPopupMenu(Popupgogrupoproduto,'Gravar �ltimas altera��es',VK_F6,spdGravargogrupoproduto.OnClick);
	fncgogrupoproduto.criaAtalhoPopupMenu(Popupgogrupoproduto,'Cancelar �ltimas altera��e',VK_F7,spdCancelargogrupoproduto.OnClick);
	fncgogrupoproduto.criaAtalhoPopupMenu(Popupgogrupoproduto,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgogrupoproduto.OnClick);

	{comando de adi��o de teclas de atalhos gogrupoproduto<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgogrupoproduto.open();
	{abertura dos datasets<<<}

	fncgogrupoproduto.criaAtalhoPopupMenuNavegacao(Popupgogrupoproduto,cdsgogrupoproduto);

	fncgogrupoproduto.definirMascaraCampos(cdsgogrupoproduto);

	fncgogrupoproduto.configurarGridesFormulario(fgogrupoproduto,false, true);

end;

procedure Tfgogrupoproduto.spdAdicionargogrupoprodutoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgogrupoproduto.TabIndex;
	if (fncgogrupoproduto.adicionar(cdsgogrupoproduto)=true) then
		begin
			fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,true);
			dbegogrupoprodutocodprojeto.SetFocus;
	end
	else
		fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,false);
end;

procedure Tfgogrupoproduto.spdAlterargogrupoprodutoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgogrupoproduto.TabIndex;
	if (fncgogrupoproduto.alterar(cdsgogrupoproduto)=true) then
		begin
			fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,true);
			dbegogrupoprodutocodprojeto.SetFocus;
	end
	else
		fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,false);
end;

procedure Tfgogrupoproduto.spdGravargogrupoprodutoClick(Sender: TObject);
begin
	if (fncgogrupoproduto.gravar(cdsgogrupoproduto)=true) then
		fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,false)
	else
		fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,true);
end;

procedure Tfgogrupoproduto.spdCancelargogrupoprodutoClick(Sender: TObject);
begin
	if (fncgogrupoproduto.cancelar(cdsgogrupoproduto)=true) then
		fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,false)
	else
		fncgogrupoproduto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoproduto,objetosModoEdicaoInativosgogrupoproduto,true);
end;

procedure Tfgogrupoproduto.spdExcluirgogrupoprodutoClick(Sender: TObject);
begin
	fncgogrupoproduto.excluir(cdsgogrupoproduto);
end;

procedure Tfgogrupoproduto.cdsgogrupoprodutoBeforePost(DataSet: TDataSet);
begin
	if cdsgogrupoproduto.State in [dsinsert] then
	cdsgogrupoprodutoID.Value:=fncgogrupoproduto.autoNumeracaoGenerator(dm.conexao,'G_gogrupoproduto');
end;

procedure Tfgogrupoproduto.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);
    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;
  except
    on e: exception do
    begin
      application.MessageBox(pchar('Sem conex�o de rede.Motivo' + ''#13''#10'' + e.Message), 'Exce��o.', MB_OK);
      application.Terminate;
    end;
  end;
end;


procedure Tfgogrupoproduto.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgogrupoproduto.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgogrupoproduto.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgogrupoproduto.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgogrupoproduto.IsEmpty then 
	fncgogrupoproduto.visualizarRelatorios(frxgogrupoproduto);
end;

end.

