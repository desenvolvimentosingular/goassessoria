unit ufiltro_avancado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxGDIPlusClasses, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.StdCtrls, uPesquisa, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, udm, Vcl.DialogMessage, uFuncoes,
  frxClass, frxDBSet, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,cxGroupBox, cxRadioGroup, Vcl.Menus, cxButtons, Datasnap.DBClient,
  Datasnap.Provider, Dateutils, frxChart, Data.FMTBcd, Data.SqlExpr,
  udm_relatorios;

type


    Tpesquisas_relatorios = Class helper for Tdm_relatorios
     public
     procedure analisegeral;
     procedure analiseportadoracumulado;
     procedure analiseporprojeto;
     procedure analiseporfilial;

     procedure Crontrole_recebivel_I;
     procedure Crontrole_recebivel_II;
     procedure Crontrole_recebivel_III;
     procedure Crontrole_recebivel_IV;
     procedure Crontrole_recebivel_V;

     procedure painel_disponibilidade_I;
     procedure painel_disponibilidade_II;
     procedure painel_disponibilidade_III;
     procedure painel_disponibilidade_IV;
     procedure painel_disponibilidade_V;


     procedure faturamento_I;
     procedure faturamento_II;
     procedure faturamento_III;
     procedure faturamento_IV;
     procedure faturamento_V;


     procedure painel_risco_I;
     procedure painel_risco_II;
     procedure painel_risco_III;
     procedure painel_risco_IV;
     procedure painel_risco_V;

     procedure financeiro_I;
     procedure financeiro_II;
     procedure financeiro_III;
     procedure financeiro_IV;
     procedure financeiro_V;


 End;
    Tffiltro_avancado = class(TForm)
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    imglogo: TImage;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    pnlLinhaBotoessecao: TPanel;
    spdenviar: TButton;
    spdfechar: TButton;
    pnlDadosfiltros: TPanel;
    pnlLayoutLinhafiltro1: TPanel;
    lblprojeto: TLabel;
    btnPesquisarprojeto: TSpeedButton;
    dbecodprojeto: TEdit;
    dbeprojeto: TEdit;
    pnlLayoutLinhafiltro2: TPanel;
    lblfilial: TLabel;
    btnPesquisarfilial: TSpeedButton;
    dbeID_FILIAL: TEdit;
    dbeFILIAL: TEdit;
    pnlLayoutLinhafiltro3: TPanel;
    lblPORTADOR: TLabel;
    btnPesquisarFUNDO: TSpeedButton;
    dbeID_FUNDO: TEdit;
    dbeFUNDO: TEdit;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    pnlLayoutLinhafiltro4: TPanel;
    lblDTFATURAMENTO: TLabel;
    data1: TcxDateEdit;
    data2: TcxDateEdit;
    psqfundo: TPesquisa;
    psqfilial: TPesquisa;
    psqprojeto: TPesquisa;
    spbdatesintetico: TcxButton;
    popmenudata: TPopupMenu;
    Hoje: TMenuItem;
    Ontem: TMenuItem;
    SemanaAtual: TMenuItem;
    SemanaAnterior: TMenuItem;
    UltimosQuizeDias: TMenuItem;
    N1: TMenuItem;
    MesAtual: TMenuItem;
    MesAnterior: TMenuItem;
    N2: TMenuItem;
    TrimestreAtual: TMenuItem;
    TrimestreAnterior: TMenuItem;
    N3: TMenuItem;
    SemestreAtual: TMenuItem;
    SemestreAnterior: TMenuItem;
    N4: TMenuItem;
    AnoAtual: TMenuItem;
    AnoAnterior: TMenuItem;
    N5: TMenuItem;
    LimparDatas: TMenuItem;
    chbrelatorio1: TCheckBox;
    chbrelatorio2: TCheckBox;
    chbrelatorio3: TCheckBox;
    chbrelatorio4: TCheckBox;
    chbrelatorio5: TCheckBox;
    chbrelatorio6: TCheckBox;
    chbrelatorio7: TCheckBox;
    chbrelatorio8: TCheckBox;
    chbrelatorio9: TCheckBox;
    chbrelatorio10: TCheckBox;
    chbrelatorio11: TCheckBox;
    chbrelatorio12: TCheckBox;
    chbrelatorio13: TCheckBox;
    chbrelatorio14: TCheckBox;
    chbrelatorio15: TCheckBox;
    chbrelatorio16: TCheckBox;
    chbrelatorio17: TCheckBox;
    chbrelatorio18: TCheckBox;
    chbrelatorio19: TCheckBox;
    chbrelatorio20: TCheckBox;
    chbrelatorio21: TCheckBox;
    chbrelatorio22: TCheckBox;
    chbrelatorio23: TCheckBox;
    chbrelatorio24: TCheckBox;
    chbrelatorio25: TCheckBox;
    chbrelatorio26: TCheckBox;
    procedure spdfecharClick(Sender: TObject);
    procedure dbecodprojetoKeyPress(Sender: TObject; var Key: Char);
    procedure dbeID_FUNDOKeyPress(Sender: TObject; var Key: Char);
    procedure dbeID_FILIALKeyPress(Sender: TObject; var Key: Char);
    procedure spdenviarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spbdatesinteticoClick(Sender: TObject);
    procedure HojeClick(Sender: TObject);
    procedure OntemClick(Sender: TObject);
    procedure SemanaAtualClick(Sender: TObject);
    procedure SemanaAnteriorClick(Sender: TObject);
    procedure UltimosQuizeDiasClick(Sender: TObject);
    procedure MesAtualClick(Sender: TObject);
    procedure MesAnteriorClick(Sender: TObject);
    procedure TrimestreAtualClick(Sender: TObject);
    procedure SemestreAtualClick(Sender: TObject);
    procedure SemestreAnteriorClick(Sender: TObject);
    procedure AnoAtualClick(Sender: TObject);
    procedure AnoAnteriorClick(Sender: TObject);
    procedure LimparDatasClick(Sender: TObject);
    procedure chbrelatorio26Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
  fncfiltros: TFuncoes;
  end;

var
  ffiltro_avancado: Tffiltro_avancado;

implementation

{$R *.dfm}

uses ugorelatorios_automaticos;

procedure Tpesquisas_relatorios.analisegeral;
  var consulta, group_by, where : string;
begin
    FDAnalisePortador.Close;
    FDAnalisePortador.SQL.Clear;

     consulta :=
    'SELECT c.codportador                                                                                                                                                           '+
    '    ,c.portador                                                                                                                                                                '+
    '    ,count(*) total_bordero                                                                                                                                                    '+
    '    ,sum(c.qttitulos) qttitulos                                                                                                                                                '+
    '    ,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS,0)) VLMEDIORECEBIVEIS                                                                                          '+
    '    ,SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero) PM_EMPRESA                                                                                       '+
    '    ,sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                      '+
    '    ,sum((c.vlbrutofinalbordero) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado                    '+
    '    ,SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                                 '+
    '    ,((sum(C.ADVALOREM + C.DESAGIOACORDADO)/ sum(c.vlbrutofinalbordero))/(sum(multiplicadorpmfloat)/sum(c.vlbrutofinalbordero))) *30 taxa_nominal                              '+
    '    ,(((sum(c.vlbrutofinalbordero)/(sum(c.vlbrutofinalbordero)-sum(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)))-1)/   '+
    '    (sum(c.vlbrutofinalbordero * c.prazomedio)/sum(c.vlbrutofinalbordero)))*30 custoefetivo                                                                                    '+
    '  FROM gocontrolerecebivel c                                                                                                                                                   '+
    'WHERE (coalesce(c.qttitulos, 0) - coalesce(c.qtdrecusada, 0)) > 0                                                                                                              ';
     group_by:='GROUP BY 1,2';

     FDAnalisePortador.SQL.Add(consulta);
     FDAnalisePortador.SQL.Add(' and DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2');
     FDAnalisePortador.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
     FDAnalisePortador.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);

     if ffiltro_avancado.dbeID_FUNDO.Text<>EmptyStr then
     begin
       FDAnalisePortador.SQL.Add(' and CODPORTADOR=:CODPORTADOR');
       FDAnalisePortador.ParamByName('CODPORTADOR').asInteger := strtoInt(ffiltro_avancado.dbeID_FUNDO.Text);
     end;
     FDAnalisePortador.SQL.Add(group_by);
     FDAnalisePortador.Open();

     if  not FDAnalisePortador.IsEmpty then
     begin

     end;


end;

procedure Tpesquisas_relatorios.analiseporfilial;
  var consulta, group_by, where : string;
begin

end;

procedure Tpesquisas_relatorios.analiseporprojeto;
  var consulta, group_by, where : string;
begin


end;

procedure Tpesquisas_relatorios.analiseportadoracumulado;
  var consulta, group_by, where : string;
begin


end;

procedure Tffiltro_avancado.AnoAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncfiltros.anoanterior (Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.AnoAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.anoantual (Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.chbrelatorio26Click(Sender: TObject);
var i : integer;
begin
   if chbrelatorio26.Checked then
   begin
       for I := 0 to ffiltro_avancado.ComponentCount-1 do
        begin
           if (ffiltro_avancado.Components[i] is TCheckBox)  then
           begin
             if (ffiltro_avancado.Components[i] as TCheckBox).Checked= false then
             if (ffiltro_avancado.Components[i] as TCheckBox).Name<>'chbrelatorio26' then
              (ffiltro_avancado.Components[i] as TCheckBox).Checked := true;
           end;
        end;
   end;


   if chbrelatorio26.Checked=false then
   begin
       for I := 0 to ffiltro_avancado.ComponentCount-1 do
        begin
           if (ffiltro_avancado.Components[i] is TCheckBox)  then
           begin
             if (ffiltro_avancado.Components[i] as TCheckBox).Checked= true then
             if (ffiltro_avancado.Components[i] as TCheckBox).Name<>'chbrelatorio26' then
              (ffiltro_avancado.Components[i] as TCheckBox).Checked:= false;

           end;

        end;
   end;

end;

procedure Tpesquisas_relatorios.Crontrole_recebivel_I;
begin

end;

procedure Tpesquisas_relatorios.Crontrole_recebivel_II;
begin
 //
end;

procedure Tpesquisas_relatorios.Crontrole_recebivel_III;
begin
  //
end;

procedure Tpesquisas_relatorios.Crontrole_recebivel_IV;
begin
//
end;

procedure Tpesquisas_relatorios.Crontrole_recebivel_V;
begin

end;

procedure Tffiltro_avancado.dbecodprojetoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeprojeto.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.dbeID_FUNDOKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeID_FUNDO.Text  :=EmptyStr;
  end;
end;

procedure Tpesquisas_relatorios.faturamento_I;
begin
  //
end;

procedure Tpesquisas_relatorios.faturamento_II;
begin
  //
end;

procedure Tpesquisas_relatorios.faturamento_III;
begin
 //
end;

procedure Tpesquisas_relatorios.faturamento_IV;
begin
 //
end;

procedure Tpesquisas_relatorios.faturamento_V;
begin
 //
end;

procedure Tpesquisas_relatorios.financeiro_I;
begin
  //
end;

procedure Tpesquisas_relatorios.financeiro_II;
begin

end;

procedure Tpesquisas_relatorios.financeiro_III;
begin
 //
end;

procedure Tpesquisas_relatorios.financeiro_IV;
begin
 //
end;

procedure Tpesquisas_relatorios.financeiro_V;
begin
 //
end;

procedure Tffiltro_avancado.dbeID_FILIALKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;
  if key = #8 then
  begin
   dbeFILIAL.Text  :=EmptyStr;
  end;

end;

procedure Tffiltro_avancado.SemanaAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncfiltros.SemanaAnterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemanaAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.SemanaAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemestreAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.semestreanterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.semestreatual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  fncfiltros.configurarGridesFormulario(ffiltro_avancado, true, false);
	{>>>eliminando container de fun��es da memoria}
	fncfiltros.Free;
	{eliminando container de fun��es da memoria<<<}

  ffiltro_avancado :=nil;
 	Action           :=cafree;
end;

procedure Tffiltro_avancado.FormCreate(Sender: TObject);
begin
  fncfiltros:= TFuncoes.Create(Self);
	fncfiltros.definirConexao(dm.conexao);
  fncfiltros.definirConexao(dm.conexao);
	fncfiltros.definirFormulario(Self);
	fncfiltros.validarTransacaoRelacionamento:=true;
	fncfiltros.autoAplicarAtualizacoesBancoDados:=true;

end;

procedure Tffiltro_avancado.FormShow(Sender: TObject);
begin
 data1.Date:= StartofTheMonth(Date);
 data2.Date:= EndOfTheMonth(Date);
end;

procedure Tffiltro_avancado.HojeClick(Sender: TObject);
begin
 try
  fncfiltros.DiaAtual(Date);
  data1.Date  := fncfiltros.DiaAtual(Date);
  data2.Date  := fncfiltros.DiaAtual(Date);
 finally
 end;
end;

procedure Tffiltro_avancado.LimparDatasClick(Sender: TObject);
begin
  data1.Clear;
  data2.Clear;
end;

procedure Tffiltro_avancado.MesAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.MesAnterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.MesAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.MesAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.OntemClick(Sender: TObject);
begin
 try
    Data1.Date  :=fncfiltros.DiaAnterior(Date);
    Data2.Date  :=fncfiltros.DiaAnterior(Date);
   finally
   end;
end;

procedure Tpesquisas_relatorios.painel_disponibilidade_I;
begin
  //
end;

procedure Tpesquisas_relatorios.painel_disponibilidade_II;
begin
 //
end;

procedure Tpesquisas_relatorios.painel_disponibilidade_III;
begin
 //
end;

procedure Tpesquisas_relatorios.painel_disponibilidade_IV;
begin
 //
end;

procedure Tpesquisas_relatorios.painel_disponibilidade_V;
begin
  //
end;

procedure Tpesquisas_relatorios.painel_risco_I;
begin
 //
end;

procedure Tpesquisas_relatorios.painel_risco_II;
begin
  //
end;

procedure Tpesquisas_relatorios.painel_risco_III;
begin
 //
end;

procedure Tpesquisas_relatorios.painel_risco_IV;
begin
  //
end;

procedure Tpesquisas_relatorios.painel_risco_V;
begin
 //
end;

procedure Tffiltro_avancado.spbdatesinteticoClick(Sender: TObject);
begin
 with TcxButton(Sender).ClientToScreen(point(TcxButton(Sender).Width,
    TcxButton(Sender).Height)) do
  begin
    popmenudata.Popup(X, Y);
  end;
end;

procedure Tffiltro_avancado.spdfecharClick(Sender: TObject);
begin
 ModalResult:= mrcancel;
end;

procedure Tffiltro_avancado.spdenviarClick(Sender: TObject);
 var nome_pasta :string;
begin
  if ((data1.Text= EmptyStr) or (data2.Text=EmptyStr)) then
  abort;

  nome_pasta :=inttostr(dayof (Date))+'_'+inttostr(monthof(Date))+'_'+ inttostr(yearof(Date));

  if not DirectoryExists(fgorelatorios_automaticos.cdsgorelatorios_automaticosCAMINHO_PASTA.AsString+nome_pasta) then
    CreateDir(Pchar(fgorelatorios_automaticos.cdsgorelatorios_automaticosCAMINHO_PASTA.AsString+nome_pasta));

  dm_relatorios.frxExportPDF.ShowDialog:=false;
  dm_relatorios.frxExportPDF.DefaultPath:= Pchar(fgorelatorios_automaticos.cdsgorelatorios_automaticosCAMINHO_PASTA.AsString+nome_pasta);

  TDialogMessage.ShowWaitMessage('Carregando dados...',
  procedure
  begin
   if chbrelatorio1.Checked then

  end);


end;

procedure Tffiltro_avancado.TrimestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.trimestreatual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.UltimosQuizeDiasClick(Sender: TObject);
var data : TStringList;
begin

   try
    data := TStringList.Create;
    data:=fncfiltros.UltimosQuinzeDias(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;

end;

end.
