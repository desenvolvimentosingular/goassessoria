CREATE TABLE GODISPONIBILIDADE(
ID Integer   
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time   
,
DTCOMPETENCIA Date  not null 
,
VLFOMENTOMP Double Precision   
,
VLMOVIMENTODIA Double Precision   
,
VLCAIXAGERAL Double Precision   
,
VLBANCOCONTACORRENTE Double Precision   
,
VLCHEQUES Double Precision   
,
VLDUPLICATAS Double Precision   
,
VLAVISTA Double Precision   
,
VLCARTEIRASIMPLES Double Precision   
,
VLESTMPCOMPRA Double Precision   
,
VLESTMPCOMPRACURVAA Double Precision   
,
VLESTMPCOMPRACURVAB Double Precision   
,
VLESTMPCOMPRACURVAC Double Precision   
,
VLESTMPSEMGIRO Double Precision   
,
VLESTEMCURVAA Double Precision   
,
VLESTEMCURVAB Double Precision   
,
VLESTEMCURVAC Double Precision   
,
VLESTEMSEMGIRO Double Precision   
,
VLESTPACURVAA Double Precision   
,
VLESTPACURVAB Double Precision   
,
VLESTPAFORALINHA Double Precision   
,
VLESTPACURVANICHO Double Precision   
,
VLPAESTLOWPRICE Double Precision   
,
VLESTPATERCEIRO Double Precision   
,
VLLANCAMENTO_PROJETO Double Precision   
,
VLESTTRANSITO Double Precision   
,
VLCONTAPAGARVENCIDO Double Precision   
,
VLIMPOSTOSVENCIDOS Double Precision   
,
VLCONTASPAGAR30 Double Precision   
,
VLCONTASPAGAR60 Double Precision   
,
VLATIVOS Double Precision   
);
commit;
CREATE ASC INDEX I01_GODI_IDADE_ID ON GODISPONIBILIDADE (ID);
CREATE ASC INDEX I02_GODI_IDADE_DT_CADASTRO ON GODISPONIBILIDADE (DT_CADASTRO);
CREATE ASC INDEX I03_GODI_IDADE_HS_CADASTRO ON GODISPONIBILIDADE (HS_CADASTRO);
CREATE ASC INDEX I04_GODI_IDADE_DTCOMPETENCIA ON GODISPONIBILIDADE (DTCOMPETENCIA);
CREATE ASC INDEX I05_GODI_IDADE_VLFOMENTOMP ON GODISPONIBILIDADE (VLFOMENTOMP);
CREATE ASC INDEX I06_GODI_IDADE_VLMOVIMENTODIA ON GODISPONIBILIDADE (VLMOVIMENTODIA);
CREATE ASC INDEX I07_GODI_IDADE_VLCAIXAGERAL ON GODISPONIBILIDADE (VLCAIXAGERAL);
CREATE ASC INDEX I08_GODI_IDADE_VLBANCOCONTACORRE ON GODISPONIBILIDADE (VLBANCOCONTACORRENTE);
CREATE ASC INDEX I09_GODI_IDADE_VLCHEQUES ON GODISPONIBILIDADE (VLCHEQUES);
CREATE ASC INDEX I10_GODI_IDADE_VLDUPLICATAS ON GODISPONIBILIDADE (VLDUPLICATAS);
CREATE ASC INDEX I11_GODI_IDADE_VLAVISTA ON GODISPONIBILIDADE (VLAVISTA);
CREATE ASC INDEX I12_GODI_IDADE_VLCARTEIRASIMPLES ON GODISPONIBILIDADE (VLCARTEIRASIMPLES);
CREATE ASC INDEX I13_GODI_IDADE_VLESTMPCOMPRA ON GODISPONIBILIDADE (VLESTMPCOMPRA);
CREATE ASC INDEX I14_GODI_IDADE_VLESTMPCOMPRACURV ON GODISPONIBILIDADE (VLESTMPCOMPRACURVAA);
CREATE ASC INDEX I15_GODI_IDADE_VLESTMPCOMPRACURV ON GODISPONIBILIDADE (VLESTMPCOMPRACURVAB);
CREATE ASC INDEX I16_GODI_IDADE_VLESTMPCOMPRACURV ON GODISPONIBILIDADE (VLESTMPCOMPRACURVAC);
CREATE ASC INDEX I17_GODI_IDADE_VLESTMPSEMGIRO ON GODISPONIBILIDADE (VLESTMPSEMGIRO);
CREATE ASC INDEX I18_GODI_IDADE_VLESTEMCURVAA ON GODISPONIBILIDADE (VLESTEMCURVAA);
CREATE ASC INDEX I19_GODI_IDADE_VLESTEMCURVAB ON GODISPONIBILIDADE (VLESTEMCURVAB);
CREATE ASC INDEX I20_GODI_IDADE_VLESTEMCURVAC ON GODISPONIBILIDADE (VLESTEMCURVAC);
CREATE ASC INDEX I21_GODI_IDADE_VLESTEMSEMGIRO ON GODISPONIBILIDADE (VLESTEMSEMGIRO);
CREATE ASC INDEX I22_GODI_IDADE_VLESTPACURVAA ON GODISPONIBILIDADE (VLESTPACURVAA);
CREATE ASC INDEX I23_GODI_IDADE_VLESTPACURVAB ON GODISPONIBILIDADE (VLESTPACURVAB);
CREATE ASC INDEX I24_GODI_IDADE_VLESTPAFORALINHA ON GODISPONIBILIDADE (VLESTPAFORALINHA);
CREATE ASC INDEX I25_GODI_IDADE_VLESTPACURVANICHO ON GODISPONIBILIDADE (VLESTPACURVANICHO);
CREATE ASC INDEX I26_GODI_IDADE_VLPAESTLOWPRICE ON GODISPONIBILIDADE (VLPAESTLOWPRICE);
CREATE ASC INDEX I27_GODI_IDADE_VLESTPATERCEIRO ON GODISPONIBILIDADE (VLESTPATERCEIRO);
CREATE ASC INDEX I28_GODI_IDADE_VLLANCAMENTO_PROJ ON GODISPONIBILIDADE (VLLANCAMENTO_PROJETO);
CREATE ASC INDEX I29_GODI_IDADE_VLESTTRANSITO ON GODISPONIBILIDADE (VLESTTRANSITO);
CREATE ASC INDEX I30_GODI_IDADE_VLCONTAPAGARVENCI ON GODISPONIBILIDADE (VLCONTAPAGARVENCIDO);
CREATE ASC INDEX I31_GODI_IDADE_VLIMPOSTOSVENCIDO ON GODISPONIBILIDADE (VLIMPOSTOSVENCIDOS);
CREATE ASC INDEX I32_GODI_IDADE_VLCONTASPAGAR30 ON GODISPONIBILIDADE (VLCONTASPAGAR30);
CREATE ASC INDEX I33_GODI_IDADE_VLCONTASPAGAR60 ON GODISPONIBILIDADE (VLCONTASPAGAR60);
CREATE ASC INDEX I34_GODI_IDADE_VLATIVOS ON GODISPONIBILIDADE (VLATIVOS);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Data Compet�ncia' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'DTCOMPETENCIA';
update rdb$relation_fields set rdb$description = 'Fomento Mat�ria Prima' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLFOMENTOMP';
update rdb$relation_fields set rdb$description = 'Movimento Dia' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLMOVIMENTODIA';
update rdb$relation_fields set rdb$description = 'Valor Caixa geral' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLCAIXAGERAL';
update rdb$relation_fields set rdb$description = 'Banco conta corrente' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLBANCOCONTACORRENTE';
update rdb$relation_fields set rdb$description = 'Valor Cheque' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLCHEQUES';
update rdb$relation_fields set rdb$description = 'Valor Duplicatas' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLDUPLICATAS';
update rdb$relation_fields set rdb$description = 'Valor � Vista' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLAVISTA';
update rdb$relation_fields set rdb$description = 'Carteira Simples Especial+Dupl. N�o Desc' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLCARTEIRASIMPLES';
update rdb$relation_fields set rdb$description = 'Valor Estoque MP(Pre�o Compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTMPCOMPRA';
update rdb$relation_fields set rdb$description = 'Estoque MP Curva A (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTMPCOMPRACURVAA';
update rdb$relation_fields set rdb$description = 'Estoque MP Curva B (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTMPCOMPRACURVAB';
update rdb$relation_fields set rdb$description = 'Estoque MP Curva C (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTMPCOMPRACURVAC';
update rdb$relation_fields set rdb$description = 'Estoque MP Sem Giro (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTMPSEMGIRO';
update rdb$relation_fields set rdb$description = 'Estoque EM Curva A (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTEMCURVAA';
update rdb$relation_fields set rdb$description = 'Estoque EM Curva B (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTEMCURVAB';
update rdb$relation_fields set rdb$description = 'Estoque EM Curva C (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTEMCURVAC';
update rdb$relation_fields set rdb$description = 'Estoque EM sem giro (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTEMSEMGIRO';
update rdb$relation_fields set rdb$description = 'Estoque PA Curva A (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTPACURVAA';
update rdb$relation_fields set rdb$description = 'Estoque PA Curva B (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTPACURVAB';
update rdb$relation_fields set rdb$description = 'Estoque PA Fora Linha  (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTPAFORALINHA';
update rdb$relation_fields set rdb$description = 'Estoque PA Curva Nicha (Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTPACURVANICHO';
update rdb$relation_fields set rdb$description = 'Estoque PA Low Price(Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLPAESTLOWPRICE';
update rdb$relation_fields set rdb$description = 'Estoque PA Terceiro(Pre�o de compra)' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTPATERCEIRO';
update rdb$relation_fields set rdb$description = 'Valor Projeto Lan�amento' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLLANCAMENTO_PROJETO';
update rdb$relation_fields set rdb$description = 'Valor Estoque Tr�nsito' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLESTTRANSITO';
update rdb$relation_fields set rdb$description = 'Valor Contas a Pagar Vencida' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLCONTAPAGARVENCIDO';
update rdb$relation_fields set rdb$description = 'Valor Impostos Vencidos' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLIMPOSTOSVENCIDOS';
update rdb$relation_fields set rdb$description = 'Valor Contas a Pagar 30 dias' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLCONTASPAGAR30';
update rdb$relation_fields set rdb$description = 'Valor Contas a Pagar 31 a 60 dias' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLCONTASPAGAR60';
update rdb$relation_fields set rdb$description = 'Valor Ativos' where rdb$relation_name = 'GODISPONIBILIDADE' and rdb$field_name = 'VLATIVOS';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPROJETO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPROJETO_GEN FOR GOGRUPOPROJETO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPROJETO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPAINELRISCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPAINELRISCO_GEN FOR GOPAINELRISCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPAINELRISCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOS;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOS_GEN FOR GOPROJETOS ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOS, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOSUPERVISOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOSUPERVISOR_GEN FOR GOSUPERVISOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOSUPERVISOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOTIPOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOTIPOFUNDO_GEN FOR GOTIPOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOTIPOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
