unit ugodisponibilidade;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 23/09/2021*}                                          
{*Hora 20:13:14*}                                            
{*Unit godisponibilidade *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa, DateUtils,
  cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit,
  cxCurrencyEdit, cxCheckBox;

type                                                                                        
	Tfgodisponibilidade = class(TForm)
    sqlgodisponibilidade: TSQLDataSet;
	dspgodisponibilidade: TDataSetProvider;
	cdsgodisponibilidade: TClientDataSet;
	dsgodisponibilidade: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
    frxgodisponibilidadeI: TfrxReport;
	frxDBgodisponibilidade: TfrxDBDataset;
	Popupgodisponibilidade: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	tabgodisponibilidade: tcxTabSheet;
	SCRgodisponibilidade: TScrollBox;
	{>>>bot�es de manipula��o de dados godisponibilidade}
	pnlLinhaBotoesgodisponibilidade: TPanel;
	spdAdicionargodisponibilidade: TButton;
	spdAlterargodisponibilidade: TButton;
	spdGravargodisponibilidade: TButton;
	spdCancelargodisponibilidade: TButton;
	spdExcluirgodisponibilidade: TButton;
	cdsgodisponibilidadeID: TIntegerField;
	cdsgodisponibilidadeDT_CADASTRO: TDateField;
	cdsgodisponibilidadeHS_CADASTRO: TTimeField;
	cdsgodisponibilidadeDTCOMPETENCIA: TDateField;
	cdsgodisponibilidadeVLFOMENTOMP: TFloatField;
	cdsgodisponibilidadeVLMOVIMENTODIA: TFloatField;
	cdsgodisponibilidadeVLCAIXAGERAL: TFloatField;
	cdsgodisponibilidadeVLBANCOCONTACORRENTE: TFloatField;
	cdsgodisponibilidadeVLCHEQUES: TFloatField;
	cdsgodisponibilidadeVLDUPLICATAS: TFloatField;
	cdsgodisponibilidadeVLAVISTA: TFloatField;
	cdsgodisponibilidadeVLCARTEIRASIMPLES: TFloatField;
	cdsgodisponibilidadeVLESTMPCOMPRA: TFloatField;
	cdsgodisponibilidadeVLESTMPCOMPRACURVAA: TFloatField;
	cdsgodisponibilidadeVLESTMPCOMPRACURVAB: TFloatField;
	cdsgodisponibilidadeVLESTMPCOMPRACURVAC: TFloatField;
	cdsgodisponibilidadeVLESTMPSEMGIRO: TFloatField;
	cdsgodisponibilidadeVLESTEMCURVAA: TFloatField;
	cdsgodisponibilidadeVLESTEMCURVAB: TFloatField;
	cdsgodisponibilidadeVLESTEMCURVAC: TFloatField;
	cdsgodisponibilidadeVLESTEMSEMGIRO: TFloatField;
	cdsgodisponibilidadeVLESTPACURVAA: TFloatField;
	cdsgodisponibilidadeVLESTPACURVAB: TFloatField;
	cdsgodisponibilidadeVLESTPAFORALINHA: TFloatField;
	cdsgodisponibilidadeVLESTPACURVANICHO: TFloatField;
	cdsgodisponibilidadeVLPAESTLOWPRICE: TFloatField;
	cdsgodisponibilidadeVLESTPATERCEIRO: TFloatField;
	cdsgodisponibilidadeVLLANCAMENTO_PROJETO: TFloatField;
	cdsgodisponibilidadeVLESTTRANSITO: TFloatField;
	cdsgodisponibilidadeVLCONTAPAGARVENCIDO: TFloatField;
	cdsgodisponibilidadeVLIMPOSTOSVENCIDOS: TFloatField;
	cdsgodisponibilidadeVLCONTASPAGAR30: TFloatField;
	cdsgodisponibilidadeVLCONTASPAGAR60: TFloatField;
	cdsgodisponibilidadeVLATIVOS: TFloatField;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    imglogo: TImage;
    cdsfiltros: TClientDataSet;
    cdsgodisponibilidadeCODPROJETO: TIntegerField;
    cdsgodisponibilidadePROJETO: TStringField;
    cdsgodisponibilidadeCODFILIAL: TIntegerField;
    cdsgodisponibilidadeFILIAL: TStringField;
    cgcInterno: TcxPageControl;
    tabPrincipal: TcxTabSheet;
    pnlDadosgocontrolerecebivel: TPanel;
    pnlLayoutLinhasgodisponibilidade1: TPanel;
    pnlLayoutTitulosgodisponibilidadeid: TPanel;
    pnlLayoutCamposgodisponibilidadeid: TPanel;
    dbegodisponibilidadeID: TDBEdit;
    pnlLayoutLinhasgodisponibilidade2: TPanel;
    pnlLayoutTitulosgodisponibilidadedt_cadastro: TPanel;
    pnlLayoutCamposgodisponibilidadedt_cadastro: TPanel;
    dbegodisponibilidadeDT_CADASTRO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade3: TPanel;
    pnlLayoutTitulosgodisponibilidadehs_cadastro: TPanel;
    pnlLayoutCamposgodisponibilidadehs_cadastro: TPanel;
    dbegodisponibilidadeHS_CADASTRO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade4: TPanel;
    pnlLayoutTitulosgodisponibilidadedtcompetencia: TPanel;
    pnlLayoutCamposgodisponibilidadedtcompetencia: TPanel;
    pnlLayoutLinhasgocontrolerecebivel6: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodfilial: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodfilial: TPanel;
    dbegodisponibilidadeCODFILIAL: TDBEdit;
    btnfilial: TButton;
    pnlLayoutCamposgocontrolerecebivelfilial: TPanel;
    dbegodisponibilidadeFILIAL: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel4: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodportador: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodportador: TPanel;
    dbegodisponibilidadeCODPROJETO: TDBEdit;
    btnprojeto: TButton;
    pnlLayoutCamposgocontrolerecebivelportador: TPanel;
    dbegodisponibilidadePROJETO: TDBEdit;
    tabDividasCurtoPrazo: TcxTabSheet;
    pnlLayoutLinhasgodisponibilidade5: TPanel;
    pnlLayoutTitulosgodisponibilidadevlfomentomp: TPanel;
    pnlLayoutCamposgodisponibilidadevlfomentomp: TPanel;
    dbegodisponibilidadeVLFOMENTOMP: TDBEdit;
    pnlLayoutLinhasgodisponibilidade6: TPanel;
    pnlLayoutTitulosgodisponibilidadevlmovimentodia: TPanel;
    pnlLayoutCamposgodisponibilidadevlmovimentodia: TPanel;
    dbegodisponibilidadeVLMOVIMENTODIA: TDBEdit;
    tabCreditosCurtoPrazo: TcxTabSheet;
    pnlLayoutLinhasgodisponibilidade7: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcaixageral: TPanel;
    pnlLayoutCamposgodisponibilidadevlcaixageral: TPanel;
    dbegodisponibilidadeVLCAIXAGERAL: TDBEdit;
    pnlLayoutLinhasgodisponibilidade8: TPanel;
    pnlLayoutTitulosgodisponibilidadevlbancocontacorrente: TPanel;
    pnlLayoutCamposgodisponibilidadevlbancocontacorrente: TPanel;
    dbegodisponibilidadeVLBANCOCONTACORRENTE: TDBEdit;
    pnlLayoutLinhasgodisponibilidade9: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcheques: TPanel;
    pnlLayoutCamposgodisponibilidadevlcheques: TPanel;
    dbegodisponibilidadeVLCHEQUES: TDBEdit;
    pnlLayoutLinhasgodisponibilidade10: TPanel;
    pnlLayoutTitulosgodisponibilidadevlduplicatas: TPanel;
    pnlLayoutCamposgodisponibilidadevlduplicatas: TPanel;
    dbegodisponibilidadeVLDUPLICATAS: TDBEdit;
    pnlLayoutLinhasgodisponibilidade11: TPanel;
    pnlLayoutTitulosgodisponibilidadevlavista: TPanel;
    pnlLayoutCamposgodisponibilidadevlavista: TPanel;
    dbegodisponibilidadeVLAVISTA: TDBEdit;
    pnlLayoutLinhasgodisponibilidade12: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcarteirasimples: TPanel;
    pnlLayoutCamposgodisponibilidadevlcarteirasimples: TPanel;
    dbegodisponibilidadeVLCARTEIRASIMPLES: TDBEdit;
    tabEstoques: TcxTabSheet;
    pnlLayoutLinhasgodisponibilidade13: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestmpcompra: TPanel;
    pnlLayoutCamposgodisponibilidadevlestmpcompra: TPanel;
    dbegodisponibilidadeVLESTMPCOMPRA: TDBEdit;
    pnlLayoutLinhasgodisponibilidade14: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestmpcompracurvaa: TPanel;
    pnlLayoutCamposgodisponibilidadevlestmpcompracurvaa: TPanel;
    dbegodisponibilidadeVLESTMPCOMPRACURVAA: TDBEdit;
    pnlLayoutLinhasgodisponibilidade15: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestmpcompracurvab: TPanel;
    pnlLayoutCamposgodisponibilidadevlestmpcompracurvab: TPanel;
    dbegodisponibilidadeVLESTMPCOMPRACURVAB: TDBEdit;
    pnlLayoutLinhasgodisponibilidade16: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestmpcompracurvac: TPanel;
    pnlLayoutCamposgodisponibilidadevlestmpcompracurvac: TPanel;
    dbegodisponibilidadeVLESTMPCOMPRACURVAC: TDBEdit;
    pnlLayoutLinhasgodisponibilidade17: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestmpsemgiro: TPanel;
    pnlLayoutCamposgodisponibilidadevlestmpsemgiro: TPanel;
    dbegodisponibilidadeVLESTMPSEMGIRO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade18: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestemcurvaa: TPanel;
    pnlLayoutCamposgodisponibilidadevlestemcurvaa: TPanel;
    dbegodisponibilidadeVLESTEMCURVAA: TDBEdit;
    pnlLayoutLinhasgodisponibilidade19: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestemcurvab: TPanel;
    pnlLayoutCamposgodisponibilidadevlestemcurvab: TPanel;
    dbegodisponibilidadeVLESTEMCURVAB: TDBEdit;
    pnlLayoutLinhasgodisponibilidade20: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestemcurvac: TPanel;
    pnlLayoutCamposgodisponibilidadevlestemcurvac: TPanel;
    dbegodisponibilidadeVLESTEMCURVAC: TDBEdit;
    pnlLayoutLinhasgodisponibilidade21: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestemsemgiro: TPanel;
    pnlLayoutCamposgodisponibilidadevlestemsemgiro: TPanel;
    dbegodisponibilidadeVLESTEMSEMGIRO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade22: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestpacurvaa: TPanel;
    pnlLayoutCamposgodisponibilidadevlestpacurvaa: TPanel;
    dbegodisponibilidadeVLESTPACURVAA: TDBEdit;
    pnlLayoutLinhasgodisponibilidade23: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestpacurvab: TPanel;
    pnlLayoutCamposgodisponibilidadevlestpacurvab: TPanel;
    dbegodisponibilidadeVLESTPACURVAB: TDBEdit;
    pnlLayoutLinhasgodisponibilidade24: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestpaforalinha: TPanel;
    pnlLayoutCamposgodisponibilidadevlestpaforalinha: TPanel;
    dbegodisponibilidadeVLESTPAFORALINHA: TDBEdit;
    pnlLayoutLinhasgodisponibilidade25: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestpacurvanicho: TPanel;
    pnlLayoutCamposgodisponibilidadevlestpacurvanicho: TPanel;
    dbegodisponibilidadeVLESTPACURVANICHO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade26: TPanel;
    pnlLayoutTitulosgodisponibilidadevlpaestlowprice: TPanel;
    pnlLayoutCamposgodisponibilidadevlpaestlowprice: TPanel;
    dbegodisponibilidadeVLPAESTLOWPRICE: TDBEdit;
    pnlLayoutLinhasgodisponibilidade27: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestpaterceiro: TPanel;
    pnlLayoutCamposgodisponibilidadevlestpaterceiro: TPanel;
    dbegodisponibilidadeVLESTPATERCEIRO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade28: TPanel;
    pnlLayoutTitulosgodisponibilidadevllancamento_projeto: TPanel;
    pnlLayoutCamposgodisponibilidadevllancamento_projeto: TPanel;
    dbegodisponibilidadeVLLANCAMENTO_PROJETO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade29: TPanel;
    pnlLayoutTitulosgodisponibilidadevlesttransito: TPanel;
    pnlLayoutCamposgodisponibilidadevlesttransito: TPanel;
    dbegodisponibilidadeVLESTTRANSITO: TDBEdit;
    tabContasPagar: TcxTabSheet;
    pnlLayoutLinhasgodisponibilidade30: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcontapagarvencido: TPanel;
    pnlLayoutCamposgodisponibilidadevlcontapagarvencido: TPanel;
    dbegodisponibilidadeVLCONTAPAGARVENCIDO: TDBEdit;
    pnlLayoutLinhasgodisponibilidade31: TPanel;
    pnlLayoutTitulosgodisponibilidadevlimpostosvencidos: TPanel;
    pnlLayoutCamposgodisponibilidadevlimpostosvencidos: TPanel;
    dbegodisponibilidadeVLIMPOSTOSVENCIDOS: TDBEdit;
    pnlLayoutLinhasgodisponibilidade32: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcontaspagar30: TPanel;
    pnlLayoutCamposgodisponibilidadevlcontaspagar30: TPanel;
    dbegodisponibilidadeVLCONTASPAGAR30: TDBEdit;
    pnlLayoutLinhasgodisponibilidade33: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcontaspagar60: TPanel;
    pnlLayoutCamposgodisponibilidadevlcontaspagar60: TPanel;
    dbegodisponibilidadeVLCONTASPAGAR60: TDBEdit;
    pnlLayoutLinhasgodisponibilidade34: TPanel;
    pnlLayoutTitulosgodisponibilidadevlativos: TPanel;
    pnlLayoutCamposgodisponibilidadevlativos: TPanel;
    dbegodisponibilidadeVLSALARIOSPJS: TDBEdit;
    psqprojeto: TPesquisa;
    psqfilial: TPesquisa;
    cdsgodisponibilidadeVLTOTALDIVIDASCURTOPRAZO: TCurrencyField;
    cdsgodisponibilidadeVLTOTALCREDITOCURTOPRAZO: TCurrencyField;
    cdsgodisponibilidadeVLTOTALSALDOLIQUIDO1: TCurrencyField;
    cdsgodisponibilidadeVLTOTALESTOQUES: TCurrencyField;
    cdsgodisponibilidadeVLTOTALSALDOLIQUIDO2: TCurrencyField;
    cdsgodisponibilidadeVLTOTALCONTASPAGAR: TCurrencyField;
    cdsgodisponibilidadeVLTOTALSALDOLIQUIDO3: TCurrencyField;
    cdsgodisponibilidadeVLTOTALNEGOCIACOESPAGAR: TCurrencyField;
    cdsgodisponibilidadeVLTOTALSALDOLIQUIDO4: TCurrencyField;
    cdsgodisponibilidadeVLSALARIOSPJS: TFloatField;
    pnlLayoutLinhasgodisponibilidadeativos: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcontaspagarATIVOS: TPanel;
    pnlLayoutCamposgodisponibilidadevlcontaspagaATIVOS: TPanel;
    dbegodisponibilidadeVLATIVOS: TDBEdit;
    RelatrioMensal1: TMenuItem;
    dbegodisponibilidadeDTCOMPETENCIA: TcxDBDateEdit;
    gridisponibilidade: TcxGrid;
    GriddisponibilidadeDBTV: TcxGridDBTableView;
    GriddisponibilidadeDBTVCODPROJETO: TcxGridDBColumn;
    GriddisponibilidadeDBTVPROJETO: TcxGridDBColumn;
    GriddisponibilidadeDBTVCODFILIAL: TcxGridDBColumn;
    GriddisponibilidadeDBTVFILIAL: TcxGridDBColumn;
    GriddisponibilidadeDBTVDTCOMPETENCIA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLFOMENTOMP: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLMOVIMENTODIA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCAIXAGERAL: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLBANCOCONTACORRENTE: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCHEQUES: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLDUPLICATAS: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLAVISTA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCARTEIRASIMPLES: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRACURVAA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRACURVAB: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRACURVAC: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPSEMGIRO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTEMCURVAA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTEMCURVAB: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTEMCURVAC: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPACURVAA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPACURVAB: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPAFORALINHA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPACURVANICHO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLPAESTLOWPRICE: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPATERCEIRO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLLANCAMENTO_PROJETO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTTRANSITO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTAPAGARVENCIDO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLIMPOSTOSVENCIDOS: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTASPAGAR30: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTASPAGAR60: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLATIVOS: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLSALARIOSPJS: TcxGridDBColumn;
    cxGridLevedisponibilidade: TcxGridLevel;
    spdExportargopainelrisco: TButton;
    GriddisponibilidadeDBTVVLTOTALDIVIDASCURTOPRAZO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALCREDITOCURTOPRAZO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO1: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALESTOQUES: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO2: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALCONTASPAGAR: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO3: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO4: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLTOTALNEGOCIACOESPAGAR: TcxGridDBColumn;
    pnlLayoutLinhasgodisponibilidade35: TPanel;
    pnlLayoutTitulosgodisponibilidadevlestpacurvaC: TPanel;
    pnlLayoutCamposgodisponibilidadevlestpacurvaC: TPanel;
    dbegodisponibilidadeVLESTPACURVAC: TDBEdit;
    pnlLayoutLinhasgodisponibilidade36: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcontaspagarMAIS60: TPanel;
    pnlLayoutCamposgodisponibilidadevlcontaspagaMAIS60: TPanel;
    dbegodisponibilidadeVLCONTASPAGAMAIS60: TDBEdit;
    pnlLayoutLinhasgodisponibilidadeRENEG: TPanel;
    pnlLayoutTitulosgodisponibilidadevlcontaspagarRENEG: TPanel;
    pnlLayoutCamposgodisponibilidadevlcontaspagaRENEG: TPanel;
    dbegodisponibilidadeRENEG: TDBEdit;
    cdsgodisponibilidadeVLESTPACURVAC: TFloatField;
    cdsgodisponibilidadeVLCONTASPAGARACIMA60: TFloatField;
    cdsgodisponibilidadeVLRENEGOCIACOES: TFloatField;
    GriddisponibilidadeDBTVVLESTPACURVAC: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTASPAGARACIMA60: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLRENEGOCIACOES: TcxGridDBColumn;
    spdDUPLICAR: TButton;
    cdsintervalo: TClientDataSet;
    cdsintervalodtiniciomes: TDateField;
    cdsintervalodtfinalmes: TDateField;
    frxDBIntervalo: TfrxDBDataset;
    cdsgodisponibilidadeMAIORDATA: TAggregateField;
    cdsgodisponibilidadeMENORDATA: TAggregateField;
    lblProjetoresultado: TLabel;
    lblusuarioresultado: TLabel;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    lblversao: TLabel;
    frxgodisponibilidadeII: TfrxReport;
    GriddisponibilidadeDBTVID: TcxGridDBColumn;
    cdsgodisponibilidadeselecionado: TBooleanField;
    GriddisponibilidadeDBTVSelecionar: TcxGridDBColumn;
    spdExcluirgodisponibilidadetodos: TButton;
    spdmarcartodos: TButton;
    spddesmarcartodos: TButton;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargodisponibilidadeClick(Sender: TObject);
	procedure spdAlterargodisponibilidadeClick(Sender: TObject);
	procedure spdGravargodisponibilidadeClick(Sender: TObject);
	procedure spdCancelargodisponibilidadeClick(Sender: TObject);
	procedure spdExcluirgodisponibilidadeClick(Sender: TObject);
	procedure cdsgodisponibilidadeBeforePost(DataSet: TDataSet);
    procedure spdOpcoesClick(Sender: TObject);
    procedure cdsgodisponibilidadeCalcFields(DataSet: TDataSet);
    procedure RelatrioMensal1Click(Sender: TObject);
    procedure GriddisponibilidadeDBTVDblClick(Sender: TObject);
    procedure spdExportargopainelriscoClick(Sender: TObject);
    procedure GriddisponibilidadeDBTVVLTOTALDIVIDASCURTOPRAZOCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALCREDITOCURTOPRAZOCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALESTOQUESCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO2CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALCONTASPAGARCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO3CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO4CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cgcInternoChange(Sender: TObject);
    procedure spdDUPLICARClick(Sender: TObject);
    procedure spdExcluirgodisponibilidadetodosClick(Sender: TObject);
    procedure spdmarcartodosClick(Sender: TObject);
    procedure spddesmarcartodosClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgodisponibilidade: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgodisponibilidade: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgodisponibilidade: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgodisponibilidade: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagodisponibilidade: Array[0..0]  of TDuplicidade;
	duplicidadeCampogodisponibilidade: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
  procedure sessao;
  function ConsultaMensal: string;
	public

end;

var
fgodisponibilidade: Tfgodisponibilidade;

implementation

uses udm, Vcl.DialogMessage, System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfgodisponibilidade.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgodisponibilidade.TabIndex;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALCONTASPAGARCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALCREDITOCURTOPRAZOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALDIVIDASCURTOPRAZOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALESTOQUESCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO2CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO3CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.GriddisponibilidadeDBTVVLTOTALSALDOLIQUIDO4CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgodisponibilidade.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
  if cdsgodisponibilidade.State in [dsedit,dsinsert]  then cdsgodisponibilidade.Cancel;
	fncgodisponibilidade.verificarEmTransacao(cdsgodisponibilidade);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgodisponibilidade.configurarGridesFormulario(fgodisponibilidade,true, false);

	fncgodisponibilidade.Free;
	{eliminando container de fun��es da memoria<<<}

	fgodisponibilidade:=nil;
	Action:=cafree;
end;

procedure Tfgodisponibilidade.FormCreate(Sender: TObject);
begin
  sessao;
  {>>>container de fun��es}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
	{>>>container de fun��es}
	fncgodisponibilidade:= TFuncoes.Create(Self);
	fncgodisponibilidade.definirConexao(dm.conexao);
	fncgodisponibilidade.definirConexaohistorico(dm.conexao);
	fncgodisponibilidade.definirFormulario(Self);
	fncgodisponibilidade.validarTransacaoRelacionamento:=true;
	fncgodisponibilidade.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDTCOMPETENCIA';
	pesquisasItem.titulo:='Data Compet�ncia';
	pesquisasItem.campo:='DTCOMPETENCIA';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

  pesquisasItem := TPesquisas.Create;
  pesquisasItem.agrupador := 'pesCODPROJETO';
  pesquisasItem.titulo := 'C�digo Projeto';
  pesquisasItem.campo := 'CODPROJETO';
  pesquisasItem.tipoFDB := 'Integer';
  pesquisasItem.criterio := '=';
  pesquisasItem.intervalo := 'S';
  pesquisasItem.tamanho := 72;
  pesquisasItem.comboItens.clear;
  pesquisasItem.lookupSelect := '';
  pesquisasItem.lookupCampoID := '';
  pesquisasItem.lookupCampoTipoID := 'Integer';
  pesquisasItem.lookupCampoDescricao := '';
  pesquisasItem.lookupGridItens.clear;
  pesquisasLista[3] := pesquisasItem;

  pesquisasItem := TPesquisas.Create;
  pesquisasItem.agrupador := 'pesPROJETO';
  pesquisasItem.titulo := 'Projeto';
  pesquisasItem.campo := 'PROJETO';
  pesquisasItem.tipoFDB := 'Varchar';
  pesquisasItem.criterio := 'LIKE';
  pesquisasItem.intervalo := 'N';
  pesquisasItem.tamanho := 480;
  pesquisasItem.comboItens.clear;
  pesquisasItem.lookupSelect := '';
  pesquisasItem.lookupCampoID := '';
  pesquisasItem.lookupCampoTipoID := 'Integer';
  pesquisasItem.lookupCampoDescricao := '';
  pesquisasItem.lookupGridItens.clear;
  pesquisasLista[4] := pesquisasItem;

  pesquisasItem := TPesquisas.Create;
  pesquisasItem.agrupador := 'pesCODFILIAL';
  pesquisasItem.titulo := 'C�d.Filial';
  pesquisasItem.campo := 'CODFILIAL';
  pesquisasItem.tipoFDB := 'Integer';
  pesquisasItem.criterio := '=';
  pesquisasItem.intervalo := 'S';
  pesquisasItem.tamanho := 72;
  pesquisasItem.comboItens.clear;
  pesquisasItem.lookupSelect := '';
  pesquisasItem.lookupCampoID := '';
  pesquisasItem.lookupCampoTipoID := 'Integer';
  pesquisasItem.lookupCampoDescricao := '';
  pesquisasItem.lookupGridItens.clear;
  pesquisasLista[5] := pesquisasItem;

  pesquisasItem := TPesquisas.Create;
  pesquisasItem.agrupador := 'pesFILIAL';
  pesquisasItem.titulo := 'Filial';
  pesquisasItem.campo := 'FILIAL';
  pesquisasItem.tipoFDB := 'Varchar';
  pesquisasItem.criterio := 'LIKE';
  pesquisasItem.intervalo := 'N';
  pesquisasItem.tamanho := 480;
  pesquisasItem.comboItens.clear;
  pesquisasItem.lookupSelect := '';
  pesquisasItem.lookupCampoID := '';
  pesquisasItem.lookupCampoTipoID := 'Integer';
  pesquisasItem.lookupCampoDescricao := '';
  pesquisasItem.lookupGridItens.clear;
  pesquisasLista[6] := pesquisasItem;


   {gerando campos no clientdatsaet de filtros}
   fncgodisponibilidade.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
   {gerando campos no clientdatsaet de filtros}


	fncgodisponibilidade.criarPesquisas(sqlgodisponibilidade,cdsgodisponibilidade,'select * from godisponibilidade', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios godisponibilidade}
	camposObrigatoriosgodisponibilidade[0] :=dbegodisponibilidadedtcompetencia;
	{campos obrigatorios godisponibilidade<<<}

	{>>>objetos ativos no modo de manipula��o de dados godisponibilidade}
//	objetosModoEdicaoAtivosgodisponibilidade[0]:=pnlDadosgodisponibilidade;
	objetosModoEdicaoAtivosgodisponibilidade[0]:=spdCancelargodisponibilidade;
	objetosModoEdicaoAtivosgodisponibilidade[1]:=spdGravargodisponibilidade;
  objetosModoEdicaoAtivosgodisponibilidade[2]:=dbegodisponibilidadeCODPROJETO;
  objetosModoEdicaoAtivosgodisponibilidade[3]:=dbegodisponibilidadeCODFILIAL;
  objetosModoEdicaoAtivosgodisponibilidade[4]:=btnprojeto;
  objetosModoEdicaoAtivosgodisponibilidade[5]:=btnfilial;
	{objetos ativos no modo de manipula��o de dados godisponibilidade<<<}

	{>>>objetos inativos no modo de manipula��o de dados godisponibilidade}
	objetosModoEdicaoInativosgodisponibilidade[0]:=spdAdicionargodisponibilidade;
	objetosModoEdicaoInativosgodisponibilidade[1]:=spdAlterargodisponibilidade;
	objetosModoEdicaoInativosgodisponibilidade[2]:=spdExcluirgodisponibilidade;
	objetosModoEdicaoInativosgodisponibilidade[3]:=spdOpcoes;
	objetosModoEdicaoInativosgodisponibilidade[4]:=tabPesquisas;
  objetosModoEdicaoInativosgodisponibilidade[5]:=dbegodisponibilidadePROJETO;
  objetosModoEdicaoInativosgodisponibilidade[6]:=dbegodisponibilidadeFILIAL;
	{objetos inativos no modo de manipula��o de dados godisponibilidade<<<}

	{>>>comando de adi��o de teclas de atalhos godisponibilidade}
	fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Adicionar novo registro',VK_F4,spdAdicionargodisponibilidade.OnClick);
	fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Alterar registro selecionado',VK_F5,spdAlterargodisponibilidade.OnClick);
	fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Gravar �ltimas altera��es',VK_F6,spdGravargodisponibilidade.OnClick);
	fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Cancelar �ltimas altera��e',VK_F7,spdCancelargodisponibilidade.OnClick);
	fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgodisponibilidade.OnClick);
 	fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Excluir agrupado selecionados',ShortCut(VK_F8,[ssCtrl]),spdExcluirgodisponibilidadetodos.OnClick);
  fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Marcar todos',ShortCut(VK_F9,[ssCtrl]),spdmarcartodos.OnClick);
  fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Desmarcar todos',ShortCut(VK_F10,[ssCtrl]),spddesmarcartodos.OnClick);
  fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Duplicar registro selecionado',ShortCut(VK_F12,[ssCtrl]),spdDUPLICAR.OnClick);
  fncgodisponibilidade.criaAtalhoPopupMenu(Popupgodisponibilidade,'Exportar Grid Excel',ShortCut(VK_F3,[ssCtrl]),spdExportargopainelrisco.OnClick);

	{comando de adi��o de teclas de atalhos godisponibilidade<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgodisponibilidade.open();

	{abertura dos datasets<<<}

  {filtros projeto}
  if ParamStr(4) <>'99' then
  fncgodisponibilidade.filtrar(cdsgodisponibilidade,'CODPROJETO='+ParamStr(4));
  {filtros projeto}

	fncgodisponibilidade.criaAtalhoPopupMenuNavegacao(Popupgodisponibilidade,cdsgodisponibilidade);

	fncgodisponibilidade.definirMascaraCampos(cdsgodisponibilidade);

	fncgodisponibilidade.configurarGridesFormulario(fgodisponibilidade,false, true);

    {<<vers�o da rotina>>}
  lblversao.Caption:=lblversao.Caption+' - '+fncgodisponibilidade.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}


end);
end;

procedure Tfgodisponibilidade.spdAdicionargodisponibilidadeClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgodisponibilidade.TabIndex;
  cgcInterno.ActivePageIndex:=tabPrincipal.TabIndex;

	if (fncgodisponibilidade.adicionar(cdsgodisponibilidade)=true) then
		begin
			fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,true);
			dbegodisponibilidadedtcompetencia.SetFocus;
	end
	else
		fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,false);
end;

procedure Tfgodisponibilidade.spdAlterargodisponibilidadeClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgodisponibilidade.TabIndex;
  cgcInterno.ActivePageIndex:=tabPrincipal.TabIndex;

	if (fncgodisponibilidade.alterar(cdsgodisponibilidade)=true) then
		begin
			fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,true);
			dbegodisponibilidadedtcompetencia.SetFocus;
	end
	else
		fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,false);
end;

procedure Tfgodisponibilidade.spdGravargodisponibilidadeClick(Sender: TObject);
begin
	fncgodisponibilidade.verificarCamposObrigatorios(cdsgodisponibilidade, camposObrigatoriosgodisponibilidade);

	if (fncgodisponibilidade.gravar(cdsgodisponibilidade)=true) then
		fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,false)
	else
		fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,true);
end;

procedure Tfgodisponibilidade.spdCancelargodisponibilidadeClick(Sender: TObject);
begin
	if (fncgodisponibilidade.cancelar(cdsgodisponibilidade)=true) then
		fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,false)
	else
		fncgodisponibilidade.ativarModoEdicao(objetosModoEdicaoAtivosgodisponibilidade,objetosModoEdicaoInativosgodisponibilidade,true);
end;

procedure Tfgodisponibilidade.spddesmarcartodosClick(Sender: TObject);
begin

   TDialogMessage.ShowWaitMessage('Desmarcando todos...',
   procedure
   begin
      cdsgodisponibilidade.First;
      while not cdsgodisponibilidade.Eof do
      begin
      cdsgodisponibilidade.Edit;
      cdsgodisponibilidadeselecionado.AsBoolean:= false;
      cdsgodisponibilidade.Post;
      cdsgodisponibilidade.Next;
      end;
   end);

end;

procedure Tfgodisponibilidade.spdDUPLICARClick(Sender: TObject);
begin
    if not cdsgodisponibilidade.IsEmpty then
    fncgodisponibilidade.Duplicarregistro(cdsgodisponibilidade, true, 'ID','G_GODISPONIBILIDADE');
end;

procedure Tfgodisponibilidade.spdExcluirgodisponibilidadeClick(Sender: TObject);
begin
	fncgodisponibilidade.excluir(cdsgodisponibilidade);
end;

procedure Tfgodisponibilidade.spdExcluirgodisponibilidadetodosClick(
  Sender: TObject);
begin
  cdsgodisponibilidade.Filtered := False;
  cdsgodisponibilidade.Filter := 'selecionado = ' + 'True';
  cdsgodisponibilidade.Filtered := True;

  cdsgodisponibilidade.First;
  while not cdsgodisponibilidade.Eof do
  begin
   dm.FDQuery.Close;
   dm.FDQuery.sql.Clear;
   dm.FDQuery.SQL.Add('delete from godisponibilidade where id=:id');
   dm.FDQuery.ParamByName('id').AsInteger:= cdsgodisponibilidadeid.AsInteger;
   dm.FDQuery.ExecSQL;
   cdsgodisponibilidade.Next;
  end;

  cdsgodisponibilidade.Filtered := false;
  application.MessageBox('Exclus�o agrupada realizada com sucesso','Exclus�o agrupada.',MB_OK);

end;

procedure Tfgodisponibilidade.spdExportargopainelriscoClick(Sender: TObject);
begin
    if not cdsgodisponibilidade.IsEmpty then
    fncgodisponibilidade.exportarGrides(fgodisponibilidade, gridisponibilidade);
end;

procedure Tfgodisponibilidade.cdsgodisponibilidadeBeforePost(DataSet: TDataSet);
begin
	if cdsgodisponibilidade.State in [dsinsert] then
	cdsgodisponibilidadeID.Value:=fncgodisponibilidade.autoNumeracaoGenerator(dm.conexao,'G_godisponibilidade');
end;

procedure Tfgodisponibilidade.cdsgodisponibilidadeCalcFields(DataSet: TDataSet);
begin
  {Dividas Curto Prazo}
  cdsgodisponibilidadeVLTOTALDIVIDASCURTOPRAZO.AsFloat := (
    cdsgodisponibilidadeVLMOVIMENTODIA.AsFloat +
    cdsgodisponibilidadeVLFOMENTOMP.AsFloat
  );

  {Creditos Curto Prazo}
  cdsgodisponibilidadeVLTOTALCREDITOCURTOPRAZO.AsFloat :=  (
    cdsgodisponibilidadeVLCAIXAGERAL.AsFloat +
    cdsgodisponibilidadeVLBANCOCONTACORRENTE.AsFloat +
    cdsgodisponibilidadeVLCHEQUES.AsFloat +
    cdsgodisponibilidadeVLDUPLICATAS.AsFloat +
    cdsgodisponibilidadeVLAVISTA.AsFloat +
    cdsgodisponibilidadeVLCARTEIRASIMPLES.AsFloat
  );

  {Saldo Liquido 1}
  cdsgodisponibilidadeVLTOTALSALDOLIQUIDO1.AsFloat := (
    cdsgodisponibilidadeVLTOTALCREDITOCURTOPRAZO.AsFloat -
    cdsgodisponibilidadeVLTOTALDIVIDASCURTOPRAZO.AsFloat
  );

  {Estoques}
  cdsgodisponibilidadeVLTOTALESTOQUES.AsFloat :=  (
    cdsgodisponibilidadeVLESTMPCOMPRA.AsFloat +
    cdsgodisponibilidadeVLESTMPCOMPRACURVAA.AsFloat +
    cdsgodisponibilidadeVLESTMPCOMPRACURVAB.AsFloat +
    cdsgodisponibilidadeVLESTMPCOMPRACURVAC.AsFloat +
    cdsgodisponibilidadeVLESTMPSEMGIRO.AsFloat +
    cdsgodisponibilidadeVLESTEMCURVAA.AsFloat +
    cdsgodisponibilidadeVLESTEMCURVAB.AsFloat +
    cdsgodisponibilidadeVLESTEMCURVAC.AsFloat +
    cdsgodisponibilidadeVLESTEMSEMGIRO.AsFloat +
    cdsgodisponibilidadeVLESTPACURVAA.AsFloat +
    cdsgodisponibilidadeVLESTPACURVAB.AsFloat +
    cdsgodisponibilidadeVLESTPACURVAC.AsFloat +
    cdsgodisponibilidadeVLESTPAFORALINHA.AsFloat +
    cdsgodisponibilidadeVLESTPACURVANICHO.AsFloat +
    cdsgodisponibilidadeVLPAESTLOWPRICE.AsFloat +
    cdsgodisponibilidadeVLESTPATERCEIRO.AsFloat +
    cdsgodisponibilidadeVLLANCAMENTO_PROJETO.AsFloat +
    cdsgodisponibilidadeVLESTTRANSITO.AsFloat
  );

  {Saldo Liquido 2}
  cdsgodisponibilidadeVLTOTALSALDOLIQUIDO2.AsFloat := (
    cdsgodisponibilidadeVLTOTALSALDOLIQUIDO1.AsFloat +
    cdsgodisponibilidadeVLTOTALESTOQUES.AsFloat
  );

  {Contas a Pagar}
  cdsgodisponibilidadeVLTOTALCONTASPAGAR.AsFloat :=  (
    cdsgodisponibilidadeVLSALARIOSPJS.AsFloat +
    cdsgodisponibilidadeVLCONTAPAGARVENCIDO.AsFloat +
    cdsgodisponibilidadeVLIMPOSTOSVENCIDOS.AsFloat +
    cdsgodisponibilidadeVLCONTASPAGAR30.AsFloat +
    cdsgodisponibilidadeVLCONTASPAGAR60.AsFloat +
    cdsgodisponibilidadeVLCONTASPAGARACIMA60.AsFloat);

  {Saldo Liquido 3}
  cdsgodisponibilidadeVLTOTALSALDOLIQUIDO3.AsFloat := (
    cdsgodisponibilidadeVLTOTALSALDOLIQUIDO2.AsFloat -
    cdsgodisponibilidadeVLTOTALCONTASPAGAR.AsFloat
  );

  {Total Renegociacoes a Pagar}
  cdsgodisponibilidadeVLTOTALNEGOCIACOESPAGAR.AsFloat := cdsgodisponibilidadeVLRENEGOCIACOES.AsFloat;
  {Saldo Liquido 4}
  cdsgodisponibilidadeVLTOTALSALDOLIQUIDO4.AsFloat := ((cdsgodisponibilidadeVLTOTALSALDOLIQUIDO3.AsFloat + cdsgodisponibilidadeVLATIVOS.AsFloat) -  cdsgodisponibilidadeVLTOTALNEGOCIACOESPAGAR.AsFloat);

end;

procedure Tfgodisponibilidade.cgcInternoChange(Sender: TObject);
begin
  SCRgodisponibilidade.VertScrollBar.Position :=  0;
end;

procedure Tfgodisponibilidade.sessao;
var host: string; arquivo: tinifile;
begin

   try
   //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);


    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;


procedure Tfgodisponibilidade.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgodisponibilidade.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgodisponibilidade.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgodisponibilidade.spdmarcartodosClick(Sender: TObject);
begin


   TDialogMessage.ShowWaitMessage('Marcando todos...',
   procedure
   begin
    cdsgodisponibilidade.First;
    while not cdsgodisponibilidade.Eof do
    begin
      cdsgodisponibilidade.Edit;
      cdsgodisponibilidadeselecionado.AsBoolean:= true;
      cdsgodisponibilidade.Post;
      cdsgodisponibilidade.Next;
    end;
  end);

end;

procedure Tfgodisponibilidade.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgodisponibilidade.menuImpRelSinteticoClick(Sender: TObject);
begin
  if not cdsgodisponibilidade.IsEmpty then
    fncgodisponibilidade.visualizarRelatorios(frxgodisponibilidadeI);
end;

procedure Tfgodisponibilidade.RelatrioMensal1Click(Sender: TObject);
 var
  vCODPROJETO, vCODFILIAL: Integer;
  vDataInicail, vDataFinal: TDateTime;
begin
  vCODPROJETO  := cdsgodisponibilidadeCODPROJETO.AsInteger;
  vCODFILIAL   := cdsgodisponibilidadeCODFILIAL.AsInteger;
  vDataInicail := StartofTheMonth(cdsgodisponibilidadeDTCOMPETENCIA.AsDateTime);
  vDataFinal   := EndofTheMonth(cdsgodisponibilidadeDTCOMPETENCIA.AsDateTime);

  vDataInicail  :=StrToDateDef(cdsgodisponibilidadeMENORDATA.asString,now);
  vDataFinal   :=StrToDateDef(cdsgodisponibilidadeMAIORDATA.asString,now);


  cdsintervalo.EmptyDataSet;
  cdsintervalo.Append;
  cdsintervalodtiniciomes.AsDateTime := vDataInicail;
  cdsintervalodtfinalmes.AsDateTime  := vDataFinal;
  cdsintervalo.Post;


  cdsgodisponibilidade.Close;
  sqlgodisponibilidade.CommandText := ConsultaMensal;
  sqlgodisponibilidade.ParamByName('CODPROJETO').AsInteger   := vCODPROJETO;
  sqlgodisponibilidade.ParamByName('CODFILIAL').AsInteger    := vCODFILIAL;
  sqlgodisponibilidade.ParamByName('DTCOMPETENCIAINICIAL').AsDateTime :=  vDataInicail;
  sqlgodisponibilidade.ParamByName('DTCOMPETENCIAFINAL').AsDateTime   :=  vDataFinal;

  cdsgodisponibilidade.Open;

  if not cdsgodisponibilidade.IsEmpty then
	  fncgodisponibilidade.visualizarRelatorios(frxgodisponibilidadeI);
end;

function Tfgodisponibilidade.ConsultaMensal: string;
begin
  Result := 'select '+
             ' 0 ID, '+
             ' CURRENT_DATE DT_CADASTRO, '+
             ' CURRENT_TIME HS_CADASTRO, '+
             ' CURRENT_DATE DTCOMPETENCIA, '+
             ' CODPROJETO, PROJETO, CODFILIAL, FILIAL, '+
             ' SUM(VLFOMENTOMP) VLFOMENTOMP,  '+
             ' SUM(VLMOVIMENTODIA) VLMOVIMENTODIA,  '+
             ' SUM(VLCAIXAGERAL) VLCAIXAGERAL,  '+
             ' SUM(VLBANCOCONTACORRENTE) VLBANCOCONTACORRENTE,  '+
             ' SUM(VLCHEQUES) VLCHEQUES,  '+
             ' SUM(VLDUPLICATAS) VLDUPLICATAS, '+
             ' SUM(VLAVISTA) VLAVISTA, '+
             ' SUM(VLCARTEIRASIMPLES) VLCARTEIRASIMPLES, '+
             ' SUM(VLESTMPCOMPRA) VLESTMPCOMPRA, '+
             ' SUM(VLESTMPCOMPRACURVAA) VLESTMPCOMPRACURVAA,  '+
             ' SUM(VLESTMPCOMPRACURVAB) VLESTMPCOMPRACURVAB, '+
             ' SUM(VLESTMPCOMPRACURVAC) VLESTMPCOMPRACURVAC, '+
             ' SUM(VLESTMPSEMGIRO) VLESTMPSEMGIRO, '+
             ' SUM(VLESTEMCURVAA) VLESTEMCURVAA, '+
             ' SUM(VLESTEMCURVAB) VLESTEMCURVAB, '+
             ' SUM(VLESTEMCURVAC) VLESTEMCURVAC,    '+
             ' SUM(VLESTEMSEMGIRO) VLESTEMSEMGIRO,     '+
             ' SUM(VLESTPACURVAA) VLESTPACURVAA,  '+
             ' SUM(VLESTPACURVAB) VLESTPACURVAB,  '+
             ' SUM(VLESTPACURVAC) VLESTPACURVAC,  '+
             ' SUM(VLESTPAFORALINHA) VLESTPAFORALINHA,  '+
             ' SUM(VLESTPACURVANICHO) VLESTPACURVANICHO,   '+
             ' SUM(VLPAESTLOWPRICE) VLPAESTLOWPRICE,   '+
             ' SUM(VLESTPATERCEIRO) VLESTPATERCEIRO,     '+
             ' SUM(VLLANCAMENTO_PROJETO) VLLANCAMENTO_PROJETO, '+
             ' SUM(VLESTTRANSITO) VLESTTRANSITO,  '+
             ' SUM(VLSALARIOSPJS) VLSALARIOSPJS,  '+
             ' SUM(VLCONTAPAGARVENCIDO) VLCONTAPAGARVENCIDO,  '+
             ' SUM(VLIMPOSTOSVENCIDOS) VLIMPOSTOSVENCIDOS,  '+
             ' SUM(VLCONTASPAGAR30) VLCONTASPAGAR30, '+
             ' SUM(VLCONTASPAGAR60) VLCONTASPAGAR60,  '+
             ' SUM(VLCONTASPAGARACIMA60) VLCONTASPAGARACIMA60,  '+
             ' SUM(VLATIVOS) VLATIVOS,   '+
             ' SUM(VLRENEGOCIACOES) VLRENEGOCIACOES   '+
             ' from godisponibilidade   '+
             ' where CODPROJETO = :CODPROJETO '+
             ' AND CODFILIAL = :CODFILIAL  '+
             ' AND DTCOMPETENCIA between :DTCOMPETENCIAINICIAL and :DTCOMPETENCIAFINAL   '+
             ' group by ID,  DT_CADASTRO, HS_CADASTRO, DTCOMPETENCIA, '+
             ' CODPROJETO, PROJETO, CODFILIAL, FILIAL';
end;

end.

