CREATE TABLE GOPROJETOCONSULTOR(
ID Integer   
,
CONSTRAINT pk_GOPROJETOCONSULTOR PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODPROJETO Integer   
,
CODCONSULTOR Integer  not null 
,
CONSULTOR Varchar(250)   COLLATE PT_BR
,
DT_ENTRADA Date   
,
DT_SAIDA Date   
);
commit;
CREATE ASC INDEX I01_GOPR_ULTOR_ID ON GOPROJETOCONSULTOR (ID);
CREATE ASC INDEX I02_GOPR_ULTOR_DT_CADASTRO ON GOPROJETOCONSULTOR (DT_CADASTRO);
CREATE ASC INDEX I03_GOPR_ULTOR_HS_CADASTRO ON GOPROJETOCONSULTOR (HS_CADASTRO);
CREATE ASC INDEX I04_GOPR_ULTOR_CODPROJETO ON GOPROJETOCONSULTOR (CODPROJETO);
CREATE ASC INDEX I05_GOPR_ULTOR_CODCONSULTOR ON GOPROJETOCONSULTOR (CODCONSULTOR);
CREATE ASC INDEX I06_GOPR_ULTOR_CONSULTOR ON GOPROJETOCONSULTOR (CONSULTOR);
CREATE ASC INDEX I07_GOPR_ULTOR_DT_ENTRADA ON GOPROJETOCONSULTOR (DT_ENTRADA);
CREATE ASC INDEX I08_GOPR_ULTOR_DT_SAIDA ON GOPROJETOCONSULTOR (DT_SAIDA);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Projeto' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'CODPROJETO';
update rdb$relation_fields set rdb$description = 'C�d.Consultor' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'CODCONSULTOR';
update rdb$relation_fields set rdb$description = 'Consultor' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'CONSULTOR';
update rdb$relation_fields set rdb$description = 'Data de Entrada' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'DT_ENTRADA';
update rdb$relation_fields set rdb$description = 'Data Sa�da' where rdb$relation_name = 'GOPROJETOCONSULTOR' and rdb$field_name = 'DT_SAIDA';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
