program menu;

uses
  Vcl.Forms,
  umenu in 'umenu.pas' {fPrincipal},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  uprocessoatualizacao in '..\class_shared\uprocessoatualizacao.pas' {fprocessoatualizacao},
  uEnvioMail in '..\class_shared\uEnvioMail.pas' {FenvioMail};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TfPrincipal, fPrincipal);
  Application.CreateForm(Tflogin, flogin);
  Application.CreateForm(Tfprocesso, fprocesso);
  Application.CreateForm(Tfprocessoatualizacao, fprocessoatualizacao);
  Application.Run;
end.
