unit umenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ExtCtrls, Data.FMTBcd,
  Data.DB, Datasnap.DBClient, Datasnap.Provider, Data.SqlExpr, Vcl.StdCtrls,
  dxGDIPlusClasses, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,shlobj, ShellAPI,
  Vcl.Menus, cxButtons, Vcl.WinXCtrls, uFuncoes,FileCtrl,IniFiles,
  Vcl.DialogMessage, uprocessoatualizacao,system.Zip, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type

  TfPrincipal = class(TForm)
    sqlrotina: TSQLDataSet;
    dsprotina: TDataSetProvider;
    cdsrotina: TClientDataSet;
    dsrotina: TDataSource;
    pnlbotoes: TFlowPanel;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    Label1: TLabel;
    edtBusca: TEdit;
    Image2: TImage;
    PopupMenu1: TPopupMenu;
    Favorito1: TMenuItem;
    pnlFavoritos: TFlowPanel;
    pnlTitulo: TPanel;
    lblProjeto: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    lblempresa: TLabel;
    cdsrotinaID_MODULO: TIntegerField;
    cdsrotinaDESCRICAO: TStringField;
    cdsrotinaDESCRICAO_1: TStringField;
    cdsrotinaTITULO: TStringField;
    cdsrotinaMENU: TStringField;
    MainMenu: TMainMenu;
    mmfinanceiro: TMenuItem;
    mmcontroladoria: TMenuItem;
    mmoutro: TMenuItem;
    Cadastros1: TMenuItem;
    ControledeRecebveis1: TMenuItem;
    ControledeRecebveis2: TMenuItem;
    Relatrios1: TMenuItem;
    PaineldeRisco1: TMenuItem;
    PaineldeRisco2: TMenuItem;
    Relatrios2: TMenuItem;
    PainelDisponibilidade1: TMenuItem;
    PaineldeDisponibilidade1: TMenuItem;
    Relatrios3: TMenuItem;
    CadastrodeUsurios1: TMenuItem;
    CadastrodeBanco1: TMenuItem;
    CadastrodeCNAE1: TMenuItem;
    cA1: TMenuItem;
    CadastrodeConsultor1: TMenuItem;
    CadastrodeFilial1: TMenuItem;
    CadastrodePortador1: TMenuItem;
    CadastrodeProjetos1: TMenuItem;
    ControledeAcesso1: TMenuItem;
    ControldedeAcesso1: TMenuItem;
    pnlrodape: TPanel;
    lblProjetoresultado: TLabel;
    lblusuarioresultado: TLabel;
    PopupHelp: TPopupMenu;
    Verificaratualizaes1: TMenuItem;
    spdOpcoes: TSpeedButton;
    spbreplicador: TSpeedButton;
    spbemail: TSpeedButton;
    FDMail: TFDQuery;
    FDMailID: TIntegerField;
    FDMailDT_CADASTRO: TDateField;
    FDMailHS_CADASTRO: TTimeField;
    FDMailDESCRICAO: TStringField;
    FDMailEMAIL1: TStringField;
    FDMailEMAIL2: TStringField;
    FDMailHORA: TTimeField;
    FDMailCAMINHO_PASTA: TStringField;
    FDMailREPLICADOR: TStringField;
    Timer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure criarbtn(Caption:string; top,left,height,width:integer; onClick:TNotifyEvent;  parent:TWinControl; Modulo:String;executavel:String);
    procedure SpeedButton1Click(sender: TObject);
    procedure SpeedButton1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure edtBuscaChange(Sender: TObject);
    procedure edtBuscaClick(Sender: TObject);
    procedure edtBuscaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Favorito1Click(Sender: TObject);
    procedure sessao;
    procedure criarfavorito(Caption:string; top,left,height,width:integer; onClick:TNotifyEvent;  parent:TWinControl);

    procedure CadastrodeUsurios1Click(Sender: TObject);
    procedure CadastrodeBanco1Click(Sender: TObject);
    procedure CadastrodeCNAE1Click(Sender: TObject);
    procedure cA1Click(Sender: TObject);
    procedure CadastrodeConsultor1Click(Sender: TObject);
    procedure CadastrodeFilial1Click(Sender: TObject);
    procedure CadastrodePortador1Click(Sender: TObject);
    procedure CadastrodeProjetos1Click(Sender: TObject);
    procedure ControledeRecebveis2Click(Sender: TObject);
    procedure PaineldeRisco2Click(Sender: TObject);
    procedure PaineldeDisponibilidade1Click(Sender: TObject);
    procedure ControldedeAcesso1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spdOpcoesClick(Sender: TObject);
    procedure Verificaratualizaes1Click(Sender: TObject);
    procedure spbreplicadorClick(Sender: TObject);
    procedure atualizarreplicador(id: integer; tipo:string);
    procedure spbemailClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure replicar_automatico;


  private
    { Private declarations }
    fncPrincipal: TFuncoes;

  public
   fncmenu: TFuncoes;
   caminho_projeto_remoto, caminho_projeto_local : string;
    { Public declarations }
  end;

var
  fPrincipal: TfPrincipal;
  fav: string;
  ArquivoINI: TIniFile;
  codprojeto: integer;
  HoraExecucaoMail : String;

implementation

{$R *.dfm}

uses udm;


procedure TfPrincipal.edtBuscaChange(Sender: TObject);
var
i:integer;
Modulo: String;
begin
 cdsrotina.DisableControls;

 try
   cdsrotina.Filtered :=false;
   cdsrotina.FilterOptions:= [foNoPartialCompare];
   cdsrotina.Filter :=  'UPPER(Titulo) LIKE ''%' + UPPERCASE(edtBusca.Text)+ '%''';
   cdsrotina.Filtered := True;

   for i := pnlbotoes.ComponentCount-1 downto 0 do
      if (pnlbotoes.Components[i] is TPanel) or (pnlbotoes.Components[i] is TLabel) then
         TButton(pnlbotoes.Components[i]).Free;


     while not cdsrotina.Eof do
 begin



   criarbtn(cdsrotinaTITULO.AsString,top,left,97,193,SpeedButton1Click,pnlBotoes,Modulo, cdsrotinaDESCRICAO_1.AsString);
   Modulo:= cdsrotinaDESCRICAO.AsString;
   cdsrotina.Next;






 end;

 finally
   cdsrotina.First;
   cdsrotina.EnableControls;
 end;
end;

procedure TfPrincipal.edtBuscaClick(Sender: TObject);
begin
 edtBusca.Color := clWhite;
end;

procedure TfPrincipal.edtBuscaExit(Sender: TObject);
begin
 edtBusca.Color := $00441E13;
end;



procedure TfPrincipal.Favorito1Click(Sender: TObject);
var
 strValores : TStringList;
 path:string;
begin

  ArquivoINI := TIniFile.Create(ExtractFilePath(Application.ExeName)+'favorito.ini');


  if ArquivoINI.SectionExists('Favorito') then
  begin
    strValores := TStringList.Create;
    ArquivoINI.ReadSection('Favorito', strValores);

    ArquivoINI.WriteString('Favorito', 'botao'+ IntToStr(strValores.Count), fav );
  end
  else
  begin
    ArquivoINI.WriteString('Favorito', 'botao0', fav );
  end;
  ArquivoINI.Free;


end;

procedure TfPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if Application.MessageBox('Deseja realmente Fechar? Todas as rotinas ser�o encerradas.','Fechar',MB_YESNO+MB_ICONQUESTION)=IDYES then
  begin
    fncmenu.KillTask('GOFATURAMENTO.exe');
    fncmenu.KillTask('gocadastros.exe');
    fncmenu.KillTask('goajustes.exe');
    fncmenu.KillTask('gopainelimportacao.exe');
    fncmenu.KillTask('GORECEBIVEIS.exe');
    fncmenu.KillTask('GOCPG.exe');
    fncmenu.KillTask('gopainelrisco.exe');
    fncmenu.KillTask('GODISPONIBILIDADE.exe');
    fncmenu.KillTask('GOMOVIMENTOFINANCEIRO.exe');
    fncmenu.KillTask('gocpr.exe');
    dm.FDQuery.Close;
    dm.FDQuery.SQL.Clear;
    dm.FDQuery.SQL.Add('update gousuarios set conectado=:conectado');
    dm.FDQuery.ParamByName('conectado').AsString:='N';
    dm.FDQuery.ExecSQL;
    application.Terminate;
  end
  else abort;
end;

procedure TfPrincipal.FormCreate(Sender: TObject);
var
  cont:integer;
  top:integer;
  left:integer;
  Rct: TRect;
  Modulo: String;
  edtmodulo:TLabel;
  i: integer;
   j: integer;
  strValores : TStringList;
  strValores1 : TStringList;
  edtfav:TLabel;
 region : hrgn;
begin

// sessao;
 {>>>container de tradu�a� de compomentes}
 if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
    begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
    end;
  {>>>container de tradu�a� de compomentes}
	{>>>container de fun��es}


	fncmenu:= TFuncoes.Create(Self);
	fncmenu.definirConexao(dm.conexao);
	fncmenu.definirFormulario(Self);
	fncmenu.validarTransacaoRelacionamento:=true;
	fncmenu.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  {<<vers�o da rotina>>}
  //lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncmenu.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}

 top:=50;
 left:=30;

 cdsrotina.Open;
 cdsrotina.DisableControls;
 while not cdsrotina.Eof do
 begin

   criarbtn(cdsrotinaTITULO.AsString,top,left,80,150,SpeedButton1Click,pnlBotoes,Modulo,cdsrotinaDESCRICAO_1.AsString);
   Modulo:= cdsrotinaDESCRICAO.AsString;
   cdsrotina.Next;


   left:=left +1000;

 end;
  //calcular tamanho do painel
  for I := 0 to pnlBotoes.ComponentCount  - 1 do
  begin

   if pnlbotoes.Components[i] is TPanel then
   begin
    pnlBotoes.Height:= pnlBotoes.Height+ 60;
   end;

  end;


  pnlFavoritos.Height := pnlbotoes.Height;

 HorzScrollBar.Range := pnlbotoes.Width;
 VertScrollBar.Range := pnlbotoes.Height;

  ArquivoINI := TIniFile.Create(ExtractFilePath(Application.ExeName)+'favorito.ini');

  if ArquivoINI.SectionExists('Favorito') then
  begin
    strValores := TStringList.Create;
    strValores1 := TStringList.Create;
    ArquivoINI.ReadSection('Favorito', strValores);


    edtfav := TLabel.Create(parent);
    //edt.Brush.Style:=bsclear;
    edtfav.Caption :=  'Favoritos';
    edtfav.Font.Color :=clWhite;
    edtfav.Font.Name := 'Segoe MDL2 Assets';
    edtfav.Font.Size := 20;
    edtfav.Font.Style := [fsBold];
    edtfav.Alignment := taLeftJustify;
    edtfav.Parent:=pnlFavoritos;
    edtfav.Align:=alTop;
    edtfav.Left:=0;
    edtfav.Top:=20;
    edtfav.Height:=29 ;
    edtfav.Width:=5000;
    edtfav.AlignWithMargins := true;
    edtfav.Margins.Left   := 40;
    edtfav.Margins.Top    := 20;

    for I := 0 to strValores.Count - 1 do
      strValores1.add(ArquivoINI.ReadString('Favorito', strValores.Strings[i], 'Erro'));


       for I := 0 to strValores1.Count - 1 do
      begin

        criarfavorito(strValores1.Strings[i],2,left,20,193,SpeedButton1Click,pnlFavoritos);
      end;

  end;

  ArquivoINI.Free;



end;

procedure TfPrincipal.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  LTopLeft, LTopRight, LBottomLeft, LBottomRight: SmallInt;
  LPoint: TPoint;
  ScrollBox: TScrollBox;
begin
  ScrollBox := TScrollBox(Sender);
  LPoint := ScrollBox.ClientToScreen(Point(0,0));
  LTopLeft := LPoint.X;
  LTopRight := LTopLeft + ScrollBox.ClientWidth;
  LBottomLeft := LPoint.Y;
  LBottomRight := LBottomLeft + ScrollBox.ClientWidth;
  if (MousePos.X >= LTopLeft) and
    (MousePos.X <= LTopRight) and
    (MousePos.Y >= LBottomLeft) and
    (MousePos.Y <= LBottomRight) then
  begin
    ScrollBox.VertScrollBar.Position := ScrollBox.VertScrollBar.Position - WheelDelta;
    Handled := True;
  end;
end;



procedure TfPrincipal.FormShow(Sender: TObject);
var
  Usuario: String;
  resultado : TStringList;
  sNomeArquivoAtualizacao: string;
  arquivo : TZipFile;
  arquivoini : tinifile;
  instalado : string;
begin
  instalado:='S';

  arquivoini  :=tinifile.Create('C:\goassessoria\parametros.ini');
  instalado   :=arquivoini.ReadString('local1','propriedade1','Erro ao ler par�metro');

  if arquivoini.ReadString('email','email','Erro ao ler par�metro')='S' then spbemail.Enabled:=true else spbemail.Enabled :=false;

  if instalado='N' then
  begin
    if FileExists('c:\goassessoria\firebird.exe') then
    begin
      ShellExecute(Self.Handle, 'open', 'c:\goassessoria\firebird.exe',nil, nil, SW_SHOWNORMAL);
      if Application.MessageBox('Deseja realmente continuar??','Instala�ao.',MB_YESNO+MB_ICONQUESTION)=IDNO then
      abort;
    end;

    if  not DirectoryExists('c:\goassessoria\executaveis') then
    begin

     CreateDir('c:\goassessoria\executaveis');
     CopyFile(pchar('c:\goassessoria\Tradu��oDev.ini'), pchar('c:\goassessoria\executaveis\Tradu��oDev.ini'), True);
     if CopyFile(pchar('c:\goassessoria\goAssessoria.zip'), pchar('c:\goassessoria\executaveis\goAssessoria.zip'), True) then
     begin

      try
       arquivo  :=   TZipFile.Create;
       arquivo.Open('c:\goassessoria\executaveis\goAssessoria.zip',zmread);
       arquivo.ExtractAll('c:\goassessoria\executaveis');

      finally
       freeandnil(arquivo);
       if FileExists('c:\goassessoria\executaveis\goAssessoria.zip') then
       DeleteFile('c:\goassessoria\executaveis\goAssessoria.zip');

       if FileExists('c:\goassessoria\parametros.ini') then
       DeleteFile('c:\goassessoria\parametros.ini');

      end;
     end;

    end;
  end;

  resultado:= TStringList.Create;
  resultado:= fncPrincipal.exibirLogin(dm.conexao,'Go Software - Login',Usuario);

  lblusuarioresultado.Caption :=resultado[0];
  lblProjetoresultado.Caption :=resultado[1];

  dm.FDQuery.Close;
  dm.FDQuery.sql.Clear;
  dm.FDQuery.sql.Add('select ID FROM goprojetos WHERE DESCRICAO=:DESCRICAO');
  dm.FDQuery.ParamByName('DESCRICAO').AsString :=resultado[1];
  dm.FDQuery.Open();
  codprojeto:= dm.FDQuery.FieldByName('ID').AsInteger;



    dm.FDQuery.Close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.sql.Add('select hora from gorelatorios_automaticos ');
    dm.FDQuery.Open();
    HoraExecucaoMail := dm.FDQuery.FieldByName('hora').AsString;

end;


procedure TfPrincipal.PaineldeDisponibilidade1Click(Sender: TObject);
begin
  try
    if FileExists(ExtractFilePath(Application.ExeName) +'gopaineldisponibilidade.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gopaineldisponibilidade.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuarioresultado.Caption+' '+lblprojetoresultado.Caption+' '+inttostr(codprojeto)), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.PaineldeRisco2Click(Sender: TObject);
begin
  try
    if FileExists(ExtractFilePath(Application.ExeName) +'gopainelrisco.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gopainelrisco.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.replicar_automatico;
var arquivoini : tinifile;
    replicador : string;
begin
       arquivoini :=Tinifile.Create('C:\goassessoria\parametros.ini');
       replicador :=arquivoini.ReadString('replicador','replicador','Erro ao ler par�metro');
       if replicador='S' then
       begin
           try
              TDialogMessage.ShowWaitMessage('Replicando nuvem...', procedure
              begin
               sleep(1500);
               dm.FDSaveCloud.Close;
               dm.FDSaveCloud.sql.Clear;
               dm.FDSaveCloud.sql.Add('select * from gooverload  c where c.status=upper(''aguardando'') order by id');
               dm.FDSaveCloud.Open();

               while not dm.FDSaveCloud.Eof do
               begin
                dm.FDQuery.Close;
                dm.FDQuery.sql.Clear;
                dm.FDQuery.SQL.Add('INSERT INTO GOOVERLOAD (');
                dm.FDQuery.SQL.Add('	ID                   ');
                dm.FDQuery.SQL.Add('	,COMANDO             ');
                dm.FDQuery.SQL.Add('	,OPERACAO            ');
                dm.FDQuery.SQL.Add('	,STATUS              ');
                dm.FDQuery.SQL.Add('	,TABELA              ');
                dm.FDQuery.SQL.Add('	)                    ');
                dm.FDQuery.SQL.Add('VALUES (               ');
                dm.FDQuery.SQL.Add('	:ID                  ');
                dm.FDQuery.SQL.Add('	,:COMANDO            ');
                dm.FDQuery.SQL.Add('	,:OPERACAO           ');
                dm.FDQuery.SQL.Add('	,:STATUS             ');
                dm.FDQuery.SQL.Add('	,:TABELA             ');
                dm.FDQuery.SQL.Add('	)                    ');
                dm.FDQuery.ParamByName('ID').AsInteger      :=  dm.FDSaveCloud.FieldByName('ID').AsInteger;
                dm.FDQuery.ParamByName('COMANDO').AsString  :=  dm.FDSaveCloud.FieldByName('COMANDO').AsString;
                dm.FDQuery.ParamByName('OPERACAO').AsString :=  dm.FDSaveCloud.FieldByName('OPERACAO').AsString;
                dm.FDQuery.ParamByName('STATUS').AsString   :=  dm.FDSaveCloud.FieldByName('STATUS').AsString;
                dm.FDQuery.ParamByName('TABELA').AsString   :=  dm.FDSaveCloud.FieldByName('TABELA').AsString;
                dm.FDQuery.ExecSQL;
                dm.FDSaveCloud.Next;
               end;
              end);

               if ((not dm.FDConSaveCloud.InTransaction) and (not dm.FDSaveCloud.IsEmpty)) then
                 dm.FDConSaveCloud.StartTransaction;

               dm.FDSaveCloud.First;
               while not dm.FDSaveCloud.Eof do
               begin
                atualizarreplicador(dm.FDSaveCloud.FieldByName('id').asInteger,'online');
                dm.FDSaveCloud.Next;
               end;

               if dm.FDConSaveCloud.InTransaction then
                 dm.FDConSaveCloud.Commit
               else
                dm.FDConSaveCloud.Rollback;

              dm.FDQuery.Close;
              dm.FDQuery.sql.Clear;
              dm.FDQuery.sql.Add('select * from gooverload  c where c.status=upper(''aguardando'') order by id');
              dm.FDQuery.Open();

              while not dm.FDQuery.Eof do
              begin
                dm.FDreplicador.Close;
                dm.FDreplicador.sql.Clear;
                dm.FDreplicador.sql.Add(dm.FDQuery.FieldByName('comando').AsString);
                dm.FDreplicador.ExecSQL;
                atualizarreplicador(dm.FDQuery.FieldByName('id').asInteger,'local');
                dm.FDQuery.Next;
              end;


            //  application.MessageBox('Processo de replica��o conclu�do com sucesso.', 'Replica��o.', MB_ICONEXCLAMATION + MB_OK);

              except
               on e : exception do
               begin
                application.MessageBox(pchar('Erro ao gravar replica��o, motivo:'+e.Message),'Replicador.',MB_ICONEXCLAMATION);
                if dm.FDConSaveCloud.InTransaction then
                  dm.FDConSaveCloud.Rollback;
                end;
           end;
        end;

      if replicador='N' then
      begin
          try

              TDialogMessage.ShowWaitMessage('Replicando nuvem...', procedure
              begin
               sleep(1500);
               dm.FDQuery.Close;
               dm.FDQuery.sql.Clear;
               dm.FDQuery.sql.Add('select * from gooverload  c where c.status=upper(''aguardando'') order by id');
               dm.FDQuery.Open();

               if ((not dm.FDConSaveCloud.InTransaction) and (not dm.FDQuery.IsEmpty)) then
                 dm.FDConSaveCloud.StartTransaction;

               while not dm.FDQuery.Eof do
               begin
                dm.FDSaveCloud.Close;
                dm.FDSaveCloud.sql.Clear;
                dm.FDSaveCloud.SQL.Add('INSERT INTO GOOVERLOAD (');
                dm.FDSaveCloud.SQL.Add('	ID                   ');
                dm.FDSaveCloud.SQL.Add('	,COMANDO             ');
                dm.FDSaveCloud.SQL.Add('	,OPERACAO            ');
                dm.FDSaveCloud.SQL.Add('	,STATUS              ');
                dm.FDSaveCloud.SQL.Add('	,TABELA              ');
                dm.FDSaveCloud.SQL.Add('	)                    ');
                dm.FDSaveCloud.SQL.Add('VALUES (               ');
                dm.FDSaveCloud.SQL.Add('	:ID                  ');
                dm.FDSaveCloud.SQL.Add('	,:COMANDO            ');
                dm.FDSaveCloud.SQL.Add('	,:OPERACAO           ');
                dm.FDSaveCloud.SQL.Add('	,:STATUS             ');
                dm.FDSaveCloud.SQL.Add('	,:TABELA             ');
                dm.FDSaveCloud.SQL.Add('	)                    ');
                dm.FDSaveCloud.ParamByName('ID').AsInteger      :=  dm.FDQuery.FieldByName('ID').AsInteger;
                dm.FDSaveCloud.ParamByName('COMANDO').AsString  :=  dm.FDQuery.FieldByName('COMANDO').AsString;
                dm.FDSaveCloud.ParamByName('OPERACAO').AsString :=  dm.FDQuery.FieldByName('OPERACAO').AsString;
                dm.FDSaveCloud.ParamByName('STATUS').AsString   :=  dm.FDQuery.FieldByName('STATUS').AsString;
                dm.FDSaveCloud.ParamByName('TABELA').AsString   :=  dm.FDQuery.FieldByName('TABELA').AsString;
                dm.FDSaveCloud.ExecSQL;
                atualizarreplicador(dm.FDQuery.FieldByName('id').asInteger, 'local');
                dm.FDQuery.Next;
               end;


               if dm.FDConSaveCloud.InTransaction then
                 dm.FDConSaveCloud.Commit
                else
                dm.FDConSaveCloud.Rollback;


              end);

               application.MessageBox('Processo de replica��o conclu�do com sucesso.', 'Replica��o.', MB_ICONEXCLAMATION + MB_OK);

              except
               on e : exception do
               begin
                application.MessageBox(pchar('Erro ao gravar replica��o, motivo:'+e.Message),'Replicador.',MB_ICONEXCLAMATION);
               end;
           end;
      end;
end;

procedure TfPrincipal.criarfavorito(Caption:string; top,left,height,width:integer; onClick:TNotifyEvent;  parent:TWinControl);
var
btn:TSpeedButton;
pnl:TPanel;

begin



  pnl:=TPanel.Create(parent);
  pnl.Parent:=parent;
  pnl.ParentBackground:=false;
  pnl.Top:=top;
  pnl.Left:= left;
  pnl.Height:=height;
  pnl.Width:=width;
  pnl.Caption:='';
  pnl.Color:= $00441E13;
  pnl.BevelOuter:= bvNone;
  pnl.Align:=alLeft;
  pnl.AlignWithMargins := true;
  pnl.Margins.Left   := 40;
  pnl.Margins.Top    := 20;



  btn:=TSpeedButton.Create(parent);
  btn.Parent:=pnl;
  btn.Flat:= true;
  btn.Align:=alclient;
  btn.Caption:=caption;
  btn.Font.Color:= clWhite;
  btn.Font.Style := [fsBold];
  btn.Font.Size:=8;
  btn.Margin := 0;
  btn.Layout:= blGlyphLeft;
  btn.Font.Color:= Clwhite;
  btn.OnClick:=OnClick;
  btn.OnMouseDown:= SpeedButton1MouseDown;
  btn.Cursor:= crHandPoint;

end;

procedure TfPrincipal.cA1Click(Sender: TObject);
begin
try
    if FileExists(ExtractFilePath(Application.ExeName) +'gocarteira.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gocarteira.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);

   finally
   end;
end;

procedure TfPrincipal.CadastrodeBanco1Click(Sender: TObject);
begin
 try
    if FileExists(ExtractFilePath(Application.ExeName) +'usuario.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'usuario.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);

   finally
   end;
end;

procedure TfPrincipal.CadastrodeCNAE1Click(Sender: TObject);
begin
 try
    if FileExists(ExtractFilePath(Application.ExeName) +'gocnae.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gocnae.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);

   finally
   end;
end;

procedure TfPrincipal.CadastrodeConsultor1Click(Sender: TObject);
begin
  try
    if FileExists(ExtractFilePath(Application.ExeName) +'gosupervisor.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gosupervisor.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.CadastrodeFilial1Click(Sender: TObject);
begin
   try
    if FileExists(ExtractFilePath(Application.ExeName) +'gofilial.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gofilial.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.CadastrodePortador1Click(Sender: TObject);
begin
try
    if FileExists(ExtractFilePath(Application.ExeName) +'goportador.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'goportador.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.CadastrodeProjetos1Click(Sender: TObject);
begin
  try
    if FileExists(ExtractFilePath(Application.ExeName) +'goprojeto.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'goprojeto.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.CadastrodeUsurios1Click(Sender: TObject);
begin

 try
    if FileExists(ExtractFilePath(Application.ExeName) +'usuario.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'usuario.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);

   finally
   end;
end;

procedure TfPrincipal.ControldedeAcesso1Click(Sender: TObject);
begin

  try
    if FileExists(ExtractFilePath(Application.ExeName) +'permissao_usuario.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'permissao_usuario.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.ControledeRecebveis2Click(Sender: TObject);
begin
  try
    if FileExists(ExtractFilePath(Application.ExeName) +'gorecebiveis.exe') then
    begin
      ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName)+'gorecebiveis.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuario.Caption), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
   finally
   end;
end;

procedure TfPrincipal.criarbtn(Caption:string; top,left,height,width:integer; onClick:TNotifyEvent;  parent:TWinControl; modulo:String; executavel:String);
var
btn:TSpeedButton;
pnl:TPanel;
imagem: TImage;
edt: TLabel;
edtmodulo:TLabel;
cor: string;
begin


  if modulo <> cdsrotinaDESCRICAO.AsString  then
  begin
  edtmodulo := TLabel.Create(parent);
  //edt.Brush.Style:=bsclear;
  edtmodulo.Caption :=  cdsrotinaDESCRICAO.AsString;
  edtmodulo.Font.Color :=clWhite;
  edtmodulo.Font.Name := 'Segoe MDL2 Assets';
  edtmodulo.Font.Size := 20;
  edtmodulo.Font.Style := [fsBold];
  edtmodulo.Alignment := taLeftJustify;
  edtmodulo.Parent:=pnlbotoes;
  edtmodulo.Align:=alTop;
  edtmodulo.Left:=0;
  edtmodulo.Top:=20;
  edtmodulo.Height:=29 ;
  edtmodulo.Width:=5000;
  edtmodulo.AlignWithMargins := true;
  edtmodulo.Margins.Left   := 40;
  edtmodulo.Margins.Top    := 20;
  end;

  CASE cdsrotinaID_MODULO.AsInteger  of
    1: cor := '$00BC5B1E';
    2: cor := '$00BC5B1E';
    3: cor := '$00BC5B1E';
    4: cor := '$00BC5B1E';
    5: cor := '$00BC5B1E';
    6: cor := '$00BC5B1E';
    7: cor := '$00606060';
    8: cor := '$00606060';

  END;


  pnl:=TPanel.Create(parent);
  pnl.Parent:=parent;
  pnl.ParentBackground:=false;
  pnl.Top:=top;
  pnl.Left:= left;
  pnl.Height:=height;
  pnl.Width:=width;
  pnl.Caption:='';
  pnl.Color:= StrToInt(cor);
  pnl.BevelOuter:= bvNone;
  pnl.Align:=alLeft;
  pnl.AlignWithMargins := true;
  pnl.Margins.Left   := 40;
  pnl.Margins.Top    := 20;

  imagem := TImage.Create(parent);
  imagem.Center:=true;
  imagem.Stretch := True;
  imagem.Proportional:=true;
  imagem.Parent:=pnl;
  imagem.Align:=alCustom;
  imagem.Height:=54;
  imagem.Width:=68;
  imagem.Left:=0;
  imagem.Top:=25;
  imagem.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + '\iconebotao.png');

  {edt := TLabel.Create(parent);
  //edt.Brush.Style:=bsclear;
  edt.Caption := caption ;
  edt.Font.Color :=clWhite;
  edt.Font.Name := 'Segoe MDL2 Assets';
  edt.Font.Size := 11;
  edt.Font.Style := [fsBold];
  edt.Alignment := taRightJustify;
  edt.Parent:=pnl;
  edt.Align:=alCustom;
  edt.Left:=0;
  edt.Top:=15;
  edt.Height:=height ;
  edt.Width:=width - 10;}





  btn:=TSpeedButton.Create(parent);
  btn.Parent:=pnl;
  btn.Flat:= true;
  btn.Align:=alclient;
  btn.Caption:=caption;
  btn.Font.Color:= StrToInt(cor);
  btn.Margin:= 4 ;
  btn.Font.Style := [fsBold];
  btn.Font.Size:=8;
  btn.Layout:= blGlyphRight;
  btn.Font.Color:= Clwhite;
  btn.OnClick:=OnClick;
  btn.OnMouseDown:= SpeedButton1MouseDown;
  btn.Cursor:= crHandPoint;
  btn.Name:= executavel;

end;
procedure TfPrincipal.SpeedButton1Click(sender: TObject);
var
 versao_executavel_servidor, versao_executavel_local, unidade_mapeada : string;
 arquivo: tinifile;
begin
    {
     try
        fncmenu.KillTask(TButton(sender).Name+'.exe');
        if FileExists(ExtractFilePath(Application.ExeName)+'executaveis\'+TButton(sender).Name + '.exe') then
        begin
          ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName) +'executaveis\'+TButton(sender).Name + '.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuarioresultado.Caption+' '+inttostr(codprojeto)), nil, SW_SHOWNORMAL);
          //ShellExecute( Self.Handle, 'open', PChar(ExtractFilePath(Application.ExeName) +'executaveis\'+TButton(sender).Name + '.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']), nil, SW_SHOWNORMAL);
        end
        else
         Application.MessageBox('Rotina n�o localizada na raiz do sistema!','Rotina',MB_OK+MB_ICONINFORMATION);
       finally
       end;
     }

  try
    versao_executavel_local    :=EmptyStr;
    versao_executavel_servidor :=EmptyStr;

     TDialogMessage.ShowWaitMessage('Verificando atualiza��es...',
     procedure
     begin
        versao_executavel_local    :=EmptyStr;
        versao_executavel_servidor :=EmptyStr;

        arquivo      :=Tinifile.Create(GetCurrentDir+'\parametros.ini');
        caminho_projeto_remoto:=arquivo.ReadString('local2','propriedade2','Erro ao ler par�metro');
        caminho_projeto_local :=arquivo.ReadString('local3','propriedade3','Erro ao ler par�metro');
        unidade_mapeada                  :=arquivo.ReadString('MAPA','propriedade5','Erro ao ler par�metro');

        fncmenu.KillTask(TButton(sender).Name+'.exe');

        ShellExecute(handle, 'Open',pchar('cmd'),pchar('/K net use E:'+unidade_mapeada),nil,0);

        versao_executavel_local := fncmenu.recuperarVersaoExecutavel(caminho_projeto_local+TButton(sender).Name + '.exe')  ;
        versao_executavel_servidor := fncmenu.recuperarVersaoExecutavel(caminho_projeto_remoto+TButton(sender).Name + '.exe');
     end);


   if versao_executavel_servidor<>versao_executavel_local then
   begin
    if FileExists(caminho_projeto_remoto+TButton(sender).Name + '.exe') then
    begin
      if not FileExists(caminho_projeto_local+TButton(sender).Name + '.exe') then
      begin

        TDialogMessage.ShowWaitMessage('Atualizando vers�o...',
        procedure
        begin
          if CopyFile(pchar(caminho_projeto_remoto+TButton(sender).Name + '.exe'), pchar(caminho_projeto_local+TButton(sender).Name + '.exe'), True) then
                application.MessageBox('Vers�o atualizada com sucesso.','Atualiza��es',MB_OK+MB_ICONINFORMATION);
        end);
      end
      else
        begin
           if MessageDlg('Foi encontrado uma vers�o local anterior, deseja atualizar para uma nova vers�o?', mtConfirmation,[mbyes,mbno],0)=mryes then
           begin
            TDialogMessage.ShowWaitMessage('Atualizando vers�o local...',
            procedure
            begin

              if deletefile(pchar(caminho_projeto_local+TButton(sender).Name + '.exe')) then
              if CopyFile(pchar(caminho_projeto_remoto+TButton(sender).Name + '.exe'), pchar(caminho_projeto_local+TButton(sender).Name + '.exe'), True) then
              application.MessageBox('Vers�o atualizada com sucesso.','Atualiza��es',MB_OK+MB_ICONINFORMATION);
            end);
           end
           else
              application.MessageBox('vers�o n�o atualizada.','Atualiza��es',MB_ICONERROR);
        end;
    end;
   end;

   finally
     freeandnil(arquivo);
   end;
   TDialogMessage.ShowWaitMessage('Carregando rotina...',
   procedure
   begin
    if FileExists(caminho_projeto_local+TButton(sender).Name + '.exe') then
    begin
        ShellExecute( Self.Handle, 'open', PChar(caminho_projeto_local+TButton(sender).Name + '.exe'), pWidechar(dm.conexao.Params.Values['User_Name']+' '+dm.conexao.Params.Values['password']+' '+lblusuarioresultado.Caption+' '+inttostr(codprojeto)), nil, SW_SHOWNORMAL);
    end
    else
     Application.MessageBox('vers�o inexistente no diret�rio parametrizado!','Atualiza��es',MB_OK+MB_ICONINFORMATION);
  end);


end;

procedure TfPrincipal.SpeedButton1MouseDown(Sender: TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    If(Shift = [ssRight])Then
    Begin
       fav := TButton(sender).Caption;
       PopupMenu1.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
    End;
end;

procedure TfPrincipal.TimerTimer(Sender: TObject);
begin
  //Executa backup de acordo com o filtro
  if (TimeToStr(Time) = HoraExecucaoMail) then
  begin
   //replicar_automatico;
     spbemail.Click;
  end;
end;

procedure TfPrincipal.Verificaratualizaes1Click(Sender: TObject);
  var parametros : TStringList;
  versao_executavel_servidor, versao_executavel_local, MAPA : string;
  arquivo: tinifile;
  caminho_projeto_1, caminho_projeto_2, rotina : string;
begin
      dm.FDQuery.Close;
      dm.FDQuery.sql.Clear;
      dm.FDQuery.sql.Add('SELECT UPPER(GOROTINA.DESCRICAO)DESCRICAO              ');
      dm.FDQuery.sql.Add('FROM GOROTINA                                          ');
      dm.FDQuery.sql.Add('INNER JOIN GOMODULO ON GOMODULO.ID = GOROTINA.ID_MODULO');
      dm.FDQuery.sql.Add('WHERE MENU = ''S''                                      ');
      dm.FDQuery.sql.Add('ORDER BY ID_MODULO                                     ');
      dm.FDQuery.Open();

      while not  dm.FDQuery.Eof  do
      begin
        rotina  := dm.FDQuery.FieldByName('descricao').AsString+'.exe';
        fncmenu.KillTask(rotina);
        versao_executavel_local    :=EmptyStr;
        versao_executavel_servidor :=EmptyStr;

        arquivo            :=tinifile.Create('C:\goassessoria\parametros.ini');
        caminho_projeto_1  :=arquivo.ReadString('local2','propriedade2','Erro ao ler par�metro');
        caminho_projeto_2  :=arquivo.ReadString('local3','propriedade3','Erro ao ler par�metro');
        MAPA               :=arquivo.ReadString('MAPA','propriedade5','Erro ao ler par�metro');

        ShellExecute(handle, 'Open',pchar('cmd'),pchar('/K net use Z:'+MAPA),nil,0);

        versao_executavel_local    := fncmenu.recuperarVersaoExecutavel(caminho_projeto_2+'\executaveis\'+rotina)  ;
        versao_executavel_servidor := fncmenu.recuperarVersaoExecutavel(caminho_projeto_1+'executaveis\'+rotina);

         if versao_executavel_servidor<>versao_executavel_local then
         begin
          if FileExists(caminho_projeto_1+'executaveis\'+rotina) then
          begin
            if not FileExists(caminho_projeto_2+'\executaveis\'+rotina) then
            begin

            TDialogMessage.ShowWaitMessage('Atualizando vers�o...',
            procedure
            begin
//              if CopyFile(pchar(caminho_projeto_1+'executaveis\'+rotina), pchar(caminho_projeto_2+'\executaveis\'+rotina), True) then
//                application.MessageBox('Rotina atualizada com sucesso.','Projeto',MB_OK+MB_ICONINFORMATION);
              CopyFile(pchar(caminho_projeto_1+'executaveis\'+rotina), pchar(caminho_projeto_2+'\executaveis\'+rotina), True);
            end);
            end
            else
            begin
                TDialogMessage.ShowWaitMessage('Substituindo arquivo local...',
                procedure
                begin
                  if deletefile(pchar(caminho_projeto_2+'\executaveis\'+rotina)) then
                    CopyFile(pchar(caminho_projeto_1+'executaveis\'+rotina), pchar(caminho_projeto_2+'\executaveis\'+rotina), True);
//                  if CopyFile(pchar(caminho_projeto_1+'executaveis\'+rotina), pchar(caminho_projeto_2+'\executaveis\'+rotina), True) then
//                  application.MessageBox('Arquivo substitu�do com sucesso.','Projeto',MB_OK+MB_ICONINFORMATION);

                end);
            end;
          end;
         end;
        dm.FDQuery.next;
      end;
//     try
//        if (fprocessoatualizacao = nil) then Application.CreateForm(Tfprocessoatualizacao, fprocessoatualizacao);
//        parametros     := TStringList.Create;
//        parametros.Add('assessoria.orgfree.com');  {host}
//        parametros.Add('assessoria.orgfree.com');  {user}
//        parametros.Add('Nkv4415');  {senha}
//        parametros.Add('21');  {porta}
//        parametros.Add(ParamStr(1)); {usuario banco}
//        parametros.Add(ParamStr(2));  {senha banco}
//        parametros.Add(ParamStr(3));  {usuario}
//        parametros.Add(ParamStr(4));  {projeto}
//
//        fprocessoatualizacao.Parametros:= parametros;
//        fprocessoatualizacao.Show;
//
//    finally
//
//    end;


end;

procedure TfPrincipal.sessao;
begin
    try
    dm.conexao.Close;
   // dm.conexao.Params.Values['Database']   := 'C:\goAssessoria\db\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
   // dm.fdConexao.Params.Values['Database']   := 'C:\goAssessoria\db\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

  procedure TfPrincipal.atualizarreplicador(id: integer; tipo:string);
  begin
    if tipo='online' then
    begin
      dm.FDreplicadorover.Close;
      dm.FDreplicadorover.sql.Clear;
      dm.FDreplicadorover.SQL.Add('update gooverload set status=:status where id=:id');
      dm.FDreplicadorover.ParamByName('status').AsString:='Concluido';
      dm.FDreplicadorover.ParamByName('id').asInteger:=id;
      dm.FDreplicadorover.ExecSQL;
    end;

    if tipo='local' then
    begin
      dm.FDreplicadorlocal.Close;
      dm.FDreplicadorlocal.sql.Clear;
      dm.FDreplicadorlocal.SQL.Add('update gooverload set status=:status where id=:id');
      dm.FDreplicadorlocal.ParamByName('status').AsString:='Concluido';
      dm.FDreplicadorlocal.ParamByName('id').asInteger:=id;
      dm.FDreplicadorlocal.ExecSQL;
    end;
  end;

procedure TfPrincipal.spbemailClick(Sender: TObject);
var  mail : Tmail;
begin

 /// if Application.MessageBox('Deseja realmente disparar Email??','Email',MB_YESNO+MB_ICONQUESTION)=IDNO then abort;

   with dm do
   begin
     FDRecebivelFundo.Close;
     FDRecebivelFundo.Open();

    if  not FDRecebivelFundo.IsEmpty then
   	 begin
       FDMAIL.Close;
       FDMAIL.Open();

       try
         dm.frxExportPDF.FileName:= FDMailCAMINHO_PASTA.AsString+'\recebiveis_portadorProjeto.pdf';
         dm.frxanaliseportadorprojeto.PrintOptions.ShowDialog:=false;
         dm.frxanaliseportadorprojeto.PrepareReport();
         dm.frxanaliseportadorprojeto.Export(dm.frxExportPDF);
       except
        on E: EConvertError do
        begin
        // Trate o erro EConvertError aqui
        end;
       end;

       try
         dm.frxExportPDF.FileName:= FDMailCAMINHO_PASTA.AsString+'\recebiveis_portador.pdf';
         dm.frxanaliseportador.PrintOptions.ShowDialog:=false;
         dm.frxanaliseportador.PrepareReport();
         dm.frxanaliseportador.Export(dm.frxExportPDF);
       except
       end;
       try
         dm.frxExportPDF.FileName:= FDMailCAMINHO_PASTA.AsString+'\recebiveis_Projeto.pdf';
         dm.frxanaliseprojeto.PrintOptions.ShowDialog:=false;
         dm.frxanaliseprojeto.PrepareReport();
         dm.frxanaliseprojeto.Export(dm.frxExportPDF);
       except
       end;

       {recebiveis_portadorProjeto}
       try
         mail            := Tmail.Create;
         mail.porta      :=465;
         mail.host       :='smtp.gmail.com';
         mail.username         :='adm1gosoftware@gmail.com';
         mail.senha            :='jsnwsezjulpljwgx';
         mail.remetente  := 'goAssessoria';
         mail.nome_remetente:='Turn Assessoria';
         mail.assunto       :='Relat�rios di�rios de CONTROLE DE RECEB�VEL-'+DateToStr(Date);
         mail.corpo         := 'Segue anexado relat�rio di�rio de Controlde de Receb�vel.';
         mail.anexo         := FDMailCAMINHO_PASTA.AsString+'\recebiveis_portadorProjeto.pdf';
         mail.destinatario  := FDMailEMAIL1.AsString;
         //mail.copia         := 'natalia@turnassessoria.com.br';
         //mail.copia         := 'pierre@turnassessoria.com.br';
         mail.copia         :=  FDMailEMAIL2  .AsString;
        // mail.copia_oculta  := 'natalia@turnassessoria.com.br';

        if not fncmenu.enviar_email(mail) then
         application.MessageBox('Problema ao disparar email autom�tico','Email autom�tico',MB_ICONERROR);
        finally
          freeandnil(mail);
         if FileExists(FDMailCAMINHO_PASTA.AsString+'\recebiveis_portadorProjeto.pdf') then
          DeleteFile(FDMailCAMINHO_PASTA.AsString+'\recebiveis_portadorProjeto.pdf');
        end;

        {recebiveis_portador}
         try
         mail  := Tmail.Create;

         mail.porta      :=465;
         mail.host       :='smtp.gmail.com';
         mail.username         :='adm1gosoftware@gmail.com';
         mail.senha            :='jsnwsezjulpljwgx';
         mail.remetente  := 'goAssessoria';
         mail.nome_remetente:='Turn Assessoria';
         mail.assunto       :='Relat�rios di�rios de CONTROLE DE RECEB�VEL-'+DateToStr(Date);
         mail.corpo         := 'Segue anexado relat�rio di�rio de Controlde de Receb�vel.';
         mail.anexo         := FDMailCAMINHO_PASTA.AsString+'\recebiveis_portador.pdf';
         mail.destinatario  := FDMailEMAIL1.AsString;
         //mail.copia         := 'natalia@turnassessoria.com.br';
         //mail.copia         := 'pierre@turnassessoria.com.br';
         mail.copia         := FDMailEMAIL2.AsString;
         //mail.copia_oculta  := 'natalia@turnassessoria.com.br';

        if not fncmenu.enviar_email(mail) then
         application.MessageBox('Problema ao disparar email autom�tico','Email autom�tico',MB_ICONERROR);
        finally
          freeandnil(mail);
         if FileExists(FDMailCAMINHO_PASTA.AsString+'\recebiveis_portador.pdf') then
          DeleteFile(FDMailCAMINHO_PASTA.AsString+'\recebiveis_portador.pdf');
        end;

        {recebiveis_Projeto}
         try
         mail  := Tmail.Create;

         mail.porta      :=465;
         mail.host       :='smtp.gmail.com';
         mail.username         :='adm1gosoftware@gmail.com';
         mail.senha            :='jsnwsezjulpljwgx';
         mail.remetente  := 'goAssessoria';
         mail.nome_remetente:='Turn Assessoria';
         mail.assunto       :='Relat�rios di�rios de CONTROLE DE RECEB�VEL-'+DateToStr(Date);
         mail.corpo         := 'Segue anexado relat�rio di�rio de Controlde de Receb�vel.';
         mail.anexo         := FDMailCAMINHO_PASTA.AsString+'recebiveis_Projeto.pdf';
         mail.destinatario  := FDMailEMAIL1.AsString;
         //mail.copia         := 'natalia@turnassessoria.com.br';
         //mail.copia         := 'pierre@turnassessoria.com.br';
         mail.copia         := FDMailEMAIL2.AsString;
       //  mail.copia_oculta  := 'natalia@turnassessoria.com.br';

        if not fncmenu.enviar_email(mail) then
         application.MessageBox('Problema ao disparar email autom�tico','Email autom�tico',MB_ICONERROR);
        finally
          freeandnil(mail);
         if FileExists(FDMailCAMINHO_PASTA.AsString+'\recebiveis_Projeto.pdf') then
          DeleteFile(FDMailCAMINHO_PASTA.AsString+'\recebiveis_Projeto.pdf');
        end;

     end;

     FDProjetosDisponibilidade.Close;
     FDProjetosDisponibilidade.Open();

     while not FDProjetosDisponibilidade.Eof do
     begin
       FDDisponibilidade.Close;
       FDDisponibilidade.ParamByName('dtcompetencia').AsDate:=FDProjetosDisponibilidadeDTCOMPETENCIA.asDatetime;
       FDDisponibilidade.ParamByName('codprojeto').asinteger:=FDProjetosDisponibilidadecodprojeto.asInteger;
       FDDisponibilidade.Open();

       if  not FDDisponibilidade.IsEmpty then
       begin
         FDMAIL.Close;
         FDMAIL.Open();

         dm.frxExportPDF.FileName:= FDMailCAMINHO_PASTA.AsString+ FDDisponibilidadePROJETO.AsString+'_'+'relatorio_disponibilidade.pdf';
         dm.frxgodisponibilidade.PrintOptions.ShowDialog:=false;
         dm.frxgodisponibilidade.PrepareReport();
         dm.frxgodisponibilidade.Export(dm.frxExportPDF);

         try
           mail  := Tmail.Create;
           mail.porta      :=465;
           mail.host       :='smtp.gmail.com';
           mail.username         :='adm1gosoftware@gmail.com';
           mail.senha            :='jsnwsezjulpljwgx';
           mail.remetente  := 'goAssessoria';
           mail.nome_remetente:='Turn Assessoria';
           mail.assunto       :='Relat�rios di�rios do PAINEL DE DISPONIBILIDADE '+FDDisponibilidadePROJETO.AsString+'/'+FDDisponibilidadeDTCOMPETENCIA.AsString;
           mail.corpo         := 'Segue anexado relat�rio di�rio do PAINEL DE DISPONIBILIDADE.';
           mail.anexo         := FDMailCAMINHO_PASTA.AsString+ FDDisponibilidadePROJETO.AsString+'_'+'relatorio_disponibilidade.pdf';
           mail.destinatario  := FDMailEMAIL1.AsString;
          // mail.copia         := 'natalia@turnassessoria.com.br';
           mail.copia         :=  FDMailEMAIL2.AsString;
          // mail.copia_oculta  := 'waldes.reddata@gmail.com';

          if not fncmenu.enviar_email(mail) then
           application.MessageBox('Problema ao disparar email autom�tico','Email autom�tico',MB_ICONERROR);
          finally
            freeandnil(mail);
           if FileExists(FDMailCAMINHO_PASTA.AsString+ FDDisponibilidadePROJETO.AsString+'_'+'relatorio_disponibilidade.pdf') then
            DeleteFile(FDMailCAMINHO_PASTA.AsString+ FDDisponibilidadePROJETO.AsString+'_'+'relatorio_disponibilidade.pdf');
          end;

       end;
       FDProjetosDisponibilidade.Next;
     end;


          // relatorios de faturamento//
          FDMAIL.Close;
          FDMAIL.Open();
          FDQuerygoprojetofilial.Close;
          FDQuerygoprojetofilial.Open();
          while not FDQuerygoprojetofilial.Eof do
          begin

            CDSFaturamentoGrupo.Close;
            FDFaturamentoGrupo.Close;

            FDFaturamentoGrupo.SQL.Clear;
            FDFaturamentoGrupo.SQL.Add('SELECT                                                                  ');
            FDFaturamentoGrupo.SQL.Add('    codprojeto,                                                         ');
            FDFaturamentoGrupo.SQL.Add('    projeto,                                                            ');
            FDFaturamentoGrupo.SQL.Add('    CODFILIAL,                                                          ');
            FDFaturamentoGrupo.SQL.Add('    FILIAL,                                                             ');
            FDFaturamentoGrupo.SQL.Add('    dt_lancamento,                                                      ');
            FDFaturamentoGrupo.SQL.Add('    CASE                                                                ');
            FDFaturamentoGrupo.SQL.Add('        WHEN DIA_SEMANA IN (''DOMINGO'', ''S�BADO'') THEN               ');
            FDFaturamentoGrupo.SQL.Add('    ''Final de Semana''                                                 ');
            FDFaturamentoGrupo.SQL.Add('        ELSE                                                            ');
            FDFaturamentoGrupo.SQL.Add('    ''Dia da Semana''                                                   ');
            FDFaturamentoGrupo.SQL.Add('    END Dia,                                                            ');
            FDFaturamentoGrupo.SQL.Add('    dia_semana,                                                         ');
            FDFaturamentoGrupo.SQL.Add('    descricao_dia_util,                                                 ');
            FDFaturamentoGrupo.SQL.Add('    GRUPO_PRODUTO,                                                      ');
            FDFaturamentoGrupo.SQL.Add('    meta,                                                               ');
            FDFaturamentoGrupo.SQL.Add('    ALIQUOTA,                                                           ');
            FDFaturamentoGrupo.SQL.Add('    vlfaturado,                                                         ');
            FDFaturamentoGrupo.SQL.Add('    TOTAL_UTEIS_YERSTEDAY,                                              ');
            FDFaturamentoGrupo.SQL.Add('    TOTAL_UTEIS_TOMORROW                                                ');
            FDFaturamentoGrupo.SQL.Add('FROM                                                                    ');
            FDFaturamentoGrupo.SQL.Add('    (                                                                   ');
            FDFaturamentoGrupo.SQL.Add('    SELECT                                                              ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO.codprojeto,                                       ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO.projeto,                                          ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO.codfilial,                                        ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO.filial,                                           ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO.DT_LANCAMENTO                                     ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO.DIA_SEMANA                                        ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        gofaturamento_dia_util.dia_util descricao_dia_util              ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        COALESCE(GOFATURAMENTO.GRUPO_PRODUTO,                           ');
            FDFaturamentoGrupo.SQL.Add('        ''SEM CLASSIFICA��O'') GRUPO_PRODUTO                            ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        goparametros.valor AS meta                                      ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        gogrupoproduto.meta AS ALIQUOTA                                 ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        sum(GOFATURAMENTO.VLFATURADO) VLFATURADO                        ');
            FDFaturamentoGrupo.SQL.Add('        ,                                                               ');
            FDFaturamentoGrupo.SQL.Add('        (                                                               ');
            FDFaturamentoGrupo.SQL.Add('        SELECT                                                          ');
            FDFaturamentoGrupo.SQL.Add('            count(*)                                                    ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            GOFATURAMENTO_DIA_UTIL                                      ');
            FDFaturamentoGrupo.SQL.Add('        WHERE                                                           ');
            FDFaturamentoGrupo.SQL.Add('            GOFATURAMENTO_DIA_UTIL.ID_FILIAL =:ID_FILIAL                ');
            FDFaturamentoGrupo.SQL.Add('            AND DATA BETWEEN (                                          ');
            FDFaturamentoGrupo.SQL.Add('            SELECT                                                      ');
            FDFaturamentoGrupo.SQL.Add('                CAST(current_date AS date) - EXTRACT(DAY                ');
            FDFaturamentoGrupo.SQL.Add('            FROM                                                        ');
            FDFaturamentoGrupo.SQL.Add('                CAST(current_date AS date)) + 1                         ');
            FDFaturamentoGrupo.SQL.Add('            FROM                                                        ');
            FDFaturamentoGrupo.SQL.Add('                RDB$DATABASE) AND CURRENT_DATE-1                        ');
            FDFaturamentoGrupo.SQL.Add('            AND DIA_UTIL = ''SIM''                                      ');
            FDFaturamentoGrupo.SQL.Add('      ) TOTAL_UTEIS_YERSTEDAY                                           ');
            FDFaturamentoGrupo.SQL.Add('  ,                                                                     ');
            FDFaturamentoGrupo.SQL.Add('        (                                                               ');
            FDFaturamentoGrupo.SQL.Add('        SELECT                                                          ');
            FDFaturamentoGrupo.SQL.Add('            count(*)                                                    ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            GOFATURAMENTO_DIA_UTIL                                      ');
            FDFaturamentoGrupo.SQL.Add('        WHERE                                                           ');
            FDFaturamentoGrupo.SQL.Add('            GOFATURAMENTO_DIA_UTIL.ID_FILIAL =:ID_FILIAL                ');
            FDFaturamentoGrupo.SQL.Add('            AND DATA BETWEEN CURRENT_DATE  AND (                        ');
            FDFaturamentoGrupo.SQL.Add('            SELECT                                                      ');
            FDFaturamentoGrupo.SQL.Add('                CAST(current_date AS date) - EXTRACT(DAY                ');
            FDFaturamentoGrupo.SQL.Add('            FROM                                                        ');
            FDFaturamentoGrupo.SQL.Add('                CAST(current_date AS date)) + 32 - EXTRACT(DAY          ');
            FDFaturamentoGrupo.SQL.Add('            FROM                                                        ');
            FDFaturamentoGrupo.SQL.Add('                (CAST(current_date AS date) - EXTRACT(DAY               ');
            FDFaturamentoGrupo.SQL.Add('            FROM                                                        ');
            FDFaturamentoGrupo.SQL.Add('                CAST(current_date AS date)) + 32))                      ');
            FDFaturamentoGrupo.SQL.Add('            FROM                                                        ');
            FDFaturamentoGrupo.SQL.Add('                RDB$DATABASE)                                           ');
            FDFaturamentoGrupo.SQL.Add('            AND DIA_UTIL = ''SIM''                                      ');
            FDFaturamentoGrupo.SQL.Add('      ) TOTAL_UTEIS_TOMORROW                                            ');
            FDFaturamentoGrupo.SQL.Add('    FROM                                                                ');
            FDFaturamentoGrupo.SQL.Add('        GOFATURAMENTO,                                                  ');
            FDFaturamentoGrupo.SQL.Add('        goparametros,                                                   ');
            FDFaturamentoGrupo.SQL.Add('        gogrupoproduto,                                                 ');
            FDFaturamentoGrupo.SQL.Add('        gofaturamento_dia_util                                          ');
            FDFaturamentoGrupo.SQL.Add('    WHERE                                                               ');
            FDFaturamentoGrupo.SQL.Add('        gofaturamento.codprojeto = goparametros.id_projeto              ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.codfilial = goparametros.id_filial            ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.grupo_produto = gogrupoproduto.descricao      ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.codPROJETO = gogrupoproduto.codprojeto        ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.codfilial = gofaturamento_dia_util.id_filial  ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.dt_lancamento = gofaturamento_dia_util.data   ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.dt_lancamento BETWEEN (                       ');
            FDFaturamentoGrupo.SQL.Add('        SELECT                                                          ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date) - EXTRACT(DAY                    ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date)) + 1                             ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            RDB$DATABASE) AND                                           ');
            FDFaturamentoGrupo.SQL.Add('    (                                                                   ');
            FDFaturamentoGrupo.SQL.Add('        SELECT                                                          ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date) - EXTRACT(DAY                    ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date)) + 32 - EXTRACT(DAY              ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            (CAST(current_date AS date) - EXTRACT(DAY                   ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date)) + 32))                          ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            RDB$DATABASE)                                               ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.codfilial = :ID_FILIAL                        ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.CODPROJETO = :CODPROJETO                      ');
            FDFaturamentoGrupo.SQL.Add('        AND gofaturamento.DT_LANCAMENTO BETWEEN (                       ');
            FDFaturamentoGrupo.SQL.Add('        SELECT                                                          ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date) - EXTRACT(DAY                    ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date)) + 1                             ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            RDB$DATABASE) AND                                           ');
            FDFaturamentoGrupo.SQL.Add('    (                                                                   ');
            FDFaturamentoGrupo.SQL.Add('        SELECT                                                          ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date) - EXTRACT(DAY                    ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date)) + 32 - EXTRACT(DAY              ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            (CAST(current_date AS date) - EXTRACT(DAY                   ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            CAST(current_date AS date)) + 32))                          ');
            FDFaturamentoGrupo.SQL.Add('        FROM                                                            ');
            FDFaturamentoGrupo.SQL.Add('            RDB$DATABASE)                                               ');
            FDFaturamentoGrupo.SQL.Add('    GROUP BY 1,2,3,4,5,6,7,8,9,10)                                      ');
            FDFaturamentoGrupo.SQL.Add('ORDER BY                                                                ');
            FDFaturamentoGrupo.SQL.Add('    GRUPO_PRODUTO,                                                      ');
            FDFaturamentoGrupo.SQL.Add('    DT_LANCAMENTO                                                       ');
            FDFaturamentoGrupo.ParamByName('ID_FILIAL').AsInteger  := FDQuerygoprojetofilialCODFILIAL.AsInteger;
            FDFaturamentoGrupo.ParamByName('CODPROJETO').AsInteger := FDQuerygoprojetofilialCODPROJETO.AsInteger;
            FDFaturamentoGrupo.Open();
            CDSFaturamentoGrupo.Open;

            if not CDSFaturamentoGrupo.IsEmpty then
            begin
              try
                 dm.frxExportPDF.FileName:= FDMailCAMINHO_PASTA.AsString+'\faturamento_'+FDQuerygoprojetofilialPROJETO.asString+'_'+FDQuerygoprojetofilialFilial.AsString+'.pdf';
                 dm.frxgofaturamentogrupo.PrintOptions.ShowDialog:=false;
                 dm.frxgofaturamentogrupo.PrepareReport();
                 dm.frxgofaturamentogrupo.Export(dm.frxExportPDF);
               except
               end;


               try
                 mail            := Tmail.Create;
                 mail.porta      :=465;
                 mail.host       :='smtp.gmail.com';
                 mail.username         :='adm1gosoftware@gmail.com';
                 mail.senha            :='jsnwsezjulpljwgx';
                 mail.remetente  := 'goAssessoria';
                 mail.nome_remetente:='Turn Assessoria';
                 mail.assunto       :='Relat�rios di�rios do FATURAMENTO '+FDQuerygoprojetofilialPROJETO.AsString+'-'+FDQuerygoprojetofilialFilial.AsString;
                 mail.corpo         := 'Segue anexado relat�rio di�rio do FATURAMENTO.';
                 mail.anexo         := FDMailCAMINHO_PASTA.AsString+'\faturamento_'+FDQuerygoprojetofilialPROJETO.asString+'_'+FDQuerygoprojetofilialFilial.AsString+'.pdf';
                 mail.destinatario  := FDMailEMAIL1.AsString;
               //  mail.copia         := 'natalia@turnassessoria.com.br';
                 mail.copia         :=  FDMailEMAIL2.AsString;
                 mail.copia_oculta  := 'natalia@turnassessoria.com.br';

                if not fncmenu.enviar_email(mail) then
                 application.MessageBox('Problema ao disparar email autom�tico','Email autom�tico',MB_ICONERROR);
                finally
                  freeandnil(mail);
                 if FileExists(FDMailCAMINHO_PASTA.AsString+'\faturamento_'+FDQuerygoprojetofilialPROJETO.asString+'_'+FDQuerygoprojetofilialFilial.AsString+'.pdf') then
                  DeleteFile(FDMailCAMINHO_PASTA.AsString+'\faturamento_'+FDQuerygoprojetofilialPROJETO.asString+'_'+FDQuerygoprojetofilialFilial.AsString+'.pdf');
                end;

            end;

            FDQuerygoprojetofilial.Next;
          end;


          // relatorios de painel de risco//
          FDMAIL.Close;
          FDMAIL.Open();
          cdsgopainelrisco.Close;
          cdsgopainelrisco.Open;

          try
             dm.frxExportPDF.FileName:= FDMailCAMINHO_PASTA.AsString+'\painelrisco.pdf';
             dm.frxbase.PrintOptions.ShowDialog:=false;
             dm.frxbase.PrepareReport();
             dm.frxbase.Export(dm.frxExportPDF);
           except
           end;


           try
             mail            := Tmail.Create;
             mail.porta      :=465;
             mail.host       :='smtp.gmail.com';
             mail.username         :='adm1gosoftware@gmail.com';
             mail.senha            :='jsnwsezjulpljwgx';
             mail.remetente  := 'goAssessoria';
             mail.nome_remetente:='Turn Assessoria';
             mail.assunto       :='Relat�rios di�rios do PAINEL DE RISCO';
             mail.corpo         := 'Segue anexado relat�rio di�rio do PAINEL DE RISCO.';
             mail.anexo         := FDMailCAMINHO_PASTA.AsString+'\painelrisco.pdf';
             mail.destinatario  := FDMailEMAIL1.AsString;
             mail.copia         :=  FDMailEMAIL2.AsString;
             mail.copia_oculta  := 'natalia@turnassessoria.com.br';

            if not fncmenu.enviar_email(mail) then
             application.MessageBox('Problema ao disparar email autom�tico','Email autom�tico',MB_ICONERROR);
            finally
              freeandnil(mail);
             if FileExists(FDMailCAMINHO_PASTA.AsString+'\painelrisco.pdf') then
              DeleteFile(FDMailCAMINHO_PASTA.AsString+'\painelrisco.pdf');
            end;

  end;

end;

procedure TfPrincipal.spbreplicadorClick(Sender: TObject);
var arquivoini : tinifile;
    replicador : string;
begin
 if Application.MessageBox('Deseja realmente replicar??','Replicador',MB_YESNO+MB_ICONQUESTION)=IDYES then
 begin

       arquivoini :=Tinifile.Create('C:\goassessoria\parametros.ini');
       replicador :=arquivoini.ReadString('replicador','replicador','Erro ao ler par�metro');

       if replicador='S' then
       begin
           try
              TDialogMessage.ShowWaitMessage('Replicando nuvem...', procedure
              begin
               sleep(1500);
               dm.FDSaveCloud.Close;
               dm.FDSaveCloud.sql.Clear;
               dm.FDSaveCloud.sql.Add('select * from gooverload  c where c.status=upper(''aguardando'') order by id');
               dm.FDSaveCloud.Open();

               if dm.FDSaveCloud.IsEmpty then
                application.MessageBox('Sem registros para replicar.', 'Replica��o.', MB_ICONEXCLAMATION + MB_OK);

               while not dm.FDSaveCloud.Eof do
               begin
                dm.FDQuery.Close;
                dm.FDQuery.sql.Clear;
                dm.FDQuery.SQL.Add('INSERT INTO GOOVERLOAD (');
                dm.FDQuery.SQL.Add('	ID                   ');
                dm.FDQuery.SQL.Add('	,COMANDO             ');
                dm.FDQuery.SQL.Add('	,OPERACAO            ');
                dm.FDQuery.SQL.Add('	,STATUS              ');
                dm.FDQuery.SQL.Add('	,TABELA              ');
                dm.FDQuery.SQL.Add('	)                    ');
                dm.FDQuery.SQL.Add('VALUES (               ');
                dm.FDQuery.SQL.Add('	:ID                  ');
                dm.FDQuery.SQL.Add('	,:COMANDO            ');
                dm.FDQuery.SQL.Add('	,:OPERACAO           ');
                dm.FDQuery.SQL.Add('	,:STATUS             ');
                dm.FDQuery.SQL.Add('	,:TABELA             ');
                dm.FDQuery.SQL.Add('	)                    ');
                dm.FDQuery.ParamByName('ID').AsInteger      :=  dm.FDSaveCloud.FieldByName('ID').AsInteger;
                dm.FDQuery.ParamByName('COMANDO').AsString  :=  dm.FDSaveCloud.FieldByName('COMANDO').AsString;
                dm.FDQuery.ParamByName('OPERACAO').AsString :=  dm.FDSaveCloud.FieldByName('OPERACAO').AsString;
                dm.FDQuery.ParamByName('STATUS').AsString   :=  dm.FDSaveCloud.FieldByName('STATUS').AsString;
                dm.FDQuery.ParamByName('TABELA').AsString   :=  dm.FDSaveCloud.FieldByName('TABELA').AsString;
                dm.FDQuery.ExecSQL;
                dm.FDSaveCloud.Next;
               end;
              end);

               if ((not dm.FDConSaveCloud.InTransaction) and (not dm.FDSaveCloud.IsEmpty)) then
                 dm.FDConSaveCloud.StartTransaction;

               dm.FDSaveCloud.First;
               while not dm.FDSaveCloud.Eof do
               begin
                atualizarreplicador(dm.FDSaveCloud.FieldByName('id').asInteger,'online');
                dm.FDSaveCloud.Next;
               end;

               if dm.FDConSaveCloud.InTransaction then
                 dm.FDConSaveCloud.Commit
               else
                dm.FDConSaveCloud.Rollback;

              dm.FDQuery.Close;
              dm.FDQuery.sql.Clear;
              dm.FDQuery.sql.Add('select * from gooverload  c where c.status=upper(''aguardando'') order by id');
              dm.FDQuery.Open();

              while not dm.FDQuery.Eof do
              begin
                dm.FDreplicador.Close;
                dm.FDreplicador.sql.Clear;
                dm.FDreplicador.sql.Add(dm.FDQuery.FieldByName('comando').AsString);
                dm.FDreplicador.ExecSQL;
                atualizarreplicador(dm.FDQuery.FieldByName('id').asInteger,'local');
                dm.FDQuery.Next;
              end;


             application.MessageBox('Processo de replica��o conclu�do com sucesso.', 'Replica��o.', MB_ICONEXCLAMATION + MB_OK);

              except
               on e : exception do
               begin
                application.MessageBox(pchar('Erro ao gravar replica��o, motivo:'+e.Message),'Replicador.',MB_ICONEXCLAMATION);
                if dm.FDConSaveCloud.InTransaction then
                  dm.FDConSaveCloud.Rollback;
                end;
           end;
        end;

      if replicador='N' then
      begin
          try

              TDialogMessage.ShowWaitMessage('Replicando nuvem...', procedure
              begin
               sleep(1500);
               dm.FDQuery.Close;
               dm.FDQuery.sql.Clear;
               dm.FDQuery.sql.Add('select * from gooverload  c where c.status=upper(''aguardando'') order by id');
               dm.FDQuery.Open();

               if ((not dm.FDConSaveCloud.InTransaction) and (not dm.FDQuery.IsEmpty)) then
                 dm.FDConSaveCloud.StartTransaction;

               while not dm.FDQuery.Eof do
               begin
                dm.FDSaveCloud.Close;
                dm.FDSaveCloud.sql.Clear;
                dm.FDSaveCloud.SQL.Add('INSERT INTO GOOVERLOAD (');
                dm.FDSaveCloud.SQL.Add('	ID                   ');
                dm.FDSaveCloud.SQL.Add('	,COMANDO             ');
                dm.FDSaveCloud.SQL.Add('	,OPERACAO            ');
                dm.FDSaveCloud.SQL.Add('	,STATUS              ');
                dm.FDSaveCloud.SQL.Add('	,TABELA              ');
                dm.FDSaveCloud.SQL.Add('	)                    ');
                dm.FDSaveCloud.SQL.Add('VALUES (               ');
                dm.FDSaveCloud.SQL.Add('	:ID                  ');
                dm.FDSaveCloud.SQL.Add('	,:COMANDO            ');
                dm.FDSaveCloud.SQL.Add('	,:OPERACAO           ');
                dm.FDSaveCloud.SQL.Add('	,:STATUS             ');
                dm.FDSaveCloud.SQL.Add('	,:TABELA             ');
                dm.FDSaveCloud.SQL.Add('	)                    ');
                dm.FDSaveCloud.ParamByName('ID').AsInteger      :=  dm.FDQuery.FieldByName('ID').AsInteger;
                dm.FDSaveCloud.ParamByName('COMANDO').AsString  :=  dm.FDQuery.FieldByName('COMANDO').AsString;
                dm.FDSaveCloud.ParamByName('OPERACAO').AsString :=  dm.FDQuery.FieldByName('OPERACAO').AsString;
                dm.FDSaveCloud.ParamByName('STATUS').AsString   :=  dm.FDQuery.FieldByName('STATUS').AsString;
                dm.FDSaveCloud.ParamByName('TABELA').AsString   :=  dm.FDQuery.FieldByName('TABELA').AsString;
                dm.FDSaveCloud.ExecSQL;
                atualizarreplicador(dm.FDQuery.FieldByName('id').asInteger, 'local');
                dm.FDQuery.Next;
               end;


               if dm.FDConSaveCloud.InTransaction then
                 dm.FDConSaveCloud.Commit
                else
                dm.FDConSaveCloud.Rollback;


              end);

               application.MessageBox('Processo de replica��o conclu�do com sucesso.', 'Replica��o.', MB_ICONEXCLAMATION + MB_OK);

              except
               on e : exception do
               begin
                application.MessageBox(pchar('Erro ao gravar replica��o, motivo:'+e.Message),'Replicador.',MB_ICONEXCLAMATION);
               end;
           end;
      end;

 end;

end;

procedure TfPrincipal.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    PopupHelp.Popup(X, Y);
  end;
end;

end.
