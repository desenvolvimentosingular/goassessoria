unit ugofilial;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 07/08/2021*}                                          
{*Hora 12:11:13*}                                            
{*Unit gofilial *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxDropDownEdit, cxContainer,
  dxCore, cxDateUtils, cxTextEdit, cxMaskEdit, cxCalendar, cxButtons;

type                                                                                        
	Tfgofilial = class(TForm)
    sqlgofilial: TSQLDataSet;
	dspgofilial: TDataSetProvider;
	cdsgofilial: TClientDataSet;
	dsgofilial: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgofilial: TfrxReport;
	frxDBgofilial: TfrxDBDataset;
	Popupgofilial: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgofilial: tcxTabSheet;
	SCRgofilial: TScrollBox;
	pnlDadosgofilial: TPanel;
	{>>>bot�es de manipula��o de dados gofilial}
	pnlLinhaBotoesgofilial: TPanel;
	spdAdicionargofilial: TButton;
	spdAlterargofilial: TButton;
	spdGravargofilial: TButton;
	spdCancelargofilial: TButton;
	spdExcluirgofilial: TButton;

	{bot�es de manipula��o de dadosgofilial<<<}
	{campo ID}
	sqlgofilialID: TIntegerField;
	cdsgofilialID: TIntegerField;
	pnlLayoutLinhasgofilial1: TPanel;
	pnlLayoutTitulosgofilialid: TPanel;
	pnlLayoutCamposgofilialid: TPanel;
	dbegofilialid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgofilialDT_CADASTRO: TDateField;
	cdsgofilialDT_CADASTRO: TDateField;
	pnlLayoutLinhasgofilial2: TPanel;
	pnlLayoutTitulosgofilialdt_cadastro: TPanel;
	pnlLayoutCamposgofilialdt_cadastro: TPanel;
	dbegofilialdt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgofilialHS_CADASTRO: TTimeField;
	cdsgofilialHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgofilial3: TPanel;
	pnlLayoutTitulosgofilialhs_cadastro: TPanel;
	pnlLayoutCamposgofilialhs_cadastro: TPanel;
	dbegofilialhs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlgofilialDESCRICAO: TStringField;
	cdsgofilialDESCRICAO: TStringField;
	pnlLayoutLinhasgofilial4: TPanel;
	pnlLayoutTitulosgofilialdescricao: TPanel;
	pnlLayoutCamposgofilialdescricao: TPanel;
	dbegofilialdescricao: TDBEdit;

	{campo CNPJ}
	sqlgofilialCNPJ: TStringField;
	cdsgofilialCNPJ: TStringField;
	pnlLayoutLinhasgofilial5: TPanel;
	pnlLayoutTitulosgofilialcnpj: TPanel;
	pnlLayoutCamposgofilialcnpj: TPanel;
	dbegofilialcnpj: TDBEdit;

	{campo CEP}
	sqlgofilialCEP: TStringField;
	cdsgofilialCEP: TStringField;
	pnlLayoutLinhasgofilial6: TPanel;
	pnlLayoutTitulosgofilialcep: TPanel;
	pnlLayoutCamposgofilialcep: TPanel;
	dbegofilialcep: TDBEdit;

	{campo ENDERECO}
	sqlgofilialENDERECO: TStringField;
	cdsgofilialENDERECO: TStringField;
	pnlLayoutLinhasgofilial7: TPanel;
	pnlLayoutTitulosgofilialendereco: TPanel;
	pnlLayoutCamposgofilialendereco: TPanel;
	dbegofilialendereco: TDBEdit;

	{campo UF}
	sqlgofilialUF: TStringField;
	cdsgofilialUF: TStringField;
	pnlLayoutLinhasgofilial8: TPanel;
	pnlLayoutTitulosgofilialuf: TPanel;
	pnlLayoutCamposgofilialuf: TPanel;
	cbogofilialuf: TDBComboBox;

	{campo CIDADE}
	sqlgofilialCIDADE: TStringField;
	cdsgofilialCIDADE: TStringField;
	pnlLayoutLinhasgofilial9: TPanel;
	pnlLayoutTitulosgofilialcidade: TPanel;
	pnlLayoutCamposgofilialcidade: TPanel;
	dbegofilialcidade: TDBEdit;

	{campo CNAE}
	sqlgofilialCNAE: TStringField;
	cdsgofilialCNAE: TStringField;
	pnlLayoutLinhasgofilial10: TPanel;
	pnlLayoutTitulosgofilialcnae: TPanel;
	pnlLayoutCamposgofilialcnae: TPanel;
	dbegofilialcnae: TDBEdit;

	{campo REPRESENTANTE}
	sqlgofilialREPRESENTANTE: TStringField;
	cdsgofilialREPRESENTANTE: TStringField;
	pnlLayoutLinhasgofilial11: TPanel;
	pnlLayoutTitulosgofilialrepresentante: TPanel;
	pnlLayoutCamposgofilialrepresentante: TPanel;
	dbegofilialrepresentante: TDBEdit;

	{campo CAPITAL_SOCIAL}
	sqlgofilialCAPITAL_SOCIAL: TFloatField;
	cdsgofilialCAPITAL_SOCIAL: TFloatField;
	pnlLayoutLinhasgofilial12: TPanel;
	pnlLayoutTitulosgofilialcapital_social: TPanel;
	pnlLayoutCamposgofilialcapital_social: TPanel;
	dbegofilialcapital_social: TDBEdit;

	{campo EMAIL1}
	sqlgofilialEMAIL1: TStringField;
	cdsgofilialEMAIL1: TStringField;
	pnlLayoutLinhasgofilial13: TPanel;
	pnlLayoutTitulosgofilialemail1: TPanel;
	pnlLayoutCamposgofilialemail1: TPanel;
	dbegofilialemail1: TDBEdit;

	{campo EMAIL2}
	sqlgofilialEMAIL2: TStringField;
	cdsgofilialEMAIL2: TStringField;
	pnlLayoutLinhasgofilial14: TPanel;
	pnlLayoutTitulosgofilialemail2: TPanel;
	pnlLayoutCamposgofilialemail2: TPanel;
	dbegofilialemail2: TDBEdit;

	{campo TELEFONE1}
	sqlgofilialTELEFONE1: TStringField;
	cdsgofilialTELEFONE1: TStringField;
	pnlLayoutLinhasgofilial15: TPanel;
	pnlLayoutTitulosgofilialtelefone1: TPanel;
	pnlLayoutCamposgofilialtelefone1: TPanel;
	dbegofilialtelefone1: TDBEdit;

	{campo TELEFONE2}
	sqlgofilialTELEFONE2: TStringField;
	cdsgofilialTELEFONE2: TStringField;
	pnlLayoutLinhasgofilial16: TPanel;
	pnlLayoutTitulosgofilialtelefone2: TPanel;
	pnlLayoutCamposgofilialtelefone2: TPanel;
	dbegofilialtelefone2: TDBEdit;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    frxDBDfiltros: TfrxDBDataset;
    cdsfiltros: TClientDataSet;
    psqCNAE: TPesquisa;
    sqlgofilialCODCNAE: TStringField;
    cdsgofilialCODCNAE: TStringField;
    dbegofilialCODCNAE: TDBEdit;
    btnCNAE: TButton;
    spdduplicargofilial: TButton;
    sqlgofilialBAIRRO: TStringField;
    cdsgofilialBAIRRO: TStringField;
    tabDiasUteisfaturamento: TcxTabSheet;
    griddiasuteis: TcxGrid;
    cxGridDBTDiasUteis: TcxGridDBTableView;
    cxGridLevelDiasUteis: TcxGridLevel;
    FDDiasUteis: TFDQuery;
    dsDiasUteis: TDataSource;
    FDDiasUteisID: TIntegerField;
    FDDiasUteisID_FILIAL: TIntegerField;
    FDDiasUteisFILIAL: TStringField;
    FDDiasUteisDATA: TDateField;
    FDDiasUteisDIA_SEMANA: TStringField;
    FDDiasUteisDIA_UTIL_PADRAO: TStringField;
    FDDiasUteisDIA_UTIL: TStringField;
    cxGridDBTDiasUteisDATA: TcxGridDBColumn;
    cxGridDBTDiasUteisDIA_SEMANA: TcxGridDBColumn;
    cxGridDBTDiasUteisID_FILIAL: TcxGridDBColumn;
    cxGridDBTDiasUteisFILIAL: TcxGridDBColumn;
    cxGridDBTDiasUteisDIA_UTIL_PADRAO: TcxGridDBColumn;
    cxGridDBTDiasUteisDIA_UTIL: TcxGridDBColumn;
    pnlLinhaFiltrosDiasUteis: TPanel;
    spdFiltrarDiasUteis: TButton;
    data1: TcxDateEdit;
    data2: TcxDateEdit;
    lbldata1: TLabel;
    lbldata2: TLabel;
    cmbDiasSemana: TcxComboBox;
    lblDiassemana: TLabel;
    spbdatesintetico: TcxButton;
    popmenudata: TPopupMenu;
    Hoje: TMenuItem;
    Ontem: TMenuItem;
    SemanaAtual: TMenuItem;
    SemanaAnterior: TMenuItem;
    UltimosQuizeDias: TMenuItem;
    N1: TMenuItem;
    MesAtual: TMenuItem;
    MesAnterior: TMenuItem;
    N2: TMenuItem;
    TrimestreAtual: TMenuItem;
    TrimestreAnterior: TMenuItem;
    N3: TMenuItem;
    SemestreAtual: TMenuItem;
    SemestreAnterior: TMenuItem;
    N4: TMenuItem;
    AnoAtual: TMenuItem;
    AnoAnterior: TMenuItem;
    N5: TMenuItem;
    LimparDatas: TMenuItem;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargofilialClick(Sender: TObject);
	procedure spdAlterargofilialClick(Sender: TObject);
	procedure spdGravargofilialClick(Sender: TObject);
	procedure spdCancelargofilialClick(Sender: TObject);
	procedure spdExcluirgofilialClick(Sender: TObject);

    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdsgofilialNewRecord(DataSet: TDataSet);
    procedure spdOpcoesClick(Sender: TObject);
    procedure spdduplicargofilialClick(Sender: TObject);
    procedure dbegofilialCEPExit(Sender: TObject);
    procedure cdsgofilialAfterClose(DataSet: TDataSet);
    procedure spdFiltrarDiasUteisClick(Sender: TObject);
    procedure cdsgofilialAfterScroll(DataSet: TDataSet);
    procedure spbdatesinteticoClick(Sender: TObject);
    procedure HojeClick(Sender: TObject);
    procedure OntemClick(Sender: TObject);
    procedure SemanaAtualClick(Sender: TObject);
    procedure SemanaAnteriorClick(Sender: TObject);
    procedure UltimosQuizeDiasClick(Sender: TObject);
    procedure MesAtualClick(Sender: TObject);
    procedure MesAnteriorClick(Sender: TObject);
    procedure TrimestreAtualClick(Sender: TObject);
    procedure TrimestreAnteriorClick(Sender: TObject);
    procedure SemestreAtualClick(Sender: TObject);
    procedure SemestreAnteriorClick(Sender: TObject);
    procedure AnoAtualClick(Sender: TObject);
    procedure AnoAnteriorClick(Sender: TObject);
    procedure LimparDatasClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgofilial: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgofilial: Array[0..10]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgofilial: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgofilial: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagofilial: Array[0..0]  of TDuplicidade;
	duplicidadeCampogofilial: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
    procedure permissao;
	public

end;

var
fgofilial: Tfgofilial;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgofilial.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgofilial.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgofilial.TabIndex;
end;

procedure Tfgofilial.HojeClick(Sender: TObject);
begin
 try
  fncgofilial.DiaAtual(Date);
  data1.Date  := fncgofilial.DiaAtual(Date);
  data2.Date  := fncgofilial.DiaAtual(Date);
 finally
 end;
end;

procedure Tfgofilial.LimparDatasClick(Sender: TObject);
begin
  Data1.Clear;
  data2.Clear;
end;

procedure Tfgofilial.AnoAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data        := TStringList.Create;
    data        := fncgofilial.anoanterior(Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.AnoAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data        := TStringList.Create;
    data        := fncgofilial.anoantual(Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.cdsgofilialAfterClose(DataSet: TDataSet);
begin
 FDDiasUteis.close;
end;

procedure Tfgofilial.cdsgofilialAfterScroll(DataSet: TDataSet);
begin
 FDDiasUteis.Close;
 FDDiasUteis.SQL.Clear;
 FDDiasUteis.SQL.Add('SELECT * FROM gofaturamento_dia_util  WHERE ID_FILIAL=:ID_FILIAL');
 FDDiasUteis.ParamByName('ID_FILIAL').AsInteger:= cdsgofilialID.AsInteger;
 FDDiasUteis.Open();
end;

procedure Tfgofilial.cdsgofilialNewRecord(DataSet: TDataSet);
begin
	if cdsgofilial.State in [dsinsert] then
	cdsgofilialID.Value:=fncgofilial.autoNumeracaoGenerator(dm.conexao,'G_gofilial');
end;

procedure Tfgofilial.dbegofilialCEPExit(Sender: TObject);
begin
   if (cdsgofilialCEP.AsString<>EmptyStr) then
   if  cdsgofilial.State in [dsedit,dsinsert]   then
   begin
    dm.FDQuery.Close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.sql.Add('SELECT CIDADE, UF, BAIRRO, RUA FROM CEP WHERE CEP=:CEP');
    dm.FDQuery.ParamByName('CEP').AsString  := cdsgofilialCEP.AsString;
    dm.FDQuery.Open();
    cdsgofilialCIDADE.AsString  := dm.FDQuery.FieldByName('CIDADE').AsString;
    cdsgofilialUF.AsString  := dm.FDQuery.FieldByName('UF').AsString;
   end;

end;

procedure Tfgofilial.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgofilial.verificarEmTransacao(cdsgofilial);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgofilial.configurarGridesFormulario(fgofilial,true, false);

	fncgofilial.Free;
	{eliminando container de fun��es da memoria<<<}

	fgofilial:=nil;
	Action:=cafree;
end;

procedure Tfgofilial.FormCreate(Sender: TObject);
begin
  sessao;
  {>>>container de tradu�a� de compomentes}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
   begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
   end;
  {>>>container de tradu�a� de compomentes}

	{>>>container de fun��es}
	fncgofilial:= TFuncoes.Create(Self);
	fncgofilial.definirConexao(dm.conexao);
	fncgofilial.definirConexaohistorico(dm.conexao);
	fncgofilial.definirFormulario(Self);
	fncgofilial.validarTransacaoRelacionamento:=true;
	fncgofilial.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	duplicidadeCampogofilial:=TDuplicidade.Create;
	duplicidadeCampogofilial.agrupador:='';
	duplicidadeCampogofilial.objeto :=dbegofilialCNPJ;
	duplicidadeListagofilial[0]:=duplicidadeCampogofilial;

	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCIDADE';
	pesquisasItem.titulo:='Cidade';
	pesquisasItem.campo:='CIDADE';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncgofilial.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}


	fncgofilial.criarPesquisas(sqlgofilial,cdsgofilial,'select * from gofilial', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gofilial}
  camposObrigatoriosgofilial[0]:=dbegofilialDESCRICAO;
  camposObrigatoriosgofilial[1]:=dbegofilialCNPJ;
  camposObrigatoriosgofilial[2]:=dbegofilialCEP;
  camposObrigatoriosgofilial[3]:=dbegofilialENDERECO;
	{campos obrigatorios gofilial<<<}

	{>>>objetos ativos no modo de manipula��o de dados gofilial}
 //	objetosModoEdicaoAtivosgofilial[0]:=pnlDadosgofilial;
	objetosModoEdicaoAtivosgofilial[1]:=spdCancelargofilial;
	objetosModoEdicaoAtivosgofilial[2]:=spdGravargofilial;
  objetosModoEdicaoAtivosgofilial[3]:=dbegofilialCODCNAE;
  objetosModoEdicaoAtivosgofilial[4]:=btnCNAE;
	{objetos ativos no modo de manipula��o de dados gofilial<<<}

	{>>>objetos inativos no modo de manipula��o de dados gofilial}
	objetosModoEdicaoInativosgofilial[0]:=spdAdicionargofilial;
	objetosModoEdicaoInativosgofilial[1]:=spdAlterargofilial;
	objetosModoEdicaoInativosgofilial[2]:=spdExcluirgofilial;
	objetosModoEdicaoInativosgofilial[3]:=spdOpcoes;
	objetosModoEdicaoInativosgofilial[4]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gofilial<<<}

	{>>>comando de adi��o de teclas de atalhos gofilial}
	fncgofilial.criaAtalhoPopupMenu(Popupgofilial,'Adicionar novo registro',VK_F4,spdAdicionargofilial.OnClick);
	fncgofilial.criaAtalhoPopupMenu(Popupgofilial,'Alterar registro selecionado',VK_F5,spdAlterargofilial.OnClick);
	fncgofilial.criaAtalhoPopupMenu(Popupgofilial,'Gravar �ltimas altera��es',VK_F6,spdGravargofilial.OnClick);
	fncgofilial.criaAtalhoPopupMenu(Popupgofilial,'Cancelar �ltimas altera��e',VK_F7,spdCancelargofilial.OnClick);
	fncgofilial.criaAtalhoPopupMenu(Popupgofilial,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgofilial.OnClick);
  fncgofilial.criaAtalhoPopupMenu(Popupgofilial,'Duplicar registro selecionado',ShortCut(VK_F12,[ssCtrl]),spdDuplicargofilial.OnClick);

	{comando de adi��o de teclas de atalhos gofilial<<<}

	pgcPrincipal.ActivePageIndex:=0;
  permissao;
  {comandos de permiss�o}

	{>>>abertura dos datasets}
	cdsgofilial.open();
	{abertura dos datasets<<<}

	fncgofilial.criaAtalhoPopupMenuNavegacao(Popupgofilial,cdsgofilial);

	fncgofilial.definirMascaraCampos(cdsgofilial);

	fncgofilial.configurarGridesFormulario(fgofilial,false, true);


end;

procedure Tfgofilial.spbdatesinteticoClick(Sender: TObject);
begin
 with TcxButton(Sender).ClientToScreen(point(TcxButton(Sender).Width,
    TcxButton(Sender).Height)) do
  begin
    popmenudata.Popup(X, Y);
  end;
end;

procedure Tfgofilial.spdAdicionargofilialClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgofilial.TabIndex;
	if (fncgofilial.adicionar(cdsgofilial)=true) then
		begin
			fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,true);
			dbegofilialDESCRICAO.SetFocus;
	end
	else
		fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,false);
end;

procedure Tfgofilial.spdAlterargofilialClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgofilial.TabIndex;
	if (fncgofilial.alterar(cdsgofilial)=true) then
		begin
			fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,true);
			dbegofilialDESCRICAO.SetFocus;
	end
	else
		fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,false);
end;

procedure Tfgofilial.spdGravargofilialClick(Sender: TObject);
begin
  fncgofilial.verificarCamposObrigatorios(cdsgofilial, camposObrigatoriosgofilial, pgcPrincipal);
	fncgofilial.verificarDuplicidade(dm.FDConexao, cdsgofilial, 'gofilial', 'ID', duplicidadeListagofilial);

	if (fncgofilial.gravar(cdsgofilial)=true) then
  begin
		fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,false);
    permissao;
  end
	else
		fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,true);
end;

procedure Tfgofilial.spdCancelargofilialClick(Sender: TObject);
begin
	if (fncgofilial.cancelar(cdsgofilial)=true) then
  begin
		fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,false);
    permissao;
  end
	else
		fncgofilial.ativarModoEdicao(objetosModoEdicaoAtivosgofilial,objetosModoEdicaoInativosgofilial,true);
end;

procedure Tfgofilial.spdduplicargofilialClick(Sender: TObject);
begin
  fncgofilial.Duplicarregistro(cdsgofilial, true,'ID','G_gofilial');
end;

procedure Tfgofilial.spdExcluirgofilialClick(Sender: TObject);
begin
	fncgofilial.excluir(cdsgofilial);
end;

procedure Tfgofilial.spdFiltrarDiasUteisClick(Sender: TObject);
begin
 if ((data1.Text<>EmptyStr) and  (data2.Text<>EmptyStr) and (cmbDiasSemana.Text<>EmptyStr)) then
 begin
     FDDiasUteis.DisableControls;
      try
        FDDiasUteis.Filtered := False;
        FDDiasUteis.FilterOptions := [foNoPartialCompare];
        FDDiasUteis.Filter := 'UPPER(DIA_SEMANA)='+QuotedStr(cmbDiasSemana.Text)+' AND DATA BETWEEN ' + QuotedStr(Datetostr(data1.Date))+' and ' + QuotedStr(Datetostr(data2.Date));
        FDDiasUteis.Filtered := True;
      finally
        FDDiasUteis.First;
        FDDiasUteis.EnableControls;
      end;
 end;

  if ((data1.Text<>EmptyStr) and  (data2.Text<>EmptyStr)) then
 begin
     FDDiasUteis.DisableControls;
      try
        FDDiasUteis.Filtered := False;
        FDDiasUteis.FilterOptions := [foNoPartialCompare];
        FDDiasUteis.Filter := ' DATA BETWEEN ' + QuotedStr(Datetostr(data1.Date))+' and ' + QuotedStr(Datetostr(data2.Date));
        FDDiasUteis.Filtered := True;
      finally
        FDDiasUteis.First;
        FDDiasUteis.EnableControls;
      end;
 end;

end;

procedure Tfgofilial.sessao;
begin
   try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;

end;

procedure Tfgofilial.permissao;
begin
  {comandos de permiss�o}
  if fncgofilial.permissao('GOFILIAL', 'Acesso', ParamStr(3)) = false then
  begin
    application.MessageBox('Sem permiss�o de acesso.', 'Permiss�o', MB_ICONEXCLAMATION);
    application.Terminate;
  end;
  spdAdicionargofilial.Enabled := fncgofilial.permissao('GOFILIAL', 'Adicionar', ParamStr(3));
  spdAlterargofilial.Enabled := fncgofilial.permissao('GOFILIAL', 'Alterar', ParamStr(3));
  spdExcluirgofilial.Enabled := fncgofilial.permissao('GOFILIAL', 'Excluir', ParamStr(3));
  Popupgofilial.Items[0].Enabled := fncgofilial.permissao('GOFILIAL', 'Adicionar', ParamStr(3));
  Popupgofilial.Items[1].Enabled := fncgofilial.permissao('GOFILIAL', 'Alterar', ParamStr(3));
  Popupgofilial.Items[4].Enabled := fncgofilial.permissao('GOFILIAL', 'Excluir', ParamStr(3));
end;



procedure Tfgofilial.SemanaAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncgofilial.SemanaAnterior(Date);
    data1.Date  := StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
  end;
end;

procedure Tfgofilial.SemanaAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncgofilial.SemanaAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;

end;

procedure Tfgofilial.SemestreAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data        := TStringList.Create;
    data        := fncgofilial.semestreanterior(Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.SemestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data        := TStringList.Create;
    data        := fncgofilial.semestreatual(Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgofilial.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgofilial.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgofilial.spdOpcoesClick(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgofilial.TrimestreAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data        := TStringList.Create;
    data        := fncgofilial.trimestreatual(Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.TrimestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data        := TStringList.Create;
    data        := fncgofilial.trimestreatual (Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.UltimosQuizeDiasClick(Sender: TObject);
var data : TStringList;
begin
 try
    data        := TStringList.Create;
    data        :=fncgofilial.UltimosQuinzeDias (Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgofilial.IsEmpty then
	fncgofilial.visualizarRelatorios(frxgofilial);
end;

procedure Tfgofilial.MesAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data        := TStringList.Create;
    data        :=fncgofilial.MesAnterior (Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.MesAtualClick(Sender: TObject);
var data : TStringList;
begin
 try
    data        := TStringList.Create;
    data        :=fncgofilial.MesAtual (Date);
    Data1.Date  := StrToDateTime(data[0]);
    Data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tfgofilial.OntemClick(Sender: TObject);
begin
 try
    Data1.Date  :=fncgofilial.DiaAnterior(Date);
    Data2.Date  :=fncgofilial.DiaAnterior(Date);
   finally
   end;
end;

end.

