object fdtcompetencia: Tfdtcompetencia
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'fdtcompetencia'
  ClientHeight = 171
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblprojeto: TLabel
    Left = 7
    Top = 39
    Width = 58
    Height = 13
    Caption = 'Cod.Projeto'
  end
  object lbldtcompetencia: TLabel
    Left = 8
    Top = 80
    Width = 77
    Height = 13
    Caption = 'Dt.Compet'#234'ncia'
  end
  object pnldados: TPanel
    Left = 0
    Top = 33
    Width = 406
    Height = 136
    Align = alTop
    BevelKind = bkFlat
    TabOrder = 4
    ExplicitTop = 27
    ExplicitWidth = 422
    object btnPesquisarprojeto: TSpeedButton
      Left = 72
      Top = 18
      Width = 20
      Height = 21
      Caption = '...'
    end
    object pnlLinhaBotoessecao: TPanel
      Left = 1
      Top = 102
      Width = 400
      Height = 29
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 74
      ExplicitWidth = 146
      object spdok: TButton
        Left = 334
        Top = 2
        Width = 60
        Height = 22
        Caption = 'OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = spdokClick
      end
      object spdfechar: TButton
        Left = 273
        Top = 2
        Width = 60
        Height = 22
        Caption = 'Fechar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = spdfecharClick
      end
    end
  end
  object dtcompetencia: TcxDateEdit
    Left = 5
    Top = 99
    AutoSize = False
    Style.BorderStyle = ebs3D
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.Kind = lfStandard
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfStandard
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfStandard
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 0
    Height = 21
    Width = 97
  end
  object pnlLinhaTitulo: TPanel
    Left = 0
    Top = 0
    Width = 406
    Height = 33
    Align = alTop
    Alignment = taLeftJustify
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = 6441265
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    OnMouseDown = pnlLinhaTituloMouseDown
    ExplicitWidth = 422
    object lblSite: TLabel
      Left = 20
      Top = -1
      Width = 126
      Height = 13
      Caption = ' www.gosoftware.com.br '
      Color = 6441265
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object dbecodprojeto: TEdit
    Left = 4
    Top = 54
    Width = 67
    Height = 21
    Color = 13613470
    TabOrder = 2
    OnKeyPress = dbecodprojetoKeyPress
  end
  object dbeprojeto: TEdit
    Left = 97
    Top = 54
    Width = 300
    Height = 21
    Enabled = False
    TabOrder = 3
  end
  object psqprojeto: TPesquisa
    Colunas = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'C'#243'digo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 300
        Visible = True
      end>
    CampoResultado = 'ID'
    CampoDescricao = 'DESCRICAO'
    SQL.Strings = (
      'select  ID, CODGRUPO, GRUPO, DESCRICAO from goprojetos WHERE 1=1'
      '')
    MultiSelecao = True
    Conexao = dm.conexao
    AbrirAuto = True
    PesquisaPadrao = 0
    Filtros = <
      item
        DataType = ftString
        Name = 'DESCRICAO'
        ParamType = ptUnknown
        Value = 'Descri'#231#227'o'
      end>
    EditCodigo = dbecodprojeto
    EditDescricao = dbeprojeto
    BotaoPesquisa = btnPesquisarprojeto
    Left = 115
    Top = 94
  end
end
