unit udtcompetencia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils,
  Vcl.StdCtrls, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, Vcl.ExtCtrls,
  Vcl.Buttons, uPesquisa;

type
  Tfdtcompetencia = class(TForm)
    dtcompetencia: TcxDateEdit;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    dbecodprojeto: TEdit;
    dbeprojeto: TEdit;
    lblprojeto: TLabel;
    psqprojeto: TPesquisa;
    lbldtcompetencia: TLabel;
    pnldados: TPanel;
    pnlLinhaBotoessecao: TPanel;
    spdok: TButton;
    spdfechar: TButton;
    btnPesquisarprojeto: TSpeedButton;
    procedure spdokClick(Sender: TObject);
    procedure spdfecharClick(Sender: TObject);
    procedure pnlLinhaTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dbecodprojetoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fdtcompetencia: Tfdtcompetencia;

implementation

{$R *.dfm}

procedure Tfdtcompetencia.dbecodprojetoKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeprojeto.Text  :=EmptyStr;
  end;
end;

procedure Tfdtcompetencia.FormShow(Sender: TObject);
begin
 dbecodprojeto.SetFocus;
end;

procedure Tfdtcompetencia.pnlLinhaTituloMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tfdtcompetencia.spdfecharClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

procedure Tfdtcompetencia.spdokClick(Sender: TObject);
begin
 ModalResult := mrOk;
end;

end.
