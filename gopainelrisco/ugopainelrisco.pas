unit ugopainelrisco;

{ *Autor- Waldes Alves de Souza * }
{ *Data 25/08/2021* }
{ *Hora 17:59:42* }
{ *Unit gopainelrisco * }

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider,
  midaslib,
  Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,
  Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,

  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxDBEdit, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa, cxCurrencyEdit,
  cxCheckBox;

type
  Tfgopainelrisco = class(TForm)

    dspgopainelrisco: TDataSetProvider;
    dsgopainelrisco: TDataSource;

    popupImpressao: TPopupMenu;
    menuImpRelSintetico: TMenuItem;
    tabPesquisas: Tcxtabsheet;
    flwpOpcoes: TFlowPanel;
    flwpPesquisas: TFlowPanel;
    pnlResultadoPesquisas: TPanel;
    pnlTituloResultadoPesquisa: TPanel;
    pnlTituloPesquisa: TPanel;
    frxpainel1: TfrxReport;
    frxDBgopainelrisco: TfrxDBDataset;
    Popupgopainelrisco: TPopupMenu;
    pgcPrincipal: TcxPageControl;
    GridResultadoPesquisa: TcxGrid;
    tabgopainelrisco: Tcxtabsheet;
    SCRgopainelrisco: TScrollBox;
    pnlDadosgopainelrisco: TPanel;
    { >>>bot�es de manipula��o de dados gopainelrisco }
    pnlLinhaBotoesgopainelrisco: TPanel;
    spdAdicionargopainelrisco: TButton;
    spdAlterargopainelrisco: TButton;
    spdGravargopainelrisco: TButton;
    spdCancelargopainelrisco: TButton;
    spdExcluirgopainelrisco: TButton;
    pnlLayoutLinhasgopainelrisco1: TPanel;
    pnlLayoutTitulosgopainelriscoid: TPanel;
    pnlLayoutCamposgopainelriscoid: TPanel;
    dbegopainelriscoid: TDBEdit;
    pnlLayoutLinhasgopainelrisco2: TPanel;
    pnlLayoutTitulosgopainelriscodt_cadastro: TPanel;
    pnlLayoutCamposgopainelriscodt_cadastro: TPanel;
    dbegopainelriscodt_cadastro: TDBEdit;
    pnlLayoutLinhasgopainelrisco3: TPanel;
    pnlLayoutTitulosgopainelriscohs_cadastro: TPanel;
    pnlLayoutCamposgopainelriscohs_cadastro: TPanel;
    dbegopainelriscohs_cadastro: TDBEdit;
    pnlLayoutLinhasgopainelrisco4: TPanel;
    pnlLayoutTitulosgopainelriscodescricao: TPanel;
    pnlLayoutCamposgopainelriscodescricao: TPanel;
    dbegopainelriscodescricao: TDBEdit;
    pnlLayoutLinhasgopainelrisco5: TPanel;
    pnlLayoutTitulosgopainelriscocodprojeto: TPanel;
    pnlLayoutCamposgopainelriscocodprojeto: TPanel;
    dbegopainelriscocodprojeto: TDBEdit;
    pnlLayoutLinhasgopainelrisco7: TPanel;
    pnlLayoutTitulosgopainelriscocodfilial: TPanel;
    pnlLayoutCamposgopainelriscocodfilial: TPanel;
    dbegopainelriscocodfilial: TDBEdit;
    pnlLayoutLinhasgopainelrisco9: TPanel;
    pnlLayoutTitulosgopainelriscodata: TPanel;
    pnlLayoutCamposgopainelriscodata: TPanel;
    pnlLayoutLinhasgopainelrisco10: TPanel;
    pnlLayoutLinhasgopainelrisco11: TPanel;

    pnlTitulo: TPanel;

    lblTitulo: TLabel;

    spdOpcoes: TSpeedButton;

    imglogo: TImage;

    lblusuario: TLabel;

    GridResultadoPesquisaLevel1: TcxGridLevel;

    pnlLayoutCamposgopainelriscoprojeto: TPanel;

    dbegopainelriscoPROJETO: TDBEdit;

    pnlLayoutCamposgopainelriscofilial: TPanel;

    dbegopainelriscoFILIAL: TDBEdit;

    dbegopainelriscoDATA: TcxDBDateEdit;

    cdsfiltros: TClientDataSet;
    psqfilial: TPesquisa;
    btnfilial: TButton;
    btnprojeto: TButton;
    psqprojeto: TPesquisa;
    pnlLayoutLinhasgopainelrisco8: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;

    btnPORTADOR: TButton;
    Panel4: TPanel;

    dbegopainelriscoCODPORTADOR: TDBEdit;
    cdsgopainelriscoID: TIntegerField;
    cdsgopainelriscoDT_CADASTRO: TDateField;
    cdsgopainelriscoHS_CADASTRO: TTimeField;
    cdsgopainelriscoDESCRICAO: TStringField;
    cdsgopainelriscoCODPROJETO: TIntegerField;
    cdsgopainelriscoPROJETO: TStringField;
    cdsgopainelriscoCODFILIAL: TIntegerField;
    cdsgopainelriscoFILIAL: TStringField;
    cdsgopainelriscoDATA: TDateField;
    cdsgopainelriscoVLVENCIDOS: TFloatField;
    cdsgopainelriscoVLVENCER: TFloatField;
    cdsgopainelriscoCODPORTADOR: TIntegerField;
    cdsgopainelriscoPORTADOR: TStringField;
    sqlgopainelrisco: TSQLDataSet;
    sqlgopainelriscoID: TIntegerField;
    sqlgopainelriscoDT_CADASTRO: TDateField;
    sqlgopainelriscoHS_CADASTRO: TTimeField;
    sqlgopainelriscoDESCRICAO: TStringField;
    sqlgopainelriscoCODPROJETO: TIntegerField;
    sqlgopainelriscoPROJETO: TStringField;
    sqlgopainelriscoCODFILIAL: TIntegerField;
    sqlgopainelriscoFILIAL: TStringField;
    sqlgopainelriscoDATA: TDateField;
    sqlgopainelriscoVLVENCIDOS: TFloatField;
    sqlgopainelriscoVLVENCER: TFloatField;
    sqlgopainelriscoCODPORTADOR: TIntegerField;
    sqlgopainelriscoPORTADOR: TStringField;
    cdsgopainelrisco: TClientDataSet;
    psqportador: TPesquisa;
    dbegopainelriscoPORTADOR: TDBEdit;
    spdDuplicargopainelrisco: TButton;
    GridResultadoPesquisaDBTVVLVENCER: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLVENCIDOS: TcxGridDBColumn;
    pnlLayoutLinhasgopainelrisco12: TPanel;
    pnlLayoutTitulosgopainelriscovlimite: TPanel;
    pnlLayoutCamposgopainelriscovlimite: TPanel;
    dbegopainelriscoVLIMITE: TDBEdit;
    sqlgopainelriscoVLIMITE: TFloatField;
    cdsgopainelriscoVLIMITE: TFloatField;
    cdsgopainelriscoVLIMITEDISPONIVEL: TFloatField;
    GridResultadoPesquisaDBTVCODPORTADOR: TcxGridDBColumn;
    GridResultadoPesquisaDBTVPORTADOR: TcxGridDBColumn;
    cdsgopainelriscoVLIMITEUTILIZADO: TFloatField;
    GridResultadoPesquisaDBTVVLIMITEUTILIZADO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLIMITEDISPONIVEL: TcxGridDBColumn;
    spdExportargopainelrisco: TButton;
    spdconsolidargopainelrisco: TButton;
    pnlLayoutTitulosgopainelriscovlvencidos: TPanel;
    pnlLayoutCamposgopainelriscovlvencidos: TPanel;
    dbegopainelriscoVLVENCIDOS: TDBEdit;
    pnlLayoutTitulosgopainelriscovlvencer: TPanel;
    pnlLayoutCamposgopainelriscovlvencer: TPanel;
    dbegopainelriscoVLVENCER: TDBEdit;
    Panel1: TPanel;
    Panel5: TPanel;
    dbegopainelriscoLIMITEDESCONTO: TDBEdit;
    Panel6: TPanel;
    Panel7: TPanel;
    dbegopainelriscoVENCIDOSDESCONTO: TDBEdit;
    Panel8: TPanel;
    Panel9: TPanel;
    dbegopainelriscoVENCERDESCONTO: TDBEdit;
    Panel10: TPanel;
    Panel11: TPanel;
    dbegopainelriscoLIMITECOMISSARIA: TDBEdit;
    Panel12: TPanel;
    Panel13: TPanel;
    dbegopainelriscoVENCIDOSCOMISSARIA: TDBEdit;
    Panel14: TPanel;
    Panel15: TPanel;
    dbegopainelriscoVENCERCOMISSARIA: TDBEdit;
    sqlgopainelriscoVLLIMITEDESCONTO: TFloatField;
    sqlgopainelriscoVLVENCIDOSDESCONTO: TFloatField;
    sqlgopainelriscoVLVENCERDESCONTO: TFloatField;
    sqlgopainelriscoVLLIMITECOMISSSARIA: TFloatField;
    sqlgopainelriscoVLVENCIDOSCOMISSARIA: TFloatField;
    sqlgopainelriscoVLVENCERCOMISSARIA: TFloatField;
    cdsgopainelriscoVLLIMITEDESCONTO: TFloatField;
    cdsgopainelriscoVLVENCIDOSDESCONTO: TFloatField;
    cdsgopainelriscoVLVENCERDESCONTO: TFloatField;
    cdsgopainelriscoVLLIMITECOMISSSARIA: TFloatField;
    cdsgopainelriscoVLVENCIDOSCOMISSARIA: TFloatField;
    cdsgopainelriscoVLVENCERCOMISSARIA: TFloatField;
    cdsgopainelriscoVLIMITEUTILIZADODESCONTO: TFloatField;
    cdsgopainelriscoVLIMITEDISPONIVELDESCONTO: TFloatField;
    cdsgopainelriscoVLIMITEUTILIZADOCOMISSARIA: TFloatField;
    cdsgopainelriscoVLIMITEDISPONIVELCOMISSARIA: TFloatField;
    pnlLayoutTitulosgopainelriscovlvencerfomento: TPanel;
    Panel17: TPanel;
    dbegopainelriscoLIMITEUTILIZADOFOMENTO: TDBEdit;
    pnlLayoutTitulosgopainelriscolimitedisponivelfomento: TPanel;
    Panel19: TPanel;
    dbegopainelriscoLIMITEUDISPONIVELFOMENTO: TDBEdit;
    Panel20: TPanel;
    Panel21: TPanel;
    dbegopainelriscoLIMITEUTILIZADODESCONTO: TDBEdit;
    Panel22: TPanel;
    Panel23: TPanel;
    dbegopainelriscoLIMITEDISPONIVELDESCONTO: TDBEdit;
    Panel24: TPanel;
    Panel25: TPanel;
    dbegopainelriscoLIMITEUTILIZADOCOMISSARIA: TDBEdit;
    Panel26: TPanel;
    Panel27: TPanel;
    dbegopainelriscoLIMITEUDISPONIVELCOMISSARIA: TDBEdit;
    spPKG_CONSOLIDA_RISCO: TSQLStoredProc;
    GridResultadoPesquisaDBTVVLVENCERDESCONTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLVENCIDOSDESCONTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLIMITEUTILIZADODESCONTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLIMITEDISPONIVELDESCONTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLVENCERCOMISSARIA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLVENCIDOSCOMISSARIA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLLIMITECOMISSSARIA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLIMITEDISPONIVELCOMISSARIA: TcxGridDBColumn;
    cdsgopainelriscoAGRUPADOR: TStringField;
    frxbase: TfrxReport;
    frxpainel2: TfrxReport;
    frxpainel3: TfrxReport;
    RelatrioII1: TMenuItem;
    RelatrioIII1: TMenuItem;
    RelatrioIV1: TMenuItem;
    lblTituloversao: TLabel;
    lblProjetoresultado: TLabel;
    lblusuarioresultado: TLabel;
    lblversao: TLabel;
    cdsgopainelriscoselecionado: TBooleanField;
    GridResultadoPesquisaDBTVSelecionado: TcxGridDBColumn;
    spdmarcartodos: TButton;
    spddesmarcartodos: TButton;
    spdexcluirselecionados: TButton;
        { objetos de manipula��o de dados<<< }

    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure altenarPopupTabSheet(Sender: TObject);
    procedure spdImpressaoClick(Sender: TObject);
    procedure menuImpRelSinteticoClick(Sender: TObject);
    { >>>procedimentos de manipula��o de dados }
    procedure spdAdicionargopainelriscoClick(Sender: TObject);
    procedure spdAlterargopainelriscoClick(Sender: TObject);
    procedure spdGravargopainelriscoClick(Sender: TObject);
    procedure spdCancelargopainelriscoClick(Sender: TObject);
    procedure spdExcluirgopainelriscoClick(Sender: TObject);
    procedure cdsgopainelriscoBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure spdDuplicargopainelriscoClick(Sender: TObject);
    procedure psqportadorPreencherCampos(Sender: TObject);
    procedure spdOpcoesClick(Sender: TObject);
    procedure cdsgopainelriscoCalcFields(DataSet: TDataSet);
    procedure spdExportargopainelriscoClick(Sender: TObject);
    procedure spdconsolidargopainelriscoClick(Sender: TObject);
    procedure RelatrioII1Click(Sender: TObject);
    procedure RelatrioIII1Click(Sender: TObject);
    procedure spdmarcartodosClick(Sender: TObject);
    procedure spddesmarcartodosClick(Sender: TObject);
    procedure spdexcluirselecionadosClick(Sender: TObject);
    { procedimentos de manipula��o de dados<<< }

  private

    { container de funcoes }
    fncgopainelrisco: TFuncoes;

    { array de componentes para verifica��o de campos obrigat�rios }
    camposObrigatoriosgopainelrisco: Array [0 .. 1] of TWinControl;

    { array de componentes de ativa��o para modo de edi��o }
    objetosModoEdicaoAtivosgopainelrisco: Array [0 .. 10] of TControl;
    objetosModoEdicaoInativosgopainelrisco: Array [0 .. 10] of TControl;

    { array de componentes para verifica��o de duplicidade }
    duplicidadeListagopainelrisco: Array [0 .. 0] of TDuplicidade;
    duplicidadeCampogopainelrisco: TDuplicidade;
    { array de crit�rios de pesquisas }
    pesquisasLista: Array [0 .. 20] of TPesquisas;
    pesquisasItem: TPesquisas;
    procedure sessao;
  public

  end;

var
  fgopainelrisco: Tfgopainelrisco;

implementation

uses udm, Vcl.DialogMessage, udtcompetencia, System.IniFiles;

{$R *.dfm}
// {$R logo.res}

procedure Tfgopainelrisco.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin Key := #0;Perform(WM_nextdlgctl, 0, 0);end;
end;

procedure Tfgopainelrisco.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
  pgcPrincipal.ActivePageIndex := tabgopainelrisco.TabIndex;
end;

procedure Tfgopainelrisco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  { >>>verificando exist�ncia de registros em transa��o }
  fncgopainelrisco.verificarEmTransacao(cdsgopainelrisco);
  { verificando exist�ncia de registros em transa��o<<< }

  { >>>eliminando container de fun��es da memoria }
  fncgopainelrisco.configurarGridesFormulario(fgopainelrisco, true, false);

  fncgopainelrisco.Free;
  { eliminando container de fun��es da memoria<<< }

  fgopainelrisco := nil;
  Action := cafree;
end;

procedure Tfgopainelrisco.FormCreate(Sender: TObject);
begin
  sessao;
  { >>>container de fun��es }
  if FileExists(ExtractFilePath(Application.ExeName) + '\Tradu��oDev.ini') then
  begin
    dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName) +
      '\Tradu��oDev.ini');
    dm.cxLocalizer.LanguageIndex := 1;
    dm.cxLocalizer.Active := true;
  end;
  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
    procedure
    begin

      { >>>container de fun��es }
      fncgopainelrisco := TFuncoes.Create(Self);
      fncgopainelrisco.definirConexao(dm.conexao);
      fncgopainelrisco.definirConexaohistorico(dm.conexao);
      fncgopainelrisco.definirFormulario(Self);
      fncgopainelrisco.validarTransacaoRelacionamento := true;
      fncgopainelrisco.autoAplicarAtualizacoesBancoDados := true;
      { container de fun��es<<< }

      {<<vers�o da rotina>>}
      lblversao.Caption:=lblversao.Caption+' - '+fncgopainelrisco.recuperarVersaoExecutavel;
      {<<vers�o da rotina>>}


      { >>>verificar campos duplicidade }
      { verificar campos duplicidade<<< }

      { >>>gerando objetos de pesquisas }
      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesID';
      pesquisasItem.titulo := 'C�digo';
      pesquisasItem.campo := 'ID';
      pesquisasItem.tipoFDB := 'Integer';
      pesquisasItem.criterio := '=';
      pesquisasItem.intervalo := 'S';
      pesquisasItem.tamanho := 72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[0] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesDT_CADASTRO';
      pesquisasItem.titulo := 'Data de Cadastro';
      pesquisasItem.campo := 'DT_CADASTRO';
      pesquisasItem.tipoFDB := 'Date';
      pesquisasItem.criterio := '=';
      pesquisasItem.intervalo := 'S';
      pesquisasItem.tamanho := 72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[1] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesDATA';
      pesquisasItem.titulo := 'Data Compet�ncia';
      pesquisasItem.campo := 'DATA';
      pesquisasItem.tipoFDB := 'Date';
      pesquisasItem.criterio := '=';
      pesquisasItem.intervalo := 'S';
      pesquisasItem.tamanho := 72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[2] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesDESCRICAO';
      pesquisasItem.titulo := 'Descri��o';
      pesquisasItem.campo := 'DESCRICAO';
      pesquisasItem.tipoFDB := 'Varchar';
      pesquisasItem.criterio := 'LIKE';
      pesquisasItem.intervalo := 'N';
      pesquisasItem.tamanho := 480;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[3] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesCODPROJETO';
      pesquisasItem.titulo := 'C�digo Projeto';
      pesquisasItem.campo := 'CODPROJETO';
      pesquisasItem.tipoFDB := 'Integer';
      pesquisasItem.criterio := '=';
      pesquisasItem.intervalo := 'S';
      pesquisasItem.tamanho := 72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[4] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesPROJETO';
      pesquisasItem.titulo := 'Projeto';
      pesquisasItem.campo := 'PROJETO';
      pesquisasItem.tipoFDB := 'Varchar';
      pesquisasItem.criterio := 'LIKE';
      pesquisasItem.intervalo := 'N';
      pesquisasItem.tamanho := 480;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[5] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesCODFILIAL';
      pesquisasItem.titulo := 'C�d.Filial';
      pesquisasItem.campo := 'CODFILIAL';
      pesquisasItem.tipoFDB := 'Integer';
      pesquisasItem.criterio := '=';
      pesquisasItem.intervalo := 'S';
      pesquisasItem.tamanho := 72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[6] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesFILIAL';
      pesquisasItem.titulo := 'Filial';
      pesquisasItem.campo := 'FILIAL';
      pesquisasItem.tipoFDB := 'Varchar';
      pesquisasItem.criterio := 'LIKE';
      pesquisasItem.intervalo := 'N';
      pesquisasItem.tamanho := 480;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[7] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesCODPORTADOR';
      pesquisasItem.titulo := 'C�d.Portador';
      pesquisasItem.campo := 'CODPORTADOR';
      pesquisasItem.tipoFDB := 'Integer';
      pesquisasItem.criterio := '=';
      pesquisasItem.intervalo := 'S';
      pesquisasItem.tamanho := 72;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[8] := pesquisasItem;

      pesquisasItem := TPesquisas.Create;
      pesquisasItem.agrupador := 'pesPORTADOR';
      pesquisasItem.titulo := 'Portador';
      pesquisasItem.campo := 'PORTADOR';
      pesquisasItem.tipoFDB := 'Varchar';
      pesquisasItem.criterio := 'LIKE';
      pesquisasItem.intervalo := 'N';
      pesquisasItem.tamanho := 480;
      pesquisasItem.comboItens.clear;
      pesquisasItem.lookupSelect := '';
      pesquisasItem.lookupCampoID := '';
      pesquisasItem.lookupCampoTipoID := 'Integer';
      pesquisasItem.lookupCampoDescricao := '';
      pesquisasItem.lookupGridItens.clear;
      pesquisasLista[9] := pesquisasItem;

      { gerando campos no clientdatsaet de filtros }

      fncgopainelrisco.carregarfiltros(pesquisasLista, cdsfiltros, flwpOpcoes);

      { gerando campos no clientdatsaet de filtros }

      fncgopainelrisco.criarPesquisas(sqlgopainelrisco, cdsgopainelrisco,'select * from gopainelrisco', '', '', flwpOpcoes, flwpPesquisas,pesquisasLista, nil, cdsfiltros);
      { gerando objetos de pesquisas<<< }

      { >>>campos obrigatorios gopainelrisco }
      camposObrigatoriosgopainelrisco[0] := dbegopainelriscodescricao;
      camposObrigatoriosgopainelrisco[1] := dbegopainelriscoDATA;
      { campos obrigatorios gopainelrisco<<< }

      { >>>objetos ativos no modo de manipula��o de dados gopainelrisco }
      objetosModoEdicaoAtivosgopainelrisco[0] := pnlDadosgopainelrisco;
      objetosModoEdicaoAtivosgopainelrisco[1] := spdCancelargopainelrisco;
      objetosModoEdicaoAtivosgopainelrisco[2] := spdGravargopainelrisco;
      objetosModoEdicaoAtivosgopainelrisco[3] := dbegopainelriscoCODPROJETO;
      objetosModoEdicaoAtivosgopainelrisco[4] := btnprojeto;
      objetosModoEdicaoAtivosgopainelrisco[5] := dbegopainelriscoCODFILIAL;
      objetosModoEdicaoAtivosgopainelrisco[6] := btnfilial;
      objetosModoEdicaoAtivosgopainelrisco[7] := dbegopainelriscoCODPORTADOR;
      objetosModoEdicaoAtivosgopainelrisco[8] := btnPORTADOR;
      { objetos ativos no modo de manipula��o de dados gopainelrisco<<< }

      { >>>objetos inativos no modo de manipula��o de dados gopainelrisco }
      objetosModoEdicaoInativosgopainelrisco[0] := spdAdicionargopainelrisco;
      objetosModoEdicaoInativosgopainelrisco[1] := spdAlterargopainelrisco;
      objetosModoEdicaoInativosgopainelrisco[2] := spdExcluirgopainelrisco;
      objetosModoEdicaoInativosgopainelrisco[3] := spdOpcoes;
      objetosModoEdicaoInativosgopainelrisco[4] := tabPesquisas;
      objetosModoEdicaoInativosgopainelrisco[5] := dbegopainelriscoPROJETO;
      objetosModoEdicaoInativosgopainelrisco[6] := dbegopainelriscoFILIAL;
      objetosModoEdicaoInativosgopainelrisco[7] := dbegopainelriscoPORTADOR;
      { objetos inativos no modo de manipula��o de dados gopainelrisco<<< }

      { >>>comando de adi��o de teclas de atalhos gopainelrisco }
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Adicionar novo registro', VK_F4, spdAdicionargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Alterar registro selecionado', VK_F5, spdAlterargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Gravar �ltimas altera��es', VK_F6, spdGravargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Cancelar �ltimas altera��e', VK_F7, spdCancelargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Excluir registro selecionado', ShortCut(VK_DELETE, [ssCtrl]),spdExcluirgopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Duplicar registro selecionado', ShortCut(vk_f12, [ssCtrl]),spdDuplicargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Exportar Grid Excel', ShortCut(vk_f9, [ssCtrl]),spdExportargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Consolidar Data Compet�ncia', ShortCut(vk_f12, [ssCtrl]),spdconsolidargopainelrisco.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'[Selecionar Todo(s)]', ShortCut(vk_f1, [ssCtrl]),spdmarcartodos.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'[Inverter Sele��o]', ShortCut(vk_f2, [ssCtrl]),spddesmarcartodos.OnClick);
      fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'[Excluir Selecionado(s)]', ShortCut(vk_f3, [ssCtrl]),spdexcluirselecionados.OnClick);

      { comando de adi��o de teclas de atalhos gopainelrisco<<< }

      pgcPrincipal.ActivePageIndex := 0;

      { >>>abertura dos datasets }
      cdsgopainelrisco.open();
      { abertura dos datasets<<< }
      {filtros projeto}

      if ParamStr(4) <>'99' then
       fncgopainelrisco.filtrar(cdsgopainelrisco,'CODPROJETO='+ParamStr(4)) ;
      {filtros projeto}


      fncgopainelrisco.criaAtalhoPopupMenuNavegacao(Popupgopainelrisco,cdsgopainelrisco);

      fncgopainelrisco.definirMascaraCampos(cdsgopainelrisco);

      fncgopainelrisco.configurarGridesFormulario(fgopainelrisco, false, true);

    end);

end;

procedure Tfgopainelrisco.spdAdicionargopainelriscoClick(Sender: TObject);
begin
  pgcPrincipal.ActivePageIndex := tabgopainelrisco.TabIndex;
  if (fncgopainelrisco.adicionar(cdsgopainelrisco) = true) then
  begin
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, true);
    dbegopainelriscodescricao.SetFocus;
  end
  else
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, false);
end;

procedure Tfgopainelrisco.spdAlterargopainelriscoClick(Sender: TObject);
begin
  pgcPrincipal.ActivePageIndex := tabgopainelrisco.TabIndex;
  if (fncgopainelrisco.alterar(cdsgopainelrisco) = true) then
  begin
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, true);
    dbegopainelriscodescricao.SetFocus;
  end
  else
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, false);
end;

procedure Tfgopainelrisco.spdGravargopainelriscoClick(Sender: TObject);
begin
  fncgopainelrisco.verificarCamposObrigatorios(cdsgopainelrisco,
    camposObrigatoriosgopainelrisco);

  if (fncgopainelrisco.gravar(cdsgopainelrisco) = true) then
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, false)
  else
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, true);
end;

procedure Tfgopainelrisco.spdCancelargopainelriscoClick(Sender: TObject);
begin
  if (fncgopainelrisco.cancelar(cdsgopainelrisco) = true) then
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, false)
  else
    fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,
      objetosModoEdicaoInativosgopainelrisco, true);
end;

procedure Tfgopainelrisco.spdconsolidargopainelriscoClick(Sender: TObject);
var
Trans : TTransactionDesc;
begin
       try
         if (fdtcompetencia = nil) then Application.CreateForm(Tfdtcompetencia, fdtcompetencia);
         fdtcompetencia.ShowModal;

         if ((fdtcompetencia.dbecodprojeto.Text=EmptyStr) or (fdtcompetencia.dtcompetencia.Text=EmptyStr)) then
         abort;

         if fdtcompetencia.ModalResult=mrOk then
         begin
          Application.ProcessMessages;
          Trans.TransactionID  := 1;
          Trans.IsolationLevel := xilREADCOMMITTED;
          spPKG_CONSOLIDA_RISCO.sqlConnection.StartTransaction(Trans);
          spPKG_CONSOLIDA_RISCO.Close;
          spPKG_CONSOLIDA_RISCO.ParamByName('DTCOMPETENCIA').AsDate := fdtcompetencia.dtcompetencia.Date;
          spPKG_CONSOLIDA_RISCO.ParamByName('ID_PROJETO').asInteger := strtoInt(fdtcompetencia.dbecodprojeto.Text);
          spPKG_CONSOLIDA_RISCO.ExecProc;
          spPKG_CONSOLIDA_RISCO.SQLConnection.Commit(Trans);
          application.MessageBox('Applica��o realizada com sucesso.','Consolida��o Compet�ncia', MB_OK+64);
         end;
       except

       on Exc:Exception do
        begin
          spPKG_CONSOLIDA_RISCO.sqlConnection.Rollback(Trans);
        end;

       end;
end;

procedure Tfgopainelrisco.spddesmarcartodosClick(Sender: TObject);
begin
   try
    if cdsgopainelrisco.IsEmpty then abort;
    cdsgopainelrisco.DisableControls;

    cdsgopainelrisco.First;

    while not cdsgopainelrisco.Eof do
    begin
     cdsgopainelrisco.Edit;
     if cdsgopainelriscoselecionado.AsBoolean= true then
       cdsgopainelriscoselecionado.AsBoolean:= false;
     cdsgopainelrisco.Post;
     cdsgopainelrisco.Next;
    end;
   finally
    cdsgopainelrisco.EnableControls;
   end;

end;

procedure Tfgopainelrisco.spdDuplicargopainelriscoClick(Sender: TObject);
begin
  if not cdsgopainelrisco.IsEmpty then
  fncgopainelrisco.Duplicarregistro(cdsgopainelrisco, true,'ID','G_GOPAINELRISCO');
end;

procedure Tfgopainelrisco.spdExcluirgopainelriscoClick(Sender: TObject);
begin
  fncgopainelrisco.excluir(cdsgopainelrisco);
end;

procedure Tfgopainelrisco.spdexcluirselecionadosClick(Sender: TObject);
begin
  try

   cdsgopainelrisco.Filter := 'selecionado = ' + 'True';
   cdsgopainelrisco.Filtered := True;

   cdsgopainelrisco.First;
   while not cdsgopainelrisco.Eof do
   begin

      fncgopainelrisco.excluir(cdsgopainelrisco,true);

     cdsgopainelrisco.First;
   end;
  finally
   cdsgopainelrisco.Filtered := False;
  end;
end;

procedure Tfgopainelrisco.spdExportargopainelriscoClick(Sender: TObject);
begin
    if not cdsgopainelrisco.IsEmpty then
    fncgopainelrisco.exportarGrides(fgopainelrisco, GridResultadoPesquisa);

end;

procedure Tfgopainelrisco.cdsgopainelriscoBeforePost(DataSet: TDataSet);
begin
  if cdsgopainelrisco.State in [dsinsert] then
    cdsgopainelriscoID.Value := fncgopainelrisco.autoNumeracaoGenerator(dm.conexao, 'G_gopainelrisco');

end;

procedure Tfgopainelrisco.cdsgopainelriscoCalcFields(DataSet: TDataSet);
begin
  cdsgopainelriscoVLIMITEUTILIZADO.AsString := FloatToStr((cdsgopainelriscoVLVENCIDOS.AsFloat + cdsgopainelriscoVLVENCER.AsFloat));
  cdsgopainelriscoVLIMITEDISPONIVEL.AsFloat := (cdsgopainelriscoVLIMITE.AsFloat - cdsgopainelriscoVLIMITEUTILIZADO.AsFloat);

  cdsgopainelriscoVLIMITEUTILIZADODESCONTO.AsString := FloatToStr((cdsgopainelriscoVLVENCIDOSDESCONTO.AsFloat + cdsgopainelriscoVLVENCERDESCONTO.AsFloat));
  cdsgopainelriscoVLIMITEDISPONIVELDESCONTO.AsFloat := (cdsgopainelriscoVLLIMITEDESCONTO.AsFloat - cdsgopainelriscoVLIMITEUTILIZADODESCONTO.AsFloat);

  cdsgopainelriscoVLIMITEUTILIZADOCOMISSARIA.AsString := FloatToStr((cdsgopainelriscoVLVENCIDOSCOMISSARIA.AsFloat + cdsgopainelriscoVLVENCERCOMISSARIA.AsFloat));
  cdsgopainelriscoVLIMITEDISPONIVELCOMISSARIA.AsFloat := (cdsgopainelriscoVLLIMITECOMISSSARIA.AsFloat - cdsgopainelriscoVLIMITEUTILIZADOCOMISSARIA.AsFloat);

 cdsgopainelriscoAGRUPADOR.AsString:=   cdsgopainelriscoCODPROJETO.AsString+'-'+ cdsgopainelriscoDATA.AsString;

end;

procedure Tfgopainelrisco.sessao;
var host: string; arquivo: tinifile;
begin

    try
   //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);


    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;


procedure Tfgopainelrisco.altenarPopupTabSheet(Sender: TObject);
begin
  if (Sender is Tcxtabsheet) then fgopainelrisco.PopupMenu := (Sender as Tcxtabsheet).PopupMenu;
end;

procedure Tfgopainelrisco.spdImpressaoClick(Sender: TObject);
begin
  popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgopainelrisco.spdmarcartodosClick(Sender: TObject);
begin
  try
   if cdsgopainelrisco.IsEmpty then abort;
   cdsgopainelrisco.DisableControls;
   cdsgopainelrisco.First;

   while not cdsgopainelrisco.Eof do
   begin
     cdsgopainelrisco.Edit;
     cdsgopainelriscoselecionado.AsBoolean:= true;
     cdsgopainelrisco.Post;
     cdsgopainelrisco.Next;
   end;
   finally
    cdsgopainelrisco.EnableControls;
   end;
end;

procedure Tfgopainelrisco.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgopainelrisco.menuImpRelSinteticoClick(Sender: TObject);
begin
  if not cdsgopainelrisco.IsEmpty then
  begin
    fncgopainelrisco.visualizarRelatorios(frxpainel1);
  end;
end;

procedure Tfgopainelrisco.psqportadorPreencherCampos(Sender: TObject);
begin
   if ((cdsgopainelriscoCODPORTADOR.AsInteger >0)  and (cdsgopainelriscoCODPROJETO.AsInteger > 0))  then
   begin
     dm.FDQuery.Close;
     dm.FDQuery.sql.Clear;
     dm.FDQuery.sql.Add('SELECT COALESCE(LIMITE_FOMENTO,0)LIMITE_FOMENTO FROM GOPROJETOFUNDO WHERE CODPROJETO=:CODPROJETO');
     dm.FDQuery.sql.Add('AND CODPORTADOR=:CODPORTADOR');
     dm.FDQuery.ParamByName('CODPROJETO').AsInteger   := cdsgopainelriscoCODPROJETO.AsInteger;
     dm.FDQuery.ParamByName('CODPORTADOR').AsInteger  := cdsgopainelriscoCODPORTADOR.AsInteger;
     dm.FDQuery.Open();
     cdsgopainelriscoVLIMITE.AsFloat  :=  dm.FDQuery.FieldByName('LIMITE_FOMENTO').AsFloat;

   end;

end;

procedure Tfgopainelrisco.RelatrioII1Click(Sender: TObject);
begin
  if not cdsgopainelrisco.IsEmpty then
    fncgopainelrisco.visualizarRelatorios(frxpainel2);
end;

procedure Tfgopainelrisco.RelatrioIII1Click(Sender: TObject);
begin
  if not cdsgopainelrisco.IsEmpty then
    fncgopainelrisco.visualizarRelatorios(frxpainel3);
end;

end.
