unit ugogrupoprojeto;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 09/08/2021*}                                          
{*Hora 20:25:45*}                                            
{*Unit gogrupoprojeto *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr;

type                                                                                        
	Tfgogrupoprojeto = class(TForm)
    sqlgogrupoprojeto: TSQLDataSet;
	dspgogrupoprojeto: TDataSetProvider;
	cdsgogrupoprojeto: TClientDataSet;
	dsgogrupoprojeto: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgogrupoprojeto: TfrxReport;
	frxDBgogrupoprojeto: TfrxDBDataset;
	Popupgogrupoprojeto: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgogrupoprojeto: tcxTabSheet;
	SCRgogrupoprojeto: TScrollBox;
	pnlDadosgogrupoprojeto: TPanel;
	{>>>bot�es de manipula��o de dados gogrupoprojeto}
	pnlLinhaBotoesgogrupoprojeto: TPanel;
	spdAdicionargogrupoprojeto: TButton;
	spdAlterargogrupoprojeto: TButton;
	spdGravargogrupoprojeto: TButton;
	spdCancelargogrupoprojeto: TButton;
	spdExcluirgogrupoprojeto: TButton;

	{bot�es de manipula��o de dadosgogrupoprojeto<<<}
	{campo ID}
	sqlgogrupoprojetoID: TIntegerField;
	cdsgogrupoprojetoID: TIntegerField;
	pnlLayoutLinhasgogrupoprojeto1: TPanel;
	pnlLayoutTitulosgogrupoprojetoid: TPanel;
	pnlLayoutCamposgogrupoprojetoid: TPanel;
	dbegogrupoprojetoid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgogrupoprojetoDT_CADASTRO: TDateField;
	cdsgogrupoprojetoDT_CADASTRO: TDateField;
	pnlLayoutLinhasgogrupoprojeto2: TPanel;
	pnlLayoutTitulosgogrupoprojetodt_cadastro: TPanel;
	pnlLayoutCamposgogrupoprojetodt_cadastro: TPanel;
	dbegogrupoprojetodt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgogrupoprojetoHS_CADASTRO: TTimeField;
	cdsgogrupoprojetoHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgogrupoprojeto3: TPanel;
	pnlLayoutTitulosgogrupoprojetohs_cadastro: TPanel;
	pnlLayoutCamposgogrupoprojetohs_cadastro: TPanel;
	dbegogrupoprojetohs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlgogrupoprojetoDESCRICAO: TStringField;
	cdsgogrupoprojetoDESCRICAO: TStringField;
	pnlLayoutLinhasgogrupoprojeto4: TPanel;
	pnlLayoutTitulosgogrupoprojetodescricao: TPanel;
	pnlLayoutCamposgogrupoprojetodescricao: TPanel;
	dbegogrupoprojetodescricao: TDBEdit;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    frxDBDfiltros: TfrxDBDataset;
    cdsfiltros: TClientDataSet;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargogrupoprojetoClick(Sender: TObject);
	procedure spdAlterargogrupoprojetoClick(Sender: TObject);
	procedure spdGravargogrupoprojetoClick(Sender: TObject);
	procedure spdCancelargogrupoprojetoClick(Sender: TObject);
	procedure spdExcluirgogrupoprojetoClick(Sender: TObject);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdsgogrupoprojetoNewRecord(DataSet: TDataSet);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgogrupoprojeto: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgogrupoprojeto: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgogrupoprojeto: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgogrupoprojeto: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagogrupoprojeto: Array[0..0]  of TDuplicidade;
	duplicidadeCampogogrupoprojeto: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fgogrupoprojeto: Tfgogrupoprojeto;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgogrupoprojeto.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgogrupoprojeto.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgogrupoprojeto.TabIndex;
end;

procedure Tfgogrupoprojeto.cdsgogrupoprojetoNewRecord(DataSet: TDataSet);
begin
	if cdsgogrupoprojeto.State in [dsinsert] then
	cdsgogrupoprojetoID.Value:=fncgogrupoprojeto.autoNumeracaoGenerator(dm.conexao,'G_gogrupoprojeto');
end;

procedure Tfgogrupoprojeto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgogrupoprojeto.verificarEmTransacao(cdsgogrupoprojeto);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgogrupoprojeto.configurarGridesFormulario(fgogrupoprojeto,true, false);

	fncgogrupoprojeto.Free;
	{eliminando container de fun��es da memoria<<<}

	fgogrupoprojeto:=nil;
	Action:=cafree;
end;

procedure Tfgogrupoprojeto.FormCreate(Sender: TObject);
begin
  sessao;

  {<<>> fun��o de tradu�a� de componentes>>>}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  {<<>> fun��o de tradu�a� de componentes>>>}

	{>>>container de fun��es}
	fncgogrupoprojeto:= TFuncoes.Create(Self);
	fncgogrupoprojeto.definirConexao(dm.conexao);
	fncgogrupoprojeto.definirConexaohistorico(dm.conexao);
	fncgogrupoprojeto.definirFormulario(Self);
	fncgogrupoprojeto.validarTransacaoRelacionamento:=true;
	fncgogrupoprojeto.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
  duplicidadeCampogogrupoprojeto:=TDuplicidade.Create;
	duplicidadeCampogogrupoprojeto.agrupador:='';
	duplicidadeCampogogrupoprojeto.objeto :=dbegogrupoprojetoDESCRICAO;
	duplicidadeListagogrupoprojeto[0]:=duplicidadeCampogogrupoprojeto;
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

    {gerando campos no clientdatsaet de filtros}
  fncgogrupoprojeto.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncgogrupoprojeto.criarPesquisas(sqlgogrupoprojeto,cdsgogrupoprojeto,'select * from gogrupoprojeto', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gogrupoprojeto}
  camposObrigatoriosgogrupoprojeto[0]:=dbegogrupoprojetoDESCRICAO;
	{campos obrigatorios gogrupoprojeto<<<}

	{>>>objetos ativos no modo de manipula��o de dados gogrupoprojeto}
	objetosModoEdicaoAtivosgogrupoprojeto[0]:=pnlDadosgogrupoprojeto;
	objetosModoEdicaoAtivosgogrupoprojeto[1]:=spdCancelargogrupoprojeto;
	objetosModoEdicaoAtivosgogrupoprojeto[2]:=spdGravargogrupoprojeto;
	{objetos ativos no modo de manipula��o de dados gogrupoprojeto<<<}

	{>>>objetos inativos no modo de manipula��o de dados gogrupoprojeto}
	objetosModoEdicaoInativosgogrupoprojeto[0]:=spdAdicionargogrupoprojeto;
	objetosModoEdicaoInativosgogrupoprojeto[1]:=spdAlterargogrupoprojeto;
	objetosModoEdicaoInativosgogrupoprojeto[2]:=spdExcluirgogrupoprojeto;
	objetosModoEdicaoInativosgogrupoprojeto[3]:=spdOpcoes;
	objetosModoEdicaoInativosgogrupoprojeto[7]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gogrupoprojeto<<<}

	{>>>comando de adi��o de teclas de atalhos gogrupoprojeto}
	fncgogrupoprojeto.criaAtalhoPopupMenu(Popupgogrupoprojeto,'Adicionar novo registro',VK_F4,spdAdicionargogrupoprojeto.OnClick);
	fncgogrupoprojeto.criaAtalhoPopupMenu(Popupgogrupoprojeto,'Alterar registro selecionado',VK_F5,spdAlterargogrupoprojeto.OnClick);
	fncgogrupoprojeto.criaAtalhoPopupMenu(Popupgogrupoprojeto,'Gravar �ltimas altera��es',VK_F6,spdGravargogrupoprojeto.OnClick);
	fncgogrupoprojeto.criaAtalhoPopupMenu(Popupgogrupoprojeto,'Cancelar �ltimas altera��e',VK_F7,spdCancelargogrupoprojeto.OnClick);
	fncgogrupoprojeto.criaAtalhoPopupMenu(Popupgogrupoprojeto,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgogrupoprojeto.OnClick);

	{comando de adi��o de teclas de atalhos gogrupoprojeto<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgogrupoprojeto.open();
	{abertura dos datasets<<<}

	fncgogrupoprojeto.criaAtalhoPopupMenuNavegacao(Popupgogrupoprojeto,cdsgogrupoprojeto);

	fncgogrupoprojeto.definirMascaraCampos(cdsgogrupoprojeto);

	fncgogrupoprojeto.configurarGridesFormulario(fgogrupoprojeto,false, true);

end;

procedure Tfgogrupoprojeto.spdAdicionargogrupoprojetoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgogrupoprojeto.TabIndex;
	if (fncgogrupoprojeto.adicionar(cdsgogrupoprojeto)=true) then
		begin
			fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,true);
			dbegogrupoprojetodescricao.SetFocus;
	end
	else
		fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,false);
end;

procedure Tfgogrupoprojeto.spdAlterargogrupoprojetoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgogrupoprojeto.TabIndex;
	if (fncgogrupoprojeto.alterar(cdsgogrupoprojeto)=true) then
		begin
			fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,true);
			dbegogrupoprojetodescricao.SetFocus;
	end
	else
		fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,false);
end;

procedure Tfgogrupoprojeto.spdGravargogrupoprojetoClick(Sender: TObject);
begin

  fncgogrupoprojeto.verificarCamposObrigatorios(cdsgogrupoprojeto, camposObrigatoriosgogrupoprojeto, pgcPrincipal);
	fncgogrupoprojeto.verificarDuplicidade(dm.FDConexao, cdsgogrupoprojeto, 'gofilial', 'ID', duplicidadeListagogrupoprojeto);

	if (fncgogrupoprojeto.gravar(cdsgogrupoprojeto)=true) then
		fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,false)
	else
		fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,true);

end;

procedure Tfgogrupoprojeto.spdCancelargogrupoprojetoClick(Sender: TObject);
begin
	if (fncgogrupoprojeto.cancelar(cdsgogrupoprojeto)=true) then
		fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,false)
	else
		fncgogrupoprojeto.ativarModoEdicao(objetosModoEdicaoAtivosgogrupoprojeto,objetosModoEdicaoInativosgogrupoprojeto,true);
end;

procedure Tfgogrupoprojeto.spdExcluirgogrupoprojetoClick(Sender: TObject);
begin
	fncgogrupoprojeto.excluir(cdsgogrupoprojeto);
end;

procedure Tfgogrupoprojeto.sessao;
begin
   try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;



procedure Tfgogrupoprojeto.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgogrupoprojeto.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgogrupoprojeto.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgogrupoprojeto.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgogrupoprojeto.IsEmpty then 
	fncgogrupoprojeto.visualizarRelatorios(frxgogrupoprojeto);
end;

end.

