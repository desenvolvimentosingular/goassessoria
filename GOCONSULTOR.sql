CREATE TABLE GOCONSULTOR(
ID Integer   
,
CONSTRAINT pk_GOCONSULTOR PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
COD_SUPERVISOR Integer   
,
SUPERVISOR Varchar(250)   COLLATE PT_BR
,
NOME Varchar(250)  not null COLLATE PT_BR
,
CPF Varchar(30)  not null COLLATE PT_BR
,
E_MAIL1 Varchar(250)   COLLATE PT_BR
,
E_MAIL2 Varchar(250)   COLLATE PT_BR
,
CLASSIFICACAO Varchar(250)  not null COLLATE PT_BR
,
ENDERECO Varchar(250)   COLLATE PT_BR
,
UF Varchar(2)   COLLATE PT_BR
,
CIDADE Varchar(250)   COLLATE PT_BR
,
CEP Varchar(10)   COLLATE PT_BR
,
STATUS Varchar(10)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOCO_ULTOR_ID ON GOCONSULTOR (ID);
CREATE ASC INDEX I02_GOCO_ULTOR_DT_CADASTRO ON GOCONSULTOR (DT_CADASTRO);
CREATE ASC INDEX I03_GOCO_ULTOR_HS_CADASTRO ON GOCONSULTOR (HS_CADASTRO);
CREATE ASC INDEX I04_GOCO_ULTOR_COD_SUPERVISOR ON GOCONSULTOR (COD_SUPERVISOR);
CREATE ASC INDEX I05_GOCO_ULTOR_SUPERVISOR ON GOCONSULTOR (SUPERVISOR);
CREATE ASC INDEX I06_GOCO_ULTOR_NOME ON GOCONSULTOR (NOME);
CREATE ASC INDEX I07_GOCO_ULTOR_CPF ON GOCONSULTOR (CPF);
CREATE ASC INDEX I08_GOCO_ULTOR_E_MAIL1 ON GOCONSULTOR (E_MAIL1);
CREATE ASC INDEX I09_GOCO_ULTOR_E_MAIL2 ON GOCONSULTOR (E_MAIL2);
CREATE ASC INDEX I10_GOCO_ULTOR_CLASSIFICACAO ON GOCONSULTOR (CLASSIFICACAO);
CREATE ASC INDEX I11_GOCO_ULTOR_ENDERECO ON GOCONSULTOR (ENDERECO);
CREATE ASC INDEX I12_GOCO_ULTOR_UF ON GOCONSULTOR (UF);
CREATE ASC INDEX I13_GOCO_ULTOR_CIDADE ON GOCONSULTOR (CIDADE);
CREATE ASC INDEX I14_GOCO_ULTOR_CEP ON GOCONSULTOR (CEP);
CREATE ASC INDEX I15_GOCO_ULTOR_STATUS ON GOCONSULTOR (STATUS);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�digo Supervisor' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'COD_SUPERVISOR';
update rdb$relation_fields set rdb$description = 'Supervisor' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'SUPERVISOR';
update rdb$relation_fields set rdb$description = 'Nome' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'NOME';
update rdb$relation_fields set rdb$description = 'CPF' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'CPF';
update rdb$relation_fields set rdb$description = 'E_Mail' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'E_MAIL1';
update rdb$relation_fields set rdb$description = 'E_Mail' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'E_MAIL2';
update rdb$relation_fields set rdb$description = 'Classifica��o' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'CLASSIFICACAO';
update rdb$relation_fields set rdb$description = 'Endere�o' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'ENDERECO';
update rdb$relation_fields set rdb$description = 'UF' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'UF';
update rdb$relation_fields set rdb$description = 'Cidade' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'CIDADE';
update rdb$relation_fields set rdb$description = 'CEP' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'CEP';
update rdb$relation_fields set rdb$description = 'Status' where rdb$relation_name = 'GOCONSULTOR' and rdb$field_name = 'STATUS';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
