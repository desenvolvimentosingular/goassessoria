unit ugocontrolerecebivel;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 10/08/2021*}                                          
{*Hora 01:28:39*}                                            
{*Unit gocontrolerecebivel *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, uPesquisa, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, cxCurrencyEdit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  frxChart, cxCheckBox;

type                                                                                        
	Tfgocontrolerecebivel = class(TForm)
    sqlgocontrolerecebivel: TSQLDataSet;
	dspgocontrolerecebivel: TDataSetProvider;
	cdsgocontrolerecebivel: TClientDataSet;
	dsgocontrolerecebivel: TDataSource;
	popupImpressao: TPopupMenu;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxDBgocontrolerecebivel: TfrxDBDataset;
	Popupgocontrolerecebivel: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgocontrolerecebivel: tcxTabSheet;
	SCRgocontrolerecebivel: TScrollBox;
	{>>>bot�es de manipula��o de dados gocontrolerecebivel}
	pnlLinhaBotoesgocontrolerecebivel: TPanel;
	spdAdicionargocontrolerecebivel: TButton;
	spdAlterargocontrolerecebivel: TButton;
	spdGravargocontrolerecebivel: TButton;
	spdCancelargocontrolerecebivel: TButton;
	spdExcluirgocontrolerecebivel: TButton;

	{bot�es de manipula��o de dadosgocontrolerecebivel<<<}
	{campo ID}
	sqlgocontrolerecebivelID: TIntegerField;
	cdsgocontrolerecebivelID: TIntegerField;

	{campo DT_CADASTRO}
	sqlgocontrolerecebivelDT_CADASTRO: TDateField;
	cdsgocontrolerecebivelDT_CADASTRO: TDateField;

	{campo HS_CADASTRO}
	sqlgocontrolerecebivelHS_CADASTRO: TTimeField;
	cdsgocontrolerecebivelHS_CADASTRO: TTimeField;

	{campo CODPORTADOR}
	sqlgocontrolerecebivelCODPORTADOR: TIntegerField;
	cdsgocontrolerecebivelCODPORTADOR: TIntegerField;

	{campo PORTADOR}
	sqlgocontrolerecebivelPORTADOR: TStringField;
	cdsgocontrolerecebivelPORTADOR: TStringField;

	{campo CODFILIAL}
	sqlgocontrolerecebivelCODFILIAL: TIntegerField;
	cdsgocontrolerecebivelCODFILIAL: TIntegerField;

	{campo FILIAL}
	sqlgocontrolerecebivelFILIAL: TStringField;
	cdsgocontrolerecebivelFILIAL: TStringField;

	{campo DTCOMPETENCIA}
	sqlgocontrolerecebivelDTCOMPETENCIA: TDateField;
	cdsgocontrolerecebivelDTCOMPETENCIA: TDateField;

	{campo CODCARTEIRA}
	sqlgocontrolerecebivelCODCARTEIRA: TIntegerField;
	cdsgocontrolerecebivelCODCARTEIRA: TIntegerField;

	{campo CARTEIRA}
	sqlgocontrolerecebivelCARTEIRA: TStringField;
	cdsgocontrolerecebivelCARTEIRA: TStringField;

	{campo CODTIPO}
	sqlgocontrolerecebivelCODTIPO: TIntegerField;
	cdsgocontrolerecebivelCODTIPO: TIntegerField;

	{campo TIPO}
	sqlgocontrolerecebivelTIPO: TStringField;
	cdsgocontrolerecebivelTIPO: TStringField;

	{campo NUMBORDERO}
	sqlgocontrolerecebivelNUMBORDERO: TFloatField;
	cdsgocontrolerecebivelNUMBORDERO: TFloatField;

	{campo QTTITULOS}
	sqlgocontrolerecebivelQTTITULOS: TIntegerField;
	cdsgocontrolerecebivelQTTITULOS: TIntegerField;

	{campo VLBRUTOBORDERO}
	sqlgocontrolerecebivelVLBRUTOBORDERO: TFloatField;
	cdsgocontrolerecebivelVLBRUTOBORDERO: TFloatField;

	{campo QTDRECUSADA}
	sqlgocontrolerecebivelQTDRECUSADA: TIntegerField;
	cdsgocontrolerecebivelQTDRECUSADA: TIntegerField;

	{campo VLBRUTORECUSADO}
	sqlgocontrolerecebivelVLBRUTORECUSADO: TFloatField;
	cdsgocontrolerecebivelVLBRUTORECUSADO: TFloatField;

	{campo PRAZOMEDIO}
	sqlgocontrolerecebivelPRAZOMEDIO: TFloatField;
	cdsgocontrolerecebivelPRAZOMEDIO: TFloatField;

	{campo PRAZOMEDIOACORDADO}
	sqlgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField;
	cdsgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField;

	{campo TAC}
	sqlgocontrolerecebivelTAC: TFloatField;
	cdsgocontrolerecebivelTAC: TFloatField;

	{campo TARIFAENTRADATITULOS}
	sqlgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField;
	cdsgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField;

	{campo DESAGIOACORDADO}
	sqlgocontrolerecebivelDESAGIOACORDADO: TFloatField;
	cdsgocontrolerecebivelDESAGIOACORDADO: TFloatField;

	{campo CALCULOIOF}
	sqlgocontrolerecebivelCALCULOIOF: TFloatField;
	cdsgocontrolerecebivelCALCULOIOF: TFloatField;

	{campo OUTRASDESPESAS}
	sqlgocontrolerecebivelOUTRASDESPESAS: TFloatField;
	cdsgocontrolerecebivelOUTRASDESPESAS: TFloatField;

	{campo PERCTAXANOMINALACORDADA}
	sqlgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField;
	cdsgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField;

	{campo VLCOBERTURAFORMENTO}
	sqlgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField;
	cdsgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField;

	{campo VLDESTINADOENCARGOFOMENTO}
	sqlgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField;

	{campo VLDESTINADORECOMPORATITULO}
	sqlgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField;
	cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField;

	{campo VLDESTINADODEFASAGEM}
	sqlgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField;
	cdsgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField;

	{campo VLDESTINADOCOMISSARIA}
	sqlgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField;

	{campo VLDESTINADOGARANTIA}
	sqlgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField;

	{campo VLDESTINADORETENCAODIVIDA}
	sqlgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField;

	{campo VLDESTINADOCONTAGRAFICA}
	sqlgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField;
	cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField;

	{campo CUSTOSFORAOPERACAO}
	sqlgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField;
	cdsgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField;

	{campo VLOUTROSCREDITOS}
	sqlgocontrolerecebivelVLOUTROSCREDITOS: TFloatField;
	cdsgocontrolerecebivelVLOUTROSCREDITOS: TFloatField;

	{campo VLRECEBIDOCAIXA}
	sqlgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField;
	cdsgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField;

	{campo DTCREDITO}
	sqlgocontrolerecebivelDTCREDITO: TDateField;
	cdsgocontrolerecebivelDTCREDITO: TDateField;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    imglogo: TImage;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pgcInterno: TcxPageControl;
    tabprincipal: TcxTabSheet;
    tabbordero: TcxTabSheet;
    tabcustos: TcxTabSheet;
    cdsfiltros: TClientDataSet;
    psqfilial: TPesquisa;
    psqportador: TPesquisa;
    psqcarteira: TPesquisa;
    psqtipo: TPesquisa;
    pnlDadosgocontrolerecebivelbordero: TPanel;
    pnlLayoutLinhasgocontrolerecebivel13: TPanel;
    pnlLayoutTitulosgocontrolerecebivelnumbordero: TPanel;
    pnlLayoutCamposgocontrolerecebivelnumbordero: TPanel;
    dbegocontrolerecebivelNUMBORDERO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel14: TPanel;
    pnlLayoutTitulosgocontrolerecebivelqttitulos: TPanel;
    pnlLayoutCamposgocontrolerecebivelqttitulos: TPanel;
    dbegocontrolerecebivelQTTITULOS: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel15: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlbrutobordero: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlbrutobordero: TPanel;
    dbegocontrolerecebivelVLBRUTOBORDERO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel16: TPanel;
    pnlLayoutTitulosgocontrolerecebivelqtdrecusada: TPanel;
    pnlLayoutCamposgocontrolerecebivelqtdrecusada: TPanel;
    dbegocontrolerecebivelQTDRECUSADA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel17: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlbrutorecusado: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlbrutorecusado: TPanel;
    dbegocontrolerecebivelVLBRUTORECUSADO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel18: TPanel;
    pnlLayoutTitulosgocontrolerecebivelprazomedio: TPanel;
    pnlLayoutCamposgocontrolerecebivelprazomedio: TPanel;
    dbegocontrolerecebivelPRAZOMEDIO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel19: TPanel;
    pnlLayoutTitulosgocontrolerecebivelprazomedioacordado: TPanel;
    pnlLayoutCamposgocontrolerecebivelprazomedioacordado: TPanel;
    dbegocontrolerecebivelPRAZOMEDIOACORDADO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel25: TPanel;
    pnlLayoutTitulosgocontrolerecebivelperctaxanominalacordada: TPanel;
    pnlLayoutCamposgocontrolerecebivelperctaxanominalacordada: TPanel;
    dbegocontrolerecebivelPERCTAXANOMINALACORDADA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel24: TPanel;
    pnlLayoutTitulosgocontrolerecebiveloutrasdespesas: TPanel;
    pnlLayoutCamposgocontrolerecebiveloutrasdespesas: TPanel;
    dbegocontrolerecebivelOUTRASDESPESAS: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel23: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcalculoiof: TPanel;
    pnlLayoutCamposgocontrolerecebivelcalculoiof: TPanel;
    dbegocontrolerecebivelCALCULOIOF: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel22: TPanel;
    pnlLayoutTitulosgocontrolerecebiveldesagioacordado: TPanel;
    pnlLayoutCamposgocontrolerecebiveldesagioacordado: TPanel;
    dbegocontrolerecebivelDESAGIOACORDADO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel21: TPanel;
    pnlLayoutTitulosgocontrolerecebiveltarifaentradatitulos: TPanel;
    pnlLayoutCamposgocontrolerecebiveltarifaentradatitulos: TPanel;
    dbegocontrolerecebivelTARIFAENTRADATITULOS: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel20: TPanel;
    pnlLayoutTitulosgocontrolerecebiveltac: TPanel;
    pnlLayoutCamposgocontrolerecebiveltac: TPanel;
    dbegocontrolerecebivelTAC: TDBEdit;
    tabutilizacaobordero: TcxTabSheet;
    tabconferenciacaixa: TcxTabSheet;
    pnlLayoutLinhasgocontrolerecebivel26: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlcoberturaformento: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlcoberturaformento: TPanel;
    dbegocontrolerecebivelVLCOBERTURAFORMENTO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel27: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadoencargofomento: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadoencargofomento: TPanel;
    dbegocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel28: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadorecomporatitulo: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadorecomporatitulo: TPanel;
    dbegocontrolerecebivelVLDESTINADORECOMPORATITULO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel29: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadodefasagem: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadodefasagem: TPanel;
    dbegocontrolerecebivelVLDESTINADODEFASAGEM: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel30: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadocomissaria: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadocomissaria: TPanel;
    dbegocontrolerecebivelVLDESTINADOCOMISSARIA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel31: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadogarantia: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadogarantia: TPanel;
    dbegocontrolerecebivelVLDESTINADOGARANTIA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel32: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadoretencaodivida: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadoretencaodivida: TPanel;
    dbegocontrolerecebivelVLDESTINADORETENCAODIVIDA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel33: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvldestinadocontagrafica: TPanel;
    pnlLayoutCamposgocontrolerecebivelvldestinadocontagrafica: TPanel;
    dbegocontrolerecebivelVLDESTINADOCONTAGRAFICA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel34: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcustosforaoperacao: TPanel;
    pnlLayoutCamposgocontrolerecebivelcustosforaoperacao: TPanel;
    dbegocontrolerecebivelCUSTOSFORAOPERACAO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel35: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvloutroscreditos: TPanel;
    pnlLayoutCamposgocontrolerecebivelvloutroscreditos: TPanel;
    dbegocontrolerecebivelVLOUTROSCREDITOS: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel36: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlrecebidocaixa: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlrecebidocaixa: TPanel;
    dbegocontrolerecebivelVLRECEBIDOCAIXA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel37: TPanel;
    pnlLayoutTitulosgocontrolerecebiveldtcredito: TPanel;
    pnlLayoutCamposgocontrolerecebiveldtcredito: TPanel;
    pnlDadosgocontrolerecebivel: TPanel;
    pnlLayoutLinhasgocontrolerecebivel1: TPanel;
    pnlLayoutTitulosgocontrolerecebivelid: TPanel;
    pnlLayoutCamposgocontrolerecebivelid: TPanel;
    dbegocontrolerecebivelID: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel2: TPanel;
    pnlLayoutTitulosgocontrolerecebiveldt_cadastro: TPanel;
    pnlLayoutCamposgocontrolerecebiveldt_cadastro: TPanel;
    dbegocontrolerecebivelDT_CADASTRO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel3: TPanel;
    pnlLayoutTitulosgocontrolerecebivelhs_cadastro: TPanel;
    pnlLayoutCamposgocontrolerecebivelhs_cadastro: TPanel;
    dbegocontrolerecebivelHS_CADASTRO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel4: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodportador: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodportador: TPanel;
    dbegocontrolerecebivelCODPORTADOR: TDBEdit;
    btnportador: TButton;
    pnlLayoutCamposgocontrolerecebivelportador: TPanel;
    dbegocontrolerecebivelPORTADOR: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel6: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodfilial: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodfilial: TPanel;
    dbegocontrolerecebivelCODFILIAL: TDBEdit;
    btnfilial: TButton;
    pnlLayoutCamposgocontrolerecebivelfilial: TPanel;
    dbegocontrolerecebivelFILIAL: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel8: TPanel;
    pnlLayoutTitulosgocontrolerecebiveldtcompetencia: TPanel;
    pnlLayoutCamposgocontrolerecebiveldtcompetencia: TPanel;
    dbegocontrolerecebiveldtcompetencia: TcxDBDateEdit;
    pnlLayoutLinhasgocontrolerecebivel9: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodcarteira: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodcarteira: TPanel;
    dbegocontrolerecebivelCODCARTEIRA: TDBEdit;
    btncarteira: TButton;
    pnlLayoutCamposgocontrolerecebivelcarteira: TPanel;
    dbegocontrolerecebivelCARTEIRA: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel11: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodtipo: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodtipo: TPanel;
    dbegocontrolerecebivelCODTIPO: TDBEdit;
    btntipo: TButton;
    pnlLayoutCamposgocontrolerecebiveltipo: TPanel;
    dbegocontrolerecebivelTIPO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel39: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlbrutofinalbordero: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlbrutofinalbordero: TPanel;
    dbegocontrolerecebivelVLBRUTOFINALBORDERO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel40: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlqtfinaltitulos: TPanel;
    pnlLayoutCamposgocontrolerecebivelqtfinaltitulos: TPanel;
    dbegocontrolerecebivelQTFINALTITULOS: TDBEdit;
    sqlgocontrolerecebivelADVALOREM: TFloatField;
    cdsgocontrolerecebivelADVALOREM: TFloatField;
    pnlLayoutLinhasgocontrolerecebivel41: TPanel;
    pnlLayoutTitulosgocontrolerecebiveladvalorem: TPanel;
    pnlLayoutCamposgocontrolerecebiveladvalorem: TPanel;
    dbegocontrolerecebivelADDVALOREM: TDBEdit;
    cdsgocontrolerecebivelVLDESPESAS_ENCARGOS: TFloatField;
    cdsgocontrolerecebivelVLLIQUIDOCALCULADO: TFloatField;
    spdDuplicargocontrolerecebivel: TButton;
    cdsgocontrolerecebivelVLLIQUIDOINFORMADO: TFloatField;
    pnlLayoutLinhasgocontrolerecebivel42: TPanel;
    pnlLayoutTitulosgocontrolerecebiveldespesasencargos: TPanel;
    pnlLayoutCamposgocontrolerecebiveldespesasencargos: TPanel;
    dbegocontrolerecebivelDESPESASENCARGOS: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel43: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvalorliquidocalculado: TPanel;
    pnlLayoutCamposgocontrolerecebivelvalorliquidoacordado: TPanel;
    dbegocontrolerecebivelVLLIQUIDOACORDADO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel44: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlliquidoinformado: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlliquidoinformado: TPanel;
    dbegocontrolerecebivelVLLIQUIDOINFORMADO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel45: TPanel;
    pnlLayoutTitulosgocontrolerecebivelCHECKVLLIQUIDO: TPanel;
    pnlLayoutCamposgocontrolerecebivelCHECKVLLIQUIDO: TPanel;
    dbegocontrolerecebivelCHECKVLLIQUIDO: TDBEdit;
    cdsgocontrolerecebivelSTATUS: TStringField;
    pnlLayoutLinhasgocontrolerecebivel46: TPanel;
    pnlLayoutTitulosgocontrolerecebivelSTATUS: TPanel;
    pnlLayoutCamposgocontrolerecebivelOK: TPanel;
    dbegocontrolerecebivelOK: TDBEdit;
    spdPesquisargocontrolerecebivel: TButton;
    cdscomponentes: TClientDataSet;
    cdscomponentesID: TIntegerField;
    cdscomponentesNOME: TStringField;
    cdscomponentesTIPO: TStringField;
    cdscomponentesVISIBLE: TBooleanField;
    cdscomponentesHINT: TStringField;
    cdscomponentesENABLE: TBooleanField;
    cdsgocontrolerecebivelVLCALCULADOCAIXA: TFloatField;
    pnlLayoutLinhasgocontrolerecebivel47: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlcalculadocaixa: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlcalculadocaixa: TPanel;
    dbegocontrolerecebivelVLCALCULADOCAIXA: TDBEdit;
    cdsgocontrolerecebivelVLSALDORECEBER: TFloatField;
    pnlLayoutLinhasgocontrolerecebivel48: TPanel;
    pnlLayoutTitulosgocontrolerecebivelsaldoreceber: TPanel;
    pnlLayoutCamposgocontrolerecebivelsaldoreceber: TPanel;
    dbegocontrolerecebivelSALDORECEBER: TDBEdit;
    dbegocontrolerecebivelDTCREDITO: TcxDBDateEdit;
    cdsgocontrolerecebivelTAXAEFETIVADMAIS: TFloatField;
    cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOADVALOREM: TFloatField;
    cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOACORDADO: TFloatField;
    cdsgocontrolerecebivelVLCUSTOTITULO: TFloatField;
    pnlLayoutLinhasgocontrolerecebivel49: TPanel;
    pnlLayoutTitulosgocontrolerecebivelTAXAEFETIVADMAIS: TPanel;
    pnlLayoutCamposgocontrolerecebivelTAXAEFETIVADMAIS: TPanel;
    pnlLayoutLinhasgocontrolerecebivel50: TPanel;
    pnlLayoutTitulosgocontrolerecebivelTXCUSTONOMINALDESAGIOADVALOR: TPanel;
    pnlLayoutCamposgocontrolerecebivelTXCUSTONOMINALDESAGIOADVALORE: TPanel;
    pnlLayoutLinhasgocontrolerecebivel51: TPanel;
    pnlLayoutTitulosgocontrolerecebivelTXCUSTONOMINALDESAGIOACORDAD: TPanel;
    pnlLayoutCamposgocontrolerecebivelTXCUSTONOMINALDESAGIOACORDADO: TPanel;
    pnlLayoutLinhasgocontrolerecebivel52: TPanel;
    pnlLayoutTitulosgocontrolerecebivelVLCUSTOTITULO: TPanel;
    pnlLayoutCamposgocontrolerecebivelVLCUSTOTITULO: TPanel;
    dbegocontrolerecebivelTAXAEFETIVADMAIS: TDBEdit;
    dbegocontrolerecebivelTXCUSTONOMINALDESAGIOADVALOREM: TDBEdit;
    dbegocontrolerecebivelTXCUSTONOMINALDESAGIOACORDADO: TDBEdit;
    dbegocontrolerecebivelVLCUSTOTITULO: TDBEdit;
    menuImpRel1: TMenuItem;
    sqlgocontrolerecebivelVLCALCULADOCAIXA: TFloatField;
    GridResultadoPesquisaDBTVVLBRUTOFINALBORDERO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVPRAZOMEDIO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVPRAZOMEDIOACORDADO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVADVALOREM: TcxGridDBColumn;
    GridResultadoPesquisaDBTVTAC: TcxGridDBColumn;
    GridResultadoPesquisaDBTVTARIFAENTRADATITULOS: TcxGridDBColumn;
    GridResultadoPesquisaDBTVDESAGIOACORDADO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVCALCULOIOF: TcxGridDBColumn;
    GridResultadoPesquisaDBTVOUTRASDESPESAS: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESPESAS_ENCARGOS: TcxGridDBColumn;
    GridResultadoPesquisaDBTVPERCTAXANOMINALACORDADA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLLIQUIDOCALCULADO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLLIQUIDOINFORMADO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVSTATUS: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLCOBERTURAFORMENTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADOENCARGOFOMENTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADORECOMPORATITULO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADODEFASAGEM: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADOCOMISSARIA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADOGARANTIA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADORETENCAODIVIDA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLDESTINADOCONTAGRAFICA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVCUSTOSFORAOPERACAO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLOUTROSCREDITOS: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLRECEBIDOCAIXA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLSALDORECEBER: TcxGridDBColumn;
    GridResultadoPesquisaDBTVDTCREDITO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVTXCUSTONOMINALDESAGIOACORDADO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVTXCUSTONOMINALDESAGIOADVALOREM: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLCUSTOTITULO: TcxGridDBColumn;
    cdsgocontrolerecebivelMULTIPLICADORDMAIS0: TFloatField;
    cdsgocontrolerecebivelPRAZOMEDIOEMPRESA: TFloatField;
    cdsgocontrolerecebivelMULTIPLICADORPMFLOAT: TFloatField;
    cdsgocontrolerecebivelVLMEDIORECEBIVEIS: TFloatField;
    pnlLayoutLinhasgocontrolerecebivel53: TPanel;
    pnlLayoutTitulosgocontrolerecebivelMULTIPLICADORD0: TPanel;
    pnlLayoutCamposgocontrolerecebivelmultiplicadordo: TPanel;
    dbegocontrolerecebivelMULTIPLICADORDO: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel54: TPanel;
    pnlLayoutTitulosgocontrolerecebivelMULTIPLICADORPMFLOAT: TPanel;
    pnlLayoutCamposgocontrolerecebivelmultiplicadorpmfloat: TPanel;
    dbegocontrolerecebivelMULTIPLICADORMPFLOAT: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel55: TPanel;
    pnlLayoutTitulosgocontrolerecebivelprazomedioempresa: TPanel;
    pnlLayoutCamposgocontrolerecebivelprazomedioempresa: TPanel;
    dbegocontrolerecebivelprazomedioempresa: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel56: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvalormediorecebiveis: TPanel;
    pnlLayoutCamposgocontrolerecebivelvalormediorecebiveis: TPanel;
    dbegocontrolerecebivelvalormediorecebiveis: TDBEdit;
    pnlLayoutLinhasgocontrolerecebivel57: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodprojeto: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodprojeto: TPanel;
    dbegocontrolerecebivelCODROJETO: TDBEdit;
    btnprojeto: TButton;
    pnlLayoutCamposgocontrolerecebivelprojeto: TPanel;
    dbegocontrolerecebivelPROJETO: TDBEdit;
    psqprojeto: TPesquisa;
    sqlgocontrolerecebivelCODPROJETO: TIntegerField;
    sqlgocontrolerecebivelPROJETO: TStringField;
    cdsgocontrolerecebivelCODPROJETO: TIntegerField;
    cdsgocontrolerecebivelPROJETO: TStringField;
    frxgofaturamentogrupo: TfrxReport;
    frxgofaturamentomarca: TfrxReport;
    frxgofaturamentofilial: TfrxReport;
    frxgofaturamentovenda: TfrxReport;
    sqlgocontrolerecebivelQTFINALTITULOS: TIntegerField;
    sqlgocontrolerecebivelVLBRUTOFINALBORDERO: TFloatField;
    cdsgocontrolerecebivelQTFINALTITULOS: TIntegerField;
    cdsgocontrolerecebivelVLBRUTOFINALBORDERO: TFloatField;
    GridResultadoPesquisaDBTVQTFINALTITULOS: TcxGridDBColumn;
    spdxlsgocontrolerecebivel: TButton;
    GridResultadoPesquisaDBTVPRAZOMEDIOEMPRESA: TcxGridDBColumn;
    GridResultadoPesquisaDBTVVLMEDIORECEBIVEIS: TcxGridDBColumn;
    pnlLayoutLinhasgocontrolerecebivel: TPanel;
    pnlLayoutTitulosgocontrolerecebivelvlmediorecebiveis: TPanel;
    pnlLayoutCamposgocontrolerecebivelvlmediorecebiveis: TPanel;
    dbegocontrolerecebivelVLMEDIORECEBIVEIS: TDBEdit;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    lblusuarioresultado: TLabel;
    lblProjetoresultado: TLabel;
    GridResultadoPesquisaDBTVCODPROJETO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVPROJETO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVCODFILIAL: TcxGridDBColumn;
    GridResultadoPesquisaDBTVFILIAL: TcxGridDBColumn;
    lblversao: TLabel;
    spdmarcartodos: TButton;
    spddesmarcartodos: TButton;
    spdexcluirselecionados: TButton;
    cdsgocontrolerecebivelSELECIONADO: TBooleanField;
    GridResultadoPesquisaDBTVSELECIONADO: TcxGridDBColumn;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargocontrolerecebivelClick(Sender: TObject);
	procedure spdAlterargocontrolerecebivelClick(Sender: TObject);
	procedure spdGravargocontrolerecebivelClick(Sender: TObject);
	procedure spdCancelargocontrolerecebivelClick(Sender: TObject);
	procedure spdExcluirgocontrolerecebivelClick(Sender: TObject);
    procedure cdsgocontrolerecebivelNewRecord(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure pgcInternoChange(Sender: TObject);
    procedure cdsgocontrolerecebivelCalcFields(DataSet: TDataSet);
    procedure spdDuplicargocontrolerecebivelClick(Sender: TObject);
    procedure spdPesquisargocontrolerecebivelClick(Sender: TObject);
    procedure cdsgocontrolerecebivelVLCALCULADOCAIXAChange(Sender: TField);
    procedure spdOpcoesClick(Sender: TObject);
    procedure dbegocontrolerecebivelVLCOBERTURAFORMENTOExit(Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADOCONTAGRAFICAExit(
      Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADOENCARGOFOMENTOExit(
      Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADORECOMPORATITULOExit(
      Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADODEFASAGEMExit(Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADOCOMISSARIAExit(Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADOGARANTIAExit(Sender: TObject);
    procedure dbegocontrolerecebivelVLDESTINADORETENCAODIVIDAExit(
      Sender: TObject);
    procedure dbegocontrolerecebivelCUSTOSFORAOPERACAOExit(Sender: TObject);
    procedure dbegocontrolerecebivelVLOUTROSCREDITOSExit(Sender: TObject);
    procedure menuImpRel1Click(Sender: TObject);
    procedure menuImpRel2Click(Sender: TObject);
    procedure psqprojetoPreencherCampos(Sender: TObject);
    procedure GridResultadoPesquisaDBTVVLBRUTOFINALBORDEROCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVQTFINALBORDEROCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVVLDESPESAS_ENCARGOSCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVVLLIQUIDOCALCULADOCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVVLLIQUIDOINFORMADOCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVVLSALDORECEBERCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVTAXAEFETIVADMAISCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVTXCUSTONOMINALDESAGIOACORDADOCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVTXCUSTONOMINALDESAGIOADVALOREMCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVVLCUSTOTITULOCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure spdxlsgocontrolerecebivelClick(Sender: TObject);
    procedure GridResultadoPesquisaDBTVPRAZOMEDIOEMPRESACustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridResultadoPesquisaDBTVVLMEDIORECEBIVEISCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure spdmarcartodosClick(Sender: TObject);
    procedure spddesmarcartodosClick(Sender: TObject);
    procedure spdexcluirselecionadosClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgocontrolerecebivel: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgocontrolerecebivel: Array[0..10]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgocontrolerecebivel: Array[0..15]  of TControl;
	objetosModoEdicaoInativosgocontrolerecebivel: Array[0..15]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagocontrolerecebivel: Array[0..2]  of TDuplicidade;
	duplicidadeCampogocontrolerecebivel: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
    procedure VLCALCULADOCAIXA;

	public


end;

var
fgocontrolerecebivel: Tfgocontrolerecebivel;

implementation

uses udm, Vcl.DialogMessage, ufiltro_avancado, System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfgocontrolerecebivel.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVDblClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocontrolerecebivel.TabIndex;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVPRAZOMEDIOEMPRESACustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVQTFINALBORDEROCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVTAXAEFETIVADMAISCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVTXCUSTONOMINALDESAGIOACORDADOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVTXCUSTONOMINALDESAGIOADVALOREMCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLBRUTOFINALBORDEROCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLCUSTOTITULOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLDESPESAS_ENCARGOSCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLLIQUIDOCALCULADOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLLIQUIDOINFORMADOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLMEDIORECEBIVEISCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;

procedure Tfgocontrolerecebivel.GridResultadoPesquisaDBTVVLSALDORECEBERCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
    if aviewInfo.Text<>EmptyStr then
     begin
      acanvas.Brush.Color:=$00C09F7E;
     end;
end;



procedure Tfgocontrolerecebivel.cdsgocontrolerecebivelCalcFields(
  DataSet: TDataSet);
begin

    cdsgocontrolerecebivelVLDESPESAS_ENCARGOS.AsFloat               := (cdsgocontrolerecebivelTAC.AsFloat  +
                                                                    cdsgocontrolerecebivelTARIFAENTRADATITULOS.asfloat+
                                                                    cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat+
                                                                    cdsgocontrolerecebivelCALCULOIOF.AsFloat+
                                                                    cdsgocontrolerecebivelADVALOREM.AsFloat+
                                                                    cdsgocontrolerecebivelOUTRASDESPESAS.AsFloat);

    cdsgocontrolerecebivelVLLIQUIDOCALCULADO.AsFloat                := (cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat-cdsgocontrolerecebivelVLDESPESAS_ENCARGOS.AsFloat);

    cdsgocontrolerecebivelVLLIQUIDOINFORMADO.AsFloat                := cdsgocontrolerecebivelVLLIQUIDOCALCULADO.AsFloat;


    cdsgocontrolerecebivelVLSALDORECEBER.AsFloat                    :=(cdsgocontrolerecebivelVLCALCULADOCAIXA.asfloat-
                                                                    cdsgocontrolerecebivelVLRECEBIDOCAIXA.AsFloat);

    cdsgocontrolerecebivelMULTIPLICADORDMAIS0.AsFloat               :=(cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat * cdsgocontrolerecebivelPRAZOMEDIO.AsFloat);

    cdsgocontrolerecebivelMULTIPLICADORPMFLOAT.AsFloat              :=(cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat * cdsgocontrolerecebivelPRAZOMEDIOACORDADO.AsFloat);

    cdsgocontrolerecebivelPRAZOMEDIOEMPRESA.AsFloat                 :=(cdsgocontrolerecebivelMULTIPLICADORDMAIS0.AsFloat/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat);

    cdsgocontrolerecebivelTAXAEFETIVADMAIS.AsFloat                  :=((((cdsgocontrolerecebivelADVALOREM.AsFloat + cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat)/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat)/(cdsgocontrolerecebivelPRAZOMEDIOEMPRESA.AsFloat/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat))*30);

    cdsgocontrolerecebivelVLMEDIORECEBIVEIS.AsFloat                 := (cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat/cdsgocontrolerecebivelQTFINALTITULOS.AsFloat);


    cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOACORDADO.AsFloat     :=(((cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat/cdsgocontrolerecebivelPRAZOMEDIOACORDADO.AsFloat)*30)/ cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat);

    cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOADVALOREM.AsFloat    :=((((cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat + cdsgocontrolerecebivelADVALOREM.AsFloat)/ (cdsgocontrolerecebivelPRAZOMEDIOACORDADO.AsFloat))*30)/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat);

    cdsgocontrolerecebivelVLCUSTOTITULO.AsFloat                     :=(cdsgocontrolerecebivelTARIFAENTRADATITULOS.AsFloat/ cdsgocontrolerecebivelQTFINALTITULOS.AsFloat);


end;

procedure Tfgocontrolerecebivel.cdsgocontrolerecebivelNewRecord(DataSet: TDataSet);
begin
	if cdsgocontrolerecebivel.State in [dsinsert] then
	cdsgocontrolerecebivelID.Value:=fncgocontrolerecebivel.autoNumeracaoGenerator(dm.conexao,'G_gocontrolerecebivel');
end;

procedure Tfgocontrolerecebivel.cdsgocontrolerecebivelVLCALCULADOCAIXAChange(
  Sender: TField);
begin
   cdsgocontrolerecebivelVLRECEBIDOCAIXA.AsFloat  := cdsgocontrolerecebivelVLCALCULADOCAIXA.AsFloat;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelCUSTOSFORAOPERACAOExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLCOBERTURAFORMENTOExit(
  Sender: TObject);
begin
   VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADOCOMISSARIAExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADOCONTAGRAFICAExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADODEFASAGEMExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADOENCARGOFOMENTOExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADOGARANTIAExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADORECOMPORATITULOExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLDESTINADORETENCAODIVIDAExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.dbegocontrolerecebivelVLOUTROSCREDITOSExit(
  Sender: TObject);
begin
  VLCALCULADOCAIXA;
end;

procedure Tfgocontrolerecebivel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgocontrolerecebivel.verificarEmTransacao(cdsgocontrolerecebivel);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgocontrolerecebivel.configurarGridesFormulario(fgocontrolerecebivel,true, false);

	fncgocontrolerecebivel.Free;
	{eliminando container de fun��es da memoria<<<}

	fgocontrolerecebivel:=nil;
	Action:=cafree;
end;

procedure Tfgocontrolerecebivel.FormCreate(Sender: TObject);
begin
   sessao;
  {>>>container de fun��es}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;

  dm.FDParametros.Close;
  dm.FDParametros.Open();
  dm.FDParametros.DisableControls;
  try
    dm.FDParametros.Filtered := False;
    dm.FDParametros.FilterOptions := [foNoPartialCompare];
    dm.FDParametros.Filter := 'UPPER(PARAMETRO)='+UPPERCASE(QuotedStr('ACESSOPORPROJETO')) + ' AND  UPPER(VALOR)='+UPPERCASE(QuotedStr('S'));
    dm.FDParametros.Filtered := True;
  finally
    dm.FDParametros.First;
    dm.FDParametros.EnableControls;
  end;


  if dm.FDParametros.RecordCount>0 then
  begin
    psqprojeto.SQL.Clear;
    psqprojeto.SQL.Add(' select ID, DESCRICAO                   ');
    psqprojeto.SQL.Add('from GOPROJETOS                         ');
    psqprojeto.SQL.Add('where ID in (select C.CODPROJETO        ');
    psqprojeto.SQL.Add('          from GOPROJETOCONSULTOR C     ');
    psqprojeto.SQL.Add('         where C.consultor ='+QuotedStr(ParamStr(3))+')');
  end;

  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin

    fncgocontrolerecebivel:= TFuncoes.Create(Self);
    fncgocontrolerecebivel.definirConexao(dm.conexao);
    fncgocontrolerecebivel.definirConexaohistorico(dm.conexao);
    fncgocontrolerecebivel.definirFormulario(Self);
    fncgocontrolerecebivel.validarTransacaoRelacionamento:=true;
    fncgocontrolerecebivel.autoAplicarAtualizacoesBancoDados:=true;
    {container de fun��es<<<}
    lblversao.Caption:=lblversao.Caption+' - '+fncgocontrolerecebivel.recuperarVersaoExecutavel;
    {>>>verificar campos duplicidade}
    duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
    duplicidadeCampogocontrolerecebivel.agrupador:='';
    duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebivelportador;
    duplicidadeListagocontrolerecebivel[0]:=duplicidadeCampogocontrolerecebivel;

    duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
    duplicidadeCampogocontrolerecebivel.agrupador:='';
    duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebivelfilial;
    duplicidadeListagocontrolerecebivel[1]:=duplicidadeCampogocontrolerecebivel;

//    duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
//    duplicidadeCampogocontrolerecebivel.agrupador:='';
//    duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebiveldtcompetencia;
//    duplicidadeListagocontrolerecebivel[2]:=duplicidadeCampogocontrolerecebivel;

    duplicidadeCampogocontrolerecebivel:=TDuplicidade.Create;
    duplicidadeCampogocontrolerecebivel.agrupador:='';
    duplicidadeCampogocontrolerecebivel.objeto :=dbegocontrolerecebivelCODROJETO;
    duplicidadeListagocontrolerecebivel[2]:=duplicidadeCampogocontrolerecebivel;

    {verificar campos duplicidade<<<}

    {>>>gerando objetos de pesquisas}
    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesID';
    pesquisasItem.titulo:='C�digo';
    pesquisasItem.campo:='ID';
    pesquisasItem.tipoFDB:='Integer';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[0]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDT_CADASTRO';
    pesquisasItem.titulo:='Data de Cadastro';
    pesquisasItem.campo:='DT_CADASTRO';
    pesquisasItem.tipoFDB:='Date';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[1]:=pesquisasItem;



    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDTCOMPETENCIA';
    pesquisasItem.titulo:='Data de Compet�ncia';
    pesquisasItem.campo:='DTCOMPETENCIA';
    pesquisasItem.tipoFDB:='Date';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[2]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesPROJETO';
    pesquisasItem.titulo:='Projeto';
    pesquisasItem.campo:='PROJETO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    dm.FDQuery.close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.SQL.Add('select DESCRICAO from goprojetos');
    dm.FDQuery.Open();
    dm.FDQuery.First;
    while not dm.FDQuery.Eof do
    begin
    pesquisasItem.comboItens.add(dm.FDQuery.FieldByName('DESCRICAO').AsString);
    dm.FDQuery.Next;
    end;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[3]:=pesquisasItem;


    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesPORTADOR';
    pesquisasItem.titulo:='Portador';
    pesquisasItem.campo:='PORTADOR';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    dm.FDQuery.close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.SQL.Add('select DESCRICAO from goportador');
    dm.FDQuery.Open();
    dm.FDQuery.First;
    while not dm.FDQuery.Eof do
    begin
    pesquisasItem.comboItens.add(dm.FDQuery.FieldByName('DESCRICAO').AsString);
    dm.FDQuery.Next;
    end;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[4]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesFILIAL';
    pesquisasItem.titulo:='Filial';
    pesquisasItem.campo:='FILIAL';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    dm.FDQuery.close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.SQL.Add('select DESCRICAO from gofilial');
    dm.FDQuery.Open();
    dm.FDQuery.First;
    while not dm.FDQuery.Eof do
    begin
    pesquisasItem.comboItens.add(dm.FDQuery.FieldByName('DESCRICAO').AsString);
    dm.FDQuery.Next;
    end;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[5]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesCARTEIRA';
    pesquisasItem.titulo:='Cateira';
    pesquisasItem.campo:='CARTEIRA';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    dm.FDQuery.close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.SQL.Add('select DESCRICAO from GOCARTEIRA');
    dm.FDQuery.Open();
    dm.FDQuery.First;
    while not dm.FDQuery.Eof do
    begin
    pesquisasItem.comboItens.add(dm.FDQuery.FieldByName('DESCRICAO').AsString);
    dm.FDQuery.Next;
    end;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[6]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pestipo';
    pesquisasItem.titulo:='Tipo';
    pesquisasItem.campo:='TIPO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    dm.FDQuery.close;
    dm.FDQuery.sql.Clear;
    dm.FDQuery.SQL.Add('select DESCRICAO from gotipo');
    dm.FDQuery.Open();
    dm.FDQuery.First;
    while not dm.FDQuery.Eof do
    begin
    pesquisasItem.comboItens.add(dm.FDQuery.FieldByName('DESCRICAO').AsString);
    dm.FDQuery.Next;
    end;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[7]:=pesquisasItem;




   {gerando campos no clientdatsaet de filtros}
    fncgocontrolerecebivel.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
   {gerando campos no clientdatsaet de filtros}

    fncgocontrolerecebivel.criarPesquisas(sqlgocontrolerecebivel,cdsgocontrolerecebivel,'select * from gocontrolerecebivel', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
    {gerando objetos de pesquisas<<<}

    {>>>campos obrigatorios gocontrolerecebivel}
    camposObrigatoriosgocontrolerecebivel[0] :=dbegocontrolerecebiveldtcompetencia;
    camposObrigatoriosgocontrolerecebivel[1] :=dbegocontrolerecebivelCODROJETO;
    camposObrigatoriosgocontrolerecebivel[2] :=dbegocontrolerecebivelCODPORTADOR;
    camposObrigatoriosgocontrolerecebivel[3] :=dbegocontrolerecebivelCODFILIAL;
    {campos obrigatorios gocontrolerecebivel<<<}

    {>>>objetos ativos no modo de manipula��o de dados gocontrolerecebivel}
    objetosModoEdicaoAtivosgocontrolerecebivel[0]:=spdCancelargocontrolerecebivel;
    objetosModoEdicaoAtivosgocontrolerecebivel[1]:=spdGravargocontrolerecebivel;
    objetosModoEdicaoAtivosgocontrolerecebivel[2]:=dbegocontrolerecebivelCODPORTADOR;
    objetosModoEdicaoAtivosgocontrolerecebivel[3]:=dbegocontrolerecebivelCODFILIAL;
    objetosModoEdicaoAtivosgocontrolerecebivel[4]:=dbegocontrolerecebivelCODCARTEIRA;
    objetosModoEdicaoAtivosgocontrolerecebivel[5]:=dbegocontrolerecebivelCODTIPO;
    objetosModoEdicaoAtivosgocontrolerecebivel[6]:=dbegocontrolerecebivelCODROJETO;
    objetosModoEdicaoAtivosgocontrolerecebivel[7]:=btnportador;
    objetosModoEdicaoAtivosgocontrolerecebivel[8]:=btnfilial;
    objetosModoEdicaoAtivosgocontrolerecebivel[9]:=btncarteira;
    objetosModoEdicaoAtivosgocontrolerecebivel[10]:=btntipo;
    objetosModoEdicaoAtivosgocontrolerecebivel[11]:=btnprojeto;

   // objetosModoEdicaoAtivosgocontrolerecebivel[10]:=pnlDadosgocontrolerecebivel;

    {objetos ativos no modo de manipula��o de dados gocontrolerecebivel<<<}

    {>>>objetos inativos no modo de manipula��o de dados gocontrolerecebivel}
    objetosModoEdicaoInativosgocontrolerecebivel[0]:=spdAdicionargocontrolerecebivel;
    objetosModoEdicaoInativosgocontrolerecebivel[1]:=spdAlterargocontrolerecebivel;
    objetosModoEdicaoInativosgocontrolerecebivel[2]:=spdExcluirgocontrolerecebivel;
    objetosModoEdicaoInativosgocontrolerecebivel[3]:=spdOpcoes;
    objetosModoEdicaoInativosgocontrolerecebivel[4]:=tabPesquisas;
    objetosModoEdicaoInativosgocontrolerecebivel[5]:=dbegocontrolerecebivelPORTADOR;
    objetosModoEdicaoInativosgocontrolerecebivel[6]:=dbegocontrolerecebivelFILIAL;
    objetosModoEdicaoInativosgocontrolerecebivel[7]:=dbegocontrolerecebivelCARTEIRA;
    objetosModoEdicaoInativosgocontrolerecebivel[8]:=dbegocontrolerecebivelTIPO;
    objetosModoEdicaoInativosgocontrolerecebivel[9]:=dbegocontrolerecebivelPROJETO;
    {objetos inativos no modo de manipula��o de dados gocontrolerecebivel<<<}

    {>>>comando de adi��o de teclas de atalhos gocontrolerecebivel}
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Adicionar novo registro',VK_F4,spdAdicionargocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Alterar registro selecionado',VK_F5,spdAlterargocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Gravar �ltimas altera��es',VK_F6,spdGravargocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Cancelar �ltimas altera��e',VK_F7,spdCancelargocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'[Selecionar Todo(s)]', ShortCut(vk_f1, [ssCtrl]),spdmarcartodos.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'[Inverter Sele��o]', ShortCut(vk_f2, [ssCtrl]),spddesmarcartodos.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'[Excluir Selecionado(s)]', ShortCut(vk_f3, [ssCtrl]),spdexcluirselecionados.OnClick);

    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Duplicar registro selecionado',ShortCut(VK_F12,[ssCtrl]),spdDuplicargocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Pesquisar campo...',ShortCut(70,[ssCtrl]),spdPesquisargocontrolerecebivel.OnClick);
    fncgocontrolerecebivel.criaAtalhoPopupMenu(Popupgocontrolerecebivel,'Exportar Grid Excel',ShortCut(VK_F9,[ssCtrl]),spdxlsgocontrolerecebivel.OnClick);
    {comando de adi��o de teclas de atalhos gocontrolerecebivel<<<}

    pgcPrincipal.ActivePageIndex:=0;
    pgcInterno.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdsgocontrolerecebivel.open();
    {abertura dos datasets<<<}

    {filtros projeto}
   { if ParamStr(5) <>'4' then
    begin
     fncgocontrolerecebivel.filtrar(cdsgocontrolerecebivel,'CODPROJETO='+ParamStr(5));
    end;   }
    {filtros projeto}


    {filtros projeto}
     if ParamStr(4) <>'99' then
    fncgocontrolerecebivel.filtrar(cdsgocontrolerecebivel,'CODPROJETO='+ParamStr(4));
    {filtros projeto}

    fncgocontrolerecebivel.criaAtalhoPopupMenuNavegacao(Popupgocontrolerecebivel,cdsgocontrolerecebivel);

    fncgocontrolerecebivel.definirMascaraCampos(cdsgocontrolerecebivel);

    fncgocontrolerecebivel.configurarGridesFormulario(fgocontrolerecebivel,false, true);

    fncgocontrolerecebivel.listar_componentes(cdscomponentes, fgocontrolerecebivel);

  end);
end;

procedure Tfgocontrolerecebivel.spdAdicionargocontrolerecebivelClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex :=0;
  pgcInterno.ActivePageIndex   :=0;

	if (fncgocontrolerecebivel.adicionar(cdsgocontrolerecebivel)=true) then
		begin
			fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
			dbegocontrolerecebiveldtcompetencia.SetFocus;
	end
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false);
end;

procedure Tfgocontrolerecebivel.spdAlterargocontrolerecebivelClick(Sender: TObject);
begin
  pgcPrincipal.ActivePageIndex :=0;
  pgcInterno.ActivePageIndex :=0;
	if (fncgocontrolerecebivel.alterar(cdsgocontrolerecebivel)=true) then
		begin
			fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
			dbegocontrolerecebiveldtcompetencia.SetFocus;
	end
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false);
end;

procedure Tfgocontrolerecebivel.spdGravargocontrolerecebivelClick(Sender: TObject);
begin

	fncgocontrolerecebivel.verificarCamposObrigatorios(cdsgocontrolerecebivel, camposObrigatoriosgocontrolerecebivel, pgcInterno);
//	fncgocontrolerecebivel.verificarDuplicidade(dm.fdconexao, cdsgocontrolerecebivel, 'gocontrolerecebivel', 'ID', duplicidadeListagocontrolerecebivel);

	if (fncgocontrolerecebivel.gravar(cdsgocontrolerecebivel)=true) then
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false)
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
end;

procedure Tfgocontrolerecebivel.spdCancelargocontrolerecebivelClick(Sender: TObject);
begin
	if (fncgocontrolerecebivel.cancelar(cdsgocontrolerecebivel)=true) then
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,false)
	else
		fncgocontrolerecebivel.ativarModoEdicao(objetosModoEdicaoAtivosgocontrolerecebivel,objetosModoEdicaoInativosgocontrolerecebivel,true);
end;

procedure Tfgocontrolerecebivel.spddesmarcartodosClick(Sender: TObject);
begin
   try
    if cdsgocontrolerecebivel.IsEmpty then abort;
    cdsgocontrolerecebivel.DisableControls;

    cdsgocontrolerecebivel.First;

    while not cdsgocontrolerecebivel.Eof do
    begin
     cdsgocontrolerecebivel.Edit;
     if cdsgocontrolerecebivelSELECIONADO.AsBoolean= true then
       cdsgocontrolerecebivelSELECIONADO.AsBoolean:= false;
     cdsgocontrolerecebivel.Post;
     cdsgocontrolerecebivel.Next;
    end;
   finally
    cdsgocontrolerecebivel.EnableControls;
   end;
end;

procedure Tfgocontrolerecebivel.spdDuplicargocontrolerecebivelClick(
  Sender: TObject);
begin
  if not cdsgocontrolerecebivel.IsEmpty then
  fncgocontrolerecebivel.Duplicarregistro(cdsgocontrolerecebivel, true,'ID','G_GOCONTROLERECEBIVEL');

end;

procedure Tfgocontrolerecebivel.spdExcluirgocontrolerecebivelClick(Sender: TObject);
begin
	fncgocontrolerecebivel.excluir(cdsgocontrolerecebivel);
end;

procedure Tfgocontrolerecebivel.spdexcluirselecionadosClick(Sender: TObject);
begin
  try

   cdsgocontrolerecebivel.Filter := 'selecionado = ' + 'True';
   cdsgocontrolerecebivel.Filtered := True;

   cdsgocontrolerecebivel.First;
   while not cdsgocontrolerecebivel.Eof do
   begin
      fncgocontrolerecebivel.excluir(cdsgocontrolerecebivel,true);
     cdsgocontrolerecebivel.First;
   end;
  finally
   cdsgocontrolerecebivel.Filtered := False;
  end;
end;

procedure Tfgocontrolerecebivel.sessao;
var host: string; arquivo: tinifile;
begin
   try
   //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);


    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfgocontrolerecebivel.VLCALCULADOCAIXA;
begin
  if cdsgocontrolerecebivel.State in [dsedit, dsInsert] then
  cdsgocontrolerecebivelVLCALCULADOCAIXA.asfloat := cdsgocontrolerecebivelVLLIQUIDOCALCULADO.AsFloat -
                                                    cdsgocontrolerecebivelVLCOBERTURAFORMENTO.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADOCOMISSARIA.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADODEFASAGEM.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADOGARANTIA.AsFloat -
                                                    cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA.AsFloat -
                                                    cdsgocontrolerecebivelCUSTOSFORAOPERACAO.AsFloat +
                                                    cdsgocontrolerecebivelVLOUTROSCREDITOS.AsFloat;
end;

procedure Tfgocontrolerecebivel.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgocontrolerecebivel.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgocontrolerecebivel.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgocontrolerecebivel.spdmarcartodosClick(Sender: TObject);
begin
  try
   if cdsgocontrolerecebivel.IsEmpty then abort;
    cdsgocontrolerecebivel.DisableControls;
   cdsgocontrolerecebivel.First;

   while not cdsgocontrolerecebivel.Eof do
   begin
     cdsgocontrolerecebivel.Edit;
     cdsgocontrolerecebivelselecionado.AsBoolean:= true;
     cdsgocontrolerecebivel.Post;
     cdsgocontrolerecebivel.Next;
   end;
   finally
    cdsgocontrolerecebivel.EnableControls;
   end;
end;

procedure Tfgocontrolerecebivel.spdOpcoesClick(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgocontrolerecebivel.spdPesquisargocontrolerecebivelClick(
  Sender: TObject);
var
value : string;
 i  : integer;
begin
 value := inputbox('Pesquisar campo em tela..', 'Nome do campo', EmptyStr);
 if value<>EmptyStr then
 begin
     cdscomponentes.DisableControls;
      try
        cdscomponentes.Filtered := False;
        cdscomponentes.FilterOptions := [foNoPartialCompare];
        cdscomponentes.Filter := 'UPPER(HINT) LIKE ''%' + UPPERCASE(value) + '%''';
        cdscomponentes.Filtered := True;
      finally
        cdscomponentes.First;
        cdscomponentes.EnableControls;
      end;

   if cdscomponentes.IsEmpty then
   Application.MessageBox('Nenhum campo encontrado com a descri��o informada!!!','Alerta',MB_OK+MB_ICONINFORMATION)
  else
  begin
   if fgocontrolerecebivel.FindComponent(cdscomponentesNOME.AsString)<>nil then
    begin
     {if ((cdscomponentesVISIBLE.AsBoolean= false) or  (cdscomponentesENABLE.AsBoolean=false)) then
      begin
       Application.MessageBox('Campo informado invis�vel ou desativado!!!','Alerta',MB_OK+MB_ICONINFORMATION);
       abort;
      end;}

      pgcPrincipal.ActivePage:= tabgocontrolerecebivel;
      for I := 0 to fgocontrolerecebivel.ComponentCount-1 do
      begin
         if (fgocontrolerecebivel.Components[i] is TCustomEdit)  then
         if (fgocontrolerecebivel.Components[i] as TCustomEdit).Name= cdscomponentesNOME.AsString then
            begin
             if ( ((fgocontrolerecebivel.Components[i] as TCustomEdit).Visible=false) or ( (fgocontrolerecebivel.Components[i] as TCustomEdit).Enabled=false)) then
             begin
              Application.MessageBox('Campo informado invis�vel ou desativado!!!','Alerta',MB_OK+MB_ICONINFORMATION);
              abort;
             end;

             pgcInterno.ActivePageIndex :=(fgocontrolerecebivel.Components[i] as TCustomEdit).Tag;
             if  (fgocontrolerecebivel.Components[i] as TCustomEdit).Enabled then
             begin
              (fgocontrolerecebivel.Components[i] as TCustomEdit).SetFocus;
             end;
            end;

       if (fgocontrolerecebivel.Components[i] is TCustomComboBox)  then
         if (fgocontrolerecebivel.Components[i] as TCustomComboBox).Name= cdscomponentesNOME.AsString then
            begin
             pgcInterno.ActivePageIndex :=(fgocontrolerecebivel.Components[i] as TCustomComboBox).Tag;
             if  (fgocontrolerecebivel.Components[i] as TCustomComboBox).Enabled then
             begin
              (fgocontrolerecebivel.Components[i] as TCustomComboBox).SetFocus;
             end;
            end;
      end;
    end;

  end;
 end;

end;

procedure Tfgocontrolerecebivel.spdxlsgocontrolerecebivelClick(Sender: TObject);
begin
  if not cdsgocontrolerecebivel.IsEmpty then
  fncgocontrolerecebivel.exportarGrides(fgocontrolerecebivel,GridResultadoPesquisa);

end;

procedure Tfgocontrolerecebivel.menuImpRel1Click(Sender: TObject);

begin

   if (ffiltro_avancado = nil) then Application.CreateForm(Tffiltro_avancado, ffiltro_avancado);
   ffiltro_avancado.ShowModal;

end;

procedure Tfgocontrolerecebivel.menuImpRel2Click(Sender: TObject);
begin

   if (ffiltro_avancado = nil) then Application.CreateForm(Tffiltro_avancado, ffiltro_avancado);
    ffiltro_avancado.ShowModal;

end;

procedure Tfgocontrolerecebivel.pgcInternoChange(Sender: TObject);
begin
  SCRgocontrolerecebivel.VertScrollBar.Position :=  0;
end;

procedure Tfgocontrolerecebivel.psqprojetoPreencherCampos(Sender: TObject);
begin
 if cdsgocontrolerecebivelCODPROJETO.AsInteger >0 then
 begin
  psqportador.SQL.Clear;
  psqportador.SQL.Add('select ID, DESCRICAO FROM goportador WHERE 1=1 and ID IN (SELECT CODPORTADOR FROM goprojetofundo WHERE CODPROJETO='+cdsgocontrolerecebivelCODPROJETO.AsString+')');

  psqfilial.SQL.Clear;
  psqfilial.SQL.Add('select ID, DESCRICAO FROM GOFILIAL WHERE 1=1 and ID IN (SELECT CODFILIAL FROM goprojetofilial WHERE CODPROJETO='+cdsgocontrolerecebivelCODPROJETO.AsString+')');

 end;

end;

end.

