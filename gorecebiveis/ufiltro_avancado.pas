unit ufiltro_avancado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxGDIPlusClasses, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.StdCtrls, uPesquisa, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, udm, Vcl.DialogMessage, uFuncoes,
  frxClass, frxDBSet, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,cxGroupBox, cxRadioGroup, Vcl.Menus, cxButtons, Datasnap.DBClient,
  Datasnap.Provider, Dateutils, frxChart, Data.FMTBcd, Data.SqlExpr,
  ugocontrolerecebivel;

type
  Tffiltro_avancado = class(TForm)
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    imglogo: TImage;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    pnlLinhaBotoessecao: TPanel;
    spdimprimir: TButton;
    spdfechar: TButton;
    pnlDadosfiltros: TPanel;
    pnlLayoutLinhafiltro1: TPanel;
    lblprojeto: TLabel;
    btnPesquisarprojeto: TSpeedButton;
    dbecodprojeto: TEdit;
    dbeprojeto: TEdit;
    pnlLayoutLinhafiltro2: TPanel;
    lblfilial: TLabel;
    btnPesquisarfilial: TSpeedButton;
    dbeID_FILIAL: TEdit;
    dbeFILIAL: TEdit;
    pnlLayoutLinhafiltro3: TPanel;
    lblPORTADOR: TLabel;
    btnPesquisarFUNDO: TSpeedButton;
    dbeID_FUNDO: TEdit;
    dbeFUNDO: TEdit;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    pnlLayoutLinhafiltro4: TPanel;
    lblDTFATURAMENTO: TLabel;
    data1: TcxDateEdit;
    data2: TcxDateEdit;
    psqfundo: TPesquisa;
    psqprojeto: TPesquisa;
    rgrelatorio: TcxRadioGroup;
    spbdatesintetico: TcxButton;
    popmenudata: TPopupMenu;
    Hoje: TMenuItem;
    Ontem: TMenuItem;
    SemanaAtual: TMenuItem;
    SemanaAnterior: TMenuItem;
    UltimosQuizeDias: TMenuItem;
    N1: TMenuItem;
    MesAtual: TMenuItem;
    MesAnterior: TMenuItem;
    N2: TMenuItem;
    TrimestreAtual: TMenuItem;
    TrimestreAnterior: TMenuItem;
    N3: TMenuItem;
    SemestreAtual: TMenuItem;
    SemestreAnterior: TMenuItem;
    N4: TMenuItem;
    AnoAtual: TMenuItem;
    AnoAnterior: TMenuItem;
    N5: TMenuItem;
    LimparDatas: TMenuItem;
    pnlLayoutLinhafiltro5: TPanel;
    lblConsultor: TLabel;
    btnPesquisarCONSULTOR: TSpeedButton;
    dbeID_CONSULTOR: TEdit;
    dbeCONSULTOR: TEdit;
    psqgrupoproduto: TPesquisa;
    FDAnalisePortador: TFDQuery;
    frxDBanalisegeral: TfrxDBDataset;
    frxanalisegeral: TfrxReport;
    frxDBanaliseprojeto: TfrxDBDataset;
    cdsanaliseprojeto: TClientDataSet;
    dspanaliseprojeto: TDataSetProvider;
    FDAnaliseProjeto: TFDQuery;
    FDanaliseFilial: TFDQuery;
    dspanalisefilial: TDataSetProvider;
    cdsanalisefilial: TClientDataSet;
    FDportadoracumulado: TFDQuery;
    FDportadoracumuladoCODPORTADOR: TIntegerField;
    FDportadoracumuladoPORTADOR: TStringField;
    FDportadoracumuladoTOTAL_BORDERO: TIntegerField;
    FDportadoracumuladoQTTITULOS: TLargeintField;
    FDportadoracumuladoVLMEDIORECEBIVEIS: TFloatField;
    FDportadoracumuladoPM_EMPRESA: TFloatField;
    FDportadoracumuladoVLBRUTOBORDERO: TFloatField;
    FDportadoracumuladoVLLIQUIDOCALCULADO: TFloatField;
    FDportadoracumuladoDESPESAS_ENCARGOS: TFloatField;
    FDportadoracumuladoVLDESTINADORECOMPORATITULO: TFloatField;
    FDportadoracumuladoTARIFAENTRADATITULOS: TFloatField;
    FDportadoracumuladoVLOUTROSCREDITOS: TFloatField;
    dspportadoracumulado: TDataSetProvider;
    cdsportadoracumulado: TClientDataSet;
    cdsportadoracumuladoCODPORTADOR: TIntegerField;
    cdsportadoracumuladoPORTADOR: TStringField;
    cdsportadoracumuladoTOTAL_BORDERO: TIntegerField;
    cdsportadoracumuladoQTTITULOS: TLargeintField;
    cdsportadoracumuladoVLMEDIORECEBIVEIS: TFloatField;
    cdsportadoracumuladoPM_EMPRESA: TFloatField;
    cdsportadoracumuladoVLBRUTOBORDERO: TFloatField;
    cdsportadoracumuladoVLLIQUIDOCALCULADO: TFloatField;
    cdsportadoracumuladoDESPESAS_ENCARGOS: TFloatField;
    cdsportadoracumuladoVLDESTINADORECOMPORATITULO: TFloatField;
    cdsportadoracumuladoTARIFAENTRADATITULOS: TFloatField;
    cdsportadoracumuladoVLOUTROSCREDITOS: TFloatField;
    frxDBportadoracumulado: TfrxDBDataset;
    frxanaliseportadoracumulado: TfrxReport;
    frxportadoracumuladoGR: TfrxReport;
    frxChartpadrao: TfrxChartObject;
    frxanaliseProjeto1: TfrxReport;
    frxanaliseFilial: TfrxReport;
    frxDBanalisefilial: TfrxDBDataset;
    FDanaliseFilialCODFILIAL: TIntegerField;
    FDanaliseFilialFILIAL: TStringField;
    FDanaliseFilialTOTAL_BORDERO: TIntegerField;
    FDanaliseFilialQTTITULOS: TLargeintField;
    FDanaliseFilialVLMEDIORECEBIVEIS: TFloatField;
    FDanaliseFilialPM_EMPRESA: TFloatField;
    FDanaliseFilialVLBRUTOBORDERO: TFloatField;
    FDanaliseFilialVLLIQUIDOCALCULADO: TFloatField;
    FDanaliseFilialDESPESAS_ENCARGOS: TFloatField;
    FDanaliseFilialCUSTO_PM_EMPRESA: TFloatField;
    FDanaliseFilialCUSTO_NOMINAL: TFloatField;
    cdsanalisefilialCODFILIAL: TIntegerField;
    cdsanalisefilialFILIAL: TStringField;
    cdsanalisefilialTOTAL_BORDERO: TIntegerField;
    cdsanalisefilialQTTITULOS: TLargeintField;
    cdsanalisefilialVLMEDIORECEBIVEIS: TFloatField;
    cdsanalisefilialPM_EMPRESA: TFloatField;
    cdsanalisefilialVLBRUTOBORDERO: TFloatField;
    cdsanalisefilialVLLIQUIDOCALCULADO: TFloatField;
    cdsanalisefilialDESPESAS_ENCARGOS: TFloatField;
    cdsanalisefilialCUSTO_PM_EMPRESA: TFloatField;
    cdsanalisefilialCUSTO_NOMINAL: TFloatField;
    frxanaliseProjeto: TfrxReport;
    FDAnalisePortadorCODPORTADOR: TIntegerField;
    FDAnalisePortadorPORTADOR: TStringField;
    FDAnalisePortadorCODPROJETO: TIntegerField;
    FDAnalisePortadorPROJETO: TStringField;
    FDAnalisePortadorTOTAL_BORDERO: TIntegerField;
    FDAnalisePortadorQTTITULOS: TLargeintField;
    FDAnalisePortadorVLMEDIORECEBIVEIS: TFloatField;
    FDAnalisePortadorPM_EMPRESA: TFloatField;
    FDAnalisePortadorVLBRUTOBORDERO: TFloatField;
    FDAnalisePortadorVLLIQUIDOCALCULADO: TFloatField;
    FDAnalisePortadorDESPESAS_ENCARGOS: TFloatField;
    FDRecebivel: TFDQuery;
    FDRecebivelTOTAL_BORDERO: TIntegerField;
    FDRecebivelQTTITULOS: TLargeintField;
    FDRecebivelVLMEDIORECEBIVEIS: TFloatField;
    FDRecebivelPM_EMPRESA: TFloatField;
    FDRecebivelVLBRUTOBORDERO: TFloatField;
    FDRecebivelVLLIQUIDOCALCULADO: TFloatField;
    FDRecebivelDESPESAS_ENCARGOS: TFloatField;
    frxdbrecebivel: TfrxDBDataset;
    FDAnalisePortadorTAXA_NOMINAL: TStringField;
    FDAnalisePortadorCUSTOEFETIVO: TStringField;
    FDRecebivelTAXA_NOMINAL: TStringField;
    FDRecebivelCUSTOEFETIVO: TStringField;
    FDAnaliseProjetoCODPROJETO: TIntegerField;
    FDAnaliseProjetoPROJETO: TStringField;
    FDAnaliseProjetoTOTAL_BORDERO: TIntegerField;
    FDAnaliseProjetoQTTITULOS: TLargeintField;
    FDAnaliseProjetoVLMEDIORECEBIVEIS: TFloatField;
    FDAnaliseProjetoPM_EMPRESA: TFloatField;
    FDAnaliseProjetoVLBRUTOBORDERO: TFloatField;
    FDAnaliseProjetoVLLIQUIDOCALCULADO: TFloatField;
    FDAnaliseProjetoDESPESAS_ENCARGOS: TFloatField;
    FDAnaliseProjetoTAXA_NOMINAL: TStringField;
    FDAnaliseProjetoCUSTOEFETIVO: TStringField;
    cdsanaliseprojetoCODPROJETO: TIntegerField;
    cdsanaliseprojetoPROJETO: TStringField;
    cdsanaliseprojetoTOTAL_BORDERO: TIntegerField;
    cdsanaliseprojetoQTTITULOS: TLargeintField;
    cdsanaliseprojetoVLMEDIORECEBIVEIS: TFloatField;
    cdsanaliseprojetoPM_EMPRESA: TFloatField;
    cdsanaliseprojetoVLBRUTOBORDERO: TFloatField;
    cdsanaliseprojetoVLLIQUIDOCALCULADO: TFloatField;
    cdsanaliseprojetoDESPESAS_ENCARGOS: TFloatField;
    cdsanaliseprojetoTAXA_NOMINAL: TStringField;
    cdsanaliseprojetoCUSTOEFETIVO: TStringField;
    psqfilial: TPesquisa;
    procedure spdfecharClick(Sender: TObject);
    procedure dbecodprojetoKeyPress(Sender: TObject; var Key: Char);
    procedure dbeID_FUNDOKeyPress(Sender: TObject; var Key: Char);
    procedure dbeID_FILIALKeyPress(Sender: TObject; var Key: Char);
    procedure spdimprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spbdatesinteticoClick(Sender: TObject);
    procedure HojeClick(Sender: TObject);
    procedure OntemClick(Sender: TObject);
    procedure SemanaAtualClick(Sender: TObject);
    procedure SemanaAnteriorClick(Sender: TObject);
    procedure UltimosQuizeDiasClick(Sender: TObject);
    procedure MesAtualClick(Sender: TObject);
    procedure MesAnteriorClick(Sender: TObject);
    procedure TrimestreAtualClick(Sender: TObject);
    procedure SemestreAtualClick(Sender: TObject);
    procedure SemestreAnteriorClick(Sender: TObject);
    procedure AnoAtualClick(Sender: TObject);
    procedure AnoAnteriorClick(Sender: TObject);
    procedure LimparDatasClick(Sender: TObject);
    procedure dbeID_CONSULTORKeyPress(Sender: TObject; var Key: Char);
    procedure psqprojetoPreencherCampos(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
     procedure analisegeral;
     procedure analiseportadoracumulado;
     procedure analiseporprojeto;
     procedure analiseporfilial;
  public
  fncfiltros: TFuncoes;
  end;

var
  ffiltro_avancado: Tffiltro_avancado;

implementation

{$R *.dfm}

procedure Tffiltro_avancado.analisegeral;
  var consulta, group_by, where : string;
begin

     FDAnalisePortador.Close;
     FDAnalisePortador.SQL.Clear;

     consulta :=
      'SELECT                                                                                                                                                                               '+
      ' c.codportador                                                                                                                                                                       '+
      ' ,c.portador                                                                                                                                                                         '+
      ' ,c.codprojeto                                                                                                                                                                       '+
      ' ,c.projeto                                                                                                                                                                          '+
      ' , count(*)total_bordero                                                                                                                                                             '+
      ' ,sum(c.qttitulos) qttitulos                                                                                                                                                         '+
      ' ,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS,0)) VLMEDIORECEBIVEIS                                                                                                   '+
      ',trunc(SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero),4) PM_EMPRESA                                                                                        '+
      ' ,sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                               '+
      ' ,sum((c.vlbrutofinalbordero) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado                             '+
      ' ,SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                                          '+
      ' , cast((trunc((((sum(C.ADVALOREM + C.DESAGIOACORDADO)/ sum(c.vlbrutofinalbordero))/(sum(multiplicadorpmfloat)/sum(c.vlbrutofinalbordero))) *30) *100,2)) as varchar(5))||'+QuotedStr('%')+' taxa_nominal'+
      ' , cast((trunc( (power((sum(vrliquidoacordadoantesretencoes)/(sum(vlbrutofinalbordero)-sum(vrliquidoacordadoantesretencoes))+1),                                                      '+
      ' ((30/(sum(c.vlbrutofinalbordero * c.prazomedio)/sum(c.vlbrutofinalbordero)))))-1)*100,2)) as varchar(5))||' +QuotedStr('%')+'custoefetivo                                            '+
      '  FROM gocontrolerecebivel c                                                                                                                                                          '+
      'WHERE  1=1';

     group_by:='GROUP BY 1,2,3,4';
     FDAnalisePortador.SQL.Add(consulta);
     FDAnalisePortador.SQL.Add(' and c.DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2');
     FDAnalisePortador.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
     FDAnalisePortador.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);

     if ffiltro_avancado.dbeID_FUNDO.Text<>EmptyStr then
     begin
       FDAnalisePortador.SQL.Add(' and c.CODPORTADOR=:CODPORTADOR');
       FDAnalisePortador.ParamByName('CODPORTADOR').asInteger := strtoInt(ffiltro_avancado.dbeID_FUNDO.Text);
     end;

     if ffiltro_avancado.dbecodprojeto.Text<>EmptyStr then
     begin
       FDAnalisePortador.SQL.Add(' and c.codprojeto=:codprojeto');
       FDAnalisePortador.ParamByName('codprojeto').asInteger := strtoInt(ffiltro_avancado.dbecodprojeto.Text);
     end;

     FDAnalisePortador.SQL.Add(group_by);
     FDAnalisePortador.Open();

     {FDRecebivel totais}
     consulta :=
      ' select count(*)total_bordero                                                                                                                                                             '+
      ' ,sum(c.qttitulos) qttitulos                                                                                                                                                         '+
      ' ,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS,0)) VLMEDIORECEBIVEIS                                                                                                   '+
      ',trunc(SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero),4) PM_EMPRESA                                                                                        '+
      ' ,sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                               '+
      ' ,sum((c.vlbrutofinalbordero) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado                             '+
      ' ,SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                                          '+
      ' , cast((trunc((((sum(C.ADVALOREM + C.DESAGIOACORDADO)/ sum(c.vlbrutofinalbordero))/(sum(multiplicadorpmfloat)/sum(c.vlbrutofinalbordero))) *30) *100,2)) as varchar(5))||'+QuotedStr('%')+' taxa_nominal'+
      ' , cast((trunc( (power((sum(vrliquidoacordadoantesretencoes)/(sum(vlbrutofinalbordero)-sum(vrliquidoacordadoantesretencoes))+1),                                                      '+
      ' ((30/(sum(c.vlbrutofinalbordero * c.prazomedio)/sum(c.vlbrutofinalbordero)))))-1)*100,2)) as varchar(5))||' +QuotedStr('%')+'custoefetivo                                            '+
      '  FROM gocontrolerecebivel c                                                                                                                                                          '+
      ' WHERE c.DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2';

     FDRecebivel.Close;
     FDRecebivel.SQL.Clear;
     FDRecebivel.SQL.Add(consulta);
      if ffiltro_avancado.dbeID_FUNDO.Text<>EmptyStr then
     begin
       FDRecebivel.SQL.Add(' and c.CODPORTADOR=:CODPORTADOR');
       FDRecebivel.ParamByName('CODPORTADOR').asInteger := strtoInt(ffiltro_avancado.dbeID_FUNDO.Text);
     end;

     if ffiltro_avancado.dbecodprojeto.Text<>EmptyStr then
     begin
       FDRecebivel.SQL.Add(' and c.codprojeto=:codprojeto');
       FDRecebivel.ParamByName('codprojeto').asInteger := strtoInt(ffiltro_avancado.dbecodprojeto.Text);
     end;

     FDRecebivel.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
     FDRecebivel.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);
     FDRecebivel.Open();
    {FDRecebivel totais}

     if  not FDAnalisePortador.IsEmpty then
   	 fncfiltros.visualizarRelatorios(frxanalisegeral);

end;

procedure Tffiltro_avancado.analiseporfilial;
  var consulta, group_by, where : string;
begin
    cdsanaliseprojeto.Close;
    FDanaliseFilial.Close;
    FDanaliseFilial.SQL.Clear;

    consulta :=
    'SELECT c.codfilial                                                                                                                                                          '+
    '	,c.filial                                                                                                                                                                     '+
    '	,count(*) total_bordero                                                                                                                                                               '+
    '	,sum(c.qttitulos) qttitulos                                                                                                                                                           '+
    '	,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS, 0)) VLMEDIORECEBIVEIS                                                                                                    '+
    '	,SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero) PM_EMPRESA                                                                                                  '+
    '	,sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                                 '+
    '	,sum((c.vlbrutofinalbordero) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado                               '+
    '	,SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                                            '+
    '	,((sum(C.ADVALOREM + C.DESAGIOACORDADO) / sum(c.vlbrutofinalbordero)) / (sum(c.vlbrutofinalbordero * c.prazomedioacordado) / sum(c.vlbrutofinalbordero))) * 30 custo_pm_empresa       '+
    '	,((sum(c.desagioacordado) / sum(c.prazomedioacordado)) * 30) / sum(c.vlbrutofinalbordero) AS custo_nominal                                                                            '+
    'FROM gocontrolerecebivel c                                                                                                                                                             '+
    'WHERE (coalesce(c.qttitulos, 0) - coalesce(c.qtdrecusada, 0)) > 0                                                                                                                      ';

    FDanaliseFilial.SQL.Add(consulta);
    FDanaliseFilial.SQL.Add(' and DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2');
    FDanaliseFilial.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
    FDanaliseFilial.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);

    if ffiltro_avancado.dbeID_FILIAL.Text<>EmptyStr then
    begin
    FDanaliseFilial.SQL.Add(' and codfilial=:codfilial');
    FDanaliseFilial.ParamByName('codfilial').asInteger := strtoInt(ffiltro_avancado.dbeID_FILIAL.Text);
    end;

    group_by:='GROUP BY 1,2';
    FDanaliseFilial.SQL.Add(group_by);
    FDanaliseFilial.Open();
    cdsanalisefilial.Open;

    if  not cdsanalisefilial.IsEmpty then
    fncfiltros.visualizarRelatorios(frxanaliseFilial);
end;

procedure Tffiltro_avancado.analiseporprojeto;
  var consulta, group_by, where : string;
begin
    cdsanaliseprojeto.Close;
    FDAnaliseProjeto.Close;
    FDAnaliseProjeto.SQL.Clear;

      consulta :=
      'SELECT                                                                                                                                                                               '+
      ' c.codprojeto                                                                                                                                                                       '+
      ' ,c.projeto                                                                                                                                                                          '+
      ' , count(*)total_bordero                                                                                                                                                             '+
      ' ,sum(c.qttitulos) qttitulos                                                                                                                                                         '+
      ' ,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS,0)) VLMEDIORECEBIVEIS                                                                                                   '+
      ',trunc(SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero),4) PM_EMPRESA                                                                                        '+
      ' ,sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                               '+
      ' ,sum((c.vlbrutofinalbordero) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado                             '+
      ' ,SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                                          '+
      ' , cast((trunc((((sum(C.ADVALOREM + C.DESAGIOACORDADO)/ sum(c.vlbrutofinalbordero))/(sum(multiplicadorpmfloat)/sum(c.vlbrutofinalbordero))) *30) *100,2)) as varchar(5))||'+QuotedStr('%')+' taxa_nominal'+
      ' , cast((trunc( (power((sum(vrliquidoacordadoantesretencoes)/(sum(vlbrutofinalbordero)-sum(vrliquidoacordadoantesretencoes))+1),                                                      '+
      ' ((30/(sum(c.vlbrutofinalbordero * c.prazomedio)/sum(c.vlbrutofinalbordero)))))-1)*100,2)) as varchar(5))||' +QuotedStr('%')+'custoefetivo                                            '+
      '  FROM gocontrolerecebivel c                                                                                                                                                          '+
      'WHERE  1=1';



    FDAnaliseProjeto.SQL.Add(consulta);
    FDAnaliseProjeto.SQL.Add(' and DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2');
    FDAnaliseProjeto.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
    FDAnaliseProjeto.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);

    if ffiltro_avancado.dbecodprojeto.Text<>EmptyStr then
    begin
    FDAnaliseProjeto.SQL.Add(' and codprojeto=:codprojeto');
    FDAnaliseProjeto.ParamByName('codprojeto').asInteger := strtoInt(ffiltro_avancado.dbecodprojeto.Text);
    end;

    group_by:='GROUP BY 1,2';
    FDAnaliseProjeto.SQL.Add(group_by);
    FDAnaliseProjeto.Open();
    cdsanaliseprojeto.Open;

      {FDRecebivel totais}
     consulta :=
      ' select count(*)total_bordero                                                                                                                                                             '+
      ' ,sum(c.qttitulos) qttitulos                                                                                                                                                         '+
      ' ,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS,0)) VLMEDIORECEBIVEIS                                                                                                   '+
      ',trunc(SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero),4) PM_EMPRESA                                                                                        '+
      ' ,sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                               '+
      ' ,sum((c.vlbrutofinalbordero) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado                             '+
      ' ,SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                                          '+
      ' , cast((trunc((((sum(C.ADVALOREM + C.DESAGIOACORDADO)/ sum(c.vlbrutofinalbordero))/(sum(multiplicadorpmfloat)/sum(c.vlbrutofinalbordero))) *30) *100,2)) as varchar(5))||'+QuotedStr('%')+' taxa_nominal'+
      ' , cast((trunc( (power((sum(vrliquidoacordadoantesretencoes)/(sum(vlbrutofinalbordero)-sum(vrliquidoacordadoantesretencoes))+1),                                                      '+
      ' ((30/(sum(c.vlbrutofinalbordero * c.prazomedio)/sum(c.vlbrutofinalbordero)))))-1)*100,2)) as varchar(5))||' +QuotedStr('%')+'custoefetivo                                            '+
      '  FROM gocontrolerecebivel c                                                                                                                                                          '+
      ' WHERE c.DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2';

     FDRecebivel.Close;
     FDRecebivel.SQL.Clear;
     FDRecebivel.SQL.Add(consulta);
      if ffiltro_avancado.dbeID_FUNDO.Text<>EmptyStr then
     begin
       FDRecebivel.SQL.Add(' and c.CODPORTADOR=:CODPORTADOR');
       FDRecebivel.ParamByName('CODPORTADOR').asInteger := strtoInt(ffiltro_avancado.dbeID_FUNDO.Text);
     end;

     if ffiltro_avancado.dbecodprojeto.Text<>EmptyStr then
     begin
       FDRecebivel.SQL.Add(' and c.codprojeto=:codprojeto');
       FDRecebivel.ParamByName('codprojeto').asInteger := strtoInt(ffiltro_avancado.dbecodprojeto.Text);
     end;

     FDRecebivel.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
     FDRecebivel.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);
     FDRecebivel.Open();
    {FDRecebivel totais}



    if  not cdsanaliseprojeto.IsEmpty then
    fncfiltros.visualizarRelatorios(frxanaliseProjeto);

end;

procedure Tffiltro_avancado.analiseportadoracumulado;
  var consulta, group_by, where : string;
begin
     cdsportadoracumulado.Close;
     FDportadoracumulado.Close;
     FDportadoracumulado.SQL.Clear;

     consulta :=
      'SELECT c.codportador                                                                                                                                                    '+
      ',c.portador                                                                                                                                                             '+
      ',count(*) total_bordero                                                                                                                                                 '+
      ',sum(c.qttitulos) qttitulos                                                                                                                                             '+
      ',sum((c.vlbrutobordero) - (c.vlbrutorecusado)) / sum(coalesce(c.qttitulos, 0) - coalesce(c.qtdrecusada, 0)) VLMEDIORECEBIVEIS                                           '+
      ',SUM((c.VLBRUTOBORDERO - c.VLBRUTORECUSADO) * c.prazomedio) / SUM(c.VLBRUTOBORDERO - c.VLBRUTORECUSADO) PM_EMPRESA                                                      '+
      ',sum(c.vlbrutobordero) vlbrutobordero                                                                                                                                   '+
      ',sum((c.vlbrutobordero - c.vlbrutorecusado) - (c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas)) vlliquidocalculado  '+
      ',SUM(c.tac + c.tarifaentradatitulos + c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas) DESPESAS_ENCARGOS                                              '+
      ',sum(c.VLDESTINADORECOMPORATITULO)VLDESTINADORECOMPORATITULO                                                                                                            '+
      ',sum(c.TARIFAENTRADATITULOS) TARIFAENTRADATITULOS                                                                                                                       '+
      ',sum(c.vloutroscreditos)vloutroscreditos                                                                                                                                '+
      'FROM gocontrolerecebivel c                                                                                                                                              '+
      'WHERE 1=1                                                                                                      ';

      group_by:='GROUP BY 1,2';

       FDportadoracumulado.SQL.Add(consulta);
       FDportadoracumulado.SQL.Add(' and DTCOMPETENCIA BETWEEN :DATA1 AND :DATA2');
       FDportadoracumulado.ParamByName('DATA1').AsDateTime := StrToDateTime(ffiltro_avancado.data1.Text);
       FDportadoracumulado.ParamByName('DATA2').AsDateTime := StrToDateTime(ffiltro_avancado.data2.Text);

       if ffiltro_avancado.dbeID_FUNDO.Text<>EmptyStr then
       begin
         FDportadoracumulado.SQL.Add(' and CODPORTADOR=:CODPORTADOR');
         FDportadoracumulado.ParamByName('CODPORTADOR').asInteger := strtoInt(ffiltro_avancado.dbeID_FUNDO.Text);
       end;
       FDportadoracumulado.SQL.Add(group_by);

       FDportadoracumulado.Open();
       cdsportadoracumulado.Open;

       if  not cdsportadoracumulado.IsEmpty then
       fncfiltros.visualizarRelatorios(frxanaliseportadoracumulado);

end;

procedure Tffiltro_avancado.AnoAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncfiltros.anoanterior (Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.AnoAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.anoantual (Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.dbecodprojetoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeprojeto.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.dbeID_FUNDOKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeID_FUNDO.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.dbeID_FILIALKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;
  if key = #8 then
  begin
   dbeFILIAL.Text  :=EmptyStr;
  end;

end;

procedure Tffiltro_avancado.dbeID_CONSULTORKeyPress(Sender: TObject;
  var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeID_CONSULTOR.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.SemanaAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncfiltros.SemanaAnterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemanaAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.SemanaAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemestreAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.semestreanterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.semestreatual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  fncfiltros.configurarGridesFormulario(ffiltro_avancado, true, false);
	{>>>eliminando container de fun��es da memoria}
	fncfiltros.Free;
	{eliminando container de fun��es da memoria<<<}

  ffiltro_avancado :=nil;
 	Action           :=cafree;
end;

procedure Tffiltro_avancado.FormCreate(Sender: TObject);
begin
  fncfiltros:= TFuncoes.Create(Self);
	fncfiltros.definirConexao(dm.conexao);
  fncfiltros.definirConexao(dm.conexao);
	fncfiltros.definirFormulario(Self);
	fncfiltros.validarTransacaoRelacionamento:=true;
	fncfiltros.autoAplicarAtualizacoesBancoDados:=true;

   if fgocontrolerecebivel.lblProjetoresultado.Caption <>'99' then
   begin
    dbecodprojeto.Text:= fgocontrolerecebivel.lblProjetoresultado.Caption;
    dbecodprojeto.OnExit(dbecodprojeto);
    dbecodprojeto.Enabled:= false;
   end
  else
    dbecodprojeto.Enabled:= true;

end;

procedure Tffiltro_avancado.HojeClick(Sender: TObject);
begin
 try
  fncfiltros.DiaAtual(Date);
  data1.Date  := fncfiltros.DiaAtual(Date);
  data2.Date  := fncfiltros.DiaAtual(Date);
 finally
 end;
end;

procedure Tffiltro_avancado.LimparDatasClick(Sender: TObject);
begin
  data1.Clear;
  data2.Clear;
end;

procedure Tffiltro_avancado.MesAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.MesAnterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.MesAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.MesAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.OntemClick(Sender: TObject);
begin
 try
    Data1.Date  :=fncfiltros.DiaAnterior(Date);
    Data2.Date  :=fncfiltros.DiaAnterior(Date);
   finally
   end;
end;

procedure Tffiltro_avancado.psqprojetoPreencherCampos(Sender: TObject);
begin
 if dbecodprojeto.Text<>EmptyStr then
  begin
   psqfilial.SQL.Clear;
   psqfilial.SQL.Add('SELECT * FROM V_PROJETO_FILIAL WHERE 1=1 AND CODPROJETO='+dbecodprojeto.Text);
  end
  else
  begin
   psqfilial.SQL.Clear;
   psqfilial.SQL.Add('SELECT * FROM V_PROJETO_FILIAL WHERE 1=1');
   end;
end;

procedure Tffiltro_avancado.spbdatesinteticoClick(Sender: TObject);
begin
 with TcxButton(Sender).ClientToScreen(point(TcxButton(Sender).Width,
    TcxButton(Sender).Height)) do
  begin
    popmenudata.Popup(X, Y);
  end;
end;

procedure Tffiltro_avancado.spdfecharClick(Sender: TObject);
begin
 ModalResult:= mrcancel;
end;

procedure Tffiltro_avancado.spdimprimirClick(Sender: TObject);
begin
  if ((data1.Text= EmptyStr) or (data2.Text=EmptyStr)) then
  abort;

//  if dbecodprojeto.Text=EmptyStr then
//  begin
//    application.MessageBox('Voc� deve preencher o Projeto.','Campo obrigat�rio.',MB_ICONEXCLAMATION);
//    dbecodprojeto.SetFocus;
//    abort;
//  end;

 { if dbeID_FILIAL.Text=EmptyStr then
  begin
    application.MessageBox('Voc� deve preencher a Filial.','Campo obrigat�rio.',MB_ICONEXCLAMATION);
    dbeID_FILIAL.SetFocus;
    abort;
  end; }

  if ((daysbetween(data2.Date ,data1.Date) >10000) and (rgrelatorio.ItemIndex=0))  then
  begin
    application.MessageBox('Intervalo pesquisado deve ser menor que 10000 dias.','Intervalo permitido.',MB_ICONEXCLAMATION);
    dbeID_FILIAL.SetFocus;
    abort;
  end;

  TDialogMessage.ShowWaitMessage('Carregando dados...',
  procedure
  begin
      case rgrelatorio.ItemIndex of
       0 : analisegeral;
       //1 : analiseportadoracumulado;
       1 : analiseporprojeto;
     //  3 : analiseporfilial;
      end;

  end);



end;

procedure Tffiltro_avancado.TrimestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.trimestreatual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.UltimosQuizeDiasClick(Sender: TObject);
var data : TStringList;
begin

   try
    data := TStringList.Create;
    data:=fncfiltros.UltimosQuinzeDias(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;

end;

end.
