CREATE TABLE GOPROJETOFUNDO(
ID Integer   
,
CONSTRAINT pk_GOPROJETOFUNDO PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODPROJETO Integer   
,
PROJETO Varchar(250)   COLLATE PT_BR
,
CODPORTADOR Integer   
,
PORTADOR Varchar(250)   COLLATE PT_BR
,
LIMITE_DESCONTO Double Precision DEFAULT 0  
,
TAXA_DESCONTO Double Precision DEFAULT 0  
,
LIMITE_FOMENTO Double Precision DEFAULT 0  
,
TAXA_FOMENTO Double Precision DEFAULT 0  
);
commit;
CREATE ASC INDEX I01_GOPR_FUNDO_ID ON GOPROJETOFUNDO (ID);
CREATE ASC INDEX I02_GOPR_FUNDO_DT_CADASTRO ON GOPROJETOFUNDO (DT_CADASTRO);
CREATE ASC INDEX I03_GOPR_FUNDO_HS_CADASTRO ON GOPROJETOFUNDO (HS_CADASTRO);
CREATE ASC INDEX I04_GOPR_FUNDO_CODPROJETO ON GOPROJETOFUNDO (CODPROJETO);
CREATE ASC INDEX I05_GOPR_FUNDO_PROJETO ON GOPROJETOFUNDO (PROJETO);
CREATE ASC INDEX I06_GOPR_FUNDO_CODPORTADOR ON GOPROJETOFUNDO (CODPORTADOR);
CREATE ASC INDEX I07_GOPR_FUNDO_PORTADOR ON GOPROJETOFUNDO (PORTADOR);
CREATE ASC INDEX I08_GOPR_FUNDO_LIMITE_DESCONTO ON GOPROJETOFUNDO (LIMITE_DESCONTO);
CREATE ASC INDEX I09_GOPR_FUNDO_TAXA_DESCONTO ON GOPROJETOFUNDO (TAXA_DESCONTO);
CREATE ASC INDEX I10_GOPR_FUNDO_LIMITE_FOMENTO ON GOPROJETOFUNDO (LIMITE_FOMENTO);
CREATE ASC INDEX I11_GOPR_FUNDO_TAXA_FOMENTO ON GOPROJETOFUNDO (TAXA_FOMENTO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Projeto1' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'CODPROJETO';
update rdb$relation_fields set rdb$description = 'Projeto' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'PROJETO';
update rdb$relation_fields set rdb$description = 'C�d.Portador' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'CODPORTADOR';
update rdb$relation_fields set rdb$description = 'Portador' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'PORTADOR';
update rdb$relation_fields set rdb$description = 'Limite Desconto' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'LIMITE_DESCONTO';
update rdb$relation_fields set rdb$description = 'Taxa Desconto %' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'TAXA_DESCONTO';
update rdb$relation_fields set rdb$description = 'Limite Fomento' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'LIMITE_FOMENTO';
update rdb$relation_fields set rdb$description = 'Taxa Fomento%' where rdb$relation_name = 'GOPROJETOFUNDO' and rdb$field_name = 'TAXA_FOMENTO';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
