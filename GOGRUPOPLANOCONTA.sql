CREATE TABLE GOGRUPOPLANOCONTA(
ID Integer   
,
CONSTRAINT pk_GOGRUPOPLANOCONTA PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODFILIAL Integer   
,
FILIAL Varchar(250)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOGR_CONTA_ID ON GOGRUPOPLANOCONTA (ID);
CREATE ASC INDEX I02_GOGR_CONTA_DT_CADASTRO ON GOGRUPOPLANOCONTA (DT_CADASTRO);
CREATE ASC INDEX I03_GOGR_CONTA_HS_CADASTRO ON GOGRUPOPLANOCONTA (HS_CADASTRO);
CREATE ASC INDEX I04_GOGR_CONTA_CODFILIAL ON GOGRUPOPLANOCONTA (CODFILIAL);
CREATE ASC INDEX I05_GOGR_CONTA_FILIAL ON GOGRUPOPLANOCONTA (FILIAL);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOGRUPOPLANOCONTA' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOGRUPOPLANOCONTA' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOGRUPOPLANOCONTA' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Filial' where rdb$relation_name = 'GOGRUPOPLANOCONTA' and rdb$field_name = 'CODFILIAL';
update rdb$relation_fields set rdb$description = 'Filial' where rdb$relation_name = 'GOGRUPOPLANOCONTA' and rdb$field_name = 'FILIAL';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
