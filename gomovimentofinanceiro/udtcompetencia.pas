unit udtcompetencia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils,
  Vcl.StdCtrls, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, Vcl.ExtCtrls;

type
  Tfdtcompetencia = class(TForm)
    pnlbase: TPanel;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    dtcompetencia: TcxDateEdit;
    pnlLinhaBotoessecao: TPanel;
    spdok: TButton;
    spdfechar: TButton;
    procedure spdokClick(Sender: TObject);
    procedure spdfecharClick(Sender: TObject);
    procedure pnlLinhaTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fdtcompetencia: Tfdtcompetencia;

implementation

{$R *.dfm}

procedure Tfdtcompetencia.pnlLinhaTituloMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure Tfdtcompetencia.spdfecharClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

procedure Tfdtcompetencia.spdokClick(Sender: TObject);
begin
 ModalResult := mrOk;
end;

end.
