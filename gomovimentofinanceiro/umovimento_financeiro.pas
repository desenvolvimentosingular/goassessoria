unit umovimento_financeiro;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 26/11/2021*}                                          
{*Hora 01:05:21*}                                            
{*Unit movimento_financeiro *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, dxGDIPlusClasses, Data.SqlExpr, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, uPesquisa, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, cxCurrencyEdit, cxCheckBox;

type                                                                                        
	Tfmovimento_financeiro = class(TForm)
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxmovimento_financeiro: TfrxReport;
	frxDBmovimento_financeiro: TfrxDBDataset;
	Popupmovimento_financeiro: TPopupMenu;
	Popupoutrasentradas: TPopupMenu;
	Popupcaptacaofomento: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabmovimento_financeiro: tcxTabSheet;
	SCRmovimento_financeiro: TScrollBox;
	pnlDadosmovimento_financeiro: TPanel;
	{>>>bot�es de manipula��o de dados movimento_financeiro}
	pnlLinhaBotoesmovimento_financeiro: TPanel;
	spdAdicionarmovimento_financeiro: TButton;
	spdAlterarmovimento_financeiro: TButton;
	spdGravarmovimento_financeiro: TButton;
	spdCancelarmovimento_financeiro: TButton;
	spdExcluirmovimento_financeiro: TButton;
	pnlLayoutLinhasmovimento_financeiro1: TPanel;
	pnlLayoutTitulosmovimento_financeiroid: TPanel;
	pnlLayoutCamposmovimento_financeiroid: TPanel;
	dbemovimento_financeiroid: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro2: TPanel;
	pnlLayoutTitulosmovimento_financeirodt_cadastro: TPanel;
	pnlLayoutCamposmovimento_financeirodt_cadastro: TPanel;
	dbemovimento_financeirodt_cadastro: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro3: TPanel;
	pnlLayoutTitulosmovimento_financeirohs_cadastro: TPanel;
	pnlLayoutCamposmovimento_financeirohs_cadastro: TPanel;
	dbemovimento_financeirohs_cadastro: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro4: TPanel;
	pnlLayoutTitulosmovimento_financeirodt_competencia: TPanel;
	pnlLayoutCamposmovimento_financeirodt_competencia: TPanel;
	pnlLayoutLinhasmovimento_financeiro5: TPanel;
	pnlLayoutTitulosmovimento_financeirocodprojeto: TPanel;
	pnlLayoutCamposmovimento_financeirocodprojeto: TPanel;
	dbemovimento_financeirocodprojeto: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro7: TPanel;
	pnlLayoutTitulosmovimento_financeirocodfilial: TPanel;
	pnlLayoutCamposmovimento_financeirocodfilial: TPanel;
	dbemovimento_financeirocodfilial: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro9: TPanel;
	pnlLayoutTitulosmovimento_financeirovlabertura: TPanel;
	pnlLayoutCamposmovimento_financeirovlabertura: TPanel;
	dbemovimento_financeirovlabertura: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro12: TPanel;
	pnlLayoutLinhasmovimento_financeiro14: TPanel;
	pnlLayoutTitulosmovimento_financeirovlcaptacaofomentoprev: TPanel;
	pnlLayoutCamposmovimento_financeirovlcaptacaofomentoprev: TPanel;
	dbemovimento_financeirovlcaptacaofomentoprev: TDBEdit;
	pnlLayoutLinhasmovimento_financeiro16: TPanel;
	pnlLayoutTitulosmovimento_financeirovloutrasaidasprev: TPanel;
	pnlLayoutCamposmovimento_financeirovloutrasaidasprev: TPanel;
	dbemovimento_financeirovloutrasaidasprev: TDBEdit;

	taboutrasentradas: tcxTabSheet;
	SCRoutrasentradas: TScrollBox;
	pnlDadosoutrasentradas: TPanel;
	{>>>bot�es de manipula��o de dados outrasentradas}
	pnlLinhaBotoesoutrasentradas: TPanel;
	spdAdicionaroutrasentradas: TButton;
	spdAlteraroutrasentradas: TButton;
	spdGravaroutrasentradas: TButton;
	spdCancelaroutrasentradas: TButton;
	spdExcluiroutrasentradas: TButton;
	pnlLayoutLinhasoutrasentradas1: TPanel;
	pnlLayoutTitulosoutrasentradasid: TPanel;
	pnlLayoutCamposoutrasentradasid: TPanel;
	dbeoutrasentradasid: TDBEdit;
	pnlLayoutLinhasoutrasentradas2: TPanel;
	pnlLayoutTitulosoutrasentradasdt_cadastro: TPanel;
	pnlLayoutCamposoutrasentradasdt_cadastro: TPanel;
	dbeoutrasentradasdt_cadastro: TDBEdit;
	pnlLayoutLinhasoutrasentradas3: TPanel;
	pnlLayoutTitulosoutrasentradashs_cadastro: TPanel;
	pnlLayoutCamposoutrasentradashs_cadastro: TPanel;
	dbeoutrasentradashs_cadastro: TDBEdit;
	pnlLayoutLinhasoutrasentradas4: TPanel;
	pnlLayoutTitulosoutrasentradascodmovfinanceiro: TPanel;
	pnlLayoutCamposoutrasentradascodmovfinanceiro: TPanel;
	dbeoutrasentradascodmovfinanceiro: TDBEdit;
	pnlLayoutLinhasoutrasentradas5: TPanel;
	pnlLayoutTitulosoutrasentradasdtcompetencia: TPanel;
	pnlLayoutCamposoutrasentradasdtcompetencia: TPanel;
	pnlLayoutLinhasoutrasentradas6: TPanel;
	pnlLayoutTitulosoutrasentradasdescricao: TPanel;
	pnlLayoutCamposoutrasentradasdescricao: TPanel;
	dbeoutrasentradasdescricao: TDBEdit;
	pnlLayoutLinhasoutrasentradas7: TPanel;
	pnlLayoutTitulosoutrasentradastipo: TPanel;
	pnlLayoutCamposoutrasentradastipo: TPanel;
	cbooutrasentradastipo: TDBComboBox;
	pnlLayoutLinhasoutrasentradas8: TPanel;
	pnlLayoutTitulosoutrasentradasvlentrada: TPanel;
	pnlLayoutCamposoutrasentradasvlentrada: TPanel;
	dbeoutrasentradasvlentrada: TDBEdit;

	tabcaptacaofomento: tcxTabSheet;
	SCRcaptacaofomento: TScrollBox;
	pnlDadoscaptacaofomento: TPanel;
	{>>>bot�es de manipula��o de dados captacaofomento}
	pnlLinhaBotoescaptacaofomento: TPanel;
	spdAdicionarcaptacaofomento: TButton;
	spdAlterarcaptacaofomento: TButton;
	spdGravarcaptacaofomento: TButton;
	spdCancelarcaptacaofomento: TButton;
	spdExcluircaptacaofomento: TButton;
	pnlLayoutLinhascaptacaofomento1: TPanel;
	pnlLayoutTituloscaptacaofomentoid: TPanel;
	pnlLayoutCamposcaptacaofomentoid: TPanel;
	dbecaptacaofomentoid: TDBEdit;
	pnlLayoutLinhascaptacaofomento2: TPanel;
	pnlLayoutTituloscaptacaofomentodt_cadastro: TPanel;
	pnlLayoutCamposcaptacaofomentodt_cadastro: TPanel;
	dbecaptacaofomentodt_cadastro: TDBEdit;
	pnlLayoutLinhascaptacaofomento3: TPanel;
	pnlLayoutTituloscaptacaofomentohs_cadastro: TPanel;
	pnlLayoutCamposcaptacaofomentohs_cadastro: TPanel;
	dbecaptacaofomentohs_cadastro: TDBEdit;
	pnlLayoutLinhascaptacaofomento4: TPanel;
	pnlLayoutTituloscaptacaofomentocodmovfinanceiro: TPanel;
	pnlLayoutCamposcaptacaofomentocodmovfinanceiro: TPanel;
	dbecaptacaofomentocodmovfinanceiro: TDBEdit;
	pnlLayoutLinhascaptacaofomento5: TPanel;
	pnlLayoutTituloscaptacaofomentodtcompetencia: TPanel;
	pnlLayoutCamposcaptacaofomentodtcompetencia: TPanel;
	pnlLayoutLinhascaptacaofomento6: TPanel;
	pnlLayoutTituloscaptacaofomentodescricao: TPanel;
	pnlLayoutCamposcaptacaofomentodescricao: TPanel;
	dbecaptacaofomentodescricao: TDBEdit;
	pnlLayoutLinhascaptacaofomento7: TPanel;
	pnlLayoutTituloscaptacaofomentocod_fundo: TPanel;
	pnlLayoutCamposcaptacaofomentocod_fundo: TPanel;
	dbecaptacaofomentocod_fundo: TDBEdit;
	pnlLayoutLinhascaptacaofomento9: TPanel;
	pnlLayoutTituloscaptacaofomentovlprevisto: TPanel;
	pnlLayoutCamposcaptacaofomentovlprevisto: TPanel;
	dbecaptacaofomentovlprevisto: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    Gridoutrasentradas: TcxGrid;
    GridoutrasentradasDBTV: TcxGridDBTableView;
    GRIDoutrasentradasDBTVID: TcxGridDBColumn;
    GRIDoutrasentradasDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDoutrasentradasDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDoutrasentradasDBTVCODMOVFINANCEIRO: TcxGridDBColumn;
    GRIDoutrasentradasDBTVDTCOMPETENCIA: TcxGridDBColumn;
    GRIDoutrasentradasDBTVDESCRICAO: TcxGridDBColumn;
    GRIDoutrasentradasDBTVTIPO: TcxGridDBColumn;
    GRIDoutrasentradasDBTVVLENTRADA: TcxGridDBColumn;
    GridoutrasentradasLevel1: TcxGridLevel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    imglogo: TImage;
    pnlLayoutCamposmovimento_financeiroprojeto: TPanel;
    dbemovimento_financeiroPROJETO: TDBEdit;
    pnlLayoutCamposmovimento_financeirofilial: TPanel;
    dbemovimento_financeiroFILIAL: TDBEdit;
    pnlLayoutTitulosmovimento_financeirovloutrasentradas_prev: TPanel;
    pnlLayoutCamposmovimento_financeirovloutrasentradas_prev: TPanel;
    dbemovimento_financeiroVLOUTRASENTRADAS_PREV: TDBEdit;
    pnlLayoutTitulosmovimento_financeirovloutrasentradasrealizada: TPanel;
    pnlLayoutCamposmovimento_financeirovloutrasentradasrealizada: TPanel;
    dbemovimento_financeiroVLOUTRASENTRADASREALIZADA: TDBEdit;
    pnlLayoutTitulosmovimento_financeirovlcaptacaofomentorealizado: TPanel;
    pnlLayoutCamposmovimento_financeirovlcaptacaofomentorealizado: TPanel;
    dbemovimento_financeiroVLCAPTACAOFOMENTOREALIZADO: TDBEdit;
    pnlLayoutTitulosmovimento_financeirovloutrassaidasrealizado: TPanel;
    pnlLayoutCamposmovimento_financeirovloutrassaidasrealizado: TPanel;
    dbemovimento_financeiroVLOUTRASSAIDASREALIZADO: TDBEdit;
    pnlLayoutCamposcaptacaofomentofundo: TPanel;
    dbecaptacaofomentoFUNDO: TDBEdit;
    btnprojeto: TButton;
    btnfilial: TButton;
    psqprojeto: TPesquisa;
    psqfilial: TPesquisa;
    psqportador: TPesquisa;
    btnpesquisarfundo: TButton;
    cdsfiltros: TClientDataSet;
    dbemovimento_financeiroDT_COMPETENCIA: TcxDBDateEdit;
    dbeoutrasentradasDTCOMPETENCIA: TcxDBDateEdit;
    dbecaptacaofomentoDTCOMPETENCIA: TcxDBDateEdit;
    pnlLayoutTituloscaptacaofomentovlrealizado: TPanel;
    pnlLayoutCamposcaptacaofomentovlrealizado: TPanel;
    dbecaptacaofomentoVLREALIZADO: TDBEdit;
    Gridcaptacaofomento: TcxGrid;
    GridcaptacaofomentoDBTV: TcxGridDBTableView;
    GRIDcaptacaofomentoDBTVID: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVCODMOVFINANCEIRO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVDTCOMPETENCIA: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVDESCRICAO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVCOD_FUNDO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVFUNDO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVVLPREVISTO: TcxGridDBColumn;
    GRIDcaptacaofomentoDBTVVLREALIZADO: TcxGridDBColumn;
    GridcaptacaofomentoLevel1: TcxGridLevel;
    pnlLayoutTitulosoutrasentradasvlentradaREALIZADO: TPanel;
    pnlLayoutCamposoutrasentradasvlentradaREALIZADO: TPanel;
    dbeoutrasentradasVLENTRADAREALIZADO: TDBEdit;
    GridoutrasentradasDBTVVLENTRADAREALIZADO: TcxGridDBColumn;
    tabcpg: TcxTabSheet;
    pnlLinhaBotoescpg: TPanel;
    spdAdicionarcpg: TButton;
    spdAlterarcpg: TButton;
    spdGravarcpg: TButton;
    spdCancelarcpg: TButton;
    spdExcluircpg: TButton;
    spdImportar: TButton;
    SCRcpg: TScrollBox;
    pnlDadoscpg: TPanel;
    pnlLayoutLinhascpg1: TPanel;
    pnlLayoutTituloscpgid: TPanel;
    pnlLayoutCamposcpgid: TPanel;
    dbecpgID: TDBEdit;
    pnlLayoutLinhascpg2: TPanel;
    pnlLayoutLinhascpg4: TPanel;
    pnlLayoutTituloscpgcodprojeto: TPanel;
    pnlLayoutCamposcpgcodprojeto: TPanel;
    dbecpgCODPROJETO: TDBEdit;
    pnlLayoutCamposcpgprojeto: TPanel;
    dbecpgPROJETO: TDBEdit;
    pnlLayoutLinhascpg6: TPanel;
    pnlLayoutTituloscpgid_fornecedor: TPanel;
    pnlLayoutCamposcpgid_fornecedor: TPanel;
    dbecpgID_FORNECEDOR: TDBEdit;
    pnlLayoutCamposcpgfornecedor: TPanel;
    dbecpgFORNECEDOR: TDBEdit;
    pnlLayoutLinhascpg8: TPanel;
    pnlLayoutTituloscpgdescricao: TPanel;
    pnlLayoutCamposcpgdescricao: TPanel;
    dbecpgDESCRICAO: TDBEdit;
    pnlLayoutLinhascpg9: TPanel;
    pnlLayoutTituloscpgdocumento: TPanel;
    pnlLayoutCamposcpgdocumento: TPanel;
    dbecpgDOCUMENTO: TDBEdit;
    pnlLayoutLinhascpg10: TPanel;
    pnlLayoutTituloscpgvl_bruto: TPanel;
    pnlLayoutCamposcpgvl_bruto: TPanel;
    dbecpgVL_BRUTO: TDBEdit;
    pnlLayoutCamposcpgvl_multa: TPanel;
    dbecpgVL_MULTA: TDBEdit;
    pnlLayoutTituloscpgvl_multa: TPanel;
    pnlLayoutTituloscpgvl_juros: TPanel;
    pnlLayoutCamposcpgvl_juros: TPanel;
    dbecpgVL_JUROS: TDBEdit;
    pnlLayoutTituloscpgvl_desconto: TPanel;
    pnlLayoutCamposcpgvl_desconto: TPanel;
    dbecpgVL_DESCONTO: TDBEdit;
    pnlLayoutLinhascpg14: TPanel;
    pnlLayoutTituloscpgdt_vencimento: TPanel;
    pnlLayoutCamposcpgdt_vencimento: TPanel;
    dbecpgDT_VENCIMENTO: TcxDBDateEdit;
    pnlLayoutCamposcpgforma_pagamento: TPanel;
    pnlLayoutTituloscpgforma_pagamento: TPanel;
    pnlLayoutLinhascpg17: TPanel;
    pnlLayoutTituloscpgdt_pagamento: TPanel;
    pnlLayoutCamposcpgdt_pagamento: TPanel;
    dbecpgDT_PAGAMENTO: TcxDBDateEdit;
    pnlLayoutLinhascpg18: TPanel;
    pnlLayoutTituloscpgid_banco_conta: TPanel;
    pnlLayoutCamposcpgid_banco_conta: TPanel;
    dbecpgID_BANCO_CONTA: TDBEdit;
    pnlLayoutCamposcpgbanco_conta: TPanel;
    dbecpgBANCO_CONTA: TDBEdit;
    frxcpg: TfrxReport;
    frxDBcpg: TfrxDBDataset;
    Popupcpg: TPopupMenu;
    gridcpg: TcxGrid;
    GridCPGDBTV: TcxGridDBTableView;
    GridCPGDBTVSEL: TcxGridDBColumn;
    GridCPGDBTVID: TcxGridDBColumn;
    GridCPGDBTVDT_CADASTRO: TcxGridDBColumn;
    GridCPGDBTVHS_CADASTRO: TcxGridDBColumn;
    GridCPGDBTVCODPROJETO: TcxGridDBColumn;
    GridCPGDBTVPROJETO: TcxGridDBColumn;
    GridCPGDBTVID_FORNECEDOR: TcxGridDBColumn;
    GridCPGDBTVFORNECEDOR: TcxGridDBColumn;
    GridCPGDBTVID_BANCO_CONTA: TcxGridDBColumn;
    GridCPGDBTVBANCO_CONTA: TcxGridDBColumn;
    GridCPGDBTVDESCRICAO: TcxGridDBColumn;
    GridCPGDBTVDOCUMENTO: TcxGridDBColumn;
    GridCPGDBTVVL_BRUTO: TcxGridDBColumn;
    GridCPGDBTVVL_MULTA: TcxGridDBColumn;
    GridCPGDBTVVL_JUROS: TcxGridDBColumn;
    GridCPGDBTVDT_VENCIMENTO: TcxGridDBColumn;
    GridCPGDBTVFORMA_PAGAMENTO: TcxGridDBColumn;
    GridCPGDBTVVL_PAGO: TcxGridDBColumn;
    GridCPGDBTVDT_PAGAMENTO: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    psqprojetocpg: TPesquisa;
    psqFornecedor: TPesquisa;
    psqconta: TPesquisa;
    pnlLayoutLinhascpg15: TPanel;
    pnlLayoutTituloscpgplanoconta: TPanel;
    pnlLayoutCamposcpgplanoconta: TPanel;
    pnlLayoutCamposcpgvl_pago: TPanel;
    dbecpgVL_PAGO: TDBEdit;
    pnlLayoutTituloscpgvl_pago: TPanel;
    pnlLayoutTituloscpgdt_cadastro: TPanel;
    pnlLayoutCamposcpgdt_cadastro: TPanel;
    dbecpgDT_CADASTRO: TDBEdit;
    pnlLayoutTituloscpghs_cadastro: TPanel;
    pnlLayoutCamposcpghs_cadastro: TPanel;
    dbecpgHS_CADASTRO: TDBEdit;
    pnlLayoutTituloscpgidfilial: TPanel;
    pnlLayoutCamposcpgidCLIENTE: TPanel;
    dbecpgIDCLIENTE: TDBEdit;
    Panel1: TPanel;
    dbecpgCLIENTE: TDBEdit;
    btnFornecedor: TButton;
    btnBancoConta: TButton;
    btnpesquisaprojetocpg: TButton;
    cmbcpgplanoconta: TcxDBComboBox;
    cbocpgFORMA_PAGAMENTO: TcxDBComboBox;
    pnlLayoutTituloscpgvl_pagar: TPanel;
    pnlLayoutCamposcpgvl_pagar: TPanel;
    dbecpgVL_PAGAR: TDBEdit;
    btnBaixa: TButton;
    tabgodestinooperacao: TcxTabSheet;
    SCRgodestinooperacao: TScrollBox;
    pnlDadosgodestinooperacao: TPanel;
    pnlLayoutLinhasgodestinooperacao1: TPanel;
    pnlLayoutTitulosgodestinooperacaoid: TPanel;
    pnlLayoutCamposgodestinooperacaoid: TPanel;
    dbegodestinooperacaoID: TDBEdit;
    pnlLayoutLinhasgodestinooperacao2: TPanel;
    pnlLayoutTitulosgodestinooperacaocodmovfinanceiro: TPanel;
    pnlLayoutCamposgodestinooperacaocodmovfinanceiro: TPanel;
    dbegodestinooperacaoCODMOVFINANCEIRO: TDBEdit;
    pnlLayoutLinhasgodestinooperacao3: TPanel;
    pnlLayoutTitulosgodestinooperacaodt_cadastro: TPanel;
    pnlLayoutCamposgodestinooperacaodt_cadastro: TPanel;
    dbegodestinooperacaoDT_CADASTRO: TDBEdit;
    pnlLayoutLinhasgodestinooperacao4: TPanel;
    pnlLayoutTitulosgodestinooperacaohs_cadastro: TPanel;
    pnlLayoutCamposgodestinooperacaohs_cadastro: TPanel;
    dbegodestinooperacaoHS_CADASTRO: TDBEdit;
    pnlLayoutLinhasgodestinooperacao5: TPanel;
    pnlLayoutTitulosgodestinooperacaocodfundo: TPanel;
    pnlLayoutCamposgodestinooperacaocodfundo: TPanel;
    dbegodestinooperacaoCODFUNDO: TDBEdit;
    pnlLayoutLinhasgodestinooperacao7: TPanel;
    pnlLayoutTitulosgodestinooperacaovlbruto: TPanel;
    pnlLayoutCamposgodestinooperacaovlbruto: TPanel;
    dbegodestinooperacaoVLBRUTO: TDBEdit;
    Popupgodestinooperacao: TPopupMenu;
    Gridgodestinooperacao: TcxGrid;
    GridgodestinooperacaoDBTV: TcxGridDBTableView;
    GRIDgodestinooperacaoDBTVID: TcxGridDBColumn;
    GRIDgodestinooperacaoDBTVDT_CADASTRO: TcxGridDBColumn;
    GRIDgodestinooperacaoDBTVHS_CADASTRO: TcxGridDBColumn;
    GRIDgodestinooperacaoDBTVCODFUNDO: TcxGridDBColumn;
    GRIDgodestinooperacaoDBTVFUNDO: TcxGridDBColumn;
    GRIDgodestinooperacaoDBTVVLBRUTO: TcxGridDBColumn;
    GridgodestinooperacaoLevel1: TcxGridLevel;
    pnlLayoutCamposgodestinooperacaofundo: TPanel;
    dbegodestinooperacaoFUNDO: TDBEdit;
    pnlLinhaBotoesgodestinooperacao: TPanel;
    spdAdicionargodestinooperacao: TButton;
    spdAlterargodestinooperacao: TButton;
    spdGravargodestinooperacao: TButton;
    spdCancelargodestinooperacao: TButton;
    spdExcluirgodestinooperacao: TButton;
    btnfundodestino: TButton;
    psqfundodestino: TPesquisa;
    pnlLayoutLinhasmovimento_financeiro17: TPanel;
    pnlLayoutTitulosmovimento_financeirovlrecebiveisprev: TPanel;
    pnlLayoutCamposmovimento_financeirovlrecebiveisprev: TPanel;
    dbemovimento_financeiroVLRECEBIVEISPREV: TDBEdit;
    pnlLayoutTitulosmovimento_financeirovlrecebiveisrealizado: TPanel;
    pnlLayoutCamposmovimento_financeirovlrecebiveisrealizado: TPanel;
    dbemovimento_financeiroVLRECEBIVEISREALIZADO: TDBEdit;
    pnlLayoutLinhasmovimento_financeiro18: TPanel;
    pnlLayoutTitulosmovimento_financeiroSALDOFINALPREVISTO: TPanel;
    pnlLayoutCamposmovimento_financeirosaldofinalprevisto: TPanel;
    dbemovimento_financeirosaldofinalprevisto: TDBEdit;
    pnlLayoutTitulosmovimento_financeiroSALDOFINALREALIZADO: TPanel;
    pnlLayoutCamposmovimento_financeirosaldofinalrealizado: TPanel;
    dbemovimento_financeirosaldorealizado: TDBEdit;
    pnlLayoutCamposgodestinooperacaovlmulta: TPanel;
    dbegodestinooperacaoVLMULTA: TDBEdit;
    pnlLayoutTitulosgodestinooperacaovlmulta: TPanel;
    pnlLayoutCamposgodestinooperacaovldesconto: TPanel;
    dbegodestinooperacaoVLDESCONTO: TDBEdit;
    pnlLayoutTitulosgodestinooperacaovldesconto: TPanel;
    pnlLayoutCamposgodestinooperacaovljuro: TPanel;
    dbegodestinooperacaoVLJURO: TDBEdit;
    pnlLayoutTitulosgodestinooperacaovljuro: TPanel;
    Panel2: TPanel;
    pnlLayoutTitulosgodestinooperacaovlliquido: TPanel;
    pnlLayoutCamposgodestinooperacaovlliquido: TPanel;
    dbegodestinooperacaoVliquido: TDBEdit;
    sqloutrasentradas: TSQLDataSet;
    dspoutrasentradas: TDataSetProvider;
    cdsoutrasentradas: TClientDataSet;
    dsoutrasentradas: TDataSource;
    sqlcpg: TSQLDataSet;
    dspcpg: TDataSetProvider;
    cdscpg: TClientDataSet;
    dscpg: TDataSource;
    sqlgodestinooperacao: TSQLDataSet;
    dspgodestinooperacao: TDataSetProvider;
    dsgodestinooperacao: TDataSource;
    sqlmovimento_financeiro: TSQLDataSet;
    dspmovimento_financeiro: TDataSetProvider;
    cdsmovimento_financeiro: TClientDataSet;
    dsmovimento_financeiro: TDataSource;
    sqlmovimento_financeiroID: TIntegerField;
    sqlmovimento_financeiroDT_CADASTRO: TDateField;
    sqlmovimento_financeiroHS_CADASTRO: TTimeField;
    sqlmovimento_financeiroDT_COMPETENCIA: TDateField;
    sqlmovimento_financeiroCODPROJETO: TIntegerField;
    sqlmovimento_financeiroPROJETO: TStringField;
    sqlmovimento_financeiroCODFILIAL: TIntegerField;
    sqlmovimento_financeiroFILIAL: TStringField;
    sqlmovimento_financeiroVLABERTURA: TFloatField;
    sqlmovimento_financeiroVLRECEBIVEISPREV: TFloatField;
    sqlmovimento_financeiroVLRECEBIVEISREALIZADO: TFloatField;
    sqlmovimento_financeiroVLOUTRASENTRADAS_PREV: TFloatField;
    sqlmovimento_financeiroVLOUTRASENTRADASREALIZADA: TFloatField;
    sqlmovimento_financeiroVLCAPTACAOFOMENTOPREV: TFloatField;
    sqlmovimento_financeiroVLCAPTACAOFOMENTOREALIZADO: TFloatField;
    sqlmovimento_financeiroVLOUTRASAIDASPREV: TFloatField;
    sqlmovimento_financeiroVLOUTRASSAIDASREALIZADO: TFloatField;
    cdsmovimento_financeiroID: TIntegerField;
    cdsmovimento_financeiroDT_CADASTRO: TDateField;
    cdsmovimento_financeiroHS_CADASTRO: TTimeField;
    cdsmovimento_financeiroDT_COMPETENCIA: TDateField;
    cdsmovimento_financeiroCODPROJETO: TIntegerField;
    cdsmovimento_financeiroPROJETO: TStringField;
    cdsmovimento_financeiroCODFILIAL: TIntegerField;
    cdsmovimento_financeiroFILIAL: TStringField;
    cdsmovimento_financeiroVLABERTURA: TFloatField;
    cdsmovimento_financeiroVLRECEBIVEISPREV: TFloatField;
    cdsmovimento_financeiroVLRECEBIVEISREALIZADO: TFloatField;
    cdsmovimento_financeiroVLOUTRASENTRADAS_PREV: TFloatField;
    cdsmovimento_financeiroVLOUTRASENTRADASREALIZADA: TFloatField;
    cdsmovimento_financeiroVLCAPTACAOFOMENTOPREV: TFloatField;
    cdsmovimento_financeiroVLCAPTACAOFOMENTOREALIZADO: TFloatField;
    cdsmovimento_financeiroVLOUTRASAIDASPREV: TFloatField;
    cdsmovimento_financeiroVLOUTRASSAIDASREALIZADO: TFloatField;
    sqlcaptacaofomento: TSQLDataSet;
    dspcaptacaofomento: TDataSetProvider;
    cdscaptacaofomento: TClientDataSet;
    dscaptacaofomento: TDataSource;
    sqlgodestinooperacaoID: TIntegerField;
    sqlgodestinooperacaoCODMOVFINANCEIRO: TIntegerField;
    sqlgodestinooperacaoDT_CADASTRO: TDateField;
    sqlgodestinooperacaoHS_CADASTRO: TTimeField;
    sqlgodestinooperacaoCODFUNDO: TIntegerField;
    sqlgodestinooperacaoFUNDO: TStringField;
    sqlgodestinooperacaoVLBRUTO: TFloatField;
    sqlgodestinooperacaoVLJUROS: TFloatField;
    sqlgodestinooperacaoVLMULTA: TFloatField;
    sqlgodestinooperacaoVLDESCONTO: TFloatField;
    sqlgodestinooperacaoVLLIQUIDO: TFloatField;
    sqlcpgID: TIntegerField;
    sqlcpgDT_CADASTRO: TDateField;
    sqlcpgHS_CADASTRO: TTimeField;
    sqlcpgCODMOVFINANCEIRO: TIntegerField;
    sqlcpgCODPROJETO: TIntegerField;
    sqlcpgPROJETO: TStringField;
    sqlcpgID_FORNECEDOR: TIntegerField;
    sqlcpgFORNECEDOR: TStringField;
    sqlcpgCODFILIAL: TIntegerField;
    sqlcpgFILIAL: TStringField;
    sqlcpgPLANOCONTA: TStringField;
    sqlcpgDESCRICAO: TStringField;
    sqlcpgDOCUMENTO: TStringField;
    sqlcpgVL_BRUTO: TFloatField;
    sqlcpgVL_MULTA: TFloatField;
    sqlcpgVL_JUROS: TFloatField;
    sqlcpgVL_DESCONTO: TFloatField;
    sqlcpgDT_VENCIMENTO: TDateField;
    sqlcpgFORMA_PAGAMENTO: TStringField;
    sqlcpgVL_PAGO: TFloatField;
    sqlcpgDT_PAGAMENTO: TDateField;
    sqlcpgID_BANCO_CONTA: TIntegerField;
    sqlcpgBANCO_CONTA: TStringField;
    sqloutrasentradasID: TIntegerField;
    sqloutrasentradasDT_CADASTRO: TDateField;
    sqloutrasentradasHS_CADASTRO: TTimeField;
    sqloutrasentradasCODMOVFINANCEIRO: TIntegerField;
    sqloutrasentradasDTCOMPETENCIA: TDateField;
    sqloutrasentradasDESCRICAO: TStringField;
    sqloutrasentradasTIPO: TStringField;
    sqloutrasentradasVLENTRADA: TFloatField;
    sqloutrasentradasVLENTRADAREALIZADO: TFloatField;
    sqlcaptacaofomentoID: TIntegerField;
    sqlcaptacaofomentoDT_CADASTRO: TDateField;
    sqlcaptacaofomentoHS_CADASTRO: TTimeField;
    sqlcaptacaofomentoCODMOVFINANCEIRO: TIntegerField;
    sqlcaptacaofomentoDTCOMPETENCIA: TDateField;
    sqlcaptacaofomentoDESCRICAO: TStringField;
    sqlcaptacaofomentoCOD_FUNDO: TIntegerField;
    sqlcaptacaofomentoFUNDO: TStringField;
    sqlcaptacaofomentoVLPREVISTO: TFloatField;
    sqlcaptacaofomentoVLREALIZADO: TFloatField;
    cdscpgTOTALVLPAGAR: TAggregateField;
    cdscpgTOTALVLPAGO: TAggregateField;
    cdscaptacaofomentoTOTALVLPREVISTO: TAggregateField;
    cdscaptacaofomentoTOTALVLREALIZADO: TAggregateField;
    cdsmovimento_financeiroSALDOFINALPREVISTO: TAggregateField;
    cdsmovimento_financeiroSALDOFINALREALIZADO: TAggregateField;
    GridgodestinooperacaoDBTVVLIQUIDO: TcxGridDBColumn;
    cdsoutrasentradasTOTALVLENTRADA: TAggregateField;
    cdsoutrasentradasTOTALVLENTRADAREALIZADO: TAggregateField;
    cdsgodestinooperacao: TClientDataSet;
    cdsgodestinooperacaoID: TIntegerField;
    cdsgodestinooperacaoCODMOVFINANCEIRO: TIntegerField;
    cdsgodestinooperacaoDT_CADASTRO: TDateField;
    cdsgodestinooperacaoHS_CADASTRO: TTimeField;
    cdsgodestinooperacaoCODFUNDO: TIntegerField;
    cdsgodestinooperacaoFUNDO: TStringField;
    cdsgodestinooperacaoVLBRUTO: TFloatField;
    cdsgodestinooperacaoVLJUROS: TFloatField;
    cdsgodestinooperacaoVLMULTA: TFloatField;
    cdsgodestinooperacaoVLDESCONTO: TFloatField;
    cdsgodestinooperacaoVLLIQUIDO: TFloatField;
    cdsgodestinooperacaoTOTALIQUIDO: TAggregateField;
    GridResultadoPesquisaDBTVSALDOFINALPREVISTO: TcxGridDBColumn;
    GridResultadoPesquisaDBTVSALDOFINALREALIZADO: TcxGridDBColumn;
    cdscpgID: TIntegerField;
    cdscpgDT_CADASTRO: TDateField;
    cdscpgHS_CADASTRO: TTimeField;
    cdscpgCODPROJETO: TIntegerField;
    cdscpgPROJETO: TStringField;
    cdscpgID_FORNECEDOR: TIntegerField;
    cdscpgFORNECEDOR: TStringField;
    cdscpgDESCRICAO: TStringField;
    cdscpgDOCUMENTO: TStringField;
    cdscpgVL_BRUTO: TFloatField;
    cdscpgVL_MULTA: TFloatField;
    cdscpgVL_JUROS: TFloatField;
    cdscpgVL_DESCONTO: TFloatField;
    cdscpgDT_VENCIMENTO: TDateField;
    cdscpgFORMA_PAGAMENTO: TStringField;
    cdscpgVL_PAGO: TFloatField;
    cdscpgDT_PAGAMENTO: TDateField;
    cdscpgID_BANCO_CONTA: TIntegerField;
    cdscpgBANCO_CONTA: TStringField;
    cdscpgSEL: TBooleanField;
    cdscpgCODMOVFINANCEIRO: TIntegerField;
    cdscpgVLPAGAR: TFloatField;
    cdscpgPLANOCONTA: TStringField;
    cdscpgCODFILIAL: TIntegerField;
    cdscpgFILIAL: TStringField;
    cdsoutrasentradasID: TIntegerField;
    cdsoutrasentradasDT_CADASTRO: TDateField;
    cdsoutrasentradasHS_CADASTRO: TTimeField;
    cdsoutrasentradasCODMOVFINANCEIRO: TIntegerField;
    cdsoutrasentradasDTCOMPETENCIA: TDateField;
    cdsoutrasentradasDESCRICAO: TStringField;
    cdsoutrasentradasTIPO: TStringField;
    cdsoutrasentradasVLENTRADA: TFloatField;
    cdsoutrasentradasVLENTRADAREALIZADO: TFloatField;
    cdscaptacaofomentoID: TIntegerField;
    cdscaptacaofomentoDT_CADASTRO: TDateField;
    cdscaptacaofomentoHS_CADASTRO: TTimeField;
    cdscaptacaofomentoCODMOVFINANCEIRO: TIntegerField;
    cdscaptacaofomentoDTCOMPETENCIA: TDateField;
    cdscaptacaofomentoDESCRICAO: TStringField;
    cdscaptacaofomentoCOD_FUNDO: TIntegerField;
    cdscaptacaofomentoFUNDO: TStringField;
    cdscaptacaofomentoVLPREVISTO: TFloatField;
    cdscaptacaofomentoVLREALIZADO: TFloatField;
    frxmovimentofinanceiroi: TfrxReport;
    popupImpressao: TPopupMenu;
    menuImpRelSintetico: TMenuItem;
    RelatrioII1: TMenuItem;
    RelatrioIII1: TMenuItem;
    RelatrioIV1: TMenuItem;
    frxmovimentofinanceiroIII: TfrxReport;
    frxmovimentofinanceiroIV: TfrxReport;
    frxmovimentofinanceiroII: TfrxReport;
    cdsmovimento_financeiroAGRUPADORI: TStringField;
    spbatualizarhistorico: TButton;
    PKG_ATUALIZAR_FINANCEIROI: TSQLStoredProc;
    cdsmovimento_financeiroMENORVLABERTURA: TFloatField;
    cdsmovimento_financeiroSALDOFINALCALCULADO1: TFloatField;
    cdsmovimento_financeiroSALDOFINALCALCULADO2: TFloatField;
    frxmovimentofinanceirov: TfrxReport;
    lblversao: TLabel;
    lblProjetoresultado: TLabel;
    lblusuario: TLabel;
    lblusuarioresultado: TLabel;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure cdsmovimento_financeiroAfterClose(DataSet: TDataSet);
	procedure cdsmovimento_financeiroAfterOpen(DataSet: TDataSet);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionarmovimento_financeiroClick(Sender: TObject);
	procedure spdAlterarmovimento_financeiroClick(Sender: TObject);
	procedure spdGravarmovimento_financeiroClick(Sender: TObject);
	procedure spdCancelarmovimento_financeiroClick(Sender: TObject);
	procedure spdExcluirmovimento_financeiroClick(Sender: TObject);
	procedure cdsmovimento_financeiroBeforePost(DataSet: TDataSet);
	procedure spdAdicionaroutrasentradasClick(Sender: TObject);
	procedure spdAlteraroutrasentradasClick(Sender: TObject);
	procedure spdGravaroutrasentradasClick(Sender: TObject);
	procedure spdCancelaroutrasentradasClick(Sender: TObject);
	procedure spdExcluiroutrasentradasClick(Sender: TObject);
	procedure cdsoutrasentradasBeforePost(DataSet: TDataSet);
	procedure spdAdicionarcaptacaofomentoClick(Sender: TObject);
	procedure spdAlterarcaptacaofomentoClick(Sender: TObject);
	procedure spdGravarcaptacaofomentoClick(Sender: TObject);
	procedure spdCancelarcaptacaofomentoClick(Sender: TObject);
	procedure spdExcluircaptacaofomentoClick(Sender: TObject);
  procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdscaptacaofomentoBeforePost(DataSet: TDataSet);
    procedure cdsoutrasentradasAfterPost(DataSet: TDataSet);
    procedure cdscaptacaofomentoAfterPost(DataSet: TDataSet);
    procedure cdsoutrasentradasNewRecord(DataSet: TDataSet);
    procedure cdscaptacaofomentoNewRecord(DataSet: TDataSet);
    procedure spdAdicionarcpgClick(Sender: TObject);
    procedure spdAlterarcpgClick(Sender: TObject);
    procedure spdGravarcpgClick(Sender: TObject);
    procedure spdCancelarcpgClick(Sender: TObject);
    procedure spdExcluircpgClick(Sender: TObject);
    procedure spdImportarClick(Sender: TObject);
    procedure cdscpgBeforePost(DataSet: TDataSet);
    procedure cdscpgCalcFields(DataSet: TDataSet);
    procedure cdscpgAfterPost(DataSet: TDataSet);
    procedure dbecpgVL_BRUTOExit(Sender: TObject);
    procedure dbecpgVL_MULTAExit(Sender: TObject);
    procedure dbecpgVL_JUROSExit(Sender: TObject);
    procedure dbecpgVL_DESCONTOExit(Sender: TObject);
    procedure btnBaixaClick(Sender: TObject);
    procedure spdAdicionargodestinooperacaoClick(Sender: TObject);
    procedure spdAlterargodestinooperacaoClick(Sender: TObject);
    procedure spdGravargodestinooperacaoClick(Sender: TObject);
    procedure spdCancelargodestinooperacaoClick(Sender: TObject);
    procedure spdExcluirgodestinooperacaoClick(Sender: TObject);
    procedure cdsgodestinooperacaoBeforePost(DataSet: TDataSet);
    procedure dbegodestinooperacaoVLBRUTOExit(Sender: TObject);
    procedure dbegodestinooperacaoVLJUROExit(Sender: TObject);
    procedure dbegodestinooperacaoVLMULTAExit(Sender: TObject);
    procedure dbegodestinooperacaoVLDESCONTOExit(Sender: TObject);
    procedure cdsmovimento_financeiroNewRecord(DataSet: TDataSet);
    procedure cdsgodestinooperacaoAfterPost(DataSet: TDataSet);
    procedure cdscpgNewRecord(DataSet: TDataSet);
    procedure cdsoutrasentradasAfterDelete(DataSet: TDataSet);
    procedure cdscpgAfterDelete(DataSet: TDataSet);
    procedure RelatrioII1Click(Sender: TObject);
    procedure RelatrioIII1Click(Sender: TObject);
    procedure RelatrioIV1Click(Sender: TObject);
    procedure spdOpcoesClick(Sender: TObject);
    procedure cdsmovimento_financeiroCalcFields(DataSet: TDataSet);
    procedure spbatualizarhistoricoClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncmovimento_financeiro: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosmovimento_financeiro: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosmovimento_financeiro: Array[0..15]  of TControl;
	objetosModoEdicaoInativosmovimento_financeiro: Array[0..15]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListamovimento_financeiro: Array[0..0]  of TDuplicidade;
	duplicidadeCampomovimento_financeiro: TDuplicidade;
	{container de funcoes}
	fncoutrasentradas: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosoutrasentradas: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosoutrasentradas: Array[0..10]  of TControl;
	objetosModoEdicaoInativosoutrasentradas: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListaoutrasentradas: Array[0..0]  of TDuplicidade;
	duplicidadeCampooutrasentradas: TDuplicidade;
	{container de funcoes}
	fnccaptacaofomento: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatorioscaptacaofomento: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivoscaptacaofomento: Array[0..10]  of TControl;
	objetosModoEdicaoInativoscaptacaofomento: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListacaptacaofomento: Array[0..0]  of TDuplicidade;
	duplicidadeCampocaptacaofomento: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;


  	{container de funcoes}
	fnccpg: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatorioscpg: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivoscpg: Array[0..15]  of TControl;
	objetosModoEdicaoInativoscpg: Array[0..15]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListacpg: Array[0..0]  of TDuplicidade;
	duplicidadeCampocpg: TDuplicidade;
	{array de crit�rios de pesquisas}

  	{container de funcoes}
	fncgodestinooperacao: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgodestinooperacao: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgodestinooperacao: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgodestinooperacao: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagodestinooperacao: Array[0..0]  of TDuplicidade;
	duplicidadeCampogodestinooperacao: TDuplicidade;
	{array de crit�rios de pesquisas}


	pesquisasItem: TPesquisas;
    procedure sessao;
    procedure calcular_valorpagar;
    procedure calcularvlliquidodestinooperacao;
    procedure somar_fomento;
    procedure somar_outrasentradas;
    procedure somar_cpg;
    procedure somar_operacaodestino;
    function TrataAcentos(Dados : String) : String;
    function stringtoStream(const Astring: string): Tstream;
	public

end;

var
fmovimento_financeiro: Tfmovimento_financeiro;

implementation

uses udm, uimportacao, udtcompetencia, System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfmovimento_financeiro.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfmovimento_financeiro.GridResultadoPesquisaDBTVDblClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabmovimento_financeiro.TabIndex;
end;

procedure Tfmovimento_financeiro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncmovimento_financeiro.verificarEmTransacao(cdsmovimento_financeiro);
	fncoutrasentradas.verificarEmTransacao(cdsoutrasentradas);
	fnccaptacaofomento.verificarEmTransacao(cdscaptacaofomento);
  fnccpg.verificarEmTransacao(cdscpg);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncmovimento_financeiro.configurarGridesFormulario(fmovimento_financeiro,true, false);

	fncmovimento_financeiro.Free;
	fncoutrasentradas.Free;
	fnccaptacaofomento.Free;
	{eliminando container de fun��es da memoria<<<}

	fmovimento_financeiro:=nil;
	Action:=cafree;

end;

procedure Tfmovimento_financeiro.FormCreate(Sender: TObject);
begin
  sessao;

  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
	{>>>container de fun��es}
	fncmovimento_financeiro:= TFuncoes.Create(Self);
	fncmovimento_financeiro.definirConexao(dm.conexao);
	fncmovimento_financeiro.definirConexaohistorico(dm.conexao);
	fncmovimento_financeiro.definirFormulario(Self);
	fncmovimento_financeiro.validarTransacaoRelacionamento:=true;
	fncmovimento_financeiro.autoAplicarAtualizacoesBancoDados:=true;

	fncoutrasentradas:= TFuncoes.Create(Self);
	fncoutrasentradas.definirConexao(dm.conexao);
	fncoutrasentradas.definirConexaohistorico(dm.conexao);
	fncoutrasentradas.definirFormulario(Self);
	fncoutrasentradas.validarTransacaoRelacionamento:=true;
	fncoutrasentradas.autoAplicarAtualizacoesBancoDados:=true;

	fnccaptacaofomento:= TFuncoes.Create(Self);
	fnccaptacaofomento.definirConexao(dm.conexao);
	fnccaptacaofomento.definirConexaohistorico(dm.conexao);
	fnccaptacaofomento.definirFormulario(Self);
	fnccaptacaofomento.validarTransacaoRelacionamento:=true;
	fnccaptacaofomento.autoAplicarAtualizacoesBancoDados:=true;


  fnccpg:= TFuncoes.Create(Self);
  fnccpg.definirConexao(dm.conexao);
  fnccpg.definirConexaohistorico(dm.conexao);
  fnccpg.definirFormulario(Self);
  fnccpg.validarTransacaoRelacionamento:=true;
  fnccpg.autoAplicarAtualizacoesBancoDados:=true;


  	{>>>container de fun��es}
	fncgodestinooperacao:= TFuncoes.Create(Self);
	fncgodestinooperacao.definirConexao(dm.conexao);
	fncgodestinooperacao.definirConexaohistorico(dm.conexao);
	fncgodestinooperacao.definirFormulario(Self);
	fncgodestinooperacao.validarTransacaoRelacionamento:=true;
	fncgodestinooperacao.autoAplicarAtualizacoesBancoDados:=true;

	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

  dm.FDQuery.Close;
  dm.FDQuery.SQL.Clear;
  dm.FDQuery.sql.Add('SELECT DISTINCT(PLANOCONTA)PLANOCONTA FROM gocpg');
  dm.FDQuery.Open();
  cmbcpgplanoconta.Properties.Items.Clear;
  while not dm.FDQuery.Eof do
  begin
    cmbcpgplanoconta.Properties.Items.Add(dm.FDQuery.FieldByName('PLANOCONTA').AsString);
    dm.FDQuery.Next;
  end;

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Dt.Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODPROJETO';
	pesquisasItem.titulo:='C�d.Projeto';
	pesquisasItem.campo:='CODPROJETO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPROJETO';
	pesquisasItem.titulo:='Projeto';
	pesquisasItem.campo:='PROJETO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODFILIAL';
	pesquisasItem.titulo:='C�d.Filial';
	pesquisasItem.campo:='CODFILIAL';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesFILIAL';
	pesquisasItem.titulo:='Filial';
	pesquisasItem.campo:='FILIAL';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;
    {gerando campos no clientdatsaet de filtros}
  fncmovimento_financeiro.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncmovimento_financeiro.criarPesquisas(sqlmovimento_financeiro,cdsmovimento_financeiro,'select * from movimento_financeiro', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios movimento_financeiro}
	{campos obrigatorios movimento_financeiro<<<}

	{>>>objetos ativos no modo de manipula��o de dados movimento_financeiro}
	objetosModoEdicaoAtivosmovimento_financeiro[0]:=spdCancelarmovimento_financeiro;
	objetosModoEdicaoAtivosmovimento_financeiro[1]:=spdGravarmovimento_financeiro;
  objetosModoEdicaoAtivosmovimento_financeiro[2]:=pnlLayoutCamposmovimento_financeirocodprojeto;
  objetosModoEdicaoAtivosmovimento_financeiro[3]:=pnlLayoutCamposmovimento_financeirocodfilial;
  objetosModoEdicaoAtivosmovimento_financeiro[4]:=dbemovimento_financeiroCODPROJETO;
  objetosModoEdicaoAtivosmovimento_financeiro[5]:=dbemovimento_financeiroCODFILIAL;
  objetosModoEdicaoAtivosmovimento_financeiro[6]:=btnprojeto;
  objetosModoEdicaoAtivosmovimento_financeiro[7]:=btnfilial;

	{objetos ativos no modo de manipula��o de dados movimento_financeiro<<<}

	{>>>objetos inativos no modo de manipula��o de dados movimento_financeiro}
	objetosModoEdicaoInativosmovimento_financeiro[0]:=spdAdicionarmovimento_financeiro;
	objetosModoEdicaoInativosmovimento_financeiro[1]:=spdAlterarmovimento_financeiro;
	objetosModoEdicaoInativosmovimento_financeiro[2]:=spdExcluirmovimento_financeiro;
	objetosModoEdicaoInativosmovimento_financeiro[3]:=spdOpcoes;
	objetosModoEdicaoInativosmovimento_financeiro[4]:=tabPesquisas;
  objetosModoEdicaoInativosmovimento_financeiro[5]:=dbemovimento_financeiroPROJETO;
  objetosModoEdicaoInativosmovimento_financeiro[6]:=dbemovimento_financeiroFILIAL;
  objetosModoEdicaoInativosmovimento_financeiro[7]:=taboutrasentradas;
  objetosModoEdicaoInativosmovimento_financeiro[8]:=tabcaptacaofomento;
  objetosModoEdicaoInativosmovimento_financeiro[9]:=tabcpg;
  objetosModoEdicaoInativosmovimento_financeiro[10]:=tabgodestinooperacao;
  objetosModoEdicaoInativosmovimento_financeiro[11]:=dbemovimento_financeiroPROJETO;
  objetosModoEdicaoInativosmovimento_financeiro[12]:=dbemovimento_financeiroFILIAL;
	{objetos inativos no modo de manipula��o de dados movimento_financeiro<<<}

	{>>>comando de adi��o de teclas de atalhos movimento_financeiro}
	fncmovimento_financeiro.criaAtalhoPopupMenu(Popupmovimento_financeiro,'Adicionar novo registro',VK_F4,spdAdicionarmovimento_financeiro.OnClick);
	fncmovimento_financeiro.criaAtalhoPopupMenu(Popupmovimento_financeiro,'Alterar registro selecionado',VK_F5,spdAlterarmovimento_financeiro.OnClick);
	fncmovimento_financeiro.criaAtalhoPopupMenu(Popupmovimento_financeiro,'Gravar �ltimas altera��es',VK_F6,spdGravarmovimento_financeiro.OnClick);
	fncmovimento_financeiro.criaAtalhoPopupMenu(Popupmovimento_financeiro,'Cancelar �ltimas altera��e',VK_F7,spdCancelarmovimento_financeiro.OnClick);
	fncmovimento_financeiro.criaAtalhoPopupMenu(Popupmovimento_financeiro,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirmovimento_financeiro.OnClick);
  fncmovimento_financeiro.criaAtalhoPopupMenu(Popupmovimento_financeiro,'Atualizar Hist�rico Financeiro',ShortCut(VK_f12,[ssCtrl]),spbatualizarhistorico.OnClick);
	{comando de adi��o de teclas de atalhos movimento_financeiro<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsmovimento_financeiro.open();
  {filtros projeto}
  if ParamStr(5) <>'4' then
  fncmovimento_financeiro.filtrar(cdsmovimento_financeiro,'CODPROJETO='+ParamStr(5));
  {filtros projeto}

	{abertura dos datasets<<<}

	{>>>campos obrigatorios outrasentradas}
	{campos obrigatorios outrasentradas<<<}

	{>>>objetos ativos no modo de manipula��o de dados outrasentradas}
	objetosModoEdicaoAtivosoutrasentradas[0]:=pnlDadosoutrasentradas;
	objetosModoEdicaoAtivosoutrasentradas[1]:=spdCancelaroutrasentradas;
	objetosModoEdicaoAtivosoutrasentradas[2]:=spdGravaroutrasentradas;
	{objetos ativos no modo de manipula��o de dados outrasentradas<<<}

	{>>>objetos inativos no modo de manipula��o de dados outrasentradas}
	objetosModoEdicaoInativosoutrasentradas[0]:=spdAdicionaroutrasentradas;
	objetosModoEdicaoInativosoutrasentradas[1]:=spdAlteraroutrasentradas;
	objetosModoEdicaoInativosoutrasentradas[2]:=spdExcluiroutrasentradas;
	objetosModoEdicaoInativosoutrasentradas[3]:=spdOpcoes;
	objetosModoEdicaoInativosoutrasentradas[4]:=tabPesquisas;
  objetosModoEdicaoInativosoutrasentradas[5]:=tabmovimento_financeiro;
  objetosModoEdicaoInativosoutrasentradas[6]:=tabcaptacaofomento;
  objetosModoEdicaoInativosoutrasentradas[7]:=tabcpg;

	{objetos inativos no modo de manipula��o de dados outrasentradas<<<}

	{>>>comando de adi��o de teclas de atalhos outrasentradas}
	fncoutrasentradas.criaAtalhoPopupMenu(Popupoutrasentradas,'Adicionar novo registro',VK_F4,spdAdicionaroutrasentradas.OnClick);
	fncoutrasentradas.criaAtalhoPopupMenu(Popupoutrasentradas,'Alterar registro selecionado',VK_F5,spdAlteraroutrasentradas.OnClick);
	fncoutrasentradas.criaAtalhoPopupMenu(Popupoutrasentradas,'Gravar �ltimas altera��es',VK_F6,spdGravaroutrasentradas.OnClick);
	fncoutrasentradas.criaAtalhoPopupMenu(Popupoutrasentradas,'Cancelar �ltimas altera��e',VK_F7,spdCancelaroutrasentradas.OnClick);
	fncoutrasentradas.criaAtalhoPopupMenu(Popupoutrasentradas,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluiroutrasentradas.OnClick);

	{comando de adi��o de teclas de atalhos outrasentradas<<<}


	{>>>abertura dos datasets}
	cdsoutrasentradas.open();
	{abertura dos datasets<<<}

	{>>>campos obrigatorios captacaofomento}
	{campos obrigatorios captacaofomento<<<}

	{>>>objetos ativos no modo de manipula��o de dados captacaofomento}
	objetosModoEdicaoAtivoscaptacaofomento[0]:=pnlLayoutCamposcaptacaofomentocod_fundo;
	objetosModoEdicaoAtivoscaptacaofomento[1]:=spdCancelarcaptacaofomento;
	objetosModoEdicaoAtivoscaptacaofomento[2]:=spdGravarcaptacaofomento;
  objetosModoEdicaoAtivoscaptacaofomento[3]:=dbecaptacaofomentoCOD_FUNDO;
  objetosModoEdicaoAtivoscaptacaofomento[4]:=btnpesquisarfundo;

	{objetos ativos no modo de manipula��o de dados captacaofomento<<<}

	{>>>objetos inativos no modo de manipula��o de dados captacaofomento}
	objetosModoEdicaoInativoscaptacaofomento[0]:=spdAdicionarcaptacaofomento;
	objetosModoEdicaoInativoscaptacaofomento[1]:=spdAlterarcaptacaofomento;
	objetosModoEdicaoInativoscaptacaofomento[2]:=spdExcluircaptacaofomento;
	objetosModoEdicaoInativoscaptacaofomento[3]:=spdOpcoes;
	objetosModoEdicaoInativoscaptacaofomento[4]:=tabPesquisas;
  objetosModoEdicaoInativoscaptacaofomento[5]:=dbecaptacaofomentoFUNDO;
  objetosModoEdicaoInativoscaptacaofomento[6]:=taboutrasentradas;
  objetosModoEdicaoInativoscaptacaofomento[7]:=tabmovimento_financeiro;
  objetosModoEdicaoInativoscaptacaofomento[8]:=tabcpg;
  objetosModoEdicaoInativoscaptacaofomento[9]:=tabgodestinooperacao;
  objetosModoEdicaoInativoscaptacaofomento[10]:=dbecaptacaofomentoFUNDO;
	{objetos inativos no modo de manipula��o de dados captacaofomento<<<}

	{>>>comando de adi��o de teclas de atalhos captacaofomento}
	fnccaptacaofomento.criaAtalhoPopupMenu(Popupcaptacaofomento,'Adicionar novo registro',VK_F4,spdAdicionarcaptacaofomento.OnClick);
	fnccaptacaofomento.criaAtalhoPopupMenu(Popupcaptacaofomento,'Alterar registro selecionado',VK_F5,spdAlterarcaptacaofomento.OnClick);
	fnccaptacaofomento.criaAtalhoPopupMenu(Popupcaptacaofomento,'Gravar �ltimas altera��es',VK_F6,spdGravarcaptacaofomento.OnClick);
	fnccaptacaofomento.criaAtalhoPopupMenu(Popupcaptacaofomento,'Cancelar �ltimas altera��e',VK_F7,spdCancelarcaptacaofomento.OnClick);
	fnccaptacaofomento.criaAtalhoPopupMenu(Popupcaptacaofomento,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluircaptacaofomento.OnClick);

	{comando de adi��o de teclas de atalhos captacaofomento<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdscaptacaofomento.open();
	{abertura dos datasets<<<}

  {>>>campos obrigatorios cpg}
    camposObrigatorioscpg[0] :=dbecpgcodprojeto;
    //camposObrigatorioscpg[1] :=dbecpgid_fornecedor;
    {campos obrigatorios cpg<<<}

    {>>>objetos ativos no modo de manipula��o de dados cpg}

    objetosModoEdicaoAtivoscpg[0]:=spdCancelarcpg;
    objetosModoEdicaoAtivoscpg[1]:=spdGravarcpg;
    objetosModoEdicaoAtivoscpg[2]:=btnpesquisaprojetocpg;
    objetosModoEdicaoAtivoscpg[3]:=btnFornecedor;
    objetosModoEdicaoAtivoscpg[4]:=btnBancoConta;
    objetosModoEdicaoAtivoscpg[5]:=dbecpgCODPROJETO;
    objetosModoEdicaoAtivoscpg[6]:=dbecpgID_FORNECEDOR;
    objetosModoEdicaoAtivoscpg[7]:=dbecpgID_BANCO_CONTA;

    {objetos ativos no modo de manipula��o de dados cpg<<<}

    {>>>objetos inativos no modo de manipula��o de dados cpg}
    objetosModoEdicaoInativoscpg[0]:=spdAdicionarcpg;
    objetosModoEdicaoInativoscpg[1]:=spdAlterarcpg;
    objetosModoEdicaoInativoscpg[2]:=spdExcluircpg;
    objetosModoEdicaoInativoscpg[3]:=spdOpcoes;
    objetosModoEdicaoInativoscpg[4]:=tabPesquisas;
    objetosModoEdicaoInativoscpg[5]:=tabmovimento_financeiro;
    objetosModoEdicaoInativoscpg[6]:=taboutrasentradas;
    objetosModoEdicaoInativoscpg[7]:=tabcaptacaofomento;
    objetosModoEdicaoInativoscpg[8]:=dbecpgPROJETO;
    objetosModoEdicaoInativoscpg[9]:=dbecpgFORNECEDOR;
    objetosModoEdicaoInativoscpg[10]:=dbecpgBANCO_CONTA;
    {objetos inativos no modo de manipula��o de dados cpg<<<}

    {>>>comando de adi��o de teclas de atalhos cpg}
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Adicionar novo registro',VK_F4,spdAdicionarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Alterar registro selecionado',VK_F5,spdAlterarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Gravar �ltimas altera��es',VK_F6,spdGravarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Cancelar �ltimas altera��e',VK_F7,spdCancelarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluircpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Importar Planilha',ShortCut(VK_f9,[ssCtrl]),spdImportar.OnClick);

    {comando de adi��o de teclas de atalhos cpg<<<}

    {>>>objetos ativos no modo de manipula��o de dados godestinooperacao}
    objetosModoEdicaoAtivosgodestinooperacao[0]:=spdCancelargodestinooperacao;
    objetosModoEdicaoAtivosgodestinooperacao[1]:=spdGravargodestinooperacao;
    objetosModoEdicaoAtivosgodestinooperacao[2]:=dbegodestinooperacaoCODFUNDO;
    objetosModoEdicaoAtivosgodestinooperacao[3]:=btnfundodestino;

    {objetos ativos no modo de manipula��o de dados godestinooperacao<<<}

    {>>>objetos inativos no modo de manipula��o de dados godestinooperacao}
    objetosModoEdicaoInativosgodestinooperacao[0]:=spdAdicionargodestinooperacao;
    objetosModoEdicaoInativosgodestinooperacao[1]:=spdAlterargodestinooperacao;
    objetosModoEdicaoInativosgodestinooperacao[2]:=spdExcluirgodestinooperacao;
    objetosModoEdicaoInativosgodestinooperacao[3]:=spdOpcoes;
    objetosModoEdicaoInativosgodestinooperacao[4]:=dbemovimento_financeiroPROJETO;
    objetosModoEdicaoInativosgodestinooperacao[4]:=dbemovimento_financeiroFILIAL;
    objetosModoEdicaoInativosgodestinooperacao[6]:=taboutrasentradas;
    objetosModoEdicaoInativosgodestinooperacao[7]:=tabcaptacaofomento;
    objetosModoEdicaoInativosgodestinooperacao[8]:=tabcpg;
    objetosModoEdicaoInativosgodestinooperacao[9]:=tabmovimento_financeiro;
    objetosModoEdicaoInativosgodestinooperacao[10]:=dbegodestinooperacaoFUNDO;

    {objetos inativos no modo de manipula��o de dados godestinooperacao<<<}

    {>>>comando de adi��o de teclas de atalhos godestinooperacao}
    fncgodestinooperacao.criaAtalhoPopupMenu(Popupgodestinooperacao,'Adicionar novo registro',VK_F4,spdAdicionargodestinooperacao.OnClick);
    fncgodestinooperacao.criaAtalhoPopupMenu(Popupgodestinooperacao,'Alterar registro selecionado',VK_F5,spdAlterargodestinooperacao.OnClick);
    fncgodestinooperacao.criaAtalhoPopupMenu(Popupgodestinooperacao,'Gravar �ltimas altera��es',VK_F6,spdGravargodestinooperacao.OnClick);
    fncgodestinooperacao.criaAtalhoPopupMenu(Popupgodestinooperacao,'Cancelar �ltimas altera��e',VK_F7,spdCancelargodestinooperacao.OnClick);
    fncgodestinooperacao.criaAtalhoPopupMenu(Popupgodestinooperacao,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgodestinooperacao.OnClick);

    {comando de adi��o de teclas de atalhos godestinooperacao<<<}

    pgcPrincipal.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdsgodestinooperacao.open();
    {abertura dos datasets<<<}


    fncmovimento_financeiro.criaAtalhoPopupMenuNavegacao(Popupmovimento_financeiro,cdsmovimento_financeiro);

    fncmovimento_financeiro.definirMascaraCampos(cdsmovimento_financeiro);

    fncmovimento_financeiro.configurarGridesFormulario(fmovimento_financeiro,false, true);

    fncoutrasentradas.definirMascaraCampos(cdsoutrasentradas);

    fnccaptacaofomento.definirMascaraCampos(cdscaptacaofomento);

    fncgodestinooperacao.definirMascaraCampos(cdsgodestinooperacao);


    {<<vers�o da rotina>>}
    lblversao.Caption:=lblversao.Caption+' - '+fncgodestinooperacao.recuperarVersaoExecutavel;
    {<<vers�o da rotina>>}



end;

procedure Tfmovimento_financeiro.spdAdicionarmovimento_financeiroClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabmovimento_financeiro.TabIndex;
	if (fncmovimento_financeiro.adicionar(cdsmovimento_financeiro)=true) then
		begin
			fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,true);
			dbemovimento_financeirodt_competencia.SetFocus;
	end
	else
		fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,false);
end;

procedure Tfmovimento_financeiro.spdAlterarmovimento_financeiroClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabmovimento_financeiro.TabIndex;
	if (fncmovimento_financeiro.alterar(cdsmovimento_financeiro)=true) then
		begin
			fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,true);
			dbemovimento_financeirodt_competencia.SetFocus;
	end
	else
		fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,false);
end;

procedure Tfmovimento_financeiro.spdGravarmovimento_financeiroClick(Sender: TObject);
begin
	if (fncmovimento_financeiro.gravar(cdsmovimento_financeiro)=true) then
		fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,false)
	else
		fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,true);
end;

procedure Tfmovimento_financeiro.spdCancelarmovimento_financeiroClick(Sender: TObject);
begin
	if (fncmovimento_financeiro.cancelar(cdsmovimento_financeiro)=true) then
		fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,false)
	else
		fncmovimento_financeiro.ativarModoEdicao(objetosModoEdicaoAtivosmovimento_financeiro,objetosModoEdicaoInativosmovimento_financeiro,true);
end;

procedure Tfmovimento_financeiro.spdExcluirmovimento_financeiroClick(Sender: TObject);
begin
	fncmovimento_financeiro.excluir(cdsmovimento_financeiro);
end;

procedure Tfmovimento_financeiro.cdsmovimento_financeiroBeforePost(DataSet: TDataSet);
begin
	if cdsmovimento_financeiro.State in [dsinsert] then
	cdsmovimento_financeiroID.Value:=fncmovimento_financeiro.autoNumeracaoGenerator(dm.conexao,'G_movimento_financeiro');

   dm.FDQuery.Close;
   dm.FDQuery.sql.Clear;
   dm.FDQuery.sql.Add('select max(DT_COMPETENCIA) DT_COMPETENCIA, sum(coalesce(VLABERTURA, 0)) VLABERTURA from MOVIMENTO_FINANCEIRO where codprojeto=:codprojeto and codfilial=:codfilial');
   dm.FDQuery.ParamByName('codfilial').asInteger  :=cdsmovimento_financeiroCODFILIAL.AsInteger;
   dm.FDQuery.ParamByName('codprojeto').asInteger :=cdsmovimento_financeiroCODPROJETO.AsInteger;
   dm.FDQuery.Open();

   cdsmovimento_financeiroVLABERTURA.AsFloat:= dm.FDQuery.FieldByName('vlabertura').AsFloat;

end;



procedure Tfmovimento_financeiro.cdsmovimento_financeiroCalcFields(
  DataSet: TDataSet);
begin
 cdsmovimento_financeiroAGRUPADORI.AsString:= cdsmovimento_financeiroCODFILIAL.AsString+'-'+cdsmovimento_financeiroCODPROJETO.AsString;
 if cdsmovimento_financeiro.RecNo=1 then
 begin
   cdsmovimento_financeiroMENORVLABERTURA.AsFloat :=cdsmovimento_financeiroVLABERTURA.AsFloat;
 end;

 if cdsmovimento_financeiroMENORVLABERTURA.AsFloat>0 then
 begin
    cdsmovimento_financeiroSALDOFINALCALCULADO1.AsFloat:=  cdsmovimento_financeiroMENORVLABERTURA.AsFloat+
                                                           cdsmovimento_financeiroVLRECEBIVEISPREV.AsFloat+
                                                           cdsmovimento_financeiroVLOUTRASENTRADAS_PREV.AsFloat+
                                                           cdsmovimento_financeiroVLCAPTACAOFOMENTOPREV.AsFloat-
                                                           cdsmovimento_financeiroVLOUTRASAIDASPREV.AsFloat;

    cdsmovimento_financeiroSALDOFINALCALCULADO2.AsFloat:=  cdsmovimento_financeiroMENORVLABERTURA.AsFloat+
                                                           cdsmovimento_financeiroVLRECEBIVEISREALIZADO.AsFloat+
                                                           cdsmovimento_financeiroVLOUTRASENTRADASREALIZADA.AsFloat+
                                                           cdsmovimento_financeiroVLCAPTACAOFOMENTOREALIZADO.AsFloat-
                                                           cdsmovimento_financeiroVLOUTRASSAIDASREALIZADO.AsFloat;

 end;


end;

procedure Tfmovimento_financeiro.cdsmovimento_financeiroNewRecord(
  DataSet: TDataSet);
begin
   cdsmovimento_financeiroVLRECEBIVEISPREV.AsFloat           :=0;
   cdsmovimento_financeiroVLRECEBIVEISREALIZADO.AsFloat      :=0;
   cdsmovimento_financeiroVLOUTRASENTRADAS_PREV.AsFloat      :=0;
   cdsmovimento_financeiroVLOUTRASENTRADASREALIZADA.AsFloat  :=0;
   cdsmovimento_financeiroVLCAPTACAOFOMENTOPREV.AsFloat      :=0;
   cdsmovimento_financeiroVLCAPTACAOFOMENTOREALIZADO.AsFloat :=0;
   cdsmovimento_financeiroVLOUTRASAIDASPREV.AsFloat          :=0;
   cdsmovimento_financeiroVLOUTRASSAIDASREALIZADO.AsFloat    :=0;
end;

procedure Tfmovimento_financeiro.spdAdicionaroutrasentradasClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=taboutrasentradas.TabIndex;
	if (fncoutrasentradas.adicionar(cdsoutrasentradas)=true) then
		begin
			fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,true);
			dbeoutrasentradasdtcompetencia.SetFocus;
	end
	else
		fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,false);
end;

procedure Tfmovimento_financeiro.spdAlteraroutrasentradasClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=taboutrasentradas.TabIndex;
	if (fncoutrasentradas.alterar(cdsoutrasentradas)=true) then
		begin
			fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,true);
			dbeoutrasentradasdtcompetencia.SetFocus;
	end
	else
		fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,false);
end;

procedure Tfmovimento_financeiro.spdGravaroutrasentradasClick(Sender: TObject);
begin
	if (fncoutrasentradas.gravar(cdsoutrasentradas)=true) then
		fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,false)
	else
		fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,true);
end;

procedure Tfmovimento_financeiro.spdCancelaroutrasentradasClick(Sender: TObject);
begin
	if (fncoutrasentradas.cancelar(cdsoutrasentradas)=true) then
		fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,false)
	else
		fncoutrasentradas.ativarModoEdicao(objetosModoEdicaoAtivosoutrasentradas,objetosModoEdicaoInativosoutrasentradas,true);
end;

procedure Tfmovimento_financeiro.spdExcluiroutrasentradasClick(Sender: TObject);
begin
	fncoutrasentradas.excluir(cdsoutrasentradas);
end;

procedure Tfmovimento_financeiro.cdsoutrasentradasAfterDelete(
  DataSet: TDataSet);
begin
    cdsmovimento_financeiro.Edit;
    cdsmovimento_financeiroVLOUTRASENTRADAS_PREV.AsFloat      :=StrToFloatDef(cdsoutrasentradasTOTALVLENTRADA.value,0);
    cdsmovimento_financeiroVLOUTRASENTRADASREALIZADA.AsFloat  :=strtofloatDef(cdsoutrasentradasTOTALVLENTRADAREALIZADO.value,0);
    cdsmovimento_financeiro.ApplyUpdates(0);
end;

procedure Tfmovimento_financeiro.cdsoutrasentradasAfterPost(DataSet: TDataSet);
begin
  somar_outrasentradas;
end;

procedure Tfmovimento_financeiro.cdsoutrasentradasBeforePost(DataSet: TDataSet);
begin
	if cdsoutrasentradas.State in [dsinsert] then
	cdsoutrasentradasID.Value:=fncoutrasentradas.autoNumeracaoGenerator(dm.conexao,'G_outrasentradas');
end;



procedure Tfmovimento_financeiro.cdsoutrasentradasNewRecord(DataSet: TDataSet);
begin
    cdsoutrasentradasDTCOMPETENCIA.AsDateTime   := cdsmovimento_financeiroDT_COMPETENCIA.AsDateTime;
    cdsoutrasentradasVLENTRADA.AsFloat          :=0;
    cdsoutrasentradasVLENTRADAREALIZADO.AsFloat :=0;
end;

procedure Tfmovimento_financeiro.dbecpgVL_BRUTOExit(Sender: TObject);
begin
  calcular_valorpagar;
end;

procedure Tfmovimento_financeiro.dbecpgVL_DESCONTOExit(Sender: TObject);
begin
 calcular_valorpagar;
end;

procedure Tfmovimento_financeiro.dbecpgVL_JUROSExit(Sender: TObject);
begin
 calcular_valorpagar;
end;

procedure Tfmovimento_financeiro.dbecpgVL_MULTAExit(Sender: TObject);
begin
 calcular_valorpagar;
end;

procedure Tfmovimento_financeiro.dbegodestinooperacaoVLBRUTOExit(
  Sender: TObject);
begin
  calcularvlliquidodestinooperacao;
end;

procedure Tfmovimento_financeiro.dbegodestinooperacaoVLDESCONTOExit(
  Sender: TObject);
begin
 calcularvlliquidodestinooperacao;
end;

procedure Tfmovimento_financeiro.dbegodestinooperacaoVLJUROExit(
  Sender: TObject);
begin
 calcularvlliquidodestinooperacao;
end;

procedure Tfmovimento_financeiro.dbegodestinooperacaoVLMULTAExit(
  Sender: TObject);
begin
 calcularvlliquidodestinooperacao;
end;

procedure Tfmovimento_financeiro.spbatualizarhistoricoClick(Sender: TObject);
var
Trans : TTransactionDesc;
begin
      try
         if (fdtcompetencia = nil) then Application.CreateForm(Tfdtcompetencia, fdtcompetencia);
         fdtcompetencia.ShowModal;
         if fdtcompetencia.ModalResult=mrOk then
         begin
          Application.ProcessMessages;
          Trans.TransactionID  := 1;
          Trans.IsolationLevel := xilREADCOMMITTED;
          PKG_ATUALIZAR_FINANCEIROI.sqlConnection.StartTransaction(Trans);
          PKG_ATUALIZAR_FINANCEIROI.Close;
          PKG_ATUALIZAR_FINANCEIROI.ParamByName('DT_COMPETENCIA').AsDate := fdtcompetencia.dtcompetencia.Date;
          PKG_ATUALIZAR_FINANCEIROI.ParamByName('CODFILIAL').asInteger  := cdsmovimento_financeiroCODFILIAL.AsInteger;
          PKG_ATUALIZAR_FINANCEIROI.ParamByName('codprojeto').AsDate    := cdsmovimento_financeiroCODPROJETO.AsInteger;
          PKG_ATUALIZAR_FINANCEIROI.ExecProc;
          PKG_ATUALIZAR_FINANCEIROI.SQLConnection.Commit(Trans);

          application.MessageBox('Applica��o realizada com sucesso.','Atualizar hist�rico financeiro', MB_OK+64);
         end;
       except

       on Exc:Exception do
        begin
          PKG_ATUALIZAR_FINANCEIROI.sqlConnection.Rollback(Trans);
        end;

       end;
end;

procedure Tfmovimento_financeiro.spdAdicionarcaptacaofomentoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcaptacaofomento.TabIndex;
	if (fnccaptacaofomento.adicionar(cdscaptacaofomento)=true) then
		begin
			fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,true);
			dbecaptacaofomentodtcompetencia.SetFocus;
	end
	else
		fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,false);
end;

procedure Tfmovimento_financeiro.spdAdicionarcpgClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcpg.TabIndex;

	if (fnccpg.adicionar(cdscpg)=true) then
		begin
			fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
			dbecpgID_FORNECEDOR.SetFocus;
	end
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false);

end;

procedure Tfmovimento_financeiro.spdAdicionargodestinooperacaoClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgodestinooperacao.TabIndex;
	if (fncgodestinooperacao.adicionar(cdsgodestinooperacao)=true) then
		begin
			fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,true);
			dbegodestinooperacaoCODFUNDO.SetFocus;
	end
	else
		fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,false);

end;

procedure Tfmovimento_financeiro.spdAlterarcaptacaofomentoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcaptacaofomento.TabIndex;
	if (fnccaptacaofomento.alterar(cdscaptacaofomento)=true) then
		begin
			fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,true);
			dbecaptacaofomentodtcompetencia.SetFocus;
	end
	else
		fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,false);
end;

procedure Tfmovimento_financeiro.spdAlterarcpgClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcpg.TabIndex;
	if (fnccpg.alterar(cdscpg)=true) then
		begin
			fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
			dbecpgID_FORNECEDOR.SetFocus;
	end
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false);
end;

procedure Tfmovimento_financeiro.spdAlterargodestinooperacaoClick(
  Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgodestinooperacao.TabIndex;
	if (fncgodestinooperacao.alterar(cdsgodestinooperacao)=true) then
		begin
			fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,true);
			dbegodestinooperacaoCODFUNDO.SetFocus;
	end
	else
		fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,false);
end;

procedure Tfmovimento_financeiro.spdGravarcaptacaofomentoClick(Sender: TObject);
begin
	if (fnccaptacaofomento.gravar(cdscaptacaofomento)=true) then
		fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,false)
	else
		fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,true);
end;

procedure Tfmovimento_financeiro.spdGravarcpgClick(Sender: TObject);
begin
	fnccpg.verificarCamposObrigatorios(cdscpg, camposObrigatorioscpg, pgcPrincipal);

	if (fnccpg.gravar(cdscpg)=true) then
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false)
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
end;

procedure Tfmovimento_financeiro.spdGravargodestinooperacaoClick(
  Sender: TObject);
begin
	if (fncgodestinooperacao.gravar(cdsgodestinooperacao)=true) then
		fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,false)
	else
		fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,true);
end;

procedure Tfmovimento_financeiro.spdCancelarcaptacaofomentoClick(Sender: TObject);
begin
	if (fnccaptacaofomento.cancelar(cdscaptacaofomento)=true) then
		fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,false)
	else
		fnccaptacaofomento.ativarModoEdicao(objetosModoEdicaoAtivoscaptacaofomento,objetosModoEdicaoInativoscaptacaofomento,true);
end;

procedure Tfmovimento_financeiro.spdCancelarcpgClick(Sender: TObject);
begin
	if (fnccpg.cancelar(cdscpg)=true) then
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false)
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
end;

procedure Tfmovimento_financeiro.spdCancelargodestinooperacaoClick(
  Sender: TObject);
begin
	if (fncgodestinooperacao.cancelar(cdsgodestinooperacao)=true) then
		fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,false)
	else
		fncgodestinooperacao.ativarModoEdicao(objetosModoEdicaoAtivosgodestinooperacao,objetosModoEdicaoInativosgodestinooperacao,true);
end;

procedure Tfmovimento_financeiro.spdExcluircaptacaofomentoClick(Sender: TObject);
begin
	fnccaptacaofomento.excluir(cdscaptacaofomento);
end;

procedure Tfmovimento_financeiro.spdExcluircpgClick(Sender: TObject);
begin
	fnccpg.excluir(cdscpg);
end;

procedure Tfmovimento_financeiro.spdExcluirgodestinooperacaoClick(
  Sender: TObject);
begin
	fncgodestinooperacao.excluir(cdsgodestinooperacao);
end;

procedure Tfmovimento_financeiro.sessao;
var host: string; arquivo: tinifile;
begin
   try
   //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);


    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfmovimento_financeiro.btnBaixaClick(Sender: TObject);
var
  i: Integer;
begin
  cdscpg.Filtered := False;
  cdscpg.Filter := 'SEL = ' + 'True';
  cdscpg.Filtered := True;

  cdscpg.First;
  while not cdscpg.EOF do
  begin
    if cdscpgSEL.Value then
    begin
      if cdscpgVL_PAGO.AsCurrency > 0 then
      begin
        application.MessageBox(pchar('Registro j� baixado! Documento: '+cdscpgDOCUMENTO.AsString),'Baixa de registro.',MB_ICONEXCLAMATION);
        cdscpg.Next;
        Continue;
      end;

      if not(cdscpg.State in [dsEdit]) then
        cdscpg.Edit;

      cdscpgVL_PAGO.AsCurrency := (cdscpgVL_BRUTO.AsCurrency + cdscpgVL_JUROS.AsCurrency +cdscpgVL_MULTA.AsCurrency) - cdscpgVL_DESCONTO.AsCurrency;

      cdscpgDT_PAGAMENTO.AsDateTime := Trunc(Date);

      cdscpg.ApplyUpdates(0);;
    end;
    cdscpg.Next;
  end;

  cdscpg.Filter := '';
  cdscpg.Filtered := False;
end;

procedure Tfmovimento_financeiro.calcular_valorpagar;
begin
  cdscpgVLPAGAR.AsFloat := (cdscpgVL_BRUTO.AsFloat + cdscpgVL_MULTA.AsFloat + cdscpgVL_JUROS.AsFloat - cdscpgVL_DESCONTO.AsFloat);
end;

procedure Tfmovimento_financeiro.calcularvlliquidodestinooperacao;
begin
  if cdsgodestinooperacao.State in [dsInsert, DsEdit] then
  cdsgodestinooperacaoVLLIQUIDO.AsFloat := (cdsgodestinooperacaoVLBRUTO.AsFloat - cdsgodestinooperacaoVLJUROS.AsFloat - cdsgodestinooperacaoVLMULTA.AsFloat +cdsgodestinooperacaoVLDESCONTO.AsFloat);
end;

procedure Tfmovimento_financeiro.somar_fomento;
begin
  if not cdscaptacaofomento.IsEmpty then
  begin
    cdsmovimento_financeiro.Edit;
    cdsmovimento_financeiroVLCAPTACAOFOMENTOPREV.AsFloat := strtofloatDef(cdscaptacaofomentoTOTALVLPREVISTO.Value, 0);
    cdsmovimento_financeiroVLCAPTACAOFOMENTOREALIZADO.AsFloat := strtofloatDef(cdscaptacaofomentoTOTALVLREALIZADO.value, 0);
    cdsmovimento_financeiro.ApplyUpdates(0);
  end;
end;

procedure Tfmovimento_financeiro.somar_outrasentradas;
begin
 if not cdsoutrasentradas.IsEmpty then
 begin
  cdsmovimento_financeiro.Edit;
  cdsmovimento_financeiroVLOUTRASENTRADAS_PREV.AsFloat := StrToFloatDef(cdsoutrasentradasTOTALVLENTRADA.value, 0);
  cdsmovimento_financeiroVLOUTRASENTRADASREALIZADA.AsFloat := strtofloatDef(cdsoutrasentradasTOTALVLENTRADAREALIZADO.value, 0);
  cdsmovimento_financeiro.ApplyUpdates(0);
 end;
end;

procedure Tfmovimento_financeiro.somar_cpg;
begin
  if not cdscpg.IsEmpty then
  begin
    cdsmovimento_financeiro.Edit;
    cdsmovimento_financeiroVLOUTRASAIDASPREV.AsFloat := strtofloatDef(cdscpgTOTALVLPAGAR.Value, 0);
    cdsmovimento_financeiroVLOUTRASSAIDASREALIZADO.AsFloat := strtofloatDef(cdscpgTOTALVLPAGO.value, 0);
    cdsmovimento_financeiro.ApplyUpdates(0);
  end;
end;

procedure Tfmovimento_financeiro.somar_operacaodestino;
begin
  if not cdsgodestinooperacao.IsEmpty then
  begin
    cdsmovimento_financeiro.Edit;
    cdsmovimento_financeiroVLRECEBIVEISPREV.AsFloat      := strtofloatDef(cdsgodestinooperacaoTOTALIQUIDO.Value, 0);
    cdsmovimento_financeiroVLRECEBIVEISREALIZADO.AsFloat := strtofloatDef(cdsgodestinooperacaoTOTALIQUIDO.value, 0);
    cdsmovimento_financeiro.ApplyUpdates(0);
  end
  else
  begin
    cdsmovimento_financeiro.Edit;
    cdsmovimento_financeiroVLRECEBIVEISPREV.AsFloat       := 0;
    cdsmovimento_financeiroVLRECEBIVEISREALIZADO.AsFloat  := 0;
    cdsmovimento_financeiro.ApplyUpdates(0);

  end;
end;


procedure Tfmovimento_financeiro.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fmovimento_financeiro.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfmovimento_financeiro.cdsmovimento_financeiroAfterOpen(DataSet: TDataSet);
begin
	cdsoutrasentradas.Open;
	cdscaptacaofomento.Open;
  cdscpg.Open;
  cdsgodestinooperacao.Open;

end;

procedure Tfmovimento_financeiro.cdscaptacaofomentoAfterPost(DataSet: TDataSet);
begin
  somar_fomento;
end;

procedure Tfmovimento_financeiro.cdscaptacaofomentoBeforePost(
  DataSet: TDataSet);
begin
	if cdscaptacaofomento.State in [dsinsert] then
	 cdscaptacaofomentoID.Value:=fnccaptacaofomento.autoNumeracaoGenerator(dm.conexao,'g_captacaofomento');

end;

procedure Tfmovimento_financeiro.cdscaptacaofomentoNewRecord(DataSet: TDataSet);
begin
  if not cdscaptacaofomento.IsEmpty then
  begin
    cdscaptacaofomentoDTCOMPETENCIA.AsDateTime := cdsmovimento_financeiroDT_COMPETENCIA.AsDateTime;
    cdscaptacaofomentoVLPREVISTO.AsFloat  :=0;
    cdscaptacaofomentoVLREALIZADO.AsFloat :=0;
  end;

end;

procedure Tfmovimento_financeiro.cdscpgAfterDelete(DataSet: TDataSet);
begin
   if not cdscpg.IsEmpty then
   begin
    cdsmovimento_financeiro.Edit;
    cdsmovimento_financeiroVLOUTRASAIDASPREV.AsFloat       :=strtofloatDef(cdscpgTOTALVLPAGAR.Value,0);
    cdsmovimento_financeiroVLOUTRASSAIDASREALIZADO.AsFloat :=strtofloatDef(cdscpgTOTALVLPAGO.value,0);
    cdsmovimento_financeiro.ApplyUpdates(0);
   end;
end;

procedure Tfmovimento_financeiro.cdscpgAfterPost(DataSet: TDataSet);
begin
  somar_cpg;
end;

procedure Tfmovimento_financeiro.cdscpgBeforePost(DataSet: TDataSet);
begin
	if cdscpg.State in [dsinsert] then
	cdscpgID.Value:=fnccpg.autoNumeracaoGenerator(dm.conexao,'G_cpg');
end;

procedure Tfmovimento_financeiro.cdscpgCalcFields(DataSet: TDataSet);
begin
  calcular_valorpagar;
end;

procedure Tfmovimento_financeiro.cdscpgNewRecord(DataSet: TDataSet);
begin
   cdscpgCODPROJETO.AsInteger  := cdsmovimento_financeiroCODPROJETO.AsInteger;
   cdscpgPROJETO.AsString      := cdsmovimento_financeiroPROJETO.AsString;
end;

procedure Tfmovimento_financeiro.cdsgodestinooperacaoAfterPost(
  DataSet: TDataSet);
begin
   somar_operacaodestino;
end;

procedure Tfmovimento_financeiro.cdsgodestinooperacaoBeforePost(
  DataSet: TDataSet);
begin
	if cdsgodestinooperacao.State in [dsinsert] then
	 cdsgodestinooperacaoID.Value:=fncgodestinooperacao.autoNumeracaoGenerator(dm.conexao,'G_godestinooperacao');
end;

procedure Tfmovimento_financeiro.cdsmovimento_financeiroAfterClose(DataSet: TDataSet);
begin
	cdsoutrasentradas.Close;
	cdscaptacaofomento.Close;
  cdscpg.Close;
  cdsgodestinooperacao.Close;

end;

procedure Tfmovimento_financeiro.spdImportarClick(Sender: TObject);
begin

    if not cdsmovimento_financeiro.IsEmpty then
    begin
     if (fimportacao = nil) then Application.CreateForm(Tfimportacao, fimportacao);
     fimportacao.ShowModal;
     cdscpg.Close;
     cdscpg.Open;
     if cdscpg.IsEmpty then
     begin
      cdscpg.Edit;
      cdscpgDT_CADASTRO.AsDateTime:=cdscpgDT_CADASTRO.AsDateTime;
      cdscpg.Post;
      cdscpg.ApplyUpdates(0);
     end;

     cdscpg.Refresh;
    end;
end;

procedure Tfmovimento_financeiro.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfmovimento_financeiro.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

function Tfmovimento_financeiro.stringtoStream(const Astring: string): Tstream;
begin
 result := TStringStream.Create(Astring);
end;

function Tfmovimento_financeiro.TrataAcentos(Dados: String): String;
var
    Retorno : String;
    i, y : Integer;
  const
    Codigos: array[1..29] of String = (#160,#133,#198,#131,#181,#183,#199,
    #182,#130,#136,#144,#210,#161,#214,#162,#111,#228,#229,#224,#174,#163,
    #129,#233,#154,#135,#128,#167,#166,#248);
begin
  for i := 1 to Length(Dados) do
    begin
      y := Pos(Dados[i], '�������������������������Ǻ��');
      if y > 0 then
        Retorno := Retorno + Codigos[y]
      else
        Retorno := Retorno + Dados[i];
    end;
    Result := Retorno;

end;

procedure Tfmovimento_financeiro.menuImpRelSinteticoClick(Sender: TObject);
 var
 relatorio: TStringList;
 stream : TStream;

begin
 if not cdsmovimento_financeiro.IsEmpty then
  begin
    try
     { relatorio :=TStringList.Create;
      stream    :=TStream.Create;
      dm.FDRelatorios.Filter   :='DESCRICAOI='+ QuotedStr('FINANCEIROI');
      dm.FDRelatorios.Filtered := true;

      relatorio.Add(TrataAcentos(dm.FDRelatoriosARQUIVO.AsString));
      dm.FDRelatorios.Open();
      if FileExists('C:\goassessoria\spool\'+dm.FDRelatoriosDESCRICAOI.AsString+'.fr3') then
      begin
       deletefile('C:\goassessoria\spool\'+dm.FDRelatoriosDESCRICAOI.AsString+'.fr3');
      end;
      stream  := stringtoStream(dm.FDRelatoriosARQUIVO.AsString);
      //fncmovimento_financeiro.salvarTextoAnsi(relatorio,'C:\goassessoria\spool\'+dm.FDRelatoriosDESCRICAOI.AsString+'.fr3');
    //  frxmovimentofinanceiroV.LoadFromFile('C:\goassessoria\spool\'+dm.FDRelatoriosDESCRICAOI.AsString+'.fr3', false);
      frxmovimentofinanceiroV.LoadFromStream(stream); }
      fncmovimento_financeiro.visualizarRelatorios(frxmovimentofinanceiroI);

    finally
     // freeandnil(relatorio);
    end;

  end;
end;

procedure Tfmovimento_financeiro.RelatrioII1Click(Sender: TObject);
begin
if not cdsmovimento_financeiro.IsEmpty then
	fncmovimento_financeiro.visualizarRelatorios(frxmovimentofinanceiroII);
end;

procedure Tfmovimento_financeiro.RelatrioIII1Click(Sender: TObject);
begin
{if not cdsmovimento_financeiro.IsEmpty then
	fncmovimento_financeiro.visualizarRelatorios(frxmovimentofinanceiroIII);  }
end;

procedure Tfmovimento_financeiro.RelatrioIV1Click(Sender: TObject);
begin
{if not cdsmovimento_financeiro.IsEmpty then
	fncmovimento_financeiro.visualizarRelatorios(frxmovimentofinanceiroIV); }
end;

end.

