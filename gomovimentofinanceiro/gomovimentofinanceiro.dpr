program gomovimentofinanceiro;

uses
  Vcl.Forms,
  umovimento_financeiro in 'umovimento_financeiro.pas' {fmovimento_financeiro},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  uimportacao in 'uimportacao.pas' {fimportacao},
  Vcl.Themes,
  Vcl.Styles,
  uEnvioMail in '..\class_shared\uEnvioMail.pas' {FenvioMail},
  udtcompetencia in 'udtcompetencia.pas' {fdtcompetencia},
  uprocessoatualizacao in '..\class_shared\uprocessoatualizacao.pas' {fprocessoatualizacao};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfmovimento_financeiro, fmovimento_financeiro);
  Application.CreateForm(Tfdtcompetencia, fdtcompetencia);
  Application.Run;
end.
