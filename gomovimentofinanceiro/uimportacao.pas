unit uimportacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
   Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,System.Win.ComObj,
    Datasnap.DBClient, Vcl.Controls, Data.DB,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxCurrencyEdit, dxGDIPlusClasses, Vcl.Buttons, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxSplitter, uFuncoes, udm, Vcl.DialogMessage,
  Vcl.Menus, Data.FMTBcd, Data.SqlExpr, umovimento_financeiro;

type
    TNumberType = (ntStr, ntInteger, ntFloat);
    Tfimportacao = class(TForm)
    pnlLinhaBotoesrua: TPanel;
    spdimportarexcel: TButton;
    cdscpg: TClientDataSet;
    cdscpgCODPROJETO: TIntegerField;
    cdscpgPROJETO: TStringField;
    cdscpgID_FORNECEDOR: TIntegerField;
    cdscpgFORNECEDOR: TStringField;
    cdscpgDESCRICAO: TStringField;
    cdscpgDOCUMENTO: TStringField;
    cdscpgVL_BRUTO: TFloatField;
    cdscpgVL_MULTA: TFloatField;
    cdscpgVL_JUROS: TFloatField;
    cdscpgVL_DESCONTO: TFloatField;
    cdscpgVL_PAGO: TFloatField;
    cdscpgDT_VENCIMENTO: TDateField;
    cdscpgDT_PAGAMENTO: TDateField;
    cdscpgFORMA_PGTO: TStringField;
    cdscpgID_CONTA_BANCO: TIntegerField;
    cdscpgBANCO_CONTA: TStringField;
    dscpg: TDataSource;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    pnlcontroles: TPanel;
    sbdgravar: TButton;
    sbpinserir: TButton;
    sbpexcluir: TButton;
    pnlgridplanilha: TPanel;
    pnlgridcpg: TPanel;
    cxSplitter: TcxSplitter;
    pnltitulodadosplanilha: TPanel;
    pnltitulodadoscpg: TPanel;
    GridResultadoPesquisa: TcxGrid;
    GridResultadoPesquisaDBTV: TcxGridDBTableView;
    GRIDResultadoPesquisaDBTVCODPROJETO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVPROJETO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVID_FORNECEDOR: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVFORNECEDOR: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVID_BANCO_CONTA: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVBANCO_CONTA: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDESCRICAO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDOCUMENTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_BRUTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_MULTA: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_JUROS: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDT_VENCIMENTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVFORMA_PAGAMENTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_PAGO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDT_PAGAMENTO: TcxGridDBColumn;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    Popupcpg: TPopupMenu;
    StringGrid: TStringGrid;
    btndeletar: TButton;
    procedure spdimportarexcelClick(Sender: TObject);
    procedure sbpinserirClick(Sender: TObject);
    procedure sbpexcluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbdgravarClick(Sender: TObject);
    procedure btndeletarClick(Sender: TObject);
  private
    fncimportacao : TFuncoes;
    procedure exportardadosPlanilha;
    procedure gravar_dados;
    function  pesquisarfornecedor(fornecedor : string): string;
    function  pesquisarcontabanco(conta_banco : string;codprojeto: integer): string;
    { Private declarations }
  public
    { Public declarations }
    opnPlanilha : TSaveDialog;
    function XlsToStringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;

  end;

var
  fimportacao: Tfimportacao;

implementation

{$R *.dfm}

{ Tfprincipal }
procedure Tfimportacao.sbdgravarClick(Sender: TObject);
begin

 if Application.MessageBox('Deseja realmente consolidar os lan�amentos?','Gravar Lan�amentos',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;

 TDialogMessage.ShowWaitMessage('Gravando Lan�amentos...',
  procedure
  begin
       gravar_dados;
  end);


end;

procedure Tfimportacao.sbpexcluirClick(Sender: TObject);
begin


 if not cdscpg.IsEmpty then
 begin
  if Application.MessageBox('Deseja realmente deletar os lan�amentos da gride?','Exportados.',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;
  cdscpg.EmptyDataSet;
 end;

end;

procedure Tfimportacao.sbpinserirClick(Sender: TObject);
begin

 if Application.MessageBox('Deseja realmente importar a Planilha?','Importa��o',MB_YESNO+MB_ICONQUESTION)=IDNO then
 abort;
 TDialogMessage.ShowWaitMessage('Exportando dados planilha...',
  procedure
  begin
     exportardadosPlanilha;
  end);
end;

procedure Tfimportacao.spdimportarexcelClick(Sender: TObject);
begin
  TDialogMessage.ShowWaitMessage('Importando planilha...',
  procedure
  begin
   fncimportacao.importartoStringgrid(StringGrid);
  end);

end;

function Tfimportacao.XlsToStringGrid(AGrid: TStringGrid;
  AXLSFile: string): Boolean;
  const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;

begin
   Result := False;
  //Cria Excel- OLE Object
  XLApp := CreateOleObject('Excel.Application');
  try
    //Esconde Excel
    XLApp.Visible := False;
    //Abre o Workbook
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    //Pegar o n�mero da �ltima linha
    x := XLApp.ActiveCell.Row;
    //Pegar o n�mero da �ltima coluna
    y := XLApp.ActiveCell.Column;
    //Seta Stringgrid linha e coluna
    AGrid.RowCount := x;
    AGrid.ColCount := y;
    //Associaca a variant WorkSheet com a variant do Delphi
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    //Cria o loop para listar os registros no TStringGrid
    k := 1;
    repeat
      for r := 1 to y do
      begin

        AGrid.Cells[(r - 1), (k - 1)] := RangeMatrix[K, R];

      end;
      Inc(k, 1);
    until k > x;
    RangeMatrix := Unassigned;
  finally
    //Fecha o Excel
    if not VarIsEmpty(XLApp) then
      begin
        XLApp.Quit;
        XLAPP := Unassigned;
        Sheet := Unassigned;
        Result := True;
      end;
  end;
end;

procedure Tfimportacao.gravar_dados;
begin
  try
    cdscpg.DisableControls;
    cdscpg.First;
    while not cdscpg.Eof do
    begin
      dm.FDQuery.Close;
      dm.FDQuery.SQL.Clear;
      dm.FDQuery.SQL.Add('select * from goplanocontas where descricao=:descricao and codprojeto=:codprojeto');
      dm.FDQuery.ParamByName('descricao').AsString  := cdscpgDESCRICAO.asstring;
      dm.FDQuery.ParamByName('codprojeto').AsInteger := cdscpgCODPROJETO.AsInteger;
      dm.FDQuery.open;

      if dm.FDQuery.IsEmpty then
      begin
       dm.FDQuery.Close;
       dm.FDQuery.SQL.Clear;
       dm.FDQuery.SQL.Add('insert into GOPLANOCONTAS (DESCRICAO, CODPROJETO, PROJETO) values (:DESCRICAO, :CODPROJETO, :PROJETO)');
       dm.FDQuery.ParamByName('descricao').AsString   := cdscpgDESCRICAO.asstring;
       dm.FDQuery.ParamByName('CODPROJETO').AsInteger := cdscpgCODPROJETO.AsInteger;
       dm.FDQuery.ParamByName('PROJETO').AsString     := cdscpgPROJETO.asstring;
       dm.FDQuery.ExecSQL;
      end;


      dm.FDQuery.Close;
      dm.FDQuery.SQL.Clear;
      dm.FDQuery.SQL.Add('INSERT INTO GOCPG ( ');
      dm.FDQuery.SQL.Add('   CODPROJETO       ');
      dm.FDQuery.SQL.Add('  ,PROJETO          ');
      dm.FDQuery.SQL.Add('  ,ID_FORNECEDOR    ');
      dm.FDQuery.SQL.Add('  ,FORNECEDOR       ');
      dm.FDQuery.SQL.Add('  ,DESCRICAO        ');
      dm.FDQuery.SQL.Add('  ,PLANOCONTA       ');
      dm.FDQuery.SQL.Add('  ,DOCUMENTO        ');
      dm.FDQuery.SQL.Add('  ,VL_BRUTO         ');
      dm.FDQuery.SQL.Add('  ,VL_MULTA         ');
      dm.FDQuery.SQL.Add('  ,VL_JUROS         ');
      dm.FDQuery.SQL.Add('  ,VL_DESCONTO      ');
      dm.FDQuery.SQL.Add('  ,DT_VENCIMENTO    ');
      dm.FDQuery.SQL.Add('  ,FORMA_PAGAMENTO  ');
      dm.FDQuery.SQL.Add('  ,VL_PAGO          ');
      dm.FDQuery.SQL.Add('   ,DT_PAGAMENTO    ');
      dm.FDQuery.SQL.Add('  ,ID_BANCO_CONTA   ');
      dm.FDQuery.SQL.Add('  ,BANCO_CONTA      ');
      dm.FDQuery.SQL.Add(' ,CODMOVFINANCEIRO  ');
      dm.FDQuery.SQL.Add(' ,CODFILIAL ');
      dm.FDQuery.SQL.Add(' ,FILIAL ');
      dm.FDQuery.SQL.Add('  )                 ');
      dm.FDQuery.SQL.Add('VALUES (            ');
      dm.FDQuery.SQL.Add('  :CODPROJETO       ');
      dm.FDQuery.SQL.Add('  ,:PROJETO         ');
      dm.FDQuery.SQL.Add('  ,:ID_FORNECEDOR   ');
      dm.FDQuery.SQL.Add('  ,:FORNECEDOR      ');
      dm.FDQuery.SQL.Add('  ,:DESCRICAO       ');
      dm.FDQuery.SQL.Add('  ,:PLANOCONTA       ');
      dm.FDQuery.SQL.Add('  ,:DOCUMENTO       ');
      dm.FDQuery.SQL.Add('  ,:VL_BRUTO        ');
      dm.FDQuery.SQL.Add('  ,:VL_MULTA        ');
      dm.FDQuery.SQL.Add('  ,:VL_JUROS        ');
      dm.FDQuery.SQL.Add('  ,:VL_DESCONTO     ');
      dm.FDQuery.SQL.Add('  ,:DT_VENCIMENTO   ');
      dm.FDQuery.SQL.Add('  ,:FORMA_PAGAMENTO ');
      dm.FDQuery.SQL.Add('  ,:VL_PAGO         ');
      dm.FDQuery.SQL.Add('  ,:DT_PAGAMENTO    ');
      dm.FDQuery.SQL.Add('  ,:ID_BANCO_CONTA  ');
      dm.FDQuery.SQL.Add('  ,:BANCO_CONTA     ');
      dm.FDQuery.SQL.Add(' ,:CODMOVFINANCEIRO ');
      dm.FDQuery.SQL.Add(' ,:CODFILIAL        ');
      dm.FDQuery.SQL.Add(' ,:FILIAL           ');
      dm.FDQuery.SQL.Add('  )                 ');
      dm.FDQuery.ParamByName('CODPROJETO').AsInteger          := cdscpgCODPROJETO.AsInteger;
      dm.FDQuery.ParamByName('PROJETO').AsString              := cdscpgPROJETO.AsString;
      dm.FDQuery.ParamByName('ID_FORNECEDOR').AsInteger       := cdscpgID_FORNECEDOR.AsInteger;
      dm.FDQuery.ParamByName('FORNECEDOR').asstring           := cdscpgFORNECEDOR.asstring;
      dm.FDQuery.ParamByName('DESCRICAO').asstring            := cdscpgDESCRICAO.asstring;
      dm.FDQuery.ParamByName('PLANOCONTA').asstring           := cdscpgDESCRICAO.asstring;
      dm.FDQuery.ParamByName('DOCUMENTO').asstring            := cdscpgDOCUMENTO.asstring;
      dm.FDQuery.ParamByName('VL_BRUTO').asfloat              := cdscpgVL_BRUTO.asfloat;
      dm.FDQuery.ParamByName('VL_MULTA').asfloat              := cdscpgVL_MULTA.asfloat;
      dm.FDQuery.ParamByName('VL_JUROS').asfloat              := cdscpgVL_JUROS.asfloat;
      dm.FDQuery.ParamByName('VL_DESCONTO').asfloat           := cdscpgVL_DESCONTO.asfloat;
      dm.FDQuery.ParamByName('DT_VENCIMENTO').AsDateTime      := cdscpgDT_VENCIMENTO.AsDateTime;
      dm.FDQuery.ParamByName('FORMA_PAGAMENTO').asstring      := cdscpgFORMA_PGTO.asstring;
      dm.FDQuery.ParamByName('VL_PAGO').asfloat               := cdscpgVL_PAGO.asfloat;
      dm.FDQuery.ParamByName('DT_PAGAMENTO').AsDateTime       := cdscpgDT_PAGAMENTO.AsDateTime;
      dm.FDQuery.ParamByName('ID_BANCO_CONTA').AsInteger      := cdscpgID_CONTA_BANCO.AsInteger;
      dm.FDQuery.ParamByName('BANCO_CONTA').asstring          := cdscpgBANCO_CONTA.AsString;
      dm.FDQuery.ParamByName('CODMOVFINANCEIRO').AsInteger    := fmovimento_financeiro.cdsmovimento_financeiroID.AsInteger;
      dm.FDQuery.ParamByName('CODFILIAL').AsInteger           := fmovimento_financeiro.cdsmovimento_financeiroCODFILIAL.AsInteger;
      dm.FDQuery.ParamByName('FILIAL').asString               := fmovimento_financeiro.cdsmovimento_financeiroFILIAL.asString;
      dm.FDQuery.ExecSQL;
      cdscpg.Next;
    end;
    application.MessageBox('Dados gravados com sucesso.','Grava��o.',MB_OK+64);

  finally
    cdscpg.EnableControls;
  end;
end;

function Tfimportacao.pesquisarcontabanco(conta_banco: string;
  codprojeto: integer): string;
  var sucesso : string;
begin
   try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select coalesce(ID,0)ID FROM GOCONTA_BANCO WHERE descricao=:descricao and codprojeto=:codprojeto');
    dm.FDQuery.ParamByName('descricao').asString :=conta_banco;
    dm.FDQuery.ParamByName('codprojeto').asInteger :=codprojeto;
    dm.FDQuery.Open();

    if dm.FDQuery.RecordCount=0 then
    begin
     sucesso :='0';
    end
    else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end
end;

function Tfimportacao.pesquisarfornecedor(fornecedor: string): string;
var sucesso : string;
begin
 try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select coalesce(ID,0)ID FROM gofornecedor WHERE NOME_FANTASIA=:NOME_FANTASIA');
    dm.FDQuery.ParamByName('NOME_FANTASIA').asString :=fornecedor;
    dm.FDQuery.Open();

    if dm.FDQuery.RecordCount=0 then
    begin
     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('insert into gofornecedor(nome_fantasia)values(:nome_fantasia)');
     dm.FDQuery.ParamByName('NOME_FANTASIA').asString :=fornecedor;
     dm.FDQuery.ExecSQL;
     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('SELECT MAX(ID)ID FROM gofornecedor');
     dm.FDQuery.Open();
     sucesso := dm.FDQuery.FieldByName('ID').AsString;
    end
    else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end

end;

procedure Tfimportacao.btndeletarClick(Sender: TObject);
var
  i: Integer;
begin
 if Application.MessageBox('Deseja realmente excluir??','Exclus�o',MB_YESNO+MB_ICONQUESTION)=IDYES then
 begin
   with StringGrid do
   begin
        for I := 0 to ColCount do
        cols[i].Clear;
   end;
 end;
end;

procedure Tfimportacao.exportardadosPlanilha;
var
  i: Integer;
begin
  try
    cdscpg.EmptyDataSet;
    cdscpg.DisableControls;
    for I := 0 to pred(StringGrid.RowCount) do
    begin
      if i = 0 then
        continue;
      if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;
      cdscpg.Append;
      cdscpgCODPROJETO.AsInteger      := strtoint(StringGrid.Cells[0, i]);
      cdscpgPROJETO.asstring          := StringGrid.Cells[1, i];
      cdscpgID_FORNECEDOR.AsInteger   := strtoInt(pesquisarfornecedor(stringGrid.Cells[3, i]));
      cdscpgFORNECEDOR.asstring       := StringGrid.Cells[3, i];
      cdscpgDESCRICAO.asstring        := StringGrid.Cells[4, i];
      cdscpgDOCUMENTO.AsString        := StringGrid.Cells[5, i];
      cdscpgVL_BRUTO.AsFloat          := StrToFloat(StringGrid.Cells[6, i]);
      cdscpgVL_MULTA.AsFloat          := StrToFloat(StringGrid.Cells[7, i]);
      cdscpgVL_JUROS.AsFloat          := StrToFloat(StringGrid.Cells[8, i]);
      cdscpgVL_DESCONTO.AsFloat       := StrToFloat(StringGrid.Cells[9, i]);
      cdscpgVL_PAGO.AsFloat           := StrToFloat(StringGrid.Cells[10, i]);
      cdscpgDT_VENCIMENTO.AsDateTime  := StrToDate(StringGrid.Cells[11, i]);
      cdscpgDT_PAGAMENTO.AsDateTime   := StrToDate(StringGrid.Cells[12, i]);
      cdscpgFORMA_PGTO.AsString       := StringGrid.Cells[13, i];
      cdscpgID_CONTA_BANCO.AsInteger  := strtoInt(pesquisarcontabanco(stringGrid.Cells[15, i],cdscpgCODPROJETO.AsInteger));
      cdscpgBANCO_CONTA.AsString      := StringGrid.Cells[15, i];
      cdscpg.Post;
    end;
  finally
    cdscpg.EnableControls;
  end;
end;

procedure Tfimportacao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncimportacao.verificarEmTransacao(cdscpg);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncimportacao.configurarGridesFormulario(fimportacao,true, false);

	fncimportacao.Free;
	{eliminando container de fun��es da memoria<<<}

	fimportacao:=nil;
	Action:=cafree;

end;

procedure Tfimportacao.FormCreate(Sender: TObject);
begin
   {>>>container de fun��es}
    fncimportacao:= TFuncoes.Create(Self);
    fncimportacao.definirConexao(dm.conexao);
    fncimportacao.definirFormulario(Self);
    fncimportacao.validarTransacaoRelacionamento:=true;
    fncimportacao.autoAplicarAtualizacoesBancoDados:=true;
    {container de fun��es<<<}


    fncimportacao.criaAtalhoPopupMenuNavegacao(Popupcpg,cdscpg);

    fncimportacao.definirMascaraCampos(cdscpg);

    fncimportacao.configurarGridesFormulario(fimportacao,false, true);

end;


end.



