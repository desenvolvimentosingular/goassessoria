unit ugofaturamento;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 01/11/2021*}                                          
{*Hora 19:57:51*}                                            
{*Unit gofaturamento *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, dxGDIPlusClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, uPesquisa, Vcl.DialogMessage,
  cxCurrencyEdit, cxCheckBox;

type                                                                                        
	Tfgofaturamento = class(TForm)
    sqlgofaturamento: TSQLDataSet;
	dspgofaturamento: TDataSetProvider;
	cdsgofaturamento: TClientDataSet;
	dsgofaturamento: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	Popupgofaturamento: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgofaturamento: tcxTabSheet;
	SCRgofaturamento: TScrollBox;
	pnlDadosgofaturamento: TPanel;
	{>>>bot�es de manipula��o de dados gofaturamento}
	pnlLinhaBotoesgofaturamento: TPanel;
	spdAdicionargofaturamento: TButton;
	spdAlterargofaturamento: TButton;
	spdGravargofaturamento: TButton;
	spdCancelargofaturamento: TButton;
	spdExcluirgofaturamento: TButton;

	{bot�es de manipula��o de dadosgofaturamento<<<}
	{campo ID}
	sqlgofaturamentoID: TIntegerField;
	cdsgofaturamentoID: TIntegerField;
	pnlLayoutLinhasgofaturamento1: TPanel;
	pnlLayoutTitulosgofaturamentoid: TPanel;
	pnlLayoutCamposgofaturamentoid: TPanel;
	dbegofaturamentoid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgofaturamentoDT_CADASTRO: TDateField;
	cdsgofaturamentoDT_CADASTRO: TDateField;
	pnlLayoutLinhasgofaturamento2: TPanel;
	pnlLayoutTitulosgofaturamentodt_cadastro: TPanel;
	pnlLayoutCamposgofaturamentodt_cadastro: TPanel;
	dbegofaturamentodt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgofaturamentoHS_CADASTRO: TTimeField;
	cdsgofaturamentoHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgofaturamento3: TPanel;
	pnlLayoutTitulosgofaturamentohs_cadastro: TPanel;
	pnlLayoutCamposgofaturamentohs_cadastro: TPanel;
	dbegofaturamentohs_cadastro: TDBEdit;

	{campo CODPROJETO}
	sqlgofaturamentoCODPROJETO: TIntegerField;
	cdsgofaturamentoCODPROJETO: TIntegerField;
	pnlLayoutLinhasgofaturamento4: TPanel;
	pnlLayoutTitulosgofaturamentocodprojeto: TPanel;
	pnlLayoutCamposgofaturamentocodprojeto: TPanel;
	dbegofaturamentocodprojeto: TDBEdit;

	{campo PROJETO}
	sqlgofaturamentoPROJETO: TStringField;
	cdsgofaturamentoPROJETO: TStringField;

	{campo CODFILIAL}
	sqlgofaturamentoCODFILIAL: TIntegerField;
	cdsgofaturamentoCODFILIAL: TIntegerField;
	pnlLayoutLinhasgofaturamento6: TPanel;
	pnlLayoutTitulosgofaturamentocodfilial: TPanel;
	pnlLayoutCamposgofaturamentocodfilial: TPanel;
	dbegofaturamentocodfilial: TDBEdit;

	{campo FILIAL}
	sqlgofaturamentoFILIAL: TStringField;
	cdsgofaturamentoFILIAL: TStringField;

	{campo CODCLIENTE}
	sqlgofaturamentoCODCLIENTE: TIntegerField;
	cdsgofaturamentoCODCLIENTE: TIntegerField;
	pnlLayoutLinhasgofaturamento8: TPanel;
	pnlLayoutTitulosgofaturamentocodcliente: TPanel;
	pnlLayoutCamposgofaturamentocodcliente: TPanel;
	dbegofaturamentocodcliente: TDBEdit;

	{campo CLIENTE}
	sqlgofaturamentoCLIENTE: TStringField;
	cdsgofaturamentoCLIENTE: TStringField;

	{campo PEDIDO}
	sqlgofaturamentoPEDIDO: TIntegerField;
	cdsgofaturamentoPEDIDO: TIntegerField;
	pnlLayoutLinhasgofaturamento10: TPanel;
	pnlLayoutTitulosgofaturamentopedido: TPanel;
	pnlLayoutCamposgofaturamentopedido: TPanel;
	dbegofaturamentopedido: TDBEdit;

	{campo NF}
	sqlgofaturamentoNF: TIntegerField;
	cdsgofaturamentoNF: TIntegerField;

	{campo USUARIO}
	sqlgofaturamentoUSUARIO: TStringField;
	cdsgofaturamentoUSUARIO: TStringField;
	pnlLayoutLinhasgofaturamento12: TPanel;
	pnlLayoutTitulosgofaturamentousuario: TPanel;
	pnlLayoutCamposgofaturamentousuario: TPanel;
	dbegofaturamentousuario: TDBEdit;

	{campo VENDEDOR}
	sqlgofaturamentoVENDEDOR: TStringField;
	cdsgofaturamentoVENDEDOR: TStringField;
	pnlLayoutLinhasgofaturamento13: TPanel;
	pnlLayoutTitulosgofaturamentovendedor: TPanel;
	pnlLayoutCamposgofaturamentovendedor: TPanel;
	dbegofaturamentovendedor: TDBEdit;

	{campo DT_LANCAMENTO}
	sqlgofaturamentoDT_LANCAMENTO: TDateField;
	cdsgofaturamentoDT_LANCAMENTO: TDateField;
	pnlLayoutLinhasgofaturamento14: TPanel;
	pnlLayoutTitulosgofaturamentodt_lancamento: TPanel;
	pnlLayoutCamposgofaturamentodt_lancamento: TPanel;

	{campo DT_ENTREGA_PREVISTA}
	sqlgofaturamentoDT_ENTREGA_PREVISTA: TDateField;
	cdsgofaturamentoDT_ENTREGA_PREVISTA: TDateField;

	{campo DT_ENTREGA}
	sqlgofaturamentoDT_ENTREGA: TDateField;
	cdsgofaturamentoDT_ENTREGA: TDateField;

	{campo STATUS}
	sqlgofaturamentoSTATUS: TStringField;
	cdsgofaturamentoSTATUS: TStringField;
	pnlLayoutLinhasgofaturamento17: TPanel;
	pnlLayoutTitulosgofaturamentostatus: TPanel;
	pnlLayoutCamposgofaturamentostatus: TPanel;
	cbogofaturamentostatus: TDBComboBox;

	{campo SITUACAO_NF}
	sqlgofaturamentoSITUACAO_NF: TStringField;
	cdsgofaturamentoSITUACAO_NF: TStringField;
	pnlLayoutLinhasgofaturamento19: TPanel;
	pnlLayoutTitulosgofaturamentocodproduto: TPanel;
	pnlLayoutCamposgofaturamentocodproduto: TPanel;
	dbegofaturamentocodproduto: TDBEdit;

	{campo PRODUTO}
	sqlgofaturamentoPRODUTO: TStringField;
	cdsgofaturamentoPRODUTO: TStringField;

	{campo GRUPO_PRODUTO}
	sqlgofaturamentoGRUPO_PRODUTO: TStringField;
	cdsgofaturamentoGRUPO_PRODUTO: TStringField;
	pnlLayoutLinhasgofaturamento21: TPanel;
	pnlLayoutTitulosgofaturamentogrupo_produto: TPanel;
	pnlLayoutCamposgofaturamentogrupo_produto: TPanel;
	dbegofaturamentogrupo_produto: TDBEdit;

	{campo ESTILO}
	sqlgofaturamentoESTILO: TStringField;
	cdsgofaturamentoESTILO: TStringField;
	pnlLayoutLinhasgofaturamento22: TPanel;
	pnlLayoutTitulosgofaturamentoestilo: TPanel;
	pnlLayoutCamposgofaturamentoestilo: TPanel;
	dbegofaturamentoestilo: TDBEdit;

	{campo MARCA}
	sqlgofaturamentoMARCA: TStringField;
	cdsgofaturamentoMARCA: TStringField;
	pnlLayoutLinhasgofaturamento23: TPanel;
	pnlLayoutTitulosgofaturamentomarca: TPanel;
	pnlLayoutCamposgofaturamentomarca: TPanel;
	dbegofaturamentomarca: TDBEdit;

	{campo QT}
	sqlgofaturamentoQT: TFloatField;
	cdsgofaturamentoQT: TFloatField;
	pnlLayoutLinhasgofaturamento24: TPanel;
	pnlLayoutTitulosgofaturamentoqt: TPanel;
	pnlLayoutCamposgofaturamentoqt: TPanel;
	dbegofaturamentoqt: TDBEdit;

	{campo UNIDADE}
	sqlgofaturamentoUNIDADE: TStringField;
	cdsgofaturamentoUNIDADE: TStringField;

	{campo UNIDADE_VENDA}
	sqlgofaturamentoUNIDADE_VENDA: TStringField;
	cdsgofaturamentoUNIDADE_VENDA: TStringField;

	{campo PRECO}
	sqlgofaturamentoPRECO: TFloatField;
	cdsgofaturamentoPRECO: TFloatField;
	pnlLayoutLinhasgofaturamento27: TPanel;
	pnlLayoutTitulosgofaturamentopreco: TPanel;
	pnlLayoutCamposgofaturamentopreco: TPanel;
	dbegofaturamentopreco: TDBEdit;

	{campo VLPEDIDOITEM}
	sqlgofaturamentoVLPEDIDOITEM: TFloatField;
	cdsgofaturamentoVLPEDIDOITEM: TFloatField;
	pnlLayoutLinhasgofaturamento28: TPanel;
	pnlLayoutTitulosgofaturamentovlpedidoitem: TPanel;
	pnlLayoutCamposgofaturamentovlpedidoitem: TPanel;
	dbegofaturamentovlpedidoitem: TDBEdit;

	{campo VLCOBRADOITEM}
	sqlgofaturamentoVLCOBRADOITEM: TFloatField;
	cdsgofaturamentoVLCOBRADOITEM: TFloatField;
	pnlLayoutLinhasgofaturamento29: TPanel;
	pnlLayoutTitulosgofaturamentovlcobradoitem: TPanel;
	pnlLayoutCamposgofaturamentovlcobradoitem: TPanel;
	dbegofaturamentovlcobradoitem: TDBEdit;

	{campo VLFRETE}
	sqlgofaturamentoVLFRETE: TFloatField;
	cdsgofaturamentoVLFRETE: TFloatField;
	pnlLayoutLinhasgofaturamento30: TPanel;
	pnlLayoutTitulosgofaturamentovlfrete: TPanel;
	pnlLayoutCamposgofaturamentovlfrete: TPanel;
	dbegofaturamentovlfrete: TDBEdit;

	{campo VLFATURADO}
	sqlgofaturamentoVLFATURADO: TFloatField;
	cdsgofaturamentoVLFATURADO: TFloatField;
	pnlLayoutLinhasgofaturamento31: TPanel;
	pnlLayoutTitulosgofaturamentovlfaturado: TPanel;
	pnlLayoutCamposgofaturamentovlfaturado: TPanel;
	dbegofaturamentovlfaturado: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    imglogo: TImage;
    pnlLayoutCamposgofaturamentoprojeto: TPanel;
    dbegofaturamentoPROJETO: TDBEdit;
    pnlLayoutCamposgofaturamentofilial: TPanel;
    dbegofaturamentoFILIAL: TDBEdit;
    pnlLayoutCamposgofaturamentocliente: TPanel;
    dbegofaturamentoCLIENTE: TDBEdit;
    pnlLayoutTitulosgofaturamentonf: TPanel;
    pnlLayoutCamposgofaturamentonf: TPanel;
    dbegofaturamentoNF: TDBEdit;
    pnlLayoutTitulosgofaturamentodt_entrega_prevista: TPanel;
    pnlLayoutCamposgofaturamentodt_entrega_prevista: TPanel;
    pnlLayoutTitulosgofaturamentodt_entrega: TPanel;
    pnlLayoutCamposgofaturamentodt_entrega: TPanel;
    pnlLayoutTitulosgofaturamentosituacao_nf: TPanel;
    pnlLayoutCamposgofaturamentosituacao_nf: TPanel;
    cbogofaturamentoSITUACAO_NF: TDBComboBox;
    pnlLayoutCamposgofaturamentoproduto: TPanel;
    dbegofaturamentoPRODUTO: TDBEdit;
    pnlLayoutCamposgofaturamentounidade: TPanel;
    cbogofaturamentoUNIDADE: TDBComboBox;
    pnlLayoutTitulosgofaturamentounidade: TPanel;
    pnlLayoutTitulosgofaturamentounidade_venda: TPanel;
    pnlLayoutCamposgofaturamentounidade_venda: TPanel;
    cbogofaturamentoUNIDADE_VENDA: TDBComboBox;
    dbegocontrolerecebiveldtcompetencia: TcxDBDateEdit;
    dbegofaturamentoDT_ENTREGA: TcxDBDateEdit;
    dbegofaturamentoDT_LANCAMENTO: TcxDBDateEdit;
    psqprojeto: TPesquisa;
    psqfilial: TPesquisa;
    btnprojeto: TButton;
    btnfilial: TButton;
    btncliente: TButton;
    psqcliente: TPesquisa;
    btnproduto: TButton;
    psqproduto: TPesquisa;
    cdsfiltros: TClientDataSet;
    sqlgofaturamentoCODPRODUTO: TStringField;
    cdsgofaturamentoCODPRODUTO: TStringField;
    sqlgofaturamentoPARAMETRO: TStringField;
    cdsgofaturamentoPARAMETRO: TStringField;
    btnpesquisargrupoproduto: TButton;
    psqgrupoproduto: TPesquisa;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    lblProjetoresultado: TLabel;
    lblusuarioresultado: TLabel;
    lblversao: TLabel;
    cdsgofaturamentoselecionado: TBooleanField;
    GridResultadoPesquisaDBTVselecionado: TcxGridDBColumn;
    spdmarcartodos: TButton;
    spddesmarcartodos: TButton;
    spdexcluirselecionados: TButton;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargofaturamentoClick(Sender: TObject);
	procedure spdAlterargofaturamentoClick(Sender: TObject);
	procedure spdGravargofaturamentoClick(Sender: TObject);
	procedure spdCancelargofaturamentoClick(Sender: TObject);
	procedure spdExcluirgofaturamentoClick(Sender: TObject);
	procedure cdsgofaturamentoBeforePost(DataSet: TDataSet);

    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure spdOpcoesClick(Sender: TObject);
    procedure spdmarcartodosClick(Sender: TObject);
    procedure spddesmarcartodosClick(Sender: TObject);
    procedure spdexcluirselecionadosClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgofaturamento: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgofaturamento: Array[0..5]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgofaturamento: Array[0..15]  of TControl;
	objetosModoEdicaoInativosgofaturamento: Array[0..15]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagofaturamento: Array[0..0]  of TDuplicidade;
	duplicidadeCampogofaturamento: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..30]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fgofaturamento: Tfgofaturamento;

implementation

uses udm, ufiltro_avancado,System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfgofaturamento.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgofaturamento.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgofaturamento.TabIndex;
end;

procedure Tfgofaturamento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgofaturamento.verificarEmTransacao(cdsgofaturamento);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgofaturamento.configurarGridesFormulario(fgofaturamento,true, false);

	fncgofaturamento.Free;
	{eliminando container de fun��es da memoria<<<}

	fgofaturamento:=nil;
	Action:=cafree;
end;

procedure Tfgofaturamento.FormCreate(Sender: TObject);
begin
  sessao;
 {>>>container de fun��es}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
	{>>>container de fun��es}
	fncgofaturamento:= TFuncoes.Create(Self);
	fncgofaturamento.definirConexao(dm.conexao);
	fncgofaturamento.definirConexaohistorico(dm.conexao);
	fncgofaturamento.definirFormulario(Self);
	fncgofaturamento.validarTransacaoRelacionamento:=true;
	fncgofaturamento.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODPROJETO';
	pesquisasItem.titulo:='C�d.Projeto';
	pesquisasItem.campo:='CODPROJETO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPROJETO';
	pesquisasItem.titulo:='Projeto';
	pesquisasItem.campo:='PROJETO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODFILIAL';
	pesquisasItem.titulo:='C�d.Filial';
	pesquisasItem.campo:='CODFILIAL';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesFILIAL';
	pesquisasItem.titulo:='Filial';
	pesquisasItem.campo:='FILIAL';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODCLIENTE';
	pesquisasItem.titulo:='Cod.Cliente';
	pesquisasItem.campo:='CODCLIENTE';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[6]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCLIENTE';
	pesquisasItem.titulo:='Cliente';
	pesquisasItem.campo:='CLIENTE';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[7]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPEDIDO';
	pesquisasItem.titulo:='N�.Pedido';
	pesquisasItem.campo:='PEDIDO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[8]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesNF';
	pesquisasItem.titulo:='Nota Fiscal';
	pesquisasItem.campo:='NF';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[9]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUSUARIO';
	pesquisasItem.titulo:='Usu�rio';
	pesquisasItem.campo:='USUARIO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[10]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesVENDEDOR';
	pesquisasItem.titulo:='Vendedor';
	pesquisasItem.campo:='VENDEDOR';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[11]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_LANCAMENTO';
	pesquisasItem.titulo:='Data Lan�amento';
	pesquisasItem.campo:='DT_LANCAMENTO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[12]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_ENTREGA_PREVISTA';
	pesquisasItem.titulo:='Data prevista entrega';
	pesquisasItem.campo:='DT_ENTREGA_PREVISTA';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[13]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_ENTREGA';
	pesquisasItem.titulo:='Data entrega';
	pesquisasItem.campo:='DT_ENTREGA';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[14]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesSTATUS';
	pesquisasItem.titulo:='Status';
	pesquisasItem.campo:='STATUS';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('A ENTREGAR');
	pesquisasItem.comboItens.add('ENTREGUE');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[15]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesSITUACAO_NF';
	pesquisasItem.titulo:='Situa��o NFe';
	pesquisasItem.campo:='SITUACAO_NF';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('AUTORIZADA');
	pesquisasItem.comboItens.add('NAO AUTORIZADA');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[16]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODPRODUTO';
	pesquisasItem.titulo:='C�d.Produto';
	pesquisasItem.campo:='CODPRODUTO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[17]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPRODUTO';
	pesquisasItem.titulo:='Produto';
	pesquisasItem.campo:='PRODUTO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[18]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesGRUPO_PRODUTO';
	pesquisasItem.titulo:='Grupo Produto';
	pesquisasItem.campo:='GRUPO_PRODUTO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[19]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesESTILO';
	pesquisasItem.titulo:='Estilo';
	pesquisasItem.campo:='ESTILO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[20]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesMARCA';
	pesquisasItem.titulo:='Marca';
	pesquisasItem.campo:='MARCA';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[21]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUNIDADE';
	pesquisasItem.titulo:='Unidade';
	pesquisasItem.campo:='UNIDADE';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('CX');
	pesquisasItem.comboItens.add('UN');
	pesquisasItem.comboItens.add('FD');
	pesquisasItem.comboItens.add('CX');
	pesquisasItem.comboItens.add('MIL');
	pesquisasItem.comboItens.add('PC');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[22]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUNIDADE_VENDA';
	pesquisasItem.titulo:='Unidade venda';
	pesquisasItem.campo:='UNIDADE_VENDA';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('CX');
	pesquisasItem.comboItens.add('UN');
	pesquisasItem.comboItens.add('FD');
	pesquisasItem.comboItens.add('MIL');
	pesquisasItem.comboItens.add('PC');
	pesquisasItem.comboItens.add('LT');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[23]:=pesquisasItem;

  {gerando campos no clientdatsaet de filtros}
  fncgofaturamento.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}


	fncgofaturamento.criarPesquisas(sqlgofaturamento,cdsgofaturamento,'select * from gofaturamento ', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gofaturamento}

  camposObrigatoriosgofaturamento[0] :=dbegofaturamentoCODPROJETO;
  camposObrigatoriosgofaturamento[1] :=dbegofaturamentoCODFILIAL;
  camposObrigatoriosgofaturamento[2] :=dbegofaturamentoDT_LANCAMENTO;
  camposObrigatoriosgofaturamento[3] :=dbegofaturamentoVLFATURADO;

	{campos obrigatorios gofaturamento<<<}

	{>>>objetos ativos no modo de manipula��o de dados gofaturamento}
	//objetosModoEdicaoAtivosgofaturamento[0]:=pnlDadosgofaturamento;
	objetosModoEdicaoAtivosgofaturamento[0]:=spdCancelargofaturamento;
	objetosModoEdicaoAtivosgofaturamento[1]:=spdGravargofaturamento;
  objetosModoEdicaoAtivosgofaturamento[2]:=dbegofaturamentoCODPROJETO;
  objetosModoEdicaoAtivosgofaturamento[3]:=dbegofaturamentoCODFILIAL;
  objetosModoEdicaoAtivosgofaturamento[4]:=dbegofaturamentoCODCLIENTE;
  objetosModoEdicaoAtivosgofaturamento[5]:=dbegofaturamentoCODPRODUTO;
  objetosModoEdicaoAtivosgofaturamento[6]:=btnprojeto;
  objetosModoEdicaoAtivosgofaturamento[7]:=btnfilial;
  objetosModoEdicaoAtivosgofaturamento[8]:=btncliente;
  objetosModoEdicaoAtivosgofaturamento[9]:=btnproduto;
  objetosModoEdicaoAtivosgofaturamento[10]:=btnpesquisargrupoproduto;

	{objetos ativos no modo de manipula��o de dados gofaturamento<<<}

	{>>>objetos inativos no modo de manipula��o de dados gofaturamento}
	objetosModoEdicaoInativosgofaturamento[0]:=spdAdicionargofaturamento;
	objetosModoEdicaoInativosgofaturamento[1]:=spdAlterargofaturamento;
	objetosModoEdicaoInativosgofaturamento[2]:=spdExcluirgofaturamento;
	objetosModoEdicaoInativosgofaturamento[3]:=dbegofaturamentoPROJETO;
	objetosModoEdicaoInativosgofaturamento[5]:=dbegofaturamentoFILIAL;
  objetosModoEdicaoInativosgofaturamento[6]:=dbegofaturamentoCLIENTE;
  objetosModoEdicaoInativosgofaturamento[7]:=dbegofaturamentoPRODUTO;
	{objetos inativos no modo de manipula��o de dados gofaturamento<<<}

	{>>>comando de adi��o de teclas de atalhos gofaturamento}


	fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'Adicionar novo registro',VK_F4,spdAdicionargofaturamento.OnClick);
	fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'Alterar registro selecionado',VK_F5,spdAlterargofaturamento.OnClick);
	fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'Gravar �ltimas altera��es',VK_F6,spdGravargofaturamento.OnClick);
	fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'Cancelar �ltimas altera��e',VK_F7,spdCancelargofaturamento.OnClick);
	fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgofaturamento.OnClick);
  fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'[Selecionar Todo(s)]', ShortCut(vk_f1, [ssCtrl]),spdmarcartodos.OnClick);
  fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'[Inverter Sele��o]', ShortCut(vk_f2, [ssCtrl]),spddesmarcartodos.OnClick);
  fncgofaturamento.criaAtalhoPopupMenu(Popupgofaturamento,'[Excluir Selecionado(s)]', ShortCut(vk_f3, [ssCtrl]),spdexcluirselecionados.OnClick);

	{comando de adi��o de teclas de atalhos gofaturamento<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgofaturamento.open();
	{abertura dos datasets<<<}


  {filtros projeto}
   if ParamStr(4) <>'99' then
  fncgofaturamento.filtrar(cdsgofaturamento,'CODPROJETO='+ParamStr(4)+' AND PARAMETRO='+QuotedStr('N'));
  {filtros projeto}


	fncgofaturamento.criaAtalhoPopupMenuNavegacao(Popupgofaturamento,cdsgofaturamento);

	fncgofaturamento.definirMascaraCampos(cdsgofaturamento);

	fncgofaturamento.configurarGridesFormulario(fgofaturamento,false, true);

  {<<vers�o da rotina>>}
  lblversao.Caption:=lblversao.Caption+' - '+fncgofaturamento.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}

 end);

end;

procedure Tfgofaturamento.spdAdicionargofaturamentoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgofaturamento.TabIndex;
	if (fncgofaturamento.adicionar(cdsgofaturamento)=true) then
		begin
			fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,true);
			dbegofaturamentocodprojeto.SetFocus;
	end
	else
		fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,false);
end;

procedure Tfgofaturamento.spdAlterargofaturamentoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgofaturamento.TabIndex;
	if (fncgofaturamento.alterar(cdsgofaturamento)=true) then
		begin
			fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,true);
			dbegofaturamentocodprojeto.SetFocus;
	end
	else
		fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,false);
end;

procedure Tfgofaturamento.spdGravargofaturamentoClick(Sender: TObject);
begin

  fncgofaturamento.verificarCamposObrigatorios(cdsgofaturamento,camposObrigatoriosgofaturamento, pgcPrincipal);
	if (fncgofaturamento.gravar(cdsgofaturamento)=true) then
		fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,false)
	else
		fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,true);
end;

procedure Tfgofaturamento.spdCancelargofaturamentoClick(Sender: TObject);
begin
	if (fncgofaturamento.cancelar(cdsgofaturamento)=true) then
		fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,false)
	else
		fncgofaturamento.ativarModoEdicao(objetosModoEdicaoAtivosgofaturamento,objetosModoEdicaoInativosgofaturamento,true);
end;

procedure Tfgofaturamento.spddesmarcartodosClick(Sender: TObject);
begin
 try
    if cdsgofaturamento.IsEmpty then abort;
    cdsgofaturamento.DisableControls;

    cdsgofaturamento.First;

    while not cdsgofaturamento.Eof do
    begin
     cdsgofaturamento.Edit;
     if cdsgofaturamentoselecionado.AsBoolean= true then
       cdsgofaturamentoselecionado.AsBoolean:= false;
     cdsgofaturamento.Post;
     cdsgofaturamento.Next;
    end;
   finally
    cdsgofaturamento.EnableControls;
   end;
end;

procedure Tfgofaturamento.spdExcluirgofaturamentoClick(Sender: TObject);
begin
	fncgofaturamento.excluir(cdsgofaturamento);
end;

procedure Tfgofaturamento.spdexcluirselecionadosClick(Sender: TObject);
begin

  try
   cdsgofaturamento.Filtered := false;
   cdsgofaturamento.Filter := 'selecionado = ' + 'True and parametro=''S''';
   cdsgofaturamento.Filtered := True;

   cdsgofaturamento.First;
   while not cdsgofaturamento.Eof do
   begin
      fncgofaturamento.excluir(cdsgofaturamento,true);
     cdsgofaturamento.First;
   end;
  finally
   cdsgofaturamento.Filter := 'parametro=''S''';
   cdsgofaturamento.Filtered := true;
  end;

end;

procedure Tfgofaturamento.cdsgofaturamentoBeforePost(DataSet: TDataSet);
begin
	if cdsgofaturamento.State in [dsinsert] then
	cdsgofaturamentoID.Value:=fncgofaturamento.autoNumeracaoGenerator(dm.conexao,'G_gofaturamento');
end;

procedure Tfgofaturamento.sessao;
var host: string; arquivo: tinifile;
begin
   try
   //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);


    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;



procedure Tfgofaturamento.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgofaturamento.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgofaturamento.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgofaturamento.spdmarcartodosClick(Sender: TObject);
begin
 try

   if cdsgofaturamento.IsEmpty then abort;
   cdsgofaturamento.DisableControls;
   cdsgofaturamento.First;

   while not cdsgofaturamento.Eof do
   begin
     cdsgofaturamento.Edit;
     cdsgofaturamentoselecionado.AsBoolean:= true;
     cdsgofaturamento.Post;
     cdsgofaturamento.Next;
   end;
   finally
    cdsgofaturamento.EnableControls;
   end;
end;

procedure Tfgofaturamento.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfgofaturamento.menuImpRelSinteticoClick(Sender: TObject);
begin
    if (ffiltro_avancado = nil) then Application.CreateForm(Tffiltro_avancado, ffiltro_avancado);
    ffiltro_avancado.ShowModal;
end;

end.

