unit ufiltro_avancado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxGDIPlusClasses, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.StdCtrls, uPesquisa, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, udm, Vcl.DialogMessage, uFuncoes,
  frxClass, frxDBSet, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, ugofaturamento,
  cxGroupBox, cxRadioGroup, Vcl.Menus, cxButtons, Datasnap.DBClient,
  Datasnap.Provider, Dateutils,clipbrd;

type
  Tffiltro_avancado = class(TForm)
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    imglogo: TImage;
    pnlLinhaTitulo: TPanel;
    lblSite: TLabel;
    pnlLinhaBotoessecao: TPanel;
    spdimprimir: TButton;
    spdfechar: TButton;
    pnlDadosfiltros: TPanel;
    pnlLayoutLinhafiltro1: TPanel;
    lblprojeto: TLabel;
    btnPesquisarprojeto: TSpeedButton;
    dbecodprojeto: TEdit;
    dbeprojeto: TEdit;
    pnlLayoutLinhafiltro2: TPanel;
    lblfilial: TLabel;
    btnPesquisarfilial: TSpeedButton;
    dbeID_FILIAL: TEdit;
    dbeFILIAL: TEdit;
    pnlLayoutLinhafiltro3: TPanel;
    lblCLIENTE: TLabel;
    btnPesquisarcliente: TSpeedButton;
    dbeID_CLIENTE: TEdit;
    dbeCLIENTE: TEdit;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    pnlLayoutLinhafiltro4: TPanel;
    lblDTFATURAMENTO: TLabel;
    data1: TcxDateEdit;
    data2: TcxDateEdit;
    psqcliente: TPesquisa;
    psqfilial: TPesquisa;
    psqprojeto: TPesquisa;
    frxDBgofaturamentogrupo: TfrxDBDataset;
    FDFaturamentoGrupo: TFDQuery;
    FDFaturamentoMarca: TFDQuery;
    FDFaturamentoFilial: TFDQuery;
    FDFaturamentoVenda: TFDQuery;
    frxDBgofaturamentomarca: TfrxDBDataset;
    frxDBgofaturamentofilial: TfrxDBDataset;
    frxDBgofaturamentovenda: TfrxDBDataset;
    FDFaturamentoFilialDT_LANCAMENTO: TDateField;
    FDFaturamentoFilialDIA_SEMANA: TStringField;
    FDFaturamentoFilialFILIAL: TStringField;
    FDFaturamentoFilialVLFATURADO: TFloatField;
    FDFaturamentoMarcaDT_LANCAMENTO: TDateField;
    FDFaturamentoMarcaDIA_SEMANA: TStringField;
    FDFaturamentoMarcaMARCA: TStringField;
    FDFaturamentoMarcaVLFATURADO: TFloatField;
    FDFaturamentoVendaDT_LANCAMENTO: TDateField;
    FDFaturamentoVendaDIA_SEMANA: TStringField;
    FDFaturamentoVendaVENDEDOR: TStringField;
    FDFaturamentoVendaVLFATURADO: TFloatField;
    FDFaturamentoMarcaCODPROJETO: TIntegerField;
    FDFaturamentoMarcaPROJETO: TStringField;
    FDFaturamentoFilialCODPROJETO: TIntegerField;
    FDFaturamentoFilialPROJETO: TStringField;
    FDFaturamentoVendaCODPROJETO: TIntegerField;
    FDFaturamentoVendaPROJETO: TStringField;
    FDFaturamentoMarcaCODFILIAL: TIntegerField;
    FDFaturamentoMarcaFILIAL: TStringField;
    FDFaturamentoFilialCODFILIAL: TIntegerField;
    FDFaturamentoVendaCODFILIAL: TIntegerField;
    FDFaturamentoVendaFILIAL: TStringField;
    rgrelatorio: TcxRadioGroup;
    FDFaturamentoMarcaMETA: TStringField;
    FDFaturamentoFilialMETA: TStringField;
    FDFaturamentoVendaMETA: TStringField;
    frxgofaturamentomarca: TfrxReport;
    frxgofaturamentofilial: TfrxReport;
    frxgofaturamentovenda: TfrxReport;
    spbdatesintetico: TcxButton;
    popmenudata: TPopupMenu;
    Hoje: TMenuItem;
    Ontem: TMenuItem;
    SemanaAtual: TMenuItem;
    SemanaAnterior: TMenuItem;
    UltimosQuizeDias: TMenuItem;
    N1: TMenuItem;
    MesAtual: TMenuItem;
    MesAnterior: TMenuItem;
    N2: TMenuItem;
    TrimestreAtual: TMenuItem;
    TrimestreAnterior: TMenuItem;
    N3: TMenuItem;
    SemestreAtual: TMenuItem;
    SemestreAnterior: TMenuItem;
    N4: TMenuItem;
    AnoAtual: TMenuItem;
    AnoAnterior: TMenuItem;
    N5: TMenuItem;
    LimparDatas: TMenuItem;
    pnlLayoutLinhafiltro5: TPanel;
    lblgrupoproduto: TLabel;
    btnPesquisarGRUPPRODUTO: TSpeedButton;
    dbeID_GRUPO_PRODUTO: TEdit;
    dbeGRUPOPRODUTO: TEdit;
    psqgrupoproduto: TPesquisa;
    dspFaturamentoGrupo: TDataSetProvider;
    CDSFaturamentoGrupo: TClientDataSet;
    FDFaturamentoGrupoCODPROJETO: TIntegerField;
    FDFaturamentoGrupoPROJETO: TStringField;
    FDFaturamentoGrupoCODFILIAL: TIntegerField;
    FDFaturamentoGrupoFILIAL: TStringField;
    FDFaturamentoGrupoDT_LANCAMENTO: TDateField;
    FDFaturamentoGrupoDIA: TStringField;
    FDFaturamentoGrupoDIA_SEMANA: TStringField;
    FDFaturamentoGrupoDESCRICAO_DIA_UTIL: TStringField;
    FDFaturamentoGrupoGRUPO_PRODUTO: TStringField;
    FDFaturamentoGrupoALIQUOTA: TFloatField;
    FDFaturamentoGrupoVLFATURADO: TFloatField;
    FDFaturamentoGrupoTOTAL_UTEIS_YERSTEDAY: TIntegerField;
    FDFaturamentoGrupoTOTAL_UTEIS_TOMORROW: TIntegerField;
    CDSFaturamentoGrupoCODPROJETO: TIntegerField;
    CDSFaturamentoGrupoPROJETO: TStringField;
    CDSFaturamentoGrupoCODFILIAL: TIntegerField;
    CDSFaturamentoGrupoFILIAL: TStringField;
    CDSFaturamentoGrupoDT_LANCAMENTO: TDateField;
    CDSFaturamentoGrupoDIA: TStringField;
    CDSFaturamentoGrupoDIA_SEMANA: TStringField;
    CDSFaturamentoGrupoDESCRICAO_DIA_UTIL: TStringField;
    CDSFaturamentoGrupoGRUPO_PRODUTO: TStringField;
    CDSFaturamentoGrupoALIQUOTA: TFloatField;
    CDSFaturamentoGrupoVLFATURADO: TFloatField;
    CDSFaturamentoGrupoTOTAL_UTEIS_YERSTEDAY: TIntegerField;
    CDSFaturamentoGrupoTOTAL_UTEIS_TOMORROW: TIntegerField;
    FDFaturamentoGrupoMETA: TFloatField;
    CDSFaturamentoGrupoMETA: TFloatField;
    frxgofaturamentogrupo: TfrxReport;
    FDFaturamentoProjeto: TFDQuery;
    FDFaturamentoProjetoCODPROJETO: TIntegerField;
    FDFaturamentoProjetoPROJETO: TStringField;
    FDFaturamentoProjetoDT_LANCAMENTO: TDateField;
    FDFaturamentoProjetoDIA: TStringField;
    FDFaturamentoProjetoDIA_SEMANA: TStringField;
    FDFaturamentoProjetoMETA: TFloatField;
    FDFaturamentoProjetoVLFATURADO: TFloatField;
    FDFaturamentoProjetoTOTAL_UTEIS_YERSTEDAY: TIntegerField;
    FDFaturamentoProjetoTOTAL_UTEIS_TOMORROW: TIntegerField;
    frxDBFaturamentoProjeto: TfrxDBDataset;
    frxgofaturamentoProjeto: TfrxReport;
    procedure spdfecharClick(Sender: TObject);
    procedure dbecodprojetoKeyPress(Sender: TObject; var Key: Char);
    procedure dbeID_CLIENTEKeyPress(Sender: TObject; var Key: Char);
    procedure dbeID_FILIALKeyPress(Sender: TObject; var Key: Char);
    procedure spdimprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FDFaturamentoGrupoAfterOpen(DataSet: TDataSet);
    procedure FDFaturamentoMarcaAfterOpen(DataSet: TDataSet);
    procedure FDFaturamentoFilialAfterOpen(DataSet: TDataSet);
    procedure FDFaturamentoVendaAfterOpen(DataSet: TDataSet);
    procedure FDFaturamentoGrupoCalcFields(DataSet: TDataSet);
    procedure spbdatesinteticoClick(Sender: TObject);
    procedure HojeClick(Sender: TObject);
    procedure OntemClick(Sender: TObject);
    procedure SemanaAtualClick(Sender: TObject);
    procedure SemanaAnteriorClick(Sender: TObject);
    procedure UltimosQuizeDiasClick(Sender: TObject);
    procedure MesAtualClick(Sender: TObject);
    procedure MesAnteriorClick(Sender: TObject);
    procedure TrimestreAtualClick(Sender: TObject);
    procedure SemestreAtualClick(Sender: TObject);
    procedure SemestreAnteriorClick(Sender: TObject);
    procedure AnoAtualClick(Sender: TObject);
    procedure AnoAnteriorClick(Sender: TObject);
    procedure LimparDatasClick(Sender: TObject);
    procedure dbeID_GRUPO_PRODUTOKeyPress(Sender: TObject; var Key: Char);
    procedure frxgofaturamentogrupo1BeforePrint(Sender: TfrxReportComponent);
    procedure FormShow(Sender: TObject);
    procedure psqprojetoPreencherCampos(Sender: TObject);
  private
    procedure pesquisar_grupoproduto;
    procedure pesquisar_marca;
    procedure pesquisar_filial;
    procedure pesquisar_vendedor;
    procedure pesquisar_Projeto;
    { Private declarations }
  public
    { Public declarations }
  public
  fncfiltros: TFuncoes;
  end;

var
  ffiltro_avancado: Tffiltro_avancado;

implementation

{$R *.dfm}

procedure Tffiltro_avancado.AnoAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncfiltros.anoanterior (Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.AnoAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.anoantual (Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.dbecodprojetoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeprojeto.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.dbeID_CLIENTEKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeCLIENTE.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.dbeID_FILIALKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;
  if key = #8 then
  begin
   dbeFILIAL.Text  :=EmptyStr;
  end;

end;

procedure Tffiltro_avancado.dbeID_GRUPO_PRODUTOKeyPress(Sender: TObject;
  var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

  if key = #8 then
  begin
   dbeGRUPOPRODUTO.Text  :=EmptyStr;
  end;
end;

procedure Tffiltro_avancado.FDFaturamentoFilialAfterOpen(DataSet: TDataSet);
begin
//  carregar_parametros(FDFaturamentoFilialCODPROJETO.AsInteger, FDFaturamentoFilialCODFILIAL.AsInteger);
end;

procedure Tffiltro_avancado.FDFaturamentoGrupoAfterOpen(DataSet: TDataSet);
begin
//   carregar_parametros(FDFaturamentoGrupoCODPROJETO.AsInteger, FDFaturamentoGrupoCODFILIAL.AsInteger);
end;

procedure Tffiltro_avancado.FDFaturamentoGrupoCalcFields(DataSet: TDataSet);
begin
//  FDFaturamentoGrupoAGRUPADOR.AsString  := FDFaturamentoGrupoCODPROJETO.AsString +'**' + FDFaturamentoGrupoDT_LANCAMENTO.AsString;
end;

procedure Tffiltro_avancado.FDFaturamentoMarcaAfterOpen(DataSet: TDataSet);
begin
//  carregar_parametros(FDFaturamentoMarcaCODPROJETO.AsInteger, FDFaturamentoMarcaCODFILIAL.AsInteger);
end;

procedure Tffiltro_avancado.pesquisar_grupoproduto;
begin
    CDSFaturamentoGrupo.Close;
    FDFaturamentoGrupo.Close;
    FDFaturamentoGrupo.SQL.Clear;
    FDFaturamentoGrupo.SQL.Add('SELECT  codprojeto, projeto, CODFILIAL, FILIAL, dt_lancamento,                                                                          ');
    FDFaturamentoGrupo.SQL.Add('case when DIA_SEMANA in (''DOMINGO'',''S�BADO'') then                                                                                   ');
    FDFaturamentoGrupo.SQL.Add('    ''Final de Semana''                                                                                                                 ');
    FDFaturamentoGrupo.SQL.Add('    else                                                                                                                                ');
    FDFaturamentoGrupo.SQL.Add('    ''Dia da Semana''                                                                                                                   ');
    FDFaturamentoGrupo.SQL.Add('    end Dia, dia_semana, descricao_dia_util,GRUPO_PRODUTO, meta, ALIQUOTA, vlfaturado,TOTAL_UTEIS_YERSTEDAY,TOTAL_UTEIS_TOMORROW        ');
    FDFaturamentoGrupo.SQL.Add('FROM (                                                                                                                                  ');
    FDFaturamentoGrupo.SQL.Add('    SELECT GOFATURAMENTO.codprojeto, GOFATURAMENTO.projeto, GOFATURAMENTO.codfilial, GOFATURAMENTO.filial,  GOFATURAMENTO.DT_LANCAMENTO ');
    FDFaturamentoGrupo.SQL.Add('        ,GOFATURAMENTO.DIA_SEMANA                                                                                                       ');
    FDFaturamentoGrupo.SQL.Add('        ,gofaturamento_dia_util.dia_util descricao_dia_util                                                                             ');
    FDFaturamentoGrupo.SQL.Add('        ,COALESCE(GOFATURAMENTO.GRUPO_PRODUTO,''SEM CLASSIFICA��O'') GRUPO_PRODUTO                                                      ');
    FDFaturamentoGrupo.SQL.Add('        , goparametros.valor   as meta                                                                                                  ');
    FDFaturamentoGrupo.SQL.Add('        , gogrupoproduto.meta AS ALIQUOTA                                                                                               ');
    FDFaturamentoGrupo.SQL.Add('        ,sum(GOFATURAMENTO.VLFATURADO) VLFATURADO                                                                                       ');
    FDFaturamentoGrupo.SQL.Add('        ,(                                                                                                                              ');
    FDFaturamentoGrupo.SQL.Add('      SELECT count(*)                                                                                                                   ');
    FDFaturamentoGrupo.SQL.Add('      FROM GOFATURAMENTO_DIA_UTIL                                                                                                       ');
    FDFaturamentoGrupo.SQL.Add('      WHERE GOFATURAMENTO_DIA_UTIL.ID_FILIAL =:CODFILIAL                                                                                ');
    FDFaturamentoGrupo.SQL.Add('          AND DATA BETWEEN :DATA1 AND CURRENT_DATE-1                                                                                      ');
    FDFaturamentoGrupo.SQL.Add('          AND DIA_UTIL = ''SIM''                                                                                                        ');
    FDFaturamentoGrupo.SQL.Add('      ) TOTAL_UTEIS_YERSTEDAY                                                                                                           ');
    FDFaturamentoGrupo.SQL.Add('  ,(                                                                                                                                    ');
    FDFaturamentoGrupo.SQL.Add('      SELECT count(*)                                                                                                                   ');
    FDFaturamentoGrupo.SQL.Add('      FROM GOFATURAMENTO_DIA_UTIL                                                                                                       ');
    FDFaturamentoGrupo.SQL.Add('      WHERE GOFATURAMENTO_DIA_UTIL.ID_FILIAL =:CODFILIAL                                                                                ');
    FDFaturamentoGrupo.SQL.Add('          AND DATA BETWEEN CURRENT_DATE + 1   AND :DATA2                                                                                ');
    FDFaturamentoGrupo.SQL.Add('          AND DIA_UTIL = ''SIM''                                                                                                        ');
    FDFaturamentoGrupo.SQL.Add('      ) TOTAL_UTEIS_TOMORROW                                                                                                            ');
    FDFaturamentoGrupo.SQL.Add('    FROM GOFATURAMENTO, goparametros, gogrupoproduto, gofaturamento_dia_util WHERE gofaturamento.codprojeto= goparametros.id_projeto    ');
    FDFaturamentoGrupo.SQL.Add('    and gofaturamento.codfilial= goparametros.id_filial                                                                                 ');
    FDFaturamentoGrupo.SQL.Add('    AND gofaturamento.grupo_produto= gogrupoproduto.descricao                                                                           ');
    FDFaturamentoGrupo.SQL.Add('    and gofaturamento.codPROJETO= gogrupoproduto.codprojeto                                                                             ');
   // FDFaturamentoGrupo.SQL.Add('    and gofaturamento.CODFILIAL=  gogrupoproduto.codcliente                                                                           ');
    FDFaturamentoGrupo.SQL.Add('    AND gogrupoproduto.CODCLIENTE=goparametros.id_filial                                                                                 ');

    FDFaturamentoGrupo.SQL.Add('    and gofaturamento.codfilial=  gofaturamento_dia_util.id_filial                                                                      ');
    FDFaturamentoGrupo.SQL.Add('    and gofaturamento.dt_lancamento= gofaturamento_dia_util.data                                                                        ');
    FDFaturamentoGrupo.SQL.Add('    and gofaturamento.dt_lancamento  BETWEEN :DATA1 AND :DATA2                                                                          ');
    FDFaturamentoGrupo.SQL.Add('    and gofaturamento.codfilial=:CODFILIAL                                                                                              ');

    if dbecodprojeto.Text <> EmptyStr then
    begin
      FDFaturamentoGrupo.SQL.Add(' and gofaturamento.CODPROJETO=:CODPROJETO');
      FDFaturamentoGrupo.ParamByName('CODPROJETO').AsInteger := strtoInt(dbecodprojeto.Text);
    end;
    if dbeID_FILIAL.Text <> EmptyStr then
    begin
      FDFaturamentoGrupo.SQL.Add(' and gofaturamento.CODFILIAL=:CODFILIAL');
      FDFaturamentoGrupo.ParamByName('CODFILIAL').AsInteger := strtoInt(dbeID_FILIAL.Text);
    end;
    if ((data1.Text <> EmptyStr) and (data2.Text <> EmptyStr)) then
    begin
      FDFaturamentoGrupo.SQL.Add(' and gofaturamento.DT_LANCAMENTO BETWEEN :DATA1 AND :DATA2');
      FDFaturamentoGrupo.ParamByName('DATA1').AsDateTime := StrToDateTime(data1.Text);
      FDFaturamentoGrupo.ParamByName('DATA2').AsDateTime := StrToDateTime(data2.Text);
    end;
      if dbeID_GRUPO_PRODUTO.Text <> EmptyStr then
    begin
      FDFaturamentoGrupo.SQL.Add(' and gofaturamento.GRUPO_PRODUTO=:GRUPO_PRODUTO');
      FDFaturamentoGrupo.ParamByName('GRUPO_PRODUTO').asString := dbeGRUPOPRODUTO.Text;
    end;

      FDFaturamentoGrupo.SQL.Add('    GROUP BY 1,2,3,4,5,6,7,8,9,10)                                                                                                      ');
      FDFaturamentoGrupo.SQL.Add('ORDER BY GRUPO_PRODUTO,DT_LANCAMENTO                                                                                                    ');

      FDFaturamentoGrupo.Open();
      Clipboard.AsText :=FDFaturamentoGrupo.SQL.GetText;
      CDSFaturamentoGrupo.Open;

  if not CDSFaturamentoGrupo.IsEmpty then
  begin
    if data1.Date>= StartofTheMonth(date) then
    fncfiltros.visualizarRelatorios(frxgofaturamentogrupo)
    else
    fncfiltros.visualizarRelatorios(frxgofaturamentogrupo);
  end;

end;

procedure Tffiltro_avancado.pesquisar_marca;
begin

  FDFaturamentoMarca.Close;
  FDFaturamentoMarca.SQL.Clear;
  FDFaturamentoMarca.SQL.Add('select * from view_faturamento_diario_marca where 1=1');
  if dbecodprojeto.Text <> EmptyStr then
  begin
    FDFaturamentoMarca.SQL.Add(' and CODPROJETO=:CODPROJETO');
    FDFaturamentoMarca.ParamByName('CODPROJETO').AsInteger := strtoInt(dbecodprojeto.Text);
  end;
  if dbeID_FILIAL.Text <> EmptyStr then
  begin
    FDFaturamentoMarca.SQL.Add(' and CODFILIAL=:CODFILIAL');
    FDFaturamentoMarca.ParamByName('CODFILIAL').AsInteger := strtoInt(dbeID_FILIAL.Text);
  end;
  if ((data1.Text <> EmptyStr) and (data2.Text <> EmptyStr)) then
  begin
    FDFaturamentoMarca.SQL.Add(' and DT_LANCAMENTO BETWEEN :DATA1 AND :DATA2');
    FDFaturamentoMarca.ParamByName('DATA1').AsDateTime := StrToDateTime(data1.Text);
    FDFaturamentoMarca.ParamByName('DATA2').AsDateTime := StrToDateTime(data2.Text);
  end;

  FDFaturamentoMarca.Open;
  if not FDFaturamentoMarca.IsEmpty then
    fncfiltros.visualizarRelatorios(frxgofaturamentomarca);

end;

procedure Tffiltro_avancado.pesquisar_Projeto;
begin

  FDFaturamentoProjeto.Close;
  FDFaturamentoProjeto.ParamByName('codprojeto').AsInteger :=strtoInt(dbecodprojeto.Text);
  FDFaturamentoProjeto.ParamByName('DATA1').AsDateTime := StrToDateTime(data1.Text);
  FDFaturamentoProjeto.ParamByName('DATA2').AsDateTime := StrToDateTime(data2.Text);
  FDFaturamentoProjeto.Open();

  if not FDFaturamentoProjeto.IsEmpty then
  begin
    if data1.Date>= StartofTheMonth(date) then
     fncfiltros.visualizarRelatorios(frxgofaturamentoProjeto)
    else
     fncfiltros.visualizarRelatorios(frxgofaturamentoProjeto);
  end;
end;

procedure Tffiltro_avancado.pesquisar_vendedor;
begin
  FDFaturamentoVenda.Close;
  FDFaturamentoVenda.SQL.Clear;
  FDFaturamentoVenda.SQL.Add('select * from view_faturamento_diario_venda where 1=1');
  if dbecodprojeto.Text <> EmptyStr then
  begin
    FDFaturamentoVenda.SQL.Add(' and CODPROJETO=:CODPROJETO');
    FDFaturamentoVenda.ParamByName('CODPROJETO').AsInteger := strtoInt(dbecodprojeto.Text);
  end;
  if dbeID_FILIAL.Text <> EmptyStr then
  begin
    FDFaturamentoVenda.SQL.Add(' and CODFILIAL=:CODFILIAL');
    FDFaturamentoVenda.ParamByName('CODFILIAL').AsInteger := strtoInt(dbeID_FILIAL.Text);
  end;
  if ((data1.Text <> EmptyStr) and (data2.Text <> EmptyStr)) then
  begin
    FDFaturamentoVenda.SQL.Add(' and DT_LANCAMENTO BETWEEN :DATA1 AND :DATA2');
    FDFaturamentoVenda.ParamByName('DATA1').AsDateTime := StrToDateTime(data1.Text);
    FDFaturamentoVenda.ParamByName('DATA2').AsDateTime := StrToDateTime(data2.Text);
  end;
  FDFaturamentoVenda.Open();
  if not FDFaturamentoVenda.IsEmpty then
  fncfiltros.visualizarRelatorios(frxgofaturamentovenda);

end;

procedure Tffiltro_avancado.psqprojetoPreencherCampos(Sender: TObject);
begin
  if dbecodprojeto.Text<>EmptyStr then
  begin
   psqfilial.SQL.Clear;
   psqfilial.SQL.Add('SELECT * FROM V_PROJETO_FILIAL WHERE 1=1 AND CODPROJETO='+dbecodprojeto.Text);
   psqgrupoproduto.SQL.Clear;
   psqgrupoproduto.SQL.Add('SELECT * FROM v_grupo_produto WHERE 1=1 AND CODPROJETO='+dbecodprojeto.Text);
  end
  else
  begin
   psqfilial.SQL.Clear;
   psqfilial.SQL.Add('SELECT * FROM V_PROJETO_FILIAL WHERE 1=1');
   psqgrupoproduto.SQL.Clear;
   psqgrupoproduto.SQL.Add('SELECT * FROM v_grupo_produto WHERE 1=1');
   end;
end;

procedure Tffiltro_avancado.SemanaAnteriorClick(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncfiltros.SemanaAnterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemanaAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.SemanaAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemestreAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.semestreanterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.SemestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.semestreatual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;



procedure Tffiltro_avancado.pesquisar_filial;
begin
  FDFaturamentoFilial.Close;
  FDFaturamentoFilial.SQL.Clear;
  FDFaturamentoFilial.SQL.Add('select * from view_faturamento_diario_FILIAL where 1=1');
  if dbecodprojeto.Text <> EmptyStr then
  begin
    FDFaturamentoFilial.SQL.Add(' and CODPROJETO=:CODPROJETO');
    FDFaturamentoFilial.ParamByName('CODPROJETO').AsInteger := strtoInt(dbecodprojeto.Text);
  end;
  if dbeID_FILIAL.Text <> EmptyStr then
  begin
    FDFaturamentoFilial.SQL.Add(' and CODFILIAL=:CODFILIAL');
    FDFaturamentoFilial.ParamByName('CODFILIAL').AsInteger := strtoInt(dbeID_FILIAL.Text);
  end;
  if ((data1.Text <> EmptyStr) and (data2.Text <> EmptyStr)) then
  begin
    FDFaturamentoFilial.SQL.Add(' and DT_LANCAMENTO BETWEEN :DATA1 AND :DATA2');
    FDFaturamentoFilial.ParamByName('DATA1').AsDateTime := StrToDateTime(data1.Text);
    FDFaturamentoFilial.ParamByName('DATA2').AsDateTime := StrToDateTime(data2.Text);
  end;
  FDFaturamentoFilial.Open;
  if not FDFaturamentoFilial.IsEmpty then
    fncfiltros.visualizarRelatorios(frxgofaturamentoFILIAL);

end;

procedure Tffiltro_avancado.FDFaturamentoVendaAfterOpen(DataSet: TDataSet);
begin
//   carregar_parametros(FDFaturamentoVendaCODPROJETO.AsInteger, FDFaturamentoVendaCODFILIAL.AsInteger);
end;

procedure Tffiltro_avancado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  fncfiltros.configurarGridesFormulario(ffiltro_avancado, true, false);
	{>>>eliminando container de fun��es da memoria}
	fncfiltros.Free;
	{eliminando container de fun��es da memoria<<<}

	ffiltro_avancado :=nil;
	Action           :=cafree;
end;

procedure Tffiltro_avancado.FormCreate(Sender: TObject);
begin
  fncfiltros:= TFuncoes.Create(Self);
	fncfiltros.definirConexao(dm.conexao);
  fncfiltros.definirConexao(dm.conexao);
	fncfiltros.definirFormulario(Self);
	fncfiltros.validarTransacaoRelacionamento:=true;
	fncfiltros.autoAplicarAtualizacoesBancoDados:=true;
  if fgofaturamento.lblProjetoresultado.Caption <>'99' then
   begin
    dbecodprojeto.Text:= fgofaturamento.lblProjetoresultado.Caption;
    dbecodprojeto.OnExit(dbecodprojeto);
    dbecodprojeto.Enabled:= false;
   end
  else
    dbecodprojeto.Enabled:= true;


end;

procedure Tffiltro_avancado.FormShow(Sender: TObject);
begin
 data1.Date:= StartofTheMonth(Date);
 data2.Date:= EndOfTheMonth(Date);

end;

procedure Tffiltro_avancado.frxgofaturamentogrupo1BeforePrint(
  Sender: TfrxReportComponent);
  var vlfaturado : double;
begin
  {if (Sender is tfrxmemoview) then
    begin
      if (Sender as tfrxmemoview).Name='memoCampoVLALCANCARFOOTER' then
      begin
//       if  StrToFloatDef(CDSFaturamentoGrupototalfaturado.AsString,0.00)  >CDSFaturamentoGrupoALIQUOTA.AsFloat then
  //      vlfaturado:= strtoFloat((Sender as tfrxmemoview).Text);
//        if vlfaturado<0 then
        if strtoFloat((Sender as tfrxmemoview).Text)<0 then
       (Sender as tfrxmemoview).Text:='0';

      end;
       if (Sender as tfrxmemoview).Name='MemoFOOTERGERAL' then
      begin
       //if  StrToFloatDef (CDSFaturamentoGrupototalfaturado.AsString,0.00)>CDSFaturamentoGrupoALIQUOTA.AsFloat then
        if strtoFloat((Sender as tfrxmemoview).Text)<0 then
       (Sender as tfrxmemoview).Text:='0';
      end;
    end; }
end;

procedure Tffiltro_avancado.HojeClick(Sender: TObject);
begin
 try
  fncfiltros.DiaAtual(Date);
  data1.Date  := fncfiltros.DiaAtual(Date);
  data2.Date  := fncfiltros.DiaAtual(Date);
 finally
 end;
end;

procedure Tffiltro_avancado.LimparDatasClick(Sender: TObject);
begin
  data1.Clear;
  data2.Clear;
end;

procedure Tffiltro_avancado.MesAnteriorClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.MesAnterior(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.MesAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.MesAtual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.OntemClick(Sender: TObject);
begin
 try
    Data1.Date  :=fncfiltros.DiaAnterior(Date);
    Data2.Date  :=fncfiltros.DiaAnterior(Date);
   finally
   end;
end;

procedure Tffiltro_avancado.spbdatesinteticoClick(Sender: TObject);
begin
 with TcxButton(Sender).ClientToScreen(point(TcxButton(Sender).Width,
    TcxButton(Sender).Height)) do
  begin
    popmenudata.Popup(X, Y);
  end;
end;

procedure Tffiltro_avancado.spdfecharClick(Sender: TObject);
begin
 ModalResult:= mrOk;
end;

procedure Tffiltro_avancado.spdimprimirClick(Sender: TObject);
begin
  if ((data1.Text= EmptyStr) or (data2.Text=EmptyStr)) then
  abort;

  if dbecodprojeto.Text=EmptyStr then
  begin
    application.MessageBox('Voc� deve preencher o Projeto.','Campo obrigat�rio.',MB_ICONEXCLAMATION);
    dbecodprojeto.SetFocus;
    abort;
  end;

   case rgrelatorio.ItemIndex of
     1: if dbeID_FILIAL.Text=EmptyStr then
        begin
          application.MessageBox('Voc� deve preencher a Filial.','Campo obrigat�rio.',MB_ICONEXCLAMATION);
          dbeID_FILIAL.SetFocus;
          abort;
        end;
    end;


  if ((daysbetween(data2.Date ,data1.Date) >31) and (rgrelatorio.ItemIndex=0))  then
  begin
    application.MessageBox('Intervalo pesquisado deve ser menor que 31 dias.','Intervalo permitido.',MB_ICONEXCLAMATION);
    dbeID_FILIAL.SetFocus;
    abort;
  end;

  TDialogMessage.ShowWaitMessage('Gerando relat�rio...',
  procedure
  begin
      case rgrelatorio.ItemIndex of
       0: pesquisar_projeto;
       1: pesquisar_grupoproduto;
      end;

  end);
end;

procedure Tffiltro_avancado.TrimestreAtualClick(Sender: TObject);
var data : TStringList;
begin
   try
    data := TStringList.Create;
    data:=fncfiltros.trimestreatual(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure Tffiltro_avancado.UltimosQuizeDiasClick(Sender: TObject);
var data : TStringList;
begin

   try
    data := TStringList.Create;
    data:=fncfiltros.UltimosQuinzeDias(Date);
    data1.Date:= StrToDateTime(data[0]);
    data2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;

end;

end.
