CREATE TABLE GOFATURAMENTO(
ID Integer   
,
CONSTRAINT pk_GOFATURAMENTO PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODPROJETO Integer   
,
PROJETO Varchar(250)   COLLATE PT_BR
,
CODFILIAL Integer   
,
FILIAL Varchar(250)   COLLATE PT_BR
,
CODCLIENTE Integer   
,
CLIENTE Varchar(250)   COLLATE PT_BR
,
PEDIDO Integer   
,
NF Integer   
,
USUARIO Varchar(250)   COLLATE PT_BR
,
VENDEDOR Varchar(250)   COLLATE PT_BR
,
DT_LANCAMENTO Date   
,
DT_ENTREGA_PREVISTA Date   
,
DT_ENTREGA Date   
,
STATUS Varchar(30)   COLLATE PT_BR
,
SITUACAO_NF Varchar(30)   COLLATE PT_BR
,
CODPRODUTO Integer   
,
PRODUTO Varchar(250)   COLLATE PT_BR
,
GRUPO_PRODUTO Varchar(250)   COLLATE PT_BR
,
ESTILO Varchar(250)   COLLATE PT_BR
,
MARCA Varchar(250)   COLLATE PT_BR
,
QT Double Precision   
,
UNIDADE Varchar(30)   COLLATE PT_BR
,
UNIDADE_VENDA Varchar(30)   COLLATE PT_BR
,
PRECO Double Precision   
,
VLPEDIDOITEM Double Precision   
,
VLCOBRADOITEM Double Precision   
,
VLFRETE Double Precision   
,
VLFATURADO Double Precision   
);
commit;
CREATE ASC INDEX I01_GOFA_MENTO_ID ON GOFATURAMENTO (ID);
CREATE ASC INDEX I02_GOFA_MENTO_DT_CADASTRO ON GOFATURAMENTO (DT_CADASTRO);
CREATE ASC INDEX I03_GOFA_MENTO_HS_CADASTRO ON GOFATURAMENTO (HS_CADASTRO);
CREATE ASC INDEX I04_GOFA_MENTO_CODPROJETO ON GOFATURAMENTO (CODPROJETO);
CREATE ASC INDEX I05_GOFA_MENTO_PROJETO ON GOFATURAMENTO (PROJETO);
CREATE ASC INDEX I06_GOFA_MENTO_CODFILIAL ON GOFATURAMENTO (CODFILIAL);
CREATE ASC INDEX I07_GOFA_MENTO_FILIAL ON GOFATURAMENTO (FILIAL);
CREATE ASC INDEX I08_GOFA_MENTO_CODCLIENTE ON GOFATURAMENTO (CODCLIENTE);
CREATE ASC INDEX I09_GOFA_MENTO_CLIENTE ON GOFATURAMENTO (CLIENTE);
CREATE ASC INDEX I10_GOFA_MENTO_PEDIDO ON GOFATURAMENTO (PEDIDO);
CREATE ASC INDEX I11_GOFA_MENTO_NF ON GOFATURAMENTO (NF);
CREATE ASC INDEX I12_GOFA_MENTO_USUARIO ON GOFATURAMENTO (USUARIO);
CREATE ASC INDEX I13_GOFA_MENTO_VENDEDOR ON GOFATURAMENTO (VENDEDOR);
CREATE ASC INDEX I14_GOFA_MENTO_DT_LANCAMENTO ON GOFATURAMENTO (DT_LANCAMENTO);
CREATE ASC INDEX I15_GOFA_MENTO_DT_ENTREGA_PREVIS ON GOFATURAMENTO (DT_ENTREGA_PREVISTA);
CREATE ASC INDEX I16_GOFA_MENTO_DT_ENTREGA ON GOFATURAMENTO (DT_ENTREGA);
CREATE ASC INDEX I17_GOFA_MENTO_STATUS ON GOFATURAMENTO (STATUS);
CREATE ASC INDEX I18_GOFA_MENTO_SITUACAO_NF ON GOFATURAMENTO (SITUACAO_NF);
CREATE ASC INDEX I19_GOFA_MENTO_CODPRODUTO ON GOFATURAMENTO (CODPRODUTO);
CREATE ASC INDEX I20_GOFA_MENTO_PRODUTO ON GOFATURAMENTO (PRODUTO);
CREATE ASC INDEX I21_GOFA_MENTO_GRUPO_PRODUTO ON GOFATURAMENTO (GRUPO_PRODUTO);
CREATE ASC INDEX I22_GOFA_MENTO_ESTILO ON GOFATURAMENTO (ESTILO);
CREATE ASC INDEX I23_GOFA_MENTO_MARCA ON GOFATURAMENTO (MARCA);
CREATE ASC INDEX I24_GOFA_MENTO_QT ON GOFATURAMENTO (QT);
CREATE ASC INDEX I25_GOFA_MENTO_UNIDADE ON GOFATURAMENTO (UNIDADE);
CREATE ASC INDEX I26_GOFA_MENTO_UNIDADE_VENDA ON GOFATURAMENTO (UNIDADE_VENDA);
CREATE ASC INDEX I27_GOFA_MENTO_PRECO ON GOFATURAMENTO (PRECO);
CREATE ASC INDEX I28_GOFA_MENTO_VLPEDIDOITEM ON GOFATURAMENTO (VLPEDIDOITEM);
CREATE ASC INDEX I29_GOFA_MENTO_VLCOBRADOITEM ON GOFATURAMENTO (VLCOBRADOITEM);
CREATE ASC INDEX I30_GOFA_MENTO_VLFRETE ON GOFATURAMENTO (VLFRETE);
CREATE ASC INDEX I31_GOFA_MENTO_VLFATURADO ON GOFATURAMENTO (VLFATURADO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data Cadastro' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora cadastro' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Projeto' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'CODPROJETO';
update rdb$relation_fields set rdb$description = 'Projeto' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'PROJETO';
update rdb$relation_fields set rdb$description = 'C�d.Filial' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'CODFILIAL';
update rdb$relation_fields set rdb$description = 'Filial' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'FILIAL';
update rdb$relation_fields set rdb$description = 'Cod.Cliente' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'CODCLIENTE';
update rdb$relation_fields set rdb$description = 'Cliente' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'CLIENTE';
update rdb$relation_fields set rdb$description = 'N�.Pedido' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'PEDIDO';
update rdb$relation_fields set rdb$description = 'Nota Fiscal' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'NF';
update rdb$relation_fields set rdb$description = 'Usu�rio' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'USUARIO';
update rdb$relation_fields set rdb$description = 'Vendedor' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'VENDEDOR';
update rdb$relation_fields set rdb$description = 'Data Lan�amento' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'DT_LANCAMENTO';
update rdb$relation_fields set rdb$description = 'Data prevista entrega' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'DT_ENTREGA_PREVISTA';
update rdb$relation_fields set rdb$description = 'Data entrega' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'DT_ENTREGA';
update rdb$relation_fields set rdb$description = 'Status' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'STATUS';
update rdb$relation_fields set rdb$description = 'Situa��o NFe' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'SITUACAO_NF';
update rdb$relation_fields set rdb$description = 'C�d.Produto' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'CODPRODUTO';
update rdb$relation_fields set rdb$description = 'Produto' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'PRODUTO';
update rdb$relation_fields set rdb$description = 'Grupo Produto' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'GRUPO_PRODUTO';
update rdb$relation_fields set rdb$description = 'Estilo' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'ESTILO';
update rdb$relation_fields set rdb$description = 'Marca' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'MARCA';
update rdb$relation_fields set rdb$description = 'Qtd.' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'QT';
update rdb$relation_fields set rdb$description = 'Unidade' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'UNIDADE';
update rdb$relation_fields set rdb$description = 'Unidade venda' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'UNIDADE_VENDA';
update rdb$relation_fields set rdb$description = 'Pre�o unit�rio' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'PRECO';
update rdb$relation_fields set rdb$description = 'Valor pedido item' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'VLPEDIDOITEM';
update rdb$relation_fields set rdb$description = 'Valor cobrado item' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'VLCOBRADOITEM';
update rdb$relation_fields set rdb$description = 'Frete cobrado' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'VLFRETE';
update rdb$relation_fields set rdb$description = 'Valor faturado' where rdb$relation_name = 'GOFATURAMENTO' and rdb$field_name = 'VLFATURADO';

commit;
CREATE GENERATOR G_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER BANCO_GEN FOR BANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_CONTA_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER CONTA_BANCO_GEN FOR CONTA_BANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_CONTA_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_CPG;
commit;
SET TERM  ^^ ;
CREATE TRIGGER CPG_GEN FOR CPG ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_CPG, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPROJETO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPROJETO_GEN FOR GOGRUPOPROJETO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPROJETO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPAINELRISCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPAINELRISCO_GEN FOR GOPAINELRISCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPAINELRISCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOS;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOS_GEN FOR GOPROJETOS ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOS, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOSUPERVISOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOSUPERVISOR_GEN FOR GOSUPERVISOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOSUPERVISOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOTIPOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOTIPOFUNDO_GEN FOR GOTIPOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOTIPOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFATURAMENTO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFATURAMENTO_GEN FOR GOFATURAMENTO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFATURAMENTO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
