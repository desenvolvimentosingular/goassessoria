program gopainelimportacao;

uses
  Vcl.Forms,
  upainelimportacao in 'upainelimportacao.pas' {fpainelimportacao},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  Vcl.Themes,
  Vcl.Styles,
  uEnvioMail in '..\class_shared\uEnvioMail.pas' {FenvioMail},
  uprocessoatualizacao in '..\class_shared\uprocessoatualizacao.pas' {fprocessoatualizacao},
  udtcompetencia in 'udtcompetencia.pas' {fdtcompetencia};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfpainelimportacao, fpainelimportacao);
  Application.Run;
end.
