unit upainelimportacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxGDIPlusClasses, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.Grids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxCurrencyEdit,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, uFuncoes, dxBarBuiltInMenu, cxPC,
  cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, Datasnap.DBClient,
  cxSplitter, Vcl.DialogMessage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, Vcl.Menus, uPesquisa, udm, Data.FMTBcd, Data.SqlExpr;

type
  Tfpainelimportacao = class(TForm)
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    pnlLinhaBotoesgocontrolerecebivel: TPanel;
    spdgravar: TButton;
    pnldados: TPanel;
    pnlgridplanilha: TPanel;
    pnltitulodadosplanilha: TPanel;
    StringGrid: TStringGrid;
    pnlcontroles: TPanel;
    sbpinserir: TButton;
    sbpexcluir: TButton;
    pnlgriddadosimportados: TPanel;
    pnltitulodados: TPanel;
    spdcarregar: TButton;
    dbecaminhoplanilha: TEdit;
    pgcprincipal: TcxPageControl;
    tabcontrolerecebivel: TcxTabSheet;
    tabcpg: TcxTabSheet;
    lblTipoImportacao: TLabel;
    cmbopcaodestino: TcxComboBox;
    Gridcontrolerecebivel: TcxGrid;
    GridcontrolerecebivelDBTV: TcxGridDBTableView;
    GridcontrolerecebivelDBTVCODPORTADOR: TcxGridDBColumn;
    GridcontrolerecebivelDBTVPORTADOR: TcxGridDBColumn;
    GridcontrolerecebivelDBTVDTCOMPETENCIA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVCODCARTEIRA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVCARTEIRA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVCODTIPO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVTIPO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVNUMBORDERO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVQTTITULOS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLBRUTOBORDERO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVQTDRECUSADA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLBRUTORECUSADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLBRUTOFINALBORDERO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVPRAZOMEDIO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVPRAZOMEDIOACORDADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVADVALOREM: TcxGridDBColumn;
    GridcontrolerecebivelDBTVTAC: TcxGridDBColumn;
    GridcontrolerecebivelDBTVTARIFAENTRADATITULOS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVDESAGIOACORDADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVCALCULOIOF: TcxGridDBColumn;
    GridcontrolerecebivelDBTVOUTRASDESPESAS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESPESAS_ENCARGOS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVPERCTAXANOMINALACORDADA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLLIQUIDOCALCULADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVCHECKVLLIQUIDO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLLIQUIDOINFORMADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVSTATUS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLCOBERTURAFORMENTO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADOENCARGOFOMENTO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADORECOMPORATITULO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADODEFASAGEM: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADOCOMISSARIA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADOGARANTIA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADORETENCAODIVIDA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLDESTINADOCONTAGRAFICA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVCUSTOSFORAOPERACAO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLOUTROSCREDITOS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLRECEBIDOCAIXA: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLSALDORECEBER: TcxGridDBColumn;
    GridcontrolerecebivelDBTVDTCREDITO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVTAXAEFETIVADMAIS: TcxGridDBColumn;
    GridcontrolerecebivelDBTVTXCUSTONOMINALDESAGIOACORDADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVTXCUSTONOMINALDESAGIOADVALOREM: TcxGridDBColumn;
    GridcontrolerecebivelDBTVVLCUSTOTITULO: TcxGridDBColumn;
    GridcontrolerecebivelLevel1: TcxGridLevel;
    cdsgocontrolerecebivel: TClientDataSet;
    cdsgocontrolerecebivelCODPORTADOR: TIntegerField;
    cdsgocontrolerecebivelPORTADOR: TStringField;
    cdsgocontrolerecebivelCODFILIAL: TIntegerField;
    cdsgocontrolerecebivelFILIAL: TStringField;
    cdsgocontrolerecebivelDTCOMPETENCIA: TDateField;
    cdsgocontrolerecebivelCODCARTEIRA: TIntegerField;
    cdsgocontrolerecebivelCARTEIRA: TStringField;
    cdsgocontrolerecebivelCODTIPO: TIntegerField;
    cdsgocontrolerecebivelTIPO: TStringField;
    cdsgocontrolerecebivelNUMBORDERO: TFloatField;
    cdsgocontrolerecebivelQTTITULOS: TIntegerField;
    cdsgocontrolerecebivelVLBRUTOBORDERO: TFloatField;
    cdsgocontrolerecebivelQTDRECUSADA: TIntegerField;
    cdsgocontrolerecebivelVLBRUTORECUSADO: TFloatField;
    cdsgocontrolerecebivelVLBRUTOFINALBORDERO: TFloatField;
    cdsgocontrolerecebivelQTFINALBORDERO: TFloatField;
    cdsgocontrolerecebivelPRAZOMEDIO: TFloatField;
    cdsgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField;
    cdsgocontrolerecebivelADVALOREM: TFloatField;
    cdsgocontrolerecebivelTAC: TFloatField;
    cdsgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField;
    cdsgocontrolerecebivelDESAGIOACORDADO: TFloatField;
    cdsgocontrolerecebivelCALCULOIOF: TFloatField;
    cdsgocontrolerecebivelOUTRASDESPESAS: TFloatField;
    cdsgocontrolerecebivelVLDESPESAS_ENCARGOS: TFloatField;
    cdsgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField;
    cdsgocontrolerecebivelVLLIQUIDOCALCULADO: TFloatField;
    cdsgocontrolerecebivelCHECKVLLIQUIDO: TFloatField;
    cdsgocontrolerecebivelVLLIQUIDOINFORMADO: TFloatField;
    cdsgocontrolerecebivelSTATUS: TStringField;
    cdsgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField;
    cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField;
    cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField;
    cdsgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField;
    cdsgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField;
    cdsgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField;
    cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField;
    cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField;
    cdsgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField;
    cdsgocontrolerecebivelVLOUTROSCREDITOS: TFloatField;
    cdsgocontrolerecebivelVLCALCULADOCAIXA: TFloatField;
    cdsgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField;
    cdsgocontrolerecebivelVLSALDORECEBER: TFloatField;
    cdsgocontrolerecebivelDTCREDITO: TDateField;
    cdsgocontrolerecebivelTAXAEFETIVADMAIS: TFloatField;
    cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOACORDADO: TFloatField;
    cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOADVALOREM: TFloatField;
    cdsgocontrolerecebivelVLCUSTOTITULO: TFloatField;
    cdsgocontrolerecebivelMULTIPLICADORDMAIS0: TFloatField;
    cdsgocontrolerecebivelPRAZOMEDIOEMPRESA: TFloatField;
    cdsgocontrolerecebivelMULTIPLICADORPMFLOAT: TFloatField;
    cdsgocontrolerecebivelVLMEDIORECEBIVEIS: TFloatField;
    dsgocontrolerecebivel: TDataSource;
    tabpaineldisponibilidade: TcxTabSheet;
    gridisponibilidade: TcxGrid;
    GriddisponibilidadeDBTV: TcxGridDBTableView;
    cxGridLevedisponibilidade: TcxGridLevel;
    cdsgodisponibilidade: TClientDataSet;
    cdsgodisponibilidadeVLFOMENTOMP: TFloatField;
    cdsgodisponibilidadeVLMOVIMENTODIA: TFloatField;
    cdsgodisponibilidadeVLCAIXAGERAL: TFloatField;
    cdsgodisponibilidadeVLBANCOCONTACORRENTE: TFloatField;
    cdsgodisponibilidadeVLCHEQUES: TFloatField;
    cdsgodisponibilidadeVLDUPLICATAS: TFloatField;
    cdsgodisponibilidadeVLAVISTA: TFloatField;
    cdsgodisponibilidadeVLCARTEIRASIMPLES: TFloatField;
    cdsgodisponibilidadeVLESTMPCOMPRA: TFloatField;
    cdsgodisponibilidadeVLESTMPCOMPRACURVAA: TFloatField;
    cdsgodisponibilidadeVLESTMPCOMPRACURVAB: TFloatField;
    cdsgodisponibilidadeVLESTMPCOMPRACURVAC: TFloatField;
    cdsgodisponibilidadeVLESTMPSEMGIRO: TFloatField;
    cdsgodisponibilidadeVLESTEMCURVAA: TFloatField;
    cdsgodisponibilidadeVLESTEMCURVAB: TFloatField;
    cdsgodisponibilidadeVLESTEMCURVAC: TFloatField;
    cdsgodisponibilidadeVLESTEMSEMGIRO: TFloatField;
    cdsgodisponibilidadeVLESTPACURVAA: TFloatField;
    cdsgodisponibilidadeVLESTPACURVAB: TFloatField;
    cdsgodisponibilidadeVLESTPAFORALINHA: TFloatField;
    cdsgodisponibilidadeVLESTPACURVANICHO: TFloatField;
    cdsgodisponibilidadeVLPAESTLOWPRICE: TFloatField;
    cdsgodisponibilidadeVLESTPATERCEIRO: TFloatField;
    cdsgodisponibilidadeVLLANCAMENTO_PROJETO: TFloatField;
    cdsgodisponibilidadeVLESTTRANSITO: TFloatField;
    cdsgodisponibilidadeVLCONTAPAGARVENCIDO: TFloatField;
    cdsgodisponibilidadeVLIMPOSTOSVENCIDOS: TFloatField;
    cdsgodisponibilidadeVLCONTASPAGAR30: TFloatField;
    cdsgodisponibilidadeVLCONTASPAGAR60: TFloatField;
    cdsgodisponibilidadeVLATIVOS: TFloatField;
    cdsgodisponibilidadeCODPROJETO: TIntegerField;
    cdsgodisponibilidadePROJETO: TStringField;
    cdsgodisponibilidadeCODFILIAL: TIntegerField;
    cdsgodisponibilidadeFILIAL: TStringField;
    cdsgodisponibilidadeVLSALARIOSPJS: TFloatField;
    dsgodisponibilidade: TDataSource;
    sbpdeletarplanilha: TButton;
    GriddisponibilidadeDBTVDTCOMPETENCIA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLFOMENTOMP: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLMOVIMENTODIA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCAIXAGERAL: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLBANCOCONTACORRENTE: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCHEQUES: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLDUPLICATAS: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLAVISTA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCARTEIRASIMPLES: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRACURVAA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRACURVAB: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPCOMPRACURVAC: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTMPSEMGIRO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTEMCURVAA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTEMCURVAB: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTEMCURVAC: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPACURVAA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPACURVAB: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPAFORALINHA: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPACURVANICHO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLPAESTLOWPRICE: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTPATERCEIRO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLLANCAMENTO_PROJETO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLESTTRANSITO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTAPAGARVENCIDO: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLIMPOSTOSVENCIDOS: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTASPAGAR30: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLCONTASPAGAR60: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLATIVOS: TcxGridDBColumn;
    GriddisponibilidadeDBTVVLSALARIOSPJS: TcxGridDBColumn;
    GriddisponibilidadeDBTVCODPROJETO: TcxGridDBColumn;
    GriddisponibilidadeDBTVPROJETO: TcxGridDBColumn;
    GriddisponibilidadeDBTVFILIAL: TcxGridDBColumn;
    GriddisponibilidadeDBTVCODFILIAL: TcxGridDBColumn;
    FDDisponibilidade: TFDQuery;
    cdsgodisponibilidadeDTCOMPETENCIA: TDateTimeField;
    Popupgodisponibilidade: TPopupMenu;
    Popupgocontrolerecebivel: TPopupMenu;
    psqprojeto: TPesquisa;
    pnlLayoutLinhasgocontrolerecebivel4: TPanel;
    pnlLayoutTitulosgocontrolerecebivelcodportador: TPanel;
    pnlLayoutCamposgocontrolerecebivelcodportador: TPanel;
    btnprojeto: TButton;
    pnlLayoutCamposgocontrolerecebivelportador: TPanel;
    dbegodisponibilidadeCODPROJETO: TEdit;
    dbegodisponibilidadePROJETO: TEdit;
    pnlLayoutTitulosgocontrolerecebivelplanilha: TPanel;
    GridResultadoPesquisa: TcxGrid;
    GridResultadoPesquisaDBTV: TcxGridDBTableView;
    GRIDResultadoPesquisaDBTVCODPROJETO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVPROJETO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVID_FORNECEDOR: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVFORNECEDOR: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVID_BANCO_CONTA: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVBANCO_CONTA: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDESCRICAO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDOCUMENTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_BRUTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_MULTA: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_JUROS: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDT_VENCIMENTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVFORMA_PAGAMENTO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVVL_PAGO: TcxGridDBColumn;
    GRIDResultadoPesquisaDBTVDT_PAGAMENTO: TcxGridDBColumn;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    cdscpg: TClientDataSet;
    cdscpgCODPROJETO: TIntegerField;
    cdscpgPROJETO: TStringField;
    cdscpgID_FORNECEDOR: TIntegerField;
    cdscpgFORNECEDOR: TStringField;
    cdscpgDESCRICAO: TStringField;
    cdscpgDOCUMENTO: TStringField;
    cdscpgVL_BRUTO: TFloatField;
    cdscpgVL_MULTA: TFloatField;
    cdscpgVL_JUROS: TFloatField;
    cdscpgVL_DESCONTO: TFloatField;
    cdscpgVL_PAGO: TFloatField;
    cdscpgDT_VENCIMENTO: TDateField;
    cdscpgDT_PAGAMENTO: TDateField;
    cdscpgFORMA_PGTO: TStringField;
    cdscpgID_CONTA_BANCO: TIntegerField;
    cdscpgBANCO_CONTA: TStringField;
    dscpg: TDataSource;
    Popucpg: TPopupMenu;
    FDCPG: TFDQuery;
    FDRecebivel: TFDQuery;
    cdscpgDUPLICADO: TStringField;
    GridResultadoPesquisaDBTVDUPLICADO: TcxGridDBColumn;
    pnlduplicado: TPanel;
    lblduplicado: TLabel;
    cdsgocontrolerecebivelDUPLICADO: TStringField;
    GridcontrolerecebivelDBTVDUPLICADO: TcxGridDBColumn;
    cdsgocontrolerecebivelCODPROJETO: TIntegerField;
    cdsgocontrolerecebivelPROJETO: TStringField;
    cdsgodisponibilidadeDUPLICADO: TStringField;
    GriddisponibilidadeDBTVduplicado: TcxGridDBColumn;
    tabfaturamento: TcxTabSheet;
    Gridgofaturamento: TcxGrid;
    GridgofaturamentoDBTV: TcxGridDBTableView;
    GRIDgofaturamentoDBTVCODPROJETO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVPROJETO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVCODFILIAL: TcxGridDBColumn;
    GRIDgofaturamentoDBTVFILIAL: TcxGridDBColumn;
    GRIDgofaturamentoDBTVCODCLIENTE: TcxGridDBColumn;
    GRIDgofaturamentoDBTVCLIENTE: TcxGridDBColumn;
    GRIDgofaturamentoDBTVPEDIDO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVNF: TcxGridDBColumn;
    GRIDgofaturamentoDBTVVENDEDOR: TcxGridDBColumn;
    GRIDgofaturamentoDBTVDT_LANCAMENTO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVDT_ENTREGA_PREVISTA: TcxGridDBColumn;
    GRIDgofaturamentoDBTVDT_ENTREGA: TcxGridDBColumn;
    GRIDgofaturamentoDBTVSTATUS: TcxGridDBColumn;
    GRIDgofaturamentoDBTVSITUACAO_NF: TcxGridDBColumn;
    GRIDgofaturamentoDBTVCODPRODUTO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVPRODUTO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVGRUPO_PRODUTO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVMARCA: TcxGridDBColumn;
    GRIDgofaturamentoDBTVQT: TcxGridDBColumn;
    GRIDgofaturamentoDBTVUNIDADE: TcxGridDBColumn;
    GRIDgofaturamentoDBTVUNIDADE_VENDA: TcxGridDBColumn;
    GRIDgofaturamentoDBTVPRECO: TcxGridDBColumn;
    GRIDgofaturamentoDBTVVLPEDIDOITEM: TcxGridDBColumn;
    GRIDgofaturamentoDBTVVLCOBRADOITEM: TcxGridDBColumn;
    GRIDgofaturamentoDBTVVLFRETE: TcxGridDBColumn;
    GRIDgofaturamentoDBTVVLFATURADO: TcxGridDBColumn;
    GridgofaturamentoLevel1: TcxGridLevel;
    cdsgofaturamento: TClientDataSet;
    cdsgofaturamentoCODPROJETO: TIntegerField;
    cdsgofaturamentoPROJETO: TStringField;
    cdsgofaturamentoCODFILIAL: TIntegerField;
    cdsgofaturamentoFILIAL: TStringField;
    cdsgofaturamentoCODCLIENTE: TIntegerField;
    cdsgofaturamentoCLIENTE: TStringField;
    cdsgofaturamentoPEDIDO: TIntegerField;
    cdsgofaturamentoNF: TIntegerField;
    cdsgofaturamentoUSUARIO: TStringField;
    cdsgofaturamentoVENDEDOR: TStringField;
    cdsgofaturamentoDT_LANCAMENTO: TDateField;
    cdsgofaturamentoDT_ENTREGA_PREVISTA: TDateField;
    cdsgofaturamentoDT_ENTREGA: TDateField;
    cdsgofaturamentoSTATUS: TStringField;
    cdsgofaturamentoSITUACAO_NF: TStringField;
    cdsgofaturamentoPRODUTO: TStringField;
    cdsgofaturamentoGRUPO_PRODUTO: TStringField;
    cdsgofaturamentoESTILO: TStringField;
    cdsgofaturamentoMARCA: TStringField;
    cdsgofaturamentoQT: TFloatField;
    cdsgofaturamentoUNIDADE: TStringField;
    cdsgofaturamentoUNIDADE_VENDA: TStringField;
    cdsgofaturamentoPRECO: TFloatField;
    cdsgofaturamentoVLPEDIDOITEM: TFloatField;
    cdsgofaturamentoVLCOBRADOITEM: TFloatField;
    cdsgofaturamentoVLFRETE: TFloatField;
    cdsgofaturamentoVLFATURADO: TFloatField;
    dsgofaturamento: TDataSource;
    tabcliente: TcxTabSheet;
    Gridgocliente: TcxGrid;
    GridgoclienteDBTV: TcxGridDBTableView;
    GRIDgoclienteDBTVGRUPO: TcxGridDBColumn;
    GRIDgoclienteDBTVTIPO: TcxGridDBColumn;
    GRIDgoclienteDBTVRAZAO_SOCIAL: TcxGridDBColumn;
    GRIDgoclienteDBTVNOME_FANTASIA: TcxGridDBColumn;
    GRIDgoclienteDBTVCNPJ_CPF: TcxGridDBColumn;
    GRIDgoclienteDBTVUF: TcxGridDBColumn;
    GRIDgoclienteDBTVCEP: TcxGridDBColumn;
    GRIDgoclienteDBTVCIDADE: TcxGridDBColumn;
    GRIDgoclienteDBTVBAIRRO: TcxGridDBColumn;
    GRIDgoclienteDBTVENDERECO: TcxGridDBColumn;
    GRIDgoclienteDBTVNUMERO: TcxGridDBColumn;
    GRIDgoclienteDBTVCOMPLEMENTO: TcxGridDBColumn;
    GridgoclienteLevel1: TcxGridLevel;
    cdsgocliente: TClientDataSet;
    cdsgoclienteGRUPO: TStringField;
    cdsgoclienteTIPO: TStringField;
    cdsgoclienteRAZAO_SOCIAL: TStringField;
    cdsgoclienteNOME_FANTASIA: TStringField;
    cdsgoclienteCNPJ_CPF: TStringField;
    cdsgoclienteUF: TStringField;
    cdsgoclienteCEP: TStringField;
    cdsgoclienteCIDADE: TStringField;
    cdsgoclienteBAIRRO: TStringField;
    cdsgoclienteENDERECO: TStringField;
    cdsgoclienteNUMERO: TStringField;
    cdsgoclienteCOMPLEMENTO: TStringField;
    dsgocliente: TDataSource;
    cdsgofaturamentoDUPLICADO: TStringField;
    cdsgofaturamentoOPERACAO: TStringField;
    cdsgofaturamentoCODPRODUTO: TStringField;
    FDFaturamento: TFDQuery;
    GridgofaturamentoDBTVDUPLICADO: TcxGridDBColumn;
    GridcontrolerecebivelDBTVFILIAL: TcxGridDBColumn;
    tabrisco: TcxTabSheet;
    cdsbase: TClientDataSet;
    dsbase: TDataSource;
    cdsbasePORTADOR: TStringField;
    cdsbaseEMPRESA: TStringField;
    cdsbaseDOCUMENTO: TStringField;
    cdsbaseDTVENCIMENTO: TDateField;
    cdsbaseSACADO: TStringField;
    cdsbaseVALOR: TFloatField;
    cdsbaseSTATUS: TStringField;
    cdsbaseTIPO: TStringField;
    gridrisco: TcxGrid;
    cxGridDBTVRisco: TcxGridDBTableView;
    gridlevelrisco: TcxGridLevel;
    cxGridDBTVRiscoPortador: TcxGridDBColumn;
    cxGridDBTVRiscoEMPRESA: TcxGridDBColumn;
    cxGridDBTVRiscoDOCUMENTO: TcxGridDBColumn;
    cxGridDBTVRiscoSACADO: TcxGridDBColumn;
    cxGridDBTVRiscoVALOR: TcxGridDBColumn;
    cxGridDBTVRiscoSTATUS: TcxGridDBColumn;
    cxGridDBTVRiscoTIPO: TcxGridDBColumn;
    cdsbaseCODPROJETO: TIntegerField;
    cdsbasePROJETO: TStringField;
    cdsbaseCODFILIAL: TIntegerField;
    cdsbaseCODPORTADOR: TIntegerField;
    FDBase: TFDQuery;
    cxGridDBTVRiscoCODPORTADOR: TcxGridDBColumn;
    cxGridDBTVRiscoCODFILIAL: TcxGridDBColumn;
    cdsgodisponibilidadeVLESTPACURVAC: TFloatField;
    cdsgodisponibilidadeVLCONTASPAGARACIMA60: TFloatField;
    cdsgodisponibilidadeVLRENEGOCIACOES: TFloatField;
    lblversao: TLabel;
    spPKG_CONSOLIDA_RISCO: TSQLStoredProc;
    cxGridDBTVRiscodtcompetencia: TcxGridDBColumn;
    cdsbasedtcompetencia: TDateField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spdcarregarClick(Sender: TObject);
    procedure cmbopcaodestinoPropertiesChange(Sender: TObject);
    procedure sbpdeletarplanilhaClick(Sender: TObject);
    procedure sbpinserirClick(Sender: TObject);
    procedure sbpexcluirClick(Sender: TObject);
    procedure spdgravarClick(Sender: TObject);
    procedure dbegodisponibilidadeCODPROJETOKeyPress(Sender: TObject;
      var Key: Char);
    procedure cdsgocontrolerecebivelCalcFields(DataSet: TDataSet);
    procedure cdscpgNewRecord(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cdsgocontrolerecebivelNewRecord(DataSet: TDataSet);
    procedure GridcontrolerecebivelDBTVCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GriddisponibilidadeDBTVCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cdsgodisponibilidadeNewRecord(DataSet: TDataSet);
    procedure GridgofaturamentoDBTVCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cdsgofaturamentoNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    fncimportacao: TFuncoes;

    procedure sessao;
    procedure exportar_painel_disponibilidade;
    procedure exportar_painel_recebiveis;
    procedure exportar_painel_cpg;
    procedure exportar_painel_financeiro;
    procedure exportar_faturamento;
    procedure exportar_base_risco;


    procedure detetar_painel_recebiveis;
    procedure deletar_cpg;
    procedure deletar_painel_disponibilidade;
    procedure deletar_painel_financeiro;
    procedure deletar_faturamento;

    procedure gravar_painel_recebiveis;
    procedure gravar_cpg;
    procedure gravar_painel_disponibilidade;
    procedure gravar_painel_financeiro;
    procedure gravar_faturamento;
    procedure gravar_base;

    function  pesquisar_portador(portador : string): string;

    function  pesquisar_filial(filial : string): string;
    function  pesquisar_carteira(carteira : string): string;
    function  pesquisar_tipo(tipo : string): string;
    function  pesquisar_cliente(cliente : string): string;




  public
    { Public declarations }
  end;


var
  fpainelimportacao: Tfpainelimportacao;



implementation

{$R *.dfm}

uses udtcompetencia, System.IniFiles;

procedure Tfpainelimportacao.cdscpgNewRecord(DataSet: TDataSet);
begin
 cdscpgDUPLICADO.AsString:='N';
end;

procedure Tfpainelimportacao.cdsgocontrolerecebivelCalcFields(
  DataSet: TDataSet);
begin
  cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat              := (cdsgocontrolerecebivelVLBRUTOBORDERO.AsFloat  -   cdsgocontrolerecebivelVLBRUTORECUSADO.AsFloat);
  cdsgocontrolerecebivelQTFINALBORDERO.AsInteger                 := (cdsgocontrolerecebivelQTTITULOS.AsInteger -  cdsgocontrolerecebivelQTDRECUSADA.AsInteger);

  cdsgocontrolerecebivelVLDESPESAS_ENCARGOS.AsFloat              := (cdsgocontrolerecebivelTAC.AsFloat  +
                                                                     cdsgocontrolerecebivelTARIFAENTRADATITULOS.asfloat+
                                                                     cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat+
                                                                     cdsgocontrolerecebivelCALCULOIOF.AsFloat+
                                                                     cdsgocontrolerecebivelADVALOREM.AsFloat+
                                                                     cdsgocontrolerecebivelOUTRASDESPESAS.AsFloat);

  cdsgocontrolerecebivelVLLIQUIDOCALCULADO.AsFloat               := (cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat-
                                                                    cdsgocontrolerecebivelVLDESPESAS_ENCARGOS.AsFloat);

  cdsgocontrolerecebivelVLLIQUIDOINFORMADO.AsFloat               := cdsgocontrolerecebivelVLLIQUIDOCALCULADO.AsFloat;


 cdsgocontrolerecebivelVLSALDORECEBER.AsFloat                    :=(cdsgocontrolerecebivelVLCALCULADOCAIXA.asfloat-
                                                                    cdsgocontrolerecebivelVLRECEBIDOCAIXA.AsFloat);

 cdsgocontrolerecebivelMULTIPLICADORDMAIS0.AsFloat               :=(cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat * cdsgocontrolerecebivelPRAZOMEDIO.AsFloat);

 cdsgocontrolerecebivelMULTIPLICADORPMFLOAT.AsFloat              :=(cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat * cdsgocontrolerecebivelPRAZOMEDIOACORDADO.AsFloat);

 cdsgocontrolerecebivelPRAZOMEDIOEMPRESA.AsFloat                 :=(cdsgocontrolerecebivelMULTIPLICADORDMAIS0.AsFloat/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat);

 cdsgocontrolerecebivelTAXAEFETIVADMAIS.AsFloat                  :=((((cdsgocontrolerecebivelADVALOREM.AsFloat + cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat)/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat)/(cdsgocontrolerecebivelPRAZOMEDIOEMPRESA.AsFloat/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat))*30);

 cdsgocontrolerecebivelVLMEDIORECEBIVEIS.AsFloat                 := (cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat/cdsgocontrolerecebivelQTFINALBORDERO.AsFloat);


 cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOACORDADO.AsFloat     :=(((cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat*cdsgocontrolerecebivelPRAZOMEDIOACORDADO.AsFloat)*30)/ cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat);

 cdsgocontrolerecebivelTXCUSTONOMINALDESAGIOADVALOREM.AsFloat    :=((((cdsgocontrolerecebivelDESAGIOACORDADO.AsFloat + cdsgocontrolerecebivelADVALOREM.AsFloat)/ (cdsgocontrolerecebivelPRAZOMEDIOACORDADO.AsFloat))*30)/cdsgocontrolerecebivelVLBRUTOFINALBORDERO.AsFloat);

 cdsgocontrolerecebivelVLCUSTOTITULO.AsFloat                     :=(cdsgocontrolerecebivelTARIFAENTRADATITULOS.AsFloat/ cdsgocontrolerecebivelQTFINALBORDERO.AsFloat);

end;

procedure Tfpainelimportacao.cdsgocontrolerecebivelNewRecord(DataSet: TDataSet);
begin

 cdsgocontrolerecebivelDUPLICADO.AsString:='N';

end;

procedure Tfpainelimportacao.cdsgodisponibilidadeNewRecord(DataSet: TDataSet);
begin
  cdsgodisponibilidadeduplicado.AsString:='N';
end;

procedure Tfpainelimportacao.cdsgofaturamentoNewRecord(DataSet: TDataSet);
begin
   cdsgofaturamentoduplicado.AsString:='N';
end;

procedure Tfpainelimportacao.cmbopcaodestinoPropertiesChange(Sender: TObject);
begin

   case  cmbopcaodestino.ItemIndex of
    0: pgcprincipal.ActivePageIndex:=0;
    1: pgcprincipal.ActivePageIndex:=1;
    2: pgcprincipal.ActivePageIndex:=2;
    3: pgcprincipal.ActivePageIndex:=3;
    4: pgcprincipal.ActivePageIndex:=4;
    5: pgcprincipal.ActivePageIndex:=5;
   end;

end;

procedure Tfpainelimportacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
 //	fncimportacao.verificarEmTransacao(cdsgocontrolerecebivel);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncimportacao.configurarGridesFormulario(fpainelimportacao,true, false);

	fncimportacao.Free;
	{eliminando container de fun��es da memoria<<<}

	fpainelimportacao:=nil;
	Action:=cafree;

end;

procedure Tfpainelimportacao.FormCreate(Sender: TObject);
begin
  sessao;
  {>>>container de fun��es}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  fncimportacao:= TFuncoes.Create(Self);
  fncimportacao.definirConexao(dm.conexao);
  fncimportacao.definirConexaohistorico(dm.conexao);
  fncimportacao.definirFormulario(Self);
  fncimportacao.validarTransacaoRelacionamento:=true;
  fncimportacao.autoAplicarAtualizacoesBancoDados:=true;
  {container de fun��es<<<}

  fncimportacao.criaAtalhoPopupMenuNavegacao(Popupgocontrolerecebivel,cdsgocontrolerecebivel);

  fncimportacao.criaAtalhoPopupMenuNavegacao(Popucpg,cdscpg);

  fncimportacao.criaAtalhoPopupMenuNavegacao(Popupgodisponibilidade,cdsgodisponibilidade);

	fncimportacao.definirMascaraCampos(cdsgodisponibilidade);

	fncimportacao.configurarGridesFormulario(fpainelimportacao,false, true);

  {<<vers�o da rotina>>}
  lblversao.Caption:=lblversao.Caption+' - '+fncimportacao.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}

end;

procedure Tfpainelimportacao.gravar_painel_disponibilidade;
begin
 try
   if dbegodisponibilidadeCODPROJETO.Text=EmptyStr then
    begin
     application.MessageBox('Um projeto deve ser selecionado.','Campo obrigat�rio.',MB_ICONWARNING);
     abort;
    end;

     cdsgodisponibilidade.First;
     while not cdsgodisponibilidade.Eof do
     begin
      dm.FDQuery.Close;
      dm.FDQuery.sql.Clear;
      dm.FDQuery.SQL.Add('select * from GODISPONIBILIDADE where projeto=:projeto AND dtcompetencia=:dtcompetencia');
      dm.FDQuery.ParamByName('projeto').AsString         := cdsgodisponibilidadeprojeto.AsString;
      dm.FDQuery.ParamByName('dtcompetencia').AsDateTime := cdsgodisponibilidadeDTCOMPETENCIA.AsDateTime;
      dm.FDQuery.Open();

      if not dm.FDQuery.IsEmpty then
      cdsgodisponibilidade.Next;

      FDDisponibilidade.Close;
      FDDisponibilidade.SQL.Clear;
      FDDisponibilidade.sql.add('INSERT INTO GODISPONIBILIDADE (	');
      FDDisponibilidade.sql.add('	DTCOMPETENCIA                    ');
      FDDisponibilidade.sql.add('	,VLFOMENTOMP                     ');
      FDDisponibilidade.sql.add('	,VLMOVIMENTODIA                  ');
      FDDisponibilidade.sql.add('	,VLCAIXAGERAL                    ');
      FDDisponibilidade.sql.add('	,VLBANCOCONTACORRENTE            ');
      FDDisponibilidade.sql.add('	,VLCHEQUES                       ');
      FDDisponibilidade.sql.add('	,VLDUPLICATAS                    ');
      FDDisponibilidade.sql.add('	,VLAVISTA                        ');
      FDDisponibilidade.sql.add('	,VLCARTEIRASIMPLES               ');
      FDDisponibilidade.sql.add('	,VLESTMPCOMPRA                   ');
      FDDisponibilidade.sql.add('	,VLESTMPCOMPRACURVAA             ');
      FDDisponibilidade.sql.add('	,VLESTMPCOMPRACURVAB             ');
      FDDisponibilidade.sql.add('	,VLESTMPCOMPRACURVAC             ');
      FDDisponibilidade.sql.add('	,VLESTMPSEMGIRO                  ');
      FDDisponibilidade.sql.add('	,VLESTEMCURVAA                   ');
      FDDisponibilidade.sql.add('	,VLESTEMCURVAB                   ');
      FDDisponibilidade.sql.add('	,VLESTEMCURVAC                   ');
      FDDisponibilidade.sql.add('	,VLESTEMSEMGIRO                  ');
      FDDisponibilidade.sql.add('	,VLESTPACURVAA                   ');
      FDDisponibilidade.sql.add('	,VLESTPACURVAB                   ');
      FDDisponibilidade.sql.add('	,VLESTPACURVAC                   ');
      FDDisponibilidade.sql.add('	,VLESTPAFORALINHA                ');
      FDDisponibilidade.sql.add('	,VLESTPACURVANICHO               ');
      FDDisponibilidade.sql.add('	,VLPAESTLOWPRICE                 ');
      FDDisponibilidade.sql.add('	,VLESTPATERCEIRO                 ');
      FDDisponibilidade.sql.add('	,VLLANCAMENTO_PROJETO            ');
      FDDisponibilidade.sql.add('	,VLESTTRANSITO                   ');
      FDDisponibilidade.sql.add('	,VLCONTAPAGARVENCIDO             ');
      FDDisponibilidade.sql.add('	,VLIMPOSTOSVENCIDOS              ');
      FDDisponibilidade.sql.add('	,VLCONTASPAGAR30                 ');
      FDDisponibilidade.sql.add('	,VLCONTASPAGAR60                 ');
      FDDisponibilidade.sql.add(', VLCONTASPAGARACIMA60            ');
      FDDisponibilidade.sql.add('	,VLATIVOS                        ');
      FDDisponibilidade.sql.add('	,VLSALARIOSPJS                   ');
      FDDisponibilidade.sql.add('	,VLRENEGOCIACOES                 ');
      FDDisponibilidade.sql.add('	,CODPROJETO                      ');
      FDDisponibilidade.sql.add('	,PROJETO                         ');
      FDDisponibilidade.sql.add('	,CODFILIAL                       ');
      FDDisponibilidade.sql.add('	,FILIAL                          ');
      FDDisponibilidade.sql.add('	)                                ');
      FDDisponibilidade.sql.add('VALUES (                          ');
      FDDisponibilidade.sql.add('	:DTCOMPETENCIA                   ');
      FDDisponibilidade.sql.add('	,:VLFOMENTOMP                    ');
      FDDisponibilidade.sql.add('	,:VLMOVIMENTODIA                 ');
      FDDisponibilidade.sql.add('	,:VLCAIXAGERAL                   ');
      FDDisponibilidade.sql.add('	,:VLBANCOCONTACORRENTE           ');
      FDDisponibilidade.sql.add('	,:VLCHEQUES                      ');
      FDDisponibilidade.sql.add('	,:VLDUPLICATAS                   ');
      FDDisponibilidade.sql.add('	,:VLAVISTA                       ');
      FDDisponibilidade.sql.add('	,:VLCARTEIRASIMPLES              ');
      FDDisponibilidade.sql.add('	,:VLESTMPCOMPRA                  ');
      FDDisponibilidade.sql.add('	,:VLESTMPCOMPRACURVAA            ');
      FDDisponibilidade.sql.add('	,:VLESTMPCOMPRACURVAB            ');
      FDDisponibilidade.sql.add('	,:VLESTMPCOMPRACURVAC            ');
      FDDisponibilidade.sql.add('	,:VLESTMPSEMGIRO                 ');
      FDDisponibilidade.sql.add('	,:VLESTEMCURVAA                  ');
      FDDisponibilidade.sql.add('	,:VLESTEMCURVAB                  ');
      FDDisponibilidade.sql.add('	,:VLESTEMCURVAC                  ');
      FDDisponibilidade.sql.add('	,:VLESTEMSEMGIRO                 ');
      FDDisponibilidade.sql.add('	,:VLESTPACURVAA                  ');
      FDDisponibilidade.sql.add('	,:VLESTPACURVAB                  ');
      FDDisponibilidade.sql.add('	,:VLESTPACURVAC                  ');
      FDDisponibilidade.sql.add('	,:VLESTPAFORALINHA               ');
      FDDisponibilidade.sql.add('	,:VLESTPACURVANICHO              ');
      FDDisponibilidade.sql.add('	,:VLPAESTLOWPRICE                ');
      FDDisponibilidade.sql.add('	,:VLESTPATERCEIRO                ');
      FDDisponibilidade.sql.add('	,:VLLANCAMENTO_PROJETO           ');
      FDDisponibilidade.sql.add('	,:VLESTTRANSITO                  ');
      FDDisponibilidade.sql.add('	,:VLCONTAPAGARVENCIDO            ');
      FDDisponibilidade.sql.add('	,:VLIMPOSTOSVENCIDOS             ');
      FDDisponibilidade.sql.add('	,:VLCONTASPAGAR30                ');
      FDDisponibilidade.sql.add('	,:VLCONTASPAGAR60                ');
      FDDisponibilidade.sql.add(', :VLCONTASPAGARACIMA60           ');
      FDDisponibilidade.sql.add('	,:VLATIVOS                       ');
      FDDisponibilidade.sql.add('	,:VLSALARIOSPJS                  ');
      FDDisponibilidade.sql.add('	,:VLRENEGOCIACOES                ');
      FDDisponibilidade.sql.add('	,:CODPROJETO                     ');
      FDDisponibilidade.sql.add('	,:PROJETO                        ');
      FDDisponibilidade.sql.add('	,:CODFILIAL                      ');
      FDDisponibilidade.sql.add('	,:FILIAL                         ');
      FDDisponibilidade.sql.add('	)                                ');

      FDDisponibilidade.ParamByName('CODPROJETO')          .Asfloat     := cdsgodisponibilidadeCODPROJETO.AsInteger           ;
      FDDisponibilidade.ParamByName('PROJETO')             .asstring     := cdsgodisponibilidadePROJETO.asstring               ;
      FDDisponibilidade.ParamByName('CODFILIAL')           .Asfloat     := cdsgodisponibilidadeCODFILIAL.AsInteger            ;
      FDDisponibilidade.ParamByName('FILIAL')              .asstring     := cdsgodisponibilidadeFILIAL.asstring                ;
      FDDisponibilidade.ParamByName('DTCOMPETENCIA').AsDateTime:=  cdsgodisponibilidadeDTCOMPETENCIA.asDateTime       ;
      FDDisponibilidade.ParamByName('VLFOMENTOMP').Asfloat     := cdsgodisponibilidadeVLFOMENTOMP.AsFloat            ;
      FDDisponibilidade.ParamByName('VLMOVIMENTODIA').Asfloat     := cdsgodisponibilidadeVLMOVIMENTODIA.AsFloat         ;
      FDDisponibilidade.ParamByName('VLCAIXAGERAL').Asfloat     := cdsgodisponibilidadeVLCAIXAGERAL.AsFloat           ;
      FDDisponibilidade.ParamByName('VLBANCOCONTACORRENTE').Asfloat     := cdsgodisponibilidadeVLBANCOCONTACORRENTE.AsFloat   ;
      FDDisponibilidade.ParamByName('VLCHEQUES').Asfloat     := cdsgodisponibilidadeVLCHEQUES.AsFloat              ;
      FDDisponibilidade.ParamByName('VLDUPLICATAS').Asfloat     := cdsgodisponibilidadeVLDUPLICATAS.AsFloat           ;
      FDDisponibilidade.ParamByName('VLAVISTA').Asfloat     := cdsgodisponibilidadeVLAVISTA.AsFloat               ;
      FDDisponibilidade.ParamByName('VLCARTEIRASIMPLES').Asfloat     := cdsgodisponibilidadeVLCARTEIRASIMPLES.AsFloat      ;
      FDDisponibilidade.ParamByName('VLESTMPCOMPRA').Asfloat     := cdsgodisponibilidadeVLESTMPCOMPRA.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTMPCOMPRACURVAA').Asfloat     :=   cdsgodisponibilidadeVLESTMPCOMPRACURVAA.AsFloat    ;
      FDDisponibilidade.ParamByName('VLESTMPCOMPRACURVAB').Asfloat     := cdsgodisponibilidadeVLESTMPCOMPRACURVAB.AsFloat    ;
      FDDisponibilidade.ParamByName('VLESTMPCOMPRACURVAC').Asfloat     := cdsgodisponibilidadeVLESTMPCOMPRACURVAC.AsFloat    ;
      FDDisponibilidade.ParamByName('VLESTMPSEMGIRO').Asfloat     := cdsgodisponibilidadeVLESTMPSEMGIRO.AsFloat         ;
      FDDisponibilidade.ParamByName('VLESTEMCURVAA').Asfloat     := cdsgodisponibilidadeVLESTEMCURVAA.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTEMCURVAB').Asfloat     := cdsgodisponibilidadeVLESTEMCURVAB.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTEMCURVAC').Asfloat     := cdsgodisponibilidadeVLESTEMCURVAC.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTEMSEMGIRO') .Asfloat     := cdsgodisponibilidadeVLESTEMSEMGIRO.AsFloat         ;
      FDDisponibilidade.ParamByName('VLESTPACURVAA')  .Asfloat     := cdsgodisponibilidadeVLESTPACURVAA.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTPACURVAB')  .Asfloat     := cdsgodisponibilidadeVLESTPACURVAB.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTPACURVAC')  .Asfloat     := cdsgodisponibilidadeVLESTPACURVAC.AsFloat          ;
      FDDisponibilidade.ParamByName('VLESTPAFORALINHA')    .Asfloat     := cdsgodisponibilidadeVLESTPAFORALINHA.AsFloat       ;
      FDDisponibilidade.ParamByName('VLESTPACURVANICHO')   .Asfloat     := cdsgodisponibilidadeVLESTPACURVANICHO.AsFloat      ;
      FDDisponibilidade.ParamByName('VLPAESTLOWPRICE')     .Asfloat     := cdsgodisponibilidadeVLPAESTLOWPRICE.AsFloat        ;
      FDDisponibilidade.ParamByName('VLESTPATERCEIRO')     .Asfloat     := cdsgodisponibilidadeVLESTPATERCEIRO.AsFloat        ;
      FDDisponibilidade.ParamByName('VLLANCAMENTO_PROJETO').Asfloat     := cdsgodisponibilidadeVLLANCAMENTO_PROJETO.AsFloat   ;
      FDDisponibilidade.ParamByName('VLESTTRANSITO')       .Asfloat     := cdsgodisponibilidadeVLESTTRANSITO.AsFloat          ;
      FDDisponibilidade.ParamByName('VLCONTAPAGARVENCIDO') .Asfloat     := cdsgodisponibilidadeVLCONTAPAGARVENCIDO.AsFloat    ;
      FDDisponibilidade.ParamByName('VLIMPOSTOSVENCIDOS')  .Asfloat     := cdsgodisponibilidadeVLIMPOSTOSVENCIDOS.AsFloat     ;
      FDDisponibilidade.ParamByName('VLCONTASPAGAR30')     .Asfloat     := cdsgodisponibilidadeVLCONTASPAGAR30.AsFloat        ;
      FDDisponibilidade.ParamByName('VLCONTASPAGAR60')     .Asfloat     := cdsgodisponibilidadeVLCONTASPAGAR60.AsFloat        ;
      FDDisponibilidade.ParamByName('VLCONTASPAGARACIMA60').Asfloat     := cdsgodisponibilidadeVLCONTASPAGARACIMA60.AsFloat   ;
      FDDisponibilidade.ParamByName('VLRENEGOCIACOES')     .Asfloat     := cdsgodisponibilidadeVLRENEGOCIACOES.AsFloat        ;
      FDDisponibilidade.ParamByName('VLSALARIOSPJS')       .Asfloat     := cdsgodisponibilidadeVLSALARIOSPJS.AsFloat        ;
      FDDisponibilidade.ExecSQL;

      cdsgodisponibilidade.Next;
    end;
      application.MessageBox('Dados gravados com sucesso. ','Grva��o dados.', MB_ICONEXCLAMATION);
   except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao gravar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;

end;

procedure Tfpainelimportacao.gravar_painel_financeiro;
begin
  try


   except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao gravar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;
end;

procedure Tfpainelimportacao.gravar_painel_recebiveis;
begin
  try

    if dbegodisponibilidadeCODPROJETO.Text=EmptyStr then
    begin
     application.MessageBox('Um projeto deve ser selecionado.','Campo obrigat�rio.',MB_ICONWARNING);
     abort;
    end;

    cdsgocontrolerecebivel.DisableControls;
    cdsgocontrolerecebivel.First;

    while not cdsgocontrolerecebivel.Eof do
    begin
      {dm.FDQuery.Close;
      dm.FDQuery.sql.Clear;
      dm.FDQuery.SQL.Add('select * from GOCONTROLERECEBIVEL where PORTADOR=:PORTADOR AND FILIAL=:FILIAL AND NUMBORDERO=:NUMBORDERO');
      dm.FDQuery.ParamByName('PORTADOR').AsString:= cdsgocontrolerecebivelPORTADOR.AsString;
      dm.FDQuery.ParamByName('FILIAL').AsString   := cdsgocontrolerecebivelfilial.AsString;
      dm.FDQuery.ParamByName('NUMBORDERO').asInteger:= cdsgocontrolerecebivelNUMBORDERO.asInteger;
      dm.FDQuery.Open();
      if not dm.FDQuery.IsEmpty then
      cdsgocontrolerecebivel.Next; }


      FDRecebivel.Close;
      FDRecebivel.SQL.Clear;
      FDRecebivel.SQL.Add('INSERT INTO GOCONTROLERECEBIVEL(');
      FDRecebivel.SQL.Add('	 CODPROJETO                    ');
      FDRecebivel.SQL.Add('	,PROJETO                       ');
      FDRecebivel.SQL.Add('	,CODPORTADOR                   ');
      FDRecebivel.SQL.Add('	,PORTADOR                      ');
      FDRecebivel.SQL.Add('	,CODFILIAL                     ');
      FDRecebivel.SQL.Add('	,FILIAL                        ');
      FDRecebivel.SQL.Add('	,DTCOMPETENCIA                 ');
      FDRecebivel.SQL.Add('	,CODCARTEIRA                   ');
      FDRecebivel.SQL.Add('	,CARTEIRA                      ');
      FDRecebivel.SQL.Add('	,CODTIPO                       ');
      FDRecebivel.SQL.Add('	,TIPO                          ');
      FDRecebivel.SQL.Add('	,NUMBORDERO                    ');
      FDRecebivel.SQL.Add('	,QTTITULOS                     ');
      FDRecebivel.SQL.Add('	,VLBRUTOBORDERO                ');
      FDRecebivel.SQL.Add('	,QTDRECUSADA                   ');
      FDRecebivel.SQL.Add('	,VLBRUTORECUSADO               ');
      FDRecebivel.SQL.Add('	,PRAZOMEDIO                    ');
      FDRecebivel.SQL.Add('	,PRAZOMEDIOACORDADO            ');
      FDRecebivel.SQL.Add('	,TAC                           ');
      FDRecebivel.SQL.Add('	,TARIFAENTRADATITULOS          ');
      FDRecebivel.SQL.Add('	,ADVALOREM                     ');
      FDRecebivel.SQL.Add('	,DESAGIOACORDADO               ');
      FDRecebivel.SQL.Add('	,CALCULOIOF                    ');
      FDRecebivel.SQL.Add('	,OUTRASDESPESAS                ');
      FDRecebivel.SQL.Add('	,PERCTAXANOMINALACORDADA       ');
      FDRecebivel.SQL.Add('	,VLCOBERTURAFORMENTO           ');
      FDRecebivel.SQL.Add('	,VLDESTINADOENCARGOFOMENTO     ');
      FDRecebivel.SQL.Add('	,VLDESTINADORECOMPORATITULO    ');
      FDRecebivel.SQL.Add('	,VLDESTINADODEFASAGEM          ');
      FDRecebivel.SQL.Add('	,VLDESTINADOCOMISSARIA         ');
      FDRecebivel.SQL.Add('	,VLDESTINADOGARANTIA           ');
      FDRecebivel.SQL.Add('	,VLDESTINADORETENCAODIVIDA     ');
      FDRecebivel.SQL.Add('	,VLDESTINADOCONTAGRAFICA       ');
      FDRecebivel.SQL.Add('	,CUSTOSFORAOPERACAO            ');
      FDRecebivel.SQL.Add('	,VLCALCULADOCAIXA              ');
      FDRecebivel.SQL.Add('	,VLOUTROSCREDITOS              ');
      FDRecebivel.SQL.Add('	,VLRECEBIDOCAIXA               ');
      FDRecebivel.SQL.Add('	,DTCREDITO                     ');
      FDRecebivel.SQL.Add('	)                              ');
      FDRecebivel.SQL.Add('VALUES (	                       ');
      FDRecebivel.SQL.Add('	 :CODPROJETO                   ');
      FDRecebivel.SQL.Add('	,:PROJETO                      ');
      FDRecebivel.SQL.Add(' ,:CODPORTADOR                  ');
      FDRecebivel.SQL.Add('	,:PORTADOR                     ');
      FDRecebivel.SQL.Add('	,:CODFILIAL                    ');
      FDRecebivel.SQL.Add('	,:FILIAL                       ');
      FDRecebivel.SQL.Add('	,:DTCOMPETENCIA                ');
      FDRecebivel.SQL.Add('	,:CODCARTEIRA                  ');
      FDRecebivel.SQL.Add('	,:CARTEIRA                     ');
      FDRecebivel.SQL.Add('	,:CODTIPO                      ');
      FDRecebivel.SQL.Add('	,:TIPO                         ');
      FDRecebivel.SQL.Add('	,:NUMBORDERO                   ');
      FDRecebivel.SQL.Add('	,:QTTITULOS                    ');
      FDRecebivel.SQL.Add('	,:VLBRUTOBORDERO               ');
      FDRecebivel.SQL.Add('	,:QTDRECUSADA                  ');
      FDRecebivel.SQL.Add('	,:VLBRUTORECUSADO              ');
      FDRecebivel.SQL.Add('	,:PRAZOMEDIO                   ');
      FDRecebivel.SQL.Add('	,:PRAZOMEDIOACORDADO           ');
      FDRecebivel.SQL.Add('	,:TAC                          ');
      FDRecebivel.SQL.Add('	,:TARIFAENTRADATITULOS         ');
      FDRecebivel.SQL.Add('	,:ADVALOREM                    ');
      FDRecebivel.SQL.Add('	,:DESAGIOACORDADO              ');
      FDRecebivel.SQL.Add('	,:CALCULOIOF                   ');
      FDRecebivel.SQL.Add('	,:OUTRASDESPESAS               ');
      FDRecebivel.SQL.Add('	,:PERCTAXANOMINALACORDADA      ');
      FDRecebivel.SQL.Add('	,:VLCOBERTURAFORMENTO          ');
      FDRecebivel.SQL.Add('	,:VLDESTINADOENCARGOFOMENTO    ');
      FDRecebivel.SQL.Add('	,:VLDESTINADORECOMPORATITULO   ');
      FDRecebivel.SQL.Add('	,:VLDESTINADODEFASAGEM         ');
      FDRecebivel.SQL.Add('	,:VLDESTINADOCOMISSARIA        ');
      FDRecebivel.SQL.Add('	,:VLDESTINADOGARANTIA          ');
      FDRecebivel.SQL.Add('	,:VLDESTINADORETENCAODIVIDA    ');
      FDRecebivel.SQL.Add('	,:VLDESTINADOCONTAGRAFICA      ');
      FDRecebivel.SQL.Add('	,:CUSTOSFORAOPERACAO           ');
      FDRecebivel.SQL.Add('	,:VLCALCULADOCAIXA             ');
      FDRecebivel.SQL.Add('	,:VLOUTROSCREDITOS             ');
      FDRecebivel.SQL.Add('	,:VLRECEBIDOCAIXA              ');
      FDRecebivel.SQL.Add('	,:DTINDICE                     ');
      FDRecebivel.SQL.Add('	)                              ');
      FDRecebivel.ParamByName('CODPROJETO').asInteger                :=  strtoint(dbegodisponibilidadeCODPROJETO.Text);
      FDRecebivel.ParamByName('PROJETO').asstring                    :=  dbegodisponibilidadePROJETO.Text;
      FDRecebivel.ParamByName('CODPORTADOR').asInteger               :=  cdsgocontrolerecebivelCODPORTADOR               .asinteger;
      FDRecebivel.ParamByName('PORTADOR').asstring                   :=  cdsgocontrolerecebivelPORTADOR                  .asstring;
      FDRecebivel.ParamByName('CODFILIAL').asInteger                 :=  cdsgocontrolerecebivelCODFILIAL                 .asinteger;
      FDRecebivel.ParamByName('FILIAL').asstring                     :=  cdsgocontrolerecebivelFILIAL                    .asstring;
      FDRecebivel.ParamByName('DTCOMPETENCIA').AsDateTime            :=  cdsgocontrolerecebivelDTCOMPETENCIA             .asdatetime;
      FDRecebivel.ParamByName('DTINDICE').AsDateTime                 :=  cdsgocontrolerecebivelDTCOMPETENCIA             .asdatetime;
      FDRecebivel.ParamByName('CODCARTEIRA').AsInteger               :=  cdsgocontrolerecebivelCODCARTEIRA               .asinteger;
      FDRecebivel.ParamByName('CARTEIRA').asstring                   :=  cdsgocontrolerecebivelCARTEIRA                  .asstring;
      FDRecebivel.ParamByName('CODTIPO').AsInteger                   :=  cdsgocontrolerecebivelCODTIPO                   .asinteger;
      FDRecebivel.ParamByName('TIPO').asstring                       :=  cdsgocontrolerecebivelTIPO                      .asstring;
      FDRecebivel.ParamByName('NUMBORDERO').AsFloat                  :=  cdsgocontrolerecebivelNUMBORDERO                .asfloat;
      FDRecebivel.ParamByName('QTTITULOS').AsFloat                   :=  cdsgocontrolerecebivelQTTITULOS                 .asfloat;
      FDRecebivel.ParamByName('VLBRUTOBORDERO').AsFloat              :=  cdsgocontrolerecebivelVLBRUTOBORDERO            .asfloat;
      FDRecebivel.ParamByName('QTDRECUSADA').AsFloat                 :=  cdsgocontrolerecebivelQTDRECUSADA               .asfloat;
      FDRecebivel.ParamByName('VLBRUTORECUSADO').AsFloat             :=  cdsgocontrolerecebivelVLBRUTORECUSADO           .asfloat;
      FDRecebivel.ParamByName('PRAZOMEDIO').AsFloat                  :=  cdsgocontrolerecebivelPRAZOMEDIO                .asfloat;
      FDRecebivel.ParamByName('PRAZOMEDIOACORDADO').AsFloat          :=  cdsgocontrolerecebivelPRAZOMEDIOACORDADO        .asfloat;
      FDRecebivel.ParamByName('TAC').AsFloat                         :=  cdsgocontrolerecebivelTAC                       .asfloat;
      FDRecebivel.ParamByName('TARIFAENTRADATITULOS').AsFloat        :=  cdsgocontrolerecebivelTARIFAENTRADATITULOS      .asfloat;
      FDRecebivel.ParamByName('ADVALOREM').AsFloat                   :=  cdsgocontrolerecebivelADVALOREM                 .asfloat;
      FDRecebivel.ParamByName('DESAGIOACORDADO').AsFloat             :=  cdsgocontrolerecebivelDESAGIOACORDADO           .asfloat;
      FDRecebivel.ParamByName('CALCULOIOF').AsFloat                  :=  cdsgocontrolerecebivelCALCULOIOF                .asfloat;
      FDRecebivel.ParamByName('OUTRASDESPESAS').AsFloat              :=  cdsgocontrolerecebivelOUTRASDESPESAS            .asfloat;
      FDRecebivel.ParamByName('PERCTAXANOMINALACORDADA').AsFloat     :=  cdsgocontrolerecebivelPERCTAXANOMINALACORDADA   .asfloat;
      FDRecebivel.ParamByName('VLCOBERTURAFORMENTO').AsFloat         :=  cdsgocontrolerecebivelVLCOBERTURAFORMENTO       .asfloat;
      FDRecebivel.ParamByName('VLDESTINADOENCARGOFOMENTO').AsFloat   :=  cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO .asfloat;
      FDRecebivel.ParamByName('VLDESTINADORECOMPORATITULO').AsFloat  :=  cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO.asfloat;
      FDRecebivel.ParamByName('VLDESTINADODEFASAGEM').AsFloat        :=  cdsgocontrolerecebivelVLDESTINADODEFASAGEM      .asfloat;
      FDRecebivel.ParamByName('VLDESTINADOCOMISSARIA').AsFloat       :=  cdsgocontrolerecebivelVLDESTINADOCOMISSARIA     .asfloat;
      FDRecebivel.ParamByName('VLDESTINADOGARANTIA').AsFloat         :=  cdsgocontrolerecebivelVLDESTINADOGARANTIA       .asfloat;
      FDRecebivel.ParamByName('VLDESTINADORETENCAODIVIDA').AsFloat   :=  cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA .asfloat;
      FDRecebivel.ParamByName('VLDESTINADOCONTAGRAFICA').AsFloat     :=  cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA   .asfloat;
      FDRecebivel.ParamByName('VLOUTROSCREDITOS').AsFloat            :=  cdsgocontrolerecebivelVLOUTROSCREDITOS          .asfloat;
      FDRecebivel.ParamByName('CUSTOSFORAOPERACAO').AsFloat          :=  cdsgocontrolerecebivelCUSTOSFORAOPERACAO        .asfloat;
      FDRecebivel.ExecSQL;

      cdsgocontrolerecebivel.Next;

    end;

    application.MessageBox('Dados gravados com sucesso.','Grava��o.',MB_OK+64);
   except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao gravar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;
end;


procedure Tfpainelimportacao.GridcontrolerecebivelDBTVCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if (VarAsType(GridcontrolerecebivelDBTV.ViewData.Records[AViewInfo.GridRecord.Index].Values[GridcontrolerecebivelDBTVDUPLICADO.Index], varString  )='S') then
    begin
      Acanvas.Brush.Color := clYellow;
    end
end;

procedure Tfpainelimportacao.GriddisponibilidadeDBTVCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if (VarAsType(GriddisponibilidadeDBTV.ViewData.Records[AViewInfo.GridRecord.Index].Values[GriddisponibilidadeDBTVDUPLICADO.Index], varString  )='S') then
    begin
      Acanvas.Brush.Color := clYellow;
    end
end;

procedure Tfpainelimportacao.GridgofaturamentoDBTVCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if (VarAsType(GridgofaturamentoDBTV.ViewData.Records[AViewInfo.GridRecord.Index].Values[GridgofaturamentoDBTVduplicado.Index], varString  )='S') then
    begin
      Acanvas.Brush.Color := clYellow;
    end;

end;

procedure Tfpainelimportacao.GridResultadoPesquisaDBTVCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
   if (VarAsType(GridResultadoPesquisaDBTV.ViewData.Records[AViewInfo.GridRecord.Index].Values[GridResultadoPesquisaDBTVDUPLICADO.Index], varString  )='S') then
    begin
      Acanvas.Brush.Color := clYellow;
    end
end;

function Tfpainelimportacao.pesquisar_carteira(carteira: string): string;
var sucesso : string;
begin
 try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select coalesce(ID,0)ID FROM gocarteira WHERE DESCRICAO=:DESCRICAO');
    dm.FDQuery.ParamByName('DESCRICAO').asString :=carteira;
    dm.FDQuery.Open();
   if dm.FDQuery.RecordCount=0 then
    sucesso:='0'
   else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end

end;

function Tfpainelimportacao.pesquisar_cliente(cliente: string): string;
var sucesso: string;
begin
   try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select coalesce(ID,0)ID FROM gocliente WHERE   RAZAO_SOCIAL=:RAZAO_SOCIAL;');
    dm.FDQuery.ParamByName('RAZAO_SOCIAL').asString :=cliente;
    dm.FDQuery.Open();
   if dm.FDQuery.RecordCount=0 then
    begin
      dm.FDQuery.Close;
      dm.FDQuery.sql.clear;
      dm.FDQuery.sql.Add('insert into GOCLIENTE (RAZAO_SOCIAL) values(:RAZAO_SOCIAL)');
      dm.FDQuery.ParamByName('RAZAO_SOCIAL').asString :=cliente;
      dm.FDQuery.ExecSQL;

      dm.FDQuery.Close;
      dm.FDQuery.sql.clear;
      dm.FDQuery.sql.Add('select max(ID)ID FROM  GOCLIENTE');
      dm.FDQuery.Open();
      sucesso := dm.FDQuery.FieldByName('ID').asstring;

    end
   else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end
end;

function Tfpainelimportacao.pesquisar_filial(filial: string): string;
var sucesso: string;
begin
   try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select *  FROM gofilial WHERE DESCRICAO=:DESCRICAO');
    dm.FDQuery.ParamByName('DESCRICAO').asString :=filial;
    dm.FDQuery.Open();

    if dm.FDQuery.RecordCount=0 then
    begin
     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('INSERT INTO GOFILIAL (DESCRICAO)');
     dm.FDQuery.sql.Add('VALUES (:DESCRICAO)             ');
     dm.FDQuery.ParamByName('DESCRICAO').AsString  :=filial;
     dm.FDQuery.ExecSQL;

     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('select max(ID)ID FROM  GOFILIAL');
     dm.FDQuery.Open();
     sucesso := dm.FDQuery.FieldByName('ID').asstring;
    end
   else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end
end;

function Tfpainelimportacao.pesquisar_portador(portador: string): string;
var sucesso: string;
begin
   try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select  *  FROM goportador WHERE DESCRICAO=:DESCRICAO');
    dm.FDQuery.ParamByName('DESCRICAO').asString :=portador;
    dm.FDQuery.Open();
    if dm.FDQuery.RecordCount=0 then
    begin
     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('INSERT INTO goportador (DESCRICAO)');
     dm.FDQuery.sql.Add('VALUES (:DESCRICAO)            ');
     dm.FDQuery.ParamByName('DESCRICAO').AsString  :=portador;
     dm.FDQuery.ExecSQL;

     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('select max(ID)ID FROM  goportador');
     dm.FDQuery.Open();
     sucesso := dm.FDQuery.FieldByName('ID').asstring;
    end
    else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end
end;

function Tfpainelimportacao.pesquisar_tipo(tipo: string): string;
var sucesso: string;
begin
   try
    dm.FDQuery.Close;
    dm.FDQuery.sql.clear;
    dm.FDQuery.sql.Add('select coalesce(ID,0)ID FROM gotipo WHERE DESCRICAO=:DESCRICAO');
    dm.FDQuery.ParamByName('DESCRICAO').asString :=tipo;
    dm.FDQuery.Open();
   if dm.FDQuery.RecordCount=0 then
    sucesso:='0'
   else
    sucesso:=  dm.FDQuery.FieldByName('ID').AsString;
   finally
    result := sucesso;
   end
end;

procedure Tfpainelimportacao.gravar_base;
var
Trans : TTransactionDesc;
begin
   try
   if cdsbaseCODPROJETO.Text=EmptyStr then
    begin
     application.MessageBox('Um projeto deve ser selecionado.','Campo obrigat�rio.',MB_ICONWARNING);
     abort;
    end;

     cdsbase.First;
     while not cdsbase.Eof do
     begin
      FDBase.Close;
      FDBase.SQL.Clear;
      FDBase.SQL.Add('INSERT INTO GOBASERISCO ( ');
      FDBase.SQL.Add('  CODPORTADOR             ');
      FDBase.SQL.Add('  ,PORTADOR               ');
      FDBase.SQL.Add('  ,ID_FILIAL              ');
      FDBase.SQL.Add('  ,FILIAL                 ');
      FDBase.SQL.Add('  ,DOCUMENTO              ');
      FDBase.SQL.Add('  ,VENCIMENTO             ');
      FDBase.SQL.Add('  ,dtcompetencia          ');
      FDBase.SQL.Add('  ,SACADO                 ');
      FDBase.SQL.Add('  ,VALOR                  ');
      FDBase.SQL.Add('  ,STATUS                 ');
      FDBase.SQL.Add('   ,TIPO                  ');
      FDBase.SQL.Add('  ,CODPROJETO             ');
      FDBase.SQL.Add('  ,PROJETO                ');
      FDBase.SQL.Add('  )                       ');
      FDBase.SQL.Add('  VALUES (                ');
      FDBase.SQL.Add('   :CODPORTADOR           ');
      FDBase.SQL.Add('  ,:PORTADOR              ');
      FDBase.SQL.Add('  ,:ID_FILIAL             ');
      FDBase.SQL.Add('  ,:FILIAL                ');
      FDBase.SQL.Add('  ,:DOCUMENTO             ');
      FDBase.SQL.Add('  ,:VENCIMENTO            ');
      FDBase.SQL.Add('  ,:dtcompetencia          ');
      FDBase.SQL.Add('  ,:SACADO                ');
      FDBase.SQL.Add('  ,:VALOR                 ');
      FDBase.SQL.Add('  ,:STATUS                ');
      FDBase.SQL.Add('  ,:TIPO                  ');
      FDBase.SQL.Add('  ,:CODPROJETO            ');
      FDBase.SQL.Add('  ,:PROJETO               ');
      FDBase.SQL.Add('  )                       ');
      FDBase.ParamByName('CODPORTADOR').AsInteger :=cdsbaseCODPORTADOR.AsInteger ;
      FDBase.ParamByName('PORTADOR').asstring     :=cdsbasePORTADOR.asstring ;
      FDBase.ParamByName('ID_FILIAL').AsInteger   :=cdsbaseCODFILIAL.AsInteger ;
      FDBase.ParamByName('FILIAL').asstring       :=cdsbaseEMPRESA.asstring ;
      FDBase.ParamByName('DOCUMENTO').asstring    :=cdsbaseDOCUMENTO.asstring ;
      FDBase.ParamByName('VENCIMENTO').AsDateTime :=cdsbaseDTVENCIMENTO.AsDateTime ;
      FDBase.ParamByName('dtcompetencia').AsDateTime :=cdsbasedtcompetencia.AsDateTime ;
      FDBase.ParamByName('SACADO').asstring       :=cdsbaseSACADO.asstring ;
      FDBase.ParamByName('VALOR').AsFloat         :=cdsbaseVALOR.AsFloat ;
      FDBase.ParamByName('STATUS').asstring       :=cdsbaseSTATUS.asstring ;
      FDBase.ParamByName('TIPO').asstring         :=cdsbaseTIPO.asstring ;
      FDBase.ParamByName('CODPROJETO').AsInteger  :=cdsbaseCODPROJETO .AsInteger ;
      FDBase.ParamByName('PROJETO').asstring      :=cdsbasePROJETO.asstring ;
      FDBase.ExecSQL;

      cdsbase.Next;
    end;
     application.MessageBox('Dados gravados com sucesso. ','Grva��o dados.', MB_ICONEXCLAMATION);
     if Application.MessageBox('Deseja consolidar competencia?','Painel de Risco',MB_YESNO+MB_ICONQUESTION)=IDYES then
       begin

       try
          Application.ProcessMessages;
          Trans.TransactionID  := 1;
          Trans.IsolationLevel := xilREADCOMMITTED;
          spPKG_CONSOLIDA_RISCO.sqlConnection.StartTransaction(Trans);
          spPKG_CONSOLIDA_RISCO.Close;
          spPKG_CONSOLIDA_RISCO.ParamByName('DTCOMPETENCIA').AsDate := cdsbasedtcompetencia.AsDateTime;
          spPKG_CONSOLIDA_RISCO.ParamByName('ID_PROJETO').asInteger := strtoInt(dbegodisponibilidadeCODPROJETO.Text);
          spPKG_CONSOLIDA_RISCO.ExecProc;
          spPKG_CONSOLIDA_RISCO.SQLConnection.Commit(Trans);
          application.MessageBox('Applica��o realizada com sucesso.','Consolida��o Compet�ncia', MB_OK+64);

       except

       on Exc:Exception do
        begin
          spPKG_CONSOLIDA_RISCO.sqlConnection.Rollback(Trans);
        end;
       end;
     end;

   except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao gravar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;
end;

procedure Tfpainelimportacao.gravar_cpg;
 label cpg;
begin
  try

   if dbegodisponibilidadeCODPROJETO.Text=EmptyStr then
    begin
     application.MessageBox('Um projeto deve ser selecionado.','Campo obrigat�rio.',MB_ICONWARNING);
     abort;
    end;

    cdscpg.DisableControls;
    cdscpg.First;
    while not cdscpg.Eof do
    begin
      dm.FDQuery.Close;
      dm.FDQuery.sql.Clear;
      dm.FDQuery.sql.Add('select count(*)qt from gocpg where id_fornecedor=:id_fornecedor and documento=:documento and codprojeto=:codprojeto');
      dm.FDQuery.ParamByName('id_fornecedor').AsInteger:= cdscpgID_FORNECEDOR.AsInteger;
      dm.FDQuery.ParamByName('codprojeto').AsInteger   := cdscpgCODPROJETO.AsInteger;
      dm.FDQuery.ParamByName('documento').asString     := cdscpgDOCUMENTO.asString;
      dm.FDQuery.Open();

      if dm.FDQuery.FieldByName('qt').AsInteger>0   then
      begin
       cdscpg.Edit;
       cdscpgDUPLICADO.AsString :='S';
       cdscpg.post;
      end;
      cdscpg.Next;
    end;



   if fncimportacao.filtrar(cdscpg,'UPPER(DUPLICADO) LIKE ''%' + UPPERCASE('S') + '%''') then
     application.MessageBox('Alguns registros j� est�o gravados no goAssessoria. Estes ser�o ignorados.','Grava��o.',MB_OK+64);

    fncimportacao.filtrar(cdscpg,'UPPER(DUPLICADO) LIKE ''%' + UPPERCASE('N') + '%''');
    cdscpg.EnableControls;
    cdscpg.First;
    while not cdscpg.Eof do
    begin

      cpg:
        dm.FDQuery.Close;
        dm.FDQuery.sql.Clear;
        dm.FDQuery.sql.Add('select count(*)qt from gocpg where id_fornecedor=:id_fornecedor and documento=:documento and codprojeto=:codprojeto');
        dm.FDQuery.ParamByName('id_fornecedor').AsInteger:= cdscpgID_FORNECEDOR.AsInteger;
        dm.FDQuery.ParamByName('codprojeto').AsInteger   := cdscpgCODPROJETO.AsInteger;
        dm.FDQuery.ParamByName('documento').asString     := cdscpgDOCUMENTO.asString;
        dm.FDQuery.Open();

       if dm.FDQuery.FieldByName('qt').AsInteger>0   then
       begin
        cdscpg.Next;
        goto cpg;
       end;

       FDCPG.Close;
       FDCPG.SQL.Clear;
       FDCPG.SQL.Add('INSERT INTO GOCPG ( ');
       FDCPG.SQL.Add('   CODPROJETO       ');
       FDCPG.SQL.Add('  ,PROJETO          ');
       FDCPG.SQL.Add('  ,ID_FORNECEDOR    ');
       FDCPG.SQL.Add('  ,FORNECEDOR       ');
       FDCPG.SQL.Add('  ,DESCRICAO        ');
       FDCPG.SQL.Add('  ,DOCUMENTO        ');
       FDCPG.SQL.Add('  ,VL_BRUTO         ');
       FDCPG.SQL.Add('  ,VL_MULTA         ');
       FDCPG.SQL.Add('  ,VL_JUROS         ');
       FDCPG.SQL.Add('  ,VL_DESCONTO      ');
       FDCPG.SQL.Add('  ,DT_VENCIMENTO    ');
       FDCPG.SQL.Add('  ,FORMA_PAGAMENTO  ');
       FDCPG.SQL.Add('  ,VL_PAGO          ');
       FDCPG.SQL.Add('   ,DT_PAGAMENTO    ');
       FDCPG.SQL.Add('  ,ID_BANCO_CONTA   ');
       FDCPG.SQL.Add('  ,BANCO_CONTA      ');
       FDCPG.SQL.Add('  )                 ');
       FDCPG.SQL.Add('VALUES (            ');
       FDCPG.SQL.Add('  :CODPROJETO       ');
       FDCPG.SQL.Add('  ,:PROJETO         ');
       FDCPG.SQL.Add('  ,:ID_FORNECEDOR   ');
       FDCPG.SQL.Add('  ,:FORNECEDOR      ');
       FDCPG.SQL.Add('  ,:DESCRICAO       ');
       FDCPG.SQL.Add('  ,:DOCUMENTO       ');
       FDCPG.SQL.Add('  ,:VL_BRUTO        ');
       FDCPG.SQL.Add('  ,:VL_MULTA        ');
       FDCPG.SQL.Add('  ,:VL_JUROS        ');
       FDCPG.SQL.Add('  ,:VL_DESCONTO     ');
       FDCPG.SQL.Add('  ,:DT_VENCIMENTO   ');
       FDCPG.SQL.Add('  ,:FORMA_PAGAMENTO ');
       FDCPG.SQL.Add('  ,:VL_PAGO         ');
       FDCPG.SQL.Add('  ,:DT_PAGAMENTO    ');
       FDCPG.SQL.Add('  ,:ID_BANCO_CONTA  ');
       FDCPG.SQL.Add('  ,:BANCO_CONTA     ');
       FDCPG.SQL.Add('  )                 ');
       FDCPG.ParamByName('CODPROJETO').AsInteger          := cdscpgCODPROJETO.AsInteger;
       FDCPG.ParamByName('PROJETO').AsString              := cdscpgPROJETO.AsString;
       FDCPG.ParamByName('ID_FORNECEDOR').AsInteger       := cdscpgID_FORNECEDOR.AsInteger;
       FDCPG.ParamByName('FORNECEDOR').asstring           := cdscpgFORNECEDOR.asstring;
       FDCPG.ParamByName('DOCUMENTO').asstring            := cdscpgDOCUMENTO.asstring;
       FDCPG.ParamByName('VL_BRUTO').asfloat              := cdscpgVL_BRUTO.asfloat;
       FDCPG.ParamByName('VL_MULTA').asfloat              := cdscpgVL_MULTA.asfloat;
       FDCPG.ParamByName('VL_JUROS').asfloat              := cdscpgVL_JUROS.asfloat;
       FDCPG.ParamByName('VL_DESCONTO').asfloat           := cdscpgVL_DESCONTO.asfloat;
       FDCPG.ParamByName('DT_VENCIMENTO').AsDateTime      := cdscpgDT_VENCIMENTO.AsDateTime;
       FDCPG.ParamByName('FORMA_PAGAMENTO').asstring      := cdscpgFORMA_PGTO.asstring;
       FDCPG.ParamByName('VL_PAGO').asfloat               := cdscpgVL_PAGO.asfloat;
       FDCPG.ParamByName('DT_PAGAMENTO').AsDateTime       := cdscpgDT_PAGAMENTO.AsDateTime;
       FDCPG.ParamByName('ID_BANCO_CONTA').AsInteger      := cdscpgID_CONTA_BANCO.AsInteger;
       FDCPG.ParamByName('BANCO_CONTA').asstring          := cdscpgBANCO_CONTA.AsString;
       FDCPG.ExecSQL;

     cdscpg.Next;
    end;
    if fncimportacao.filtrar(cdscpg,'UPPER(DUPLICADO) LIKE ''%' + UPPERCASE('N') + '%''') then
     application.MessageBox('Dados gravados com sucesso.','Grava��o.',MB_OK+64)
    else
     application.MessageBox('Nenhum registro novo para ser gravado.','Grava��o.',MB_OK+64);

    cdscpg.Filtered:=false;

   except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao gravar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;
end;

procedure Tfpainelimportacao.gravar_faturamento;
label faturamento;
begin
  try

   if dbegodisponibilidadeCODPROJETO.Text=EmptyStr then
    begin
     application.MessageBox('Um projeto deve ser selecionado.','Campo obrigat�rio.',MB_ICONWARNING);
     abort;
    end;

    cdsgofaturamento.DisableControls;
   { cdsgofaturamento.First;
    while not cdsgofaturamento.Eof do
    begin
      dm.FDQuery.Close;
      dm.FDQuery.sql.Clear;
      dm.FDQuery.sql.Add('select count(*)qt from  gofaturamento where codprojeto=:codprojeto and codfilial=:codfilial and  nf=:nf and CODPRODUTO=:CODPRODUTO');
      dm.FDQuery.ParamByName('codfilial').AsInteger  := cdsgofaturamentoCODFILIAL.AsInteger;
      dm.FDQuery.ParamByName('codprojeto').AsInteger := cdsgofaturamentoCODPROJETO.AsInteger;
      dm.FDQuery.ParamByName('nf').AsInteger         := cdsgofaturamentoNF.AsInteger;
      dm.FDQuery.ParamByName('CODPRODUTO').asString  := cdsgofaturamentoCODPRODUTO.asString;
      dm.FDQuery.Open();

      if dm.FDQuery.FieldByName('qt').AsInteger>0   then
      begin
       cdsgofaturamento.Edit;
       cdsgofaturamentoDUPLICADO.AsString :='S';
       cdsgofaturamento.post;
      end;
      cdsgofaturamento.Next;
    end;   }



   if fncimportacao.filtrar(cdscpg,'UPPER(DUPLICADO) LIKE ''%' + UPPERCASE('S') + '%''') then
     application.MessageBox('Alguns registros j� est�o gravados no goAssessoria. Estes ser�o ignorados.','Grava��o.',MB_OK+64);

    fncimportacao.filtrar(cdsgofaturamento,'UPPER(DUPLICADO) LIKE ''%' + UPPERCASE('N') + '%''');
    cdsgofaturamento.EnableControls;

    cdsgofaturamento.First;
    while not cdsgofaturamento.Eof do
    begin

      dm.FDQuery.Close;
      dm.FDQuery.sql.Clear;
      dm.FDQuery.sql.Add('select count(*)qt from  gofaturamento where codprojeto=:codprojeto and codfilial=:codfilial and  nf=:nf and GRUPO_PRODUTO=:GRUPO_PRODUTO');
      dm.FDQuery.ParamByName('codfilial').AsInteger    := cdsgofaturamentoCODFILIAL.AsInteger;
      dm.FDQuery.ParamByName('codprojeto').AsInteger   := cdsgofaturamentoCODPROJETO.AsInteger;
      dm.FDQuery.ParamByName('nf').AsInteger           := cdsgofaturamentoNF.AsInteger;
      dm.FDQuery.ParamByName('GRUPO_PRODUTO').asString := cdsgofaturamentoGRUPO_PRODUTO.AsString;
      dm.FDQuery.Open();

       if dm.FDQuery.FieldByName('qt').AsInteger=0   then
       begin
          FDFaturamento.Close;
          FDFaturamento.SQL.Clear;
          FDFaturamento.SQL.Add('  INSERT INTO GOFATURAMENTO ( ');
          FDFaturamento.SQL.Add(' CODPROJETO                   ');
          FDFaturamento.SQL.Add(' ,PROJETO                     ');
          FDFaturamento.SQL.Add(' ,CODFILIAL                   ');
          FDFaturamento.SQL.Add(' ,FILIAL                      ');
          FDFaturamento.SQL.Add(' ,CODCLIENTE                  ');
          FDFaturamento.SQL.Add(' ,CLIENTE                     ');
          FDFaturamento.SQL.Add('  ,NF                         ');
          FDFaturamento.SQL.Add(' ,VLFATURADO                  ');
          FDFaturamento.SQL.Add(' ,DT_LANCAMENTO               ');
          FDFaturamento.SQL.Add(' ,GRUPO_PRODUTO               ');
          FDFaturamento.SQL.Add(' )                            ');
          FDFaturamento.SQL.Add('VALUES (                      ');
          FDFaturamento.SQL.Add(' :CODPROJETO                  ');
          FDFaturamento.SQL.Add(' ,:PROJETO                    ');
          FDFaturamento.SQL.Add(' ,:CODFILIAL                  ');
          FDFaturamento.SQL.Add(' ,:FILIAL                     ');
          FDFaturamento.SQL.Add(' ,:CODCLIENTE                 ');
          FDFaturamento.SQL.Add(' ,:CLIENTE                    ');
          FDFaturamento.SQL.Add(' ,:NF                         ');
          FDFaturamento.SQL.Add(' ,:VLFATURADO                 ');
          FDFaturamento.SQL.Add(' ,:DT_LANCAMENTO                ');
          FDFaturamento.SQL.Add(' ,:GRUPO_PRODUTO               ');
          FDFaturamento.SQL.Add(' )                            ');
          FDFaturamento.ParamByName('CODPROJETO').AsInteger            :=   cdsgofaturamento.FieldByName('CODPROJETO').AsInteger;
          FDFaturamento.ParamByName('PROJETO').AsString                :=   cdsgofaturamento.FieldByName('PROJETO').AsString;
          FDFaturamento.ParamByName('CODFILIAL').AsInteger             :=   cdsgofaturamento.FieldByName('CODFILIAL').AsInteger;
          FDFaturamento.ParamByName('FILIAL').AsString                 :=   cdsgofaturamento.FieldByName('FILIAL').AsString;
          FDFaturamento.ParamByName('CODCLIENTE').AsInteger            :=   cdsgofaturamento.FieldByName('CODCLIENTE').AsInteger;
          FDFaturamento.ParamByName('CLIENTE').AsString                :=   cdsgofaturamento.FieldByName('CLIENTE').AsString    ;
          FDFaturamento.ParamByName('GRUPO_PRODUTO').AsString          :=   cdsgofaturamento.FieldByName('GRUPO_PRODUTO').AsString    ;

          FDFaturamento.ParamByName('NF').AsInteger                    :=   cdsgofaturamento.FieldByName('NF').AsInteger       ;
          //       FDFaturamento.ParamByName('USUARIO').AsString                :=   cdsgofaturamento.FieldByName('USUARIO').AsString    ;
          //       FDFaturamento.ParamByName('VENDEDOR').AsString               :=   cdsgofaturamento.FieldByName('VENDEDOR').AsString    ;
          FDFaturamento.ParamByName('DT_LANCAMENTO').AsDateTime        :=   cdsgofaturamento.FieldByName('DT_LANCAMENTO').AsDateTime  ;
          //       FDFaturamento.ParamByName('DT_ENTREGA_PREVISTA').AsDateTime  :=   cdsgofaturamento.FieldByName('DT_ENTREGA_PREVISTA').AsDateTime ;
          //       FDFaturamento.ParamByName('DT_ENTREGA').AsDateTime           :=   cdsgofaturamento.FieldByName('DT_ENTREGA').AsDateTime      ;
          //       FDFaturamento.ParamByName('STATUS').AsString                 :=   cdsgofaturamento.FieldByName('STATUS').AsString         ;
          //       FDFaturamento.ParamByName('SITUACAO_NF').AsString            :=   cdsgofaturamento.FieldByName('SITUACAO_NF').AsString    ;
          //       FDFaturamento.ParamByName('CODPRODUTO').asString             :=   cdsgofaturamento.FieldByName('CODPRODUTO').asString   ;
          //       FDFaturamento.ParamByName('PRODUTO').AsString                :=   cdsgofaturamento.FieldByName('PRODUTO').AsString       ;
          //       FDFaturamento.ParamByName('GRUPO_PRODUTO').AsString          :=   cdsgofaturamento.FieldByName('GRUPO_PRODUTO').AsString  ;
          //       FDFaturamento.ParamByName('ESTILO').AsString                 :=   cdsgofaturamento.FieldByName('ESTILO').AsString      ;
          //       FDFaturamento.ParamByName('MARCA').AsString                  :=   cdsgofaturamento.FieldByName('MARCA').AsString       ;
          //       FDFaturamento.ParamByName('QT').AsFloat                      :=   cdsgofaturamento.FieldByName('QT').AsFloat           ;
          //       FDFaturamento.ParamByName('UNIDADE').AsString                :=   cdsgofaturamento.FieldByName('UNIDADE').AsString     ;
          //       FDFaturamento.ParamByName('UNIDADE_VENDA').AsString          :=   cdsgofaturamento.FieldByName('UNIDADE_VENDA').AsString  ;
          //       FDFaturamento.ParamByName('PRECO').AsFloat                   :=   cdsgofaturamento.FieldByName('PRECO').AsFloat         ;
          //       FDFaturamento.ParamByName('VLPEDIDOITEM').AsFloat            :=   cdsgofaturamento.FieldByName('VLPEDIDOITEM').AsFloat  ;
          //       FDFaturamento.ParamByName('VLCOBRADOITEM').AsFloat           :=   cdsgofaturamento.FieldByName('VLCOBRADOITEM').AsFloat ;
          FDFaturamento.ParamByName('VLFATURADO').AsFloat              :=   cdsgofaturamento.FieldByName('VLFATURADO').AsFloat;
          FDFaturamento.ExecSQL;

       end;



     cdsgofaturamento.Next;
    end;

    if fncimportacao.filtrar(cdsgofaturamento,'UPPER(DUPLICADO) LIKE ''%' + UPPERCASE('N') + '%''') then
     application.MessageBox('Dados gravados com sucesso.','Grava��o.',MB_OK+64)
    else
     application.MessageBox('Nenhum registro novo para ser gravado.','Grava��o.',MB_OK+64);

    cdsgofaturamento.Filtered:=false;

   except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao gravar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;

end;

procedure Tfpainelimportacao.sbpdeletarplanilhaClick(Sender: TObject);
var i : integer;
begin

 if Application.MessageBox('Deseja realmente excluir??','Exclus�o',MB_YESNO+MB_ICONQUESTION)=IDYES then
 begin
   with StringGrid do
   begin
        for I := 0 to ColCount do
        cols[i].Clear;
   end;
    dbecaminhoplanilha.Text:=EmptyStr;
 end;
end;

procedure Tfpainelimportacao.sbpexcluirClick(Sender: TObject);
begin
  case  cmbopcaodestino.ItemIndex of
    0: detetar_painel_recebiveis;
    1: deletar_painel_disponibilidade;
    2: deletar_painel_financeiro;
    3: deletar_cpg;
    4: deletar_faturamento;
   end;

end;

procedure Tfpainelimportacao.sbpinserirClick(Sender: TObject);
begin

 if cmbopcaodestino.ItemIndex=-1 then
 begin
     Application.MessageBox('Voc� selecionar uma das op��es de destino.', 'Exporta��o',MB_ICONEXCLAMATION);
     abort;
 end;


 if Application.MessageBox('Deseja realmente importar a Planilha?','Importa��o',MB_YESNO+MB_ICONQUESTION)=IDNO then
 abort;
 TDialogMessage.ShowWaitMessage('Exportando dados planilha...',
  procedure
  begin

    case  cmbopcaodestino.ItemIndex of
    0: exportar_painel_recebiveis;
    1: exportar_painel_disponibilidade;
    2: exportar_painel_financeiro;
    3: exportar_painel_cpg;
    4: exportar_faturamento;
    5: exportar_base_risco;
   end;
  end);


end;

procedure Tfpainelimportacao.sessao;
var host: string; arquivo: tinifile;
begin
  try
    //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);

    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfpainelimportacao.dbegodisponibilidadeCODPROJETOKeyPress(
  Sender: TObject; var Key: Char);
begin

   if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
   key := #0;

   if key = #8 then
   begin
    dbegodisponibilidadePROJETO.Text  :=EmptyStr;
   end;

end;

procedure Tfpainelimportacao.deletar_painel_disponibilidade;
begin
 if not cdsgodisponibilidade.IsEmpty then
 begin
  if Application.MessageBox('Deseja realmente deletar os lan�amentos do Painel de Disponibilidade?','Exportados.',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;
  cdsgodisponibilidade.EmptyDataSet;
 end;

end;

procedure Tfpainelimportacao.deletar_painel_financeiro;
begin

end;

procedure Tfpainelimportacao.deletar_cpg;
begin

 if not cdscpg.IsEmpty then
 begin
  if Application.MessageBox('Deseja realmente deletar os lan�amentos de Contas a Pagar?','Exportados.',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;
  cdscpg.EmptyDataSet;
 end;

end;

procedure Tfpainelimportacao.deletar_faturamento;
begin
 if not cdsgofaturamento.IsEmpty then
 begin
  if Application.MessageBox('Deseja realmente deletar os faturamentos?','Exportados.',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;
  cdsgofaturamento.EmptyDataSet;
 end;
end;

procedure Tfpainelimportacao.detetar_painel_recebiveis;
begin
 if not cdsgocontrolerecebivel.IsEmpty then
 begin
  if Application.MessageBox('Deseja realmente deletar os lan�amentos do Painel de Disponibilidade?','Exportados.',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;
  cdsgocontrolerecebivel.EmptyDataSet;
 end;

end;

procedure Tfpainelimportacao.exportar_painel_disponibilidade;
 function corrigir_celular(valor : String): double ;
 begin
     if valor=EmptyStr then
     result :=0
     else
     result := StrToFloat(valor);
 end;

var
  i: Integer;
  projeto,   Filial : boolean;
begin

 try
   try

    projeto:= true;
    Filial := true;

    for I := 0 to pred(StringGrid.RowCount) do
    begin
     if i = 0 then
      continue;
     if trim(StringGrid.Cells[0, i]) = EmptyStr then
      continue;
     dm.FDQuery.Close;
     dm.FDQuery.sql.clear;
     dm.FDQuery.sql.Add('select  *  FROM gofilial WHERE DESCRICAO=:DESCRICAO');
     dm.FDQuery.ParamByName('DESCRICAO').asString :=StringGrid.Cells[3, i];
     dm.FDQuery.Open();
     Filial := not dm.FDQuery.IsEmpty;;
    end;

    if (not Filial) then
     application.MessageBox('Filial sem cadastro. ','Grva��o dados.', MB_ICONEXCLAMATION);

    if (not projeto) then
     application.MessageBox('Projeto sem cadastro. ','Grva��o dados.', MB_ICONEXCLAMATION);

    if ((not Filial) or (not projeto )) then exit;

    cdsgodisponibilidade.Open;
    cdsgodisponibilidade.EmptyDataSet;
    cdsgodisponibilidade.DisableControls;
    for I := 0 to pred(StringGrid.RowCount) do
    begin
      if i = 0 then
        continue;
      if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;

      cdsgodisponibilidade.Append;
      cdsgodisponibilidadeCODPROJETO.AsInteger           :=strtoint(StringGrid.Cells[0, i]);
      cdsgodisponibilidadePROJETO.asstring               :=StringGrid.Cells[1, i];
      cdsgodisponibilidadeCODFILIAL.AsInteger            :=strtoint(StringGrid.Cells[2, i]);
      cdsgodisponibilidadeFILIAL.asstring                :=StringGrid.Cells[3, i];
      cdsgodisponibilidadeDTCOMPETENCIA.AsDateTime       :=StrToDateTime(StringGrid.Cells[4, i]);
      cdsgodisponibilidadeVLFOMENTOMP.AsFloat            := corrigir_celular(StringGrid.Cells[5, i]);
      cdsgodisponibilidadeVLMOVIMENTODIA.AsFloat         :=corrigir_celular(StringGrid.Cells[6, i]);
      cdsgodisponibilidadeVLCAIXAGERAL.AsFloat           :=corrigir_celular(StringGrid.Cells[8, i]);
      cdsgodisponibilidadeVLBANCOCONTACORRENTE.AsFloat   :=corrigir_celular(StringGrid.Cells[9, i]);
      cdsgodisponibilidadeVLCHEQUES.AsFloat              :=corrigir_celular(StringGrid.Cells[10, i]);
      cdsgodisponibilidadeVLDUPLICATAS.AsFloat           :=corrigir_celular(StringGrid.Cells[11, i]);
      cdsgodisponibilidadeVLAVISTA.AsFloat               :=corrigir_celular(StringGrid.Cells[12, i]);
      cdsgodisponibilidadeVLCARTEIRASIMPLES.AsFloat      :=corrigir_celular(StringGrid.Cells[13, i]);
      //cdsgodisponibilidadeVLESTMPCOMPRA.AsFloat          :=corrigir_celular(StringGrid.Cells[15, i]);
      cdsgodisponibilidadeVLESTMPCOMPRACURVAA.AsFloat    :=corrigir_celular(StringGrid.Cells[15, i]);
      cdsgodisponibilidadeVLESTMPCOMPRACURVAB.AsFloat    :=corrigir_celular(StringGrid.Cells[16, i]);
      cdsgodisponibilidadeVLESTMPCOMPRACURVAC.AsFloat    :=corrigir_celular(StringGrid.Cells[17, i]);
      //cdsgodisponibilidadeVLESTMPSEMGIRO.AsFloat         :=corrigir_celular(StringGrid.Cells[19, i]);
      cdsgodisponibilidadeVLESTEMCURVAA.AsFloat          :=corrigir_celular(StringGrid.Cells[18, i]);
      cdsgodisponibilidadeVLESTEMCURVAB.AsFloat          :=corrigir_celular(StringGrid.Cells[19, i]);
      cdsgodisponibilidadeVLESTEMCURVAC.AsFloat          :=corrigir_celular(StringGrid.Cells[20, i]);
      //cdsgodisponibilidadeVLESTEMSEMGIRO.AsFloat         :=corrigir_celular(StringGrid.Cells[23, i]);
      cdsgodisponibilidadeVLESTPACURVAA.AsFloat          :=corrigir_celular(StringGrid.Cells[21, i]);
      cdsgodisponibilidadeVLESTPACURVAB.AsFloat          :=corrigir_celular(StringGrid.Cells[22, i]);
      cdsgodisponibilidadeVLESTPACURVAC.AsFloat          :=corrigir_celular(StringGrid.Cells[23, i]);

//      cdsgodisponibilidadeVLESTPAFORALINHA.AsFloat       :=corrigir_celular(StringGrid.Cells[26, i]);
    //  cdsgodisponibilidadeVLESTPACURVANICHO.AsFloat      :=corrigir_celular(StringGrid.Cells[27, i]);
   //   cdsgodisponibilidadeVLPAESTLOWPRICE.AsFloat        :=corrigir_celular(StringGrid.Cells[28, i]);
      cdsgodisponibilidadeVLESTPATERCEIRO.AsFloat        :=corrigir_celular(StringGrid.Cells[24, i]);
      cdsgodisponibilidadeVLESTTRANSITO.AsFloat          :=corrigir_celular(StringGrid.Cells[25, i]);
//      cdsgodisponibilidadeVLLANCAMENTO_PROJETO.AsFloat   :=corrigir_celular(StringGrid.Cells[30, i]);
      cdsgodisponibilidadeVLSALARIOSPJS.AsFloat          :=corrigir_celular(StringGrid.Cells[28, i]);
      cdsgodisponibilidadeVLCONTAPAGARVENCIDO.AsFloat    :=corrigir_celular(StringGrid.Cells[29, i]);
      cdsgodisponibilidadeVLIMPOSTOSVENCIDOS.AsFloat     :=corrigir_celular(StringGrid.Cells[30, i]);
      cdsgodisponibilidadeVLCONTASPAGAR30.AsFloat        :=corrigir_celular(StringGrid.Cells[31, i]);
      cdsgodisponibilidadeVLCONTASPAGAR60.AsFloat        :=corrigir_celular(StringGrid.Cells[32, i]);
      cdsgodisponibilidadeVLCONTASPAGARACIMA60.AsFloat   :=corrigir_celular(StringGrid.Cells[33, i]);
      cdsgodisponibilidadeVLATIVOS.AsFloat               :=corrigir_celular(StringGrid.Cells[36, i]);
      cdsgodisponibilidadeVLRENEGOCIACOES.AsFloat        :=corrigir_celular(StringGrid.Cells[37, i]);

      cdsgodisponibilidade.Post;

//      cdsgodisponibilidade.IndexFieldNames:='CODFILIAL;CODFUNDO;';
//      cdsgodisponibilidade.SetKey;
//      cdsgodisponibilidadeCODPROJETO.AsInteger:= strtoint(dbegodisponibilidadeCODPROJETO.Text);
//      //cdsgodisponibilidadePROJETO.asString    := dbegodisponibilidadePROJETO.Text;
//     // cdsgodisponibilidadeCODFILIAL           .asInteger     :=strtoInt(pesquisar_filial(stringGrid.Cells[2, i]));
//      cdsgodisponibilidadeCODFILIAL           .asInteger       :=1;
//      //cdsgodisponibilidadeFILIAL              .asstring      :=StringGrid.Cells[3, i];
//      cdsgodisponibilidadeDTCOMPETENCIA       .AsDateTime    :=StrToDateTime(StringGrid.Cells[4, i]);
//
//      if cdsgodisponibilidade.GotoKey then
//      begin
//       cdsgodisponibilidade.Edit;
//       cdsgodisponibilidadeDUPLICADO.asString:='S';
//       cdsgodisponibilidade.Post;
//      end;

    end;
  finally
    cdsgodisponibilidade.EnableControls;
    fncimportacao.KillTask('EXCEL.EXE');
  end;

  except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao exportar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;

end;

procedure Tfpainelimportacao.exportar_painel_financeiro;
begin
///
end;

procedure Tfpainelimportacao.exportar_painel_recebiveis;
 function corrigir_celular(valor : String): double ;
 begin
     if valor=EmptyStr then
     result :=0
     else
     result := StrToFloat(valor);
 end;
var i : integer;
    Portador, Filial,qttitulos: Boolean ;
    listaportador, listafilial : string;
begin
try
   try
    Portador:= true;
    Filial := true;
    qttitulos:= true;

     for I := 0 to pred(StringGrid.RowCount) do
      begin
       if i = 0 then
        continue;
       if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;
       if corrigir_celular(StringGrid.Cells[6, i])=0 then qttitulos :=false;

      end;



     for I := 0 to pred(StringGrid.RowCount) do
      begin
       if i = 0 then
        continue;
       if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;
       dm.FDQuery.Close;
       dm.FDQuery.sql.clear;
       dm.FDQuery.sql.Add('select  *  FROM goportador WHERE DESCRICAO=:DESCRICAO');
       dm.FDQuery.ParamByName('DESCRICAO').asString :=StringGrid.Cells[0, i];
       dm.FDQuery.Open();
       Portador := not dm.FDQuery.IsEmpty;
       if dm.FDQuery.IsEmpty then
       listaportador := listaportador +#13+ StringGrid.Cells[0, i];
      end;

      for I := 0 to pred(StringGrid.RowCount) do
      begin
       if i = 0 then
        continue;
       if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;

       dm.FDQuery.Close;
       dm.FDQuery.sql.clear;
       dm.FDQuery.sql.Add('select  *  FROM gofilial WHERE DESCRICAO=:DESCRICAO');
       dm.FDQuery.ParamByName('DESCRICAO').asString :=StringGrid.Cells[4, i];
       dm.FDQuery.Open();
       Filial := not dm.FDQuery.IsEmpty;
       if dm.FDQuery.IsEmpty then
       listafilial := listafilial +#13+ StringGrid.Cells[4, i];
      end;

    if (not Filial) then
     application.MessageBox(pchar('Filial sem cadastro. '+listafilial),'Grva��o dados.', MB_ICONEXCLAMATION);

    if (not portador) then
     application.MessageBox(pchar('Portador sem cadastro. '+listaportador),'Grva��o dados.', MB_ICONEXCLAMATION);

    if (not qttitulos) then
     application.MessageBox(pchar('Coluna com Qtd. de titulos zerado. '+listaportador),'Grva��o dados.', MB_ICONEXCLAMATION);

    if ((not Filial) or (not portador )) then exit;
    if not qttitulos then exit;

    cdsgocontrolerecebivel.Open;
    cdsgocontrolerecebivel.EmptyDataSet;
    cdsgocontrolerecebivel.DisableControls;
    for I := 0 to pred(StringGrid.RowCount) do
    begin
      if i = 0 then
        continue;
      if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;

      cdsgocontrolerecebivel.Append;

      cdsgocontrolerecebivelCODPORTADOR                     .asInteger     :=strtoInt(pesquisar_portador(stringGrid.Cells[0, i]));
      cdsgocontrolerecebivelPORTADOR                        .asstring      :=StringGrid.Cells[0, i];
      cdsgocontrolerecebivelCODFILIAL                       .asInteger     :=strtoInt(pesquisar_filial(stringGrid.Cells[4, i]));
      cdsgocontrolerecebivelFILIAL                          .asstring      :=StringGrid.Cells[4, i];
      cdsgocontrolerecebivelDTCOMPETENCIA                   .AsDateTime    :=StrToDateTime(StringGrid.Cells[3, i]);
      cdsgocontrolerecebivelCODCARTEIRA                     .asInteger     :=strtoInt(pesquisar_carteira(stringGrid.Cells[1, i]));
      cdsgocontrolerecebivelCARTEIRA                        .asstring      :=StringGrid.Cells[1, i];
      cdsgocontrolerecebivelCODTIPO                         .asInteger     :=strtoInt(pesquisar_tipo(stringGrid.Cells[2, i]));
      cdsgocontrolerecebivelTIPO                            .asstring      :=StringGrid.Cells[2, i];
      cdsgocontrolerecebivelNUMBORDERO                      .AsFloat       :=corrigir_celular(StringGrid.Cells[5, i]);
      cdsgocontrolerecebivelQTTITULOS                       .AsFloat       :=corrigir_celular(StringGrid.Cells[6, i]);
      cdsgocontrolerecebivelVLBRUTOBORDERO                  .AsFloat       :=corrigir_celular(StringGrid.Cells[7, i]);
      cdsgocontrolerecebivelQTDRECUSADA                     .AsFloat       :=corrigir_celular(StringGrid.Cells[8, i]);
      cdsgocontrolerecebivelVLBRUTORECUSADO                 .AsFloat       :=corrigir_celular(StringGrid.Cells[9, i]);
      cdsgocontrolerecebivelPRAZOMEDIO                      .AsFloat       :=corrigir_celular(StringGrid.Cells[12, i]);
      cdsgocontrolerecebivelPRAZOMEDIOACORDADO              .AsFloat       :=corrigir_celular(StringGrid.Cells[13, i]);
      cdsgocontrolerecebivelTAC                             .AsFloat       :=corrigir_celular(StringGrid.Cells[14, i]);
      cdsgocontrolerecebivelTARIFAENTRADATITULOS            .AsFloat       :=corrigir_celular(StringGrid.Cells[15, i]);
      cdsgocontrolerecebivelADVALOREM                       .AsFloat       :=corrigir_celular(StringGrid.Cells[16, i]);
      cdsgocontrolerecebivelDESAGIOACORDADO                 .AsFloat       :=corrigir_celular(StringGrid.Cells[17, i]);
      cdsgocontrolerecebivelCALCULOIOF                      .AsFloat       :=corrigir_celular(StringGrid.Cells[18, i]);
      cdsgocontrolerecebivelOUTRASDESPESAS                  .AsFloat       :=corrigir_celular(StringGrid.Cells[19, i]);
      cdsgocontrolerecebivelPERCTAXANOMINALACORDADA         .AsFloat       :=corrigir_celular(StringGrid.Cells[21, i]);
      cdsgocontrolerecebivelVLCOBERTURAFORMENTO             .AsFloat       :=corrigir_celular(StringGrid.Cells[25, i]);
      cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO       .AsFloat       :=corrigir_celular(StringGrid.Cells[26, i]);
      cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO      .AsFloat       :=corrigir_celular(StringGrid.Cells[27, i]);
      cdsgocontrolerecebivelVLDESTINADODEFASAGEM            .AsFloat       :=corrigir_celular(StringGrid.Cells[29, i]);
      cdsgocontrolerecebivelVLDESTINADOCOMISSARIA           .AsFloat       :=corrigir_celular(StringGrid.Cells[28, i]);
      cdsgocontrolerecebivelVLDESTINADOGARANTIA             .AsFloat       :=corrigir_celular(StringGrid.Cells[30, i]);
      cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA       .AsFloat       :=corrigir_celular(StringGrid.Cells[31, i]);
      cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA         .AsFloat       :=corrigir_celular(StringGrid.Cells[32, i]);
      cdsgocontrolerecebivelCUSTOSFORAOPERACAO              .AsFloat       :=corrigir_celular(StringGrid.Cells[33, i]);
      cdsgocontrolerecebivelVLOUTROSCREDITOS                .AsFloat       :=corrigir_celular(StringGrid.Cells[34, i]);
      cdsgocontrolerecebivel.Post;

//      cdsgocontrolerecebivel.IndexFieldNames:='PORTADOR;FILIAL;NUMBORDERO';
//      cdsgocontrolerecebivel.SetKey;
//
//     // cdsgocontrolerecebivelCODPROJETO.AsInteger := strtoint(dbegodisponibilidadeCODPROJETO.Text);
//      cdsgocontrolerecebivelPROJETO.asString     := dbegodisponibilidadePROJETO.Text;
//      //cdsgocontrolerecebivelCODPORTADOR                     .asInteger   :=strtoInt(pesquisar_portador(stringGrid.Cells[0, i]));
//      cdsgocontrolerecebivelPORTADOR                        .asstring      :=StringGrid.Cells[0, i];
//     // cdsgocontrolerecebivelCODFILIAL                       .asInteger   :=strtoInt(pesquisar_filial(stringGrid.Cells[4, i]));
//      cdsgocontrolerecebivelFILIAL                          .asstring      :=StringGrid.Cells[4, i];
//      cdsgocontrolerecebivelDTCOMPETENCIA                   .AsDateTime    :=StrToDateTime(StringGrid.Cells[3, i]);
//
//      if cdsgocontrolerecebivel.GotoKey then
//      begin
//       cdsgocontrolerecebivel.Edit;
//       cdsgocontrolerecebivelDUPLICADO.asString:='S';
//       cdsgocontrolerecebivel.Post;
//      end;
    end;


  finally
    cdsgocontrolerecebivel.EnableControls;
    fncimportacao.KillTask('EXCEL.EXE');
  end;

  except
    on e: exception do
    begin
      application.MessageBox(pchar('Houve problemas ao exportar dados.Motivo: ' + e.Message),'Processo manual', MB_OK + 64);
    end;
  end;
end;

procedure Tfpainelimportacao.exportar_base_risco;
 function corrigir_celular(valor : String): double ;
 begin
     if ((valor=EmptyStr) or  (valor='-') or (valor='R$')) then
      result :=0
     else
      result := StrToFloat(valor);
 end;
var i : integer;
    portador, filial : boolean;

begin
    try

     Portador:= true;
     Filial := true;

     for I := 0 to pred(StringGrid.RowCount) do
      begin
       dm.FDQuery.Close;
       dm.FDQuery.sql.clear;
       dm.FDQuery.sql.Add('select  *  FROM goportador WHERE DESCRICAO=:DESCRICAO');
       dm.FDQuery.ParamByName('DESCRICAO').asString :=StringGrid.Cells[0, i];
       dm.FDQuery.Open();
       Portador := not dm.FDQuery.IsEmpty;
      end;

      for I := 0 to pred(StringGrid.RowCount) do
      begin
       dm.FDQuery.Close;
       dm.FDQuery.sql.clear;
       dm.FDQuery.sql.Add('select  *  FROM gofilial WHERE DESCRICAO=:DESCRICAO');
       dm.FDQuery.ParamByName('DESCRICAO').asString :=StringGrid.Cells[1, i];
       dm.FDQuery.Open();
       Filial :=  not dm.FDQuery.IsEmpty;
      end;

    if (not Filial) then
     application.MessageBox('Filial sem cadastro. ','Grva��o dados.', MB_ICONEXCLAMATION);

    if (not portador) then
     application.MessageBox('Portador sem cadastro. ','Grva��o dados.', MB_ICONEXCLAMATION);

    if ((not Filial) or (not portador )) then exit;

    cdsbase.EmptyDataSet;
    cdsbase.DisableControls;
    for I := 0 to pred(StringGrid.RowCount) do
    begin
      if i = 0 then
        continue;
      if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;

      cdsbase.Append;
      cdsbaseCODPROJETO.AsInteger   := strtoint(dbegodisponibilidadeCODPROJETO.Text);
      cdsbasePROJETO.asstring       := dbegodisponibilidadePROJETO.Text;
      cdsbasePORTADOR.asString      := StringGrid.Cells[0, i];
      cdsbaseCODPORTADOR.AsInteger  :=strtoint(pesquisar_filial(StringGrid.Cells[0, i]));
      cdsbaseEMPRESA.AsString       := StringGrid.Cells[1, i];
      cdsbaseCODFILIAL.AsInteger    := strtoint(pesquisar_filial(StringGrid.Cells[1, i]));

      cdsbaseDOCUMENTO.AsString      :=StringGrid.Cells[2, i];
      cdsbaseDTVENCIMENTO.AsDateTime :=StrToDateTime(StringGrid.Cells[3, i]);
      cdsbaseDTcompetencia.AsDateTime :=StrToDateTime(StringGrid.Cells[4, i]);
      cdsbaseSACADO.AsString         :=StringGrid.Cells[5, i];
      cdsbaseVALOR.AsFloat           :=corrigir_celular(StringGrid.Cells[6, i]);
      cdsbaseSTATUS.AsString         :=StringGrid.Cells[7, i];
      cdsbaseTIPO.AsString           :=StringGrid.Cells[8, i];
      cdsbase.Post;

//      cdsbase.IndexFieldNames:='FILIAL;PORTADOR;CODPROJETO';
//      cdsbase.SetKey;
//      cdsbaseCODPROJETO.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
//      cdsbaseCODFILIAL.AsInteger.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
//      cdsgofaturamentoPEDIDO.AsInteger                := strtoint(StringGrid.Cells[1, i]);
//
//      if cdsgofaturamento.GotoKey then
//      begin
//       cdsgofaturamento.Edit;
//       cdsgofaturamentoDUPLICADO.asString:='S';
//       cdsgofaturamento.Post;
//      end;

    end;
  finally
    cdsbase.EnableControls;
    fncimportacao.KillTask('EXCEL.EXE');
  end;
end;

procedure Tfpainelimportacao.exportar_faturamento;
var i : integer;
     Filial, Grupoproduto  : boolean;
begin
  try

     Filial := true;
     Grupoproduto  := true;

      for I := 0 to pred(StringGrid.RowCount) do
      begin
       dm.FDQuery.Close;
       dm.FDQuery.sql.clear;
       dm.FDQuery.sql.Add('select  *  FROM gofilial WHERE DESCRICAO=:DESCRICAO');
       dm.FDQuery.ParamByName('DESCRICAO').asString := UpperCase(StringGrid.Cells[0, i]);
       dm.FDQuery.Open();
       Filial := not dm.FDQuery.IsEmpty;
      end;

      if (not Filial) then
      application.MessageBox('Filial sem cadastro. ','Grva��o dados.', MB_ICONEXCLAMATION);

      for I := 0 to pred(StringGrid.RowCount) do
      begin
       dm.FDQuery.Close;
       dm.FDQuery.sql.clear;
       dm.FDQuery.sql.Add('select  *  FROM gogrupoproduto WHERE DESCRICAO=:DESCRICAO');
       dm.FDQuery.ParamByName('DESCRICAO').asString := UpperCase(StringGrid.Cells[3, i]);
       dm.FDQuery.Open();
       Grupoproduto := not dm.FDQuery.IsEmpty;
      end;



    if (not Grupoproduto) then
     application.MessageBox('Grupo de produtos sem cadastro. ','Grva��o dados.', MB_ICONEXCLAMATION);


    if (not Filial) then exit;


    cdsgofaturamento.EmptyDataSet;
    cdsgofaturamento.DisableControls;
    for I := 0 to pred(StringGrid.RowCount) do
    begin
      if i = 0 then
        continue;
      if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;

      cdsgofaturamento.Append;
      cdsgofaturamentoCODPROJETO.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
      cdsgofaturamentoPROJETO.asstring                := dbegodisponibilidadePROJETO.Text;
      cdsgofaturamentoFILIAL.AsString                 := StringGrid.Cells[0, i];
      cdsgofaturamentoCODFILIAL.AsInteger             := strtoint(pesquisar_filial(StringGrid.Cells[0, i]));
    //  cdsgofaturamentoPEDIDO.AsInteger                := strtoint(StringGrid.Cells[1, i]);
      cdsgofaturamentoNF.AsInteger                    := strtoint(StringGrid.Cells[1, i]);
      cdsgofaturamentoCODCLIENTE.AsInteger            := strtoint(pesquisar_cliente(StringGrid.Cells[2, i]));
      cdsgofaturamentoCLIENTE.AsString                := StringGrid.Cells[2, i];
     // cdsgofaturamentoVENDEDOR.AsString               := StringGrid.Cells[4, i];
      cdsgofaturamentoDT_LANCAMENTO.asDateTime        := StrToDateTime(StringGrid.Cells[4, i]);
     //cdsgofaturamentoDT_ENTREGA_PREVISTA.AsDateTime  := StrToDateTime( StringGrid.Cells[6, i]);
     { cdsgofaturamentoDT_ENTREGA.AsDateTime           := StrToDateTime(StringGrid.Cells[7, i]);
      cdsgofaturamentoSTATUS.AsString                 := StringGrid.Cells[8, i];
      cdsgofaturamentoSITUACAO_NF.AsString            := StringGrid.Cells[9, i];
      cdsgofaturamentoOPERACAO.AsString               := StringGrid.Cells[10, i];
      cdsgofaturamentoCODPRODUTO.asString             := StringGrid.Cells[11, i];
      cdsgofaturamentoPRODUTO.AsString                := StringGrid.Cells[12, i]; }
      cdsgofaturamentoGRUPO_PRODUTO.AsString          := StringGrid.Cells[3, i];
     { cdsgofaturamentoMARCA.AsString                  := StringGrid.Cells[14, i];
      cdsgofaturamentoQT.AsFloat                      :=StrToFloat(StringGrid.Cells[15, i]);
      cdsgofaturamentoUNIDADE.AsString                := StringGrid.Cells[16, i];
      cdsgofaturamentoPRECO.AsFloat                   := StrToFloat(StringGrid.Cells[17, i]);
      cdsgofaturamentoVLPEDIDOITEM.AsFloat            :=StrToFloat(StringGrid.Cells[18, i]);
      cdsgofaturamentoVLCOBRADOITEM.AsFloat           :=StrToFloat(StringGrid.Cells[19, i]);
      cdsgofaturamentoVLFRETE.AsFloat                 :=StrToFloat(StringGrid.Cells[20, i]);}
      cdsgofaturamentoVLFATURADO.AsFloat              :=StrToFloat(StringGrid.Cells[5, i]);
      cdsgofaturamento.Post;

      {cdsgofaturamento.IndexFieldNames:='CODPROJETO;CODCLIENTE;PEDIDO';
      cdsgofaturamento.SetKey;
      cdsgofaturamentoCODPROJETO.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
      cdsgofaturamentoCODCLIENTE.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
      cdsgofaturamentoPEDIDO.AsInteger                := strtoint(StringGrid.Cells[1, i]);
      if cdsgofaturamento.GotoKey then
      begin
       cdsgofaturamento.Edit;
       cdsgofaturamentoDUPLICADO.asString:='S';
       cdsgofaturamento.Post;
      end;}

    end;
  finally
    cdsgofaturamento.EnableControls;
    fncimportacao.KillTask('EXCEL.EXE');
  end;
end;

procedure Tfpainelimportacao.exportar_painel_cpg;
var i : integer;
begin
try
    cdscpg.EmptyDataSet;
    cdscpg.DisableControls;
    for I := 0 to pred(StringGrid.RowCount) do
    begin
      if i = 0 then
        continue;
      if trim(StringGrid.Cells[0, i]) = EmptyStr then
        continue;

      cdscpg.Append;
      cdscpgCODPROJETO.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
      cdscpgPROJETO.asstring                := dbegodisponibilidadePROJETO.Text;
      if StringGrid.Cells[2, i]<>'' then
      cdscpgID_FORNECEDOR.AsInteger         := strtoint(StringGrid.Cells[2, i])
      else
      cdscpgID_FORNECEDOR.AsInteger         :=0;
      cdscpgFORNECEDOR.asstring             := StringGrid.Cells[3, i];
      cdscpgDESCRICAO.asstring              := StringGrid.Cells[4, i];
      cdscpgDOCUMENTO.AsString              := StringGrid.Cells[5, i];
      cdscpgVL_BRUTO.AsFloat                := StrToFloat(StringGrid.Cells[6, i]);
      cdscpgVL_MULTA.AsFloat                := StrToFloat(StringGrid.Cells[7, i]);
      cdscpgVL_JUROS.AsFloat                := StrToFloat(StringGrid.Cells[8, i]);
      cdscpgVL_DESCONTO.AsFloat             := StrToFloat(StringGrid.Cells[9, i]);
      cdscpgVL_PAGO.AsFloat                 := StrToFloat(StringGrid.Cells[10, i]);
      cdscpgDT_VENCIMENTO.AsDateTime        := StrToDate(StringGrid.Cells[11, i]);
      cdscpgDT_PAGAMENTO.AsDateTime         := StrToDate(StringGrid.Cells[12, i]);
      cdscpgFORMA_PGTO.AsString             := StringGrid.Cells[13, i];
      if StringGrid.Cells[14, i]<>'' then
      cdscpgID_CONTA_BANCO.AsInteger        := strtoint(StringGrid.Cells[14, i])
      else
      cdscpgID_CONTA_BANCO.AsInteger         :=0;
      cdscpgBANCO_CONTA.AsString            := StringGrid.Cells[15, i];
      cdscpg.Post;

      cdscpg.IndexFieldNames:='CODPROJETO;ID_FORNECEDOR;DOCUMENTO';
      cdscpg.SetKey;
      cdscpgCODPROJETO.AsInteger            := strtoint(dbegodisponibilidadeCODPROJETO.Text);
      cdscpgID_FORNECEDOR.AsInteger         := strtoint(StringGrid.Cells[0, i]);
      cdscpgDOCUMENTO.AsString              := StringGrid.Cells[3, i];

      if cdscpg.GotoKey then
      begin
       cdscpg.Edit;
       cdscpgDUPLICADO.asString:='S';
       cdscpg.Post;
      end;

    end;
  finally
    cdscpg.EnableControls;
    fncimportacao.KillTask('EXCEL.EXE');
  end;
end;

procedure Tfpainelimportacao.spdcarregarClick(Sender: TObject);
begin
   TDialogMessage.ShowWaitMessage('Importando planilha...',
  procedure
  begin
   fncimportacao.KillTask('EXCEL.exe');
   dbecaminhoplanilha.Text:=fncimportacao.importartoStringgrid_agrupado(StringGrid);
   fncimportacao.KillTask('EXCEL.exe');
  end);
end;

procedure Tfpainelimportacao.spdgravarClick(Sender: TObject);
begin
  if dbegodisponibilidadeCODPROJETO.Text =EmptyStr then
  begin
   application.MessageBox('� necess�rio escolher o projeto que ser�o grados os dados da planilha.','Campo obrigat�rio',MB_ICONEXCLAMATION);
   dbegodisponibilidadeCODPROJETO.SetFocus;
   abort;
  end;

  if Application.MessageBox('Conferiu os dados importados ? Deseja realmente gravar?','Grva��o dados.',MB_YESNO+MB_ICONQUESTION)=IDNO then
  abort;

  TDialogMessage.ShowWaitMessage('Gravando dados...',
  procedure
  begin
    case  cmbopcaodestino.ItemIndex of
    0: gravar_painel_recebiveis;
    1: gravar_painel_disponibilidade;
    2: gravar_painel_financeiro;
    3: gravar_cpg;
    4: gravar_faturamento;
    5: gravar_base;
   end;

  end);



end;

end.
