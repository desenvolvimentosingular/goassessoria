program historico_usuario;

uses
  Vcl.Forms,
  uusuarios_historico in 'uusuarios_historico.pas' {fusuarios_historico},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  uacesso in '..\class_shared\uacesso.pas' {facesso},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.Themes,
  Vcl.Styles,
  uEnvioMail in '..\class_shared\uEnvioMail.pas' {FenvioMail},
  uprocessoatualizacao in '..\class_shared\uprocessoatualizacao.pas' {fprocessoatualizacao};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfusuarios_historico, fusuarios_historico);
  Application.Run;
end.
