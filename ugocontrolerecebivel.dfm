object fgocontrolerecebivel: Tfgocontrolerecebivel
Left = 0
Top = 0
Name = 'fgocontrolerecebivel'
Caption = 'Gest�o Controle  Receb�veis goAssossoria'
Height = 600
Width = 800
Color = clBtnFace
Font.Charset = DEFAULT_CHARSET
Font.Color = clWindowText
PopupMenu = Popupgocontrolerecebivel
Font.Height = -11
Font.Name = 'Tahoma'
Font.Style = []
OldCreateOrder = False
PixelsPerInch = 96
KeyPreview = True
OnClose = FormClose
OnCreate = FormCreate
OnKeyPress = FormKeyPress
WindowState = wsMaximized
FormStyle = fsMDIChild
TextHeight = 13

object Popupgocontrolerecebivel: TPopupMenu
Left = 574
Top = 20
end

object pnlTitulo: TPanel
Left = 0
Top = 2
Width = 890
Height = 54
Align = alTop
ParentBackground = False
BevelOuter = bvNone
Color = $00B49165
TabOrder = 1

	object lblTitulo: TLabel
	Left = 16
	Top = 8
	Width = 136
	Height = 16
	Caption = 'Gest�o Controle  Receb�veis goAssossoria'
	Color = $00B49165
	Font.Charset = DEFAULT_CHARSET
	Font.Color = clWhite
	Font.Height = -13
	Font.Name = 'Tahoma'
	Font.Style = [fsBold]
	ParentColor = False
	ParentFont = False
	end

	object spdOpcoes: TSpeedButton
	Left = 345
	Top = 5
	Width = 65
	Height = 22
	Hint = 'Op��es extras do formul�rio.'
	 Anchors = [akTop, akleft]            
	Caption = '&Op��es'
	Flat = True
	ParentShowHint = False
	Font.Color = clWhite
	Font.Charset = DEFAULT_CHARSET     
	    Font.Color = clWhite           
	    Font.Height = -11              
	     Font.Name = 'Tahoma'        
	    Font.Style = [fsBold]          
	    ParentFont = False             
	ShowHint = True
	end

	object spdImpressao: TSpeedButton
	Left = 416
	Top = 5
	Width = 65
	Height = 22
	Hint = 'Assistente de impress�o'
	 Anchors = [akTop, akleft]      
	PopupMenu = popupImpressao
	Caption = '&Impress�o'
	Flat = True
	ParentShowHint = False
	Font.Color = clWhite
	Font.Charset = DEFAULT_CHARSET     
	    Font.Color = clWhite           
	    Font.Height = -11              
	     Font.Name = 'Tahoma'        
	    Font.Style = [fsBold]          
	    ParentFont = False             
	ShowHint = True
	OnClick = spdImpressaoClick
	end

	object spdExportacao: TSpeedButton
	Left =  487
	Top = 5
	Width = 65
	Height = 22
	Hint = 'Assistente de exporta��o.'
	 Anchors = [akTop, akleft]      
	Caption = '&Exporta��o'
	Flat = True
	ParentShowHint = False
	Font.Color = clWhite
	Font.Charset = DEFAULT_CHARSET     
	    Font.Color = clWhite           
	    Font.Height = -11              
	     Font.Name = 'Tahoma'        
	    Font.Style = [fsBold]          
	    ParentFont = False             
	ShowHint = True
	end

	object popupImpressao: TPopupMenu
	Left = 454
	Top = 20
		object menuImpRelSintetico: TMenuItem
		Caption = 'Relat�rio Sint�tico'
		OnClick = menuImpRelSinteticoClick
		end
	end
	object pnlLogo: TPanel
	Left = 768
	Top = 0
	Width = 60
	Height = 58
	Align = alRight
	BevelOuter = bvNone
	Color = $00553F2A
	TabOrder = 0

		object imglogo: TImage
		Left = 69
		Top = 7
		Width = 45
		Height = 45
		Align = alclient
		Hint = 'www.singularsolucao.com.br'
		ParentShowHint = False
		ShowHint = True
		end

	end

end

	object pnlLinhaTitulo: TPanel
	Left = 0
	Top = 60
	Width = 890
	Height = 17
	Align = alTop
	Alignment = taLeftJustify
	BevelOuter = bvNone
	Color = $006FD8FF
	ParentBlackground = False
	Font.Charset = DEFAULT_CHARSET
	Font.Color = clWhite
	Font.Height = -11
	Font.Name = 'Tahoma'
	Font.Style = [fsBold]
	ParentFont = False
	TabOrder = 2
		object lblSite: TLabel
		Left = 623
		Top = 0
		Width = 169
		Height = 17
		Align = alRight
		Caption = 'www.singularsolucao.com.br'
		Color = $006FD8FF
		Font.Charset = DEFAULT_CHARSET
		Font.Color = clWhite
		Font.Height = -13
		Font.Name = 'Tahoma'
		Font.Style = [fsbold]
		ParentColor = False
		ParentFont = False
		ExplicitLeft = 619
		ExplicitHeight = 16
		end
	end
	object frxgocontrolerecebivel: TfrxReport
	Version = '4.7.5'
	DotMatrixReport = False
	IniFile = '\Software\Fast Reports'
	PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline,pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
	PreviewOptions.Zoom = 1.000000000000000000
	PrintOptions.Printer = 'Default'
	PrintOptions.PrintOnSheet = 0
	ReportOptions.CreateDate = 40094.709455300920000000
	ReportOptions.LastChange = 40094.709455300920000000
	ScriptLanguage = 'PascalScript'
	ScriptText.Strings = (
	'begin'
	
	'end.')
	Left = 292
	Top = 134
	Datasets = <
	item
	DataSet = frxDBgocontrolerecebivel
	DataSetName = 'frxDBgocontrolerecebivel'
	end>
	Variables = <>
	Style = <>
	end
	object frxDBgocontrolerecebivel: TfrxDBDataset
	UserName = 'frxDBgocontrolerecebivel'
	CloseDataSource = False
	DataSet = cdsgocontrolerecebivel
	Left = 322
	Top = 134
	end
object sqlgocontrolerecebivel: TSQLDataSet
Params = <>
Left = 10
Top = 10
SQLConnection = dm.conexao
DbxCommandType = 'Dbx.SQL'
CommandText = 'select first 0 * from gocontrolerecebivel'
	object sqlgocontrolerecebivelID: TIntegerField
		FieldName = 'ID'
		ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
	end
	object sqlgocontrolerecebivelDT_CADASTRO: TDateField
		FieldName = 'DT_CADASTRO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelHS_CADASTRO: TTimeField
		FieldName = 'HS_CADASTRO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelCODPORTADOR: TIntegerField
		FieldName = 'CODPORTADOR'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelPORTADOR: TStringField
		FieldName = 'PORTADOR'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object sqlgocontrolerecebivelCODFILIAL: TIntegerField
		FieldName = 'CODFILIAL'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelFILIAL: TStringField
		FieldName = 'FILIAL'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object sqlgocontrolerecebivelDTCOMPETENCIA: TDateField
		FieldName = 'DTCOMPETENCIA'
		ProviderFlags = [pfInUpdate]
		Required = True
	end
	object sqlgocontrolerecebivelCODCARTEIRA: TIntegerField
		FieldName = 'CODCARTEIRA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelCARTEIRA: TStringField
		FieldName = 'CARTEIRA'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object sqlgocontrolerecebivelCODTIPO: TIntegerField
		FieldName = 'CODTIPO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelTIPO: TStringField
		FieldName = 'TIPO'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object sqlgocontrolerecebivelNUMBORDERO: TFloatField
		FieldName = 'NUMBORDERO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelQTTITULOS: TIntegerField
		FieldName = 'QTTITULOS'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLBRUTOBORDERO: TFloatField
		FieldName = 'VLBRUTOBORDERO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelQTDRECUSADA: TIntegerField
		FieldName = 'QTDRECUSADA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLBRUTORECUSADO: TFloatField
		FieldName = 'VLBRUTORECUSADO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelPRAZOMEDIO: TFloatField
		FieldName = 'PRAZOMEDIO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField
		FieldName = 'PRAZOMEDIOACORDADO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelTAC: TFloatField
		FieldName = 'TAC'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField
		FieldName = 'TARIFAENTRADATITULOS'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelDESAGIOACORDADO: TFloatField
		FieldName = 'DESAGIOACORDADO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelCALCULOIOF: TFloatField
		FieldName = 'CALCULOIOF'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelOUTRASDESPESAS: TFloatField
		FieldName = 'OUTRASDESPESAS'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField
		FieldName = 'PERCTAXANOMINALACORDADA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField
		FieldName = 'VLCOBERTURAFORMENTO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField
		FieldName = 'VLDESTINADOENCARGOFOMENTO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField
		FieldName = 'VLDESTINADORECOMPORATITULO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField
		FieldName = 'VLDESTINADODEFASAGEM'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField
		FieldName = 'VLDESTINADOCOMISSARIA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField
		FieldName = 'VLDESTINADOGARANTIA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField
		FieldName = 'VLDESTINADORETENCAODIVIDA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField
		FieldName = 'VLDESTINADOCONTAGRAFICA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField
		FieldName = 'CUSTOSFORAOPERACAO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLOUTROSCREDITOS: TFloatField
		FieldName = 'VLOUTROSCREDITOS'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField
		FieldName = 'VLRECEBIDOCAIXA'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelDTCREDITO: TDateField
		FieldName = 'DTCREDITO'
		ProviderFlags = [pfInUpdate]
	end
	object sqlgocontrolerecebivelDTINDICE: TDateField
		FieldName = 'DTINDICE'
		ProviderFlags = [pfInUpdate]
	end
end

object dspgocontrolerecebivel: TDataSetProvider
Left = 40
DataSet = sqlgocontrolerecebivel
UpdateMode = upWhereKeyOnly
Top = 10
end

object cdsgocontrolerecebivel: TClientDataSet
Aggregates = <>
Params = <>
Left = 70
Top = 10
BeforePost = cdsgocontrolerecebivelBeforePost
AfterOpen = cdsgocontrolerecebivelAfterOpen
AfterClose = cdsgocontrolerecebivelAfterClose
ProviderName = 'dspgocontrolerecebivel'
	object cdsgocontrolerecebivelID: TIntegerField
		FieldName = 'ID'
		DisplayLabel = 'C�digo'
		ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
	end
	object cdsgocontrolerecebivelDT_CADASTRO: TDateField
		FieldName = 'DT_CADASTRO'
		DisplayLabel = 'Data de Cadastro'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelHS_CADASTRO: TTimeField
		FieldName = 'HS_CADASTRO'
		DisplayLabel = 'Hora de Cadastro'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelCODPORTADOR: TIntegerField
		FieldName = 'CODPORTADOR'
		DisplayLabel = 'C�d.Portador'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelPORTADOR: TStringField
		FieldName = 'PORTADOR'
		DisplayLabel = 'Portador'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object cdsgocontrolerecebivelCODFILIAL: TIntegerField
		FieldName = 'CODFILIAL'
		DisplayLabel = 'C�d.Filial'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelFILIAL: TStringField
		FieldName = 'FILIAL'
		DisplayLabel = 'Filial'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object cdsgocontrolerecebivelDTCOMPETENCIA: TDateField
		FieldName = 'DTCOMPETENCIA'
		DisplayLabel = 'Data Compet�ncia'
		ProviderFlags = [pfInUpdate]
		Required = True
	end
	object cdsgocontrolerecebivelCODCARTEIRA: TIntegerField
		FieldName = 'CODCARTEIRA'
		DisplayLabel = 'C�d. Carteira'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelCARTEIRA: TStringField
		FieldName = 'CARTEIRA'
		DisplayLabel = 'Carteira'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object cdsgocontrolerecebivelCODTIPO: TIntegerField
		FieldName = 'CODTIPO'
		DisplayLabel = 'C�d.Tipo'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelTIPO: TStringField
		FieldName = 'TIPO'
		DisplayLabel = 'Tipo'
		ProviderFlags = [pfInUpdate]
	Size = 250
	end
	object cdsgocontrolerecebivelNUMBORDERO: TFloatField
		FieldName = 'NUMBORDERO'
		DisplayLabel = 'N� de border�s'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelQTTITULOS: TIntegerField
		FieldName = 'QTTITULOS'
		DisplayLabel = 'Qtd. T�tulos'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLBRUTOBORDERO: TFloatField
		FieldName = 'VLBRUTOBORDERO'
		DisplayLabel = 'Valor bruto border�'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelQTDRECUSADA: TIntegerField
		FieldName = 'QTDRECUSADA'
		DisplayLabel = 'Qtd. Recusada'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLBRUTORECUSADO: TFloatField
		FieldName = 'VLBRUTORECUSADO'
		DisplayLabel = 'Valor Bruto Recusado'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelPRAZOMEDIO: TFloatField
		FieldName = 'PRAZOMEDIO'
		DisplayLabel = 'Prazo M�dio'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelPRAZOMEDIOACORDADO: TFloatField
		FieldName = 'PRAZOMEDIOACORDADO'
		DisplayLabel = 'Prazo M�dio Acordado'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelTAC: TFloatField
		FieldName = 'TAC'
		DisplayLabel = 'TAC'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelTARIFAENTRADATITULOS: TFloatField
		FieldName = 'TARIFAENTRADATITULOS'
		DisplayLabel = 'Tarifa entrada de T�tulos'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelDESAGIOACORDADO: TFloatField
		FieldName = 'DESAGIOACORDADO'
		DisplayLabel = 'Des�gio acordado'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelCALCULOIOF: TFloatField
		FieldName = 'CALCULOIOF'
		DisplayLabel = 'C�lculo IOF sobre Valor L�quido'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelOUTRASDESPESAS: TFloatField
		FieldName = 'OUTRASDESPESAS'
		DisplayLabel = 'Outras Despesas'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelPERCTAXANOMINALACORDADA: TFloatField
		FieldName = 'PERCTAXANOMINALACORDADA'
		DisplayLabel = '% taxa nominal acordada'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLCOBERTURAFORMENTO: TFloatField
		FieldName = 'VLCOBERTURAFORMENTO'
		DisplayLabel = 'Valor Destinado Cobertura Fomento'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TFloatField
		FieldName = 'VLDESTINADOENCARGOFOMENTO'
		DisplayLabel = 'Valor Destinado Encargo Fomento'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADORECOMPORATITULO: TFloatField
		FieldName = 'VLDESTINADORECOMPORATITULO'
		DisplayLabel = 'Valor Destinado Recompra T�tulos'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADODEFASAGEM: TFloatField
		FieldName = 'VLDESTINADODEFASAGEM'
		DisplayLabel = 'Valor Destinado Defasagem'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADOCOMISSARIA: TFloatField
		FieldName = 'VLDESTINADOCOMISSARIA'
		DisplayLabel = 'Valor Destinado Comiss�ria'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADOGARANTIA: TFloatField
		FieldName = 'VLDESTINADOGARANTIA'
		DisplayLabel = 'Valor Destinado Garantia'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADORETENCAODIVIDA: TFloatField
		FieldName = 'VLDESTINADORETENCAODIVIDA'
		DisplayLabel = 'Valor Destinado Reten��o D�vida'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLDESTINADOCONTAGRAFICA: TFloatField
		FieldName = 'VLDESTINADOCONTAGRAFICA'
		DisplayLabel = 'Valor Destinado Conta Gr�fica'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelCUSTOSFORAOPERACAO: TFloatField
		FieldName = 'CUSTOSFORAOPERACAO'
		DisplayLabel = 'Custos fora opera��o(Ex.TED, DOC, ETC)'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLOUTROSCREDITOS: TFloatField
		FieldName = 'VLOUTROSCREDITOS'
		DisplayLabel = 'Valor recebido outros cr�ditos'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelVLRECEBIDOCAIXA: TFloatField
		FieldName = 'VLRECEBIDOCAIXA'
		DisplayLabel = 'Valor Recebido Caixa'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelDTCREDITO: TDateField
		FieldName = 'DTCREDITO'
		DisplayLabel = 'Data Cr�dito'
		ProviderFlags = [pfInUpdate]
	end
	object cdsgocontrolerecebivelDTINDICE: TDateField
		FieldName = 'DTINDICE'
		DisplayLabel = 'Data �ndice'
		ProviderFlags = [pfInUpdate]
	end
end

object dsgocontrolerecebivel: TDataSource
AutoEdit = False
Left = 100
DataSet = cdsgocontrolerecebivel
Top = 10
end

	object pgcPrincipal: TcxPageControl
	Left = 0
	Top = 58
	Width = 657
	Height = 415
	ActivePage = tabPrincipal
	Align = alClient
	TabOrder = 1
	Properties.ActivePage = tabPesquisas  
	Properties.CustomButtons.Buttons = <> 
	Properties.Style = 9                  
	Properties.TabSlants.Positions = []   
	TabSlants.Positions = []              
	object tabPrincipal: TcxTabSheet
	Caption = 'P&rincipal'
	PopupMenu = popupgocontrolerecebivel
	OnShow = altenarPopupTabSheet
		object pnlTituloGridgocontrolerecebivel: TPanel
		Caption = '  Gest�o Controle  Receb�veis goAssossoria'
		Alignment = taLeftJustify
		Left = 0
		Top = 0
		Width = 792
		Height = 19
		Align = alTop
		BevelOuter = bvNone
		Color = $00BD9E79
		end

		object Gridgocontrolerecebivel: TcxGrid
		Left = 0                            
		Top = 19                            
		Width = 700                         
		Height = 300                        
		Align = alClient                    
		TabOrder = 1                        
		LookAndFeel.Kind = lfStandard       
		LookAndFeel.NativeStyle = False     
		ExplicitLeft = 128                  
		ExplicitTop = 112                   
		ExplicitWidth = 250                 
		ExplicitHeight = 200                
		object GridgocontrolerecebivelDBTV: TcxGridDBTableView
		Navigator.Buttons.CustomButtons = <>                        
		Navigator.Buttons.Insert.Enabled = False                    
		Navigator.Buttons.Append.Enabled = False                    
		Navigator.Buttons.Delete.Enabled = False                    
		Navigator.Buttons.Edit.Enabled = False                      
		Navigator.Buttons.Cancel.Enabled = False                    
		Navigator.Visible = false                                   
		DataController.DataSource = dsgocontrolerecebivel
		DataController.Summary.DefaultGroupSummaryItems = <>        
		DataController.Summary.FooterSummaryItems = <>              
		DataController.Summary.SummaryGroups = <>                   
		OptionsData.CancelOnExit = False                            
		OptionsData.Deleting = False                                
		OptionsData.Editing = False                                 
		OptionsData.Inserting = False                               
		OptionsView.NoDataToDisplayInfoText = 'Sem registros'     
		OptionsView.GroupByBox = False                              
		OptionsView.footer = true                                   
		Styles.Background = cxStylepadrao                           
		Styles.Selection = cxStyle1                                 
		object GRIDgocontrolerecebivelDBTVID :TcxGridDBColumn
		DataBinding.FieldName ='ID'
		end
		object GRIDgocontrolerecebivelDBTVDT_CADASTRO :TcxGridDBColumn
		DataBinding.FieldName ='DT_CADASTRO'
		end
		object GRIDgocontrolerecebivelDBTVHS_CADASTRO :TcxGridDBColumn
		DataBinding.FieldName ='HS_CADASTRO'
		end
		object GRIDgocontrolerecebivelDBTVCODPORTADOR :TcxGridDBColumn
		DataBinding.FieldName ='CODPORTADOR'
		end
		object GRIDgocontrolerecebivelDBTVPORTADOR :TcxGridDBColumn
		DataBinding.FieldName ='PORTADOR'
		end
		object GRIDgocontrolerecebivelDBTVDTCOMPETENCIA :TcxGridDBColumn
		DataBinding.FieldName ='DTCOMPETENCIA'
		end
		object GRIDgocontrolerecebivelDBTVCODCARTEIRA :TcxGridDBColumn
		DataBinding.FieldName ='CODCARTEIRA'
		end
		object GRIDgocontrolerecebivelDBTVCARTEIRA :TcxGridDBColumn
		DataBinding.FieldName ='CARTEIRA'
		end
		object GRIDgocontrolerecebivelDBTVCODTIPO :TcxGridDBColumn
		DataBinding.FieldName ='CODTIPO'
		end
		object GRIDgocontrolerecebivelDBTVTIPO :TcxGridDBColumn
		DataBinding.FieldName ='TIPO'
		end
		object GRIDgocontrolerecebivelDBTVNUMBORDERO :TcxGridDBColumn
		DataBinding.FieldName ='NUMBORDERO'
		end
		object GRIDgocontrolerecebivelDBTVQTTITULOS :TcxGridDBColumn
		DataBinding.FieldName ='QTTITULOS'
		end
		object GRIDgocontrolerecebivelDBTVVLBRUTOBORDERO :TcxGridDBColumn
		DataBinding.FieldName ='VLBRUTOBORDERO'
		end
		object GRIDgocontrolerecebivelDBTVQTDRECUSADA :TcxGridDBColumn
		DataBinding.FieldName ='QTDRECUSADA'
		end
		object GRIDgocontrolerecebivelDBTVVLBRUTORECUSADO :TcxGridDBColumn
		DataBinding.FieldName ='VLBRUTORECUSADO'
		end
		end
		end

	end
	object tabgocontrolerecebivel: TcxTabSheet
	Caption = 'Gest�o Controle  Receb�veis goAssossoria'
	PopupMenu = popupgocontrolerecebivel
	OnShow = altenarPopupTabSheet
		object pnlLinhaBotoesgocontrolerecebivel: TPanel
		Left = 0
		Top = 495
		Width = 792
		Height = 25
		Align = alBottom
		BevelOuter = bvNone
		Color = clBtnFace
		TabOrder = 3

			object spdAdicionargocontrolerecebivel: TButton
			Left = 5
			Top = 1
			Width = 65
			Height = 22
			Hint = 'F4 - Adiciona um novo registro.'
			Caption = 'Adicionar'
			Font.Charset = DEFAULT_CHARSET
			Font.Color = clBlack
			Font.Height = -11
			Font.Name = 'Tahoma'
			Font.Style = []
			ParentFont = False
			ParentShowHint = False
			ShowHint = True
			OnClick = spdAdicionargocontrolerecebivelClick
			end

			object spdAlterargocontrolerecebivel: TButton
			Left = 76
			Top = 1
			Width = 65
			Height = 22
			Hint = 'F5 - Alterar registro selecionado.'
			Caption = 'Alterar'
			Font.Charset = DEFAULT_CHARSET
			Font.Color = clBlack
			Font.Height = -11
			Font.Name = 'Tahoma'
			Font.Style = []
			ParentFont = False
			ParentShowHint = False
			ShowHint = True
			OnClick = spdAlterargocontrolerecebivelClick
			end

			object spdGravargocontrolerecebivel: TButton
			Left = 147
			Top = 1
			Width = 65
			Height = 22
			Hint = 'F6 - Gravar altera��es no registro selecionado.'
			Caption = 'Gravar'
			Enabled = False
			Font.Charset = DEFAULT_CHARSET
			Font.Color = clBlack
			Font.Height = -11
			Font.Name = 'Tahoma'
			Font.Style = []
			ParentFont = False
			ParentShowHint = False
			ShowHint = True
			OnClick = spdGravargocontrolerecebivelClick
			end

			object spdCancelargocontrolerecebivel: TButton
			Left = 218
			Top = 1
			Width = 65
			Height = 22
			Hint = 'F7 - Cancela altera��es no registro selecionado.'
			Caption = 'Cancelar'
			Enabled = False
			Font.Charset = DEFAULT_CHARSET
			Font.Color = clBlack
			Font.Height = -11
			Font.Name = 'Tahoma'
			Font.Style = []
			ParentFont = False
			ParentShowHint = False
			ShowHint = True
			OnClick = spdCancelargocontrolerecebivelClick
			end

			object spdExcluirgocontrolerecebivel: TButton
			Left = 289
			Top = 1
			Width = 65
			Height = 22
			Hint = 'CTRL + DEL - Exclui registro selecionado.'
			Caption = 'Excluir'
			Font.Charset = DEFAULT_CHARSET
			Font.Color = clBlack
			Font.Height = -11
			Font.Name = 'Tahoma'
			Font.Style = []
			ParentFont = False
			ParentShowHint = False
		ShowHint = True
			OnClick = spdExcluirgocontrolerecebivelClick
			end

		end

		object SCRgocontrolerecebivel: TScrollBox
		Left = 0
		Top = 257
		Width = 591
		Height = 268
		Align = alClient
		BorderStyle = bsNone
		Color = clBtnFace
		Font.Charset = DEFAULT_CHARSET
		Font.Color = clWindowText
		Font.Height = -11
		Font.Name = 'Tahoma'
		Font.Style = []
		ParentColor = False
		ParentFont = False
		TabOrder = 1
			object pnlDadosgocontrolerecebivel: TPanel
			Left = 0
			Top = 0
			Width = 591
			Enabled = true
			Height = 1482
			Align = alTop
			BevelOuter = bvNone
			Color = clBtnFace
			TabOrder = 0
				object pnlLayoutLinhasgocontrolerecebivel1: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =0

					object pnlLayoutTitulosgocontrolerecebivelid: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  C�digo:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelid: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelID: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'C�digo'
						DataField = 'ID'
						DataSource = dsgocontrolerecebivel
						Enabled = False
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel2: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =1

					object pnlLayoutTitulosgocontrolerecebiveldt_cadastro: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Data de Cadastro:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveldt_cadastro: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelDT_CADASTRO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'Data de Cadastro'
						DataField = 'DT_CADASTRO'
						DataSource = dsgocontrolerecebivel
						Enabled = False
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel3: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =2

					object pnlLayoutTitulosgocontrolerecebivelhs_cadastro: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Hora de Cadastro:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelhs_cadastro: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelHS_CADASTRO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'Hora de Cadastro'
						DataField = 'HS_CADASTRO'
						DataSource = dsgocontrolerecebivel
						Enabled = False
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel4: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =3

					object pnlLayoutTitulosgocontrolerecebivelcodportador: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  C�d.Portador:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcodportador: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCODPORTADOR: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'C�d.Portador'
						DataField = 'CODPORTADOR'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel5: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =4

					object pnlLayoutTitulosgocontrolerecebivelportador: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Portador:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelportador: TPanel
					Left = 0
					Top = 0
					Width = 490
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelPORTADOR: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 480
						TabOrder = 0
						ShowHint = True
						Hint = 'Portador'
						DataField = 'PORTADOR'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel6: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =5

					object pnlLayoutTitulosgocontrolerecebivelcodfilial: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  C�d.Filial:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcodfilial: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCODFILIAL: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'C�d.Filial'
						DataField = 'CODFILIAL'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel7: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =6

					object pnlLayoutTitulosgocontrolerecebivelfilial: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Filial:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelfilial: TPanel
					Left = 0
					Top = 0
					Width = 490
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelFILIAL: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 480
						TabOrder = 0
						ShowHint = True
						Hint = 'Filial'
						DataField = 'FILIAL'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel8: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =7

					object pnlLayoutTitulosgocontrolerecebiveldtcompetencia: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Data Compet�ncia:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveldtcompetencia: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelDTCOMPETENCIA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						 Color = 15722462
						TabOrder = 0
						ShowHint = True
						Hint = 'Data Compet�ncia'
						DataField = 'DTCOMPETENCIA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel9: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =8

					object pnlLayoutTitulosgocontrolerecebivelcodcarteira: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  C�d. Carteira:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcodcarteira: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCODCARTEIRA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'C�d. Carteira'
						DataField = 'CODCARTEIRA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel10: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =9

					object pnlLayoutTitulosgocontrolerecebivelcarteira: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Carteira:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcarteira: TPanel
					Left = 0
					Top = 0
					Width = 490
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCARTEIRA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 480
						TabOrder = 0
						ShowHint = True
						Hint = 'Carteira'
						DataField = 'CARTEIRA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel11: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =10

					object pnlLayoutTitulosgocontrolerecebivelcodtipo: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  C�d.Tipo:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcodtipo: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCODTIPO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'C�d.Tipo'
						DataField = 'CODTIPO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel12: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =11

					object pnlLayoutTitulosgocontrolerecebiveltipo: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Tipo:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveltipo: TPanel
					Left = 0
					Top = 0
					Width = 490
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelTIPO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 480
						TabOrder = 0
						ShowHint = True
						Hint = 'Tipo'
						DataField = 'TIPO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel13: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =12

					object pnlLayoutTitulosgocontrolerecebivelnumbordero: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  N� de border�s:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelnumbordero: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelNUMBORDERO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'N� de border�s'
						DataField = 'NUMBORDERO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel14: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =13

					object pnlLayoutTitulosgocontrolerecebivelqttitulos: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Qtd. T�tulos:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelqttitulos: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelQTTITULOS: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'Qtd. T�tulos'
						DataField = 'QTTITULOS'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel15: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =14

					object pnlLayoutTitulosgocontrolerecebivelvlbrutobordero: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor bruto border�:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvlbrutobordero: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLBRUTOBORDERO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor bruto border�'
						DataField = 'VLBRUTOBORDERO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel16: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =15

					object pnlLayoutTitulosgocontrolerecebivelqtdrecusada: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Qtd. Recusada:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelqtdrecusada: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelQTDRECUSADA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'Qtd. Recusada'
						DataField = 'QTDRECUSADA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel17: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =16

					object pnlLayoutTitulosgocontrolerecebivelvlbrutorecusado: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Bruto Recusado:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvlbrutorecusado: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLBRUTORECUSADO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Bruto Recusado'
						DataField = 'VLBRUTORECUSADO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel18: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =17

					object pnlLayoutTitulosgocontrolerecebivelprazomedio: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Prazo M�dio:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelprazomedio: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelPRAZOMEDIO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Prazo M�dio'
						DataField = 'PRAZOMEDIO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel19: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =18

					object pnlLayoutTitulosgocontrolerecebivelprazomedioacordado: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Prazo M�dio Acordado:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelprazomedioacordado: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelPRAZOMEDIOACORDADO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Prazo M�dio Acordado'
						DataField = 'PRAZOMEDIOACORDADO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel20: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =19

					object pnlLayoutTitulosgocontrolerecebiveltac: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  TAC:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveltac: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelTAC: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'TAC'
						DataField = 'TAC'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel21: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =20

					object pnlLayoutTitulosgocontrolerecebiveltarifaentradatitulos: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Tarifa entrada de T�tulos:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveltarifaentradatitulos: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelTARIFAENTRADATITULOS: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Tarifa entrada de T�tulos'
						DataField = 'TARIFAENTRADATITULOS'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel22: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =21

					object pnlLayoutTitulosgocontrolerecebiveldesagioacordado: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Des�gio acordado:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveldesagioacordado: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelDESAGIOACORDADO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Des�gio acordado'
						DataField = 'DESAGIOACORDADO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel23: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =22

					object pnlLayoutTitulosgocontrolerecebivelcalculoiof: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  C�lculo IOF sobre Valor L�quido:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcalculoiof: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCALCULOIOF: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'C�lculo IOF sobre Valor L�quido'
						DataField = 'CALCULOIOF'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel24: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =23

					object pnlLayoutTitulosgocontrolerecebiveloutrasdespesas: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Outras Despesas:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveloutrasdespesas: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelOUTRASDESPESAS: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Outras Despesas'
						DataField = 'OUTRASDESPESAS'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel25: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =24

					object pnlLayoutTitulosgocontrolerecebivelperctaxanominalacordada: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  % taxa nominal acordada:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelperctaxanominalacordada: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelPERCTAXANOMINALACORDADA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = '% taxa nominal acordada'
						DataField = 'PERCTAXANOMINALACORDADA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel26: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =25

					object pnlLayoutTitulosgocontrolerecebivelvlcoberturaformento: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Cobertura Fomento:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvlcoberturaformento: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLCOBERTURAFORMENTO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Cobertura Fomento'
						DataField = 'VLCOBERTURAFORMENTO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel27: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =26

					object pnlLayoutTitulosgocontrolerecebivelvldestinadoencargofomento: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Encargo Fomento:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadoencargofomento: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADOENCARGOFOMENTO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Encargo Fomento'
						DataField = 'VLDESTINADOENCARGOFOMENTO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel28: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =27

					object pnlLayoutTitulosgocontrolerecebivelvldestinadorecomporatitulo: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Recompra T�tulos:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadorecomporatitulo: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADORECOMPORATITULO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Recompra T�tulos'
						DataField = 'VLDESTINADORECOMPORATITULO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel29: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =28

					object pnlLayoutTitulosgocontrolerecebivelvldestinadodefasagem: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Defasagem:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadodefasagem: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADODEFASAGEM: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Defasagem'
						DataField = 'VLDESTINADODEFASAGEM'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel30: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =29

					object pnlLayoutTitulosgocontrolerecebivelvldestinadocomissaria: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Comiss�ria:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadocomissaria: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADOCOMISSARIA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Comiss�ria'
						DataField = 'VLDESTINADOCOMISSARIA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel31: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =30

					object pnlLayoutTitulosgocontrolerecebivelvldestinadogarantia: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Garantia:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadogarantia: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADOGARANTIA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Garantia'
						DataField = 'VLDESTINADOGARANTIA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel32: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =31

					object pnlLayoutTitulosgocontrolerecebivelvldestinadoretencaodivida: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Reten��o D�vida:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadoretencaodivida: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADORETENCAODIVIDA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Reten��o D�vida'
						DataField = 'VLDESTINADORETENCAODIVIDA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel33: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =32

					object pnlLayoutTitulosgocontrolerecebivelvldestinadocontagrafica: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Destinado Conta Gr�fica:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvldestinadocontagrafica: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLDESTINADOCONTAGRAFICA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Destinado Conta Gr�fica'
						DataField = 'VLDESTINADOCONTAGRAFICA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel34: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =33

					object pnlLayoutTitulosgocontrolerecebivelcustosforaoperacao: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Custos fora opera��o(Ex.TED, DOC, ETC):'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelcustosforaoperacao: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelCUSTOSFORAOPERACAO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Custos fora opera��o(Ex.TED, DOC, ETC)'
						DataField = 'CUSTOSFORAOPERACAO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel35: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =34

					object pnlLayoutTitulosgocontrolerecebivelvloutroscreditos: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor recebido outros cr�ditos:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvloutroscreditos: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLOUTROSCREDITOS: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor recebido outros cr�ditos'
						DataField = 'VLOUTROSCREDITOS'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel36: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =35

					object pnlLayoutTitulosgocontrolerecebivelvlrecebidocaixa: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Valor Recebido Caixa:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebivelvlrecebidocaixa: TPanel
					Left = 0
					Top = 0
					Width = 100
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelVLRECEBIDOCAIXA: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 90
						TabOrder = 0
						ShowHint = True
						Hint = 'Valor Recebido Caixa'
						DataField = 'VLRECEBIDOCAIXA'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel37: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =36

					object pnlLayoutTitulosgocontrolerecebiveldtcredito: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Data Cr�dito:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveldtcredito: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelDTCREDITO: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'Data Cr�dito'
						DataField = 'DTCREDITO'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

				object pnlLayoutLinhasgocontrolerecebivel38: TPanel
				Left = 0
				Top = 0
				Width = 591
				Height = 30
				Align = alTop
				BevelOuter = bvNone
				Color = clBtnFace
				TabOrder =37

					object pnlLayoutTitulosgocontrolerecebiveldtindice: TPanel
					Left = 0
					Top = 0
					Width = 150
					Height = 13
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Caption ='  Data �ndice:'
					Alignment = taLeftJustify
					end

					object pnlLayoutCamposgocontrolerecebiveldtindice: TPanel
					Left = 0
					Top = 0
					Width = 82
					Height = 30
					Align = alLeft
					BevelOuter = bvNone
					Color = clBtnFace
					TabOrder = 0
					Alignment = taLeftJustify

						object dbegocontrolerecebivelDTINDICE: TDBEdit
						Left = 6
						Top = 5
						Height = 21
						Width = 72
						TabOrder = 0
						ShowHint = True
						Hint = 'Data �ndice'
						DataField = 'DTINDICE'
						DataSource = dsgocontrolerecebivel
						end

					end

				end

			end
	end
	end
	object tabPesquisas: TcxTabSheet
	Caption = 'Pesquisas'
	PopupMenu = popupgocontrolerecebivel
	OnShow = altenarPopupTabSheet
		object pnlTituloPesquisa: TPanel
		Width = 784
		Height = 19
		Align = alTop
		Alignment = taLeftJustify
		BevelOuter = bvNone
		Caption = '  Op��o(�es) de pesquisa(s)...'
		Font.Charset = DEFAULT_CHARSET
		Font.Color = clWindowText
		Font.Height = -11
		Font.Name = 'Tahoma'
		Font.Style = [fsBold]
		ParentFont = False
		end
		object flwpOpcoes: TFlowPanel
		Left = 0
		Top = 0
		Width = 784
		Height = 70
		Align = alTop
		BevelOuter = bvNone
		end
		object flwpPesquisas: TFlowPanel
		Left = 0
		Top = 30
		Width = 784
		Height = 80
		Align = alTop
		BevelOuter = bvNone
		end
		object pnlTituloResultadoPesquisa: TPanel
		Width = 784
		Height = 19
		Align = altop
		Alignment = taLeftJustify
		BevelOuter = bvNone
		Caption = '  Resultado da pesquisa...'
		Font.Charset = DEFAULT_CHARSET
		Font.Color = clWindowText
		Font.Height = -11
		Font.Name = 'Tahoma'
		Font.Style = [fsBold]
		ParentFont = False
		end
		object pnlResultadoPesquisas: TPanel
		Width = 784
		Height = 146
		Align = alclient
		BevelOuter = bvNone
		object GridResultadoPesquisa: TcxGrid
		Left = 0                            
		Top = 19                            
		Width = 700                         
		Height = 300                        
		Align = alClient                    
		TabOrder = 1                        
		LookAndFeel.Kind = lfStandard       
		LookAndFeel.NativeStyle = False     
		ExplicitLeft = 128                  
		ExplicitTop = 112                   
		ExplicitWidth = 250                 
		ExplicitHeight = 200                
		object GridResultadoPesquisaDBTV: TcxGridDBTableView
		Navigator.Buttons.CustomButtons = <>                        
		Navigator.Buttons.Insert.Enabled = False                    
		Navigator.Buttons.Append.Enabled = False                    
		Navigator.Buttons.Delete.Enabled = False                    
		Navigator.Buttons.Edit.Enabled = False                      
		Navigator.Buttons.Cancel.Enabled = False                    
		Navigator.Visible = false                                   
		DataController.DataSource = dsgocontrolerecebivel
		DataController.Summary.DefaultGroupSummaryItems = <>        
		DataController.Summary.FooterSummaryItems = <>              
		DataController.Summary.SummaryGroups = <>                   
		OptionsData.CancelOnExit = False                            
		OptionsData.Deleting = False                                
		OptionsData.Editing = False                                 
		OptionsData.Inserting = False                               
		OptionsView.NoDataToDisplayInfoText = 'Sem registros'     
		OptionsView.GroupByBox = False                              
		OptionsView.footer = true                                   
		Styles.Background = cxStylepadrao                           
		Styles.Selection = cxStyle1                                 
		object GRIDResultadoPesquisaDBTVID :TcxGridDBColumn
		DataBinding.FieldName ='ID'
		end
		object GRIDResultadoPesquisaDBTVDT_CADASTRO :TcxGridDBColumn
		DataBinding.FieldName ='DT_CADASTRO'
		end
		object GRIDResultadoPesquisaDBTVHS_CADASTRO :TcxGridDBColumn
		DataBinding.FieldName ='HS_CADASTRO'
		end
		object GRIDResultadoPesquisaDBTVCODPORTADOR :TcxGridDBColumn
		DataBinding.FieldName ='CODPORTADOR'
		end
		object GRIDResultadoPesquisaDBTVPORTADOR :TcxGridDBColumn
		DataBinding.FieldName ='PORTADOR'
		end
		object GRIDResultadoPesquisaDBTVDTCOMPETENCIA :TcxGridDBColumn
		DataBinding.FieldName ='DTCOMPETENCIA'
		end
		object GRIDResultadoPesquisaDBTVCODCARTEIRA :TcxGridDBColumn
		DataBinding.FieldName ='CODCARTEIRA'
		end
		object GRIDResultadoPesquisaDBTVCARTEIRA :TcxGridDBColumn
		DataBinding.FieldName ='CARTEIRA'
		end
		object GRIDResultadoPesquisaDBTVCODTIPO :TcxGridDBColumn
		DataBinding.FieldName ='CODTIPO'
		end
		object GRIDResultadoPesquisaDBTVTIPO :TcxGridDBColumn
		DataBinding.FieldName ='TIPO'
		end
		object GRIDResultadoPesquisaDBTVNUMBORDERO :TcxGridDBColumn
		DataBinding.FieldName ='NUMBORDERO'
		end
		object GRIDResultadoPesquisaDBTVQTTITULOS :TcxGridDBColumn
		DataBinding.FieldName ='QTTITULOS'
		end
		object GRIDResultadoPesquisaDBTVVLBRUTOBORDERO :TcxGridDBColumn
		DataBinding.FieldName ='VLBRUTOBORDERO'
		end
		object GRIDResultadoPesquisaDBTVQTDRECUSADA :TcxGridDBColumn
		DataBinding.FieldName ='QTDRECUSADA'
		end
		object GRIDResultadoPesquisaDBTVVLBRUTORECUSADO :TcxGridDBColumn
		DataBinding.FieldName ='VLBRUTORECUSADO'
		end
		end
		end

		end
	end
	end

end
