unit ugopainelrisco;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 25/08/2021*}                                          
{*Hora 17:59:42*}                                            
{*Unit gopainelrisco *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus;

type                                                                                        
	Tfgopainelrisco = class(TForm)

	{>>>datasets}
	sqlgopainelrisco: TClientDataSet;
	dspgopainelrisco: TDataSetProvider;
	cdsgopainelrisco: TClientDataSet;
	dsgopainelrisco: TDataSource;

	{objetos datasets<<<}

	{>>>objetos gerais}
	pnlTitulo: TPanel;
	lblTitulo: TLabel;
	spdOpcoes: TSpeedButton;
	spdImpressao: TSpeedButton;
	spdExportacao: TSpeedButton;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	pnlLogo: TPanel;
	pnlLinhaTitulo: TPanel;
	imglogo: TImage;
	lblSite: TLabel;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgopainelrisco: TfrxReport;
	frxDBgopainelrisco: TfrxDBDataset;
	Popupgopainelrisco: TPopupMenu;
	{objetos gerais<<<}

	{>>>objetos de manipula��o de dados}
	pgcPrincipal: TPageControl;
	tabPrincipal: TTabSheet;
	Gridgopainelrisco: TcxGrid;
	lvlregistrosgopainelrisco: TcxGridLevel;
	grdregistrostbwgopainelrisco: TcxGridDBTableView;
	GridResultadoPesquisa: TcxGrid;
	lvlResultadoPesquisa : TcxGridLevel;
	grdtbwResultadoPesquisa: TcxGridDBTableView;
	tabgopainelrisco: tcxTabSheet;
	SCRgopainelrisco: TScrollBox;
	pnlDadosgopainelrisco: TPanel;
	{>>>bot�es de manipula��o de dados gopainelrisco}
	pnlLinhaBotoesgopainelrisco: TPanel;
	spdAdicionargopainelrisco: TButton;
	spdAlterargopainelrisco: TButton;
	spdGravargopainelrisco: TButton;
	spdCancelargopainelrisco: TButton;
	spdExcluirgopainelrisco: TButton;

	{bot�es de manipula��o de dadosgopainelrisco<<<}
	{campo ID}
	sqlgopainelriscoID: TIntegerField;
	cdsgopainelriscoID: TIntegerField;
	pnlLayoutLinhasgopainelrisco1: TPanel;
	pnlLayoutTitulosgopainelriscoid: TPanel;
	pnlLayoutCamposgopainelriscoid: TPanel;
	dbegopainelriscoid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgopainelriscoDT_CADASTRO: TDateField;
	cdsgopainelriscoDT_CADASTRO: TDateField;
	pnlLayoutLinhasgopainelrisco2: TPanel;
	pnlLayoutTitulosgopainelriscodt_cadastro: TPanel;
	pnlLayoutCamposgopainelriscodt_cadastro: TPanel;
	dbegopainelriscodt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgopainelriscoHS_CADASTRO: TTimeField;
	cdsgopainelriscoHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgopainelrisco3: TPanel;
	pnlLayoutTitulosgopainelriscohs_cadastro: TPanel;
	pnlLayoutCamposgopainelriscohs_cadastro: TPanel;
	dbegopainelriscohs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlgopainelriscoDESCRICAO: TStringField;
	cdsgopainelriscoDESCRICAO: TStringField;
	pnlLayoutLinhasgopainelrisco4: TPanel;
	pnlLayoutTitulosgopainelriscodescricao: TPanel;
	pnlLayoutCamposgopainelriscodescricao: TPanel;
	dbegopainelriscodescricao: TDBEdit;

	{campo CODPROJETO}
	sqlgopainelriscoCODPROJETO: TIntegerField;
	cdsgopainelriscoCODPROJETO: TIntegerField;
	pnlLayoutLinhasgopainelrisco5: TPanel;
	pnlLayoutTitulosgopainelriscocodprojeto: TPanel;
	pnlLayoutCamposgopainelriscocodprojeto: TPanel;
	dbegopainelriscocodprojeto: TDBEdit;

	{campo PROJETO}
	sqlgopainelriscoPROJETO: TStringField;
	cdsgopainelriscoPROJETO: TStringField;
	pnlLayoutLinhasgopainelrisco6: TPanel;
	pnlLayoutTitulosgopainelriscoprojeto: TPanel;
	pnlLayoutCamposgopainelriscoprojeto: TPanel;
	dbegopainelriscoprojeto: TDBEdit;

	{campo CODFILIAL}
	sqlgopainelriscoCODFILIAL: TIntegerField;
	cdsgopainelriscoCODFILIAL: TIntegerField;
	pnlLayoutLinhasgopainelrisco7: TPanel;
	pnlLayoutTitulosgopainelriscocodfilial: TPanel;
	pnlLayoutCamposgopainelriscocodfilial: TPanel;
	dbegopainelriscocodfilial: TDBEdit;

	{campo FILIAL}
	sqlgopainelriscoFILIAL: TStringField;
	cdsgopainelriscoFILIAL: TStringField;
	pnlLayoutLinhasgopainelrisco8: TPanel;
	pnlLayoutTitulosgopainelriscofilial: TPanel;
	pnlLayoutCamposgopainelriscofilial: TPanel;
	dbegopainelriscofilial: TDBEdit;

	{campo DATA}
	sqlgopainelriscoDATA: TDateField;
	cdsgopainelriscoDATA: TDateField;
	pnlLayoutLinhasgopainelrisco9: TPanel;
	pnlLayoutTitulosgopainelriscodata: TPanel;
	pnlLayoutCamposgopainelriscodata: TPanel;
	dbegopainelriscodata: TDBEdit;

	{campo VLVENCIDOS}
	sqlgopainelriscoVLVENCIDOS: TFloatField;
	cdsgopainelriscoVLVENCIDOS: TFloatField;
	pnlLayoutLinhasgopainelrisco10: TPanel;
	pnlLayoutTitulosgopainelriscovlvencidos: TPanel;
	pnlLayoutCamposgopainelriscovlvencidos: TPanel;
	dbegopainelriscovlvencidos: TDBEdit;

	{campo VLVENCER}
	sqlgopainelriscoVLVENCER: TFloatField;
	cdsgopainelriscoVLVENCER: TFloatField;
	pnlLayoutLinhasgopainelrisco11: TPanel;
	pnlLayoutTitulosgopainelriscovlvencer: TPanel;
	pnlLayoutCamposgopainelriscovlvencer: TPanel;
	dbegopainelriscovlvencer: TDBEdit;
    GridResultadoPesquisaLevel1: TcxGridLevel;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure cdsgopainelriscoAfterClose(DataSet: TDataSet);
	procedure cdsgopainelriscoAfterOpen(DataSet: TDataSet);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargopainelriscoClick(Sender: TObject);
	procedure spdAlterargopainelriscoClick(Sender: TObject);
	procedure spdGravargopainelriscoClick(Sender: TObject);
	procedure spdCancelargopainelriscoClick(Sender: TObject);
	procedure spdExcluirgopainelriscoClick(Sender: TObject);
	procedure cdsgopainelriscoBeforePost(DataSet: TDataSet);
	procedure grdregistrostbwgopainelriscoDblClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgopainelrisco: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgopainelrisco: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgopainelrisco: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgopainelrisco: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagopainelrisco: Array[0..0]  of TDuplicidade;
	duplicidadeCampogopainelrisco: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
	public

end;

var
fgopainelrisco: Tfgopainelrisco;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgopainelrisco.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgopainelrisco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgopainelrisco.verificarEmTransacao(cdsgopainelrisco);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgopainelrisco.configurarGridesFormulario(fgopainelrisco,true, false);

	fncgopainelrisco.Free;
	{eliminando container de fun��es da memoria<<<}

	fgopainelrisco:=nil;
	Action:=cafree;
end;

procedure Tfgopainelrisco.FormCreate(Sender: TObject);
begin
	{>>>container de fun��es}
	fncgopainelrisco:= TFuncoes.Create(Self);
	fncgopainelrisco.definirConexao(dm.conexao);
	fncgopainelrisco.definirConexaohistorico(dm.conexao);
	fncgopainelrisco.definirFormulario(Self);
	fncgopainelrisco.validarTransacaoRelacionamento:=true;
	fncgopainelrisco.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data de Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODPROJETO';
	pesquisasItem.titulo:='C�digo Projeto';
	pesquisasItem.campo:='CODPROJETO';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesPROJETO';
	pesquisasItem.titulo:='Projeto';
	pesquisasItem.campo:='PROJETO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCODFILIAL';
	pesquisasItem.titulo:='C�d.Filial';
	pesquisasItem.campo:='CODFILIAL';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesFILIAL';
	pesquisasItem.titulo:='Filial';
	pesquisasItem.campo:='FILIAL';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[6]:=pesquisasItem;

	fncgopainelrisco.criarPesquisas(sqlgopainelrisco,cdsgopainelrisco,'select * from gopainelrisco', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gopainelrisco}
	camposObrigatoriosgopainelrisco[0] :=dbegopainelriscodescricao;
	camposObrigatoriosgopainelrisco[1] :=dbegopainelriscodata;
	{campos obrigatorios gopainelrisco<<<}

	{>>>objetos ativos no modo de manipula��o de dados gopainelrisco}
	objetosModoEdicaoAtivosgopainelrisco[0]:=pnlDadosgopainelrisco;
	objetosModoEdicaoAtivosgopainelrisco[1]:=spdCancelargopainelrisco;
	objetosModoEdicaoAtivosgopainelrisco[2]:=spdGravargopainelrisco;
	{objetos ativos no modo de manipula��o de dados gopainelrisco<<<}

	{>>>objetos inativos no modo de manipula��o de dados gopainelrisco}
	objetosModoEdicaoInativosgopainelrisco[0]:=spdAdicionargopainelrisco;
	objetosModoEdicaoInativosgopainelrisco[1]:=spdAlterargopainelrisco;
	objetosModoEdicaoInativosgopainelrisco[2]:=spdExcluirgopainelrisco;
	objetosModoEdicaoInativosgopainelrisco[3]:=spdOpcoes;
	objetosModoEdicaoInativosgopainelrisco[4]:=spdImpressao;
	objetosModoEdicaoInativosgopainelrisco[5]:=spdExportacao;
	objetosModoEdicaoInativosgopainelrisco[6]:=tabPrincipal;
	objetosModoEdicaoInativosgopainelrisco[7]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gopainelrisco<<<}

	{>>>comando de adi��o de teclas de atalhos gopainelrisco}
	fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Adicionar novo registro',VK_F4,spdAdicionargopainelrisco.OnClick);
	fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Alterar registro selecionado',VK_F5,spdAlterargopainelrisco.OnClick);
	fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Gravar �ltimas altera��es',VK_F6,spdGravargopainelrisco.OnClick);
	fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Cancelar �ltimas altera��e',VK_F7,spdCancelargopainelrisco.OnClick);
	fncgopainelrisco.criaAtalhoPopupMenu(Popupgopainelrisco,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgopainelrisco.OnClick);

	{comando de adi��o de teclas de atalhos gopainelrisco<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgopainelrisco.open();
	{abertura dos datasets<<<}

	fncgopainelrisco.criaAtalhoPopupMenuNavegacao(Popupgopainelrisco,cdsgopainelrisco);

	fncgopainelrisco.definirMascaraCampos(cdsgopainelrisco);

	fncgopainelrisco.configurarGridesFormulario(fgopainelrisco,false, true);

end;

procedure Tfgopainelrisco.spdAdicionargopainelriscoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgopainelrisco.TabIndex;
	if (fncgopainelrisco.adicionar(cdsgopainelrisco)=true) then
		begin
			fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,true);
			dbegopainelriscodescricao.SetFocus;
	end
	else
		fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,false);
end;

procedure Tfgopainelrisco.spdAlterargopainelriscoClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgopainelrisco.TabIndex;
	if (fncgopainelrisco.alterar(cdsgopainelrisco)=true) then
		begin
			fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,true);
			dbegopainelriscodescricao.SetFocus;
	end
	else
		fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,false);
end;

procedure Tfgopainelrisco.spdGravargopainelriscoClick(Sender: TObject);
begin
	fncgopainelrisco.verificarCamposObrigatorios(cdsgopainelrisco, camposObrigatoriosgopainelrisco);

	if (fncgopainelrisco.gravar(cdsgopainelrisco)=true) then
		fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,false)
	else
		fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,true);
end;

procedure Tfgopainelrisco.spdCancelargopainelriscoClick(Sender: TObject);
begin
	if (fncgopainelrisco.cancelar(cdsgopainelrisco)=true) then
		fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,false)
	else
		fncgopainelrisco.ativarModoEdicao(objetosModoEdicaoAtivosgopainelrisco,objetosModoEdicaoInativosgopainelrisco,true);
end;

procedure Tfgopainelrisco.spdExcluirgopainelriscoClick(Sender: TObject);
begin
	fncgopainelrisco.excluir(cdsgopainelrisco);
end;

procedure Tfgopainelrisco.cdsgopainelriscoBeforePost(DataSet: TDataSet);
begin
	if cdsgopainelrisco.State in [dsinsert] then
	cdsgopainelriscoID.Value:=fncgopainelrisco.autoNumeracaoGenerator(dm.conexao,'G_gopainelrisco');
end;

procedure Tfgopainelrisco.GridgopainelriscoDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgopainelrisco.TabIndex;
end;

procedure Tfgopainelrisco.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgopainelrisco.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgopainelrisco.cdsgopainelriscoAfterOpen(DataSet: TDataSet);
begin
end;

procedure Tfgopainelrisco.cdsgopainelriscoAfterClose(DataSet: TDataSet);
begin
end;

procedure Tfgopainelrisco.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgopainelrisco.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgopainelrisco.IsEmpty then 
	fncgopainelrisco.visualizarRelatorios(frxgopainelrisco);
end;

end.

