CREATE TABLE GOPROJETOFILIAL(
ID Integer   
,
CONSTRAINT pk_GOPROJETOFILIAL PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODFILIAL Integer   
,
FILIAL Varchar(250)   COLLATE PT_BR
,
DT_ENTRADA Date   
,
DT_SAIDA Date   
,
STATUS Varchar(250)   COLLATE PT_BR
,
CODPROJETO Integer   
);
commit;
CREATE ASC INDEX I01_GOPR_ILIAL_ID ON GOPROJETOFILIAL (ID);
CREATE ASC INDEX I02_GOPR_ILIAL_DT_CADASTRO ON GOPROJETOFILIAL (DT_CADASTRO);
CREATE ASC INDEX I03_GOPR_ILIAL_HS_CADASTRO ON GOPROJETOFILIAL (HS_CADASTRO);
CREATE ASC INDEX I04_GOPR_ILIAL_CODFILIAL ON GOPROJETOFILIAL (CODFILIAL);
CREATE ASC INDEX I05_GOPR_ILIAL_FILIAL ON GOPROJETOFILIAL (FILIAL);
CREATE ASC INDEX I06_GOPR_ILIAL_DT_ENTRADA ON GOPROJETOFILIAL (DT_ENTRADA);
CREATE ASC INDEX I07_GOPR_ILIAL_DT_SAIDA ON GOPROJETOFILIAL (DT_SAIDA);
CREATE ASC INDEX I08_GOPR_ILIAL_STATUS ON GOPROJETOFILIAL (STATUS);
CREATE ASC INDEX I09_GOPR_ILIAL_CODPROJETO ON GOPROJETOFILIAL (CODPROJETO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d. Filial' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'CODFILIAL';
update rdb$relation_fields set rdb$description = 'Filial' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'FILIAL';
update rdb$relation_fields set rdb$description = 'Data de Entrada' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'DT_ENTRADA';
update rdb$relation_fields set rdb$description = 'Data de Sa�da' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'DT_SAIDA';
update rdb$relation_fields set rdb$description = 'Status' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'STATUS';
update rdb$relation_fields set rdb$description = 'C�d.Projeto' where rdb$relation_name = 'GOPROJETOFILIAL' and rdb$field_name = 'CODPROJETO';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
