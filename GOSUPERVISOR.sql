CREATE TABLE GOSUPERVISOR(
ID Integer   
,
CONSTRAINT pk_GOSUPERVISOR PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
NOME Varchar(250)  not null COLLATE PT_BR
,
CPF Varchar(30)  not null COLLATE PT_BR
,
GERENTE Varchar(250)   COLLATE PT_BR
,
EMAIL1 Varchar(250)   COLLATE PT_BR
,
EMAIL2 Varchar(250)   COLLATE PT_BR
,
TELEFONE1 Varchar(30)   COLLATE PT_BR
,
TELEFONE2 Varchar(30)   COLLATE PT_BR
,
STATUS Varchar(30) DEFAULT 'ATIVO'  COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOSU_VISOR_ID ON GOSUPERVISOR (ID);
CREATE ASC INDEX I02_GOSU_VISOR_DT_CADASTRO ON GOSUPERVISOR (DT_CADASTRO);
CREATE ASC INDEX I03_GOSU_VISOR_HS_CADASTRO ON GOSUPERVISOR (HS_CADASTRO);
CREATE ASC INDEX I04_GOSU_VISOR_NOME ON GOSUPERVISOR (NOME);
CREATE ASC INDEX I05_GOSU_VISOR_CPF ON GOSUPERVISOR (CPF);
CREATE ASC INDEX I06_GOSU_VISOR_GERENTE ON GOSUPERVISOR (GERENTE);
CREATE ASC INDEX I07_GOSU_VISOR_EMAIL1 ON GOSUPERVISOR (EMAIL1);
CREATE ASC INDEX I08_GOSU_VISOR_EMAIL2 ON GOSUPERVISOR (EMAIL2);
CREATE ASC INDEX I09_GOSU_VISOR_TELEFONE1 ON GOSUPERVISOR (TELEFONE1);
CREATE ASC INDEX I10_GOSU_VISOR_TELEFONE2 ON GOSUPERVISOR (TELEFONE2);
CREATE ASC INDEX I11_GOSU_VISOR_STATUS ON GOSUPERVISOR (STATUS);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Nome' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'NOME';
update rdb$relation_fields set rdb$description = 'CPF' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'CPF';
update rdb$relation_fields set rdb$description = 'Gerente' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'GERENTE';
update rdb$relation_fields set rdb$description = 'E-Mail' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'EMAIL1';
update rdb$relation_fields set rdb$description = 'E-Mail' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'EMAIL2';
update rdb$relation_fields set rdb$description = 'Telefone' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'TELEFONE1';
update rdb$relation_fields set rdb$description = 'Telefone' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'TELEFONE2';
update rdb$relation_fields set rdb$description = 'Status' where rdb$relation_name = 'GOSUPERVISOR' and rdb$field_name = 'STATUS';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOS;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOS_GEN FOR GOPROJETOS ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOS, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOSUPERVISOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOSUPERVISOR_GEN FOR GOSUPERVISOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOSUPERVISOR, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
