CREATE TABLE GOFILIAL(
ID Integer   
,
CONSTRAINT pk_GOFILIAL PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
DESCRICAO Varchar(250)   COLLATE PT_BR
,
CNPJ Varchar(30)   COLLATE PT_BR
,
CEP Varchar(10)   COLLATE PT_BR
,
ENDERECO Varchar(250)   COLLATE PT_BR
,
UF Varchar(2)   COLLATE PT_BR
,
CIDADE Varchar(250)   COLLATE PT_BR
,
CNAE Varchar(250)   COLLATE PT_BR
,
REPRESENTANTE Varchar(250)   COLLATE PT_BR
,
CAPITAL_SOCIAL Double Precision   
,
EMAIL1 Varchar(250)   COLLATE PT_BR
,
EMAIL2 Varchar(250)   COLLATE PT_BR
,
TELEFONE1 Varchar(30)   COLLATE PT_BR
,
TELEFONE2 Varchar(30)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOFI_ILIAL_ID ON GOFILIAL (ID);
CREATE ASC INDEX I02_GOFI_ILIAL_DT_CADASTRO ON GOFILIAL (DT_CADASTRO);
CREATE ASC INDEX I03_GOFI_ILIAL_HS_CADASTRO ON GOFILIAL (HS_CADASTRO);
CREATE ASC INDEX I04_GOFI_ILIAL_DESCRICAO ON GOFILIAL (DESCRICAO);
CREATE ASC INDEX I05_GOFI_ILIAL_CNPJ ON GOFILIAL (CNPJ);
CREATE ASC INDEX I06_GOFI_ILIAL_CEP ON GOFILIAL (CEP);
CREATE ASC INDEX I07_GOFI_ILIAL_ENDERECO ON GOFILIAL (ENDERECO);
CREATE ASC INDEX I08_GOFI_ILIAL_UF ON GOFILIAL (UF);
CREATE ASC INDEX I09_GOFI_ILIAL_CIDADE ON GOFILIAL (CIDADE);
CREATE ASC INDEX I10_GOFI_ILIAL_CNAE ON GOFILIAL (CNAE);
CREATE ASC INDEX I11_GOFI_ILIAL_REPRESENTANTE ON GOFILIAL (REPRESENTANTE);
CREATE ASC INDEX I12_GOFI_ILIAL_CAPITAL_SOCIAL ON GOFILIAL (CAPITAL_SOCIAL);
CREATE ASC INDEX I13_GOFI_ILIAL_EMAIL1 ON GOFILIAL (EMAIL1);
CREATE ASC INDEX I14_GOFI_ILIAL_EMAIL2 ON GOFILIAL (EMAIL2);
CREATE ASC INDEX I15_GOFI_ILIAL_TELEFONE1 ON GOFILIAL (TELEFONE1);
CREATE ASC INDEX I16_GOFI_ILIAL_TELEFONE2 ON GOFILIAL (TELEFONE2);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'DESCRICAO';
update rdb$relation_fields set rdb$description = 'CNPJ' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'CNPJ';
update rdb$relation_fields set rdb$description = 'CEP' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'CEP';
update rdb$relation_fields set rdb$description = 'Endere�o' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'ENDERECO';
update rdb$relation_fields set rdb$description = 'Estado' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'UF';
update rdb$relation_fields set rdb$description = 'Cidade' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'CIDADE';
update rdb$relation_fields set rdb$description = 'C�d. Atividade' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'CNAE';
update rdb$relation_fields set rdb$description = 'Representante legal' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'REPRESENTANTE';
update rdb$relation_fields set rdb$description = 'Capital Social' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'CAPITAL_SOCIAL';
update rdb$relation_fields set rdb$description = 'E_Mail' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'EMAIL1';
update rdb$relation_fields set rdb$description = 'E_Mail' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'EMAIL2';
update rdb$relation_fields set rdb$description = 'Telefone' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'TELEFONE1';
update rdb$relation_fields set rdb$description = 'Telefone' where rdb$relation_name = 'GOFILIAL' and rdb$field_name = 'TELEFONE2';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
