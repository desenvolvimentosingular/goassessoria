CREATE TABLE GOPROJETOS(
ID Integer  not null 
,
CONSTRAINT pk_GOPROJETOS PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODGRUPO Integer   
,
GRUPO Varchar(250)   COLLATE PT_BR
,
DESCRICAO Varchar(250)  not null COLLATE PT_BR
,
TIPO Varchar(250)  not null COLLATE PT_BR
,
DT_INICIO Date   
,
DT_TERMINO Date   
,
STATUS Varchar(30)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOPR_JETOS_ID ON GOPROJETOS (ID);
CREATE ASC INDEX I02_GOPR_JETOS_DT_CADASTRO ON GOPROJETOS (DT_CADASTRO);
CREATE ASC INDEX I03_GOPR_JETOS_HS_CADASTRO ON GOPROJETOS (HS_CADASTRO);
CREATE ASC INDEX I04_GOPR_JETOS_CODGRUPO ON GOPROJETOS (CODGRUPO);
CREATE ASC INDEX I05_GOPR_JETOS_GRUPO ON GOPROJETOS (GRUPO);
CREATE ASC INDEX I06_GOPR_JETOS_DESCRICAO ON GOPROJETOS (DESCRICAO);
CREATE ASC INDEX I07_GOPR_JETOS_TIPO ON GOPROJETOS (TIPO);
CREATE ASC INDEX I08_GOPR_JETOS_DT_INICIO ON GOPROJETOS (DT_INICIO);
CREATE ASC INDEX I09_GOPR_JETOS_DT_TERMINO ON GOPROJETOS (DT_TERMINO);
CREATE ASC INDEX I10_GOPR_JETOS_STATUS ON GOPROJETOS (STATUS);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Grupo' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'CODGRUPO';
update rdb$relation_fields set rdb$description = 'Grupo' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'GRUPO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'DESCRICAO';
update rdb$relation_fields set rdb$description = 'Tipo' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'TIPO';
update rdb$relation_fields set rdb$description = 'Data In�cio' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'DT_INICIO';
update rdb$relation_fields set rdb$description = 'Data Fim' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'DT_TERMINO';
update rdb$relation_fields set rdb$description = 'Status' where rdb$relation_name = 'GOPROJETOS' and rdb$field_name = 'STATUS';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOS;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOS_GEN FOR GOPROJETOS ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOS, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
