unit ugocliente;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 01/11/2021*}                                          
{*Hora 19:57:55*}                                            
{*Unit gocliente *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, dxGDIPlusClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, Vcl.DialogMessage;

type                                                                                        
	Tfgocliente = class(TForm)
    sqlgocliente: TSQLDataSet;
	dspgocliente: TDataSetProvider;
	cdsgocliente: TClientDataSet;
	dsgocliente: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgocliente: TfrxReport;
	frxDBgocliente: TfrxDBDataset;
	Popupgocliente: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgocliente: tcxTabSheet;
	SCRgocliente: TScrollBox;
	pnlDadosgocliente: TPanel;
	{>>>bot�es de manipula��o de dados gocliente}
	pnlLinhaBotoesgocliente: TPanel;
	spdAdicionargocliente: TButton;
	spdAlterargocliente: TButton;
	spdGravargocliente: TButton;
	spdCancelargocliente: TButton;
	spdExcluirgocliente: TButton;

	{bot�es de manipula��o de dadosgocliente<<<}
	{campo ID}
	sqlgoclienteID: TIntegerField;
	cdsgoclienteID: TIntegerField;
	pnlLayoutLinhasgocliente1: TPanel;
	pnlLayoutTitulosgoclienteid: TPanel;
	pnlLayoutCamposgoclienteid: TPanel;
	dbegoclienteid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgoclienteDT_CADASTRO: TDateField;
	cdsgoclienteDT_CADASTRO: TDateField;
	pnlLayoutLinhasgocliente2: TPanel;
	pnlLayoutTitulosgoclientedt_cadastro: TPanel;
	pnlLayoutCamposgoclientedt_cadastro: TPanel;
	dbegoclientedt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgoclienteHS_CADASTRO: TTimeField;
	cdsgoclienteHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgocliente3: TPanel;
	pnlLayoutTitulosgoclientehs_cadastro: TPanel;
	pnlLayoutCamposgoclientehs_cadastro: TPanel;
	dbegoclientehs_cadastro: TDBEdit;

	{campo GRUPO}
	sqlgoclienteGRUPO: TStringField;
	cdsgoclienteGRUPO: TStringField;
	pnlLayoutLinhasgocliente4: TPanel;
	pnlLayoutTitulosgoclientegrupo: TPanel;
	pnlLayoutCamposgoclientegrupo: TPanel;
	dbegoclientegrupo: TDBEdit;

	{campo TIPO}
	sqlgoclienteTIPO: TStringField;
	cdsgoclienteTIPO: TStringField;
	pnlLayoutLinhasgocliente5: TPanel;
	pnlLayoutTitulosgoclientetipo: TPanel;
	pnlLayoutCamposgoclientetipo: TPanel;
	cbogoclientetipo: TDBComboBox;

	{campo RAZAO_SOCIAL}
	sqlgoclienteRAZAO_SOCIAL: TStringField;
	cdsgoclienteRAZAO_SOCIAL: TStringField;
	pnlLayoutLinhasgocliente6: TPanel;
	pnlLayoutTitulosgoclienterazao_social: TPanel;
	pnlLayoutCamposgoclienterazao_social: TPanel;
	dbegoclienterazao_social: TDBEdit;

	{campo NOME_FANTASIA}
	sqlgoclienteNOME_FANTASIA: TStringField;
	cdsgoclienteNOME_FANTASIA: TStringField;
	pnlLayoutLinhasgocliente7: TPanel;
	pnlLayoutTitulosgoclientenome_fantasia: TPanel;
	pnlLayoutCamposgoclientenome_fantasia: TPanel;
	dbegoclientenome_fantasia: TDBEdit;

	{campo CNPJ_CPF}
	sqlgoclienteCNPJ_CPF: TStringField;
	cdsgoclienteCNPJ_CPF: TStringField;
	pnlLayoutLinhasgocliente8: TPanel;
	pnlLayoutTitulosgoclientecnpj_cpf: TPanel;
	pnlLayoutCamposgoclientecnpj_cpf: TPanel;
	dbegoclientecnpj_cpf: TDBEdit;

	{campo UF}
	sqlgoclienteUF: TStringField;
	cdsgoclienteUF: TStringField;
	pnlLayoutLinhasgocliente9: TPanel;
	pnlLayoutTitulosgoclienteuf: TPanel;
	pnlLayoutCamposgoclienteuf: TPanel;
	cbogoclienteuf: TDBComboBox;

	{campo CEP}
	sqlgoclienteCEP: TStringField;
	cdsgoclienteCEP: TStringField;
	pnlLayoutLinhasgocliente10: TPanel;
	pnlLayoutTitulosgoclientecep: TPanel;
	pnlLayoutCamposgoclientecep: TPanel;
	dbegoclientecep: TDBEdit;

	{campo CIDADE}
	sqlgoclienteCIDADE: TStringField;
	cdsgoclienteCIDADE: TStringField;
	pnlLayoutLinhasgocliente11: TPanel;
	pnlLayoutTitulosgoclientecidade: TPanel;
	pnlLayoutCamposgoclientecidade: TPanel;
	dbegoclientecidade: TDBEdit;

	{campo BAIRRO}
	sqlgoclienteBAIRRO: TStringField;
	cdsgoclienteBAIRRO: TStringField;
	pnlLayoutLinhasgocliente12: TPanel;
	pnlLayoutTitulosgoclientebairro: TPanel;
	pnlLayoutCamposgoclientebairro: TPanel;
	dbegoclientebairro: TDBEdit;

	{campo ENDERECO}
	sqlgoclienteENDERECO: TStringField;
	cdsgoclienteENDERECO: TStringField;
	pnlLayoutLinhasgocliente13: TPanel;
	pnlLayoutTitulosgoclienteendereco: TPanel;
	pnlLayoutCamposgoclienteendereco: TPanel;
	dbegoclienteendereco: TDBEdit;

	{campo NUMERO}
	sqlgoclienteNUMERO: TStringField;
	cdsgoclienteNUMERO: TStringField;
	pnlLayoutLinhasgocliente14: TPanel;
	pnlLayoutTitulosgoclientenumero: TPanel;
	pnlLayoutCamposgoclientenumero: TPanel;
	dbegoclientenumero: TDBEdit;

	{campo COMPLEMENTO}
	sqlgoclienteCOMPLEMENTO: TStringField;
	cdsgoclienteCOMPLEMENTO: TStringField;
	pnlLayoutLinhasgocliente15: TPanel;
	pnlLayoutTitulosgoclientecomplemento: TPanel;
	pnlLayoutCamposgoclientecomplemento: TPanel;
	dbegoclientecomplemento: TDBEdit;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    cdsfiltros: TClientDataSet;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargoclienteClick(Sender: TObject);
	procedure spdAlterargoclienteClick(Sender: TObject);
	procedure spdGravargoclienteClick(Sender: TObject);
	procedure spdCancelargoclienteClick(Sender: TObject);
	procedure spdExcluirgoclienteClick(Sender: TObject);
	procedure cdsgoclienteBeforePost(DataSet: TDataSet);

 procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgocliente: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgocliente: Array[0..0]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgocliente: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgocliente: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagocliente: Array[0..0]  of TDuplicidade;
	duplicidadeCampogocliente: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fgocliente: Tfgocliente;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgocliente.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgocliente.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocliente.TabIndex;
end;

procedure Tfgocliente.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgocliente.verificarEmTransacao(cdsgocliente);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgocliente.configurarGridesFormulario(fgocliente,true, false);

	fncgocliente.Free;
	{eliminando container de fun��es da memoria<<<}

	fgocliente:=nil;
	Action:=cafree;
end;

procedure Tfgocliente.FormCreate(Sender: TObject);
begin
  sessao;

    if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
	{>>>container de fun��es}
	fncgocliente:= TFuncoes.Create(Self);
	fncgocliente.definirConexao(dm.conexao);
	fncgocliente.definirConexaohistorico(conexao);
	fncgocliente.definirFormulario(Self);
	fncgocliente.validarTransacaoRelacionamento:=true;
	fncgocliente.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	duplicidadeCampogocliente:=TDuplicidade.Create;
	duplicidadeCampogocliente.agrupador:='';
	duplicidadeCampogocliente.objeto :=dbegoclientecnpj_cpf;
	duplicidadeListagocliente[0]:=duplicidadeCampogocliente;

	{verificar campos duplicidade<<<}

	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesID';
	pesquisasItem.titulo:='C�digo';
	pesquisasItem.campo:='ID';
	pesquisasItem.tipoFDB:='Integer';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDT_CADASTRO';
	pesquisasItem.titulo:='Data Cadastro';
	pesquisasItem.campo:='DT_CADASTRO';
	pesquisasItem.tipoFDB:='Date';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='S';
	pesquisasItem.tamanho:=72;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[1]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesTIPO';
	pesquisasItem.titulo:='Tipo';
	pesquisasItem.campo:='TIPO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('F�SICA');
	pesquisasItem.comboItens.add('JUR�DICA');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[2]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesNOME_FANTASIA';
	pesquisasItem.titulo:='Nome Fantasia';
	pesquisasItem.campo:='NOME_FANTASIA';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[3]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesUF';
	pesquisasItem.titulo:='UF';
	pesquisasItem.campo:='UF';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='=';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=60;
	pesquisasItem.comboItens.clear;
	pesquisasItem.comboItens.add('AC');
	pesquisasItem.comboItens.add('AL');
	pesquisasItem.comboItens.add('AP');
	pesquisasItem.comboItens.add('AM');
	pesquisasItem.comboItens.add('BA');
	pesquisasItem.comboItens.add('CE');
	pesquisasItem.comboItens.add('DF');
	pesquisasItem.comboItens.add('ES');
	pesquisasItem.comboItens.add('GO');
	pesquisasItem.comboItens.add('MA');
	pesquisasItem.comboItens.add('MT');
	pesquisasItem.comboItens.add('MS');
	pesquisasItem.comboItens.add('MG');
	pesquisasItem.comboItens.add('PA');
	pesquisasItem.comboItens.add('PB');
	pesquisasItem.comboItens.add('PR');
	pesquisasItem.comboItens.add('PE');
	pesquisasItem.comboItens.add('PI');
	pesquisasItem.comboItens.add('RJ');
	pesquisasItem.comboItens.add('RN');
	pesquisasItem.comboItens.add('RS');
	pesquisasItem.comboItens.add('RO');
	pesquisasItem.comboItens.add('RR');
	pesquisasItem.comboItens.add('SC');
	pesquisasItem.comboItens.add('SP');
	pesquisasItem.comboItens.add('SE');
	pesquisasItem.comboItens.add('TO');
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[4]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCIDADE';
	pesquisasItem.titulo:='Cidade';
	pesquisasItem.campo:='CIDADE';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[5]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesBAIRRO';
	pesquisasItem.titulo:='Bairro';
	pesquisasItem.campo:='BAIRRO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[6]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesENDERECO';
	pesquisasItem.titulo:='Endere�o';
	pesquisasItem.campo:='ENDERECO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[7]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCEP';
	pesquisasItem.titulo:='CEP';
	pesquisasItem.campo:='CEP';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=90;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[8]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesRAZAO_SOCIAL';
	pesquisasItem.titulo:='Raz�o Social';
	pesquisasItem.campo:='RAZAO_SOCIAL';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[9]:=pesquisasItem;

	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesCNPJ_CPF';
	pesquisasItem.titulo:='CNPJ_CPF';
	pesquisasItem.campo:='CNPJ_CPF';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=180;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[10]:=pesquisasItem;
    {gerando campos no clientdatsaet de filtros}
  fncgocliente.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}


	fncgocliente.criarPesquisas(sqlgocliente,cdsgocliente,'select * from gocliente', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gocliente}
	camposObrigatoriosgocliente[0] :=dbegoclientecnpj_cpf;
	{campos obrigatorios gocliente<<<}

	{>>>objetos ativos no modo de manipula��o de dados gocliente}
	objetosModoEdicaoAtivosgocliente[0]:=pnlDadosgocliente;
	objetosModoEdicaoAtivosgocliente[1]:=spdCancelargocliente;
	objetosModoEdicaoAtivosgocliente[2]:=spdGravargocliente;
	{objetos ativos no modo de manipula��o de dados gocliente<<<}

	{>>>objetos inativos no modo de manipula��o de dados gocliente}
	objetosModoEdicaoInativosgocliente[0]:=spdAdicionargocliente;
	objetosModoEdicaoInativosgocliente[1]:=spdAlterargocliente;
	objetosModoEdicaoInativosgocliente[2]:=spdExcluirgocliente;
	objetosModoEdicaoInativosgocliente[3]:=spdOpcoes;
	objetosModoEdicaoInativosgocliente[4]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gocliente<<<}

	{>>>comando de adi��o de teclas de atalhos gocliente}
	fncgocliente.criaAtalhoPopupMenu(Popupgocliente,'Adicionar novo registro',VK_F4,spdAdicionargocliente.OnClick);
	fncgocliente.criaAtalhoPopupMenu(Popupgocliente,'Alterar registro selecionado',VK_F5,spdAlterargocliente.OnClick);
	fncgocliente.criaAtalhoPopupMenu(Popupgocliente,'Gravar �ltimas altera��es',VK_F6,spdGravargocliente.OnClick);
	fncgocliente.criaAtalhoPopupMenu(Popupgocliente,'Cancelar �ltimas altera��e',VK_F7,spdCancelargocliente.OnClick);
	fncgocliente.criaAtalhoPopupMenu(Popupgocliente,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgocliente.OnClick);

	{comando de adi��o de teclas de atalhos gocliente<<<}

	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsgocliente.open();
	{abertura dos datasets<<<}

	fncgocliente.criaAtalhoPopupMenuNavegacao(Popupgocliente,cdsgocliente);

	fncgocliente.definirMascaraCampos(cdsgocliente);

	fncgocliente.configurarGridesFormulario(fgocliente,false, true);

  end);

end;

procedure Tfgocliente.spdAdicionargoclienteClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocliente.TabIndex;
	if (fncgocliente.adicionar(cdsgocliente)=true) then
		begin
			fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,true);
			dbegoclienteid.SetFocus;
	end
	else
		fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,false);
end;

procedure Tfgocliente.spdAlterargoclienteClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgocliente.TabIndex;
	if (fncgocliente.alterar(cdsgocliente)=true) then
		begin
			fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,true);
			dbegoclienteid.SetFocus;
	end
	else
		fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,false);
end;

procedure Tfgocliente.spdGravargoclienteClick(Sender: TObject);
begin
	fncgocliente.verificarCamposObrigatorios(cdsgocliente, camposObrigatoriosgocliente);

	fncgocliente.verificarDuplicidade(dm.fdconexao, cdsgocliente, 'gocliente', 'ID', duplicidadeListagocliente);

	if (fncgocliente.gravar(cdsgocliente)=true) then
		fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,false)
	else
		fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,true);
end;

procedure Tfgocliente.spdCancelargoclienteClick(Sender: TObject);
begin
	if (fncgocliente.cancelar(cdsgocliente)=true) then
		fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,false)
	else
		fncgocliente.ativarModoEdicao(objetosModoEdicaoAtivosgocliente,objetosModoEdicaoInativosgocliente,true);
end;

procedure Tfgocliente.spdExcluirgoclienteClick(Sender: TObject);
begin
	fncgocliente.excluir(cdsgocliente);
end;

procedure Tfgocliente.cdsgoclienteBeforePost(DataSet: TDataSet);
begin
	if cdsgocliente.State in [dsinsert] then
	cdsgoclienteID.Value:=fncgocliente.autoNumeracaoGenerator(dm.conexao,'G_gocliente');
end;

procedure Tfgocliente.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);
    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;
  except
    on e: exception do
    begin
      application.MessageBox(pchar('Sem conex�o de rede.Motivo' + ''#13''#10'' + e.Message), 'Exce��o.', MB_OK);
      application.Terminate;
    end;
  end;
end;

procedure Tfgocliente.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgocliente.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgocliente.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgocliente.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgocliente.IsEmpty then 
	fncgocliente.visualizarRelatorios(frxgocliente);
end;

end.

