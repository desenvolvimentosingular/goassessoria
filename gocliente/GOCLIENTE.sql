CREATE TABLE GOCLIENTE(
ID Integer   
,
CONSTRAINT pk_GOCLIENTE PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
GRUPO Varchar(250)   COLLATE PT_BR
,
TIPO Varchar(30)   COLLATE PT_BR
,
RAZAO_SOCIAL Varchar(250)   COLLATE PT_BR
,
NOME_FANTASIA Varchar(250)   COLLATE PT_BR
,
CNPJ_CPF Varchar(30)  not null COLLATE PT_BR
,
UF Varchar(2)   COLLATE PT_BR
,
CEP Varchar(15)   COLLATE PT_BR
,
CIDADE Varchar(250)   COLLATE PT_BR
,
BAIRRO Varchar(250)   COLLATE PT_BR
,
ENDERECO Varchar(250)   COLLATE PT_BR
,
NUMERO Varchar(30)   COLLATE PT_BR
,
COMPLEMENTO Varchar(250)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOCL_IENTE_ID ON GOCLIENTE (ID);
CREATE ASC INDEX I02_GOCL_IENTE_DT_CADASTRO ON GOCLIENTE (DT_CADASTRO);
CREATE ASC INDEX I03_GOCL_IENTE_HS_CADASTRO ON GOCLIENTE (HS_CADASTRO);
CREATE ASC INDEX I04_GOCL_IENTE_GRUPO ON GOCLIENTE (GRUPO);
CREATE ASC INDEX I05_GOCL_IENTE_TIPO ON GOCLIENTE (TIPO);
CREATE ASC INDEX I06_GOCL_IENTE_RAZAO_SOCIAL ON GOCLIENTE (RAZAO_SOCIAL);
CREATE ASC INDEX I07_GOCL_IENTE_NOME_FANTASIA ON GOCLIENTE (NOME_FANTASIA);
CREATE ASC INDEX I08_GOCL_IENTE_CNPJ_CPF ON GOCLIENTE (CNPJ_CPF);
CREATE ASC INDEX I09_GOCL_IENTE_UF ON GOCLIENTE (UF);
CREATE ASC INDEX I10_GOCL_IENTE_CEP ON GOCLIENTE (CEP);
CREATE ASC INDEX I11_GOCL_IENTE_CIDADE ON GOCLIENTE (CIDADE);
CREATE ASC INDEX I12_GOCL_IENTE_BAIRRO ON GOCLIENTE (BAIRRO);
CREATE ASC INDEX I13_GOCL_IENTE_ENDERECO ON GOCLIENTE (ENDERECO);
CREATE ASC INDEX I14_GOCL_IENTE_NUMERO ON GOCLIENTE (NUMERO);
CREATE ASC INDEX I15_GOCL_IENTE_COMPLEMENTO ON GOCLIENTE (COMPLEMENTO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data Cadastro' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora cadastro' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Grupo cliente' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'GRUPO';
update rdb$relation_fields set rdb$description = 'Tipo' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'TIPO';
update rdb$relation_fields set rdb$description = 'Raz�o Social' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'RAZAO_SOCIAL';
update rdb$relation_fields set rdb$description = 'Nome Fantasia' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'NOME_FANTASIA';
update rdb$relation_fields set rdb$description = 'CNPJ_CPF' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'CNPJ_CPF';
update rdb$relation_fields set rdb$description = 'UF' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'UF';
update rdb$relation_fields set rdb$description = 'CEP' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'CEP';
update rdb$relation_fields set rdb$description = 'Cidade' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'CIDADE';
update rdb$relation_fields set rdb$description = 'Bairro' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'BAIRRO';
update rdb$relation_fields set rdb$description = 'Endere�o' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'ENDERECO';
update rdb$relation_fields set rdb$description = 'N�mero' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'NUMERO';
update rdb$relation_fields set rdb$description = 'Complemento' where rdb$relation_name = 'GOCLIENTE' and rdb$field_name = 'COMPLEMENTO';

commit;
CREATE GENERATOR G_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER BANCO_GEN FOR BANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_CONTA_BANCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER CONTA_BANCO_GEN FOR CONTA_BANCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_CONTA_BANCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_CPG;
commit;
SET TERM  ^^ ;
CREATE TRIGGER CPG_GEN FOR CPG ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_CPG, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPROJETO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPROJETO_GEN FOR GOGRUPOPROJETO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPROJETO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPAINELRISCO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPAINELRISCO_GEN FOR GOPAINELRISCO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPAINELRISCO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOCONSULTOR_GEN FOR GOPROJETOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFILIAL_GEN FOR GOPROJETOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOFUNDO_GEN FOR GOPROJETOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETOS;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETOS_GEN FOR GOPROJETOS ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETOS, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOSUPERVISOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOSUPERVISOR_GEN FOR GOSUPERVISOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOSUPERVISOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOTIPOFUNDO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOTIPOFUNDO_GEN FOR GOTIPOFUNDO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOTIPOFUNDO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFATURAMENTO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFATURAMENTO_GEN FOR GOFATURAMENTO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFATURAMENTO, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCLIENTE;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCLIENTE_GEN FOR GOCLIENTE ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCLIENTE, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
