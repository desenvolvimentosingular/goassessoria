CREATE TABLE GOBANCOCONTA(
CODBANCO Varchar(30)   COLLATE PT_BR
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CONTA Varchar(250)   COLLATE PT_BR
,
TIPO_CONTA Varchar(250)   COLLATE PT_BR
,
CODFILIAL Integer   
,
FILIAL Varchar(250)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOBA_CONTA_CODBANCO ON GOBANCOCONTA (CODBANCO);
CREATE ASC INDEX I02_GOBA_CONTA_DT_CADASTRO ON GOBANCOCONTA (DT_CADASTRO);
CREATE ASC INDEX I03_GOBA_CONTA_HS_CADASTRO ON GOBANCOCONTA (HS_CADASTRO);
CREATE ASC INDEX I04_GOBA_CONTA_CONTA ON GOBANCOCONTA (CONTA);
CREATE ASC INDEX I05_GOBA_CONTA_TIPO_CONTA ON GOBANCOCONTA (TIPO_CONTA);
CREATE ASC INDEX I06_GOBA_CONTA_CODFILIAL ON GOBANCOCONTA (CODFILIAL);
CREATE ASC INDEX I07_GOBA_CONTA_FILIAL ON GOBANCOCONTA (FILIAL);

commit;
update rdb$relation_fields set rdb$description = 'C�d.Banco' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'CODBANCO';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Conta' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'CONTA';
update rdb$relation_fields set rdb$description = 'Tipo de Conta' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'TIPO_CONTA';
update rdb$relation_fields set rdb$description = 'C�d.Filial' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'CODFILIAL';
update rdb$relation_fields set rdb$description = 'Filial' where rdb$relation_name = 'GOBANCOCONTA' and rdb$field_name = 'FILIAL';

commit;

commit;
