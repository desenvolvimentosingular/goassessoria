CREATE TABLE GOCONTROLERECEBIVEL(
ID Integer   
,
CONSTRAINT pk_GOCONTROLERECEBIVEL PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODPORTADOR Integer   
,
PORTADOR Varchar(250)   COLLATE PT_BR
,
CODFILIAL Integer   
,
FILIAL Varchar(250)   COLLATE PT_BR
,
DTCOMPETENCIA Date  not null 
,
CODCARTEIRA Integer   
,
CARTEIRA Varchar(250)   COLLATE PT_BR
,
CODTIPO Integer   
,
TIPO Varchar(250)   COLLATE PT_BR
,
NUMBORDERO Double Precision   
,
QTTITULOS Integer   
,
VLBRUTOBORDERO Double Precision DEFAULT 0,00  
,
QTDRECUSADA Integer   
,
VLBRUTORECUSADO Double Precision DEFAULT 0,00  
,
PRAZOMEDIO Double Precision   
,
PRAZOMEDIOACORDADO Double Precision DEFAULT 0,00  
,
TAC Double Precision DEFAULT 0,00  
,
TARIFAENTRADATITULOS Double Precision DEFAULT 0,00  
,
DESAGIOACORDADO Double Precision DEFAULT 0,00  
,
CALCULOIOF Double Precision DEFAULT 0,00  
,
OUTRASDESPESAS Double Precision DEFAULT 0,00  
,
PERCTAXANOMINALACORDADA Double Precision DEFAULT 0,00  
,
VLCOBERTURAFORMENTO Double Precision DEFAULT 0,00  
,
VLDESTINADOENCARGOFOMENTO Double Precision DEFAULT 0,00  
,
VLDESTINADORECOMPORATITULO Double Precision DEFAULT 0,00  
,
VLDESTINADODEFASAGEM Double Precision DEFAULT 0,00  
,
VLDESTINADOCOMISSARIA Double Precision DEFAULT 0,00  
,
VLDESTINADOGARANTIA Double Precision DEFAULT 0,00  
,
VLDESTINADORETENCAODIVIDA Double Precision DEFAULT 0,00  
,
VLDESTINADOCONTAGRAFICA Double Precision DEFAULT 0,00  
,
CUSTOSFORAOPERACAO Double Precision DEFAULT 0,00  
,
VLOUTROSCREDITOS Double Precision DEFAULT 0,00  
,
VLRECEBIDOCAIXA Double Precision DEFAULT 0,00  
,
DTCREDITO Date   
,
DTINDICE Date   
);
commit;
CREATE ASC INDEX I01_GOCO_BIVEL_ID ON GOCONTROLERECEBIVEL (ID);
CREATE ASC INDEX I02_GOCO_BIVEL_DT_CADASTRO ON GOCONTROLERECEBIVEL (DT_CADASTRO);
CREATE ASC INDEX I03_GOCO_BIVEL_HS_CADASTRO ON GOCONTROLERECEBIVEL (HS_CADASTRO);
CREATE ASC INDEX I04_GOCO_BIVEL_CODPORTADOR ON GOCONTROLERECEBIVEL (CODPORTADOR);
CREATE ASC INDEX I05_GOCO_BIVEL_PORTADOR ON GOCONTROLERECEBIVEL (PORTADOR);
CREATE ASC INDEX I06_GOCO_BIVEL_CODFILIAL ON GOCONTROLERECEBIVEL (CODFILIAL);
CREATE ASC INDEX I07_GOCO_BIVEL_FILIAL ON GOCONTROLERECEBIVEL (FILIAL);
CREATE ASC INDEX I08_GOCO_BIVEL_DTCOMPETENCIA ON GOCONTROLERECEBIVEL (DTCOMPETENCIA);
CREATE ASC INDEX I09_GOCO_BIVEL_CODCARTEIRA ON GOCONTROLERECEBIVEL (CODCARTEIRA);
CREATE ASC INDEX I10_GOCO_BIVEL_CARTEIRA ON GOCONTROLERECEBIVEL (CARTEIRA);
CREATE ASC INDEX I11_GOCO_BIVEL_CODTIPO ON GOCONTROLERECEBIVEL (CODTIPO);
CREATE ASC INDEX I12_GOCO_BIVEL_TIPO ON GOCONTROLERECEBIVEL (TIPO);
CREATE ASC INDEX I13_GOCO_BIVEL_NUMBORDERO ON GOCONTROLERECEBIVEL (NUMBORDERO);
CREATE ASC INDEX I14_GOCO_BIVEL_QTTITULOS ON GOCONTROLERECEBIVEL (QTTITULOS);
CREATE ASC INDEX I15_GOCO_BIVEL_VLBRUTOBORDERO ON GOCONTROLERECEBIVEL (VLBRUTOBORDERO);
CREATE ASC INDEX I16_GOCO_BIVEL_QTDRECUSADA ON GOCONTROLERECEBIVEL (QTDRECUSADA);
CREATE ASC INDEX I17_GOCO_BIVEL_VLBRUTORECUSADO ON GOCONTROLERECEBIVEL (VLBRUTORECUSADO);
CREATE ASC INDEX I18_GOCO_BIVEL_PRAZOMEDIO ON GOCONTROLERECEBIVEL (PRAZOMEDIO);
CREATE ASC INDEX I19_GOCO_BIVEL_PRAZOMEDIOACORDAD ON GOCONTROLERECEBIVEL (PRAZOMEDIOACORDADO);
CREATE ASC INDEX I20_GOCO_BIVEL_TAC ON GOCONTROLERECEBIVEL (TAC);
CREATE ASC INDEX I21_GOCO_BIVEL_TARIFAENTRADATITU ON GOCONTROLERECEBIVEL (TARIFAENTRADATITULOS);
CREATE ASC INDEX I22_GOCO_BIVEL_DESAGIOACORDADO ON GOCONTROLERECEBIVEL (DESAGIOACORDADO);
CREATE ASC INDEX I23_GOCO_BIVEL_CALCULOIOF ON GOCONTROLERECEBIVEL (CALCULOIOF);
CREATE ASC INDEX I24_GOCO_BIVEL_OUTRASDESPESAS ON GOCONTROLERECEBIVEL (OUTRASDESPESAS);
CREATE ASC INDEX I25_GOCO_BIVEL_PERCTAXANOMINALAC ON GOCONTROLERECEBIVEL (PERCTAXANOMINALACORDADA);
CREATE ASC INDEX I26_GOCO_BIVEL_VLCOBERTURAFORMEN ON GOCONTROLERECEBIVEL (VLCOBERTURAFORMENTO);
CREATE ASC INDEX I27_GOCO_BIVEL_VLDESTINADOENCARG ON GOCONTROLERECEBIVEL (VLDESTINADOENCARGOFOMENTO);
CREATE ASC INDEX I28_GOCO_BIVEL_VLDESTINADORECOMP ON GOCONTROLERECEBIVEL (VLDESTINADORECOMPORATITULO);
CREATE ASC INDEX I29_GOCO_BIVEL_VLDESTINADODEFASA ON GOCONTROLERECEBIVEL (VLDESTINADODEFASAGEM);
CREATE ASC INDEX I30_GOCO_BIVEL_VLDESTINADOCOMISS ON GOCONTROLERECEBIVEL (VLDESTINADOCOMISSARIA);
CREATE ASC INDEX I31_GOCO_BIVEL_VLDESTINADOGARANT ON GOCONTROLERECEBIVEL (VLDESTINADOGARANTIA);
CREATE ASC INDEX I32_GOCO_BIVEL_VLDESTINADORETENC ON GOCONTROLERECEBIVEL (VLDESTINADORETENCAODIVIDA);
CREATE ASC INDEX I33_GOCO_BIVEL_VLDESTINADOCONTAG ON GOCONTROLERECEBIVEL (VLDESTINADOCONTAGRAFICA);
CREATE ASC INDEX I34_GOCO_BIVEL_CUSTOSFORAOPERACA ON GOCONTROLERECEBIVEL (CUSTOSFORAOPERACAO);
CREATE ASC INDEX I35_GOCO_BIVEL_VLOUTROSCREDITOS ON GOCONTROLERECEBIVEL (VLOUTROSCREDITOS);
CREATE ASC INDEX I36_GOCO_BIVEL_VLRECEBIDOCAIXA ON GOCONTROLERECEBIVEL (VLRECEBIDOCAIXA);
CREATE ASC INDEX I37_GOCO_BIVEL_DTCREDITO ON GOCONTROLERECEBIVEL (DTCREDITO);
CREATE ASC INDEX I38_GOCO_BIVEL_DTINDICE ON GOCONTROLERECEBIVEL (DTINDICE);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Portador' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CODPORTADOR';
update rdb$relation_fields set rdb$description = 'Portador' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'PORTADOR';
update rdb$relation_fields set rdb$description = 'C�d.Filial' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CODFILIAL';
update rdb$relation_fields set rdb$description = 'Filial' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'FILIAL';
update rdb$relation_fields set rdb$description = 'Data Compet�ncia' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'DTCOMPETENCIA';
update rdb$relation_fields set rdb$description = 'C�d. Carteira' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CODCARTEIRA';
update rdb$relation_fields set rdb$description = 'Carteira' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CARTEIRA';
update rdb$relation_fields set rdb$description = 'C�d.Tipo' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CODTIPO';
update rdb$relation_fields set rdb$description = 'Tipo' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'TIPO';
update rdb$relation_fields set rdb$description = 'N� de border�s' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'NUMBORDERO';
update rdb$relation_fields set rdb$description = 'Qtd. T�tulos' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'QTTITULOS';
update rdb$relation_fields set rdb$description = 'Valor bruto border�' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLBRUTOBORDERO';
update rdb$relation_fields set rdb$description = 'Qtd. Recusada' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'QTDRECUSADA';
update rdb$relation_fields set rdb$description = 'Valor Bruto Recusado' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLBRUTORECUSADO';
update rdb$relation_fields set rdb$description = 'Prazo M�dio' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'PRAZOMEDIO';
update rdb$relation_fields set rdb$description = 'Prazo M�dio Acordado' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'PRAZOMEDIOACORDADO';
update rdb$relation_fields set rdb$description = 'TAC' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'TAC';
update rdb$relation_fields set rdb$description = 'Tarifa entrada de T�tulos' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'TARIFAENTRADATITULOS';
update rdb$relation_fields set rdb$description = 'Des�gio acordado' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'DESAGIOACORDADO';
update rdb$relation_fields set rdb$description = 'C�lculo IOF sobre Valor L�quido' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CALCULOIOF';
update rdb$relation_fields set rdb$description = 'Outras Despesas' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'OUTRASDESPESAS';
update rdb$relation_fields set rdb$description = '% taxa nominal acordada' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'PERCTAXANOMINALACORDADA';
update rdb$relation_fields set rdb$description = 'Valor Destinado Cobertura Fomento' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLCOBERTURAFORMENTO';
update rdb$relation_fields set rdb$description = 'Valor Destinado Encargo Fomento' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADOENCARGOFOMENTO';
update rdb$relation_fields set rdb$description = 'Valor Destinado Recompra T�tulos' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADORECOMPORATITULO';
update rdb$relation_fields set rdb$description = 'Valor Destinado Defasagem' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADODEFASAGEM';
update rdb$relation_fields set rdb$description = 'Valor Destinado Comiss�ria' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADOCOMISSARIA';
update rdb$relation_fields set rdb$description = 'Valor Destinado Garantia' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADOGARANTIA';
update rdb$relation_fields set rdb$description = 'Valor Destinado Reten��o D�vida' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADORETENCAODIVIDA';
update rdb$relation_fields set rdb$description = 'Valor Destinado Conta Gr�fica' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLDESTINADOCONTAGRAFICA';
update rdb$relation_fields set rdb$description = 'Custos fora opera��o(Ex.TED, DOC, ETC)' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'CUSTOSFORAOPERACAO';
update rdb$relation_fields set rdb$description = 'Valor recebido outros cr�ditos' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLOUTROSCREDITOS';
update rdb$relation_fields set rdb$description = 'Valor Recebido Caixa' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'VLRECEBIDOCAIXA';
update rdb$relation_fields set rdb$description = 'Data Cr�dito' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'DTCREDITO';
update rdb$relation_fields set rdb$description = 'Data �ndice' where rdb$relation_name = 'GOCONTROLERECEBIVEL' and rdb$field_name = 'DTINDICE';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
