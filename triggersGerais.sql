
SET TERM ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_DEL_CASC FOR GOGRUPOPLANOCONTA ACTIVE AFTER DELETE POSITION 0 AS 
begin
	delete from GOPLANOCONTA
	where codgrupo = old.id;
end ^^
SET TERM ; ^^
commit;
SET TERM ^^ ;
CREATE TRIGGER GOPROJETOS_DEL_CASC FOR GOPROJETOS ACTIVE AFTER DELETE POSITION 0 AS 
begin
	delete from GOSUPERVISOR
	where codprojeto = old.id;
	delete from GOPROJETOCONSULTOR
	where codprojeto = old.id;
	delete from GOPROJETOFILIAL
	where codprojeto = old.id;
	delete from GOPROJETOFUNDO
	where codprojeto = old.id;
end ^^
SET TERM ; ^^
commit;
SET TERM ^^ ;
CREATE TRIGGER GOSUPERVISOR_DEL_CASC FOR GOSUPERVISOR ACTIVE AFTER DELETE POSITION 0 AS 
begin
	delete from GOCONSULTOR
	where cod_supervisor = old.id;
end ^^
SET TERM ; ^^
commit;

