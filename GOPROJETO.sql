CREATE TABLE GOPROJETO(
ID Integer   
,
CONSTRAINT pk_GOPROJETO PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
CODGRUPO Integer   
,
GRUPO Varchar(250)   COLLATE PT_BR
,
DESCRICAO Varchar(250)  not null COLLATE PT_BR
,
TIPO Varchar(250)  not null COLLATE PT_BR
,
DT_INICIO Date   
,
DT_TERMINO Date   
,
STATUS Varchar(30)  not null COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOPR_OJETO_ID ON GOPROJETO (ID);
CREATE ASC INDEX I02_GOPR_OJETO_DT_CADASTRO ON GOPROJETO (DT_CADASTRO);
CREATE ASC INDEX I03_GOPR_OJETO_HS_CADASTRO ON GOPROJETO (HS_CADASTRO);
CREATE ASC INDEX I04_GOPR_OJETO_CODGRUPO ON GOPROJETO (CODGRUPO);
CREATE ASC INDEX I05_GOPR_OJETO_GRUPO ON GOPROJETO (GRUPO);
CREATE ASC INDEX I06_GOPR_OJETO_DESCRICAO ON GOPROJETO (DESCRICAO);
CREATE ASC INDEX I07_GOPR_OJETO_TIPO ON GOPROJETO (TIPO);
CREATE ASC INDEX I08_GOPR_OJETO_DT_INICIO ON GOPROJETO (DT_INICIO);
CREATE ASC INDEX I09_GOPR_OJETO_DT_TERMINO ON GOPROJETO (DT_TERMINO);
CREATE ASC INDEX I10_GOPR_OJETO_STATUS ON GOPROJETO (STATUS);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'C�d.Grupo' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'CODGRUPO';
update rdb$relation_fields set rdb$description = 'Grupo' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'GRUPO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'DESCRICAO';
update rdb$relation_fields set rdb$description = 'Tipo de Projeto' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'TIPO';
update rdb$relation_fields set rdb$description = 'Data de In�cio' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'DT_INICIO';
update rdb$relation_fields set rdb$description = 'Data de T�rmino' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'DT_TERMINO';
update rdb$relation_fields set rdb$description = 'Status' where rdb$relation_name = 'GOPROJETO' and rdb$field_name = 'STATUS';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPROJETO;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPROJETO_GEN FOR GOPROJETO ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPROJETO, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
