program empresa;

uses
  Vcl.Forms,
  uempresa in 'uempresa.pas' {fempresa},
  udm in '..\class_shared\udm.pas' {dm: TDataModule},
  ulogin in '..\class_shared\ulogin.pas' {flogin},
  upreview in '..\class_shared\upreview.pas' {fpreview},
  uprocesso in '..\class_shared\uprocesso.pas' {fprocesso},
  uFuncoes in '..\class_shared\uFuncoes.pas',
  Vcl.DialogMessage in '..\class_shared\Vcl.DialogMessage.pas',
  utrocarsenha in '..\class_shared\utrocarsenha.pas' {fTrocarSenha},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Turquoise Gray');
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(Tfempresa, fempresa);
  Application.Run;
end.
