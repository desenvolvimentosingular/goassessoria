unit uempresa;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 17/02/2020*}                                          
{*Hora 16:31:34*}                                            
{*Unit empresa *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes,
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, frxClass, frxDBSet, cxClasses, Data.SqlExpr,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, cxPC, dxGDIPlusClasses, cxContainer, cxImage,
  cxDBEdit, dxSkinsCore, dxSkinSevenClassic, dxSkinscxPCPainter;

type                                                                                        
	Tfempresa = class(TForm)
  sqlempresa: TSQLDataSet;
	dspempresa: TDataSetProvider;
	cdsempresa: TClientDataSet;
	dsempresa: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	Popupempresa: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabempresa: tcxTabSheet;
	SCRempresa: TScrollBox;
	pnlDadosempresa: TPanel;
	{>>>bot�es de manipula��o de dados empresa}
	pnlLinhaBotoesempresa: TPanel;
	spdAdicionarempresa: TButton;
	spdAlterarempresa: TButton;
	spdGravarempresa: TButton;
	spdCancelarempresa: TButton;
	spdExcluirempresa: TButton;
	cdsempresaID_EMPRESA: TIntegerField;
	pnlLayoutLinhasempresa1: TPanel;
	pnlLayoutTitulosempresaid_empresa: TPanel;
	pnlLayoutCamposempresaid_empresa: TPanel;
	dbeempresaid_empresa: TDBEdit;
	cdsempresaRAZAO_SOCIAL: TStringField;
	pnlLayoutLinhasempresa2: TPanel;
	pnlLayoutTitulosempresarazao_social: TPanel;
	pnlLayoutCamposempresarazao_social: TPanel;
	dbeempresarazao_social: TDBEdit;
	cdsempresaNOME_FANTASIA: TStringField;
	pnlLayoutLinhasempresa3: TPanel;
	pnlLayoutTitulosempresanome_fantasia: TPanel;
	pnlLayoutCamposempresanome_fantasia: TPanel;
	dbeempresanome_fantasia: TDBEdit;
	cdsempresaRESPONSAVEL: TStringField;
	pnlLayoutLinhasempresa4: TPanel;
	pnlLayoutTitulosempresaresponsavel: TPanel;
	pnlLayoutCamposempresaresponsavel: TPanel;
	dbeempresaresponsavel: TDBEdit;
	cdsempresaCNPJ: TStringField;
	pnlLayoutLinhasempresa5: TPanel;
	pnlLayoutTitulosempresacnpj: TPanel;
	pnlLayoutCamposempresacnpj: TPanel;
	dbeempresacnpj: TDBEdit;
	cdsempresaIE: TStringField;
	pnlLayoutLinhasempresa6: TPanel;
	pnlLayoutTitulosempresaie: TPanel;
	pnlLayoutCamposempresaie: TPanel;
	dbeempresaie: TDBEdit;
	cdsempresaENDERECO: TStringField;
	pnlLayoutLinhasempresa7: TPanel;
	pnlLayoutTitulosempresaendereco: TPanel;
	pnlLayoutCamposempresaendereco: TPanel;
	dbeempresaendereco: TDBEdit;
	cdsempresaCOMPLEMENTO: TStringField;
	pnlLayoutLinhasempresa8: TPanel;
	pnlLayoutTitulosempresacomplemento: TPanel;
	pnlLayoutCamposempresacomplemento: TPanel;
	dbeempresacomplemento: TDBEdit;
	cdsempresaBAIRRO: TStringField;
	pnlLayoutLinhasempresa9: TPanel;
	pnlLayoutTitulosempresabairro: TPanel;
	pnlLayoutCamposempresabairro: TPanel;
	dbeempresabairro: TDBEdit;
	cdsempresaCIDADE: TStringField;
	pnlLayoutLinhasempresa10: TPanel;
	pnlLayoutTitulosempresacidade: TPanel;
	pnlLayoutCamposempresacidade: TPanel;
	dbeempresacidade: TDBEdit;
	cdsempresaUF: TStringField;
	pnlLayoutLinhasempresa11: TPanel;
	pnlLayoutTitulosempresauf: TPanel;
	pnlLayoutCamposempresauf: TPanel;
	cdsempresaCEP: TStringField;
	pnlLayoutLinhasempresa12: TPanel;
	pnlLayoutTitulosempresacep: TPanel;
	pnlLayoutCamposempresacep: TPanel;
	dbeempresacep: TDBEdit;
	cdsempresaEMAIL: TStringField;
	pnlLayoutLinhasempresa13: TPanel;
	pnlLayoutTitulosempresaemail: TPanel;
	pnlLayoutCamposempresaemail: TPanel;
	dbeempresaemail: TDBEdit;
	cdsempresaSITE: TStringField;
	pnlLayoutLinhasempresa14: TPanel;
	pnlLayoutTitulosempresasite: TPanel;
	pnlLayoutCamposempresasite: TPanel;
	dbeempresasite: TDBEdit;
	cdsempresaFONE1: TStringField;
	pnlLayoutLinhasempresa15: TPanel;
	pnlLayoutTitulosempresafone1: TPanel;
	pnlLayoutCamposempresafone1: TPanel;
	dbeempresafone1: TDBEdit;
	cdsempresaFONE2: TStringField;
	pnlLayoutLinhasempresa16: TPanel;
	pnlLayoutTitulosempresafone2: TPanel;
	pnlLayoutCamposempresafone2: TPanel;
	dbeempresafone2: TDBEdit;
	cdsempresaFAX: TStringField;
	pnlLayoutLinhasempresa17: TPanel;
	pnlLayoutTitulosempresafax: TPanel;
	pnlLayoutCamposempresafax: TPanel;
	dbeempresafax: TDBEdit;
	pnlLayoutLinhasempresa18: TPanel;
	pnlLayoutTitulosempresalogo_img: TPanel;
	pnlLayoutCamposempresalogo_img: TPanel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    dbeempresaUF: TDBComboBox;
    file_caminho_img: TFileOpenDialog;
    spb_origem_banco: TButton;
    frxempresa: TfrxReport;
    frxDBempresa: TfrxDBDataset;
    cdsempresaLOGO_IMG: TBlobField;
    dbeempresaLOGO_IMG: TDBImage;
    frxDBDfiltros: TfrxDBDataset;
    cdsfiltros: TClientDataSet;
    PopupOpcao: TPopupMenu;
    Relatriosinttico2: TMenuItem;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionarempresaClick(Sender: TObject);
	procedure spdAlterarempresaClick(Sender: TObject);
	procedure spdGravarempresaClick(Sender: TObject);
	procedure spdCancelarempresaClick(Sender: TObject);
	procedure spdExcluirempresaClick(Sender: TObject);
	procedure cdsempresaBeforePost(DataSet: TDataSet);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure spb_origem_bancoClick(Sender: TObject);
    procedure spdExportacaoClick(Sender: TObject);
    procedure frxempresaBeforePrint(Sender: TfrxReportComponent);
    procedure spdOpcoesClick(Sender: TObject);
    procedure Relatriosinttico2Click(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	//fncempresa: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosempresa: Array[0..6]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosempresa: Array[0..10]  of TControl;
	objetosModoEdicaoInativosempresa: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListaempresa: Array[0..2]  of TDuplicidade;
	duplicidadeCampoempresa: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
  procedure sessao;
    procedure permissao_agrupada;
	public
  fncempresa: TFuncoes;
  const rotina='Cadastro de Empresa';

end;

var
fempresa: Tfempresa;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfempresa.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfempresa.frxempresaBeforePrint(Sender: TfrxReportComponent);
begin
// carregarlogomarcarelatorio;
end;

procedure Tfempresa.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabempresa.TabIndex;
end;

procedure Tfempresa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fncempresa.configurarGridesFormulario(fempresa, true, false);
	{>>>verificando exist�ncia de registros em transa��o}
	fncempresa.verificarEmTransacao(cdsempresa);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncempresa.Free;
	{eliminando container de fun��es da memoria<<<}

	fempresa:=nil;
	Action:=cafree;
end;

procedure Tfempresa.FormCreate(Sender: TObject);
begin
    sessao;
   {>>>container de tradu�a� de compomentes}
    if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
    begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
    end;
  {>>>container de tradu�a� de compomentes}

	{>>>container de fun��es}
	fncempresa:= TFuncoes.Create(Self);
	fncempresa.definirConexao(dm.conexao);
  fncempresa.definirConexaohistorico(dm.conexao);
	fncempresa.definirFormulario(Self);
	fncempresa.validarTransacaoRelacionamento:=true;
	fncempresa.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

  {<<vers�o da rotina>>}
  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncempresa.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}

	{>>>verificar campos duplicidade}
	duplicidadeCampoempresa:=TDuplicidade.Create;
	duplicidadeCampoempresa.agrupador:='';
	duplicidadeCampoempresa.objeto :=dbeempresarazao_social;
	duplicidadeListaempresa[0]:=duplicidadeCampoempresa;

	duplicidadeCampoempresa:=TDuplicidade.Create;
	duplicidadeCampoempresa.agrupador:='';
	duplicidadeCampoempresa.objeto :=dbeempresanome_fantasia;
	duplicidadeListaempresa[1]:=duplicidadeCampoempresa;

	duplicidadeCampoempresa:=TDuplicidade.Create;
	duplicidadeCampoempresa.agrupador:='';
	duplicidadeCampoempresa.objeto :=dbeempresacnpj;
	duplicidadeListaempresa[2]:=duplicidadeCampoempresa;

	{verificar campos duplicidade<<<}

  {gerando campos no clientdatsaet de filtros}
  fncempresa.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	{>>>gerando objetos de pesquisas}
	fncempresa.criarPesquisas(sqlempresa,cdsempresa,'select * from goempresa', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios empresa}
	camposObrigatoriosempresa[0] :=dbeempresarazao_social;
	camposObrigatoriosempresa[1] :=dbeempresanome_fantasia;
	camposObrigatoriosempresa[2] :=dbeempresaresponsavel;
	camposObrigatoriosempresa[3] :=dbeempresacidade;
	camposObrigatoriosempresa[4] :=dbeempresauf;
	camposObrigatoriosempresa[5] :=dbeempresafone1;
	{campos obrigatorios empresa<<<}

	{>>>objetos ativos no modo de manipula��o de dados empresa}
	objetosModoEdicaoAtivosempresa[0]:=pnlDadosempresa;
	objetosModoEdicaoAtivosempresa[1]:=spdCancelarempresa;
	objetosModoEdicaoAtivosempresa[2]:=spdGravarempresa;
	{objetos ativos no modo de manipula��o de dados empresa<<<}

	{>>>objetos inativos no modo de manipula��o de dados empresa}
	objetosModoEdicaoInativosempresa[0]:=spdAdicionarempresa;
	objetosModoEdicaoInativosempresa[1]:=spdAlterarempresa;
	objetosModoEdicaoInativosempresa[2]:=spdExcluirempresa;
	objetosModoEdicaoInativosempresa[3]:=spdOpcoes;
	objetosModoEdicaoInativosempresa[6]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados empresa<<<}

	{>>>comando de adi��o de teclas de atalhos empresa}
	fncempresa.criaAtalhoPopupMenu(Popupempresa,'Adicionar novo registro',VK_F4,spdAdicionarempresa.OnClick);
	fncempresa.criaAtalhoPopupMenu(Popupempresa,'Alterar registro selecionado',VK_F5,spdAlterarempresa.OnClick);
	fncempresa.criaAtalhoPopupMenu(Popupempresa,'Gravar �ltimas altera��es',VK_F6,spdGravarempresa.OnClick);
	fncempresa.criaAtalhoPopupMenu(Popupempresa,'Cancelar �ltimas altera��e',VK_F7,spdCancelarempresa.OnClick);
	fncempresa.criaAtalhoPopupMenu(Popupempresa,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirempresa.OnClick);
	{comando de adi��o de teclas de atalhos empresa<<<}

  if fncempresa.permissao('goempresa','Acesso',ParamStr(3))=false then
  begin
    application.MessageBox('Sem permiss�o de acesso.','Permiss�o',MB_ICONEXCLAMATION);
    application.Terminate;
  end;

  spdAdicionarempresa.Enabled  := fncempresa.permissao('goempresa','Adicionar',ParamStr(3));
  spdAlterarempresa.Enabled    := fncempresa.permissao('goempresa','Alterar',ParamStr(3));
  spdExcluirempresa.Enabled    := fncempresa.permissao('goempresa','Excluir',ParamStr(3));
  menuImpRelSintetico.Enabled  := fncempresa.permissao('goempresa','Relatorio',ParamStr(3));

  Popupempresa.Items[0].Enabled:= fncempresa.permissao('goempresa','Adicionar',ParamStr(3));
  Popupempresa.Items[1].Enabled:= fncempresa.permissao('goempresa','Alterar' ,ParamStr(3));
  Popupempresa.Items[3].Enabled:= fncempresa.permissao('goempresa','Excluir',ParamStr(3));


	pgcPrincipal.ActivePageIndex:=0;

	{>>>abertura dos datasets}
	cdsempresa.open();
	{abertura dos datasets<<<}

	fncempresa.criaAtalhoPopupMenuNavegacao(Popupempresa,cdsempresa);

	fncempresa.definirMascaraCampos(cdsempresa);

  fncempresa.configurarGridesFormulario(fempresa, true, false);


  fncempresa.configurarGridesFormulario(fempresa, false, true);



end;

procedure Tfempresa.spb_origem_bancoClick(Sender: TObject);
begin
 file_caminho_img.Execute;
 if FileExists(file_caminho_img.FileName)then
   cdsempresaLOGO_IMG.AsString  := file_caminho_img.FileName;

end;

procedure Tfempresa.spdAdicionarempresaClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabempresa.TabIndex;
	if (fncempresa.adicionar(cdsempresa)=true) then
		begin
			fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,true);
			dbeempresarazao_social.SetFocus;
	end
	else
		fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,false);
end;

procedure Tfempresa.spdAlterarempresaClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabempresa.TabIndex;
	if (fncempresa.alterar(cdsempresa)=true) then
		begin
			fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,true);
			dbeempresarazao_social.SetFocus;
	end
	else
		fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,false);
end;

procedure Tfempresa.spdGravarempresaClick(Sender: TObject);
begin
	fncempresa.verificarCamposObrigatorios(cdsempresa, camposObrigatoriosempresa, pgcPrincipal);
	fncempresa.verificarDuplicidade(dm.fdconexao, cdsempresa, 'goempresa', 'ID_EMPRESA', duplicidadeListaempresa);

	if (fncempresa.gravar(cdsempresa)=true) then
  begin
		fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,false);
    permissao_agrupada;
  end
	else
		fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,true);
end;

procedure Tfempresa.spdCancelarempresaClick(Sender: TObject);
begin
	if (fncempresa.cancelar(cdsempresa)=true) then
  begin
		fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,false);
    permissao_agrupada;
  end
	else
		fncempresa.ativarModoEdicao(objetosModoEdicaoAtivosempresa,objetosModoEdicaoInativosempresa,true);
end;

procedure Tfempresa.spdExcluirempresaClick(Sender: TObject);
begin
	fncempresa.excluir(cdsempresa);
end;

procedure Tfempresa.spdExportacaoClick(Sender: TObject);
begin
if not cdsempresa.IsEmpty then
 fncempresa.exportarGrides(fempresa, GridResultadoPesquisa);
end;

procedure Tfempresa.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;

end;

procedure Tfempresa.permissao_agrupada;
begin
  if fncempresa.permissao('goempresa', 'Acesso', ParamStr(3)) = false then
  begin
    application.MessageBox('Sem permiss�o de acesso.', 'Permiss�o', MB_ICONEXCLAMATION);
    application.Terminate;
  end;
  spdAdicionarempresa.Enabled := fncempresa.permissao('goempresa', 'Adicionar', ParamStr(3));
  spdAlterarempresa.Enabled := fncempresa.permissao('goempresa', 'Alterar', ParamStr(3));
  spdExcluirempresa.Enabled := fncempresa.permissao('goempresa', 'Excluir', ParamStr(3));
  menuImpRelSintetico.Enabled := fncempresa.permissao('goempresa', 'Relatorio', ParamStr(3));
  Popupempresa.Items[0].Enabled := fncempresa.permissao('goempresa', 'Adicionar', ParamStr(3));
  Popupempresa.Items[1].Enabled := fncempresa.permissao('goempresa', 'Alterar', ParamStr(3));
  Popupempresa.Items[3].Enabled := fncempresa.permissao('goempresa', 'Excluir', ParamStr(3));
end;

procedure Tfempresa.cdsempresaBeforePost(DataSet: TDataSet);
var
  Bmp: TMemoryStream;
begin
       Bmp := TMemoryStream.Create;
       try
          Bmp.LoadFromFile(file_caminho_img.FileName);
          TBlobField(cdsempresaLOGO_IMG).LoadFromStream(Bmp);
          TBlobField(cdsempresaLOGO_IMG).Modified := True;
       finally
          Bmp.Free;
       end;

	if cdsempresa.State in [dsinsert] then
	cdsempresaID_EMPRESA.Value:=fncempresa.autoNumeracaoSelect(dm.conexao,'goempresa','ID_EMPRESA');

end;


procedure Tfempresa.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fempresa.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfempresa.spdImpressaoClick(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Width, TSpeedButton(Sender).Height)) do
  begin
    popupImpressao.Popup(X, Y);
  end;

end;

procedure Tfempresa.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    PopupOpcao.Popup(X, Y);
  end;

end;

procedure Tfempresa.menuImpRelSinteticoClick(Sender: TObject);
begin
 fncempresa.visualizarRelatorios(frxempresa);
end;

procedure Tfempresa.Relatriosinttico2Click(Sender: TObject);
begin
  if not cdsempresa.IsEmpty then
 	fncempresa.visualizarRelatorios(frxempresa);
end;

end.

