unit ugomoeda;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 07/08/2021*}                                          
{*Hora 12:11:21*}                                            
{*Unit gomoeda *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxPC, Data.SqlExpr,
  cxGridLevel, dxGDIPlusClasses;

type                                                                                        
	Tfgomoeda = class(TForm)
    sqlgomoeda: TSQLDataSet;
	dspgomoeda: TDataSetProvider;
	cdsgomoeda: TClientDataSet;
	dsgomoeda: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxgomoeda: TfrxReport;
	frxDBgomoeda: TfrxDBDataset;
	Popupgomoeda: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabgomoeda: tcxTabSheet;
	SCRgomoeda: TScrollBox;
	pnlDadosgomoeda: TPanel;
	{>>>bot�es de manipula��o de dados gomoeda}
	pnlLinhaBotoesgomoeda: TPanel;
	spdAdicionargomoeda: TButton;
	spdAlterargomoeda: TButton;
	spdGravargomoeda: TButton;
	spdCancelargomoeda: TButton;
	spdExcluirgomoeda: TButton;

	{bot�es de manipula��o de dadosgomoeda<<<}
	{campo ID}
	sqlgomoedaID: TIntegerField;
	cdsgomoedaID: TIntegerField;
	pnlLayoutLinhasgomoeda1: TPanel;
	pnlLayoutTitulosgomoedaid: TPanel;
	pnlLayoutCamposgomoedaid: TPanel;
	dbegomoedaid: TDBEdit;

	{campo DT_CADASTRO}
	sqlgomoedaDT_CADASTRO: TDateField;
	cdsgomoedaDT_CADASTRO: TDateField;
	pnlLayoutLinhasgomoeda2: TPanel;
	pnlLayoutTitulosgomoedadt_cadastro: TPanel;
	pnlLayoutCamposgomoedadt_cadastro: TPanel;
	dbegomoedadt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlgomoedaHS_CADASTRO: TTimeField;
	cdsgomoedaHS_CADASTRO: TTimeField;
	pnlLayoutLinhasgomoeda3: TPanel;
	pnlLayoutTitulosgomoedahs_cadastro: TPanel;
	pnlLayoutCamposgomoedahs_cadastro: TPanel;
	dbegomoedahs_cadastro: TDBEdit;

	{campo DESCRICAO}
	sqlgomoedaDESCRICAO: TStringField;
	cdsgomoedaDESCRICAO: TStringField;
	pnlLayoutLinhasgomoeda4: TPanel;
	pnlLayoutTitulosgomoedadescricao: TPanel;
	pnlLayoutCamposgomoedadescricao: TPanel;
	dbegomoedadescricao: TDBEdit;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    cdsfiltros: TClientDataSet;
    frxDBDfiltros: TfrxDBDataset;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionargomoedaClick(Sender: TObject);
	procedure spdAlterargomoedaClick(Sender: TObject);
	procedure spdGravargomoedaClick(Sender: TObject);
	procedure spdCancelargomoedaClick(Sender: TObject);
	procedure spdExcluirgomoedaClick(Sender: TObject);
    procedure GridResultadoPesquisaDBTVDblClick(Sender: TObject);
    procedure cdsgomoedaNewRecord(DataSet: TDataSet);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fncgomoeda: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatoriosgomoeda: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivosgomoeda: Array[0..10]  of TControl;
	objetosModoEdicaoInativosgomoeda: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListagomoeda: Array[0..0]  of TDuplicidade;
	duplicidadeCampogomoeda: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
  procedure sessao;
  procedure permissao;
	public

end;

var
fgomoeda: Tfgomoeda;

implementation

uses udm;

{$R *.dfm}
//{$R logo.res}

procedure Tfgomoeda.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfgomoeda.GridResultadoPesquisaDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgomoeda.TabIndex;
end;

procedure Tfgomoeda.cdsgomoedaNewRecord(DataSet: TDataSet);
begin
	if cdsgomoeda.State in [dsinsert] then
	cdsgomoedaID.Value:=fncgomoeda.autoNumeracaoGenerator(dm.conexao,'G_gomoeda');
end;

procedure Tfgomoeda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fncgomoeda.verificarEmTransacao(cdsgomoeda);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fncgomoeda.configurarGridesFormulario(fgomoeda,true, false);

	fncgomoeda.Free;
	{eliminando container de fun��es da memoria<<<}

	fgomoeda:=nil;
	Action:=cafree;
end;

procedure Tfgomoeda.FormCreate(Sender: TObject);
begin
  sessao;

  {<<>> fun��o de tradu�a� de componentes>>>}
  if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
  begin
   dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
   dm.cxLocalizer.LanguageIndex := 1;
   dm.cxLocalizer.Active := TRUE;
  end;
  {<<>> fun��o de tradu�a� de componentes>>>}

	{>>>container de fun��es}
	fncgomoeda:= TFuncoes.Create(Self);
	fncgomoeda.definirConexao(dm.conexao);
	fncgomoeda.definirConexaohistorico(dm.conexao);
	fncgomoeda.definirFormulario(Self);
	fncgomoeda.validarTransacaoRelacionamento:=true;
	fncgomoeda.autoAplicarAtualizacoesBancoDados:=true;
	{container de fun��es<<<}

	{>>>verificar campos duplicidade}
	duplicidadeCampogomoeda:=TDuplicidade.Create;
	duplicidadeCampogomoeda.agrupador:='';
	duplicidadeCampogomoeda.objeto :=dbegomoedaDESCRICAO;
	duplicidadeListagomoeda[0]     :=duplicidadeCampogomoeda;
	{verificar campos duplicidade<<<}


	{>>>gerando objetos de pesquisas}
	pesquisasItem:=TPesquisas.Create;
	pesquisasItem.agrupador:='pesDESCRICAO';
	pesquisasItem.titulo:='Descri��o';
	pesquisasItem.campo:='DESCRICAO';
	pesquisasItem.tipoFDB:='Varchar';
	pesquisasItem.criterio:='LIKE';
	pesquisasItem.intervalo:='N';
	pesquisasItem.tamanho:=480;
	pesquisasItem.comboItens.clear;
	pesquisasItem.lookupSelect:='';
	pesquisasItem.lookupCampoID:='';
	pesquisasItem.lookupCampoTipoID:='Integer';
	pesquisasItem.lookupCampoDescricao:='';
	pesquisasItem.lookupGridItens.clear;
	pesquisasLista[0]:=pesquisasItem;

  camposObrigatoriosgomoeda[0]:=dbegomoedaDESCRICAO;

  {gerando campos no clientdatsaet de filtros}
  fncgomoeda.carregarfiltros(pesquisasLista,cdsfiltros,flwpOpcoes);
  {gerando campos no clientdatsaet de filtros}

	fncgomoeda.criarPesquisas(sqlgomoeda,cdsgomoeda,'select * from gomoeda', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil, cdsfiltros);
	{gerando objetos de pesquisas<<<}

	{>>>campos obrigatorios gomoeda}
	{campos obrigatorios gomoeda<<<}

	{>>>objetos ativos no modo de manipula��o de dados gomoeda}
 //	objetosModoEdicaoAtivosgomoeda[0]:=pnlDadosgomoeda;
	objetosModoEdicaoAtivosgomoeda[1]:=spdCancelargomoeda;
	objetosModoEdicaoAtivosgomoeda[2]:=spdGravargomoeda;
	{objetos ativos no modo de manipula��o de dados gomoeda<<<}

	{>>>objetos inativos no modo de manipula��o de dados gomoeda}
	objetosModoEdicaoInativosgomoeda[0]:=spdAdicionargomoeda;
	objetosModoEdicaoInativosgomoeda[1]:=spdAlterargomoeda;
	objetosModoEdicaoInativosgomoeda[2]:=spdExcluirgomoeda;
	objetosModoEdicaoInativosgomoeda[3]:=spdOpcoes;
	objetosModoEdicaoInativosgomoeda[4]:=tabPesquisas;
	{objetos inativos no modo de manipula��o de dados gomoeda<<<}

	{>>>comando de adi��o de teclas de atalhos gomoeda}
	fncgomoeda.criaAtalhoPopupMenu(Popupgomoeda,'Adicionar novo registro',VK_F4,spdAdicionargomoeda.OnClick);
	fncgomoeda.criaAtalhoPopupMenu(Popupgomoeda,'Alterar registro selecionado',VK_F5,spdAlterargomoeda.OnClick);
	fncgomoeda.criaAtalhoPopupMenu(Popupgomoeda,'Gravar �ltimas altera��es',VK_F6,spdGravargomoeda.OnClick);
	fncgomoeda.criaAtalhoPopupMenu(Popupgomoeda,'Cancelar �ltimas altera��e',VK_F7,spdCancelargomoeda.OnClick);
	fncgomoeda.criaAtalhoPopupMenu(Popupgomoeda,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluirgomoeda.OnClick);

	{comando de adi��o de teclas de atalhos gomoeda<<<}

	pgcPrincipal.ActivePageIndex:=0;
  permissao;
  {comandos de permiss�o}

	{>>>abertura dos datasets}
	cdsgomoeda.open();
	{abertura dos datasets<<<}

	fncgomoeda.criaAtalhoPopupMenuNavegacao(Popupgomoeda,cdsgomoeda);

	fncgomoeda.definirMascaraCampos(cdsgomoeda);

	fncgomoeda.configurarGridesFormulario(fgomoeda,false, true);

end;

procedure Tfgomoeda.spdAdicionargomoedaClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgomoeda.TabIndex;
	if (fncgomoeda.adicionar(cdsgomoeda)=true) then
		begin
			fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,true);
			dbegomoedaDESCRICAO.SetFocus;
	end
	else
		fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,false);
end;

procedure Tfgomoeda.spdAlterargomoedaClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabgomoeda.TabIndex;
	if (fncgomoeda.alterar(cdsgomoeda)=true) then
		begin
			fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,true);
			dbegomoedaDESCRICAO.SetFocus;
	end
	else
		fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,false);
end;

procedure Tfgomoeda.spdGravargomoedaClick(Sender: TObject);
begin
  fncgomoeda.verificarCamposObrigatorios(cdsgomoeda, camposObrigatoriosgomoeda, pgcPrincipal);
  fncgomoeda.verificarDuplicidade(dm.FDconexao, cdsgomoeda, 'gomoeda', 'ID', duplicidadeListagomoeda);
	if (fncgomoeda.gravar(cdsgomoeda)=true) then
  begin
		fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,false);
    permissao;
  end
	else
		fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,true);
end;

procedure Tfgomoeda.spdCancelargomoedaClick(Sender: TObject);
begin
	if (fncgomoeda.cancelar(cdsgomoeda)=true) then
  begin
		fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,false);
    permissao;
  end
	else
		fncgomoeda.ativarModoEdicao(objetosModoEdicaoAtivosgomoeda,objetosModoEdicaoInativosgomoeda,true);
end;

procedure Tfgomoeda.spdExcluirgomoedaClick(Sender: TObject);
begin
	fncgomoeda.excluir(cdsgomoeda);
end;

procedure Tfgomoeda.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;

procedure Tfgomoeda.permissao;
begin
  {comandos de permiss�o}
  if fncgomoeda.permissao('GOMOEDA', 'Acesso', ParamStr(3)) = false then
  begin
    application.MessageBox('Sem permiss�o de acesso.', 'Permiss�o', MB_ICONEXCLAMATION);
    application.Terminate;
  end;
  spdAdicionarGOMOEDA.Enabled := fncgomoeda.permissao('GOMOEDA', 'Adicionar', ParamStr(3));
  spdAlterarGOMOEDA.Enabled := fncgomoeda.permissao('GOMOEDA', 'Alterar', ParamStr(3));
  spdExcluirGOMOEDA.Enabled := fncgomoeda.permissao('GOMOEDA', 'Excluir', ParamStr(3));
  Popupgomoeda.Items[0].Enabled := fncgomoeda.permissao('GOMOEDA', 'Adicionar', ParamStr(3));
  Popupgomoeda.Items[1].Enabled := fncgomoeda.permissao('GOMOEDA', 'Alterar', ParamStr(3));
  Popupgomoeda.Items[4].Enabled := fncgomoeda.permissao('GOMOEDA', 'Excluir', ParamStr(3));
end;


procedure Tfgomoeda.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fgomoeda.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfgomoeda.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfgomoeda.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdsgomoeda.IsEmpty then 
	fncgomoeda.visualizarRelatorios(frxgomoeda);
end;

end.

