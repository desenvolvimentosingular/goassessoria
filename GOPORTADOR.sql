CREATE TABLE GOPORTADOR(
ID Integer   
,
CONSTRAINT pk_GOPORTADOR PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
DESCRICAO Varchar(250)   COLLATE PT_BR
,
CNPJ Varchar(30)   COLLATE PT_BR
,
ENDERECO Varchar(250)   COLLATE PT_BR
,
CEP Varchar(10)   COLLATE PT_BR
,
UF Varchar(2)   COLLATE PT_BR
,
CIDADE Varchar(250)   COLLATE PT_BR
,
REPRESENTANTE Varchar(250)   COLLATE PT_BR
,
EMAIL1 Varchar(250)   COLLATE PT_BR
,
EMAIL2 Varchar(250)   COLLATE PT_BR
,
FONE1 Varchar(30)   COLLATE PT_BR
,
FONE2 Varchar(30)   COLLATE PT_BR
,
CODTIPO Integer   
,
TIPO Varchar(250)  not null COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOPO_TADOR_ID ON GOPORTADOR (ID);
CREATE ASC INDEX I02_GOPO_TADOR_DT_CADASTRO ON GOPORTADOR (DT_CADASTRO);
CREATE ASC INDEX I03_GOPO_TADOR_HS_CADASTRO ON GOPORTADOR (HS_CADASTRO);
CREATE ASC INDEX I04_GOPO_TADOR_DESCRICAO ON GOPORTADOR (DESCRICAO);
CREATE ASC INDEX I05_GOPO_TADOR_CNPJ ON GOPORTADOR (CNPJ);
CREATE ASC INDEX I06_GOPO_TADOR_ENDERECO ON GOPORTADOR (ENDERECO);
CREATE ASC INDEX I07_GOPO_TADOR_CEP ON GOPORTADOR (CEP);
CREATE ASC INDEX I08_GOPO_TADOR_UF ON GOPORTADOR (UF);
CREATE ASC INDEX I09_GOPO_TADOR_CIDADE ON GOPORTADOR (CIDADE);
CREATE ASC INDEX I10_GOPO_TADOR_REPRESENTANTE ON GOPORTADOR (REPRESENTANTE);
CREATE ASC INDEX I11_GOPO_TADOR_EMAIL1 ON GOPORTADOR (EMAIL1);
CREATE ASC INDEX I12_GOPO_TADOR_EMAIL2 ON GOPORTADOR (EMAIL2);
CREATE ASC INDEX I13_GOPO_TADOR_FONE1 ON GOPORTADOR (FONE1);
CREATE ASC INDEX I14_GOPO_TADOR_FONE2 ON GOPORTADOR (FONE2);
CREATE ASC INDEX I15_GOPO_TADOR_CODTIPO ON GOPORTADOR (CODTIPO);
CREATE ASC INDEX I16_GOPO_TADOR_TIPO ON GOPORTADOR (TIPO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'DESCRICAO';
update rdb$relation_fields set rdb$description = 'CNPJ' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'CNPJ';
update rdb$relation_fields set rdb$description = 'Endere�o' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'ENDERECO';
update rdb$relation_fields set rdb$description = 'CEP' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'CEP';
update rdb$relation_fields set rdb$description = 'Estado' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'UF';
update rdb$relation_fields set rdb$description = 'Cidade' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'CIDADE';
update rdb$relation_fields set rdb$description = 'Representante legal' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'REPRESENTANTE';
update rdb$relation_fields set rdb$description = 'E-Mail' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'EMAIL1';
update rdb$relation_fields set rdb$description = 'E-Mail' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'EMAIL2';
update rdb$relation_fields set rdb$description = 'Telefone' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'FONE1';
update rdb$relation_fields set rdb$description = 'Telefone' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'FONE2';
update rdb$relation_fields set rdb$description = 'C�d.Tipo' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'CODTIPO';
update rdb$relation_fields set rdb$description = 'Tipo' where rdb$relation_name = 'GOPORTADOR' and rdb$field_name = 'TIPO';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONSULTOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONSULTOR_GEN FOR GOCONSULTOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONSULTOR, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOCONTROLERECEBIVEL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCONTROLERECEBIVEL_GEN FOR GOCONTROLERECEBIVEL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCONTROLERECEBIVEL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOFILIAL;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOFILIAL_GEN FOR GOFILIAL ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOFILIAL, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOGRUPOPLANOCONTA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOGRUPOPLANOCONTA_GEN FOR GOGRUPOPLANOCONTA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOGRUPOPLANOCONTA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOMOEDA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOMOEDA_GEN FOR GOMOEDA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOMOEDA, 1);
end
^^
SET TERM ;  ^^
commit;
CREATE GENERATOR G_GOPORTADOR;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOPORTADOR_GEN FOR GOPORTADOR ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOPORTADOR, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
