CREATE TABLE GOBANCO(
ID Varchar()   COLLATE PT_BR
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
BANCO Varchar(250)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOBA_BANCO_ID ON GOBANCO (ID);
CREATE ASC INDEX I02_GOBA_BANCO_DT_CADASTRO ON GOBANCO (DT_CADASTRO);
CREATE ASC INDEX I03_GOBA_BANCO_HS_CADASTRO ON GOBANCO (HS_CADASTRO);
CREATE ASC INDEX I04_GOBA_BANCO_BANCO ON GOBANCO (BANCO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOBANCO' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOBANCO' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOBANCO' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Banco' where rdb$relation_name = 'GOBANCO' and rdb$field_name = 'BANCO';

commit;

commit;
