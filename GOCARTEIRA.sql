CREATE TABLE GOCARTEIRA(
ID Integer   
,
CONSTRAINT pk_GOCARTEIRA PRIMARY KEY (ID)
,
DT_CADASTRO Date DEFAULT CURRENT_DATE  
,
HS_CADASTRO Time DEFAULT CURRENT_TIME  
,
DESCRICAO Varchar(250)   COLLATE PT_BR
);
commit;
CREATE ASC INDEX I01_GOCA_TEIRA_ID ON GOCARTEIRA (ID);
CREATE ASC INDEX I02_GOCA_TEIRA_DT_CADASTRO ON GOCARTEIRA (DT_CADASTRO);
CREATE ASC INDEX I03_GOCA_TEIRA_HS_CADASTRO ON GOCARTEIRA (HS_CADASTRO);
CREATE ASC INDEX I04_GOCA_TEIRA_DESCRICAO ON GOCARTEIRA (DESCRICAO);

commit;
update rdb$relation_fields set rdb$description = 'C�digo' where rdb$relation_name = 'GOCARTEIRA' and rdb$field_name = 'ID';
update rdb$relation_fields set rdb$description = 'Data de Cadastro' where rdb$relation_name = 'GOCARTEIRA' and rdb$field_name = 'DT_CADASTRO';
update rdb$relation_fields set rdb$description = 'Hora de Cadastro' where rdb$relation_name = 'GOCARTEIRA' and rdb$field_name = 'HS_CADASTRO';
update rdb$relation_fields set rdb$description = 'Descri��o' where rdb$relation_name = 'GOCARTEIRA' and rdb$field_name = 'DESCRICAO';

commit;
CREATE GENERATOR G_GOCARTEIRA;
commit;
SET TERM  ^^ ;
CREATE TRIGGER GOCARTEIRA_GEN FOR GOCARTEIRA ACTIVE BEFORE INSERT POSITION 0 AS
begin
	if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_GOCARTEIRA, 1);
end
^^
SET TERM ;  ^^
commit;

commit;
