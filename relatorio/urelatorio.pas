unit urelatorio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxGDIPlusClasses, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxBarBuiltInMenu, cxPC, Vcl.Menus, cxContainer, cxEdit,
  Vcl.ComCtrls, dxCore, cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxCurrencyEdit, uPesquisa, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,   System.DateUtils,
  cxGridCustomView, cxGrid, cxDBEdit, Vcl.Mask, Vcl.DBCtrls,udm, uFuncoes, Vcl.DialogMessage,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  frxClass, frxDBSet;

type
  TFrelatorio = class(TForm)
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    lblTituloversao: TLabel;
    imglogo: TImage;
    lblusuario: TLabel;
    pgprincipal: TcxPageControl;
    pnlLinhaBotoes: TPanel;
    spdimprimir: TButton;
    spdfechar: TButton;
    tabrelatorio1: TcxTabSheet;
    popupImpressao: TPopupMenu;
    menuImpRel1: TMenuItem;
    flwpOpcoes1: TFlowPanel;
    Panel1: TPanel;
    btnpesquisar1: TButton;
    pnlTituloPesquisa: TPanel;
    psqportador: TPesquisa;
    psqprojeto: TPesquisa;
    pnlLayoutTituloscodprojeto: TPanel;
    dbecoprojeto: TEdit;
    spbProjeto: TSpeedButton;
    dbeprojeto: TEdit;
    pnlLayoutTituloscodfundo: TPanel;
    dbecodportador: TEdit;
    spbPortador: TSpeedButton;
    dbeportador: TEdit;
    pnlLayoutTitulosdtcompetencia: TPanel;
    dtcompetencia1: TcxDateEdit;
    dtcompetencia2: TcxDateEdit;
    spbdtcompetencia1: TSpeedButton;
    popmenudata1: TPopupMenu;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem24: TMenuItem;
    MenuItem25: TMenuItem;
    MenuItem26: TMenuItem;
    MenuItem27: TMenuItem;
    MenuItem28: TMenuItem;
    MenuItem29: TMenuItem;
    MenuItem30: TMenuItem;
    MenuItem31: TMenuItem;
    MenuItem32: TMenuItem;
    MenuItem33: TMenuItem;
    MenuItem34: TMenuItem;
    MenuItem35: TMenuItem;
    MenuItem36: TMenuItem;
    MenuItem37: TMenuItem;
    MenuItem38: TMenuItem;
    GridResultadoPesquisa1: TcxGrid;
    GridResultadoPesquisa1DBTV: TcxGridDBTableView;
    GridResultadoPesquisa1Level1: TcxGridLevel;
    FDriscorecebivel: TFDQuery;
    dsriscorecebivel: TDataSource;
    GridResultadoPesquisa1DBTVCODPROJETO: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVPROJETO: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVCODPORTADOR: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVPORTADOR: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVVENCER: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVVENCIDO: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVVLBRUTOFINALBORDERO: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVVLMEDIORECEBIVEIS: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVPM_EMPRESA: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVCUSTOEFETIVO: TcxGridDBColumn;
    GridResultadoPesquisa1DBTVTAXA_NOMINAL: TcxGridDBColumn;
    frxDBpainelrisco: TfrxDBDataset;
    frxriscorecebivel: TfrxReport;
    FDriscorecebivelCODPROJETO: TIntegerField;
    FDriscorecebivelPROJETO: TStringField;
    FDriscorecebivelCODPORTADOR: TIntegerField;
    FDriscorecebivelPORTADOR: TStringField;
    FDriscorecebivelVENCIDO: TFloatField;
    FDriscorecebivelVENCER: TFloatField;
    FDriscorecebivelVLBRUTOFINALBORDERO: TFloatField;
    FDriscorecebivelVLMEDIORECEBIVEIS: TFloatField;
    FDriscorecebivelPM_EMPRESA: TFloatField;
    FDriscorecebivelCUSTOEFETIVO: TFloatField;
    FDriscorecebivelTAXA_NOMINAL: TFloatField;
    lblversao: TLabel;
    procedure spdOpcoesClick(Sender: TObject);
    procedure dbecoprojetoKeyPress(Sender: TObject; var Key: Char);
    procedure dbecodportadorKeyPress(Sender: TObject; var Key: Char);
    procedure spbdtcompetencia1Click(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure MenuItem21Click(Sender: TObject);
    procedure MenuItem22Click(Sender: TObject);
    procedure MenuItem23Click(Sender: TObject);
    procedure MenuItem24Click(Sender: TObject);
    procedure MenuItem26Click(Sender: TObject);
    procedure MenuItem27Click(Sender: TObject);
    procedure MenuItem29Click(Sender: TObject);
    procedure MenuItem32Click(Sender: TObject);
    procedure MenuItem33Click(Sender: TObject);
    procedure MenuItem35Click(Sender: TObject);
    procedure MenuItem36Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem38Click(Sender: TObject);
    procedure btnpesquisar1Click(Sender: TObject);
    procedure spdfecharClick(Sender: TObject);
    procedure spdimprimirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    procedure sessao;
    { Private declarations }
  public
    { Public declarations }
  fncrelatorio: TFuncoes;
  end;

var
  Frelatorio: TFrelatorio;

implementation

{$R *.dfm}


procedure TFrelatorio.btnpesquisar1Click(Sender: TObject);
begin

    if ((dtcompetencia1.Text=EmptyStr) OR (dtcompetencia2.Text=EmptyStr)) then
    abort;

    FDriscorecebivel.Close;
    FDriscorecebivel.SQL.Clear;
    FDriscorecebivel.SQL.Add(' SELECT                                                                      ');
    FDriscorecebivel.SQL.Add('    CODPROJETO                                                               ');
    FDriscorecebivel.SQL.Add('   ,PROJETO                                                                  ');
    FDriscorecebivel.SQL.Add('   ,CODPORTADOR                                                              ');
    FDriscorecebivel.SQL.Add('   ,PORTADOR                                                                 ');
    FDriscorecebivel.SQL.Add('   ,SUM(VENCIDO) VENCIDO                                                     ');
    FDriscorecebivel.SQL.Add('   ,SUM(VENCER) VENCER                                                       ');
    FDriscorecebivel.SQL.Add('   ,SUM(VLBRUTOFINALBORDERO)VLBRUTOFINALBORDERO                              ');
    FDriscorecebivel.SQL.Add('   ,SUM(VLMEDIORECEBIVEIS) VLMEDIORECEBIVEIS                                 ');
    FDriscorecebivel.SQL.Add('   ,SUM(PM_EMPRESA) PM_EMPRESA                                               ');
    FDriscorecebivel.SQL.Add('   ,SUM(custoefetivo)*100 custoefetivo                                       ');
    FDriscorecebivel.SQL.Add('   ,SUM(taxa_nominal)*100 taxa_nominal                                       ');
    FDriscorecebivel.SQL.Add('FROM (                                                                       ');
    FDriscorecebivel.SQL.Add('   SELECT c.codprojeto                                                       ');
    FDriscorecebivel.SQL.Add('       ,C.PROJETO                                                            ');
    FDriscorecebivel.SQL.Add('       ,c.codportador                                                        ');
    FDriscorecebivel.SQL.Add('       ,C.PORTADOR                                                           ');
    FDriscorecebivel.SQL.Add('       ,coalesce((                                                           ');
    FDriscorecebivel.SQL.Add('           SELECT SUM(VALOR)                                                 ');
    FDriscorecebivel.SQL.Add('           FROM gobaserisco P                                                ');
    FDriscorecebivel.SQL.Add('           WHERE P.codprojeto = C.codprojeto                                 ');
    FDriscorecebivel.SQL.Add('               AND P.codportador = C.codportador                             ');
    FDriscorecebivel.SQL.Add('               AND P.STATUS = ''VENCIDO''                                    ');
    FDriscorecebivel.SQL.Add('               AND P.dtcompetencia BETWEEN :data1 AND :data2                 ');
    FDriscorecebivel.SQL.Add('           ),0) VENCIDO                                                      ');
    FDriscorecebivel.SQL.Add('       ,coalesce((                                                           ');
    FDriscorecebivel.SQL.Add('           SELECT SUM(VALOR)                                                 ');
    FDriscorecebivel.SQL.Add('           FROM gobaserisco P                                                ');
    FDriscorecebivel.SQL.Add('           WHERE P.codprojeto = C.codprojeto                                 ');
    FDriscorecebivel.SQL.Add('               AND P.codportador = C.codportador                             ');
    FDriscorecebivel.SQL.Add('               AND P.STATUS = ''A VENCER''                                   ');
    FDriscorecebivel.SQL.Add('               AND P.dtcompetencia BETWEEN :data1 AND :data2                 ');
    FDriscorecebivel.SQL.Add('           ),0) VENCER                                                       ');
    FDriscorecebivel.SQL.Add('       ,0 VLBRUTOFINALBORDERO                                                ');
    FDriscorecebivel.SQL.Add('       ,0 VLMEDIORECEBIVEIS                                                  ');
    FDriscorecebivel.SQL.Add('       ,0 PM_EMPRESA                                                         ');
    FDriscorecebivel.SQL.Add('       ,0 custoefetivo                                                       ');
    FDriscorecebivel.SQL.Add('       ,0 taxa_nominal                                                       ');
    FDriscorecebivel.SQL.Add('   FROM GOBASERISCO C                                                        ');
    FDriscorecebivel.SQL.Add('   WHERE C.DTCOMPETENCIA BETWEEN :data1 AND :data2 AND C.CONSOLIDADO = ''S'' ');

    if FDriscorecebivel.Text<>EmptyStr then
    begin
    FDriscorecebivel.SQL.Add(' and  c.codprojeto=:CODPROJETO                                                ');
    FDriscorecebivel.ParamByName('CODPROJETO').asInteger:= strtoint(dbecoprojeto.Text);
    end;

    if dbecodportador.Text<>EmptyStr then
    begin
    FDriscorecebivel.SQL.Add(' and  c.codportador=:codportador                                              ');
    FDriscorecebivel.ParamByName('codportador').asInteger:= strtoint(dbecodportador.Text);
    end;

    FDriscorecebivel.SQL.Add('    GROUP BY 1,2,3,4                                                          ');
    FDriscorecebivel.SQL.Add('    UNION                                                                     ');
    FDriscorecebivel.SQL.Add('    SELECT c.codprojeto                                                       ');
    FDriscorecebivel.SQL.Add('        ,C.PROJETO                                                            ');
    FDriscorecebivel.SQL.Add('        ,c.codportador                                                        ');
    FDriscorecebivel.SQL.Add('        ,C.PORTADOR                                                           ');
    FDriscorecebivel.SQL.Add('        ,0 VENCIDO                                                            ');
    FDriscorecebivel.SQL.Add('        ,0 A_VENCER                                                           ');
    FDriscorecebivel.SQL.Add('        ,sum(c.VLBRUTOFINALBORDERO ) VLBRUTOFINALBORDERO                      ');
    FDriscorecebivel.SQL.Add('        ,sum(c.vlbrutofinalbordero) / sum(coalesce(c.QTFINALTITULOS, 0)) VLMEDIORECEBIVEIS                  ');
    FDriscorecebivel.SQL.Add('        ,SUM((c.vlbrutofinalbordero) * c.prazomedio) / SUM(c.vlbrutofinalbordero) PM_EMPRESA                ');
    FDriscorecebivel.SQL.Add('        ,(((sum(c.vlbrutofinalbordero) / (sum(c.vlbrutofinalbordero) - sum(c.tac + c.tarifaentradatitulos + ');
    FDriscorecebivel.SQL.Add(' c.desagioacordado + c.calculoiof + c.advalorem + c.outrasdespesas))) - 1) / (sum(c.vlbrutofinalbordero * c.prazomedio) / sum(c.vlbrutofinalbordero))) * 30 custoefetivo ');
    FDriscorecebivel.SQL.Add('        ,((sum(C.ADVALOREM + C.DESAGIOACORDADO) / sum(c.vlbrutofinalbordero)) / (sum(multiplicadorpmfloat) / sum(c.vlbrutofinalbordero))) * 30 taxa_nominal              ');
    FDriscorecebivel.SQL.Add('    FROM gocontrolerecebivel c                                                                                                                                           ');
    FDriscorecebivel.SQL.Add('    WHERE (coalesce(c.qttitulos, 0) - coalesce(c.qtdrecusada, 0)) > 0 AND C.DTCOMPETENCIA BETWEEN :data1 AND :data2                                                      ');

    if dbecoprojeto.Text<>EmptyStr then
    begin
    FDriscorecebivel.SQL.Add(' and  c.codprojeto=:CODPROJETO                              ');
    FDriscorecebivel.ParamByName('CODPROJETO').asInteger:= strtoint(dbecoprojeto.Text);
    end;
    if dbecodportador.Text<>EmptyStr then
    begin
    FDriscorecebivel.SQL.Add(' and  c.codportador=:codportador                            ');
    FDriscorecebivel.ParamByName('codportador').asInteger:= strtoint(dbecodportador.Text);
    end;

    FDriscorecebivel.SQL.Add('   GROUP BY 1,2,3,4)                                        ');
    FDriscorecebivel.SQL.Add('GROUP BY 1,2,3,4                                            ');
    FDriscorecebivel.ParamByName('data1').AsDateTime:= dtcompetencia1.Date;
    FDriscorecebivel.ParamByName('data2').AsDateTime:= dtcompetencia2.Date;
    FDriscorecebivel.Open;


end;

procedure TFrelatorio.dbecodportadorKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
 key := #0;

 if key = #8 then
 begin
  dbeportador.Text  :=EmptyStr;
 end;
end;

procedure TFrelatorio.dbecoprojetoKeyPress(Sender: TObject; var Key: Char);
begin
 if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
 key := #0;

 if key = #8 then
 begin
  dbeprojeto.Text  :=EmptyStr;
 end;
end;

procedure TFrelatorio.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>fncrelatorio container de fun��es da memoria}
	fncrelatorio.configurarGridesFormulario(Frelatorio,true, false);

	fncrelatorio.Free;
	{eliminando container de fun��es da memoria<<<}

	Frelatorio:=nil;
	Action:=cafree;
end;

procedure TFrelatorio.FormCreate(Sender: TObject);
begin
  sessao;


  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
  {<<>> fun��o de tradu�a� de componentes>>>}
   sleep(250);
   if FileExists(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini') then
   begin
     dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Tradu��oDev.ini');
     dm.cxLocalizer.LanguageIndex := 1;
     dm.cxLocalizer.Active := TRUE;
   end;
  {<<>> fun��o de tradu�a� de componentes>>>}


  {container de fun��es<<<}
  {>>>container de fun��es}
	fncrelatorio:= TFuncoes.Create(Self);
	fncrelatorio.definirConexao(dm.conexao);
	fncrelatorio.definirFormulario(Self);
	fncrelatorio.validarTransacaoRelacionamento:=true;
	fncrelatorio.autoAplicarAtualizacoesBancoDados:=true;

  {<<vers�o da rotina>>}
  lblversao.Caption:=lblversao.Caption+' - '+fncrelatorio.recuperarVersaoExecutavel;
  {<<vers�o da rotina>>}

  {<<vers�o da rotina>>}
  lblTituloversao.Caption:=lblTituloversao.Caption+' - '+fncrelatorio.recuperarVersaoExecutavel;
  fncrelatorio.configurarGridesFormulario(FRelatorio,false, true);

  {<<vers�o da rotina>>}
  end);
  dtcompetencia1.Date := StartofTheMonth(Date);
  dtcompetencia2.Date := EndofTheMonth(Date);


end;

procedure TFrelatorio.sessao;
begin
  try
    dm.conexao.Close;
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password'] := ParamStr(2);
    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);
    lblusuario.Caption := lblusuario.Caption + ParamStr(3);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;
  except
    on e: exception do
    begin
      application.MessageBox(pchar('Sem conex�o de rede.Motivo' + ''#13''#10'' + e.Message), 'Exce��o.', MB_OK);
      application.Terminate;
    end;
  end;
end;

procedure TFrelatorio.MenuItem20Click(Sender: TObject);
begin
 try
  fncrelatorio.DiaAtual(Date);
  dtcompetencia1.Date    := fncrelatorio.DiaAtual(Date);
  dtcompetencia2.Date    := fncrelatorio.DiaAtual(Date);
 finally
 end;
end;

procedure TFrelatorio.MenuItem21Click(Sender: TObject);
begin
 try
    dtcompetencia1.Date  :=fncrelatorio.DiaAnterior(Date);
    dtcompetencia2.Date  :=fncrelatorio.DiaAnterior(Date);
   finally
   end;
end;

procedure TFrelatorio.MenuItem22Click(Sender: TObject);
var data : TStringList;
begin
 try
  data := TStringList.Create;
  data:=fncrelatorio.SemanaAtual(Date);
  dtcompetencia1.Date:= StrToDateTime(data[0]);
  dtcompetencia2.Date  := StrToDateTime(data[1]);
 finally
   freeandnil(data);
 end;
end;

procedure TFrelatorio.MenuItem23Click(Sender: TObject);
var data : TStringList;
begin
  try
    data := TStringList.Create;
    data:=fncrelatorio.SemanaAnterior(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
  end;
end;

procedure TFrelatorio.MenuItem24Click(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncrelatorio.UltimosQuinzeDias (Date);
    dtcompetencia1.Date:= StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);
   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem26Click(Sender: TObject);
var data : TStringList;
begin
    try
    data := TStringList.Create;
    data:=fncrelatorio.MesAtual (Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem27Click(Sender: TObject);
var data : TStringList;
begin
  try
    data := TStringList.Create;
    data:=fncrelatorio.MesAnterior(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem29Click(Sender: TObject);
var data : TStringList;
begin
  try
    data := TStringList.Create;
    data:=fncrelatorio.semestreatual(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

  finally
  freeandnil(data);
  end;
end;

procedure TFrelatorio.MenuItem32Click(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncrelatorio.semestreatual(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem33Click(Sender: TObject);
var data : TStringList;
begin
 try
    data := TStringList.Create;
    data:=fncrelatorio.semestreanterior(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem35Click(Sender: TObject);
var data : TStringList;
begin
  try
    data := TStringList.Create;
    data:=fncrelatorio.anoantual(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem36Click(Sender: TObject);
var data : TStringList;
begin
  try
    data := TStringList.Create;
    data:=fncrelatorio.anoanterior(Date);
    dtcompetencia1.Date  := StrToDateTime(data[0]);
    dtcompetencia2.Date  := StrToDateTime(data[1]);

   finally
     freeandnil(data);
   end;
end;

procedure TFrelatorio.MenuItem38Click(Sender: TObject);
begin
  dtcompetencia1.Clear;
  dtcompetencia2.Clear;
end;

procedure TFrelatorio.spbdtcompetencia1Click(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popmenudata1.Popup(X, Y);
  end;
end;

procedure TFrelatorio.spdfecharClick(Sender: TObject);
begin
 application.Terminate;
end;

procedure TFrelatorio.spdimprimirClick(Sender: TObject);
begin
     if  not FDriscorecebivel.IsEmpty then
   	 fncrelatorio.visualizarRelatorios(frxriscorecebivel);

end;

procedure TFrelatorio.spdOpcoesClick(Sender: TObject);
begin
 with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;


end.
