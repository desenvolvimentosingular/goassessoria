program goAssessoria;
uses
	Forms,
	udm in 'udm.pas' {dm: TDataModule},
	uprincipal in 'uprincipal.pas' {fprincipal},
	ugocarteira in 'ugocarteira.pas' {fgocarteira},
	ugoconsultor in 'ugoconsultor.pas' {fgoconsultor},
	ugocontrolerecebivel in 'ugocontrolerecebivel.pas' {fgocontrolerecebivel},
	ugofilial in 'ugofilial.pas' {fgofilial},
	ugogrupoplanoconta in 'ugogrupoplanoconta.pas' {fgogrupoplanoconta},
	ugomoeda in 'ugomoeda.pas' {fgomoeda},
	ugoplanoconta in 'ugoplanoconta.pas' {fgoplanoconta},
	ugoportador in 'ugoportador.pas' {fgoportador},
	ugoprojetoconsultor in 'ugoprojetoconsultor.pas' {fgoprojetoconsultor},
	ugoprojetofilial in 'ugoprojetofilial.pas' {fgoprojetofilial},
	ugoprojetofundo in 'ugoprojetofundo.pas' {fgoprojetofundo},
	ugoprojetos in 'ugoprojetos.pas' {fgoprojetos},
	ugosupervisor in 'ugosupervisor.pas' {fgosupervisor};
{$R *.res}
begin
	Application.Initialize;
	Application.Title :='goAssessoria';
	Application.CreateForm(Tdm, dm);
	Application.CreateForm(Tfprincipal, fprincipal);
	Application.Run;
end.
