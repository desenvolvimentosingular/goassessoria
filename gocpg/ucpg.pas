unit ucpg;

{*Autor- Waldes Alves de Souza *}                                                           
{*Data 18/10/2021*}                                          
{*Hora 09:47:06*}                                            
{*Unit cpg *}

interface

uses                                                                                        
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,                  
Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, FMTBcd, DB, DBClient, Provider, midaslib,        
Buttons, ComCtrls, uFuncoes, frxClass, frxDBSet,                                            
Grids, Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxCurrencyEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, Data.SqlExpr, dxGDIPlusClasses, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit,
  Vcl.DialogMessage, uPesquisa, cxCheckBox, uimportacao;

type                                                                                        
	Tfcpg = class(TForm)
    sqlcpg: TSQLDataSet;
	dspcpg: TDataSetProvider;
	cdscpg: TClientDataSet;
	dscpg: TDataSource;
	popupImpressao: TPopupMenu;
	menuImpRelSintetico: TMenuItem;
	tabPesquisas: Tcxtabsheet;
	flwpOpcoes: TFlowPanel;
	flwpPesquisas: TFlowPanel;
	pnlResultadoPesquisas: TPanel;
	pnlTituloResultadoPesquisa: TPanel;
	pnlTituloPesquisa: TPanel;
	frxcpg: TfrxReport;
	frxDBcpg: TfrxDBDataset;
	Popupcpg: TPopupMenu;
    pgcPrincipal: TcxPageControl;
	GridResultadoPesquisa: TcxGrid;
	tabcpg: tcxTabSheet;
	SCRcpg: TScrollBox;
	pnlDadoscpg: TPanel;
	{>>>bot�es de manipula��o de dados cpg}
	pnlLinhaBotoescpg: TPanel;
	spdAdicionarcpg: TButton;
	spdAlterarcpg: TButton;
	spdGravarcpg: TButton;
	spdCancelarcpg: TButton;
	spdExcluircpg: TButton;

	{bot�es de manipula��o de dadoscpg<<<}
	{campo ID}
	sqlcpgID: TIntegerField;
	cdscpgID: TIntegerField;
	pnlLayoutLinhascpg1: TPanel;
	pnlLayoutTituloscpgid: TPanel;
	pnlLayoutCamposcpgid: TPanel;
	dbecpgid: TDBEdit;

	{campo DT_CADASTRO}
	sqlcpgDT_CADASTRO: TDateField;
	cdscpgDT_CADASTRO: TDateField;
	pnlLayoutLinhascpg2: TPanel;
	pnlLayoutTituloscpgdt_cadastro: TPanel;
	pnlLayoutCamposcpgdt_cadastro: TPanel;
	dbecpgdt_cadastro: TDBEdit;

	{campo HS_CADASTRO}
	sqlcpgHS_CADASTRO: TTimeField;
	cdscpgHS_CADASTRO: TTimeField;
	pnlLayoutLinhascpg3: TPanel;
	pnlLayoutTituloscpghs_cadastro: TPanel;
	pnlLayoutCamposcpghs_cadastro: TPanel;
	dbecpghs_cadastro: TDBEdit;

	{campo CODPROJETO}
	sqlcpgCODPROJETO: TIntegerField;
	cdscpgCODPROJETO: TIntegerField;
	pnlLayoutLinhascpg4: TPanel;
	pnlLayoutTituloscpgcodprojeto: TPanel;
	pnlLayoutCamposcpgcodprojeto: TPanel;
	dbecpgcodprojeto: TDBEdit;

	{campo PROJETO}
	sqlcpgPROJETO: TStringField;
	cdscpgPROJETO: TStringField;

	{campo ID_FORNECEDOR}
	sqlcpgID_FORNECEDOR: TIntegerField;
	cdscpgID_FORNECEDOR: TIntegerField;
	pnlLayoutLinhascpg6: TPanel;
	pnlLayoutTituloscpgid_fornecedor: TPanel;
	pnlLayoutCamposcpgid_fornecedor: TPanel;
	dbecpgid_fornecedor: TDBEdit;

	{campo FORNECEDOR}
	sqlcpgFORNECEDOR: TStringField;
	cdscpgFORNECEDOR: TStringField;

	{campo DESCRICAO}
	sqlcpgDESCRICAO: TStringField;
	cdscpgDESCRICAO: TStringField;
	pnlLayoutLinhascpg8: TPanel;
	pnlLayoutTituloscpgdescricao: TPanel;
	pnlLayoutCamposcpgdescricao: TPanel;
	dbecpgdescricao: TDBEdit;

	{campo DOCUMENTO}
	sqlcpgDOCUMENTO: TStringField;
	cdscpgDOCUMENTO: TStringField;
	pnlLayoutLinhascpg9: TPanel;
	pnlLayoutTituloscpgdocumento: TPanel;
	pnlLayoutCamposcpgdocumento: TPanel;
	dbecpgdocumento: TDBEdit;

	{campo VL_BRUTO}
	sqlcpgVL_BRUTO: TFloatField;
	cdscpgVL_BRUTO: TFloatField;
	pnlLayoutLinhascpg10: TPanel;

	{campo VL_MULTA}
	sqlcpgVL_MULTA: TFloatField;
	cdscpgVL_MULTA: TFloatField;

	{campo VL_JUROS}
	sqlcpgVL_JUROS: TFloatField;
	cdscpgVL_JUROS: TFloatField;

	{campo VL_DESCONTO}
	sqlcpgVL_DESCONTO: TFloatField;
	cdscpgVL_DESCONTO: TFloatField;

	{campo DT_VENCIMENTO}
	sqlcpgDT_VENCIMENTO: TDateField;
	cdscpgDT_VENCIMENTO: TDateField;
	pnlLayoutLinhascpg14: TPanel;
	pnlLayoutTituloscpgdt_vencimento: TPanel;
	pnlLayoutCamposcpgdt_vencimento: TPanel;

	{campo FORMA_PAGAMENTO}
	sqlcpgFORMA_PAGAMENTO: TStringField;
	cdscpgFORMA_PAGAMENTO: TStringField;

	{campo VL_PAGO}
	sqlcpgVL_PAGO: TFloatField;
	cdscpgVL_PAGO: TFloatField;
	pnlLayoutLinhascpg16: TPanel;
	pnlLayoutTituloscpgvl_pago: TPanel;
	pnlLayoutCamposcpgvl_pago: TPanel;
	dbecpgvl_pago: TDBEdit;

	{campo DT_PAGAMENTO}
	sqlcpgDT_PAGAMENTO: TDateField;
	cdscpgDT_PAGAMENTO: TDateField;
	pnlLayoutLinhascpg17: TPanel;
	pnlLayoutTituloscpgdt_pagamento: TPanel;
	pnlLayoutCamposcpgdt_pagamento: TPanel;

	{campo ID_BANCO_CONTA}
	sqlcpgID_BANCO_CONTA: TIntegerField;
	cdscpgID_BANCO_CONTA: TIntegerField;
	pnlLayoutLinhascpg18: TPanel;
	pnlLayoutTituloscpgid_banco_conta: TPanel;
	pnlLayoutCamposcpgid_banco_conta: TPanel;
	dbecpgid_banco_conta: TDBEdit;

	{campo BANCO_CONTA}
	sqlcpgBANCO_CONTA: TStringField;
	cdscpgBANCO_CONTA: TStringField;
    GridResultadoPesquisaLevel1: TcxGridLevel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    spdOpcoes: TSpeedButton;
    imglogo: TImage;
    pnlLayoutCamposcpgprojeto: TPanel;
    dbecpgPROJETO: TDBEdit;
    pnlLayoutCamposcpgfornecedor: TPanel;
    dbecpgFORNECEDOR: TDBEdit;
    dbecpgDT_VENCIMENTO: TcxDBDateEdit;
    dbecpgDT_PAGAMENTO: TcxDBDateEdit;
    pnlLayoutTituloscpgvl_bruto: TPanel;
    pnlLayoutCamposcpgvl_bruto: TPanel;
    dbecpgVL_BRUTO: TDBEdit;
    pnlLayoutCamposcpgvl_multa: TPanel;
    dbecpgVL_MULTA: TDBEdit;
    pnlLayoutTituloscpgvl_multa: TPanel;
    pnlLayoutTituloscpgvl_juros: TPanel;
    pnlLayoutCamposcpgvl_juros: TPanel;
    dbecpgVL_JUROS: TDBEdit;
    pnlLayoutTituloscpgvl_desconto: TPanel;
    pnlLayoutCamposcpgvl_desconto: TPanel;
    dbecpgVL_DESCONTO: TDBEdit;
    pnlLayoutCamposcpgforma_pagamento: TPanel;
    cbocpgFORMA_PAGAMENTO: TDBComboBox;
    pnlLayoutTituloscpgforma_pagamento: TPanel;
    pnlLayoutCamposcpgbanco_conta: TPanel;
    dbecpgBANCO_CONTA: TDBEdit;
    cdsfiltros: TClientDataSet;
    btnprojeto: TButton;
    btnFornecedor: TButton;
    btnBancoConta: TButton;
    psqProjeto: TPesquisa;
    psqFornecedor: TPesquisa;
    GridCPGDBTVSEL: TcxGridDBColumn;
    GridCPGDBTV: TcxGridDBTableView;
    btnBaixa: TButton;
    cdscpgSEL: TBooleanField;
    spdImportar: TButton;
    lblTituloversao: TLabel;
    lblusuario: TLabel;
    lblProjetoresultado: TLabel;
    lblusuarioresultado: TLabel;
    lblversao: TLabel;
    spdexcluirselecionados: TButton;

	{objetos de manipula��o de dados<<<}

	procedure FormCreate(Sender: TObject);
	procedure FormKeyPress(Sender: TObject; var Key: Char);

	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure altenarPopupTabSheet(sender: TObject);
	procedure spdImpressaoClick(Sender: TObject);
	 procedure menuImpRelSinteticoClick(Sender: TObject);
	{>>>procedimentos de manipula��o de dados}
	procedure spdAdicionarcpgClick(Sender: TObject);
	procedure spdAlterarcpgClick(Sender: TObject);
	procedure spdGravarcpgClick(Sender: TObject);
	procedure spdCancelarcpgClick(Sender: TObject);
	procedure spdExcluircpgClick(Sender: TObject);
	procedure cdscpgBeforePost(DataSet: TDataSet);
    procedure GridCPGDBTVDblClick(Sender: TObject);
    procedure spdOpcoesClick(Sender: TObject);
    procedure GRIDResultadoPesquisaDBTVSELPropertiesChange(Sender: TObject);
    procedure spdImportarClick(Sender: TObject);
    procedure btnBaixaClick(Sender: TObject);
    procedure spdexcluirselecionadosClick(Sender: TObject);
	{procedimentos de manipula��o de dados<<<}

	private

	{container de funcoes}
	fnccpg: TFuncoes;

	{array de componentes para verifica��o de campos obrigat�rios}
	camposObrigatorioscpg: Array[0..1]  of TWinControl;

	{array de componentes de ativa��o para modo de edi��o}
	objetosModoEdicaoAtivoscpg: Array[0..10]  of TControl;
	objetosModoEdicaoInativoscpg: Array[0..10]  of TControl;

	{array de componentes para verifica��o de duplicidade}
	duplicidadeListacpg: Array[0..0]  of TDuplicidade;
	duplicidadeCampocpg: TDuplicidade;
	{array de crit�rios de pesquisas}
	pesquisasLista: Array[0..20]  of TPesquisas;
	pesquisasItem: TPesquisas;
    procedure sessao;
	public

end;

var
fcpg: Tfcpg;

implementation

uses udm, System.IniFiles;

{$R *.dfm}
//{$R logo.res}

procedure Tfcpg.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key=#13 then begin key := #0; Perform(WM_nextdlgctl,0,0); end;
end;

procedure Tfcpg.GridCPGDBTVDblClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcpg.TabIndex;
end;

procedure Tfcpg.GRIDResultadoPesquisaDBTVSELPropertiesChange(Sender: TObject);
var
  i: Integer;
  bSelecao: Boolean;
begin
//    if GridResultadoPesquisaDBTV.Items[0] = GRIDResultadoPesquisaDBTVSEL then
//    begin
//      bSelecao := False;
//      for I := 0 to GridResultadoPesquisaDBTV.DataController.RecordCount - 1 do
//      begin
//        if GridResultadoPesquisaDBTV.DataController.Values[I, GRIDResultadoPesquisaDBTVSEL.Index] = True then
//        begin
//          bSelecao := True;
//        end;
//      end;
//
//      if not bSelecao then
//        fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false);
//
//    end;
end;

procedure Tfcpg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	{>>>verificando exist�ncia de registros em transa��o}
	fnccpg.verificarEmTransacao(cdscpg);
	{verificando exist�ncia de registros em transa��o<<<}

	{>>>eliminando container de fun��es da memoria}
	fnccpg.configurarGridesFormulario(fcpg,true, false);

	fnccpg.Free;
	{eliminando container de fun��es da memoria<<<}

	fcpg:=nil;
	Action:=cafree;
end;

procedure Tfcpg.FormCreate(Sender: TObject);
begin
  sessao;

    { >>>container de fun��es }
  if FileExists(ExtractFilePath(Application.ExeName) + '\Tradu��oDev.ini') then
  begin
    dm.cxLocalizer.LoadFromFile(ExtractFilePath(Application.ExeName) +
      '\Tradu��oDev.ini');
    dm.cxLocalizer.LanguageIndex := 1;
    dm.cxLocalizer.Active := true;
  end;

  TDialogMessage.ShowWaitMessage('Criando pesquisas...',
  procedure
  begin
    {>>>container de fun��es}
    fnccpg:= TFuncoes.Create(Self);
    fnccpg.definirConexao(dm.conexao);
    fnccpg.definirConexaohistorico(dm.conexao);
    fnccpg.definirFormulario(Self);
    fnccpg.validarTransacaoRelacionamento:=true;
    fnccpg.autoAplicarAtualizacoesBancoDados:=true;
    {container de fun��es<<<}

    {>>>verificar campos duplicidade}
    {verificar campos duplicidade<<<}

    {>>>gerando objetos de pesquisas}
    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesID';
    pesquisasItem.titulo:='C�digo';
    pesquisasItem.campo:='ID';
    pesquisasItem.tipoFDB:='Integer';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[0]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDT_CADASTRO';
    pesquisasItem.titulo:='Data de Cadastro';
    pesquisasItem.campo:='DT_CADASTRO';
    pesquisasItem.tipoFDB:='Date';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[1]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesCODPROJETO';
    pesquisasItem.titulo:='C�d.Projeto';
    pesquisasItem.campo:='CODPROJETO';
    pesquisasItem.tipoFDB:='Integer';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[2]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesPROJETO';
    pesquisasItem.titulo:='Projeto';
    pesquisasItem.campo:='PROJETO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='LIKE';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[3]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesID_FORNECEDOR';
    pesquisasItem.titulo:='C�d.Fornecedor';
    pesquisasItem.campo:='ID_FORNECEDOR';
    pesquisasItem.tipoFDB:='Integer';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[4]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesFORNECEDOR';
    pesquisasItem.titulo:='Fornecedor';
    pesquisasItem.campo:='FORNECEDOR';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='LIKE';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[5]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDESCRICAO';
    pesquisasItem.titulo:='Descri��o';
    pesquisasItem.campo:='DESCRICAO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='LIKE';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[6]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDOCUMENTO';
    pesquisasItem.titulo:='Documento';
    pesquisasItem.campo:='DOCUMENTO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='LIKE';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[7]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDT_VENCIMENTO';
    pesquisasItem.titulo:='Data Vencimento';
    pesquisasItem.campo:='DT_VENCIMENTO';
    pesquisasItem.tipoFDB:='Date';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[8]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesFORMA_PAGAMENTO';
    pesquisasItem.titulo:='Forma pagamento';
    pesquisasItem.campo:='FORMA_PAGAMENTO';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.comboItens.add('A VISTA');
    pesquisasItem.comboItens.add('DINHEIRO');
    pesquisasItem.comboItens.add('CARTAO DE CR�DITO');
    pesquisasItem.comboItens.add('CHEQUE A VISTA');
    pesquisasItem.comboItens.add('CHEQUE A PRAZO');
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[9]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesDT_PAGAMENTO';
    pesquisasItem.titulo:='Data de Pagamento';
    pesquisasItem.campo:='DT_PAGAMENTO';
    pesquisasItem.tipoFDB:='Date';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[10]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesID_BANCO_CONTA';
    pesquisasItem.titulo:='C�digo Conta';
    pesquisasItem.campo:='ID_BANCO_CONTA';
    pesquisasItem.tipoFDB:='Integer';
    pesquisasItem.criterio:='=';
    pesquisasItem.intervalo:='S';
    pesquisasItem.tamanho:=72;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[11]:=pesquisasItem;

    pesquisasItem:=TPesquisas.Create;
    pesquisasItem.agrupador:='pesBANCO_CONTA';
    pesquisasItem.titulo:='Banco Conta';
    pesquisasItem.campo:='BANCO_CONTA';
    pesquisasItem.tipoFDB:='Varchar';
    pesquisasItem.criterio:='LIKE';
    pesquisasItem.intervalo:='N';
    pesquisasItem.tamanho:=480;
    pesquisasItem.comboItens.clear;
    pesquisasItem.lookupSelect:='';
    pesquisasItem.lookupCampoID:='';
    pesquisasItem.lookupCampoTipoID:='Integer';
    pesquisasItem.lookupCampoDescricao:='';
    pesquisasItem.lookupGridItens.clear;
    pesquisasLista[12]:=pesquisasItem;

     { gerando campos no clientdatsaet de filtros }
    fnccpg.carregarfiltros(pesquisasLista, cdsfiltros, flwpOpcoes);
    { gerando campos no clientdatsaet de filtros }

    fnccpg.criarPesquisas(sqlcpg,cdscpg,'select * from gocpg', '', '', flwpOpcoes, flwpPesquisas, pesquisasLista, nil,cdsfiltros);
    {gerando objetos de pesquisas<<<}

    {>>>campos obrigatorios cpg}
    camposObrigatorioscpg[0] :=dbecpgcodprojeto;
    camposObrigatorioscpg[1] :=dbecpgid_fornecedor;
    {campos obrigatorios cpg<<<}

    {>>>objetos ativos no modo de manipula��o de dados cpg}
    objetosModoEdicaoAtivoscpg[0]:=pnlDadoscpg;
    objetosModoEdicaoAtivoscpg[1]:=spdCancelarcpg;
    objetosModoEdicaoAtivoscpg[2]:=spdGravarcpg;
    objetosModoEdicaoAtivoscpg[3]:=btnprojeto;
    objetosModoEdicaoAtivoscpg[4]:=btnFornecedor;
    objetosModoEdicaoAtivoscpg[5]:=btnBancoConta;

    {objetos ativos no modo de manipula��o de dados cpg<<<}

    {>>>objetos inativos no modo de manipula��o de dados cpg}
    objetosModoEdicaoInativoscpg[0]:=spdAdicionarcpg;
    objetosModoEdicaoInativoscpg[1]:=spdAlterarcpg;
    objetosModoEdicaoInativoscpg[2]:=spdExcluircpg;
    objetosModoEdicaoInativoscpg[3]:=spdOpcoes;
    objetosModoEdicaoInativoscpg[4]:=tabPesquisas;
    {objetos inativos no modo de manipula��o de dados cpg<<<}

    {>>>comando de adi��o de teclas de atalhos cpg}
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Adicionar novo registro',VK_F4,spdAdicionarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Alterar registro selecionado',VK_F5,spdAlterarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Gravar �ltimas altera��es',VK_F6,spdGravarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Cancelar �ltimas altera��e',VK_F7,spdCancelarcpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Excluir registro selecionado',ShortCut(VK_DELETE,[ssCtrl]),spdExcluircpg.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'Importar Planilha',ShortCut(VK_f9,[ssCtrl]),spdImportar.OnClick);
    fnccpg.criaAtalhoPopupMenu(Popupcpg,'[Excluir Selecionado(s)]', ShortCut(vk_f3, [ssCtrl]),spdexcluirselecionados.OnClick);

    {comando de adi��o de teclas de atalhos cpg<<<}

    pgcPrincipal.ActivePageIndex:=0;

    {>>>abertura dos datasets}
    cdscpg.open();
    {abertura dos datasets<<<}


    {filtros projeto}
    if ParamStr(4) <>'99' then
    fnccpg.filtrar(cdscpg,'CODPROJETO='+ParamStr(4));
    {filtros projeto}

    fnccpg.criaAtalhoPopupMenuNavegacao(Popupcpg,cdscpg);

    fnccpg.definirMascaraCampos(cdscpg);

    fnccpg.configurarGridesFormulario(fcpg,false, true);

    {<<vers�o da rotina>>}
    lblversao.Caption:=lblversao.Caption+' - '+fnccpg.recuperarVersaoExecutavel;
    {<<vers�o da rotina>>}


  end);

end;

procedure Tfcpg.spdAdicionarcpgClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcpg.TabIndex;
	if (fnccpg.adicionar(cdscpg)=true) then
		begin
			fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
			dbecpgcodprojeto.SetFocus;
	end
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false);
end;

procedure Tfcpg.spdAlterarcpgClick(Sender: TObject);
begin
	pgcPrincipal.ActivePageIndex:=tabcpg.TabIndex;
	if (fnccpg.alterar(cdscpg)=true) then
		begin
			fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
			dbecpgcodprojeto.SetFocus;
	end
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false);
end;

procedure Tfcpg.spdGravarcpgClick(Sender: TObject);
begin
	fnccpg.verificarCamposObrigatorios(cdscpg, camposObrigatorioscpg);

	if (fnccpg.gravar(cdscpg)=true) then
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false)
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
end;

procedure Tfcpg.spdCancelarcpgClick(Sender: TObject);
begin
	if (fnccpg.cancelar(cdscpg)=true) then
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,false)
	else
		fnccpg.ativarModoEdicao(objetosModoEdicaoAtivoscpg,objetosModoEdicaoInativoscpg,true);
end;

procedure Tfcpg.spdExcluircpgClick(Sender: TObject);
begin
	fnccpg.excluir(cdscpg);
end;

procedure Tfcpg.spdexcluirselecionadosClick(Sender: TObject);
begin
    try

   cdscpg.Filter := 'sel = ' + 'True';
   cdscpg.Filtered := True;

   cdscpg.First;
   while not cdscpg.Eof do
   begin
      fnccpg.excluir(cdscpg,true);
     cdscpg.First;
   end;
  finally
   cdscpg.Filtered := False;
  end;
end;

procedure Tfcpg.btnBaixaClick(Sender: TObject);
var
  i: Integer;
begin
  cdscpg.Filtered := False;
  cdscpg.Filter := 'SEL = ' + 'True';
  cdscpg.Filtered := True;

  cdscpg.First;
  while not cdscpg.EOF do
  begin
    if cdscpgSEL.Value then
    begin
      if cdscpgVL_PAGO.AsCurrency > 0 then
      begin
        application.MessageBox(pchar('Registro j� baixado! Documento: '+cdscpgDOCUMENTO.AsString),'Baixa de registro.',MB_ICONEXCLAMATION);
        cdscpg.Next;
        Continue;
      end;

      if not(cdscpg.State in [dsEdit]) then
        cdscpg.Edit;

      cdscpgVL_PAGO.AsCurrency := (cdscpgVL_BRUTO.AsCurrency + cdscpgVL_JUROS.AsCurrency +
        cdscpgVL_MULTA.AsCurrency) - cdscpgVL_DESCONTO.AsCurrency;

      cdscpgDT_PAGAMENTO.AsDateTime := Trunc(Date);

      cdscpg.ApplyUpdates(0);;
    end;
    cdscpg.Next;
  end;

  cdscpg.Filter := '';
  cdscpg.Filtered := False;
end;

procedure Tfcpg.cdscpgBeforePost(DataSet: TDataSet);
begin
	if cdscpg.State in [dsinsert] then
	cdscpgID.Value:=fnccpg.autoNumeracaoGenerator(dm.conexao,'G_cpg');
end;

procedure Tfcpg.sessao;
var host: string; arquivo: tinifile;
begin

  try
     //var host: string; arquivo: tinifile;
    arquivo            :=tinifile.Create('C:\goAssessoria\parametros.ini');
    host               :=arquivo.ReadString('local1','propriedade1','Erro ao ler par�metro');
    dm.conexao.Close;
    dm.conexao.Params.Values['Database']  := host+'\goassessoria.fdb';
    dm.conexao.Params.Values['User_Name'] := ParamStr(1);
    dm.conexao.Params.Values['password']  := ParamStr(2);

    dm.fdConexao.Close;
    dm.fdConexao.Params.Values['Database'] := host+'\goassessoria.fdb';
    dm.fdConexao.Params.Values['Username'] := ParamStr(1);
    dm.fdConexao.Params.Values['Password'] := ParamStr(2);

    lblusuarioresultado.Caption := ParamStr(3);
    lblProjetoresultado.Caption :=ParamStr(4);
    dm.conexao.Connected := True;
    dm.fdConexao.Connected := True;

    except
     on e : exception do
     begin
        application.MessageBox(pchar('Sem conex�o de rede.Motivo'+#13#10+e.Message),'Exce��o.',MB_OK);
        application.Terminate;
     end;
  end;
end;



procedure Tfcpg.altenarPopupTabSheet(sender: TObject);
begin
	if (sender is TcxTabSheet) then fcpg.PopupMenu:=(sender as TcxTabSheet).PopupMenu;
end;

procedure Tfcpg.spdImportarClick(Sender: TObject);
begin
    if (fimportacao = nil) then Application.CreateForm(Tfimportacao, fimportacao);
    fimportacao.ShowModal;
end;

procedure Tfcpg.spdImpressaoClick(Sender: TObject);
begin
	popupImpressao.Popup((Sender as TSpeedButton).Left + 65,(Sender as TSpeedButton).Top + 60);
end;

procedure Tfcpg.spdOpcoesClick(Sender: TObject);
begin
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Height,TSpeedButton(Sender).Width-50)) do
  begin
    popupImpressao.Popup(X, Y);
  end;
end;

procedure Tfcpg.menuImpRelSinteticoClick(Sender: TObject);
begin
if not cdscpg.IsEmpty then 
	fnccpg.visualizarRelatorios(frxcpg);
end;

end.

