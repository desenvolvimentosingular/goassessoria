object fcpg: Tfcpg
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Lan'#231'amento Contas a Pagar'
  ClientHeight = 566
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  PopupMenu = Popupcpg
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object pgcPrincipal: TcxPageControl
    Left = 0
    Top = 54
    Width = 644
    Height = 512
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = tabcpg
    Properties.CustomButtons.Buttons = <>
    Properties.Style = 10
    Properties.TabSlants.Positions = []
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    TabSlants.Positions = []
    ClientRectBottom = 512
    ClientRectRight = 644
    ClientRectTop = 19
    object tabcpg: TcxTabSheet
      Caption = 'Lan'#231'amento Contas a Pagar'
      PopupMenu = Popupcpg
      OnShow = altenarPopupTabSheet
      object pnlLinhaBotoescpg: TPanel
        Left = 0
        Top = 468
        Width = 644
        Height = 25
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object spdAdicionarcpg: TButton
          Left = 5
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F4 - Adiciona um novo registro.'
          Caption = 'Adicionar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = spdAdicionarcpgClick
        end
        object spdAlterarcpg: TButton
          Left = 76
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F5 - Alterar registro selecionado.'
          Caption = 'Alterar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = spdAlterarcpgClick
        end
        object spdGravarcpg: TButton
          Left = 147
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F6 - Gravar altera'#231#245'es no registro selecionado.'
          Caption = 'Gravar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = spdGravarcpgClick
        end
        object spdCancelarcpg: TButton
          Left = 218
          Top = 1
          Width = 65
          Height = 22
          Hint = 'F7 - Cancela altera'#231#245'es no registro selecionado.'
          Caption = 'Cancelar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = spdCancelarcpgClick
        end
        object spdExcluircpg: TButton
          Left = 289
          Top = 1
          Width = 65
          Height = 22
          Hint = 'CTRL + DEL - Exclui registro selecionado.'
          Caption = 'Excluir'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = spdExcluircpgClick
        end
        object spdImportar: TButton
          Left = 355
          Top = 1
          Width = 65
          Height = 22
          Caption = 'Importar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
          OnClick = spdImportarClick
        end
        object spdexcluirselecionados: TButton
          Left = 426
          Top = 2
          Width = 62
          Height = 21
          Caption = 'Selecionar todos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          Visible = False
          OnClick = spdexcluirselecionadosClick
        end
      end
      object SCRcpg: TScrollBox
        Left = 0
        Top = 0
        Width = 644
        Height = 468
        VertScrollBar.Range = 400
        Align = alClient
        AutoScroll = False
        BorderStyle = bsNone
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        object pnlDadoscpg: TPanel
          Left = 0
          Top = 0
          Width = 644
          Height = 741
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object pnlLayoutLinhascpg1: TPanel
            Left = 0
            Top = 0
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 9
            object pnlLayoutTituloscpgid: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  C'#243'digo:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgid: TPanel
              Left = 120
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgID: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'digo'
                DataField = 'ID'
                DataSource = dscpg
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg2: TPanel
            Left = 0
            Top = 30
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 10
            object pnlLayoutTituloscpgdt_cadastro: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Data de Cadastro:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgdt_cadastro: TPanel
              Left = 120
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgDT_CADASTRO: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'Data de Cadastro'
                DataField = 'DT_CADASTRO'
                DataSource = dscpg
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg3: TPanel
            Left = 0
            Top = 60
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 11
            object pnlLayoutTituloscpghs_cadastro: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Hora de Cadastro:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpghs_cadastro: TPanel
              Left = 120
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgHS_CADASTRO: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'Hora de Cadastro'
                DataField = 'HS_CADASTRO'
                DataSource = dscpg
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg4: TPanel
            Left = 0
            Top = 90
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object pnlLayoutTituloscpgcodprojeto: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  C'#243'd.Projeto:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgcodprojeto: TPanel
              Left = 120
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgCODPROJETO: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'd.Projeto'
                Color = 15722462
                DataField = 'CODPROJETO'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutCamposcpgprojeto: TPanel
              Left = 202
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 2
              object dbecpgPROJETO: TDBEdit
                Left = 28
                Top = 5
                Width = 400
                Height = 21
                Hint = 'Projeto'
                DataField = 'PROJETO'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object btnprojeto: TButton
              Left = 204
              Top = 6
              Width = 20
              Height = 18
              Caption = '...'
              Enabled = False
              TabOrder = 3
            end
          end
          object pnlLayoutLinhascpg6: TPanel
            Left = 0
            Top = 120
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object pnlLayoutTituloscpgid_fornecedor: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  C'#243'd.Fornecedor:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgid_fornecedor: TPanel
              Left = 120
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgID_FORNECEDOR: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'd.Fornecedor'
                Color = 15722462
                DataField = 'ID_FORNECEDOR'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutCamposcpgfornecedor: TPanel
              Left = 202
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 2
              object dbecpgFORNECEDOR: TDBEdit
                Left = 28
                Top = 5
                Width = 400
                Height = 21
                Hint = 'Fornecedor'
                DataField = 'FORNECEDOR'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
              object btnFornecedor: TButton
                Left = 2
                Top = 6
                Width = 20
                Height = 18
                Caption = '...'
                Enabled = False
                TabOrder = 1
              end
            end
          end
          object pnlLayoutLinhascpg8: TPanel
            Left = 0
            Top = 180
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object pnlLayoutTituloscpgdescricao: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Descri'#231#227'o:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgdescricao: TPanel
              Left = 120
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgDESCRICAO: TDBEdit
                Left = 6
                Top = 3
                Width = 480
                Height = 21
                Hint = 'Descri'#231#227'o'
                DataField = 'DESCRICAO'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg9: TPanel
            Left = 0
            Top = 210
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 4
            object pnlLayoutTituloscpgdocumento: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Documento:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgdocumento: TPanel
              Left = 120
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgDOCUMENTO: TDBEdit
                Left = 6
                Top = 5
                Width = 480
                Height = 21
                Hint = 'Documento'
                DataField = 'DOCUMENTO'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg10: TPanel
            Left = 0
            Top = 240
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 5
            object pnlLayoutTituloscpgvl_bruto: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Valor Bruto:'
              TabOrder = 0
            end
            object pnlLayoutCamposcpgvl_bruto: TPanel
              Left = 120
              Top = 0
              Width = 100
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 1
              object dbecpgVL_BRUTO: TDBEdit
                Left = 6
                Top = 5
                Width = 90
                Height = 21
                Hint = 'Valor Bruto'
                DataField = 'VL_BRUTO'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutCamposcpgvl_multa: TPanel
              Left = 260
              Top = 0
              Width = 80
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 2
              object dbecpgVL_MULTA: TDBEdit
                Left = 6
                Top = 5
                Width = 75
                Height = 21
                Hint = 'Multa'
                DataField = 'VL_MULTA'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutTituloscpgvl_multa: TPanel
              Left = 220
              Top = 0
              Width = 40
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  Multa:'
              TabOrder = 3
            end
            object pnlLayoutTituloscpgvl_juros: TPanel
              Left = 340
              Top = 0
              Width = 40
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' Juros:'
              TabOrder = 4
            end
            object pnlLayoutCamposcpgvl_juros: TPanel
              Left = 380
              Top = 0
              Width = 80
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 5
              object dbecpgVL_JUROS: TDBEdit
                Left = 6
                Top = 5
                Width = 75
                Height = 21
                Hint = 'Valor Juros'
                DataField = 'VL_JUROS'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutTituloscpgvl_desconto: TPanel
              Left = 460
              Top = 0
              Width = 65
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' Desconto:'
              TabOrder = 6
            end
            object pnlLayoutCamposcpgvl_desconto: TPanel
              Left = 525
              Top = 0
              Width = 80
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 7
              object dbecpgVL_DESCONTO: TDBEdit
                Left = 6
                Top = 5
                Width = 75
                Height = 21
                Hint = 'Valor Desconto'
                DataField = 'VL_DESCONTO'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg14: TPanel
            Left = 0
            Top = 270
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 6
            object pnlLayoutTituloscpgdt_vencimento: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Data Vencimento:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgdt_vencimento: TPanel
              Left = 120
              Top = 0
              Width = 100
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgDT_VENCIMENTO: TcxDBDateEdit
                Left = 6
                Top = 6
                DataBinding.DataField = 'DT_VENCIMENTO'
                DataBinding.DataSource = dscpg
                Style.BorderStyle = ebs3D
                Style.LookAndFeel.Kind = lfStandard
                Style.LookAndFeel.NativeStyle = False
                StyleDisabled.BorderStyle = ebs3D
                StyleDisabled.LookAndFeel.Kind = lfStandard
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleFocused.BorderStyle = ebsFlat
                StyleFocused.LookAndFeel.Kind = lfStandard
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleHot.BorderStyle = ebsFlat
                StyleHot.LookAndFeel.Kind = lfStandard
                StyleHot.LookAndFeel.NativeStyle = False
                TabOrder = 0
                Width = 90
              end
            end
            object pnlLayoutCamposcpgforma_pagamento: TPanel
              Left = 273
              Top = 0
              Width = 156
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 2
              object cbocpgFORMA_PAGAMENTO: TDBComboBox
                Left = 6
                Top = 5
                Width = 139
                Height = 21
                Hint = 'Forma pagamento'
                Style = csDropDownList
                DataField = 'FORMA_PAGAMENTO'
                DataSource = dscpg
                Items.Strings = (
                  'A VISTA'
                  'DINHEIRO'
                  'CARTAO DE CR'#201'DITO'
                  'CHEQUE A VISTA'
                  'CHEQUE A PRAZO')
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutTituloscpgforma_pagamento: TPanel
              Left = 220
              Top = 0
              Width = 53
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '  For. pgto:'
              TabOrder = 3
            end
          end
          object pnlLayoutLinhascpg16: TPanel
            Left = 0
            Top = 330
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 8
            object pnlLayoutTituloscpgvl_pago: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Valor Pago:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgvl_pago: TPanel
              Left = 120
              Top = 0
              Width = 100
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgVL_PAGO: TDBEdit
                Left = 6
                Top = 5
                Width = 90
                Height = 21
                Hint = 'Valor Pago'
                Color = 15722462
                DataField = 'VL_PAGO'
                DataSource = dscpg
                Enabled = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
          end
          object pnlLayoutLinhascpg17: TPanel
            Left = 0
            Top = 300
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 7
            object pnlLayoutTituloscpgdt_pagamento: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  Data de Pagamento:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgdt_pagamento: TPanel
              Left = 120
              Top = 0
              Width = 100
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgDT_PAGAMENTO: TcxDBDateEdit
                Left = 6
                Top = 8
                DataBinding.DataField = 'DT_PAGAMENTO'
                DataBinding.DataSource = dscpg
                Enabled = False
                Style.BorderStyle = ebs3D
                Style.Color = 15722462
                Style.LookAndFeel.Kind = lfStandard
                Style.LookAndFeel.NativeStyle = False
                StyleDisabled.BorderStyle = ebs3D
                StyleDisabled.LookAndFeel.Kind = lfStandard
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleFocused.BorderStyle = ebsFlat
                StyleFocused.LookAndFeel.Kind = lfStandard
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleHot.BorderStyle = ebsFlat
                StyleHot.LookAndFeel.Kind = lfStandard
                StyleHot.LookAndFeel.NativeStyle = False
                TabOrder = 0
                Width = 90
              end
            end
          end
          object pnlLayoutLinhascpg18: TPanel
            Left = 0
            Top = 150
            Width = 644
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object pnlLayoutTituloscpgid_banco_conta: TPanel
              Left = 0
              Top = 0
              Width = 120
              Height = 30
              Align = alLeft
              Alignment = taRightJustify
              BevelOuter = bvNone
              Caption = '  C'#243'digo Conta:'
              TabOrder = 1
            end
            object pnlLayoutCamposcpgid_banco_conta: TPanel
              Left = 120
              Top = 0
              Width = 82
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 0
              object dbecpgID_BANCO_CONTA: TDBEdit
                Left = 6
                Top = 5
                Width = 72
                Height = 21
                Hint = 'C'#243'digo Conta'
                DataField = 'ID_BANCO_CONTA'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
            end
            object pnlLayoutCamposcpgbanco_conta: TPanel
              Left = 202
              Top = 0
              Width = 490
              Height = 30
              Align = alLeft
              Alignment = taLeftJustify
              BevelOuter = bvNone
              TabOrder = 2
              object dbecpgBANCO_CONTA: TDBEdit
                Left = 28
                Top = 5
                Width = 400
                Height = 21
                Hint = 'Banco Conta'
                DataField = 'BANCO_CONTA'
                DataSource = dscpg
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
              object btnBancoConta: TButton
                Left = 2
                Top = 6
                Width = 20
                Height = 18
                Caption = '...'
                Enabled = False
                TabOrder = 1
              end
            end
          end
        end
      end
    end
    object tabPesquisas: TcxTabSheet
      Caption = 'Pesquisas'
      PopupMenu = Popupcpg
      OnShow = altenarPopupTabSheet
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnlTituloPesquisa: TPanel
        Left = 0
        Top = 0
        Width = 644
        Height = 19
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Op'#231#227'o('#245'es) de pesquisa(s)...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object flwpOpcoes: TFlowPanel
        Left = 0
        Top = 19
        Width = 644
        Height = 70
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
      object flwpPesquisas: TFlowPanel
        Left = 0
        Top = 89
        Width = 644
        Height = 80
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
      end
      object pnlTituloResultadoPesquisa: TPanel
        Left = 0
        Top = 169
        Width = 644
        Height = 34
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  Resultado da pesquisa...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        object btnBaixa: TButton
          Left = 152
          Top = 6
          Width = 129
          Height = 25
          Caption = 'Baixar Selecionados'
          TabOrder = 0
          OnClick = btnBaixaClick
        end
      end
      object pnlResultadoPesquisas: TPanel
        Left = 0
        Top = 203
        Width = 644
        Height = 290
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 4
        object GridResultadoPesquisa: TcxGrid
          Left = 0
          Top = 0
          Width = 644
          Height = 290
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          object GridCPGDBTV: TcxGridDBTableView
            OnDblClick = GridCPGDBTVDblClick
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Insert.Enabled = False
            Navigator.Buttons.Append.Enabled = False
            Navigator.Buttons.Delete.Enabled = False
            Navigator.Buttons.Edit.Enabled = False
            Navigator.Buttons.Cancel.Enabled = False
            DataController.DataModeController.SmartRefresh = True
            DataController.DataSource = dscpg
            DataController.KeyFieldNames = 'ID'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = GridCPGDBTVID
              end
              item
                Format = 'R$ ,0.00'
                Kind = skSum
                Column = GridCPGDBTVVL_BRUTO
              end
              item
                Format = 'R$ ,0.00'
                Kind = skSum
                Column = GridCPGDBTVVL_MULTA
              end
              item
                Format = 'R$ ,0.00'
                Kind = skSum
                Column = GridCPGDBTVVL_JUROS
              end
              item
                Format = 'R$ ,0.00'
                Kind = skSum
                Column = GridCPGDBTVVL_PAGO
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = 'Sem registros'
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            Preview.Visible = True
            object GridCPGDBTVSEL: TcxGridDBColumn
              DataBinding.FieldName = 'SEL'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.Alignment = taCenter
              Properties.ImmediatePost = True
              Properties.NullStyle = nssUnchecked
              Properties.OnChange = GRIDResultadoPesquisaDBTVSELPropertiesChange
            end
            object GridCPGDBTVID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Options.Editing = False
            end
            object GridCPGDBTVDT_CADASTRO: TcxGridDBColumn
              DataBinding.FieldName = 'DT_CADASTRO'
              Options.Editing = False
            end
            object GridCPGDBTVHS_CADASTRO: TcxGridDBColumn
              DataBinding.FieldName = 'HS_CADASTRO'
              Options.Editing = False
              Width = 100
            end
            object GridCPGDBTVCODPROJETO: TcxGridDBColumn
              DataBinding.FieldName = 'CODPROJETO'
              Options.Editing = False
            end
            object GridCPGDBTVPROJETO: TcxGridDBColumn
              DataBinding.FieldName = 'PROJETO'
              Options.Editing = False
              Width = 200
            end
            object GridCPGDBTVID_FORNECEDOR: TcxGridDBColumn
              DataBinding.FieldName = 'ID_FORNECEDOR'
              Options.Editing = False
            end
            object GridCPGDBTVFORNECEDOR: TcxGridDBColumn
              DataBinding.FieldName = 'FORNECEDOR'
              Options.Editing = False
              Width = 200
            end
            object GridCPGDBTVID_BANCO_CONTA: TcxGridDBColumn
              DataBinding.FieldName = 'ID_BANCO_CONTA'
              Options.Editing = False
              Width = 70
            end
            object GridCPGDBTVBANCO_CONTA: TcxGridDBColumn
              DataBinding.FieldName = 'BANCO_CONTA'
              Options.Editing = False
              Width = 200
            end
            object GridCPGDBTVDESCRICAO: TcxGridDBColumn
              DataBinding.FieldName = 'DESCRICAO'
              Options.Editing = False
              Width = 200
            end
            object GridCPGDBTVDOCUMENTO: TcxGridDBColumn
              DataBinding.FieldName = 'DOCUMENTO'
              Options.Editing = False
              Width = 200
            end
            object GridCPGDBTVVL_BRUTO: TcxGridDBColumn
              DataBinding.FieldName = 'VL_BRUTO'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = 'R$ ,0.00'
              Options.Editing = False
            end
            object GridCPGDBTVVL_MULTA: TcxGridDBColumn
              DataBinding.FieldName = 'VL_MULTA'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = 'R$ ,0.00'
              Options.Editing = False
            end
            object GridCPGDBTVVL_JUROS: TcxGridDBColumn
              DataBinding.FieldName = 'VL_JUROS'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = 'R$ ,0.00'
              Options.Editing = False
            end
            object GridCPGDBTVDT_VENCIMENTO: TcxGridDBColumn
              DataBinding.FieldName = 'DT_VENCIMENTO'
              Options.Editing = False
            end
            object GridCPGDBTVFORMA_PAGAMENTO: TcxGridDBColumn
              DataBinding.FieldName = 'FORMA_PAGAMENTO'
              Options.Editing = False
              Width = 200
            end
            object GridCPGDBTVVL_PAGO: TcxGridDBColumn
              DataBinding.FieldName = 'VL_PAGO'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = 'R$ ,0.00'
              Options.Editing = False
            end
            object GridCPGDBTVDT_PAGAMENTO: TcxGridDBColumn
              DataBinding.FieldName = 'DT_PAGAMENTO'
              Options.Editing = False
            end
          end
          object GridResultadoPesquisaLevel1: TcxGridLevel
            GridView = GridCPGDBTV
          end
        end
      end
    end
  end
  object pnlTitulo: TPanel
    Left = 0
    Top = 0
    Width = 644
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = 6441265
    ParentBackground = False
    TabOrder = 1
    DesignSize = (
      644
      54)
    object lblTitulo: TLabel
      Left = 62
      Top = 9
      Width = 244
      Height = 13
      Caption = 'Lan'#231'amento Contas a Pagar - goAssessoria'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object spdOpcoes: TSpeedButton
      Left = 343
      Top = 8
      Width = 56
      Height = 16
      Hint = 'Op'#231#245'es extras do formul'#225'rio.'
      Anchors = [akTop, akRight]
      Caption = '&Op'#231#245'es'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = spdOpcoesClick
    end
    object imglogo: TImage
      Left = 8
      Top = 0
      Width = 48
      Height = 54
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D49484452000001FB
        000001EC0806000000D55804F10000000467414D410000B18F0BFC6105000078
        0349444154785EED9D077853D7DDFFFF10C822AB996D93CEA46DD2998EB44D77
        D399A46DBA9B1ADB6C082B1008848410902DBCC0183066198C81B0B71966D958
        B22C59DE7BC9DB92654BB2F6BE57E27F0EB9F44D8892D842B2AEA4EFE779BECF
        F3BE29887BCF1D9F7BEE3DE777FE1F0000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000008846F466D7C32D3D86676B
        1443BFAD68D13E5FD9AAFB437DA7E1979D2AF3D37AB3F3218FE7EA18EE8F0200
        0000209C1834381F2BAA52C5671C6E3C3A6F9DAC6FF26AB1314650648B4D1059
        67A44A744BB2CA3BD61E6838775CDCF37663E7D0CF59D63B96FBAB00000000E0
        2B0CC3DEA2D2DA1E2FA8EC9FB6F554EBEEC59BCA3A270A44AEFFAE2ABAEA2B31
        AB8A3CD3922506E1EE9A2B8595EAC976277307F75300000000E0132EB7677CEF
        A0EDEB97CA55B3B24EB41C5AB841AE2492677C09DE5788F4BD0B3694F61E2CE8
        4AAA6AD3FDDE6C73DDC3FD3400000000424D67BFE55BE764BDAF6E3CDA7C727E
        86AC3F4650346CC9DF98F844B163F9B6F2DA772FB4AF296BD2BC68B2B17773FF
        0C00000000461BBBCB73BBAC51F3D7B5071ACECC5E2BD5D057F2BE04EE4F6213
        44EE655B2B9A77E72B36C81A06FF3664763DC4FDB300000000083616BBFBAEC6
        2EC3CF4E8ABBDF7C634B792391B3DF3DF94F0A7D4B40BFFBE79C6DCB94D40DFC
        93FEDBDC660000000020D098ACEEFBEB3BF4BF3E2EEA5E21DC5D53322D4562F6
        25E820855DB0A1B4E384B8E70DA5C6FA358FC77B0BB75900000000F01737EB1D
        DBAFB37DB9BC59FBE2FE4B1DC2D57B6A2ECD5B2755C5258A3F72747DB0132F14
        5B966DABACD874AC29E74299727ABBD2F43D83C5FD00B7C900000000182E8306
        E767CFCB95739664C9EBE313C516225AF78DE20D71E8A703C7B414C960FAC186
        6397CA55D3154AD3D30E177B2BB70B00000000B8118661C70CE89D8F9535EBFE
        947BBE7DFDAC3525EA1B04CBD730F1C2227DF2DE9A73F9A5CAD9EA21FB63DC2E
        0100000080E266BC63543AC717A5F5EA7F66E7B5EE9C9F211BD11C791E858D17
        8A8D3B4EB76D6EEE36FEC462672770BB0800000044274E173BAE4F63FB8AA476
        E03F9B8F37EF9D95563C102328627D4834AC421E549C2BB22BE4A7247D4B1ABB
        8C3F375A997BB95D06000000A2872193EBA1A22A55ECC6A3CD4767A4960C0572
        8E3C5F42DF4E2CDB52D674B8B033B1BEC3F0AB2193F3416EF701000080C8C5EE
        F28CEF565BBEBEEF6247EAD4E4622D9162C449FEC690071966716669FBFE4B1D
        C935EDFAE774903E00008048C462774FE854999EBE58A69A99B0ABAA283641E4
        F025C6088F67E10679F7EE7CC5BA7695F9693A56816B1E000000207C31DB987B
        3BD5D66F9D2B55CE5DB1A3AA2C2E5164F321C1688B2B6157754159B3E62F037A
        FB179C2E162BED010000082FCC76E6BEAAB6A1DF1FB8DC2914EEAEBE3C6B4DC9
        C0C72D331BAD99B45A6C7E634B79C3E6E3CDBB2F57A8A775F69B9F36DBDC18CC
        07000080BFE8CDAEFB154AF33329EFD6E5C5AC2AA2A56CF9560887AFF19238A7
        24156B53F7379C28AA56FF57A9B13EEE72B363B9A60500000042CB90C9F9109D
        57BEF34C5BD68C54092D8413F183EE8218CF24A1D8B86A676541759BEEB73627
        2AF201000008217ACBB585697EB5E564CB0ED22B1DA2A2BA415C88FF61E6A697
        749C29E99EDF336079D2EA606EE79A1D000000083E4366D7C3D50AFDEF361E6D
        7E97568C236282E48394D804916DE9E6F2DA5392DED7BBD5966F59ECEE3BB9C3
        00000000041E97DB738BA2CFF84CDABEFA3C22212AF9702C691B968911143916
        6E94B71E2AE85C49673740FA000000020A2D6BAB31D81F1555AB625F5D2FEB20
        F2414F3E44895955E49CBB4ED6BDF742478A4269FE9ED3CD8EE30E1300000030
        7288E4C76B8CAECF1654F44F7A7B7BA594C8261A0BE1F035CE5969257D17CBFB
        A7DA9C98A70F0000608410C9DFAE1EB23F7E46DAB7604956793D15CB0DA24178
        9278A1D8B0F148E36E5A7BDFEEF2DCC930EC2DDC61040000003E08EBF18EED1E
        B07EE35041D7DBC2DDB5E766A4960C1099609E7C7884894B149B166596D56E39
        D1B2ED72B92AAE476DFE3AC37AC773871700004034E366BCE3BAD4B6EFEC38DD
        923979F5B5E97354F0B4C88B2FA920FC0F1D34E9884B14E9DFDA56515454AD7E
        D9CD7AF15D1F0000A21122F9F1ADBDC61F661D6FCE9E962C19E424E14B1E48F8
        8699281019C831DE6AB5BBEEE30E3D00008048C7E5F6DC5EDFA1FF75FAC18643
        F18945288413058911885CB3D74A3A4F497A5FB33A3CA8BD0F0000910AC37A6F
        296BD6BC90BCB7F6D424A1782846809E7C34258684F4F2EDB3D24A7A0E1576AD
        D09B5D9FE64E0D0000009180C5EEBE7FCF7945DAE4D5623DB9E1B3BE64804447
        A8F46313448E19A912656E7EFBDA4183F38BDC69020000201C315A99FB2F57A8
        A72CDF5E2E8B2337785F377F243A43A51F9728B24F49120D6E3DD5BAC960713D
        CC9D36000000C2019DC9F9E86949CFA237B75654C40B8BACA437EFF3868F2034
        54FA6F6DAB90B6F69A9FF578BC5856170000F84CBFCEFEE543051D2B976E2EAF
        9B92540CC923C34E8CA0C83B3D453298B6AFFE58559BEE0F0C7B1573F4010080
        4F18ADEE87F75E684F5B9459DA44246F83E4117F335170859D9E22D60873ABF3
        4BEAD4FF72BA3D28C50B0000A1C0EBF58ED1189C9FA3A3EBF75D6C5FBD2853DE
        18972876FE77D5159F3770041969262688BC9357171B5F5D2F6BD878B431E7BC
        5C396D50EF78943B05010000040B37E3BDADA259F3E2C6A3CDFB166C286D8B17
        169930850E198530B10922EB6B1BE58DA7247D8B3546D763DC290900002050D0
        57A9B246ED5FD71EA83F3E6F5D49EF24A1D8414753FBB8292348D01223105D9D
        9956A2A3033F0F1776AD54E91C8FB3AC0703FA0000E06669EB333D93BCB7E6EC
        FC8C5255BC50EC86E49150878E0999915A625A92256F78F7427B5A97DAF66D86
        BD0AE90300C04831981D8F9C2FED9B437A5155F189903CC2BFC4088AAE4E4F91
        98976E2EAFCFCD6FCF68ED353FC39DBE0000003E0E9D99F9749EA4678920A752
        327BAD448B91F508DF431F44A7A548AC4BB2CA9BCEC994F3ED4E6602773A0300
        00783FFD3AFB13478BBA57ACCAA996CC49976A6313207924FC32779D6C60CBA9
        D6DCB266CD5F2C76F73DDCE90D0000D14D57BFF9DB070BBA842BB22BCA5E5923
        35BCBCB2D0E74D1441C22553928ADD8B3795F5651E6D3A24A91DF88FD1E2BC9F
        3BDD010020BAF07ABD630BABD493966F2F2F9B995662C2377924D23279B5D8BD
        60834C9971A8E1E8952A75DC90D9F50877FA03004064E2F15E1DAB1E723C51DE
        ACFDF3FE8BED692B775697CC482DD143F248A4273641E47D6FF47E7913E9EDEF
        933769FE66B2B9D1DB0700440E36277B97AC61F0A5CDC79B72966DADA88B1716
        99C90D10CBCC22519BE9291243D29E9A8213E29EB73B55A6EFB1ACE716EE7201
        0080F0C2E16227886B0762379F68D9BB7473796BBC50ECF275E34390680C1D80
        3A7BADD42CCCAD2E3F5CD899DCD667FAB19BF18EE32E1F0000E03FF4DBE4C1CB
        1D298B334BBBE212456E5F373B0441DE2BD0336B4D893531B7A6F26041576A53
        B7E9E72EC67B2B7729010000BFA0654307F4CE2F9D2FED7B75FDE1C653E40666
        F475734310E4C3A1057A66A4963857E554D7EFBDD0BE41A9B53FC95D5A000010
        7A6899D0DE41EB53E74A950B330E379D5FB0A17410D3E710C4BFD001AB539244
        76F2C07CA6A245FB92C9EAFC1477A90100C0E8E3727B6EEB501A7F9827E97D33
        FD60E3A5F919322D46D6234860129728BEBA7C7B65E7CE336DB9D27AF57F8C56
        06A3F70100A3CB80DEFE8513E29E15C97B6B6473D2A5065F372B04416E3E9384
        62EFB22D15DDD9A75B7345D5AA38ADD1F159EE32040080E06073B27737741A9E
        DB7AB2E5DD5969F8268F20A39578D2D35F9459AA22D7DE81C2CAFEE98306D717
        B9CB1200006E0E87CB7307AD595FD9AA7FFEE0E50EE1EA3DB585F33364FDB109
        228FAF1B128220C10DB7BCAEE5CDAD158D070B3A93DA95A6678C16D703DC250B
        0000C3874E9D933769FEBA3BBF6DD35BDBCAEBA6A648ADE44603C123088F4247
        F0CF4A2BD6641C6A38754ED6FB9A4269FA019D15C35DC60000E01B8FC73BA6A5
        C7F4D3DCFCF6ADCBB65474C40BC598238F203C0F1D183B2355624C3FD878E18C
        4CB9B8A5C7400BF4A02A1F00E083300C3BB65F67FF7271ED407CCABB7525B109
        2294B3459030CB7BD3F68A6DC97BEB8AF324BDCB9ABA4D3F65D0D3070090A77F
        3A47FEC9A26AF5A4AD275B0E2EDC2057C708B08E3C82847BE213C54E414E5529
        BDB62D76F75DDC250F0088265C8CF7966EB5E55B0595FD33369F68393E6F9D4C
        8B4238081279999F51DA7FB8B073754DBBFEB7460B0AF400101530AC674CBBCA
        FCDD8B65AA57D61F6E3C3D2BAD780885701024B2431EE4BDCBB694B51E2CE84A
        AE6C1D7A5E6774605D7D002215AB83B9ABB451F3F7B4FDF517A7A78851080741
        A22CE4C1DEB324ABBC93D6DE2F6BD2BCA431BA50A0078048C16267EF69ED35FF
        E88CB46FD1EB9BE40A5F37010441A22ADE05EBA5BDB9F9ED59B286C17F986D6E
        BCDE0720DCB03BD93B3406C717AADAF4CFD3A21BC97B6B2FCECF2855C525621A
        1D82201FCCB4148931E5DDBA0B972BFA6774F65BBE6BB4A2400F00BCC6E6646F
        6FE834FEF4C0A5F6C42559E58D7189225A080753E81004194EDC335225831987
        1A0E1755AB63BBD5966FD89DCC6DDCED0500C007B426F7A72F96A9662EDC2057
        C426889C3E2E64044190E1C43335B9D890B6AFFE624145FFF44EB5F55BA42301
        E903104A06F4F6C7AADA747FCC39DBB6755AB244EFE3C2451004F12B9384628B
        70778DF8BC5C39473D64FB0277DB01008C06B410CE80DEF9B9CAD6A117B69E6C
        D93333AD64284650E4F575B1220882DC6CE21245966DA75A762894E667CC3606
        057A0008264E173B4E3DE4784CDEA4F9CBFAC3F587E9ABB69855FC96FC448188
        99915A32149B20C2E04004B9212FAF2CF44C492A369398F85EEF825CCBEE655B
        CAEA4E887BDE6CEED63F6BB2B1903E0081C660713F20AD1FFC5BDAFEFA53F142
        B1D9D7C5C8B74C4F910C09722AAFECBFD4994A6704BCBABE54159F28667CFD59
        0489A6D092D4E421D8F0D6B6CA9ACDC79BF76E3DD5BA6B495679DBB4148995EF
        D28F1114318B334BDB0E1776253676197E4EEE4D98B607C0CD6273B8EF50696D
        5FDB93AF58479EAC8DE462E3F532B3F44D0311BA6D7E4669F7EE7C45BAD9C6DC
        43F7C3EE64EE2CAEE98F210F2B67E66748FBC8030B0611225199D80491EBB58D
        A58AFD973A523AD5D6EF783CDEB12EB7E7D6D65EE38F73CF29362EC992B7D015
        ECE8F2B5BEFE3E8FC22ED850DABDF742FBDAFA0EFD2F874CCE07AFDDB40000C3
        C7E664EF52E91C5FB958D63B6BF9B6F26A7261F1BA474C24EF9A24149B48EFBD
        2D6D5FFD3171EDC0BFDD0C3B9EDB9DFFC1B057C797356BFE9C7EB0E108917E47
        5CA2C8EEEBF71024C2C212C93B66AD2951AFC8AE1097366AFEEAF17C78353AB7
        9BB9ADABDFFCDDFD97BB925F5D2FEBA0AFCE7DFC16DFE299BB4EA6CC39A7C8EC
        EAB77CD7CD78C771BB0300B81117E3196FB4BA1FAC510CFDFA5041E7DBC97BEB
        CEBCB246AA2617122F254F7B1DB10945CE19A925DA37B755C872492FBEA46EF0
        EF4AADED718FF7EA186EB73E12AFD73B6650EFF85C7EA972CE8AEC2A31E9C968
        E212C52EBEBFC24490E1868E53A1DFE2176E94B76D38DCB83F5FD637AFADCFF4
        43AB83B99BBB0C3E963E8DF5ABA78ABB17AF3DD0706CDE3A69F7E4D5C556227F
        9FFF164FC2CECB90756F3DD9BCED4AF5C07F7B062C4FD95DEC1DDCEE000088F4
        3EBFFF5287706EBAAC835C30B4A74B9FE6793BF08EAE8EF7CADA92FEAD275B76
        34F7987FEAF1786FE176C56F7A066D5FCF39DB9A496E8C1D53938B6D61F00A13
        417C8648FE2A79501F240FEC1744D5EA5893D5FD10779AFB057D3036981D9F3E
        5FDA377B45768574565AC9505C22BFA54FE2240F3ABAE4BDB5A78B6BFAFFA333
        393FED62BC58571F4427669BEB6E85D2F4BDC4DDB5E7C9C5C1FBD7D974643DED
        A92CCA2C6B2EAC524FA237216E5702867AC8F145FA2D7351A6BC9548DFCCF39E
        0C827C20F142B1FDCD6DE515F226CD5F87DB831F09F437AF54A9E2053955E299
        6925BA89E4C1C2D776F0289E4942B1F19D1D15A2C24A552C913E56DB03D183C5
        CEDEDDD2637A96F48CB74D49120D900B82D7256D8970BD44F2862559E5D544C4
        89BD83D6AF73BB123486CCAE478E8BBADF26FF66FD246131BEEB23BC0DFDF444
        24EF989321576D3CD6BC47A9B53DC99DC64183F4926FA7DFFD976EAEA88B4F14
        3B7C6D17DF42E7EABFB1A5ACACB65DFF1CD9FE9B7E1B08002F6118760C1D99DE
        D069FC19B921EC224FBB3A7201F07AD01D91BCEB3DC9CB2B8978DFD018DDA3BE
        1C2669B34F6D3DD5BA7D5AB2444BB687D7331190E80A27791B91BC929CA3D9AD
        BDE6675D6ECFA896955569ED4FAC3DD070787A8A4443A41F0E0FC5DE57D648FB
        F224BD0B06F4F62F3A5DECADDCAE0010FE98ACEE4F55B5E9FF90B6AFFE0479BA
        A5256DF93DB25E20BA3A35456A58BEBD527A4ED63BD760616EEA9BE3CDC2B0DE
        F157AAD4F1CBB795CBA62489F4B10922540B444296FF493E5DDABBF34C5B56F7
        80ED3BE4613E64D26259CF3885D2FCFD8D471B76CD58231B0C87192E318222D7
        FC0C59E7E1C2AE77FA75F6AF10E9A3F63E086FDA95E6EF0A72AA2E905EA9899C
        E4BC5F818E6CA77DC186D2E6C24A553CE955DFCBED062F2037843B257503FF12
        ECAA2E203D7D1DD95654E5434635E441D33E779DAC63FFA5CE642A298FF72A6F
        069DD15930BD83D6A7B2CF283692876235DD565FFBC0A7900727F7AC3525CABD
        173A92FB34B6A7306D0F841D068BFB213A20851B611F16928F17160D0A77D79C
        EAEC373FCDED062FA1BDA8AA36FDEF92F654E74D4B9168207D24D82152F21079
        1AD30FD6EFD7999C8F793C1EDE7E73A603673506FB17884053A6A748FAC8F511
        0E05AC98196B649A6DA75A3677AAADDF66594FC007FF021030E80D404F249F2F
        57CD58BAB9BC929CC07C7EB2769138C84DCC3A35453A98B6AFFE40B542FF9CC3
        C984CDBC58DAAB6AE834FC22FD40DD814942B181EC0FBDA9A11C2F1288D04F45
        4E727DD85E5E5968599429AF3A2BED9BEF0CB379E326ABFBC1B4FD0D0789F0AD
        647FE8FD88EF83F998C9ABC59AB3B2BE39E4FABE2518337E00F00BB3CDFD405D
        FBD0AF8F16F5BCBD7A4FDDB919A925B4100E9F9FA4DD448C036F6EAD90EE3AA7
        58276BD0FE456B707C8E4FAF23FDA15D65FEEE9EF38AD4B7B32B4553928AB564
        3F217DC49FD0B770D6B9EB648A8C430DFBF24B95B33B54A6EF32AC276C0792D1
        6BBBB5D7F8833C49EFFCB4FDB58767A595D0DEBEED86FDE655C8F6395EDB286F
        DA76AA755B51B57AA24A6BFB2A113FE6E983D167C8EC7C24E79C620D7D4D1F9B
        2032931E007D62E6F3C031BA6D56E1EEEA93BD83B62719F6EA388F27F25E95D1
        C14A2A9DE3F183059D89F3D695D04F28A8C18F0C3756616EF599AA36DD1F9C6E
        CF9DE1FE00EC0B8FC73BC6E1622714D70EFCEBAD6DE5D2B844DE8F25F2C4088A
        1CF142B1F18DCD72B9A85AFD5F728D7FA80C37000187CAA4A5C7F4E3953B2A0B
        88E46DF45B9E8F13946F71BCBABEB48596E135585CF773BB12D190879931037A
        E7E7F3A4AAD7166F2AAB27C7291CA62521A1899BF4240DA78ABB5F23228C9AF2
        AE645FEF94370EFC49B8BBE61CB997597CB40BAF422B774E128ACD8B324BEB2E
        C8FB66D087166E5700081C6EC67B2B2D069171A8E1E0AC35252A7271F0FD3531
        7D5AB711D1559F14772FEAD759BFE872F377505130D1185D9FBE58D63F73EEBA
        D26ED22678BD8F5C8F637252C9D0E6E3CDD99DFD96A7DD4C745E1F54FA27C4BD
        AF4F492AA605BE78FF268C96D29EBC5A6C5AB05EDA40EEC9BFC6777D1010E8EB
        BCB266DD9F52F7D59D9E95563210972872935EA2CF939027A16F1AACB4521579
        FA9D464517AD927F3FE4616D2C5D7BFB9D1D1545E1D08B418217FAC96D6A72B1
        3AE76C5B46CF80E59B4476B773A749D4E272B3E39BBA4DCFD2079FE929122A7D
        DECF70A1D29F9926E93F54D0B94A6B747C9EDB15004606AD412DAA56BF9C985B
        7591489E16AA60782E797A13B3ACCAA9BE44B6FB3F5A93FB61A78BC59CD51BD0
        5BDCF74BEB07FF96B4A7F65CFC7B23F8316D2F4A423FE5CC4C2B56D292CF4AAD
        FD2B3687FB4EEEB4001C36277B476BAFF1999D67DA36D2CA80F4C1C8575BF225
        F49E3C25A9D83A3FA3B4FBDD8B9D292A9DE3098F0703F9C03069EA32FC549053
        5D3033AD444B25EFEB24E3595CA4B76A3870B9F31D9DC9F930E9C9E364FF0486
        483B95356B5ECC38DC7480DC2C785FBA18B9B9CC482D511D17F7BCA11E727CC9
        EE1ADD92B6E108E9EC4CA09F36E86A9C44A49D74909CAF76E54B38E9DBE7AD93
        F5EE3CABC824DBFE5D866131900FF8466B743C7A5AD2F3DAE2ACF2A630F8264F
        47D73BE213C53A22AC3D556DFADF58EC0C7A2A23446F76DD5FD3AEFFF5E6132D
        3BA7A748FA499BD2FA03BEDA1B09BFD08760D3E2CCD2DAAAD6A13F10C9E3E63F
        4288F4EFE8565B9E3A5AD4FDE6AC34692F6DD31BDA9857799FF4FBB69C6ACD6E
        EE31FF98F5E09B3EE0A0652F8F5EE95AF9D6B6CA2AD203304EE4F7B2AAF4B5B3
        7D6A8A549775BC795B7D87E1E7F4D5B49BC1097D33182CEE4FD1023D09BB6A2E
        90F6A57390F17A3F3C431FD2E943B0FEAD6DE5C517CB54D3E9822B6E86C52A6B
        370191FE9DE5CDDA3F2EDB5221250F5066D2C654FABC5EA3225E58E49CBD56D2
        77B2B867A9C3C5DEC5ED0A8826864CCE47486FEE97470A3BDF5ABDA7F6DCECB5
        52D5A4D5623B8F25CFC6258A0CEFECAC2AA28570A40DDA9706F5F6CFDB9D1858
        14684C56F77D65CDDA17DEBDD0912AD8557D65665AC920697FBED75088F678E2
        AF4DC92A6BDE78A471EFB952E56CBA2685D581A95981862EEAD5D26B7E264FD2
        BB70DDC1FA230B36C8BB62138AE88057DEF6F827AF2EB62DDD5C5EBB3DAF756B
        61657F6C57BFF99BE15609118C105A24E3BC5C35FFF54DA50D53924486975716
        D2DE1BAFE7C9D34145F3334ADB8BEB06FF69B4329FE2760504193AC887DCD8EE
        57F4199F3925E95B427A34D5E478F0BDD46834C6FDF6F64A7969A3E6EF8306E7
        E75C8C17AFEA4709BB93B9AB5F677F425C3BF09FB47DB5A7C83D953E18F3FAF3
        677CA298F6F60784B9D505E7E5CAB93A9373D497ED0641842E9C52D9AAFDC3A6
        634DEFCE49970ED0291BBE4E041E85210F22B6C599A5F5A724BDAF75AA4CDF72
        60647DC8B03A3CB777F65BBE7956DAFBEA8A1D5552F20046EB8CFB3A6EC82885
        1C03C79C0CB96AEBC9961DED2ACBF731F3247438DD9E5BD443F6C7A4F5EABFBD
        9D5D2925C787F705ACE2124557E7A6CB74829C2A499EA477B1C5CEDECDED0E08
        47C853FEEDB286C17F6E3CDA7CF8B58DB28E7861918B0EE0F075F0F992D80491
        75F9F64AD95969DF5C22F96F9B6D0C5E45F204AB83B99D1C93A7779D536CA4B3
        1FC8F1C208FE510E795077D16956B9F9ED1BAA15FADF694DEE47B8C303428CC3
        C98CA7D3F6C80358361129EF07F3D190EBF8EAECB5D2A12D275AF6F469ECDF60
        59CC640A2BEC2ECF9DE2DAC197D30F369C5A9429EF8D178A3D3C973C1B9F2836
        09722AAFD001455DFDE6AF5BEC6E7C8FE7293A93EBC1A26A755CEABEBA339393
        4AAEAFB6E7EBB82201CA4481C849AE65C5FE4B9DA9F51D865FE94CCE07B8C301
        7886C6E87AB8B255FB7BF240B66E7E86342CD6A7989A2C71ADC8AEA83F58D095
        D2AE323FC3B05731A093CFB8DC9EDB2F952B5F49DB579FBF70A3BC77A2E04A38
        D4AD772DD95C5E73A54A1DDBADB67C1DD383C203A7DB33AE67C0F27549BDE6DF
        19871A0EC50B8BE84A7B98B617E010C9BB9664C91B0F17762536741A7E49679E
        708700F01CADD1410742FFFADD0BED698B3795B5D24F2FBE8E315F426BEFCF4A
        2B712CDF5EDEB6F742FBC6E66EE3CF5D8C179F87F8C690D9F548CED9B62CF224
        A9A6AF667C1D4C3E859CF8CE6929122D11C581BA76FD2F1D2E4C0D0A47682962
        A5D6FE2559C3E0DFB28E37E74E4B965C1FBDEFF3B823C30E4B573ADB7CA23997
        DC747F6AB0B8EFE59A1C8419E401ED53F51D865F900736C1B22D157531027E7F
        D3A76F8167A44A9CCBB757B6E6E6B767D528867E6B773218BD1F6A6845AC3C49
        EFD2A43DB505D35324BC1F38454E74275D4887886197A46EE01F7D1ADB97B85D
        01610CC3B063545ADB97CA9A752F66E7B56EE5AAF2F93C07908F8D776A8AD424
        D8552D3E23ED5B48DAF471AE894198431ED8EEA3B52CB28E37E57273F5793D9D
        954A9F74C85CCBB694B5679F6EDDD9A5367F87DB15305AD0F58CBBD5966F1C2D
        EA5E21DC5D23999B2ED3F3FC9B3C43BF39CE5B27EBC93EA3C822BDC097C84D0C
        0B374428EA21FBE7F24B55F3E767C8BAB85E0C7DBDCFE735C3431DDA36EE69C9
        92A1E4BDB5F9E764BDAF2AAECD93F760DC4A04A2241D1CF220B760D5CE4A517C
        A2984A9F7ED3E7F52730F2F0EE22E7A6585A3FF86FA395C1A7A460E2747B6EA3
        CB191E1775BF9DB4B7E622B9912AC901E0F5C00FF2F4EA7C7D5379EBAEB38A4D
        25F59A7FF60D5A9EB23A5894B48D02DC8C775CBFCEF6157993F62FBBF3151909
        BB6AAECC5E2B1D240F7D18C1CF252E51E45CBAA5A279C391868367A5BDAFB5F4
        189ED59B5D0F47EB52B3D1848BF18CD31A1D8F35F7987E72AAB867E9DA03F527
        166F2A6B23E7046F5FF14F14145D7D658DD4B07C5B65CDF653AD3B8BAAD5937A
        06ACDFC0F91A40C88DF39653C5BD4B67A44AE8AB51DEF7902609C5F6655BCA1A
        0F157625D1C524B8DD00518CC1E27EA0BEC3F0DCD1A26EC18A1D5555A447434B
        F2FA3C7FA2202C7958EFA16DD13B68FB868BF162619A28879E039D6AEBB70B2A
        FAA76F3CDA7C7CF2EA62DAE3F775EEF026F142B1634996BCF55851D78A21B30B
        BDFD40D0D2637A76C186D21E5F0DCEA74C492AB6AEC8AEA83C5CD89958DBAE7F
        8E2EB2C2ED0200D730DB987B1A3A0DBF3A25E95BB67853399D96140E33460292
        18B2AF74964C765E6B7671EDC07FE87A045CB300700DD2B11BDB33687BEAC895
        2EC19C74295D988ADFDFF5496F9F3CB8F61F29EA5E65B4383188F466500F393E
        9771A8E1640C8F17A79992243227E6D6488F8BBA57D677E87F490EFA3DDCE603
        E013BAB088B45EF3CFA43D358533D65C1B7312B1DFF4E384126671666967CED9
        B66D92BA81FF0CE89D8F71CD00804F8C16D7BDE2DA81FF669F6EDB316F5D0995
        3E6F1F8AE978B1851BE44A5A7E97DB7C30522C76F79DE74A95AF4E4D91F27294
        FDCCB41243F2DE9A823C49EF1BCDDDC69F986D2EACAC04860DC37AC7B6F5997E
        982F57CD5F7BA0E12C399F74A4A71031DFF4631344AE655B2B14EF5EECDC406B
        D76B4DEECFB0AC07AB338261A31EB27F9E3E209207C5AD8B32CB78FB76974EF7
        5EB5B352AA33393ECD6D3A1809DD6AF3B7DECEAEACF2D5B8A10C799273BFB5AD
        A2FAACB46F11B9597F1F8570C0CDE074B1B7B6ABCCDFBD58D63F6BE3D1E64373
        D2A51A5A4CC6D7B91726A1DFE47BF75FEE4A296BD6FC99AE32C9ED2A007EA133
        391F92350CFE7D77BE22734996BC838F0FC5F461BDBE43FF6B6E93C1702137C0
        B1D2FAC17F4D4E2AE1CD4026FACD71DE3A998ADC900F96B7E85EA00B40709B0B
        C04DC3B057C7D02A8A97CA95D3B34EB4EC9E9556ACA1AF087D9D8B7C0DB909BB
        576457945EAA504D31DB18BCE90201C56865EE2D6BD2FC99964D7E6B5B79359F
        66B7906D719C97F7E155FE48A18377F65FEA48238D18EA011A1E7A42D16F32E4
        06FCEEA572D5ACDE41DB57B8CD0420E0D0424BEF5E50A4D0A538C350F62E616E
        F5E56EB5F96BDCEE001070CC36D7DD95ADDADF6D3ADEBC7792506C22E71E1FC6
        BCB8F79CEFD8C06D22182EEA21FB97490F3ACF47838E5A621344EED736CA9AB7
        9C6CD95958A99A8A6A5E2058783CDE5B88E49FBC52A58EDB7AAA75C7C28DF2CE
        B8C4F07C953F2D59A2DF7CA22987CE48B1D8DDF778BD5E7CA7074141A9B13D71
        E4BDE26A97A626175BC8C371283B879ECCA34D07B84D03C345A9B53F29CCAD29
        F7D1A0410FED4DD18145C7C53D6FB5F599BF657530B7729B0540C0B03998096D
        7DA667CE972A676C38D2B067D9D6CACA692912FD448188168B0AEBD1F9B10962
        E7FC8CD22EE1EEDAF3EF5E502495B7E89E576A6D4F385C1ECCA907014763703E
        226FD2FD6DC391C61353928A43B636C5DA03F5A7B84D02C385F472BE2EC8A9AA
        F6D5A0A3115A1E31913C6C9C92F42D6DEBB37CD7EEC2F77910181C2EF6765A2D
        8E56D69B9E2256C7088AE86C93B017FC47844E9972C626888CE401BA9CECF33A
        BA6810D6A107814263743D22A91BF857F6E9B69DF3D6C974E461D9D779382A49
        DD57778EDB2C305C88ECBF114AD95F0FE9691993F7D6159C95F6BD46474C931B
        35963C047E6173B2B73776197E42177821E7D4A578E1B5BAE03ECFBB480D7D6B
        B138B3B42DE76C5B6653B7F127745022D73C008C9876A5E9BBB9F9ED1BE66748
        95E4DC0AF95C7CC8DE0FF822FBEB99915AA24F3FD878EE82BC6F4EB7DAF224B7
        99007C22163B7B172DB6744AD2BB449053299E24145BC839C5EBCA60C14E8CA0
        C891B4A7EEB2A85A1DABD4DABFEC727BC672CD05C02762B2BAEFA96AD3FD6ECD
        FEFAD344F2BC19DB02D9FB01277BBECDB1F7CE5A53A2597FB8F1706DFBD0AF70
        83021F079D2654DBAEFFD5D1A2EEB7976F2FAF884B1445733D7C5FF1CECF90F5
        651D6FDE5358D91FDF3368FB2AA6B3828FC362774FA06BE31F2AE8142CC92A6F
        260F8DBCFAF405D9FB0127FB4A5F0DCA83B0AB72AA4BC4B50331FD3AFB17B84D
        06E07F74A9ADDF3E54D89DB06C4B451D37E0CED77984BC17EFACB4E2818D479B
        0FD31A031D4AE3776C0E3796B705FFC3EEF2DC4A3F811D1775BFB56C6B45430C
        4FC7B740F67EC073D95F1BB13F6F9DAC7FDBA9965C59C3E03F06F4F6CF729B0E
        A2188DD1F5E9AA36DDF36BF6D79FA1AFAA7D9D3BC8478756214BDB577BE682BC
        6FA64269FA8E8BC1DBB368C6E1626F6DED35FE80AE7FFFCE8E2A39B9EFBA7D9D
        377C0964EF079CEC2B7C35289F42A5FFEAFA52D5CE336DDB2A5AB42F6A8D8E87
        B95D005102ADF73EA0777E9E1EFFDCFCF68DE421B08F9C1B51FD4DFE6643E7EA
        0B73AB2F963669FF6CB1B377704D0DA2887695F9DBE74BFB660B722AA57CFA2E
        FF7181ECFD205C647F3D2FAF2CF42CDC20EF79F762E7DA1AC5D06F864C4E2C6F
        1BE1D091E42AADEDCBF226CD5FB24FB76D9DB5A664809C0B91387D2E5461176C
        286DCB93F4BED6DA6B7EC66871DEC7353D88609C2E765C4DBBFED7C97BEB2EC4
        2688EC3ECE0BDE06B2F7837093FDF5D0AA7B8B32CB3A0E1674AD6EEC1CFA85C3
        C5A2884884C1B09E71E4FC7C8AAEDDB0F944CBEEE929122D39F6907C90122F14
        5B966FAF2C3F2EEE79B3B1CBF00BBDD985B76711889BF18E576AED5FA1333496
        6C2E6F24C73EECDE8E41F67E10AEB2BF9E890211F3CA1AA99A963EAD6DD7FF86
        DCA01EC4F7C7F08561BD630C66E7438A3ED3337B2FB4A7909B515D5CA29816C3
        81E4472974A023BDA61276D514EC39DF9E226BD4FC59ADB37FC1CD60D9DC70C5
        E664EF24F7FA27AF540DC4651D6FDABD744B4573FCEAD055C0BBD940F67EC0C9
        9EB703F446100FE99998DECEAE949D95F62EE819B03C856A7CE183C5EEBEAB53
        6DFDD6C532D50C723E16C4268868211C5E0F128A82D0E229AE9757165A67AF95
        74E6972A676B4D6EF4F6C38821D2F9A9219DA09D673BB266A595A8C9F1E4DD92
        B5FE04B2F7830892FDFF32492836BFB3A3A2F852B9728A4A6BFB8213F3F4798B
        C5CE4E50284DDF3B2BED9BBF7C7B7929114B587D3B8CA64C492AD66C3DD54A07
        C8FE71D0E07C842E8FCD1D46C0437A076D5FCD3DDF9E317BAD54458E5F44BD19
        83ECFD2012657F3D93578B8D74952651B53A66406FFF1CB7CB8007389CECAD0A
        A5F9BB27C43D6F2CC992D3F9BC98231F1E714F4B9168361E6DDE5BDAA8798948
        FFB32E06ABEDF109B38DB9ABA5C7F46CDAFE86D3F478DD70FC222290BD1F44B2
        ECAF273EB1C890BABF21AFADCFF4036EB74108A1A3BD8BAAD5FF5D94296F20C7
        272C9797458A18D2D3D7919BEEC9E2DA817F9387E9471D2E169FCD4208AD7AA7
        E8337EFF5041C7AA05EBA5ED3E8E59C404B2F78368903D1766D9D6CA8A6A85FE
        777AB30B538B4280D1CADC47674EECBDD091467A87833E8E11127EF14C128A8D
        AB7656165CA91E8831585C9FE20E371825EC2ECF6D5DFDE66F9F1077BFB138B3
        B4851C9388AF3D01D9FB0127FBB01D8D3FD24C4D91EA361E6DDADDD865FC29E9
        61DECB35030822068BFB53F51D865F6ECF6BDD3C27434EBF1F46C42021E48389
        4B14E9B79D6AD93C68707D9E08683C77F84190A02B8392FBF713E74A9573966D
        A9A889E1E975355120BA3A2355629E9E22B1D2FFDBD79F196988ECCF72CD0086
        4BB4C99E8B674A52F1D0D6932DDB147DC667CC36D704AE394000A1D320ABDA74
        7FC83CDAB4875CEC28841305A115D8166F2AABCF93F4BC36A0777EC1E644FD8B
        6060B4BAEFBB54AE9AF276F6B5AA77BC1CEF1223B856A1D14A1E445A4F887BDE
        D97BA17DC3B4144940969B86ECFD204A657F2DF449987E77CC39DB96D1AD367F
        93CE45E59A05DC0456077B7759B3EE85B507EA8FD0F6256D0DC94759C88DDE39
        6F9DACE3E895AE37FB75F6C7C9B535C1E3C13CFD9BC5E5F6DCAA353A3EB7EB5C
        DB7A6E7AAACFF60F6118725F7591EBDE4C1EFA5A0E177609E99B1EBAEDE74BFB
        E64D4B2E36FAF83B230E64EF07D12CFBEB2127A73B5E28D6AFDC5975853C2D4F
        2537A6BBB9E601C3C4E660EE6D579A7F905FAA9CBB2AA7BA90DC884CA46D21F9
        28CFB51B7FB24443CE89CBBBCF2BD2CA5A74CF53F173A70D1806B4E25D9FC6FE
        D4A5F2FE991B8F341E7C757D69D7C40431AD81E0B3CD43153A7683DE430F5CEE
        10D62A867E63B6B93F307E83C87E3E913DBD2FF8FCFB230964EF0790FD87629B
        93216F3F21EE5EAC37BB1EE29A09F880DC84C6688CAECF10C1CF5AB6A5A26CA2
        40442BDDA1100EF251A1332F2C6F6EAB10B5AB2CDFB739DC7893F631D89DCC9D
        B4767DDABEDAA3A4A7AC276DC7CB87E7B84491837EA62BA81A9CC430ECADDCE6
        7F08F4EC430C64FF9171CCCF286D3D5FA69E61B460B19DF74317A619D03BBF78
        4AD2BB6851A6BC9EF4DE50080719493CE4C1D0B0F640FDC1FA0EFDAF88F4D1D3
        7F1FB464746397F127EB0F37BE3B35B95843DA8B97A3EB631344F475BD76E3D1
        A65C85F293A7350758F667B89F05C305B2FFC4D8166596D51454F6C75AECEEA8
        7FBD6FB4B8EE3F58D0B5627E86AC8DB40D0AE1203713963C289A56EFAE3D5DD1
        AC799E48FF2EEE348B5A48CF783C7D53363D45424BDBF2B227FFF2CA42FAD9D3
        40DF38D0E23DE4E17F58F515B8D7F8065FBF39D240F67E00D90F3B7454A9ACA4
        41FB5737137D35F73DDEAB63BAD5966F27E4569F236D11B60B6820BC0CEDB95A
        976F2F17496A07FE15ADDFF47B066DDFCCCE6BCD9ABCFADA2B7B5FED14D29007
        330F1D8B23DC5D73A6AE5DF76B37E3FDC857F6BE80EC430C643FA2D09B9269EF
        79C56AA7DB1315DF1BBD5EEF1885D2FC83CDC79BB367A44AFAC9FEF36E601012
        51B12EDD5C5EDADC637C963B05239E7E9DFD89DC738A75B3D65C5BA88697D717
        11BD8D3C8C1555B5E97E4F7AF2E3B84D1F11907D8881EC479E8902912D31B726
        BF46A1FBBDD3C5DEC1356544E1BD7A754C6397E9E7E9071B0ED15AE8E4624721
        1C6454427B90D3538AFBF324BD0B0D1677C40E92D518EC9FDF7FA94338375DD6
        1B23E0EDCC15BA5DC69CB3ADE92EB7E7A6EE759CEC03F2D602B2F703C8DEBF90
        8BD33339A9449FB6AFFE7843A7E19774FE2BD7A4618DC77B756C458BF6F9A43D
        B5A7E9E0A097571642F2C8A887089F0E00B3CF5B57D27EF0B262A5C6E87A943B
        45C31EF200F3F0D1A2EEE5AFAE2F55D0416E745F7DB5418843AF7BCB820DB29A
        2B55AA1886F5DEF40A87907D8881EC6F2EE46275BF3722B579774B8FE1276EC6
        1BB615C3BAFA2D4FD3F9D044F2DA890211248F843C5484746AD7ECB5929EDDF9
        6D6B955AFB931E8F376CC7CCD477189E5B94296F8A178A1D3C943CEDC5D3ED32
        CFCF90355C90F75DAB39C2B281298604D98718C83E302172744F5A2D1E7A6B5B
        A574EF85F6A40E95F969AFF72AAF2B86B918EFED7425C073B2DE5919871AF64F
        4F2D198C1188227E110D24FCC2F5F45DF46DD38AECAAA23DE715C9ADBDA61FB2
        3C173FCB7AC7779287E8F3A5CA57D20F361C9D9622D1D132B2BEF63154216D6B
        27F72E5D626ECD79FAC6A1B9DBF8ACF3265FD9FB22C0B2C73CFB914264FF75C8
        3EB021178F7B66AA44BD3B5FB1B667C0F24DAEA97983C1C23C422EEAB7176596
        35929BA7913EA890ED86E4917089975E63E4DCD59187D40374FD05878BBD9D3B
        BD7901EBB93AAEB876E03F2BB22B24D35324437C7D53367975B18E8E1B18D03B
        BF44041FD43624B2A7F3EC21FB5001D90727F4E93D5E586421426DA217934A6B
        FB72A86B83AB87EC5FDE7FB94BB860BDB48DDC28CD2FAF2CF4B9ED08120EA1BD
        FD78A19899B5A664306D5F7D9EB45EFDCF50D7C2A0635E640DEABF925EF2A559
        69259AD884C0ACF216E890B6A355EF54A2DAC1FFD22572B9CD0F2A9CEC877C6D
        CF4803D9FB01641FDC50A112B19A9664C9EB8E5EE95AD1AFB33ECE35FDA8E1F5
        7AC716550FC4CDCF90764D492AB6D19BA4AF6D4590704D7CA29825D2D725EDA9
        BD5458D93FC5686546BDEA257998FE62F2DE9A53B3D74A07E212457C9D42E7A2
        057BB2CF28325B7B0D3F24BDF9511B580CD98718C87E74325120BA4A2E32E3F2
        6DE5E57992DE451AA3EB11EE10040D3A7DAEA5C7F0E32D275B76BCB2463A08C9
        23911E22FDAB44FA43829CCA22BA28937D9456B26CEA36FD74F9F6F212F2EFF3
        756D08379D429B75BC79576397F1E726ABFB1E6ED3478D00CB1E03F4460A643F
        BAA1AFF566A695E80539D557EA3AF4BFE60E4340A18B51D4B6EB7FB3F168D3BB
        7485AC7821CADA22D115D2B3F6D29EFE96134D39EA21C797B94B23A0783C9EB1
        74419FCD275A72C975D6CDD357F6CC94249166FDE1C6FDD50AFD6F0D16F703E4
        FE70D3D3E8FC01B20F31907D68426E0CCCD2CDE5F50515D75E397E6029487F71
        B357C7CB1AD42FD151BFF333A47D93578B217924AA3335A9D84E7ADC95870BBB
        12FA34F627694548EE72F11BAE74342D6DBB79C17A69C7D414292F7BF3718962
        73DAFEFA13F226CD9F87CCAE07B8CD0F19907D8881EC4317DA13989B2E1B2027
        EEB9A22A559CD9C6F8B510888BF1DC26AA56C792DFC99BB7AEA43B5EC8DB5789
        0832EAA1D7D98CD412DBF2ED9535EF5E684F57284DCF783CFE158921F7CBA776
        E72BD6BDB6514607B9F2768D882949C5BA7D173B56D362444E17EB5779DB4003
        D98718C83EF4A1238AE767942AD71F693A206BD4BE440B597087E763A1AF114D
        36F653E4E6934EFE7E1F8FBF172248C843C7CDCC489558DFD852569F9BDFBEBE
        B1CBF00B3A458EBB9C3E16B39DB9F7C0A5F6C4655B2A6AA7A748ACBE7E3FC4A1
        53FB9C93578BF5829CCACB97CBFBA618ADEE7BB9CDE705907D8881ECF913D203
        71931E88EECDADE59539675B334B9BB42F996D1F9C4A64B133F735F7989ECD2F
        ED9BB3FE70C3BE851BE5CD938462AC278F20C3CE15FA80EDA6A3E657EEAC969C
        95F5BDA6D2D9BFC6B09E0F88DFEE64EF68E931FEE89C4CF9EAF26D15F2C9AB8B
        79779D917B86734EBA4C95F26EDD99E3A2EE379BBA0D3F315A5D0F907D09C977
        F98F83C87E2E641F42207B7E266655112DC3AB2727F5295A34A4A3DFFAED2345
        DD6FAFCAA92E9A932E55925E0ABDF1A0A42D82DC6462138A9C8B334B151B8F36
        1DA285707A076D5F3D2F57CE14E6569F9FBD56D21F23E0DF75460B612DDB52D1
        4404BF82AE4A69B10FEF6D602881EC430C64CFEF905EBB6D4996BCE9EDED9515
        AFAC916AC88DC745FE3BAADD21480043A7A5926B8D599429EF7D67475539FDAC
        4684CACBB9F2F4413FE350E3F1C62EC32F8D16674006F78E06907D8881EC1104
        41F81FFAA0BF28B3AC63FFA5CEB46EB5F55BDC2D3C6C08B0EC4F733F0B860B64
        8F2008C2DFC4AC2A62166C28EDDE7DBE636345B3E6C521B3EB41EEF61D5640F6
        2106B2471004E165DCF33364CAECBCD69DA58D9ABF6B8CAECF84AA204E2080EC
        430C648F2008C2AFCC4D97F56F3EDEBCB7B8A6FFBF037AE7E719F66AD84AFE3A
        907D8881EC1104417813D73B3BAAAB8AAAD593553AC7975C6ECF2DDCAD3AEC81
        EC430C648F200812F2B866A4960CD052D7D50AFDEF9C3C5B9F3F1040F62106B2
        47100419FDC4258A5DAFAE97F5A6EDABCBCB2BE95DDCDC637CD660718FFAD2BC
        A34580659FC7FD2C182E903D8220C8E8659250EC58B5B3B2E25851D7CAC62EC3
        CFED2ECF04EE761CD140F62106B2471004199DCCCB90A90F1574A634741A7F69
        73B27770B7E1A800B20F31903D822048704397B47E6B5B65F3C572D51CB3CDC5
        AB056A460B4EF67A5FED33D240F67E00D92308820427318222764996BC23F77C
        7B565993E625A78BBD95BBF5461D907D8881EC110441021B5A6B7FF1A6B29EEC
        338A1C69FDE0BF8C56266C6AD8070BC83EC470B22FF7D5A0088220C888E25DB8
        41AEDA7CBC797F51956ACA90C9F91077AB8D7A20FB1003D9230882DC7CA6254B
        4C1B8F351F2BA8E89FA9353A3ECBDD620107641F62207B044110BF4397C175CF
        4A2B56ED39DF9E31A0B77F91BBB5821B80EC430C648F200832FCC410C14F128A
        ED0BD6CB3AD3F6D79F3E2BED7BADA5C7F823A79BBD8DBBAD021F40F62106B247
        1004195EE8143A414EB5F8C0A5F6A47695F97B5EAF770C772B059F00641F6220
        7B044190E1655A8A44DFDA6BFE0977FB042380C87E0E913D8AEA840AC81E4110
        647899B5A66490BB75821102D98718C81E4110647821B21FE06E9D608440F621
        06B2471004195E88ECD5DCAD138C10C83EC440F6088220C30B64EF3F907D8881
        EC1104418617C8DE7F022CFBD3DCCF82E102D92308820C2F90BDFFA0671F6220
        7B044190E105B2F71FC83EC440F6088220C30B64EF3F907D8821B27F0AB28FBC
        C4AC2AF2FAFAEF0882F81FC8DE7F20FB1003D9475E260A44AE79EB4A7AC88D49
        4BFE6FBA5087CF3F8720C8C802D9FB0F641F6238D997F96A50243C3325A9D820
        A91D78B9A0B27FF2F2ED95F219A925E402BBE2F3CF220832FC40F6FE43643F1B
        B20F21907DE485C8DE5ED1A2FD033DBE0E177B577EA972FEBC75D2EE18C1B5E5
        387DFE1D04413E3990BDFF40F62106B28FBC10D95B88EC9FE70EF135EA3BF4CF
        2DD8206B22FF9B11DFF311C4BF40F6FE03D98718C83EF242854E64FF027788FF
        87C9EA7EF05041C7CA57D797B64E5E2D3611E9FBFCFB0882F80E64EF3F907D88
        81EC232FF49B3D91FD8BDC21FE107A8BFBE1FD973A57CF4D9775C4275E93BE83
        FC3DF6C6DF4110E48381ECFD07B20F31907DE48593FD877AF637627530F7D628
        F4CFEDBBD891F4D6B6CA92B84491D1D7EF2108F25E88ECFBB9CB078C10C83EC4
        40F69197E1CAFE46CECB95AF4C4B91686304452E5FBF8B20D11EC8DE7F38D9EB
        7CB5EB4803D9FB01641F79E164FF81017AC3A5A5C7F46CEABBB5C7C96F68C86F
        3137FE36824473207BFF81EC430C641F79B919D95358961D5FDBAE7F2E694FED
        E9C9ABC55AF29B903E829040F6FE03D98718C83EF272B3B2BF8ECBEDB9BDAC59
        F3E2AA9D9597E38562FA3D1F53F690A80E64EF3F907D8881EC232F44F674EADD
        4DCBFE3A762733415237F0AF153BAA44318222ABAF7F1341A22190BDFF10D9BF
        02D98710C83EF21268D95FC76C633EB5E3745B666CC2B551FB98AA87445D207B
        FF81EC430C641F7921B2370543F614AB839D70B14C357DD9968AD289826BD2C7
        C87D246A02D9FB0F641F6220FBC84B30654F71B93DB70C1A9C8F9D91F6CD5B92
        555ECEBDDAA7D2C7377D24A203D9FB0F641F6238D9CB7D3528129E09D66B7C5F
        986CCC7DF51DFA5F1E2AE81424ECAABE343D55A226DB80D1FB484406B2F71FC8
        3EC440F69197D194FD753C1EEF18AB83B95BA134FF20F5DDBA3CB21DCE1BB70B
        41C23D90BDFF40F62106B28FBC8442F6EF47A9B57F39E76C5BE69C74691FD91E
        7CD347222690BDFF40F62106B28FBC10D907649EFDCD60B432F7D1C23CD9A7DB
        B6901B247DB58FD1FB48D807B2F71FC83EC440F691173EC8FE3A3AA3E3A18A66
        CD0B9B4FB4EC99912A1924DBE7BE717B11245C02D9FB0F641F6220FBC80B9F64
        7F1DADC9FD88BC49F3D2FAC38D8727AF1605E4824790D10E64EF3F907D8881EC
        232F7C943DC5E9626F510F391E2BAEE97FF9F54DF206B2AD18C4878455207BFF
        81EC430C641F79E1ABECAFE362BCB7D0EFF9C97BEBF2B99AFB9E1BF70141F818
        C8DE7F20FB1003D9475EF82EFBEB74AB2D4F5D2A574D27E79F2836416422DB8E
        91FB08AF03D9FB0F641F6220FBC84BB8C89E6273B8EF50284DDFCB97ABE6ACDC
        5955345120B2907DC0203E849781ECFD07B20F31907DE4259C647F1D17E319AB
        353A3E53D7AE7FEEE0E5AED589B9350593938AE92B7E9FFB8820A10891BD8A3B
        65C10881EC430C641F790947D9BF1F8FF7EA18A3C5F96041A57AEAA24CB982EC
        13CAEF22BC0864EF3F0196FD69EE67C17081EC232FE12EFBEBD89DECAD15CD9A
        E777E72BD6CF5B57D243F60DAFF7919006B2F71FC83EC440F691974891FD75B4
        46C7C3F2C681BFEC38DDB2754EBA5445F6113D7D242481ECFD8793FD90AF761D
        69207B3F80EC232F9CECFFC81DE28861406F7F545237F0EF2D275B76CD4A2B19
        20FB8AD1FBC8A806B2F71FC83EC440F691974895FD75545ADB97C5B583FFDD78
        B4E940BC506CF6D50608128C40F6FE43643F1BB20F21907DE425D2657F9D9E01
        CB53FB2E75A4D2F2BB31E8E523A310C8DE7F20FB1003D9475EA245F614BDD9FD
        607EA9725EDABEFAB374BF89F4F13D1F095A207BFF81EC430C27FB525F0D8A84
        67A249F61486616F5128CDDFCD93F42E16E6565F881716E989F41DA42D207E24
        A081ECFD87C87E0E641F4220FBC84BB4C9FE3AACC73B566B747EA6A1D3F8F323
        57BA57ACDE537B7EEE3A996AA24004E923010964EF3F0196FD19EE67C17081EC
        232FD12AFB1BB139D9BBAB15FA3FAC3B587F225E28B6F96A2B041949207BFF81
        EC430C641F7981EC3F48A7CAF4746E7E7BE6922C796B7CA21803F910BF03D9FB
        0F277BBDAF761D69207B3F80EC232F90FD87315A994FC91AB57FCF3EA3D8B978
        535937913ED6D247461CC8DE7F20FB1003D9475E20FB8F66C8EC7AB8A84A3569
        F3F1E67D0B37C89571892294E045861DC8DE7F20FB1003D9475E20FB4F66D0E0
        7CB4A0A27F7AE6B1E6C3AFAE2FA583F87CB62582BC3F90BDFF40F62106B28FBC
        70B28F98DAF8C1C2EBF58EE9D7D91F3F2F57CE5FBEBDA22A36418457FBC8C706
        B2F71F4EF6065FED3AD240F67E00D9475E88EC8D90FDF0F1783CB794376BFF44
        6E20E766AF95AA492F1F83F8109F21B2EFE74E1B304220FB1003D9475E207BFF
        E8565BBE795CD4FD56626ED5457A538F11A0042FF2C140F6FEC3C9DEE8AB5D47
        1AC8DE0F20FBC80B64EF3FF4D5BEDEEC7AA8BEC3F0EB63455D2B56EFA9B93837
        5DAA9A2414DB637CB435125D81ECFDE77C692F95BDC957BB8E3490BD1F40F691
        17C83E70385DEC84A66ED32F73CF2936CDCF90F5F96A6F247A02D98F1C97DB73
        8B4A6B7B825C439993938A69196B9F6D3B9240F67E00D9475E20FBC0E370B177
        9C2B552E5C9255DE4ADAD7E2ABDD91C80F643F3206F4F6CFCA1A065FCA3CDAB4
        6F665A89CE579BFA13C8DE0F20FBC80B641F1CDCAC777C45B3E62FE9071B4E93
        3646F9DD280C643F3C3406FBA72B5B87FEB0F5644BF62B6BA4EA985545ACAFF6
        F43790BD1F40F69117C83EB810E1BFB46C4B85C257DB23911DC8FEE3D1191D74
        BCCB2F779C6EC99ABD56AA0AD692D390BD1F40F691174EF628AA13242A5BB52F
        2CDB5AD1EAABED91C80E64FFD12835F627F65EE858337BADA42F4610D89EFC8D
        21B2C712B72305B28FBC40F6C185C8FE4522FB365F6D8F447620FB0F63B63113
        DA5596EFA5EDAB3F3551303A85A9207B3F80EC232F907D70E164DFEEABED91C8
        0E64FF7F58ECEE3B7B066D4F1D2DEA5EB17083BC3D4620F2F86AB320C40BD9FB
        01641F7921B2476DFC204264FF27C83E3A03D9FFBFFF6777796E576AAC5FCD93
        F42E589459D6149B200AC874BA1104B2F707C83EF202D90717C83E7A13ADB277
        33DE71ED2ACBF74F497A17A7EDAF3FF9CA1A69DF4481C8EEAB8D462190BD3F40
        F69117C83EB870B2C768FC284C34CADE6865EEDF73BE3D85F4E0E98A757C2821
        0DD9FB03641F7981EC830B277B0CD08BC24493EC2D76F6EE8A66CD0B09BB6A2E
        C408447CAA2B01D9FB03641F7981EC830B641FBD8906D95B1DCC84C62EE3CF92
        F7D69D885955446BD97B6F6C871087CA3E8FDB5C305C20FBC80B641F5C20FBE8
        4D24CBDEE6704F68ED35FE28FD60C3C11841115D8A362805710210C8DE1F20FB
        C80B641F5C20FBE84D24CADEC578C775AA4C4F679D68D91E9B201A22FBC957C9
        5F0F64EF0F907DE405B20F2E44F628AA13A521B25771A7414460B0B81FDE9ED7
        BA313E514C25EFBE717F791AC8DE1F20FBC80B641F5C20FBE84DA4C89E653D63
        06F4F62FADDC597591EC57A8A6D0F91BC8DE1F20FBC80B91BD1EB20F1E907DF4
        26DC65EFF15E1D3B64767D76FFA50EE18C353225D927BEBFB2F715C8DE1F20FB
        C80B641F5C20FBE84D38CADEE162EE6CE931FEF8B8A87B897077EDD9A9292583
        31AB8A46A5867D9002D9FB03641F7981EC830B277B14D589C28493EC5D6ECFED
        17CBFAA72FC92A2F8F4B141988E06959DBA0AE46374A81ECFD01B28FBC40F6C1
        05B28FDE848BEC2D76F6DE9D67DA322609C5DA975716F26D9EFCCD06B2F707C8
        3EF202D907174EF6A88D1F85E1BBEC2D76F77DC535FDFF15E4545F8E178ACDBE
        F6210202D9FB03641F7981EC830B641FBD21B25772A701AFB039DC7795366AFE
        9AB4A7F6FC9424913E36411469BDF9F7C703D9FB4110644F174AA0759423F964
        E37520FBE0C2C9BEC357DB23911DBEC9DEE162EFAC52E8FFB0F640FDF157D24B
        357189A2701C5D3FD240F6FEC0C95EEEA341FD897D554EB5E45041E7AA57D648
        C3755A47D807B20F2E907DF4862FB26759EFD8DA76FD731B8F35EF9BBD56DA1F
        2F14874B419C4004B2F78700CBDE91BCB7E6EC80DEF9E98A16DDF30BD64B3B26
        0A4456F2DF3D37FC39248881EC830B27FB4E5F6D8F4476F8207B37E3BDF55C69
        DFBCF919B29EC9ABC5E15610271081ECFD21C0B27711D99F36981D13E86FF70C
        58BE75B8B033E1D5F5A56D44FA16F2BFD313339A9E404312C83EB840F6D19B50
        CA9E61D85B5B7A0C3FC9CE6BDD4AB683CE95F7B98D5110C8DE1F022C7B2F91FD
        1922FB7BB99FA74FA1E369C5A6F266ED8B7BCEB7AF59B1A3AA786A72B18E9CA8
        F4DBBEAFDF406E32907D7081ECA337A32D7BA3C5F5505D87E13747AE74091272
        6B8A66A59568E82BFB28163D0D64EF0F9CECCB7C34A85F21B2CF27B27F90FBF9
        0FE0F178C7E8CDAE07EA3B0CCF91A7D31D53534AE8E20B3E7F07F13F907D7081
        ECA33744F6A332F58EDC97BF76A8A043B07C7BB97C466AC9D04481086F44FF2F
        54F6A7B8A602C32508B23F4F64FF08F7F31F89CEE4FC745E49EF12F2A4AA267F
        0FBDFC0006B20F2E907DF46634645FD13AF4BC20A7BA98DC1B876213443EB723
        CA03D9FB43907AF69F287B8AD9CE4E286DD4BCB4FE70FDC1E929622AFD70AED7
        CC9B40F6C105B28FDE0453F61AA3EBB367A57DAF2DC992B740F21F1BC8DE1F38
        D997FB6850BF42647F8EC8FE61EEE73F11A78BBD453DE4F87C71EDC0BFD71E68
        38393D45A221BF8357563711C83EB870B2EFF2D5F648642718B2375898072E96
        A95E2102BB307BAD443311A2FFA440F6FE106AD95FC7E9F6DCA2D4DA1FBF52A5
        8E4FDDDF706E6A7231FD9E8F79FA7E04B20F2E907DF42690B2375A9CF71756F6
        4F4A3FD870F2D5F5A5FD1305577CFE9BC88702D9FB4380654F47E39F25B27F88
        FBF911637732B777AB2DDF3C5FDA3727615755515CA298CED3F7F56F211F11C8
        3EB840F6D19B40C8DE6277DF2BA9D7FC7BE3B1E6430BD64B7BE213C5E8D48C2C
        90BD3F0458F69E9B95FD756C4EF6B67695F9FBD979ADD9938462DACBA783F850
        82771881EC830B641FBDB959D977918ECCE6E3CDBB5FDF54D6192F84E4FD0C64
        EF0F01963D1B28D95F47A5B57EE594A477C9F2EDE5D2C9492546F26FD0417CA8
        C8F73181EC830B641FBD21B2F76B9EBDD3C5DE59D1ACF953CABBB5F993578BE9
        DA213E7F1F1956207B7FE0645FE1A341FD49C0654F71B93DE33506E7A395AD43
        CFEFBBD8912AD8555D442EBAC1188108BD7D1F81EC830B641FBD1989ECB546E7
        676A14FADF1EB8DCB95AB0ABA6684EBA6C60A200F7AB0004B2F78720C8DEAF01
        7AC3C5E3BD3A76C8E4FC0C91D98BBBF315EB5FDF54D616232872F8D896A80D64
        1F5C38D977FB6A7B24B24364DFC79D061F495B9FE9994385DDC2553B2B69B5D0
        A118CC2E0A7458C8DE0F022C7B26D8B27F3F5AA3E31159C3E0DF73CE29B266A4
        96D0297B786A2681EC830B641FBDF938D9EB4D8E874E887BDF14E6565F999622
        31457949DB6006B2F78720C89EBEC61F15D95358D63366D0E0FC1CB9C896CFCF
        28A5CBEA46FD533491BD81C8FE79AE89408081ECA3371FF51ABF77D0FACD9D67
        5A37934E075DF7C3E7DF450216C8DE1FC25DF6D73159997B0B2B5553338F361D
        9ABD56A222DB12B5257821FBE002D9476F88EC3F301A7F40EF7C2CBF54F96AC6
        E1A63C72DD997DFD1D24E081ECFD2152644F7133DE5BC8FE3C79A95C356BFD91
        E6E3AFAC91D257FBEC0DDB18F181EC830B641FBD21B2EFF7783CE33446D72397
        CA9533361E693C38275D4A4B7DFBFCF3485002D9FB4380657F7D347E48647F1D
        2AFD6EB5E51B67A5BD0BD3F6D7E7CF4895E8C8B645CDEB7D227B23641F3C20FB
        E8CDD4E4626351956AD29653ADDBE66794F691FF8669C0A31FC8DE1F8220FB51
        1BA0F749D0297B0AA5E9FB27C5DDCB92F7D65E8C178A2D3EB639E202D90717C8
        3E7A33512062176C907590FF1B920F5D207B7F8864D95FC7E9626F6DED35FF70
        CB89E69C7861112DCC43A7EA456C4F1FB20F2E44F62F10D9639E3D82842690BD
        3F0458F6B45C2EEF647F1D8B9DB9A7A65DFF9B03973B84B4C8C5DC74593F7952
        8F38E943F6C105B247909006B2F70722FBAF478BECAFE3F57AC7D89DCCDD4488
        7FDA95DFB965D9960A4524499FC8DE04D9070FC81E41421AC8DE1F38D957FA68
        507F42659FCF77D9BF1F9B93BD5B5AAFFEF796932D7B16659676C608C2FF5B1C
        641F5C38D977FA6A7B0441821EC8DE1F022C7BBAC46D58C9FE3A669BEB3E71ED
        C0C484DC9A62227CBAD88EAFFD0B8B70B27F81DB351060207B040969207B7F80
        EC3F4873B7F1A719871A8EBFBABE5449A41F9673F421FBE002D923484803D9FB
        4390641FD055EF461BBDC5FDE079B9725EDABEDAB3B3D74AD474BA8D8F7DE56D
        88ECCD788D1F3C38D9D3E9573EDB1F4190A006B2F707C8FEA3A135F7F3243DAF
        27EFADBB342BAD5843A41F160BED10D95B20FBE001D923484803D9FB0327FB2A
        1F0DEA4F224AF6D751E91C5F3E56D4B52231B7A678565A89EEBFABAEF07A101F
        EDD997366A5EE2361F0418C81E41421AC8DE1F20FBE1D3ADB67CFB504167E2E2
        4D652D2FAF2CA483F878D9D38F178AED7B2FB4A775F59BBFED74B1B7719B0F02
        0427FB765F6D8F2048D003D9FB03643F72BA88F40F17740A12726B2ECF5E5BD2
        4FE4EAE0DBB296130522E7C20DF2F68C43F507F24BFB666AF48ECF709B0F6E12
        C81E41421AC8DE1F207BFF6159CFF8BA76DD6FB24FB76D5FBAB9BC754A5231EF
        A44F420717DADFDC5625A3DFF107F4F6CF729B0FFC04B247909006B2F7074EF6
        D53E1AD49F4495ECAFE366BCB755B5E99ECF3ADEBC7F516659CFE4D5FCEBE9D3
        90EDD26C3EDEBCA3BA55FB3BAD31BA8E512081EC1124A481ECFD01B20F1C2EC6
        7B7B79CBD05F361E6D3E3A6B4DC9908FF6E143D8C9AB459AECBCD6ACC62EE3CF
        864CCE07B8CD07C38493BDC247DB220812FC40F6FE00D9071E9BC37DEFA182CE
        E479EB64FDF18962978F76E24398996925AADC738AF4E66EFD8F8D16E73DDCE6
        834F00B247909006B2F707C83E38B8DCEC1D57AA545392F7D65D98BD56AA894B
        14F175BA1E43B6AF6FFFA58E44F590FD73DCE6838F01B247909006B2F7074EF6
        353E1AD4AF40F61FC464633F75A95C392331B7A688CED18F4DE06D353EDB9AFD
        7587E9743D9B13D3F53E0E4EF66D3EDA104190E007B2F70722FB6F40F6C14767
        743C7A56DABBF09D1D55B2E929123D913E1F7BFAECA24C79539EA47751EFA0F5
        49878B1DCF6D3E781F903D82843490BD3F40F6A38BD6E4FEF44971F7B2B7B655
        564C12169B7DB5210FE27C7D5369ED3959EF2BFD3AFB175C8C770CB7F98000D9
        23484803D9FB03641F1A060DAE2FAE3FDCB87F4A52B1E1E595850ED2767CACC6
        675FBEBD525A50D93F4963743DEA727B6EE5363FAAE164DFEAA3BD1004097E20
        7B7F80EC4387D3CDDE5ED73EF4DCA182CE556F6DAFAC20EDC7CBF2BB31AB8A6C
        73D7493B320E35EEAB6A1BFA9DC3C5DCCEED425402D923484803D9FB0364CF0F
        F24B957348FBD11EBECF76E5499889029121694FF5C9AA36FDEF2D76F7046EF3
        A30A4EF62D3EDA074190E087CAFE24773982E11224D93FC8FD3C182644F6B348
        FBD1C5757CB62BCFC2C40BC5DAB507EA0FD477187EE188B2C576207B04096920
        7B7F0882ECCF43F62387C87E26693FBEF7EC6F0C917E9176E3D1E69CE66EE3B3
        6EC63B8EDB9D8886C8FE79C81E414216C8DE1F207B7EC0F5ECC34DF6D7E39E96
        2CE9CF3ADEBC5DA1347D8FDBA58885937DB38F76401024F881ECFD01B2E70761
        2EFB6B8921D27F25BDB4AFA0B23FDEEB8DDCE97A9CEC9B7CB5018220410F64EF
        0F903D3FE0646FBFB13DC32D74B53FD2CB1FD89DAF48536AED5FE1762FA280EC
        1124A461207B3F80ECF941A4C89E860A3F3641E4989F2155ECBFD421ECD7591F
        E7763322E064DFE86BDF1104097A207B7F20B20FE4423890BD9F4492ECAF2746
        5074353E516C5B9459567FB4A87B99D6E4FE0CB7BB610D648F20210D64EF0F90
        3D3FE0646FBBB13D232357684FDF367BADB4336D5FFD8933D2BE051A83336C57
        D8E364DFE07B5F11040972207B7F80ECF94164CBFE83A1E25F9429AFA76578AD
        0EE62EAE09C2064EF6F5BEF60DE157620445AE890291DBD7FF86846D207B7FE0
        645FE5A341FD0A64EF1FD1247B1A7203BE3A33AD444BCEBD4251B53AC6E162EF
        E49A82F740F6E19178A1D8B6ED544B4EFAC186D3E4FF0F978255C82707B2F707
        C89E1F449BECAF272E51E49DBD563A90B4A7FA9CB45EFD0F97DBC3FBBAFB9CEC
        EB7CED0F12FA9073CABA2AA75A5658D93F55A5B33FBE2DAF2D87FC77C83E7202
        D9FB0364CF0FA255F63474F47E7CA2989DB74EA65A7BA0FE44458BF645866179
        BBC21E64CFCFC42688ECCBB795575C2C53D1A5999F70BA3D632D76E68EAD275B
        7691FFDD75E39F47C23690BD3F70B2AFF4D1A07E05B2F78F6896FDF55C93BE50
        EC59B0A1B47BE3B1E63DEA21FB97B8E6E115903DBF12232872BEB9B5A2E6ACB4
        6F51CF80E51B7627F3BFB51A88ECEF24B2A73D7BC83E7202D9FB4310648F8570
        FC00B2FFBF50E94F5E2DB60A77D75C6CEA36FD8C6FD5F8207B7E849C27EEA59B
        CB9B4E49FADEE8545B9FB63A980F8DFB20B29FC0F5EC3D37FE7D246C03D9FB03
        64CF0F20FB0F273E51EC5E9255DE9C7DBA35AB5D65F92ED7542107B20F79BC24
        B68CC38D475A7B8DCF5A1D9E3BB843F32188ECEF22B2DF7DC3DF47C23B90BD3F
        40F6FC00B2F71DDACB9F9A5C6C5DB6A5A26677BE22BD67D0F60DAEC94206641F
        B2D02974F6F919D28EFD97BB92FA06AD4F7187E42321B2BF9BC87ECF0DBF8384
        77207B7FE0645FE1A341FD0A64EF1F9CEC23AA825E2043A53F492876CC4D97F5
        26EDAD3D9327E95DD8D465F8899BF18CFA403EC87E54C34E4D2931BDB5ADB232
        FB74CB566983E61FFD5ADBE30E173BAC591B907D4406B2F707C89E1F40F6238A
        87C8DF3933AD78607B5E6B96DEE27E886BC65181933DE6D9073933524BB49B8F
        37EF9135A8FF3168707E8161AF8EE30EC1B0E15EE343F69115C8DE1F022C7B2F
        64EF1F90BD7F99962C3124EDA93D47DA6F8EC1E27E806BCEA04264FF02913DCA
        E5062FEE37B7955749EA06FEA31E727CCECDF83F40133DFB880C64EF0F44F64F
        41F6A107B2F73F318222EFABEB4B5569FB6ACF1454F44F3559DDF771CD1A1438
        D963219C20646A8AD4B4F640435E5DBBFE3997DB33E29EFC8D40F61119C8DE1F
        8220FB7390FDC881EC6F3EB404EFC28DF2DEF5871B0F8BAAD52F9B6DAEBBB9E6
        0D289CECB19E7DE0C2C6278A2DE4DE71F952B97256B7DAF27586F50464BA2527
        7B8CC68FAC40F6FEC0C9BEDC4783FA13C8DE4F20FBC0253641C42ECA2C55641D
        6FCE2DA953FFC3C578C673CD1C1038D937FBFAB79191852E8A24DC5D5348CEFF
        57154AD3F79DEEC01E2BC83E2203D9FB0364CF0F38D93B6E684FE426325170C5
        BD244BDE78BAA4F7359B939DC035F54D4364FF22647F73797965A1E39D1D15A5
        79929EA52D3D861F395CC1298F0CD94764207B7F80ECF901641FBCCCCF28EDCF
        3DDFBEA1A65DFF1BBB93B9E9857638D9B7F8FAB7904F0C33375DD673ACA8EBED
        C62EE3CFC8435850573B84EC233290BD3F40F6FC00B20F6EE285458EE5DBCBAB
        DEBDD09E5ADFA1FF15E949FADDD3E75EE343F6C30F4B42579DB32DDD5C5E5350
        A99E6AB1BBEFE29A33A840F61119C8DE1F022C7B0F27FB519DF71C0940F6A393
        B844918BF4F47B93F7D69E3D21EE79A3BED3F08B21B37344E72B643FDC5C71CD
        482DD1ADD85125CB39DB9659D6ACFB73BFD6F615D6337A6B1D40F61119C8DE1F
        207B7E00D98724CCAC35256A72CEE69535695F18EE542F0CD0FBF8BCBCB2903C
        50C9FAB24FB7ED94D40EFC5767727E966BBA5107B28FC840F6FE40644F8BEA40
        F62106B20F69D8E5DB2BCBC93198DB3B68FD1AC35EFDD89E27BED97F74A62489
        0D9B4FB6BE5B54AD9E3CA0B77F816BB29001D94764207B7F80ECF901641FDAD0
        DAFBF3D6C9D41B8F351FBE54AE9CAED2DABEC030AC4FE913D9FF89C8BECDD7EF
        446B486F9E79658D74F0C0E5AE5495CEF104D7542107B28FC840F6FE00D9F303
        C89E1FA1D5F8E6AD2B516D3EDEBC5754AD8E550FD9BFC81DA2FFC1C95EE1EBEF
        475B687BCD4C2B195A7BA0E1EC5969EF6B068B8B57D73E641F9181ECFD2108B2
        3F0BD98F1CC89E5FA1D5F8166C90F56ECF6BCDA135DAB526F7C3DCA1BAFE1A3F
        AA7BF6F44DC88C54BA2E418D284FD2BBACB3DFF21D8FC73B966B22DE00D94764
        207B7F08B0EC59C8DE3F207B7E263641E459BCA9AC2B37BF7DA34269FE1E3D56
        44F674D5BBA8943D95FCB4148925615795EC685177426BAFF1C70CC306B4EA5D
        2081EC233254F6A7B8430C860B64CF0F207B7E272E51644F3FD8708288FE8582
        CAFE69CBB6449FECE38562C7AA9CEACA0397DAD736760EFDD2E5F6DCC69DBEBC
        05B28FC8B090BD1F40F6FC00B2E77F482FDFFDC696B2E6E4BDB54573D3653A5F
        7F2642E39E92543C9471B8E1687D87E1374EB7E7A6AB108E16907D4406B2F707
        C89E1F40F6089F428B0FBDB246AA7B3BBBB27CCFF9F60D652D437F31595DF773
        A76BD800D94764207B7F08B0EC19C8DE3F88EC6792F683EC9190265E28661665
        96F56D39D5FAAEA46E20C668713EC29DA26109641F9181ECFD01B2E707903D12
        CAC42688AE2EDC2057671E6B3E2EAA564F1B3285B7E4AF03D94764207B7F80EC
        F901648F842A718922F78A1D55F597CA95F3B446C7E7B853322280EC233290BD
        3F0449F6FF9B930C8607648F8C76486F9E9DBD56A22137CE4BD50ADDF35EEFE8
        2D50335A10D9DF05D9475C207B7F80ECF901648F8C5668C1A0D96BA5BAD5BB6B
        C5B4204EBFCEFE15EE348C3820FB880C64EF0F9CECCB7C34A83F81ECFD04B247
        829D1841D1D5596925865539D515C7C53DEFF40E5ABFE1F15EE55DD5BB40C2C9
        3ED7577B20611BC8DE1F82207B5A1B1FB21F219CECED37B42782DC74DE2B6D5B
        625ABEADBCFA60416772B7DAF2B4C7E3BD853BF5221AC83E2203D9FB03277BB9
        8F06F52790BD9F40F64810C212D1BB5E5D5FDABDF742FBBA7695F9FB7C2E6D1B
        0C20FB880C64EF0F903D3F80EC91402536A1C8497AF2DA77765449DEBDD0BEA6
        5D697EC6E3B91A153DF91B81EC233254F679DC2106C385937DA98F06F52790BD
        9F40F6C8CD669250EC989F51AACC3AD1B2972B6B7B27777A452D44F674EA1D64
        1F5981ECFD01B2E707903DE26FE213C5EE39E95275C6E1A66374F95DBB93B98B
        3BADA21EC83E2203D9FB03277B998F06F527D7658FA23A2304B247FCC9D46489
        31757FC35959C3E03FEC2ECF04EE74021C907D4406B2F70722FBA7022C7B54D0
        F303C81E1949621344CC8CD412CD9EF3ED6B6D4EF61EEE34023700D94764207B
        7FE0642FF5D1A0FE84AE7A7786C8FE41EEE7C13081EC91E164A240E49D962231
        BC9D5D29BD5CDE37CDCDF07F4DF95002D94764207B7F80ECF901648F7C5C6841
        9C2949C5A6A59BCBABCFCB95B32D76F7A7B853077C0C907D4486CAFE347788C1
        700992EC1FE07E1E0C13C81EF1155A1067F26AB179C17A694B9EA477B1C5CEDE
        C79D32601840F61119C8DE1F022C7B0F91FD69227BF43A460891FD0CD27E903D
        723D9ED80491754EBAB4EBE895AEB74D5637DE96F901641F91F140F67E1060D9
        7BDFEBD93BEFE77E1E0C13C81EA18911889CF1C262DD8A1D55E282CAFE290E8C
        B0BF2920FB880C95FD19EE1083E11260D95F9DB946369027E97DD5E9666FE5FE
        09300C20FBE80E2D6B3B39B9C4B02DAF6DDBA0C1F925EEB4003709641F9181EC
        FD21D0B2A721372EEBAEB36DE92EC67B3BF7CF804F00B28FDEC4258A2CEB0FD7
        1FE81DB47E3312D7940F25907D4406B2F70722FB27032D7B9A8902912D694F4D
        5E6BAFF9870C1BD9CB680602C83E3A139F28766C3AD6B43F5A56A11B6D20FB88
        8C97C8FE2C7788C1700996EC6988F0992949C5DAECBCD60D2AAD0DAF263F06C8
        3E3A4365BFE564CB6EEE34000106B28FC840F6FE104CD95F4F5CA2C8363F43D6
        76ACA86BA9DEEC42DD7C1F40F6D119227B3B917D2E771A800003D94764207B7F
        180DD9D3D0F9C2938462E35BDB2A65E2DAC17F5B1DCCDDDC260002641F9DE17A
        F6907D9080EC233290BD3FD0017A89B935C53E1A34289928105D9D9A5CAC4BDB
        577F4CA5B53DC16D46D403D9476788EC5D788D1F3C20FB888C67CDFEBA53DC21
        06C3A55F677F22FD60E369D280EC0D0D1AD44C145CB10B76559FEF54999EC6E0
        24C83E5A4364EF84EC830791FD5D907DC4C59D71A8E1307788C17031DBDCF79E
        10F72C230DE8B8A141839ED80431332355A24EDE5B7BE67449EF6B9D2AF3F79C
        6ECF1DDCA64515907D7486933D5EE30709C83E22E33870B973357788C14868ED
        35FC787E4669978F461DB5C426886CB3D69428D71EA83F52D93AF40722FDA89A
        A30FD94767F0CD3EB840F691972949C586B226CD9FB9430C4682D1CADCB7FF72
        57126948D78D0D3BDA992414DBE7AD2BE9CC3EA3D8DCDA6BFC21B789110F641F
        9D21B2A7A3F17771A7010830907DE465E9E6F2A67E1DC67BF98DA2CFF8CC1B5B
        CAEA48637A6F6CDCD10E1DB93F35B9D8B86C4B45A5AC61F0256E13231AC83E3A
        43646F83EC8307641F59999E22D6EDBDD0BE96C5382FFF7131DE5B8858FF3935
        59A2238D1A72E1D34C1488D8D737C91B2EC8FB66E8CDAE885E3617B28FCE70B2
        CFE14E03106020FBC849BCB0C89479ACF9C0A0C1F505EEF0027FB139D93BB79D
        6AC9260D4BA5C30FE12788AECE5E2B51AE3FD274B8B451F357BB93B993DBDC88
        02B28FCE40F6C105B28F8878A6248986B28E37BFDB3B687B8A3BB4E06651696D
        5FD978B4E960BC506C248D3CAAD3F13E2E93578B1D8B32CBBAB2F35AB31BBB0C
        BF6018761CB7C91101641F9D81EC830B641FDE99281039966495B7E6497A970C
        E89DE8D1071A5AC75E54AD8E15E4548AE80A76A4D1DD249EF71F8450847ECB27
        4F7896B7B695571D2AE84CE819B07C3D52560983ECA333907D7081ECC332B493
        E998B74ED69B7B4E91D5D865FCB9D9C6DCC51D5210689C6ECFF8CE7ECB77C813
        D56B8B379535D0C627A1A3F5432E7D5A856FD69A125DD29EDACBE7A43DF3F416
        77D8D7DBE7646FBB715F91C80E641F5C20FBB08B6BD2EAE2A18DC79AF7C99B34
        7FD51A1D8F728712041B3A704F63707E4ED6A8F9FBB653AD3B966E2EAF23B2A5
        520AF9377DFA3D7F5A8AC4F0E6B6CAB233D2BEF9837AC7E73C1E6F582EA70BD9
        476720FBE0C2C97E97AFB64778130F7D5D3F7BAD5495B4A7265F5237F01FB39D
        B9973B842014B0AC673CADA59F7B4EB1314650642607892FDFF4BDF142B17945
        7685EC70615742B7DAF22D6E93C306C83E3A03D90717C89EF771CFCF9076EFCE
        57ACAF510CFDD664633FC51D3AC007484FFFD15392BE654BB2CA9B89F4A9A078
        217DEEF5FE50F2DE9A8B449E737426E723DC26F31EC83E3A03D90717C89EB771
        937BF560E6D1A6E3D2FAC1FF688CAE4F73870CF00D8BDD7D2F7912FB1D7922CB
        98932E5512E9D36FFABC98AE179B2062E7AD2BE9CB3AD1F22E3991FE6173B2BC
        AFB70FD9476720FBE002D9F32FB4DC6DD29EDA820BF2BEB9BD83D66FB8DC9E5B
        B9C305F88CC660FF2C15EAC6A34D87A62489B431EF8D280FF9203E9A78A1D8BD
        244BDE9A9BDFBEA1BEC3F0736E937909641F9D81EC830B64CFABB073D7C9FA8E
        8B7BDE6EE931FCD8EEF2DCC61D26104EF40EDABE4E9ED466AFDE5D5BC8CDD1E7
        85F0DF9BAE576C13E4541797D40FFE9DDB5CDEC1C99E4E73F4B91F486406B20F
        2E903D6FE25CB05EDA9E5FDA37DF6871467435D4A8C0E5F68C6FE9313D7BB4A8
        FB1DF204D74B0E3073C3010F59620445ECB2ADE535E74BFB5ED11A1DBCFB3E44
        643F9D6C27641F6581EC830B641FF230AFAC91D26FF347C4B50313AD0E760277
        684024607578EECA97ABE6CD5B57D2139B20A202E3472F5F20BA3A3FA354B5F5
        54EBAE923AF53FF9F42D1FB28FCE40F6C105B20F5DA624151B93F7D6169E93F5
        2EEC19B07CD3CD78C7738705441244A4138AAA547174E182596925FDA4674DBF
        E5F3A2A74F1E40982559F296BD173AD6D477187EC17AAE867C6E3E641F9D81EC
        830B643FEA61C9FDD5FEF6F6CADA2345DD092D3DA61F3B5CECEDDCE100914CCF
        A0EDEB67A57DAF0A73AB2FD35198E464A055F87831552F5E28B6AFDA59293925
        E97DBDABDFFC9D5096DE85ECA33344F6743D7BC83E4840F6A31AE7A24C79EBCE
        336D5B2A5AB47FB2D8D97BB8C300A205D6E31DA331381EAB6AD3FF71CF79C5FA
        953BAB65D3538AB5310211ADB9EFEBA419B5D0017C718922DBEB9BCA1A338F36
        E6D6771A7EE166AF8EFA5AC9907D7406B20F2E9CEC737CB53D1298C4917378E1
        0679E7BE8B1D6B3AFB2DDFB33BF15D1E70684DEEC70A2B5593DFD9512DE7E6E7
        F3E29B3E09FBCA1AA96AFFA50E617D87FE174E173B6ADF9888EC311A3F0A03D9
        0717C83E78894D1039DFDE5E5977B0A02BB9A5C7F45396C53779E0038FF7EA98
        B266DD9F84B9D557A6A54886C8C9435FEFF3A5288F75D5CE4A519EA4E7B54E95
        E99B2CEB09FA377DC83E3A03D90717C83E28611665CABBB3F35A7796356B5EB2
        3959AC48073E1ED6E31DDBD865F8E9BB1714696F6DABAC999A22A5F5F67D9D5C
        21091D63907EA0EE446165FFE44183F3316EB38302641F9DE164BF8B3B0D4080
        799FEC79D19108F7CCC990EBD61F693A5950A19C618880D546C12863B1BBEF29
        6BD2FC39FB8C2267C1065937FD864E4E2CBE5C9CDE39E9D2BE9D671559641B5F
        3459DD411974C2C91E15F4A22C44F60EC83E7840F681495CA2D825DC5D537A52
        DCBDA25F677FC2E309DD60661001688D8ECF1654F64FCD38DC746A6EBA4CFDF2
        CAC2900FE07B5FDC7479DFC3855DF464FF3CB7C90103B28FCE40F6C18593FD4E
        D2D690BD9F894D1039D61D6C38D5D263F839CB7AC6714D0BC0CDD3A7B13F794A
        D2F766626E8D64D26A317DB5CF97017CF4C43765E7B56476F59B9F72B93D011B
        B54F643F93FC3E641F6581EC83CBFB64CF9B7B48188559B05EDABDE554EBAE9E
        81F05B361C84096EC67B6B4B8FE967EB0E359CA49598C889C797B5F3E92B2D6B
        C6A18643E2DA817F0FE8ED8F320C7BD3AFB438D9D3C2433EFF4D243203D90717
        C87EE489796F5692266D5FFD99828AFE6936A71753E940F0E9264F94B4BCED82
        0DB2F689822BB497EF24E1C32B3976F65AA972C7E996CC8A66CD1F8D56E65E6E
        93FD02B28FCE40F6C18593FD0ED2D6BCE92CF0341E2279071D942CC8A92E3921
        EE7D4B637405755032001FC2EE64EFEAE8B77CF77C59FF2BE9071B8ECECB90F5
        4E5A2DE68B18DD33D324FDC2DD35F9C78ABADF68E931FDC86875DFC76DFAB081
        ECA333907D7081EC3F3E74BD9019A925FA655B2BAAF79C6F5F57D536F4FC9029
        B8338F0018160C7B75BC4269FED1EE7CC5A6A59BCB3BE285BCFACEED244FC6BA
        B507EAF637F7989EE136795840F6D119C83EB8BC4FF6BC5981932F992810316F
        6EAB6C2CA8E89F6173A2B42DE0297627737765ABF64F59275AF62FD850DA3B31
        41C4A76F724E414ED5C51A85EE37462B33AC8B08B28FCE40F6C185937D36696B
        C89E4B8C80D6101199DED951555AD9A67F9E6B2A00F88DD1E27CB0B0523565D9
        D68ABA975716D22A7C3E4FF010845D9459D674F44AD79BADBD861F9027E75BB9
        4DF60991FD2CF277301A3FCAC2C91E15F4820464FF7FA1EB804C128A9D4B3797
        B7D3C56A144AD30FB86602207CA86C1BFAE3DBDB2BCBA7A748E8CA7A3E4FF650
        842EF7489EA08B2F96A9A628B5F62FBA18DFC52888EC5F217F1E15F4A22C907D
        70799FECF954B363D413972862E7AD2B51AD3FDC78B48AF4E659D6833AF6207C
        E952DBBE9D7BBE7DC3A24C79C7E4D5625E89335E583494B6AFF688AC61F0A521
        B3EB216E93FF0791FD6CF2E720FB280B641F5C88ECEF8E66D993CE8667D69A12
        BD70778DE84A957AAADDE5C1343A1019909EF36DB5EDFADF6E3CDA7C68DE3A59
        3F39D9F9F4FA8E25179E727B5E6B5643A7F1E7CEF715E5E17AF6788D1F6581EC
        834B34CB3E5E28B2BFBDBDB2EA84B8F76DADD111F0AA9F00F002AB83B9A7A84A
        15B76A67A564565A892646C09F55F5489C0B37C81B4AEA07FF6673B8AFBD4EE3
        BED963805E9405B20F2ED1287B5A146792506C49DE5B7BA1536DFD1ED7140044
        367AB3EB3327C4DDCB966E2EAF8E178A4DE462E0CB7C5B666EBAACE362996A12
        ADC29727E95D40FE1B7AF65116C83EB8BC4FF6D130408F894B145966AF95F4AE
        3FD27CB05F67FF1AD70C00440F663B73FF05B96AD692CDE555935717EB270A44
        7C18B9CFC626882C0B379436BDB651DE48FEFFA81E44148D81EC830B27FB889D
        671F2328F2C69173684EBAAC77CD81FA63E7E5CA39DD6ACBB718D6F3B1B37F00
        88789C2E7682A46EE05F6F6D2B974D4B96E863F8F16A9F96007690A0CA579405
        B20F2E912A7B3A8D2E5E2876CCCF9076E59C6DDBD83B68FB26B7CB0080F76373
        B277E7497A16918BA57D92F0DACA7A102D32EA81EC83CBFB641F51D7F7D4A462
        C3FA234D871ABB4CBFF078BC015B7D138088C4E3F18CD1999C8F9227E38C19A9
        92413A279E5C4858F71A19B540F6C125D2643F5120F2C40B8BF47BCFB7A739DD
        9E3BB9DD04000C970EA5F10769FB1B8EC7258A68511EF4F2915109641F5C2245
        F6F4732339576C4BB2CAEBCEC8940B1C2ECF1DDC2E0200468AD7EB1D9B7BAE6D
        6DBC506C241757D497D744821F7203B743F6C12312641F9B2072CE4997F6ECCE
        57AC55696D18610F4020F078AF8E3D53D2FDEAECB5D24E7291E15B3E12D440F6
        C1259C07E8C5088ADC53928A87D61F6E3CD0DA6BFE11BECD031004CC76CFA772
        CF29D6D2256B492F9F8E94F7794122C8CD04B20F2EE1287B3AD27EA240645E95
        537DA5BC65E8455A1594DB1D004030F07ABD63BAD5E66FA7EDAF3F4A6ECA1672
        21D2017C54FC7C5A4E1709E340F6C125CC644FB7D13933ADA4EF6259FF0C93D5
        7D3FB71B0080D1C07BF5EA1895CEFED5F372E5CCB47DB547E385223DB928217C
        E4A603D9071722FBEBABDEF1F9739C273641645ABAB9BC62F7F9F635BD83D66F
        709B0F0008150CC3DE2A6F1EFAF32BE9A5FDE422452F1FB9A940F6C185EBD953
        D9F3F23A8D59556413E454155629F47FA0B53F3CDEAB3E97C00600840086F58C
        51284D3FD878AC79CF94A4622DB96851E616F12B907D70A96E1BFA03AD32E7AB
        ED431CD7A2CCD2A6824AF514BDC5FD304BEE29DC260300F886CDE19E50D6A4F9
        9320A7FA329DAE472E608CDC474614C83E38D85D9E3B25B503FF7E6DA3BCF5E5
        95853EDB3E446167A416F71D2AE814F4EBEC8FBB19EF586E9301007CC760713F
        7041DE3763716669C344810823F7916107B20F2C0CC38EAFEFD0FF6AFDE1C643
        B3D24A06C9F5E8B3DD4314E7DBD9551285D2FC43F230723BB7C9008070C2C578
        C7A8748EC70F5C6A4F9ABB4ED613B3EADAFAF9BE2E7804F95F20FBC0D133687B
        2A37BF3D637E86B4375E28F6D2296CBEDA3C04F1906DB124EDA93DD73D60C582
        35004402560773574B8FE967EFECAC96908B9CAE66E7EBE247906B81EC6F1EAD
        D1F1D913E29EA5CBB656D44C492AB6C7087CB77588E258BAB9BCE64A953A4E63
        743DC66D32002052E8E8B73C9DBCB7EE6CBCB068885CF054FAF89E8F7C2890BD
        FFB819CFAD45D5EA18414ED59519A912436C02BF5ED9CF4A2BEE397AA56B799F
        C6F635A7DB338EDB6C0040A43168703E7A41DE37FDAD6D9532AEF42E953EA6EA
        21FF0B64EF1FACE7EAB893C5BD4B67AF95A8E212457CBAA69C9384627DF6E9B6
        4DADBDC61F591D58990E80A8C160713F58D536F4873DF9EDEB88F82B27AF16D3
        95F5F04D1F81EC4780D5C1DEDDD8A5FFD9B1A2AE3757EEAC124D4D2E36F1E8BB
        BCE3AD6D15E5F9A5CA794AD293B7BB580CC003205A21BD915B7426E763D27AF5
        BFD30F369E887BAF04AFAF1B07122581EC3F199BD33BA1A0A27FB230B7FAD2AC
        352583B109223E3D28BB67AF95F61D17F7BCA933B91EE536190000AE8DDC1FAB
        D2399EC8CE6BD9CABDDEC772BA511AC8FEA361D8ABE32A5B877EBFFE70FDFE79
        EB4A94718922AFAF360C5118BA4856765EEBD6E66EE34F4D56F73DDC660300C0
        0719D03B3E7FB8B073E5EB9BCA9A6330723F2A03D9FB4667743C9A73B66DE3D2
        CDE5F593578B9D3C7A5D4FD797B7A4EDAFCFAB68D5BFA035B91FE1361900003E
        1AA395B9AFA65DFFDB9D6715DBE6A44B55E46682D2BB5114C8FEC3A887EC5FCE
        3ADEB8675A8AC4CCA7697444F2CEE5DB2B6B2F57A867F6696C5FE13617000086
        CFA0C1F97949EDC07FD71F693A3A35B998AEAAC7A75796489002D9FF1F5687E7
        EE8B65BDB392F6D4144C49125B7DB557A83223B54473F44A9740D167FC91DDC9
        DCC16D3200008C1C17E3BDA577D0F6E479B9728E6057B5242EF1DAF77C9F371F
        2432C2C97E27770A44250C7B757C59B3E6CF59275ADE5DB841AE223D689F6D15
        8AD0CF6BB3D24AFACFC9FA16D0B770DC260300C0CD63753077B6F4989E3D5AD4
        F3CE2B6BAEBDDAC7DCFC084D34CBDEEBF58E69ED353E9B73B675EBB2AD154DF1
        42319F3E61B153928AF5E9071B4E48EA06FE6D75B013B8CD060080C04217D839
        57AA7C95160E21371FBAC00E9D7284D7FB119468957DBFCEFA9563455D6F0B72
        2A6553938B1D3C198047AF2DF724A1D8B8726755717EA9726EB7DAF27586C51A
        F3008020E372B3B77528CDDFCF2FED9BB3667FED89F919B2DE8909228CDC8F90
        10D9DBA245F61ECFD5B12AADEDABC7C53D6F2DCA94B7F2A98EFDD46489F1EDEC
        CA8A5DE7DA322B5A742FEA8CCE471916CBCF02004280DDE599D0D865FCC5A182
        4EE1922C792BE90DA10A5F98271A64CFB2ECB8961EC3B3BBF3DBD2976D29AB9A
        28E055511C66F9F6CA3A51B57A927AC8F125EF55F4E201003CC16871DE5FD6A4
        F90B9DAE477AFA4A72C3C2ABFD304DA4CB9EF4E4BF7642DCFD967077CD95F8C4
        229BAF360855A624151BE9405879B3EE2586BD8A5E3C00809FD085760A2BFBA7
        BDB9ADA29EDCBC303F3F0CC3C97E0777482306BA1E444165FFD4F40375A767A5
        95D0A9A43EF73F14A1E3249664C9DB0E16742537741A7ECEB0DE5BB8CD060000
        7E427A24B7C81B357F17E45495C4258A78353719F9E410F1582349F66E22CEB2
        66CD8BDBF35A772EDC28EFF3B5CFA1CA4481C8336F5D491FD9B65DF226CD5FF5
        66D743DC66030000FF613DDE31F51DFA5FEDBFD491BA6C4B054AEF86512249F6
        6EC63BBEA87A206EF9F64AFA5D9EF5B5BFA1CAAC35254319879BCE1654F64F57
        0F39BEC86D320000841F5687674269A3E66FD9A7DBB2E767C868AF0AAFF6799E
        48903DC3B0E37B07AD4F9D96F4BCBE2853DEE96B3F439578A1D821C8A92C3B29
        EE5EDEA9B63E4DDF84719B0D0000E18DD6E8F8CCA572E52CD29339352D4542BF
        97F2AA9785FC5F38D96773872EEC1832BB1E29AA56C76F3CD2788CF49EF9F46D
        DE4B446FCD38D478BABE43FF9CDDE5C1FAF20080C8C3E3BD3AA65B6DF9D62949
        EF1BA46723A22B75911B202AF1F12C44F69670943DAD635FD1A2FD53CED9B6AD
        0BD64B7B7DED5B88E29D281039166C28EDDC7AB2659742697E86DB640000885C
        3C1EEFD841BDE3F3E764CA856F655757D09E7E8C00F3F3F9927092BDC3C5DEDE
        DC637CF6A4B8E7F5D57B6ACECF4D97A96304FCA8634F7AF1CEF9EB4B956BF6D7
        E59D93A95E2392FF81D5C1DECD6D3A0000440F7A8BFB33E74BFBE6A51F6CCC9B
        9E22D6C5A0A71FF28483ECC903E318ADC9FDD92357BA56CE4A2B1920DBCD9B87
        455A4F7FD9968AF6BD173AD6D777E87F637732103C000050E8CA7A47AF742508
        72AACBE2854516227D14E50951F82E7B93D57D6F65ABF6F9DC738A4D33524B74
        BEF6215499995A62D87CBCF988B45EFD1FB29D58910E00006E84F4D66EA13DA1
        DCFCF64DB4C0C8E4A4124CD70B4188ECCD7C94BDCDE1BEB3A5C7F0E3E3E29EB7
        DFDA56D9305120627C6D7FA8322BAD58B3275FB15E63747D9EDB640000001F85
        C3C54E90D60FFE3BF368D3A1E929C5BCEAB94543F8267B97DB33AE77D0FAE4C5
        32D5ACA43DB55726AF16F3AA50D394249175E5CEAADAA345DDAB0C288A030000
        23C360611EA62B7ECD4A2BD1D24A63BE6EB448E0C3C97E3B7718428AD6E47E84
        AEDD4E1FFC66A6F1EB957DBC50EC5A92256FCFCD6FCF6AEC1CFA958BF1DECA6D
        360000809160B2319F3A5CD8255C915D513135B9D8846FF9C10F5F64DF3360FB
        FAEE7CC57A5A4ED6D776862AE4C1D33B3F43AADA78A4F1A8AC61F01F16BBFB1E
        6E93010000F80BFD964F97D1DD7956B165E9E6F296494231AF562A8BB4845AF6
        7697E756459FF10799C79AF611B1F26ADCC6CCB462AD30B7FACAC5B2FED97A8B
        FB116E93010000040A878BBDB3B451FBB78D479B0FCECF28ED8B4B1451E9A312
        5F80132AD93B5DECED4A8DF56B97CA95D312736BAEC426F04AF4CC820DF2AE23
        45DD2BFB879C5FA253FFB8CD060000100CE800BE76A5F9876765CAD752DEAD3B
        4BA75FE1F57EE0329AB277BA3DB70EEA1D5F903668FEB13DAF75DBD22D150D93
        568BCDBEB66BB4438BF34C4B9118976E2EAFCD39D39AA9E833FFC8E3C11AF300
        0030EA901ED6B80B72D5FCC59BCADA27AF16D3F2BB3E6FDCC8F0331AB277339E
        B15DFDE66F9E10F72C5992555E3B51706D2964DE0CC29C955632B42AA75A7A41
        DE37C76267EFF37AD19307008090C212E1CB9B347F5B7FA4E9D8DC75B281B844
        7E2D651A6E09B6EC5D6ECF2D0D9D869FBFB3A3421C2FE4472FFE7AA624155B49
        4FBEF1504167A2D6E4FE1CB7C9000000F882D1CA3C407A6273053955D2996925
        063A6ADAD70D1DF9F870B20FCA3C7BBDD9757F458BF685655B2B2AC8BFC59B9E
        7C8CA0C8333F43DABD2DAF6D4787D2F8033A2894DB640000007CA45F67FFCAE1
        C2AE44229486A9C9C5D6181F3777E4A3130CD99B6DAE7B5A7B8D3FDC79A66DD3
        CCB462A5AF7F3754797965A19B9C2B9595ADDA171886C55C79000008173CDEAB
        63DBFA4C3FDE9ED7BA93F4D87A6313F06A7FB821B2A7B5F177704D795338DD9E
        B12AADEDCBC745DD4B176E94B711B1F2AAC4EDF41489FE8D2D65150AA5E507DC
        2603000008375C8CF7B6AA36FD1FDFDA565945848F6574879140C97EC8E47CA8
        B0B27FE2F2ED95B289822BBC29714BDFF4D0C19C0B37C8DB0E16740A4C56374A
        DC02004024D0D263FAD13B3B2A8AA7A5488626F26C0115BE8593FD4EAEE9460C
        E9CD8FAB56E89F4BDE5B73325E58A4F7F56F8428DEB84491636EBAAC77CB2945
        B6526B7F92DB6400000091C2A0C1F9B9A345DD6F2FCA2C6D243D3B03E9E16155
        3D1FB919D9DB9DCC9D972B54F1D353246AF25BBC1980471EF01C33D34A0656EF
        AE3D57A318FA3DB7B9000000221583C5F55059B3F6CFBBCE2AB2966EAEA88F17
        8A2D18B9FF7F19A9ECDD8C677C87CAFC9D13E2EE45C2DDB5F9A43D4DBE7E77B4
        430B2DC52688EC741EFF1969DFC22EB5E5DB742C07B7D9000000A205AB83B94F
        54AD8E5DBEAD5C169728B2FB9246B485C89E8EC6FFC46FF62EC63BAEAE5DF72B
        414ED579FA8040FEAEFBC6DF0A55E8F88C57D648955B4FB56E35DBDC0F729B0C
        0000209A31589887320E351C2092A0D28AEAE57487237B5A1F3EEB78F3D64942
        31AF969D8D11143153938B75C2DCEA0BF51DFA5F739B0B000000BC87D9CE7E6A
        C7E9964DD3522403F41BAF2F99444338D9FB7C8D4F67371C2EEC5A4EDA481DB3
        AA8837B31BE82B7BB2DDA62559F24A71EDE0CBACE7EA386E93010000800FC2B0
        57C7B5F5597E94B6BFFE04E9E5D3417C5137729F48D34A64BF8B6B92FF41ABCA
        6D3ED1BC99B409AF4ADC92DEBC63F65A49D77151F71B36A7F76E6E7301000080
        8FE7BDE574352F2DDB5A21274289AAF5F389ECED44F6BBB9A6F87F5EAF776C73
        B7F1D9E5DB2B0B89E8F9D416DE49ABC5BAEC338ACC01BDF3CBDCE60200000023
        C36475DF7F46DAB7604EBAB49BC8252A5EED13D93B89EC733D1ECF2D44A25FDC
        78B439977BCBC1A7B10C4C7C6291F684B8E7758CB007000070D3B0AC67ECA0C1
        F585DDF98A35F1C2623E1589094A88ECD9D4776B2F1D2CE84A989224A20B0AF1
        66943D099D4E677E6B5BA54C52AFF907438E0D7798000000809BC7E9F6DC467A
        F9AFC608AE7DB3A683D322766E3E112A1B97786D8D799FFF7B0842D736702CDC
        206FCD93F4BEA637BB1EE60E0B000000105888F0C797356B5E58BEBD524CE443
        A7E9A10A5F7043DF2AD8E99CF9DDE73BD6F40C58BEE566BC18690F000020F8D8
        9DEC84965ED38F0E15740A56EEAC12C52614193931F9121632F278A6A7480632
        8F36EDBD52D51F373064FF128B35E601000084028FC733D6EA60EE51F4197FB4
        F168F3DE18C1B5DEBE2F7921C38F737166697D75DBD01F5D6ECFED5C53030000
        00A1C76463EFAE68D1BE38375DDA4384855AFB238F8BAE48F7EE85F6D40EA5F1
        BB6EC6839E3C000000FEE166BC63EB3B0CBF9A9F21EDA0C55E7C080DF970D829
        49220D7D655FDBAEFF8DD5E199C03527000000C05FFA34B6AFEEBFD4B17A46AA
        A43F264AE6E7FB11BABEBC79F5EEDA0249EDC0BF87CC2E2C5803000020BC3059
        DDF73676197E9175A265D7E4D5E22122373A7DCC97F4A22EF40168D9D68AAA7C
        B96AAE526BFF9A8BC1E03B000000618CCEE47CA8B451FB9220A74A44176B21B2
        8BE655F5E858065BDABEFABCB63ED30FAD0E06AFEC01000044064E173B4EA5B5
        7D85F464E72DC92AAF8FD255F5DC743ADD9653AD3BBA076CDFA29509B9E60100
        000022078BDD3D41A1343D93BAAFEE24374D8F4A3F927BFA74DF5C1305570CA9
        FB1B4E15D70EFC67406F7F946B0E000000207231585C0F16D50C4EDC74AC2977
        E146B9222E514C4BD346D2743D665A4AB14EB0AB467CA8A02BB1B1DBF833B2CF
        0F71BB0F000000440F2EC67B5BCFA0ED1B67A47DAF2DDB525647D79227A20C6B
        E94F5A2D366E3DD9925BD5A6FF23AD61EFF178F1BA1E000000B03A3C773493DE
        EFFE4B1D290B37C8BBC2F49B3E4B1E560CFB2EB6A7A8871C5F66582C3D0B0000
        007C8821B3EB81CAD6A1E749CF78D7D494123A5D2F2CBEE7D3A974CBB797979D
        95F6BDAA33E1753D000000F0B1300C3B46A5B57D99968D9D9224D21199F2797E
        BE6BC186D28E83055D890D9D869F5B1DCC9DDC6E00000000E093D01A9D8FE449
        7A5F5FB1A3AA7C92F0DAFC7C3E49DF3D2D59ACD976AA25A7AC49F327BDD9F500
        B7D9000000001809163B3BA1BE43FFABFD97BB52166F2A6B8D4D10D97C887754
        139F2836A6EDAF3F5758A99AD6AFB33FCE6D2A000000006E065A37BEB451F357
        3ACA7DEE3A591F576F7F347BFA7486807BDE3A59179D39A0509ABEE746895B00
        000020B0300C3B56A5737CA9A0523D8DF4AC4FCF48950C10013B3911FB1274A0
        C290D8166E90B75EAE504FB73BF15D1E000000082AACC73B66C8EC7CB8BED3F8
        5CD6F1A6BDB3D64807B9A97A011DB91F9728722CCA2CEB5A7FB8E1C8952AF5E4
        BE41CB930CEB1DC76D06000000004603BB93BD5B5A3FF8AFADA75A73E76794D2
        D7FBB4A7EF53DEC34D6C82C8F9CE8EAAEAE3A2EE77144AF38F3C1EBCAE070000
        00420ACB7AAE4DD5BB5431F04ADAFEFA0B33D34AE874BD11F7F2270A442CE9C9
        77EFCE576CAA69D7FFCEC578C773FF0400000000F8809BBD3A4EA134FDF0B8B8
        E79D77765494C5275E5B68C7A7D86FCCCB2B0B5D2B7756974AEA3531261B7B3F
        F79300000000E023748DF8DA0EC373B487BE244BDE363141642742A783EC686F
        FF0383F962C87F9F9E22D1A7EEABBB50D5A67B9EFB09000000008403068BFB01
        69FDE0BF330E371E9BB74ED63779B5D848E46EA773F567A44A861667962AD61E
        683873ACA86B555B9FE947DC5F0300000040B8A137BB1E6CE931FCB0BA4DF7DB
        B266CD8B152DDAE76BDB75CF75A8CC4F0F999C0FB3AC070BD600000000000000
        0000000000000000000000000000000000000000000000000000000080E8E2FF
        FDBFFF0FD114883BC5052F7C0000000049454E44AE426082}
      Proportional = True
      ShowHint = False
      Stretch = True
    end
    object lblTituloversao: TLabel
      Left = 485
      Top = 11
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Projeto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object lblusuario: TLabel
      Left = 418
      Top = 30
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'goAssessoria Usu'#225'rio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object lblProjetoresultado: TLabel
      Left = 525
      Top = 11
      Width = 39
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Projeto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object lblusuarioresultado: TLabel
      Left = 525
      Top = 30
      Width = 106
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'goAssessoria Usu'#225'rio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object lblversao: TLabel
      Left = 62
      Top = 32
      Width = 106
      Height = 13
      Caption = 'goAssessoria- Vers'#227'o '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
  end
  object Popupcpg: TPopupMenu
    Left = 502
    Top = 76
  end
  object popupImpressao: TPopupMenu
    Left = 470
    Top = 76
    object menuImpRelSintetico: TMenuItem
      Caption = 'Relat'#243'rio Sint'#233'tico'
      OnClick = menuImpRelSinteticoClick
    end
  end
  object frxcpg: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40094.709455300920000000
    ReportOptions.LastChange = 40094.709455300920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 284
    Top = 78
    Datasets = <
      item
        DataSet = frxDBcpg
        DataSetName = 'frxDBcpg'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
    end
  end
  object frxDBcpg: TfrxDBDataset
    UserName = 'frxDBcpg'
    CloseDataSource = False
    DataSet = cdscpg
    BCDToCurrency = False
    Left = 314
    Top = 78
  end
  object sqlcpg: TSQLDataSet
    CommandText = 'select first 0 * from gocpg'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dm.conexao
    Left = 343
    Top = 77
    object sqlcpgID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object sqlcpgDT_CADASTRO: TDateField
      FieldName = 'DT_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgHS_CADASTRO: TTimeField
      FieldName = 'HS_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgCODPROJETO: TIntegerField
      FieldName = 'CODPROJETO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object sqlcpgPROJETO: TStringField
      FieldName = 'PROJETO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlcpgID_FORNECEDOR: TIntegerField
      FieldName = 'ID_FORNECEDOR'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object sqlcpgFORNECEDOR: TStringField
      FieldName = 'FORNECEDOR'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlcpgDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlcpgDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlcpgVL_BRUTO: TFloatField
      FieldName = 'VL_BRUTO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgVL_MULTA: TFloatField
      FieldName = 'VL_MULTA'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgVL_JUROS: TFloatField
      FieldName = 'VL_JUROS'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgVL_DESCONTO: TFloatField
      FieldName = 'VL_DESCONTO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object sqlcpgVL_PAGO: TFloatField
      FieldName = 'VL_PAGO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgID_BANCO_CONTA: TIntegerField
      FieldName = 'ID_BANCO_CONTA'
      ProviderFlags = [pfInUpdate]
    end
    object sqlcpgBANCO_CONTA: TStringField
      FieldName = 'BANCO_CONTA'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
  end
  object dspcpg: TDataSetProvider
    DataSet = sqlcpg
    UpdateMode = upWhereKeyOnly
    Left = 373
    Top = 77
  end
  object cdscpg: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspcpg'
    BeforePost = cdscpgBeforePost
    Left = 403
    Top = 77
    object cdscpgID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdscpgDT_CADASTRO: TDateField
      DisplayLabel = 'Data de Cadastro'
      FieldName = 'DT_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgHS_CADASTRO: TTimeField
      DisplayLabel = 'Hora de Cadastro'
      FieldName = 'HS_CADASTRO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgCODPROJETO: TIntegerField
      DisplayLabel = 'C'#243'd.Projeto'
      FieldName = 'CODPROJETO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdscpgPROJETO: TStringField
      DisplayLabel = 'Projeto'
      FieldName = 'PROJETO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdscpgID_FORNECEDOR: TIntegerField
      DisplayLabel = 'C'#243'd.Fornecedor'
      FieldName = 'ID_FORNECEDOR'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdscpgFORNECEDOR: TStringField
      DisplayLabel = 'Fornecedor'
      FieldName = 'FORNECEDOR'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdscpgDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdscpgDOCUMENTO: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'DOCUMENTO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdscpgVL_BRUTO: TFloatField
      DisplayLabel = 'Valor Bruto'
      FieldName = 'VL_BRUTO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgVL_MULTA: TFloatField
      DisplayLabel = 'Multa'
      FieldName = 'VL_MULTA'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgVL_JUROS: TFloatField
      DisplayLabel = 'Valor Juros'
      FieldName = 'VL_JUROS'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgVL_DESCONTO: TFloatField
      DisplayLabel = 'Valor Desconto'
      FieldName = 'VL_DESCONTO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgDT_VENCIMENTO: TDateField
      DisplayLabel = 'Data Vencimento'
      FieldName = 'DT_VENCIMENTO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgFORMA_PAGAMENTO: TStringField
      DisplayLabel = 'Forma pagamento'
      FieldName = 'FORMA_PAGAMENTO'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdscpgVL_PAGO: TFloatField
      DisplayLabel = 'Valor Pago'
      FieldName = 'VL_PAGO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgDT_PAGAMENTO: TDateField
      DisplayLabel = 'Data de Pagamento'
      FieldName = 'DT_PAGAMENTO'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgID_BANCO_CONTA: TIntegerField
      DisplayLabel = 'C'#243'digo Conta'
      FieldName = 'ID_BANCO_CONTA'
      ProviderFlags = [pfInUpdate]
    end
    object cdscpgBANCO_CONTA: TStringField
      DisplayLabel = 'Banco Conta'
      FieldName = 'BANCO_CONTA'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object cdscpgSEL: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SEL'
    end
  end
  object dscpg: TDataSource
    DataSet = cdscpg
    Left = 433
    Top = 77
  end
  object cdsfiltros: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    FieldDefs = <
      item
        Name = 'Campo'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Valor'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 531
    Top = 78
  end
  object psqProjeto: TPesquisa
    Colunas = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Visible = True
      end>
    CampoResultado = 'ID'
    CampoDescricao = 'DESCRICAO'
    SQL.Strings = (
      'select  ID, CODGRUPO, GRUPO, DESCRICAO from goprojetos WHERE 1=1')
    MultiSelecao = False
    Conexao = dm.conexao
    AbrirAuto = True
    PesquisaPadrao = 0
    Filtros = <
      item
        DataType = ftString
        Name = 'DESCRICAO'
        ParamType = ptUnknown
        Value = 'Descri'#231#227'o'
      end>
    EditCodigo = dbecpgCODPROJETO
    EditDescricao = dbecpgPROJETO
    BotaoPesquisa = btnprojeto
    Left = 392
    Top = 433
  end
  object psqFornecedor: TPesquisa
    Colunas = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome_fantasia'
        Title.Caption = 'Descri'#231#227'o'
        Width = 200
        Visible = True
      end>
    CampoResultado = 'ID'
    CampoDescricao = 'nome_fantasia'
    SQL.Strings = (
      'select ID, NOME_FANTASIA  FROM GOFORNECEDOR WHERE 1=1')
    MultiSelecao = False
    Conexao = dm.conexao
    AbrirAuto = True
    PesquisaPadrao = 0
    Filtros = <
      item
        DataType = ftString
        Name = 'nome_fantasia'
        ParamType = ptUnknown
        Value = 'Descri'#231#227'o'
      end>
    EditCodigo = dbecpgID_FORNECEDOR
    EditDescricao = dbecpgFORNECEDOR
    BotaoPesquisa = btnFornecedor
    Left = 424
    Top = 433
  end
end
