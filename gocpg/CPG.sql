/******************************************************************************/
/***               Generated by IBExpert 25/10/2021 12:30:25                ***/
/******************************************************************************/

/******************************************************************************/
/***      Following SET SQL DIALECT is just for the Database Comparer       ***/
/******************************************************************************/
SET SQL DIALECT 3;



/******************************************************************************/
/***                                 Tables                                 ***/
/******************************************************************************/


CREATE GENERATOR G_CPG;

CREATE TABLE GOCPG (
    ID               INTEGER NOT NULL,
    DT_CADASTRO      DATE DEFAULT CURRENT_DATE,
    HS_CADASTRO      TIME DEFAULT CURRENT_TIME,
    CODPROJETO       INTEGER NOT NULL,
    PROJETO          VARCHAR(250) COLLATE PT_BR,
    ID_FORNECEDOR    INTEGER NOT NULL,
    FORNECEDOR       VARCHAR(250) COLLATE PT_BR,
    DESCRICAO        VARCHAR(250) COLLATE PT_BR,
    DOCUMENTO        VARCHAR(250) COLLATE PT_BR,
    VL_BRUTO         DOUBLE PRECISION,
    VL_MULTA         DOUBLE PRECISION,
    VL_JUROS         DOUBLE PRECISION,
    VL_DESCONTO      DOUBLE PRECISION,
    DT_VENCIMENTO    DATE,
    FORMA_PAGAMENTO  VARCHAR(250) COLLATE PT_BR,
    VL_PAGO          DOUBLE PRECISION,
    DT_PAGAMENTO     DATE,
    ID_BANCO_CONTA   INTEGER,
    BANCO_CONTA      VARCHAR(250) COLLATE PT_BR
);




/******************************************************************************/
/***                              Primary Keys                              ***/
/******************************************************************************/

ALTER TABLE GOCPG ADD CONSTRAINT PK_GOCPG PRIMARY KEY (ID);


/******************************************************************************/
/***                                Triggers                                ***/
/******************************************************************************/


SET TERM ^ ;



/******************************************************************************/
/***                          Triggers for tables                           ***/
/******************************************************************************/



/* Trigger: GOCPG_GEN */
CREATE OR ALTER TRIGGER GOCPG_GEN FOR GOCPG
ACTIVE BEFORE INSERT POSITION 0
AS
begin
    if ( (new.ID is null) or (new.ID = 0) ) then new.ID = gen_id(G_CPG, 1);
end
^


SET TERM ; ^



/******************************************************************************/
/***                               Privileges                               ***/
/******************************************************************************/
